///@param nodes*
/// Combines nodes into a single node in which all rooms are reachable.
var node_list = argument[0];
var size = ds_list_size(node_list);
sylvie_set_seed(argument[1]);
sylvie_list_shuffle(node_list);
//disabled_show_debug_message("mapp_link: "+string(ds_list_size(node_list)));
var link_factor = 0; 
if argument_count > 2 {
	link_factor = argument[2];
}
var adjacents = ds_list_create();

var xp = 101;
var yp = 101;
var w = 0;
var h = 0;
for(var i=0; i<size; i++) {
	var node = node_list[|i];
	var nd = node[ninfo.data];
	var nw = ds_grid_width(nd);
	var nh = ds_grid_height(nd);
	w = max(w,node[ninfo.xp]+nw);
	h = max(h,node[ninfo.yp]+nh);
	xp = min(xp,node[ninfo.xp]);
	yp = min(yp,node[ninfo.yp]);
}
w -= xp;
h -= yp;

var data = ds_grid_sylvie_create(w,h);
ds_grid_clear(data,0);
// Join adjacent nodes
var visited = ds_map_create();
var kittens = ds_queue_create();
ds_queue_enqueue(kittens,0);
while !ds_queue_empty(kittens) {
	var kitten = ds_queue_dequeue(kittens);
	for(var i=0; i<size; i++) {
		if i == kitten { continue; }
		if visited[?i] { continue; }
		var n1 = node_list[|kitten];
		var n2 = node_list[|i];
		var adjacent = mapp_adjacent(n1,n2);
		if is_array(adjacent) {
			var n1d = n1[ninfo.data];
			var n1w = ds_grid_width(n1d);
			var n1h = ds_grid_height(n1d);
			var n2d = n2[ninfo.data];
			var n2w = ds_grid_width(n2d);
			var n2h = ds_grid_height(n2d);
			var d1 = adjacent[0];
			var d2 = adjacent[1];
			//disabled_show_debug_message("linking "+string(n1w)+","+string(n1h)+" with "+string(n2w)+","+string(n2h));
			ds_list_clear(adjacents);
			var range = adjacent[3] - adjacent[2] + 1;
			//disabled_show_debug_message("insert range "+string(adjacent[2])+"--"+string(adjacent[3]));
			if range > 0 {
				
				if n1w*n1h == 5 and n2w*n2h == 5 {
					ds_list_add(adjacents,adjacent[2]);
					ds_list_add(adjacents,adjacent[3]);
				 	ds_list_add(adjacents,(adjacent[3]-adjacent[2]) div 2);
				 	ds_list_add(adjacents,(adjacent[3]-adjacent[2]) div 2);
				} else {
					var skip = max(1,(range div (link_factor == 0 ? max(2,round(sylvie_choose(25,60)/100)*sqrt(nw*nh)) : link_factor)));
					var start = sylvie_random(adjacent[2],adjacent[3]);
					if range >= 5 {
						while start != (adjacent[3]-adjacent[2]) div 2 {
							var start = sylvie_random(adjacent[2],adjacent[3]);
						}
					}
					var a = start;
					var loop = false;
					while(skip > 0) {
						//disabled_show_debug_message(string(start)+":"+string(a)+":"+string(skip));
						if ds_list_find_index(adjacents,a) == -1 {
							if range < 5 or (range >= 5 and a != (adjacent[3]-adjacent[2]) div 2) {
								ds_list_add(adjacents,a);
							}
							//disabled_show_debug_message(a);
						}
						if loop and a >= start {
							skip--;
							loop = false;
						}
						a += skip;
						if a > adjacent[3] {
							a = (a mod (adjacent[3]+1));
							loop = true;
						}
					}
				}
			}
			sylvie_list_shuffle(adjacents);
			var override = link_factor;
			repeat(override != 0 ? override : 1 ) { //max(1,round(sylvie_choose(25,60)/100)*sqrt(nw*nh))) {
			//repeat(override != 0 ? override : max(1,round(factor*ds_list_size(adjacents)))) {
				if ds_list_empty(adjacents) { break; }
				var pos = adjacents[|ds_list_size(adjacents)-1];
				ds_list_delete(adjacents,ds_list_size(adjacents)-1);
				switch(d1) {
					case edir.up:
						var p1 = [pos,0];
						var p2 = [pos,n2h-1];
					break;
					case edir.down:
						var p1 = [pos,n1h-1];
						var p2 = [pos,0];
					break;
					case edir.left:
						var p1 = [0,pos];
						var p2 = [n2w-1,pos];
					break;
					case edir.right:
						var p1 = [n1w-1,pos];
						var p2 = [0,pos];
					break;
				}
				n1d[# p1[0],p1[1]] |= d1;
				n2d[# p2[0],p2[1]] |= d2;
				var width = 16;
				var height = 8;
				if d1 <= edir.down { 
					var open = sylvie_random(2,width-3);
					if is_array(opening_width) {
						var open = sylvie_random(max(opening_width[0],2),min(opening_width[1],width-3));
					} else if opening_width != 0 {
						if opening_width < 0 {
							var open = sylvie_random(-opening_width,width-3);
						} else {
							var open = sylvie_random(2,opening_width);
						}
					}
					var pos = sylvie_random(1,width-open-1);
				} else {
					var open = sylvie_random(2,height-2);
					if is_array(opening_height) {
						var open = sylvie_random(max(opening_height[0],2),min(opening_height[1],height-2));
					} else if opening_height != 0 {
						if opening_height < 0 {
							var open = sylvie_random(-opening_height,height-2);
						} else {
							var open = sylvie_random(2,opening_height);
						}
					}
					var pos = sylvie_random(1,height-open-1);
				}
				var amount, clear;
				amount = round(log2(d1))*8;
				clear = ~((15 << 4) << amount);
				n1d[# p1[0],p1[1]] &= clear;
				n1d[# p1[0],p1[1]] |= (pos << 4) << amount;
				clear = ~((15 << 8) << amount);
				n1d[# p1[0],p1[1]] &= clear;
				n1d[# p1[0],p1[1]] |= (open << 8) << amount;
				amount = round(log2(d2))*8;
				clear = ~((15 << 4) << amount);
				n2d[# p2[0],p2[1]] &= clear;
				n2d[# p2[0],p2[1]] |= (pos << 4) << amount;
				clear = ~((15 << 8) << amount);
				n2d[# p2[0],p2[1]] &= clear;
				n2d[# p2[0],p2[1]] |= (open << 8) << amount;
			}
			visited[? kitten] = true;
			visited[? i] = true;
			ds_queue_enqueue(kittens,i);
		} 
	}
}
ds_queue_destroy(kittens);
ds_map_destroy(visited);
for(var i=0; i<size; i++) {
	var node = node_list[|i];
	var nd = node[ninfo.data];
	var nw = ds_grid_width(nd);
	var nh = ds_grid_height(nd);
	ds_grid_add_grid_region(data,nd,0,0,nw-1,nh-1,node[ninfo.xp]-xp,node[ninfo.yp]-yp)
}
for(var i=0; i<size; i++) {
	mapp_node_destroy(node_list[|i]);
}
ds_list_destroy(node_list);
ds_list_destroy(adjacents);
return mapp_node(xp,yp,data);