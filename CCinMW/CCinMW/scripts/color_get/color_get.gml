var str = argument[0];
switch(str) {
	case "c_aqua": return c_aqua;
	case "c_black": return c_black;
	case "c_blue": return c_blue;
	case "c_dkgray": return c_dkgray;
	case "c_fuchsia": return c_fuchsia;
	case "c_gray": return c_gray;
	case "c_green": return c_green;
	case "c_lime": return c_lime;
	case "c_ltgray": return c_ltgray;
	case "c_maroon": return c_maroon;
	case "c_navy": return c_navy;
	case "c_olive": return c_olive;
	case "c_orange": return c_orange;
	case "c_purple": return c_purple;
	case "c_red": return c_red;
	case "c_silver": return c_silver;
	case "c_teal": return c_teal;
	case "c_white": return c_white;
	case "c_yellow": return c_yellow;
}
var hsl = string_split(string_extract(str,3,string_length(str)),",");
h = hsl[|0];
s = hsl[|1];
l = hsl[|2];
ds_list_sylvie_destroy(hsl);
return make_color_hsl_240(h,s,l);