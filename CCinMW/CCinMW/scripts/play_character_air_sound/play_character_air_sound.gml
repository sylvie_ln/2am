var air_onscreen = floor(air/max_air*100);

if variable_instance_exists(self, "_air_onscreen_prev")
{
	if argument[0] and air_onscreen > _air_onscreen_prev
		play_sound(sndAirRefill);

	if air_onscreen < 11 && _air_onscreen_prev > air_onscreen
		play_sound(sndAirLowTick);
}
	
_air_onscreen_prev = air_onscreen;