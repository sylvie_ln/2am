var m = argument[0];
var n = argument[1];
opening_width = sylvie_choose(sylvie_random(-7,-1),sylvie_random(1,7))*2;
opening_height = sylvie_choose(sylvie_random(-6,-2),sylvie_random(2,6));
if m > 2 and n > 2 {
	var i = m div 2;
	var j = n div 2;
	var topleft = mapp_mxn_wild(i,j,argument[2]+sylvie_random(0,1024));
	var topright = mapp_mxn_wild(m-i,j,argument[2]+sylvie_random(0,1024));
	topright[ninfo.xp] += i;
	var bottomleft = mapp_mxn_wild(i,n-j,argument[2]+sylvie_random(0,1024));
	bottomleft[ninfo.yp] += j;
	var bottomright = mapp_mxn_wild(m-i,n-j,argument[2]+sylvie_random(0,1024));
	bottomright[ninfo.xp] += i;
	bottomright[ninfo.yp] += j;
	return mapp_link(list_create_nosylvie(topleft,topright,bottomleft,bottomright),argument[2]+sylvie_random(0,1024));
}

var list = ds_list_create();
for(var i=0; i<argument[0]; i++) {
	for(var j=0; j<argument[1]; j++) {
		ds_list_add(list,mapp_node(i,j,mapp_data(0)));	
	}
}
return mapp_link(list,argument[2]+sylvie_random(0,1024));