// @param bounce_count

if global.mute_sfx { exit; }
var bounce_count = argument0;

var s = play_sound(sndCharBounce);
audio_sound_gain(s, (0.2 + power(0.8, bounce_count) * 0.8), 0);
audio_sound_pitch(s, 1 + min(2, bounce_count * 0.1));