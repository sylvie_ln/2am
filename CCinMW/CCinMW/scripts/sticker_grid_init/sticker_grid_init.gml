///@param name
if ds_map_exists(global.sticker_grid_cache_map,sprite_get_name(sprite_index)+"_"+string(floor(image_index))) {
	exit;	
}
var cell = global.bag_grid_cell_size;
var w = sprite_get_width(sprite_index) div cell;
var h = sprite_get_height(sprite_index) div cell;
var grid = ds_grid_sylvie_create(w,h);
ds_map_add(global.sticker_grid_cache_map,sprite_get_name(sprite_index)+"_"+string(floor(image_index)),grid);
var left = x - sprite_xoffset;
var top = y - sprite_yoffset;
for(var i=0; i<w*cell; i+=cell) {
	for(var j=0; j<h*cell; j+=cell) {
		if collision_rectangle(left+i,top+j,left+i+cell,top+j+cell,self,true,false) == noone {
			grid[# i div cell, j div cell] = true;
		} else {
			grid[# i div cell, j div cell] = false;	
		}
	}
}
/*
//disabled_show_debug_message(sprite_get_name(sprite_index)+" "+string(image_index));
for(var j=0; j<h; j++) {
	var str = "";
	for(var i=0; i<w; i++) {
		str += string(grid[# i,j]);
	}
	//disabled_show_debug_message(str);
}