var amount = argument[0];
var axis = argument[1];
var simulation = false;
if argument_count > 2 {
	simulation = argument[2];
}
	
if sign(amount) >= 0 and axis == 1 and fall_through == 0 {
	with instance_place(x,y+max(1,amount),oSpringShroom) {
		if object_index == oJunkParts and floor(image_index) != 2 { continue; }
		if bbox_top > other.bbox_bottom and other.vv >= 0 {
			other.vv = min(-other.vv*1.25,-sqrt(2*abs(other.grav)*32));
			//disabled_show_debug_message(other.vv);
			return true;
		}
	}
}

if amount == 0 { return true; }
if abs(amount) > 12 { amount = clamp(amount,-12,12); } 

var target = round_ties_down(amount+subpixel[axis]);
subpixel[axis] += amount-target;

var xa = (axis == 0) ? 1 : 0;
var ya = (axis == 1) ? 1 : 0;
var s = sign(target);

var moved = 0;
while moved != target{
	var push_worked = true;
	if pusher and !(axis == 1 and s > 0)  {
		ds_list_clear(actors);
		var num = instance_place_list(x+xa*s,y+ya*s,oActor,actors,false);
		var do_push = false;
		var stop = true;
		for(var i=0; i<num; i++) {
			var actor = actors[|i];
			if actor.pushable and beside(actor,axis,s) {
				var stop = false;
				with actor {
					if object_is_ancestor(object_index,oVehicle) and axis == 0 { continue; }
					push_array[i] = move(s,axis,true);
					if axis == 1 and push_array[i] == 0 { push_worked = false; }
					if axis == 1 and s < 0 and object_index == oChar 
					and !object_is_ancestor(other.object_index,oSpringShroom) 
					and other.object_index != oSpringShroom 
					and oChar.fall_through <= 0 { vv = 0; }
					if push_array[i] == 0 {
						stop = true;
					}
				}
				if stop { break; }
			}
		}
		if !stop {
			do_push = true;	
		}
		if do_push {
			for(var i=0; i<num; i++) {
				var actor = actors[|i];
				with actor {
					if axis == 0 {
						x += push_array[i];	
					} else {
						y += push_array[i];	
					}
				}
			}	
		}
	}
	
	if object_index == oChar {
		var spike = instance_place(x+xa*s,y+ya*s,oSpike);
		if spike != noone {
			die(spike);
			break;
		}
	}
	
	if collision_at(x+xa*s,y+ya*s) {
		break;
	}
	
	if !push_worked { break; }
	
	if carrier {
		ds_list_clear(actors);
		var num = instance_place_list(x,y-2,oActor,actors,false);
	}
	
	if !simulation {
		x += xa*s;
		y += ya*s;
	}
	var carry_worked = true;
	if carrier and !(axis == 1 and s < 0)  {
		for(var i=0; i<num; i++) {
			var actor = actors[|i];
			if actor.carriable and actor.bbox_bottom < bbox_top  {
				with actor {
					if carried_this_step[axis] <= abs(target)  {
						if object_index == oChar {
							if axis == 1 and s >= 0 {	
								if vv <= -2 {
									continue;
								}
							}
						}
						if !move(s,axis) and axis == 0 { carry_worked = false; }
						if s != 0 {
							carried_this_step[axis]++;
						}
						if object_index == oChar {
							walkoff_timer = 0;	
						}
					}
				}
			}
		}
	}
	
	moved += s;
	
	if !carry_worked { break; }
}

if simulation {
	return moved;	
}

if moved == target {
	return true;
} else {
	subpixel[axis] = 0.5*s;
	return false;		
}