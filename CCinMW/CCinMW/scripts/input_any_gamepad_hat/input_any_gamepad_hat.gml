for(var g=0; g<gamepad_get_device_count(); g++) {
	if !gamepad_is_connected(g) { continue; }
	if is_undefined(gamepad_hat_count(g)) { continue; }
	for(var i=0; i<gamepad_hat_count(g); i++) {
		var hvalue = gamepad_hat_value(g,i);
		if hvalue == 1 
		or hvalue == 2 
		or hvalue == 4 
		or hvalue == 8 {
			return [i,hvalue];
		}
	}
}
return [-1,0];