fill = 1;
start_xp = 0;
start_yp = 0;
if argument_count > 0 {
	start_xp = argument[0];
	start_yp = argument[1];
}
// make fill grids
doormapp = ds_grid_sylvie_create(w,h);
ds_grid_clear(doormapp,false);
itemmapp = ds_grid_sylvie_create(w,h);
ds_grid_clear(itemmapp,0);
luckmapp = ds_grid_sylvie_create(w,h);
ds_grid_clear(luckmapp,0);
loremapp = ds_grid_sylvie_create(w,h);
ds_grid_clear(loremapp,"");
cutemapp = ds_grid_sylvie_create(w,h);
ds_grid_clear(cutemapp,"");
lore_counter = 0;
item_counter = 0;
rarity = [0,0,0];
meowmeow= 0;
// kittener
//disabled_show_debug_message([start_xp,start_yp]);
kittens = ds_queue_create();
ds_queue_enqueue(kittens,[start_xp,start_yp,0,0,0,0,0,0,-1]);
kittens_visited = ds_grid_sylvie_create(w,h);
ds_grid_clear(kittens_visited,false);
kittens_visited[# start_xp,start_yp] = true;
four = list_create_nosylvie(0,1,2,3);

max_dist = 0;