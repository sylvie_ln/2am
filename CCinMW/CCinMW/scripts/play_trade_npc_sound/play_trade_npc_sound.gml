if !variable_instance_exists(self, "npc") or !instance_exists(npc) or !variable_instance_exists(npc, "text")
	return;
	
if variable_instance_exists(self, "_npc_text_prev")
{
	if _npc_text_prev != npc.text and alarm[7] < 0 {
		alarm[7] = room_speed div 2;
		play_sound(sndDialogSelect);
	}
}

_npc_text_prev = npc.text;