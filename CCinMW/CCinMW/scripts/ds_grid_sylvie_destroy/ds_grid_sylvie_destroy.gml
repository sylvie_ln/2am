var grid = argument[0];
if memory_debug {
	var str = "FREE "+object_get_name(object_index)+":"+string(id)+" destroyed grid #"+string(grid)+"\nCallstack: "+string(debug_get_callstack());
	if grid_debug and memory_detailed_debug {
		show_debug_message(str);
	}
	ds_map_delete(global.sylvie_map_of_grids,grid);
}
ds_grid_destroy(grid);