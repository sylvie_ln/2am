var n = argument[0];
var m = argument[1];

var bigg = power(2,62);
while(n >= m) {
	n -= (n > bigg) ? (bigg div m)*m : (n div m)*m; //clamp((n div m)*m,m,(bigg div m)*m)
	//show_debug_message([n,m,clamp((n div m)*m,m,(bigg div m)*m)]);	
}
while(n < 0) {
	n += (n <= -bigg) ? (bigg div m)*m : max(abs(n div m)*m,m);
	//show_debug_message([n,m,abs(n div m)*m,clamp(abs(n div m)*m,m,(bigg div m)*m)]);
}
return n;


