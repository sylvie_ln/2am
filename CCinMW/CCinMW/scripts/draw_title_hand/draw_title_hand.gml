var spr = argument[0];
var ii = argument[1];
var xp = argument[2];
var yp = argument[3];
var got = argument[4];
var called = argument[5];
sprite_index = spr;
image_index = ii;
var px = x;
var py = y;
x = xp;
y = yp;
if got or called {
	if called {
		draw_outline_ext(c_white,1);
	} else {
		draw_outline_ext(c_dkgray,1);
	}
	draw_self();
} else {
	draw_outline_ext(c_dkgray,1);
}
sprite_index = -1;
x = px;
y = py;