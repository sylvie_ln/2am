var map = global.game_save_map;

save_load_impl("save",map,"game");
var json = json_encode(map);
var buffer = buffer_create(string_length(json)+1,buffer_fixed,1);
buffer_write(buffer,buffer_string,json);
var compressed = buffer_compress(buffer,0,buffer_get_size(buffer));
buffer_save(compressed,global.world_filename+global.game_save_file);
buffer_delete(buffer);
buffer_delete(compressed);