if (ds_list_empty(questions))
	return;

if (!variable_instance_exists(self, "answer_prev"))
{
	answer_prev = answer;
	finished_prev = finished;
}

if (finished_prev != finished)
	play_sound(sndDialogOptionsShow);

if (answer != answer_prev)
	play_sound(sndDialogSelect);
	
answer_prev = answer;
finished_prev = finished;