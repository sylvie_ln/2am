if file_exists(global.world_filename+global.game_save_file) {
	var buffer = buffer_load(global.world_filename+global.game_save_file);
	var decompressed = buffer_decompress(buffer);
	global.game_save_map = json_decode(buffer_read(decompressed,buffer_string));
	buffer_delete(buffer);
	buffer_delete(decompressed);
	save_load_impl("load",global.game_save_map,"game");
}
