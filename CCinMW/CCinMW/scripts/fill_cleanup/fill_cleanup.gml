ds_queue_destroy(kittens);
ds_grid_sylvie_destroy(kittens_visited);
ds_list_destroy(four);
//show_debug_message(rarity);

var mem_map = global.memory_disks[|0];
var mem_pair = mem_map[?stepname];
mem_pair[|0] = 7;
mem_pair[|1] = 0;
//disabled_show_debug_message("memory disk 0: "+stepname+" "+string(mem_pair[|0])+","+string(mem_pair[|1]));
if dumpme and stepname == "Careless Desire" and step == 8 {
	loremapp[# mem_pair[|0],mem_pair[|1]] = "Eivlys";	
	doormapp[# mem_pair[|0],mem_pair[|1]] = true;		
	itemmapp[# mem_pair[|0],mem_pair[|1]] = 0;			
	cutemapp[# mem_pair[|0],mem_pair[|1]] = "";	
	var themapp = mapp[ninfo.data];
	themapp[# mem_pair[|0],mem_pair[|1]] = 0;
	//show_debug_message("zero end!");
}
//disabled_show_debug_message("memory disk 0: "+stepname+" "+string(mem_pair[|0])+","+string(mem_pair[|1]));

if !global.memory_disks_init {
	for(var i=1; i<sprite_get_number(sFloppy); i++) {
		var mem_map = global.memory_disks[|i];
		var mem_pair = mem_map[?stepname];
		mem_pair[|0] = start_xp;
		mem_pair[|1] = start_yp;
		//disabled_show_debug_message("memory disk "+string(i)+": "+stepname+" "+string(mem_pair[|0])+","+string(mem_pair[|1]));
	}
}