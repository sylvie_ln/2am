///@param name
///@param sticker_inst
var name = argument[0];
var sticker = argument[1];
var xp = -1;
var yp = -1;
var bag_grid = global.bag_grid_map[? name]
var bw = ds_grid_width(bag_grid);
var bh = ds_grid_height(bag_grid);
with sticker {
	event_user(1);
}
var sticker_grid = global.sticker_grid_cache_map[? sprite_get_name(sticker.sprite_index)+"_"+string(floor(sticker.image_index))];
var sw = ds_grid_width(sticker_grid);
var sh = ds_grid_height(sticker_grid);

var temp_grid = ds_grid_sylvie_create(bw,bh);

sylvie_set_seed(0);
//repeat((bw-sw)*(bh-sh)) {
//var start_time = get_timer();
var megacount = 256;
while(true) {
	var xp = 0;
	var yp = 0;
	var count = 256;
	while(bag_grid[# xp,yp] == 0) {
		var xp = sylvie_irandom(bw-sw);
		var yp = sylvie_irandom(bh-sh);
		count--;
		if count < 0 { break; }
	}
	if bag_grid_free(name,xp,yp,sticker,temp_grid) {
		sylvie_shuffle();
		ds_grid_sylvie_destroy(temp_grid);
		return [xp,yp];	
	}
	megacount--;
	if megacount < 0 { break; }
	//var elapsed = get_timer()-start_time;
	//if elapsed > (1/room_speed)*1000000 {
		//break;	
	//}
}
sylvie_shuffle();
ds_grid_sylvie_destroy(temp_grid);
return [-1,-1];