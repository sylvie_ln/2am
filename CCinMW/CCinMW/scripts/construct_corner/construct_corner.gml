var corner = argument[0];
var dira, pona, dima;
dira[0] = corner & (edir.left + edir.right);
dira[1] = corner & (edir.up + edir.down);
pona[0] = (dira[0] == edir.left) ? -1 : 1;
pona[1] = (dira[1] == edir.up) ? -1 : 1;
dima[0] = width;
dima[1] = height;
//show_debug_message("corner"+string(corner));
//show_debug_message("dira"+string(dira));
//show_debug_message("pona"+string(pona));

if width == 1 or height == 1 { return 0; }

var corner_travel, travel_start, corner_build, build_start;
var junk = sylvie_choose([[0,1],[1,0]],[[1,0],[0,1]]);
for(var n=0; n<2; n++) {
	//if sylvie_choose(0,1) { continue; }
	var a = junk[n];
	var hori = a[0];
	var vert = a[1];
	if pona[vert] == -1 {
		travel_start = 1;
		//corner_travel = sylvie_random(travel_start,border_position[dira[hori]]);
		corner_travel = border_position[dira[hori]]+((border_opening[dira[hori]]==0)?1:0);
		corner_travel = max(corner_travel,travel_start);
	} else {
		travel_start = dima[vert]-1;
		//corner_travel = sylvie_random(border_position[dira[hori]]+border_opening[dira[hori]],travel_start);
		corner_travel = border_position[dira[hori]]+border_opening[dira[hori]]+((border_opening[dira[hori]]==0)?-1:0);
		corner_travel = min(corner_travel,travel_start);
	}
	corner_build = undefined;
	for(var j=travel_start; j != corner_travel; j += -pona[vert]) {
		//show_debug_message("corner travel"+string([j,travel_start,corner_travel]));
		if pona[hori] == -1 {
			build_start = 1;
			if is_undefined(corner_build) {
				//corner_build = sylvie_random(build_start+(border_position[dira[vert]] div 2),border_position[dira[vert]]);
				corner_build = sylvie_random(build_start,border_position[dira[vert]]);
				//corner_build = border_position[dira[vert]];
			} else {
				corner_build = corner_build+sylvie_choose(-3,-2,-1,-1,0,0,0,0,0,0,0,0,1,1,2,3);
			}
			corner_build = clamp(corner_build,build_start,min(border_position[dira[vert]]+border_opening[dira[vert]],dima[hori]-1));
			corner_build = max(corner_build,build_start);
		} else {
			build_start = dima[hori]-1;
			if is_undefined(corner_build) {
				//corner_build = sylvie_random(border_position[dira[vert]]+border_opening[dira[vert]]+((dima[hori]-border_position[dira[vert]]-border_opening[dira[vert]]) div 2),build_start);
				corner_build = sylvie_random(border_position[dira[vert]]+border_opening[dira[vert]],build_start);
				//corner_build = border_position[dira[vert]]+border_opening[dira[vert]];	
			} else {
				corner_build = corner_build+sylvie_choose(-3,-2,-1,-1,0,0,0,0,0,0,0,0,1,1,2,3);
			}
			corner_build = clamp(corner_build,min(border_position[dira[vert]],1),build_start);
			corner_build = min(corner_build,build_start);
		}
		for(var k=build_start; k != corner_build; k += -pona[hori]) {
			//show_debug_message("corner build"+string([k,build_start,corner_build]));
			//if sylvie_choose(0,1) { continue; }
			if vert == 1 {
				//disabled_show_debug_message("Corner "+string(corner)+" placing at "+string(k)+","+string(j));
				place_block((left+k)*cell_size,(top+j)*cell_size);
			} else {
				//disabled_show_debug_message("Corner "+string(corner)+" placing at "+string(j)+","+string(k));
				place_block((left+j)*cell_size,(top+k)*cell_size);
			}
		}
	}
}