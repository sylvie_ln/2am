var str = "SmallBag"
switch(argument[0]) {
	case oWideBagUpgrade: 
		if global.bag == "TallBag" {
			str = "LargeBag";
		} else {
			str = "WideBag"; 
		}
	break;
	case oTallBagUpgrade: 
		if global.bag == "WideBag" {
			str = "LargeBag";
		} else {
			str = "TallBag"; 
		}
	break;
	case oLargeBagUpgrade: 
		str = "MegaBag"; 
		with oBag {
			if type == 0 {
				y -= 12;	
			}
		}
		with oBagInterior {
			if type == 0 {
				y -= 12;	
			}
		}
	break;
}
global.bag = str;
with oInTheBag {
	bag_spr = asset_get_index("s"+global.bag);
	bag_int_spr = asset_get_index("s"+global.bag+"Interior");
	with oBag {
		if type == 0 {
			sprite_index = other.bag_spr;
		}
	}
	with oBagInterior {
		if type == 0 {
			sprite_index = other.bag_int_spr;
		}
	}
}