var maybe_pogo = argument0;
if maybe_pogo.object_index == oPogo
{
	with maybe_pogo
		instance_destroy();
		
	return noone;
}

return maybe_pogo;