var name = argument[0];
var map = argument[1];
if ds_map_exists(map,name) {
	var value = map[?name];
	for(var k=ds_map_find_first(value); !is_undefined(k); k=ds_map_find_next(value,k)) {
		var suffix = "_GRID_"+k;
		var list = map[?name+suffix];
		var gr = ds_grid_sylvie_create(list[|0],list[|1]);
		sylvie_grid_rw(gr,0,list[|2]);
		value[?k] = gr;
		/*
		var grid = gr;
		//disabled_show_debug_message(name+suffix);
		for(var j=0; j<ds_grid_height(gr); j++) {
			var str = "";
			for(var i=0; i<ds_grid_width(gr); i++) {
				str += string(grid[# i,j]);
			}
			//disabled_show_debug_message(str);
		}
		*/
		
		ds_list_sylvie_destroy(list);
		ds_map_delete(map,name+suffix);
	}
	variable_global_set(name,value);
}
