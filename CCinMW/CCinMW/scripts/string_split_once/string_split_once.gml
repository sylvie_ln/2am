// Splits a single string into a pair at the first occurrence of the given separator.
// The result is returned as an array.
// Example: 
// string is "abc:def"
// separator is ":"
// result is ["abc","def"]
// Arguments:
// 0: The string
// 1: The separator
var str = argument[0];
var sep = argument[1];
for(var i=1; i<=string_length(str)-string_length(sep)+1; i++) {
    if string_copy(str,i,string_length(sep)) == sep {
        break;
    }
}
var result = array_create(2);
result[0] = string_extract(str,1,i-1);
result[1] = string_extract(str,i+string_length(sep),string_length(str));
return result;
