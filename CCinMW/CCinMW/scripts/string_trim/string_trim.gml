// Trims whitespace and unprintable characters from the start and end of a string.
var str = argument[0];
var start = 1;
var stop = string_length(str);
while ord(string_char_at(str,start)) <= ord(" ") and start <= string_length(str) {
    start++;
}
while ord(string_char_at(str,stop)) <= ord(" ") and stop >= 1 {
    stop--;
}
if start > string_length(str) or stop < 1 {
    return "";
}
return string_extract(str,start,stop);
