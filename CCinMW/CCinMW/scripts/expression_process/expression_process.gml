// This function splits expressions into tokens and then passes it to shunting_yard 
// for evaluation. It's kind of a mess, sorry.
enum cond_tokens {
    empty,
    variable,
    constant,
    string_constant,
    unknown,
    left_paren,
    right_paren,
    or_op,
    xor_op,
    and_op,
    not_op,
    eq_op,
    ne_op,
    lt_op,
    le_op,
    gt_op,
    ge_op,
    plus_op,
    minus_op,
    minus_unary_op,
    times_op,
    divide_op,
}

var cond = string_trim(argument[0])+" ";
var token = "";
var state = cond_tokens.empty;
var tokens = ds_list_sylvie_create();
var token_done = false;
for(var i=1; i<=string_length(cond); i++) {
    var c = string_char_at(cond,i);
    if ord(c) <= ord(" ")and state != cond_tokens.string_constant { // whitespace
        if state != cond_tokens.empty { 
            token_done = true;
        }
    } else {
        if ord(c) == ord("'") { // start or end of string constant
            if state == cond_tokens.empty {
                state = cond_tokens.string_constant;
                continue;
            } else if state == cond_tokens.string_constant {
                token_done = true;
            }
        } else if ord(c) >= ord("0") and ord(c) <= ord("9") { // digit 
            if state == cond_tokens.empty {
                state = cond_tokens.constant;
            } else if state != cond_tokens.constant and state != cond_tokens.variable and state != cond_tokens.string_constant {
                token_done = true;
            }
        } else if (ord(c) >= ord("A") and ord(c) <= ord("Z")) or (ord(c) >= ord("a") and ord(c) <= ord("z")) or (ord(c) == ord("_")) or (ord(c) == ord(".")) { // letter or underscore or dot
            if state == cond_tokens.empty {
                state = cond_tokens.variable;
            } else if state != cond_tokens.variable and state != cond_tokens.string_constant {
                token_done = true;
            } 
        } else { // other
            if state == cond_tokens.constant or state == cond_tokens.variable {
                token_done = true;
            } else if state != cond_tokens.string_constant {
                var previous_state = state;
                switch(token+c) {
                    case "&&":
                        state = cond_tokens.and_op;
                    break;
                    case "||":
                        state = cond_tokens.or_op;
                    break;
                    case "^^":
                        state = cond_tokens.xor_op;
                    break;
                    case "!":
                        state = cond_tokens.not_op;
                    break;
                    case "=":
                    case "==":
                        state = cond_tokens.eq_op;
                    break;
                    case "!=":
                        state = cond_tokens.ne_op;
                    break;
                    case "<":
                        state = cond_tokens.lt_op;
                    break;
                    case "<=":
                        state = cond_tokens.le_op;
                    break;
                    case ">":
                        state = cond_tokens.gt_op;
                    break;
                    case ">=":
                        state = cond_tokens.ge_op;
                    break;
                    case "(":
                        state = cond_tokens.left_paren;
                    break;
                    case ")":
                        state = cond_tokens.right_paren;
                    break;
                    case "+":
                        state = cond_tokens.plus_op;
                    break;
                    case "-":
                        state = cond_tokens.minus_op;
                    break;
                    case "*":
                        state = cond_tokens.times_op;
                    break;
                    case "/":
                        state = cond_tokens.divide_op;
                    break;
                    default:
                        state = cond_tokens.unknown;
                    break;
                }
                if previous_state != cond_tokens.empty and previous_state != cond_tokens.unknown and state != previous_state and state == cond_tokens.unknown {
                    state = previous_state;
                    token_done = true;
                }
            }
        }
        if !token_done {
            token += c;
        }
    }
    if token_done {    
        switch(token) {
            case "and":
                state = cond_tokens.and_op;
            break;
            case "or":
                state = cond_tokens.or_op;
            break;
            case "xor":
                state = cond_tokens.xor_op;
            break;
            case "not":
                state = cond_tokens.not_op;
            break;
        }        
        if state != cond_tokens.string_constant {
            i--;
        }
        var token_array = array_create(2);
        token_array[0] = token;
        token_array[1] = state;
        //disabled_show_debug_message("Adding token ["+token+","+string(state)+"]");
        ds_list_add(tokens,token_array);
        token = "";
        state = cond_tokens.empty;
        token_done = false;
    }
}

var result = shunting_yard(tokens);
ds_list_sylvie_destroy(tokens);
return result;
