/// @param sound_index
if global.mute { return 0; }
if global.mute_sfx { return 0; }
if audio_exists(argument0) {
	audio_sound_gain(argument0,global.sfx_volume*get_gain(argument0),0);
	return audio_play_sound(argument0, -1, false);
}
return -1;