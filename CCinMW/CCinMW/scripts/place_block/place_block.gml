var block = collision_point(argument[0],argument[1],oThingy,false,true);
if block == noone
or (instance_exists(block) 
    and !object_is_ancestor(block.object_index,oBlonck)
	and block.object_index != oBlonck) {
	var block = instance_create_depth(argument[0],argument[1],depth,tile);	
	block.generated = true;
	block.number = global.block_counter++;
}