///@param name
///@param x
///@param y
///@param sticker_inst
///@param temp_grid*
var name = argument[0];
var xp = argument[1];
var yp = argument[2];
var sticker = argument[3];
var bag_grid = global.bag_grid_map[? name]
var bw = ds_grid_width(bag_grid);
var bh = ds_grid_height(bag_grid);
with sticker {
	event_user(1);
}
var sticker_grid = global.sticker_grid_cache_map[? sprite_get_name(sticker.sprite_index)+"_"+string(floor(sticker.image_index))];
var sw = ds_grid_width(sticker_grid);
var sh = ds_grid_height(sticker_grid);
if argument_count > 4 {
	var temp_grid = argument[4];
} else {
	var temp_grid = ds_grid_sylvie_create(bw,bh);
}
ds_grid_clear(temp_grid,0);
ds_grid_copy(temp_grid,bag_grid);
ds_grid_add_grid_region(temp_grid,sticker_grid,0,0,sw-1,sh-1,xp,yp)
var free = (ds_grid_get_min(temp_grid,xp,yp,xp+sw-1,yp+sh-1) > 0);

if argument_count <= 4 {
	ds_grid_sylvie_destroy(temp_grid);
}
return free;