var npc = argument[0];
return false
or (npc.object_index == oRaceRunner and (npc.flag_racing mod 2) == 1) 
or (npc.object_index == oBlackRose)
or (npc.object_index == oSnakeRope and npc.mask_index == sSnakeHitbox and abs(oChar.y-npc.bbox_top) > 12)
or (room == rmMushZone and npc.object_index == oEmergencyBubble)
or (npc.object_index == oFlarekitten)
or (npc.object_index == oBunney and !bunney)
or (npc.object_index == oSpike)
or (npc.object_index == oEivlysCurse)
or (npc.object_index == oDiceGoddess)
or (object_is_ancestor(npc.object_index,oShroom) and npc.throwspin)