var name = argument[0];
if ds_map_exists(global.npc_bag_map,name) {
	var bag_sticker_list = global.npc_bag_map[?name];
	for(var i=0; i<ds_list_size(bag_sticker_list); i++) {
		var st_info = bag_sticker_list[|i];
		var item = st_info[|npc_bag_sticker_info.item];
		var cost = item[|item_data.cost];
		var hex = 16+4;
		var on_head = abs(st_info[|npc_bag_sticker_info.xp]-hex) < 1;
		if ds_map_exists(cost,object_get_name(noone)) and on_head {
			ds_list_sylvie_destroy(st_info);
			ds_list_delete(bag_sticker_list,i);
			i--;
			continue;
		}
	}
}