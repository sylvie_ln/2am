var lo = argument[0];
var hi = argument[1];
if(hi <= lo) { return lo; }
var type = random_type.integer;
if argument_count > 2 { type = argument[2]; }
var dist = distribution.uniform;
if argument_count > 3 { dist = argument[3]; }
switch(dist) {
	case distribution.uniform:
		switch(type) {
			case random_type.integer:
			// I dont know how LCGs work. I stole the constants from MMIX. 
			// The modulo thing is probably bad or something.
			// And I dont know if I should right shift by 32 or not. Sorry
				global.sylvie_seed = 
				int64((int64(6364136223846793005) 
				* int64(global.sylvie_seed)
				+ int64(1442695040888963407)) >> 32);
				return (abs(global.sylvie_seed) mod (hi-lo+1))+lo;
			break;
			case random_type.float: return random_range(lo,hi);
		}
	break;
}

enum distribution {
	uniform	
}
enum random_type {
	integer,
	float
}
