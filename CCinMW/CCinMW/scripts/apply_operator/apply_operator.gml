var operand_stack = argument[0];
var op_type = argument[1];
var v1 = argument[2];
var v2 = argument[3];
switch(op_type) {
    case cond_tokens.not_op:
        //disabled_show_debug_message("Applying NOT "+string(v1));
        ds_stack_push(operand_stack,!v1);
    break;
    case cond_tokens.and_op:
        //disabled_show_debug_message("Applying "+string(v1)+" AND "+string(v2));
        ds_stack_push(operand_stack,v1 and v2);
    break;
    case cond_tokens.or_op:
        //disabled_show_debug_message("Applying "+string(v1)+" OR "+string(v2));
        ds_stack_push(operand_stack,v1 or v2);
    break;
    case cond_tokens.xor_op:
        //disabled_show_debug_message("Applying "+string(v1)+" XOR "+string(v2));
        ds_stack_push(operand_stack,v1 xor v2);
    break;
    case cond_tokens.eq_op:
        //disabled_show_debug_message("Applying "+string(v1)+" EQ "+string(v2));
        ds_stack_push(operand_stack,v1 == v2);
    break;
    case cond_tokens.ne_op:
        //disabled_show_debug_message("Applying "+string(v1)+" NE "+string(v2));
        ds_stack_push(operand_stack,v1 != v2);
    break;
    case cond_tokens.lt_op:
        //disabled_show_debug_message("Applying "+string(v1)+" LT "+string(v2));
        ds_stack_push(operand_stack,v1 < v2);
    break;
    case cond_tokens.le_op:
        //disabled_show_debug_message("Applying "+string(v1)+" LE "+string(v2));
        ds_stack_push(operand_stack,v1 <= v2);
    break;
    case cond_tokens.gt_op:
        //disabled_show_debug_message("Applying "+string(v1)+" GT "+string(v2));
        ds_stack_push(operand_stack,v1 > v2);
    break;
    case cond_tokens.ge_op:
        //disabled_show_debug_message("Applying "+string(v1)+" GE "+string(v2));
        ds_stack_push(operand_stack,v1 >= v2);
    break;
    case cond_tokens.plus_op:
        //disabled_show_debug_message("Applying "+string(v1)+" + "+string(v2));
        if is_string(v1) or is_string(v2) {
            ds_stack_push(operand_stack,string(v1) + string(v2));
        } else {
            ds_stack_push(operand_stack,v1 + v2);
        }
    break;
    case cond_tokens.minus_op:
        //disabled_show_debug_message("Applying "+string(v1)+" - "+string(v2));
        ds_stack_push(operand_stack,v1 - v2);
    break;
    case cond_tokens.times_op:
        //disabled_show_debug_message("Applying "+string(v1)+" * "+string(v2));
        ds_stack_push(operand_stack,v1 * v2);
    break;
    case cond_tokens.divide_op:
        //disabled_show_debug_message("Applying "+string(v1)+" / "+string(v2));
        ds_stack_push(operand_stack,v1 / v2);
    break;
    case cond_tokens.minus_unary_op:
        //disabled_show_debug_message("Applying -"+string(v1));
        ds_stack_push(operand_stack,-v1);
    break;
}
return true;
