for(var i=0; i<ds_list_size(global.bag_contents); i++) {
	var info = global.bag_contents[|i];
	if argument[0] == asset_get_index(info[|bag_contents_info.type]) {
		ds_list_delete(global.bag_contents,i);
		return true;
	}
}
return false;