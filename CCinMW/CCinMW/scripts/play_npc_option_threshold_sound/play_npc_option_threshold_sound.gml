if (!variable_instance_exists(self, "npc_prev"))
	self.npc_prev = self.npc;
	
if (self.npc != self.npc_prev)
{

	play_sound(self.npc == noone ? 
	((instance_exists(self.npc_prev) and self.npc_prev.visible) ? sndNpcOptionHide : -1) :
	((instance_exists(self.npc) and self.npc.visible) ? sndNpcOptionShow : -1));
	self.npc_prev = self.npc;
	
}