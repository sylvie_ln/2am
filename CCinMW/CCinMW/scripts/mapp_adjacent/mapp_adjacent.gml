		///@param n1
///@param n2
var n1 = argument[0];
var n2 = argument[1];
var n1d = n1[ninfo.data];
var n1w = ds_grid_width(n1d);
var n1h = ds_grid_height(n1d);
var n2d = n2[ninfo.data];
var n2w = ds_grid_width(n2d);
var n2h = ds_grid_height(n2d);
var n1_left = n1[ninfo.xp];
var n1_right = n1_left+n1w-1;
var n1_top = n1[ninfo.yp];
var n1_bottom = n1_top+n1h-1;
var n2_left = n2[ninfo.xp];
var n2_right = n2_left+n2w-1;
var n2_top = n2[ninfo.yp];
var n2_bottom = n2_top+n2h-1;
var result = false;
if n1_left-1 == n2_right { 
	var base = max(n1_top,n2_top);
	var last = min(n1_bottom,n2_bottom);
	var result = [edir.left,edir.right,0,last-base];
}
if n1_top-1 == n2_bottom { 
	var base = max(n1_left,n2_left);
	var last = min(n1_right,n2_right);
	var result = [edir.up,edir.down,0,last-base];
}
if n1_right+1 == n2_left { 
	var base = max(n1_top,n2_top);
	var last = min(n1_bottom,n2_bottom);
	var result = [edir.right,edir.left,0,last-base];
}
if n1_bottom+1 == n2_top { 
	var base = max(n1_left,n2_left);
	var last = min(n1_right,n2_right);
	var result = [edir.down,edir.up,0,last-base];
}
if is_array(result) and result[2] > result[3] { result = false; }
return result;