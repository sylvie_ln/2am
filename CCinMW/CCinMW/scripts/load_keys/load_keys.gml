var map = argument[0];
with oInput {
	for(var a=0; a<ds_list_size(action_list); a++) {
		var act = action_list[|a];
		var list = actions[? act];
		ds_list_clear(list);
		var i = 0;
		while ds_map_exists(map,"Action_"+act+"_Kind_"+string(i)) {
			get_map_global("Action_"+act+"_Kind_"+string(i),map);
			var kind = variable_global_get("Action_"+act+"_Kind_"+string(i));
			if kind == "Unset" {
				input_assign_unset(act);
			} else {
				get_map_global("Action_"+act+"_Input_"+string(i),map);
				get_map_global("Action_"+act+"_Axis_"+string(i),map);
				var key = variable_global_get("Action_"+act+"_Input_"+string(i));
				var axis = variable_global_get("Action_"+act+"_Axis_"+string(i));
				switch(kind) {
					case "Key": 
						input_assign_key(act,key);
					break;
					case "GamepadButton":
						input_assign_gamepad_button(act,key);
					break;
					case "GamepadAxis":
						input_assign_gamepad_axis(act,key,axis);
					break;
					case "GamepadHat":
						input_assign_gamepad_hat(act,key,axis);
					break;
				}
			}
			i++;	
		}
	}
}