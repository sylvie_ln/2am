if memory_debug {
	global.sylvie_map_of_lists = ds_map_create();
	global.sylvie_max = -1;
	global.sylvie_map_of_grids = ds_map_create();
	global.sylvie_grid_max = -1;
}