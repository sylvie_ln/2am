var str = argument[0]
var value = undefined;
if str == "true" {
    return 1;
}
if str == "false" {
    return 0;
}
if is_color(str) {
	return color_get(str);	
}
var pos = string_pos(".",str);
if pos != 0 {
    var prefix = string_extract(str,1,pos-1);
    var postfix = string_extract(str,pos+1,string_length(str));
    var is_flag = (string_extract(postfix,1,4) == "flag");
    if prefix == "global" {
        if variable_global_exists(postfix) {
            value = variable_global_get(postfix);
        }
    } else {
        if ord(string_char_at(prefix,1)) >= ord("0") and ord(string_char_at(prefix,1)) <= ord("9") {
            // first symbol is a digit, assume an instance id was given
            if variable_instance_exists(real(prefix),postfix) {
                value = variable_instance_get(real(prefix),postfix);
            }
        } else {
            // assume an object name was given
            with asset_get_index(prefix) {
                if variable_instance_exists(id,postfix) {
                    value = variable_instance_get(id,postfix);
                }
            }
        }
    }
} else {
    if variable_instance_exists(id,str) {
        value = variable_instance_get(id,str);
    }
    var is_flag = (string_extract(str,1,4) == "flag");
}
if is_undefined(value) {    
    if is_flag {
        return 0;
    } else {
        show_error("Undefined variable "+str+" encountered in expression.",true);
    }
}
return value;
