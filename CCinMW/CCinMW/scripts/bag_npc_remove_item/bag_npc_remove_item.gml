///@param item
///@param npc
var npc = argument[1];
if !ds_map_exists(global.npc_bag_map,npc.name) {	
	if ds_map_exists(global.trade_map,npc.name) {
		var items = global.trade_map[? npc.name];
		for(var i=0; i<ds_list_size(items); i++) {
			var item = items[|i];
			var item_obj = asset_get_index(item[|item_data.obj]);
			if argument[0] == item_obj {
				ds_list_delete(items,i);
				i--;
				break;
			}
		}
	}
} else {
	var bag_sticker_list = global.npc_bag_map[? npc.name];
	for(var i=0; i<ds_list_size(bag_sticker_list); i++) {
		var st_info = bag_sticker_list[|i];
		var st_type = asset_get_index(st_info[|npc_bag_sticker_info.type]);
		if st_type == argument[0] {
			ds_list_delete(bag_sticker_list,i);
			i--; 
			var bag_grid = global.bag_grid_map[? npc.name]
			var st_x = st_info[|npc_bag_sticker_info.xp];
			var st_y = st_info[|npc_bag_sticker_info.yp];
			var st_ii = st_info[|npc_bag_sticker_info.ii];
			var st = instance_create_depth(0,0,depth-2,oSticker);
			st.sprite_index = asset_get_index(sprite_get_name(object_get_sprite(st_type))+"Sticker");
			st.image_index = st_ii;
			with st {
				event_user(1);
			}
			var bag_pos = [0,76-90];
			//disabled_show_debug_message(pos_key(st_x,st_y));
			var xp = ((st_x - st.sprite_xoffset) + (bag_pos[0])) div global.bag_grid_cell_size;
			var yp = ((st_y - st.sprite_yoffset) + (bag_pos[1])) div global.bag_grid_cell_size;
			//disabled_show_debug_message(pos_key(xp,yp));
			var sticker_grid = global.sticker_grid_cache_map[? sprite_get_name(st.sprite_index)+"_"+string(floor(st.image_index))];
			var sw = ds_grid_width(sticker_grid);
			var sh = ds_grid_height(sticker_grid);
			for(var i=0; i<sw; i++) {
				for(var j=0; j<sh; j++) {
					if sticker_grid[# i,j] == 0 {
						if xp+i >= ds_grid_width(bag_grid) or yp+j >= ds_grid_height(bag_grid) { continue; }
						bag_grid[# xp+i,yp+j] = 1;
					}
				}
			}
			/*
			var grid = bag_grid;
			//disabled_show_debug_message(npc.name);
			//disabled_show_debug_message(sprite_get_name(st.sprite_index));
			for(var j=0; j<ds_grid_width(bag_grid); j++) {
				var str = "";
				for(var i=0; i<ds_grid_height(bag_grid); i++) {
					str += string(grid[# i,j]);
				}
				//disabled_show_debug_message(str);
			}
			*/
			with st {
				instance_destroy();	
			}
			break;
		}
	}
}
return false;