///@param item
///@param npc
var npc = argument[0];
if ds_map_exists(global.npc_bag_map,npc.name) {
	var bag_sticker_list = global.npc_bag_map[? npc.name];
	for(var i=0; i<ds_list_size(bag_sticker_list); i++) {
		var st_info = bag_sticker_list[|i];
		var st_type = asset_get_index(st_info[|npc_bag_sticker_info.type]);
		if st_type == oBasicShroom {
			st_info[|npc_bag_sticker_info.type] = "oHandOfLight";
			st_info[|npc_bag_sticker_info.ii] = hand.light;
			ds_list_replace(bag_sticker_list,i,st_info);
			ds_list_mark_as_list(bag_sticker_list,i);
			exit;
		}
	}
}
return noone;