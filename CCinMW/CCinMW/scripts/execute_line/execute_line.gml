// Implement new SylvieScript NPC commands here.
// There are two modes for processing command arguments.

// Mode 1: Everything following the command name is a single argument.
// This is used by the say command, for example.
// In this mode, the variable "arg" contains the contents of that single argument.

// Mode 2: The stuff following the command name is a comma-separated list of arguments.
// For example, this is used by jumpt and jumpf: the first argument is the conditional
// expression, and the second argument is the label to jump to.
// In this mode, the variable "arg_list" is a ds_list containing all the arguments, 
// where arg_list[|0] is the first,  arg_list[|1] is the second, and so on.

// If one of the arguments to your command is an expression, you can get its value
// using the expression_process script.

var line = argument[0];
var command = line[0];
var arg = line[1];
var arg_list = string_split(arg,",");
for(var i=0; i<ds_list_size(arg_list); i++) {
    arg_list[|i] = string_trim(arg_list[|i]);
}
var argc = ds_list_size(arg_list);
var result = "";
switch(command) {
    case "!stop": 
        stop = true;
    break;
    case "!set": 
        variable_set(arg_list[|0],expression_process(arg_list[|1]));
    break;
    case "!jump":
        current_line = labels[? arg];
    break;
    case "!jumpf":
        if !expression_process(arg_list[|0]) {
            current_line = labels[? arg_list[|1]];
        }
    break;
    case "!jumpt":
        if expression_process(arg_list[|0]) {
            current_line = labels[? arg_list[|1]];
        }
    break;
    case "!jumpe":
        current_line = labels[? string(expression_process(arg))];
    break;
	case "!speaker":
		with oNPC {
			if name==arg_list[|0] {
				other.speaker = self;	
			}
		}
	break;
	case "!play":
		play_sound(asset_get_index(arg_list[|0]));
	break;
	case "!user": // num OR num,obj
		if argc > 1 {
			with asset_get_index(arg_list[|1]) {
				event_user(real(arg_list[|0]));
			}
		} else {
			event_user(real(arg_list[|0]));	
		}
	break;
	case "!call":
		var cx = camera_get_view_x(view_camera[0]);
		var cy = camera_get_view_y(view_camera[0]);
		var cw = camera_get_view_width(view_camera[0]);
		var ch = camera_get_view_height(view_camera[0]);
		var xp = (1-((x-cx) div (cw div 2)))*(cw);
		var yp = (1-((y-cy) div (ch div 2)))*(ch);
		var han = instance_create_depth(cx+xp,cy+yp,depth-100,oCalledHand);
		han.image_index = arg_list[|0];
		han.target = self.id;
		han.delete = asset_get_index(required_hand);
		with han { event_user(0); }
		with object_index {
			if image_index == other.image_index {
				alarm[11]=1;
			}
		}
		called = true;
		stop = true;
	break;
	case "!bag":
		flag_bag=bag_has_item(asset_get_index(arg_list[|0]));
	break;
	case "!handcheck":
		flag_handcheck=hand_check(asset_get_index(arg_list[|0]));
	break;
	case "!move_ns":
	case "!move": // new_x,new_y,time OR new_x,new_y,time,obj
		if argc > 3 {
			var obj = asset_get_index(arg_list[|3]);
		}  else {
			var obj = self;	
		}
		var mover = self;
		with obj {
			move_start_x = x;
			move_start_y = y;
			move_target_x = x+expression_process(arg_list[|0]);
			move_target_y = y+expression_process(arg_list[|1]);
			move_time = expression_process(arg_list[|2])*global.default_room_speed;
			move_timer = move_time;
			move_mover = mover;
			alarm[10] = 1;
		}
		if command == "!move" {
			stop = true;
		}
	break;
	case "!throw": // obj,dir,spd,ii
	case "!throw_ns": // obj,dir,spd,ii
		with speaker {
			if asset_get_index(arg_list[|0]) == oHandOfThunder and global.difficulty != 0 {
				if global.difficulty == 1 {
					trade_modify_item(bag_npc_get_item(oHandOfThunder,id),item_data.desc,"One of the legendary 12 Hands of the Clock. Trade me 1x SpeedShroom for it.",
					item_data.cost,trade_cost(oSpeedShroom,2),item_data.threshold,2);
				} else if global.difficulty == 2 {
					trade_modify_item(bag_npc_get_item(oHandOfThunder,id),item_data.desc,"One of the legendary 12 Hands of the Clock. Trade me 2x SpeedShroom for it.",
					item_data.cost,trade_cost(oSpeedShroom,2),item_data.threshold,4);
				}
				break;
			} 
			if asset_get_index(arg_list[|0]) == oHandOfLeafs and global.difficulty != 0 {
				if global.difficulty == 1 {
					trade_modify_item(bag_npc_get_item(oHandOfLeafs,id),item_data.desc,"One of the legendary 12 Hands of the Clock. Please trade me 1x FloatShroom for it.",
					item_data.cost,trade_cost(oFloatShroom,2),item_data.threshold,2);
				} else if global.difficulty == 2 {
					trade_modify_item(bag_npc_get_item(oHandOfLeafs,id),item_data.desc,"One of the legendary 12 Hands of the Clock. Please trade me 2x FloatShroom for it.",
					item_data.cost,trade_cost(oFloatShroom,2),item_data.threshold,4);
				}
				break;
			} 
			if asset_get_index(arg_list[|0]) == oHandOfEarth and global.difficulty != 0 {
				if global.difficulty == 1 {
					trade_modify_item(bag_npc_get_item(oHandOfEarth,id),item_data.desc,"One of the legendary 12 Hands of the Clock. Trade me 1x SpringShroom for it.",
					item_data.cost,trade_cost(oSpringShroom,2),item_data.threshold,2);
				} else if global.difficulty == 2 {
					trade_modify_item(bag_npc_get_item(oHandOfEarth,id),item_data.desc,"One of the legendary 12 Hands of the Clock. Trade me 2x SpringShroom for it.",
					item_data.cost,trade_cost(oSpringShroom,2),item_data.threshold,4);
				}
				break;
			} 
			if asset_get_index(arg_list[|0]) == oHandOfLight and global.difficulty != 0 {
				if global.difficulty == 1 {
					trade_modify_item(bag_npc_get_item(oBasicShroom,id),
					item_data.obj,oHandOfLight,
					item_data.name,"Hand of Light",
					item_data.desc,"One of the legendary 12 Hands of the Clock. Trade me 2x rare shrooms for it.",
					item_data.cost,
					trade_cost(oKittyShroom,3,oFloatShroom,3,oSpeedShroom,3,oSparkleShroom,3,oSpringShroom,3,oPoisonShroom,3,oRainbowShroom,3),
					item_data.threshold,6);
					bag_npc_change_basic_shroom_into_hand_of_light(id);
				} else if global.difficulty == 2 {
					trade_modify_item(bag_npc_get_item(oBasicShroom,id),
					item_data.obj,oHandOfLight,
					item_data.name,"Hand of Light",
					item_data.desc,"One of the legendary 12 Hands of the Clock. Trade me 3x rare shrooms for it.",
					item_data.cost,
					trade_cost(oKittyShroom,3,oFloatShroom,3,oSpeedShroom,3,oSparkleShroom,3,oSpringShroom,3,oPoisonShroom,3,oRainbowShroom,3),
					item_data.threshold,9);
					bag_npc_change_basic_shroom_into_hand_of_light(id);
				}
				break;
			} 
			var obj = instance_create_depth(x,y,depth,asset_get_index(arg_list[|0]));
			if bag_npc_has_item(obj.object_index,id) and !global.flag_decap {
				bag_npc_remove_item(obj.object_index,id);
			}
			if obj.object_index == oRainbowShroomCutescene {
				bag_npc_remove_item(oRainbowShroom,id);
			}
			with obj {
				if object_index == oHandOfEarth {
					global.flag_hand_earth = 1;	
				} else if object_index == oHandOfThunder {
					global.flag_hand_thunder = 1;	
				} else if object_index == oHandOfLeafs {
					global.flag_hand_leafs = 1;	
				}else if object_index == oHandOfLight {
					global.flag_hand_light = 1;	
				}
				play_sound(sndThrowBagItems);
				if argc > 3 {
					image_index = expression_process(arg_list[|3]);
				}
				if other.flag_surprise {
					image_index = sylvie_irandom(image_number-1);	
				}
				placed = false;
				var dir = sylvie_random(45,135);
				var spd = (30+sylvie_random(0,30))/10;
				if argc > 1 {
					dir=expression_process(arg_list[|1]);
				}
				if argc > 2 {
					spd=expression_process(arg_list[|2]);	
				}
				var factor = 1;
				hv = lengthdir_x(spd,dir);
				vv = lengthdir_y(spd*factor,dir);
				
				var pre = [x,y];
				var try = [dir,dir-180,dir-180+45,dir-180-45];
				for(var i=0; i<array_length_1d(try); i++) {
					var dir = try[i];
					var max_shift = point_distance(bbox_left,bbox_top,bbox_right,bbox_bottom);
					while true {
						if !collision_at(round(x),round(y)) {
							x = round(x);
							y = round(y);
							i = array_length_1d(try);
							break;
						}
						x += lengthdir_x(1,dir);
						y += lengthdir_y(1,dir);
						max_shift--;
						if max_shift < 0 {
							x = pre[0];
							y = pre[1];
							break;
						}
					}
				}
				
				thrown_timer = thrown_time;
				no_carry = true;
				if command == "!throw_ns" {
					throw_ns = true;
				}
				event_user(0);
			}
		}
	break;
	case "!unlock":
		ds_map_delete(global.lock_flag,tag);
		act2 = "Enter";
		bag_remove_item(asset_get_index(global.lock_item[? tag]));
	break;
	case "!tomorrow":
		result = string_replace(arg,"_TOMORROW",date_date_string(date_inc_day(date_current_datetime(),1)));
	break;
	case "!disappear":
		disappear = true;
	break;
	case "!finale":
		instance_create_depth(0,0,0,oTimeRestored);
	break;
	default:
		if arg != "" {
			result = command+" "+arg;
		} else {
			result = command;	
		}
	break;
		
}
ds_list_sylvie_destroy(arg_list);
return result;
