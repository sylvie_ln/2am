///@param set1
///@param set2
var set1 = argument[0];
var set2 = argument[1];
var items = set1[|0];
for(var i=0; i<ds_list_size(items); i++) {
	var item = items[|i];
	if !ds_set_exists(set2,item) {
		return false;
	}
}
return true;
