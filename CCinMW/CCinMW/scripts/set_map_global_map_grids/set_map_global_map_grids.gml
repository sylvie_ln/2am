var name = argument[0];
var map = argument[1];
var value = variable_global_get(name);
if ds_exists(value,ds_type_map) {
	for(var k=ds_map_find_first(value); !is_undefined(k); k=ds_map_find_next(value,k)) {
		var suffix = "_GRID_"+k;
		var lg = list_create(ds_grid_width(value[?k]),ds_grid_height(value[?k]),sylvie_grid_rw(value[?k],1));
		if ds_map_exists(map,name+suffix) {
			ds_list_sylvie_destroy(map[? name+suffix]);
			ds_map_replace_list(map,name+suffix,lg);
		} else {
			ds_map_add_list(map,name+suffix,lg);
		}
		/*
		var grid = value[?k];
		//disabled_show_debug_message(name+suffix);
		for(var j=0; j<ds_grid_height(grid); j++) {
			var str = "";
			for(var i=0; i<ds_grid_width(grid); i++) {
				str += string(grid[# i,j]);
			}
			//disabled_show_debug_message(str);
		}
		*/
	}
	if ds_map_exists(map,name) {
		ds_map_replace_map(map,name,value);
	} else {
		ds_map_add_map(map,name,value);
	}
} else if value == -1 {
	ds_map_delete(map,name);
}