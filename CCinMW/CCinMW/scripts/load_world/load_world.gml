if file_exists(global.world_save_file) {
	var file = file_text_open_read(global.world_save_file);
	global.world_save_map = json_decode(file_text_read_string(file));
	file_text_close(file);
	save_load_impl("load",global.world_save_map,"world");
}
