var axis = argument[0];
if os_type != os_macosx {
	return (axis == 0) ? display_mouse_get_x() : display_mouse_get_y();
} else {
	return (axis == 0) ? device_mouse_raw_x(0) : device_mouse_raw_y(0);	
}