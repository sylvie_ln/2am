var start=["qu","w","wh","r","rh","t","th","y","ygg","p","ph","s","sr","sh","st","sc","sl","sn","sm","d","dr","f","fl","fn","fr","g","gl","gr","h","j","k","kl","kr","l","z","x","xy","c","cl","cr","ch","v","b","bl","br","n","m"];
var last=["a","aa","ae","ai","ao","au","ay","e","ea","ee","ei","eo","eu","ey","i","ia","ie","ii","io","iu","o","oa","oe","oi","oo","ou","u","ua","ue","ui","uo","uu","uy","y","ya","ye","yi","yo","yu"]
var num = sylvie_choose(2,3,4,5,6,7);
var name = "";
for(var i=0; i<num; i++) {
	if i mod 2 == 0 {
		name += start[sylvie_random(0,array_length_1d(start)-1)];
	} else {
		name += last[sylvie_random(0,array_length_1d(last)-1)];
	}
}
return name;