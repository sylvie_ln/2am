///@param name
///@param fluffey
///@param tile
///@param bg_color
///@param bg_overlay
///@param rare
///@param level
global.mushzone_params = ds_map_create();
var i = 0;
var p = global.mushzone_params;
p[? "name"] = argument[i++];
p[? "fluffey"] = argument[i++];
p[? "tile"] = object_get_name(argument[i++]);
p[? "bg_color"] = argument[i++];
p[? "bg_overlay"] = sprite_get_name(argument[i++]);
p[? "rare"] = object_get_name(argument[i++]);
p[? "level"] = argument[i++];