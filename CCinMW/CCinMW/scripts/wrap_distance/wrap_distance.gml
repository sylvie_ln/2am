var x1 = argument[0];
var y1 = argument[1];
var x2 = argument[2];
var y2 = argument[3];
var w = argument[4];
var h = argument[5];
var mdx = min(abs(x2-x1),abs(x2+w-x1),abs(x2-w-x1));
var mdy = min(abs(y2-y1),abs(y2-h-y1),abs(y2-h-y1));
return mdx+mdy;