///@param obj
///@param name
///@param desc
///@param cost
///@param threshold

var item = ds_list_create();
for(var i=0; i<item_data.last; i++) {
	if i == item_data.obj {
		ds_list_add(item,object_get_name(argument[i]));
	} else {
		ds_list_add(item,argument[i]);
	}
	if i == item_data.cost {
		ds_list_mark_as_map(item,ds_list_size(item,)-1);
	}
}	
item[|item_data.restock] = 0;
ds_map_add_list(global.item_map,object_get_name(argument[item_data.obj]),item);


enum item_data {
	obj,
	name,
	desc,
	cost,
	threshold,
	last,
	// Specials
	restock,
}