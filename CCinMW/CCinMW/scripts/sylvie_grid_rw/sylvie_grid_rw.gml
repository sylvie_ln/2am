//Game maker default implementation.
var grid = argument[0];
var read = !argument[1];
if read {
	var input = argument[2];	
	ds_grid_read(grid,input);
} else {
	return ds_grid_write(grid);
}

/*
//SLOW Sylvie implementation.

var delim=","
//var escape="!COMMA!";
//var quote="\""
var grid = argument[0];
var num = !is_string(grid[# 0,0]);
var str=string(ds_grid_width(grid))+delim+string(ds_grid_height(grid))+delim+(num ? "r" : "s")+delim;
//var escapeme = false;
var read = !argument[1];
if read {
	var input = argument[2];	
}
if read {
	var list = string_split(input,",");
	var w = list[|0];
	var h = list[|1];
	var type = list[|2];
	var i = 0;
	var j = 0;
	for(var k=3; k<ds_list_size(list); k++) {
		grid[# i,j]=(type == "s") ? list[|k] : real(list[|k]);
		j++;
		if j >= h {
			j = 0;
			i++;
			if i >= w {
				break;	
			}
		}
	}
	ds_list_destroy(list);
} else {
	for(var i=0; i<ds_grid_width(grid); i++) {
		for(var j=0; j<ds_grid_height(grid); j++) {
			str+=string(grid[# i,j])+delim;	
		}
	}
	return str;
}
*/