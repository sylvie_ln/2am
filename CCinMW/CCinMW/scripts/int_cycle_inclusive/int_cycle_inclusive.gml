var min_value = min(argument2, argument0);
var current_value = argument1;
var max_value = max(argument2, argument0);

var len = max_value - min_value;
if len == 0
	return min_value;
	
len += 1;
	
while current_value < min_value
	current_value += len;
	
while current_value > max_value
	current_value -= len;
	
return current_value;