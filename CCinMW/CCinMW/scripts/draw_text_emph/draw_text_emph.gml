var xp = argument[0];
var yp = argument[1];
var txt = argument[2];
var txt_emph_list = string_split(txt,"|");
for(var i=0; i<ds_list_size(txt_emph_list); i++) {
	var emph = i mod 2 == 1;
	var txt = txt_emph_list[|i];
	if emph {
		draw_set_alpha(0.25);
		draw_text(xp,yp+1,txt);
		draw_text(xp+1,yp,txt);
		draw_text(xp+1,yp+1,txt);
		draw_set_alpha(1);
	}
	draw_text(xp,yp,txt);
	
	xp += string_width(txt);
}
ds_list_destroy(txt_emph_list);