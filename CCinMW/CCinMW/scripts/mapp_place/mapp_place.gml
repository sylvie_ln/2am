///@param x
///@param y
///@param node
var xp = argument[0];
var yp = argument[1];
var node = argument[2];
var nd = node[ninfo.data];
var nw = ds_grid_width(nd);
var nh = ds_grid_height(nd);
ds_grid_set_grid_region(mapp,nd,0,0,nw-1,nh-1,xp,yp);