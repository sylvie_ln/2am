var flag_handcheck = false;
flag_handcheck=bag_has_item(argument[0]);
if !flag_handcheck {
	switch(argument[0]) {
		case oHandOfTears:
			flag_handcheck = global.flag_hand_tears;
		break;
		case oHandOfDarkness:
			flag_handcheck = global.flag_hand_darkness;
		break;
		case oHandOfLight:
			flag_handcheck = global.flag_hand_light;
		break;
		case oHandOfFlames:
			flag_handcheck = global.flag_hand_flames;
		break;
		case oHandOfLeafs:
			flag_handcheck = global.flag_hand_leafs;
		break;
		case oHandOfLies:
			flag_handcheck = global.flag_hand_lies;
		break;
		case oHandOfStorms:
			flag_handcheck = global.flag_hand_storms;
		break;
		case oHandOfSky:
			flag_handcheck = global.flag_hand_sky;
		break;
		case oHandOfEarth:
			flag_handcheck = global.flag_hand_earth;
		break;
		case oHandOfMoon:
			flag_handcheck = global.flag_hand_moon;
		break;
		case oHandOfDreams:
			flag_handcheck = global.flag_hand_dreams;
		break;
		case oHandOfThunder:
			flag_handcheck = global.flag_hand_thunder;
		break;
	}				
}
return flag_handcheck;