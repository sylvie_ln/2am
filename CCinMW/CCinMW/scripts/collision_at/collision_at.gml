var xp = argument[0];
var yp = argument[1];
if solid_boundary {
	if very_solid_boundary or !(instance_exists(oChar) and beside(oChar,1,-1) and place_meeting(x,y-2,oChar)) {
		var sticker = asset_get_index(sprite_get_name(sprite_index)+"Sticker");
		if !sprite_exists(sticker) { sticker = sprite_index; }
		var wl = abs(sprite_get_xoffset(sprite_index)-sprite_get_bbox_left(sticker));
		var wr = abs(sprite_get_xoffset(sprite_index)-sprite_get_bbox_right(sticker));
		var hu = abs(sprite_get_yoffset(sprite_index)-sprite_get_bbox_top(sticker));
		var hd = abs(sprite_get_yoffset(sprite_index)-sprite_get_bbox_bottom(sticker));
		if	xp <  camera_get_view_x(view_camera[0])+wl
		or	xp >= camera_get_view_x(view_camera[0])+camera_get_view_width(view_camera[0])-wr 
		or yp <  camera_get_view_y(view_camera[0])+hu
		or	yp >= camera_get_view_y(view_camera[0])+camera_get_view_height(view_camera[0])-20-hd {
			if	x <  camera_get_view_x(view_camera[0])+wl
			or	x >= camera_get_view_x(view_camera[0])+camera_get_view_width(view_camera[0])-wr
			or	y <  camera_get_view_y(view_camera[0])+hu
			or	y >= camera_get_view_y(view_camera[0])+camera_get_view_height(view_camera[0])-20-hd {
				return solid_at(xp,yp) or blocker_at(xp,yp);
			}
			return true;	
		}
	}
}
return solid_at(xp,yp) or blocker_at(xp,yp);