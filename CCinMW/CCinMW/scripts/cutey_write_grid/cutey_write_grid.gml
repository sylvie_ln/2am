if !dumpme { exit; }
var grid = argument[0];
var store = argument[1];
var def = argument[2];
var v = "temp";
var w = ds_grid_width(grid);
var h = ds_grid_height(grid);
	/*
	mapmapp[? name] = mapp[ninfo.data];
	mapmapp[? name+"_door"] = doormapp;
	mapmapp[? name+"_item"] = itemmapp;
	mapmapp[? name+"_luck"] = luckmapp;
	mapmapp[? name+"_lore"] = loremapp;
	mapmapp[? name+"_cute"] = cutemapp;
	*/
cutey_write(store+"=ds_grid_create("+string(w)+","+string(h)+")");
var gridstr = ds_grid_write(grid);
var strsize = string_length(gridstr)+1;
var buffer = buffer_create(strsize,buffer_fixed,1);
buffer_write(buffer,buffer_string,gridstr);
var cmp = buffer_compress(buffer,0,strsize);
var cmpstr = buffer_base64_encode(cmp,0,strsize);
cutey_write("var "+v+" = buffer_base64_decode(\""+cmpstr+"\")");
cutey_write("var "+v+"_cmp = buffer_decompress("+v+")");
cutey_write("var "+v+"_str = buffer_read("+v+"_cmp,buffer_string)");
cutey_write("ds_grid_read("+store+","+v+"_str)");
cutey_write("buffer_delete("+v+")");
cutey_write("buffer_delete("+v+"_cmp)");
buffer_delete(buffer);
buffer_delete(cmp);
/*
cutey_write("var "+v+"="+store);
cutey_write("ds_grid_clear("+v+","+def+")");
for(var i=0; i<w; i++) {
	for(var j=0; j<h; j++) {
		if string(grid[#i,j]) == def { continue; }
		cutey_write(v+"[#"+string(i)+","+string(j)+"] = "
		+(is_string(grid[# i,j]) ? "\"" : "")
		+string(grid[# i,j])
		+(is_string(grid[# i,j]) ? "\"" : ""));
	}
}