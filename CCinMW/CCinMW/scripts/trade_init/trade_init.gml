if object_index == oInTheBag {
	var name = npc.name;
} else {
	var name = self.name;
}
if !ds_map_exists(global.trade_map,name) {
	var list = ds_list_sylvie_create();
	ds_map_add_list(global.trade_map,name,list);
} else {
	ds_list_clear(global.trade_map[? name]);	
}
restock_count = 0;