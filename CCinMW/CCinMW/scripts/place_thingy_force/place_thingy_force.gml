if argument[0] == noone { return noone; }
var xx = argument[1]*cell_size;
var yy = argument[2]*cell_size;
//show_debug_message([xx,yy]);
var hpos_start = xx+10+20;
var hpos_end = xx+320-10-20;
var hdir = sylvie_choose(-1,1);
if hdir == -1 {
	var temp = hpos_start;
	hpos_start = hpos_end;
	hpos_end = temp;
}
var yoff = -8;
var spr = object_get_sprite(argument[0]);
if sprite_exists(spr) {
	yoff = sprite_get_yoffset(spr)-sprite_get_bbox_bottom(spr)-1;
}
var vpos_start =  yy+160-10-20;
var vpos_end = yy+10+20;
var hpos = hpos_start;
var vpos = vpos_start;
while(true) {
	var blonck = collision_rectangle(hpos-8,vpos+8,hpos+7,vpos+9,oBlonck,false,true);
	if blonck != noone {
		var blonck = collision_rectangle(hpos-4,vpos-4,hpos+3,vpos+3,oBlonck,false,true);
		with blonck { 
			for(var i=0; i<ds_list_size(other.valid_blocks); i++) {
				if other.valid_blocks[|i] == id {
					ds_list_delete(other.valid_blocks,i);
					break;
				}
			}
			instance_destroy(); 
		}	
		var inst = instance_create_depth(hpos,vpos+10+yoff,depth,argument[0]);
		inst.generated = true;
		return inst;
	}
	hpos += 20*hdir;
	if hpos*hdir > hpos_end*hdir {
		vpos -= 20;
		hpos = 0;
		if vpos < vpos_end {
			if oRoomGen.xp == 0 and oRoomGen.yp == 0 {
				show_error("Horrible error.",true);
			}
			return noone;
		}
	}
}