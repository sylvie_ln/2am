var bp = array_create(9,-1);
var bo = array_create(9,-1);
var l = argument[0];
var t = argument[1];
var w = argument[2];
var h = argument[3];

// top border
var count = 0;
var start = false;
var d = edir.up;
for(var i=l; i<l+w; i++) {
	if start {
		if collision_point(i*16,t*16,oBlonck,false,true) == noone {
			bo[d] = count++;
		} else {
			break;
		}
	} else {
		if collision_point(i*16,t*16,oBlonck,false,true) != noone {
			bp[d] = count++;
		} else {
			bo[d] = 1;
			start = true;	
			count = 2;
		}
	}
}
if !start {
	bo[d] = 0;
	bp[d] = w div 2;
} else {
	bp[d] += 1;
}
//disabled_show_debug_message(string(d)+" p"+string(bp[d])+" o"+string(bo[d]));
// bottom border
var count = 0;
var start = false;
var d = edir.down;
for(var i=l; i<l+w; i++) {
	if start {
		if collision_point(i*16,(t+h-1)*16,oBlonck,false,true) == noone {
			bo[d] = count++;
		} else {
			break;
		}
	} else {
		if collision_point(i*16,(t+h-1)*16,oBlonck,false,true) != noone {
			bp[d] = count++;
		} else {
			bo[d] = 1;
			start = true;	
			count = 2;
		}
	}
}
if !start {
	bo[d] = 0;
	bp[d] = w div 2;
} else {
	bp[d] += 1;
}
//disabled_show_debug_message(string(d)+" p"+string(bp[d])+" o"+string(bo[d]));
// left border
var count = 0;
var start = false;
var d = edir.left;
for(var i=t; i<t+h; i++) {
	if start {
		if collision_point(l*16,i*16,oBlonck,false,true) == noone {
			bo[d] = count++;
		} else {
			break;
		}
	} else {
		if collision_point(l*16,i*16,oBlonck,false,true) != noone {
			bp[d] = count++;
		} else {
			bo[d] = 1;
			start = true;	
			count = 2;
		}
	}
}
if !start {
	bo[d] = 0;
	bp[d] = h div 2;
} else {
	bp[d] += 1;
}
//disabled_show_debug_message(string(d)+" p"+string(bp[d])+" o"+string(bo[d]));
// right border
var count = 0;
var start = false;
var d = edir.right;
for(var i=t; i<t+h; i++) {
	if start {
		if collision_point((l+w-1)*16,i*16,oBlonck,false,true) == noone {
			bo[d] = count++;
		} else {
			break;
		}
	} else {
		if collision_point((l+w-1)*16,i*16,oBlonck,false,true) != noone {
			bp[d] = count++;
		} else {
			bo[d] = 1;
			start = true;	
			count = 2;
		}
	}
}
if !start {
	bo[d] = 0;
	bp[d] = h div 2;
} else {
	bp[d] += 1;
}
//disabled_show_debug_message(string(d)+" p"+string(bp[d])+" o"+string(bo[d]));
return [bp,bo];