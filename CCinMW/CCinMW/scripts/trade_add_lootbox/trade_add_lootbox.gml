///@param obj
///@param per
///@param rank
var obj = argument[0];
var per = argument[1];
var rank = argument[2];
var sm = "0";
switch(rank) {
	case 1: sm="5"; break;
	case 2: sm="6"; break;
	case 3: sm="9"; break;
	case 4: sm="15"; break;
	case 5: sm="16"; break;
	case 6: sm="20"; break;
	case 7: sm="25"; break;
}
var obj_name=object_get_name(obj);
obj_name=string_extract(obj_name,2,string_length(obj_name));
var num = 0;
var acc = 0;
while(acc < rank) {
	acc += per;
	num++;
}
return trade_add_item(oSurpriseBox,"Surprise Box! ("+sm+" SM)", "A surprising box that contains so much value inside. I'll trade it for "+obj_name+" "+string(num)+"x.",
trade_cost(obj,per),rank);
