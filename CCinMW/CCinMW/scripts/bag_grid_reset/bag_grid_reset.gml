///@param name
var name = argument[0];
if ds_map_exists(global.bag_grid_map,name) {
	ds_grid_destroy(global.bag_grid_map[? name]);
}
ds_map_delete(global.bag_grid_map,name);