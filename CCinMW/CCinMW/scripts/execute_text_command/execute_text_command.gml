// Implement new SylvieScript text commands here.

// How it works:
// The command and all its arguments get deleted from the string.
// The result is inserted into the string at the point where the command started.
// If the result is "", the string is left unmodified.

// If the effect of the command isn't just a simple text replacement, you'll probably
// just set some flags here, and then modify the oTextbox object to make it 
// react to the flags correctly. For example, the "ask" command just fills up the
// "questions" list, and then oTextbox has a bunch of code for displaying 
// questions and allowing the player to select the result.

// Note: command names cannot start with n because this is reserved for newline escape \n.

var text = argument[0];
var i = argument[1];
var name = process_command(text,i,process_command_actions.get_name);
var args = process_command(text,i,process_command_actions.get_args);
var result = "";
switch(name) {
    case "\\":
        result = "\\";
    break;
	case "n":
		result = "\n";
	break;
    case "print":
        result = string(expression_process(args[|0]));
    break;
    case "ask":
        ds_list_copy(questions,args);
    break;
    case "color":
        var color = expression_process(args[|0]);
        effects[? i] = [text_effects.color,color];
    break;
}
ds_list_sylvie_destroy(args);
return result;
