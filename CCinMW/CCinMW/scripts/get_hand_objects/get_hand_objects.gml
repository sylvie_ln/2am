var result = [];

with (oHand) {
	if target == oClonkTower { continue; }
	if waiting { continue; }
	result = append_array(result, self);
}
	
with (oCalledHand)
	result = append_array(result, self);
	
with (oInTheBag)
{
	if holding and sticker == sHandsSticker
	{
		var p = pogo();
		p.x = smouse_x;
		p.y = smouse_y;
		p.xprevious = global.mouse_xprevious;
		p.yprevious = global.mouse_yprevious;
		result = append_array(result, p);
	}
}
with (oSticker)
{
    if sprite_index == sHandsSticker {
		xprevious = xprev;
		yprevious = yprev;
        result = append_array(result, self);
	}
}

return result;