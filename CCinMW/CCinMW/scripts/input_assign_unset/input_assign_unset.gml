///@param action_name String representing the action name.

var action = argument[0];
var list = oInput.actions[? action];
var was_waiting = false;
for(var j=0; j<ds_list_size(list); j++) {
	var input = list[|j];
	if input[|0] == input_kind.waiting {
		input[|0] = input_kind.unset;
		was_waiting = true; 
		break;
	}
}
if !was_waiting {
	ds_list_add(list,list_create_nosylvie(input_kind.unset));
}