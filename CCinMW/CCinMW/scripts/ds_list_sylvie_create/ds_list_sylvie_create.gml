var list = ds_list_create();
if memory_debug {
	global.sylvie_max = max(global.sylvie_max,list);
	var str = "ALOC "+object_get_name(object_index)+":"+string(id)+" created list #"+string(list)+"\nCallstack: "+string(debug_get_callstack());
	if list_debug and memory_detailed_debug {
		show_debug_message(str);
	}
	global.sylvie_map_of_lists[?list] = str;
	return list;
}
return list;