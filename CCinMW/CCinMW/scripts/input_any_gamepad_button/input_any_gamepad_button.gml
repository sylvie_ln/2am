for(var g=0; g<gamepad_get_device_count(); g++) {
	if !gamepad_is_connected(g) { continue; }
	if gamepad_button_check_pressed(g,gp_face1) { return gp_face1; }
	if gamepad_button_check_pressed(g,gp_face2) { return gp_face2; }
	if gamepad_button_check_pressed(g,gp_face3) { return gp_face3; }
	if gamepad_button_check_pressed(g,gp_face4) { return gp_face4; }
	if gamepad_button_check_pressed(g,gp_start) { return gp_start; }
	if gamepad_button_check_pressed(g,gp_select) { return gp_select; }
	if gamepad_button_check_pressed(g,gp_stickl) { return gp_stickl; }
	if gamepad_button_check_pressed(g,gp_stickr) { return gp_stickr; }
	if gamepad_button_check_pressed(g,gp_shoulderl) { return gp_shoulderl; }
	if gamepad_button_check_pressed(g,gp_shoulderr) { return gp_shoulderr; }
	if gamepad_button_check_pressed(g,gp_shoulderlb) { return gp_shoulderlb; }
	if gamepad_button_check_pressed(g,gp_shoulderrb) { return gp_shoulderrb; }
	if gamepad_button_check_pressed(g,gp_padu) { return gp_padu; }
	if gamepad_button_check_pressed(g,gp_padd) { return gp_padd; }
	if gamepad_button_check_pressed(g,gp_padl) { return gp_padl; }
	if gamepad_button_check_pressed(g,gp_padr) { return gp_padr; }
}
return -1;