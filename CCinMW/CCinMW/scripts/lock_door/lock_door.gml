///@param door_tag
///@param obj
///@param desc
global.lock_item[? argument[0]] = object_get_name(argument[1]);
global.lock_name[? argument[0]] = argument[2];
global.lock_flag[? argument[0]] = true;