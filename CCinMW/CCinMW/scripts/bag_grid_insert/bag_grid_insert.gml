///@param name
///@param x
///@param y
///@param sticker_inst
var name = argument[0];
var xp = argument[1];
var yp = argument[2];
var sticker = argument[3];
var bag_grid = global.bag_grid_map[? name]
var bw = ds_grid_width(bag_grid);
var bh = ds_grid_height(bag_grid);
with sticker {
	event_user(1);
}
var sticker_grid = global.sticker_grid_cache_map[? sprite_get_name(sticker.sprite_index)+"_"+string(floor(sticker.image_index))];
var sw = ds_grid_width(sticker_grid);
var sh = ds_grid_height(sticker_grid);
for(var i=0; i<sw; i++) {
	for(var j=0; j<sh; j++) {
		if xp+i >= bw or yp+j >= bh { continue; }
		bag_grid[# xp+i,yp+j] = min(bag_grid[# xp+i,yp+j],sticker_grid[# i,j]);
	}
}
//ds_grid_set_grid_region(bag_grid,sticker_grid,0,0,sw-1,sh-1,xp,yp);

/*
var grid = bag_grid;
//disabled_show_debug_message(name);
//disabled_show_debug_message(sprite_get_name(sNPCBag));
for(var j=0; j<bh; j++) {
	var str = "";
	for(var i=0; i<bw; i++) {
		str += string(grid[# i,j]);
	}
	//disabled_show_debug_message(str);
}