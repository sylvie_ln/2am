if argument[0] == noone { return noone; }
if !ds_list_empty(valid_blocks) {
	var vb = noone; 
	while !ds_list_empty(valid_blocks) {
		var vb = valid_blocks[|ds_list_size(valid_blocks)-1];
		ds_list_delete(valid_blocks,ds_list_size(valid_blocks)-1);
		if instance_exists(vb) { break; }
	}
	if !instance_exists(vb) or is_undefined(vb) { return noone; }
	while vb.image_alpha == 0.5  {
		if ds_list_empty(valid_blocks) { return noone; }
		ds_list_delete(valid_blocks,ds_list_size(valid_blocks)-1);
		if ds_list_empty(valid_blocks) { return noone; }
		vb = valid_blocks[|ds_list_size(valid_blocks)-1];
	}
	var yoff = -8;
	var spr = object_get_sprite(argument[0]);
	if sprite_exists(spr) {
		yoff = sprite_get_yoffset(spr)-sprite_get_bbox_bottom(spr)-1;
	}
	var pos = [vb.x+(cell_size div 2),vb.y+yoff];
	var inst = instance_create_depth(pos[0],pos[1],depth,argument[0]);
	inst.generated = true;
	return inst;
}
return noone;