if variable_global_exists("bgm") and audio_exists(global.bgm) and audio_is_playing(global.bgm) {
	audio_stop_sound(global.bgm);
	global.bgm = -1;
}
stop_radio_bgm();