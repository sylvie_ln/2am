script=@"Big. Strong. Robust.
When you hear those words, the only thing that comes to mind is a Truck.
!if global.flag_sylvietruck==0
 But I've had a truck for 30 years. I'd like to try a new kind of vehicle, possibly one with snow-ability.
 Maybe it would be good if it also looked like a Kitty. Yes, some kind of snow kitty vehicle.
!elif 1
 But thanks to you, I've retired from my years of Trucking. I'm now having a fun time in the plentiful snow this world has.
!fi"
name="Truckster"

trade_add_item(oSylvieTruck);
trade_add_lootbox(oSpeedShroom,1,1); // 1:5
trade_add_lootbox(oSpeedShroom,1,1); // 1:5
trade_add_lootbox(oSpeedShroom,1,1); // 1:5

// PRE TRADE
// When you first open the trade screen.
orig_text = "It's a lovely day for a trade.";
// When you select an item other than the selected one.
finish_text = "Want a different item?"
// When you deselect an item.
dont_text = "You don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "Don't want to trade it back."
// Prefix before item name
thatsmy_text = "This is the "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "For this trade it's something else I need."
// When you offer a low-rank item.
low_text = "Great, a speedy shroom."
// When you offer a mid-rank item.
medium_text = "That item seems good."
// When you offer a high-rank item.
high_text = "Yes! I love that item!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't need that many of 'em."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Hmm, needs more speed."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Hmm, needs more speed."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Hmm, some of that stuff, I don't want."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Hmm, some of that stuff, I don't want."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "I can't believe such a Kittey-Vehicle exists. I really want to drive it."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "What I seek must be snowey, kittey, and vehicular."
// When you offer more than one of the exact item.
multiple_exact_text = "Two of this vehicle is simply too much for me to handle, just give one."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "I want only that snowey kittey, so take out the other items."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I'm not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Rejected, sorry."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "No, I am looking for something more Kittyish and Snowey."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Amazing, let us begin the exchange."
// When you take your item back after the NPC accepts your offer.
back_text = "Huh? Backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Sorry, can't back out of the trade this late."
// When the NPC begins taking you items.
thank_text = "Great, I'll take it."
// When a trade ends and you didn't attempt to steal.
complete_text = "What a good trade! Thanks!"
// When a trade ends and you attempted to steal.
complete_steal_text = "What a bad trade! Thanks!"

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Stealing from a trucker, bad move."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Don't like your rude attitude."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Cool."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the stuff you were going to trade me."
// When you pick up an item that you stole.
yes_give_text = "Yeah, give that back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep it. But give back the stuff you were going to trade."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No, I don't want that. Give back what you were originally going to trade!"