script=@"The shoes that let you walk through this world bereft of time.
I'm the most incredible at shoe making. Will you see the shoes I've spent my life crafting?"
name="Crisis"

trade_add_item(oPegasusBoots);
trade_add_item(oHoverHeels);
trade_add_item(oClimbingCleats);
trade_add_item(oSpringSneakers);
trade_add_item(o100TonShoes);
// PRE TRADE
// When you first open the trade screen.
orig_text = "Welcome to the Shoe Shop, how may I shoe you?";
// When you select an item other than the selected one.
finish_text = "Oh, a different shoe interests you."
// When you deselect an item.
dont_text = "Oh, that's not the shoe for you?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "Don't wanna trade it back."
// Prefix before item name
thatsmy_text = "The "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "I need very specific materials to make these shoes. Can't use junk like that."
// When you offer a low-rank item.
low_text = "Yes! That is what is needed for this shoes."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't need so many materials."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need more materials than this."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I need a little more materials than this."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I don't need extra stuff, only the materials for this shoe."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I don't need extra stuff, only the materials for this shoe."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No deal."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "These materials aren't good for this shoes."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = "This isn't enough to make a shoes."

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Let's trade, enjoy a new shoe."
// When you take your item back after the NPC accepts your offer.
back_text = "Hey, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Will you offer more for these shoes?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You can't back away, it's \"shoe\" late."
// When the NPC begins taking you items.
thank_text = "Thanks, I love when I give a shoe to someone."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thank you, let's shoe again!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Shoe away."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Oh, shoe off...."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "You're terrible, I hate you today."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Glad that's over."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You have some of the promised items in your bag."
// When you pick up an item that you stole.
yes_give_text = "Give it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "No, keep that. Give back the promised items only."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "Don't want that.... give back just the promised-items."