script=@"!if global.flag_horse==0
We are just two married wifes who love this canyon view. We want to live here in the peaceful sky.
I heard someone in the Citey is knowledgeable about building structures for people to live in. 
If we could get something like that, it would be a miracle.
!elif 1
 This "+"\"house\""+@" you gave us looks great. Once time begins to flow again, we'll get it set up.
!fi"
name="TroubleAndStrife"

trade_add_item(oHorse);
trade_add_lootbox(oKittyShroom,3,5); // 2:16
trade_add_lootbox(oKittyShroom,3,5); // 2:16

// PRE TRADE
// When you first open the trade screen.
orig_text = "We just want peaceful lifes as peaceful wifes.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm... (thinking together)"
// When you select something you traded to an NPC
no_tradebacks_text = "We don't want to trade it back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "We have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Nope, that's not what we want for this trade."
// When you offer a high-rank item.
high_text = "We wouldn't mind some kitteys as a pet."
// When you offer exactly what they want.
too_many_text = "Whoa, that's too much kittys."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Need more of those cats, meow." 
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text =  "Need more of those cats, meow." 
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Hmm, can you take out the stuff that's not a kitty?"
// When your offer oes not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text =  "Hmm, can you take out the stuff that's not a kitty?"

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Oh! This looks like a lovely structure for us wifes."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "I would say that's not suitable for living in."
// When you offer more than one of the exact item.
multiple_exact_text = "It doesn't seem like we need more than one of those!"
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "If we have a place to live, we don't need any other item."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Sorry, we won't trade this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "That's a rather shabby offer, don't you think?"
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Wonderful, thank you for giving us a home."
// When you take your item back after the NPC accepts your offer.
back_text = "Hey, don't back out now...."
// When you offer more stuff after the NPC accepts your offer.
more_text = "Hm?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late to trade back."
// When the NPC begins taking you items.
thank_text = "Thank you so much."
// When a trade ends and you didn't attempt to steal.
complete_text = "It was wonderful to meet you."
// When a trade ends and you attempted to steal.
complete_steal_text = "Farewell, little thief."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hee hee, you'll regret it if you don't put that down."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "You don't want wifes like us as your enemy. Better give it back."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Meep *"
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "Don't break your promise to us. Give back all items."
// When you pick up an item that you stole.
yes_give_text = "That's right, give it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "No, keep that. Give back the stuff you stole, though."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "Don't want that, just give back what you stole."