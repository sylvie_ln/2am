script=@"Amazing, I am a geologist.
Never have I heard of such Rock formations to appear in the sky like this. Until time resumes its flow, I must study them and collect samples of the Rocks."
name="Jealousy"

trade_add_item(oHandOfSky);
trade_add_item(oBoringRock,"Crystal Rock","This looks like an ordinary Rock, but it contains crystals I must research.",trade_cost(oBoringRock,1,oUniqueRock,3),99);
trade_add_item(oBoringRock,"Skull Rock","This looks like an ordinary Rock, but it is actually a fossilized head of a Dinosaur.",trade_cost(oBoringRock,1,oUniqueRock,3),99);
trade_add_item(oBoringRock,"Turtle Rock","This looks like an ordinary Rock, but there is a vast maze-like dungeon inside of it.",trade_cost(oBoringRock,1,oUniqueRock,3),99);
trade_add_item(oBoringRock,"Ancient Rock","This looks like an ordinary Rock, but it is actually from 100,000,000 years ago.",trade_cost(oBoringRock,1,oUniqueRock,3),99);
trade_add_item(oBoringRock,"Singing Rock","This looks like an ordinary Rock, but it is actually a pop idol.",trade_cost(oBoringRock,1,oUniqueRock,3),99);
trade_add_item(oBoringRock,"Eivlys Rock","This looks like an ordinary Rock, but it is an artifact created by the witch Eivlys.",trade_cost(oBoringRock,1,oUniqueRock,3),99);
trade_add_item(oBoringRock,"Choco Rock","This looks like an ordinary Rock, but it tastes like a yummy bar of choco.",trade_cost(oBoringRock,1,oUniqueRock,3),99);

// PRE TRADE
// When you first open the trade screen.
orig_text = "I love to see Rocks that have never been seen. Rocks that are so unique.";
// When you select an item other than the selected one.
finish_text = "Ah, you want another item?"
// When you deselect an item.
dont_text = "How interesting."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't wish to trade this back."
// Prefix before item name
thatsmy_text = "This's a "
// NPC bag is empty after evaluating offer

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't want that item...."
// When you offer a low-rank item.
low_text = "I like Rocks, but that one does not seem interesting."
// When you offer a mid-rank item.
medium_text = "That item seems good."
// When you offer a high-rank item.
high_text = "W-wow.... a Rock shaped of a Kitty!! But I can't bear to trade a Rock for a Rock!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "No, won't trade...."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "No, won't trade...."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "No, won't trade...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "No, won't trade....."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "No, won't trade...."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "W-wow!!! You MUST give me this Rock that's shaped of a sweet Kitty!!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "I will only trade this for a truely unique Rock."
// When you offer more than one of the exact item.
multiple_exact_text = "I just need one, don't spoil me!"
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Don't need the other things, just give me that wonderful Kitty shaped Rock!!"

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I simply will not trade this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Sorry, I will not trade for this offer."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Geological! Let's trade!"
// When you take your item back after the NPC accepts your offer.
back_text = "Whoa, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late to reverse the trade!"
// When the NPC begins taking you items.
thank_text = "I will take this rock so lovely."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thank you, this is amazing."
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, don't take that..."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "This is a crime."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Though you gave it back, you truly have a black heart."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Give it back please."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "I don't want it back, I want the items you promised me."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I do not want that, I want the items you promised me."