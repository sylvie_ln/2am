script=@"Today's exchange rates are:
Basic Shroom = 1 SM\nTall Shroom = 2 SM\nWide Shroom = 2 SM\nTiny Shroom = 3 SM\nWorthless Shroom = 0 SM (Most Shops Will Turn You Away)\nRare Shroom = Nonexchangeable Due To High Counterfeit Rates
SM = Shroom Mony (Currency)"
name="Stocs"

repeat(6) {
	trade_add_item(oDollars);	
}

// PRE TRADE
// When you first open the trade screen.
orig_text = "I don't bother with silly item trades anymore, I'm more interested in Investing and the Stock Market.";
// When you select an item other than the selected one.
finish_text = "Gaze upon my dollars."
// When you deselect an item.
dont_text = "Gaze upon my dollars."
// When the NPC evaluates your offer
hmm_text = "Ignoring you...."
// Prefix before item name
thatsmy_text = "That's "

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't care...."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Don't care...."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Sorry, not trading it."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text