script="I sell keys. Don't forget it."
name="Lockme"

trade_recet_fluffy(name);

trade_add_item(oRegularKey);
trade_add_item(oRegularKey);
trade_add_item(oRegularKey,"Low Quality Key","A particularly horrid key. Costs 1 SM.",trade_standard_costs(),1);
trade_add_item(oRegularKey,"Feel Key","I really like the feel of this one. Costs 7 SM.",trade_standard_costs(),7);
trade_add_item(oRegularKey,"Bargains Key","40% off for a limited time. Costs 3 SM.",trade_standard_costs(),3);
trade_add_item(oRegularKey,"Demon Key","This key once belonged to a Demon. Costs 6 SM.",trade_standard_costs(),6);
trade_restock_me(trade_add_item(oRegularKey,"Expensive Key","This one costs alot for no reason. Costs 10 SM.",trade_standard_costs(),10));
trade_add_item(oRoseKey);
trade_add_item(oCloudKey);
trade_add_item(oKittenKey);

// PRE TRADE
// When you first open the trade screen.
orig_text = "No matter what the quest you are on, you'll need keys.";
// When you select an item other than the selected one.
finish_text = "Oh, a different one?"
// When you deselect an item.
dont_text = "Don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "Not trading it back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't want it."
// When you offer a low-rank item.
low_text = "The basics."
// When you offer a mid-rank item.
medium_text = "A large one. Interesting."
// When you offer a high-rank item.
high_text = "Now that's quite tiny and cute."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Hey, that's way too much."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Not enough."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "A bit more."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Don't try to pawn off extra stuff on me. Just pay with SM."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Not enough. And get rid of some of the junk."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Don't waste my time.."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Great. You've unlocked my heart."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Oh?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Too late to back out."
// When the NPC begins taking you items.
thank_text = "Wonderful."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks."
// When a trade ends and you attempted to steal.
complete_steal_text = "I'll let you go this time."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Put it down or I'll knife you."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "On the verge of knifing you."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "A wise choice."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You're not safe from the knife yet. Put them all back."
// When you pick up an item that you stole.
yes_give_text = "Yes, put it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "No backing out. Give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't care about that. Put back the items you promised."