script=@"I am simply known as a Bagsmith. My specialty is to create large Bags.
I can increase your Bag, just give me what I need for my power.";
name="Beleve";

trade_add_item(oWideBagUpgrade);
trade_add_item(oTallBagUpgrade);
trade_add_item(oLargeBagUpgrade);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Will you create a new Bag with my powers?";
// When you select an item other than the selected one.
finish_text = "Perhaps you would like this power?"
// When you deselect an item.
dont_text = "Not interested in this power? I see."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade it back."
// Prefix before item name
thatsmy_text = "This is the "
// NPC bag is empty after evaluating offer

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "This does not empower me."
// When you offer a low-rank item.
low_text = "Hee hee, now that is useful for my power."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "That is too much power."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need more items for this power."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I need more items for this power."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I do not need items that don't contribute to my power."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I do not need items that don't contribute to my power."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No, I won't trade this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "This is no good."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Great, this will update your bag, hee hee."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Hm?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You can't back out."
// When the NPC begins taking you items.
thank_text = "I will increase my power with this, thanks."
// When a trade ends and you didn't attempt to steal.
complete_text = "Enjoy this new bag."
// When a trade ends and you attempted to steal.
complete_steal_text = "Enjoy this new bag, stealing thief."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Now, we're both adults here. Put that back."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Good."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, place that back down."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep it. Give back all the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "That's not one of the items you promised."