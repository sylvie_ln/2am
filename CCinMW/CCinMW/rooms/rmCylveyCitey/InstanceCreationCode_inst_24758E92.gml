script=@"
!if flag_too_bad==1
 !if global.flag_rainbow==0
  I am so grateful for these incredible rainbow shrooms.
  Yet, as the Prophecy of Eivlys has foretold, I can only perform the ceremony once I have passed on the Hand of Dreams.
  Please, you must trade a RainbowShroom for my Hand of the Clock.
 !fi
 !set flag_too_bad,0
!elif 1
 !if global.flag_rainbow==1
  !set global.flag_rainbow,2
  I am in your debt. I can begin the ceremony I have heard of.
  !move -40,0  ,(1/2)*(40/40)
  !move   0,48 ,(1/2)*(48/40)
  Stand under the tallest flower.
  !move -16,0  ,(1/2)*(80/40)
  And throw the RainbowShroom high into the sky.
  !throw oRainbowShroomCutescene,90,12
 !elif global.flag_rainbow==2
  This is the most beautiful rainbow to me.
  I am so thankful for you and the rainbow you brought me.
 !elif 1
  A beautiful RainbowShroom sleeps in the time-frozen world.
  And when it wakes, a beautiful rainbow appears in the time-frozen sky.
  That is what the Prophecy says. But where is this rainbow Shroom to be found?
 !fi
!fi"
name="Rainbis"

trade_add_item(oHandOfDreams);
trade_add_lootbox(oRainbowShroom,3,7); // 3:25

// PRE TRADE
// When you first open the trade screen.
orig_text = "I am always on the lookout for an amazing rainbow.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
// Prefix before item name
// NPC bag is empty after evaluating offer

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "That's not quite rainbow."
// When you offer a low-rank item.
low_text = "I guess that item is okay...."
// When you offer a mid-rank item.
medium_text = "That item seems good."
// When you offer a high-rank item.
high_text = "Yes, now that's so rainbow!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Whoa, that's too much!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I'd like some more...."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Maybe a little more...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I only want RainbowShrooms...."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I only want RainbowShrooms...."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Wow!! It's that rainbow shroom!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = no_text
// When you offer more than one of the exact item.
multiple_exact_text = "Just trade me one RainbowShroom for this!"
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Not interested in anything but that Rainbow Shroom!"

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No, that's not for trade...."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "I decline this offer."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text =  "Not interested in anything but a shroom so rainbow."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Great, let's have such a rainbow trade."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late to back out."
// When the NPC begins taking you items.
thank_text = "I'll take this rainbow item."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks so much!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Shame on you for trying to steal."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Please don't steal."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "You're so dishonest."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, please give back this item."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can have it, but give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that, please give me the items you promised."
