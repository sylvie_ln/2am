script=@"The shopping district starts here and extends to the left, why don't you enjoy?
I personally sell handmade dolls and plusheys, they're guaranteed to cuddle you until the end of time."
name="Plunch"

trade_add_item(oSylviePlushey);
trade_add_item(oSylviePlushey);
trade_add_item(oDrummerPlushey);
trade_add_item(oDrummerPlushey);
trade_add_item(oJIGGLER);
trade_add_item(oJIGGLER);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Lovely handmade items for sale.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back."
// Prefix before item name
thatsmy_text = "That's a "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Sorry, I only accept standard SM currency."
// When you offer a low-rank item.
low_text = "A basic shroom, I'll take it."
// When you offer a mid-rank item.
medium_text = "Interesting, a larger mushroom."
// When you offer a high-rank item.
high_text = "Wow, you have some tiny shrooms."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "That's too much items for me."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Sorry, that is not enough cash."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "It costs just a little bit more than that."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Sorry, I only accept standard SM currency."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Sorry, I only accept standard SM currency."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "This isn't for sale."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "That's not a good trade offer..."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Woo, I hope you enjoy this new friend!"
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.out_of_bag_text = "Um, you can't back out of the trade now...."
// When the NPC begins taking you items.
thank_text = "Thank you for your purchase."
// When a trade ends and you didn't attempt to steal.
complete_text = "See you again!"
// When a trade ends and you attempted to steal.
complete_steal_text = "See you again."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Don't steal the items...."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Wow!"
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Cool."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Good, give it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can have it, just give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that, please give me the items you promised...."