script=@"!if global.flag_paperclips==1
 Hey, you helped me get rid of all those paperclips I hated. They are all gone now.
 For that, I'll give you my special treasure. It's so special that I keep it in my pocket instead of my bag.
 Thank you for your kindness.
 !set global.flag_paperclips,2
 !throw oPinkPaperclip,135,6
!elif global.flag_paperclips==2
 My world is now immaculate.
!else
 I hate paperclips that are red.\nIt's a useless item that you could never trade for anything good.\nIt's an item you wouldn't want to create for any reason.\nI really hate it, and I don't want to look at it.
!fi"
name="Pipi"

repeat(13) {
	trade_add_item(oRedPaperclip);
}

// PRE TRADE
// When you first open the trade screen.

// Alt text version!
orig_alt_text = "I'm finally free of suffering.";
if global.flag_paperclips == 0 {
	orig_text = "These hated red clips plague my life, I must be rid of them all.";
} else {
	orig_text = orig_alt_text;
}
// For alt text, add a hook in oInTheBag as well.

// When you select an item other than the selected one.
finish_text = "You want a different one??"
// When you deselect an item.
dont_text = "Oh, you don't want it? Good taste."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "You expect me to trade this back?!"
// Prefix before item name
thatsmy_text = "That's "

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Now that looks like a worthless item, suitable to trade a red paperclip for."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "Not what I want...."
// When you offer more than one of the exact item.
multiple_exact_text = "I only need you to trade me one worthless item."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Don't need anything else except that worthless item."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I absolutely won't trade it."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "No thanks."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Finally, I can get rid of this horrible item."
// When you take your item back after the NPC accepts your offer.
back_text = "Are you backing out??"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Hmm?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You can't back out of the trade."
// When the NPC begins taking you items.
thank_text = "I'll take it now."
// When a trade ends and you didn't attempt to steal.
complete_text = "Amazing, I got rid of one of this paperclips...."
// When a trade ends and you attempted to steal.
complete_steal_text = "Great, I got rid of one of this paperclips...."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, don't."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Cut it out...."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "How annoying."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, give back that."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "No way, you can keep that."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "Don't want that, give me the promised item."