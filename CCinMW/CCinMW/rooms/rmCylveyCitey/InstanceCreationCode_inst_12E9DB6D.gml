script=@"I'm an engineer, I sell lots of utilitys for exploration. Do you want to learn about them? \ask{Yes}{No}
!if answer==1
 When exploring Mushroom Zones, you might find that you can't get far enough to discover rare valuables. My items will help you get just a little farther.
 A Snake Rope will extend in the air and let you climb higher. It doesn't extend outside of Mushroom Zones, but it's quite useful inside, and can be picked up and reused.
 An Emergency Bubble is a one-time use item that lets you create an air refill spot. You can test it out in the outer worlds, but once you throw it in a Mushroom Zone, it can't be picked up.
 A Bottled Flarekitten can sniff out rare items and reveal secret passages. Again, once you release one in a Mushroom Zone, you can't get it back, but it will stick around in that zone forever.
 Finally, an Oxygen Egg will reduce the rate of air depletion in Mushroom Zones, as long as it's in your bag, they are quite pricey though.
!fi"
name="Uuella"

trade_recet_fluffy(name)

trade_add_item(oOxygenEgg,"Oxygen Egg (75% off)","Amazing deal. Reduces the rate of air depletion in Mushroom Zones. Costs 10 SM.",
trade_standard_costs(),10);
trade_add_item(oOxygenEgg,"Oxygen Egg (50% off)","Cool deal. Reduces the rate of air depletion in Mushroom Zones. Costs 20 SM.",
trade_standard_costs(),20);
trade_add_item(oOxygenEgg);
trade_restock_me(trade_add_item(oSnakeRope));
trade_restock_me(trade_add_item(oSnakeRope));
trade_restock_me(trade_add_item(oEmergencyBubble));
trade_restock_me(trade_add_item(oEmergencyBubble));
trade_restock_me(trade_add_item(oBottledFlare));
trade_restock_me(trade_add_item(oBottledFlare));

// PRE TRADE
// When you first open the trade screen.
orig_text = "Welcome to the one and only Utility Shop.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade this back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Use standard SM currency please."
// When you offer a low-rank item.
low_text = "A tasty basic mushroom."
// When you offer a mid-rank item.
medium_text = "A tasty larger mushroom."
// When you offer a high-rank item.
high_text = "A tasty tiny mushroom."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Aren't you overpaying a little for that?"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "That costs a little more."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "That costs a little more."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Use only standard SM currency please."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Use only standard SM currency please."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I'm not trading that."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "I respectfully decline this offer."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Great, I hope this is useful."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You can't back out of the trade at this point."
// When the NPC begins taking you items.
thank_text = "Thanks for your purchase."
// When a trade ends and you didn't attempt to steal.
complete_text = "See you later!"
// When a trade ends and you attempted to steal.
complete_steal_text = "People will hate you if you steal."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, cut it out."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Put it back."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Good."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yeah, put it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep it. I want the items your promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No, not that. The items you originally promised."