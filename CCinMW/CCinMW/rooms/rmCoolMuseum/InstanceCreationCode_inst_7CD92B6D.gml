name="Alicia"

if global.heist_stage != 1 { instance_destroy(); }

script=@"!if global.heist_stage==1
 Good, you made it. Nobody's looking, hand me the kitty. That's thief slang for the goods.
!elif global.heist_stage==2
 We're all set, better get the Hell out of here before the guards notice something. Later.
 !disappear
!fi"

// When you first open the trade screen
orig_text = "Quick, hand over the stuff before the guards come.";
// When you attempt to trade for an untradeable item.
refuse_text = "I'll pass it back when we're in the clear."
// When you offer an unwanted item.
no_text = "Great, now that is a lovely item to steal."
complete_text = "I'll meet you outside for the handoff. Don't be late."
dont_text = "I'll meet you outside for the handoff."
// When you select something you traded to an NPC
no_tradebacks_text = "No need to trade me, I'll throw it to you."
// Prefix before item name
thatsmy_text = "That's our "