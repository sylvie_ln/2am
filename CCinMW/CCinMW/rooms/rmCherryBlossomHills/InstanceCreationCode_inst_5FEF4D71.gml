script=@"You see that Bunney of the walls? It is said this is a symbol of the magic of Eivlys.
Who's Eivlys you ask, heh, you don't wanna know. All I can say is that I am not one of her Followers...."
name="Bunnisse"

trade_add_item(oGreenShard);
trade_add_lootbox(oFloatShroom,2,2); // 1:6
trade_add_lootbox(oFloatShroom,2,2); // 1:6
trade_add_lootbox(oFloatShroom,2,2); // 1:6

// PRE TRADE
// When you first open the trade screen.
orig_text = "I am just a humble passerby, what would I possibly have to trade?";
// When you select an item other than the selected one.
finish_text = "Heh heh."
// When you deselect an item.
dont_text = "Heh."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade it back."
// Prefix before item name
thatsmy_text = "This is a "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "That won't do for this particular item."
// When you offer a low-rank item.
low_text = "Ah, now that is so bunneyesque!"
// When you offer a low-rank item.
medium_text = "Wow, this floats like a butterfly."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Not sure I want some of those things...."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Not sure I want some of those things...."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I'm not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Nothing you offered seems interesting...."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "Nothing you offered seems bunneyish...."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "I accept, shall we begin the trade?"
// When you take your item back after the NPC accepts your offer.
back_text = "Oh?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Eh?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late to back out.... ku ku."
// When the NPC begins taking you items.
thank_text = "Thank you for this.... hee hee."
// When a trade ends and you didn't attempt to steal.
complete_text = "Have a lovely day."
// When a trade ends and you attempted to steal.
complete_steal_text = "Have a horrible day, stealing bandit."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Don't steal, you'll invoke my wrath.."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "My wrath increases."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "My wrath has subsided."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items which you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, that one. Put it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You keep that, but give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "Give me the items you promised, not that thing."