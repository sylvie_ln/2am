name="Sunny"
script=@"I'll say it someday. Thank you for everything."

trade_add_item(oLocket);
if global.difficulty == 1 {
	trade_add_item(
	oHandOfLeafs,"Hand of Leafs","One of the legendary 12 Hands of the Clock. Please trade me 1x FloatShroom for it.",
	trade_cost(oFloatShroom,2),2);
} else if global.difficulty == 2 {
	trade_add_item(
	oHandOfLeafs,"Hand of Leafs","One of the legendary 12 Hands of the Clock. Please trade me 2x FloatShroom for it.",
	trade_cost(oFloatShroom,2),4);
}
trade_add_lootbox(oSparkleShroom,3,3); // 1:9
trade_add_lootbox(oSparkleShroom,3,3); // 1:9
trade_add_lootbox(oSparkleShroom,3,3); // 1:9


// When you first open the trade screen// PRE TRADE
// When you first open the trade screen.
orig_text = "I don't have much to trade....";
// When you select an item other than the selected one.
finish_text = "Oh, you're interested in this?"
// When you deselect an item.
dont_text = "Yeah, I guess you don't care about that...."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade it back."
// Prefix before item name
thatsmy_text = "That's my "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Sorry, um.... that's not what I'm looking for."
// When you offer a high-rank item.
medium_text = "Ah, this item.... I'd like it, if that's okay."
// When you offer a high-rank item.
high_text = "Ah, this item.... I'd like it, if that's okay."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Oh, don't waste so many items on me...."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need a little more...."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I need a little more...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Um.... sorry, some of that stuff I don't want."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Um.... sorry, some of that stuff I don't want."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No matter what you offer, I'm not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Sorry, I'm looking for something different...."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "I'll never let go of this."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Okay, let's try a trade."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, are you backing out? Sorry...."
// When you offer more stuff after the NPC accepts your offer.
more_text = "Oh, hm..."
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Ah... uh... it's too late to back out, I think...."
// When the NPC begins taking you items.
thank_text = "Thank you, I will take this."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thank you, I hope you treasure that item."
// When a trade ends and you attempted to steal.
complete_steal_text = "See you...."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Ah..."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Um..."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you...."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "I'd like it if you give back the items you promised...."
// When you pick up an item that you stole.
yes_give_text = "Please give it back...."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep it, I only just want the items you promised...."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "Oh.... thanks, but I'd rather have the items you originally promised...."