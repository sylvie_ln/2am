script = @"Bun bun bun....
We are known as the bunneys who granted Eivlys the magic of zone creation.
This magic can create nearly endless labyrinths. But is a labyrinth without a goal or exit so fun to explore?
That's why most people who learn our magic struggle to create interesting worlds. I've heard Eivlys was no exception."