script = @"Boop boop boop....
Most mushroom zones contain just one kind of uncommon shroom that is native to the area. Aside from that, you'll only find worthless shrooms.
Similarly, most of the locked-up zones have just one kind of rare shroom. The unlocked zones don't contain rare shrooms.
But it's rumored that a few zones have multiple kinds of uncommon shrooms, or even multiple kinds of rare shrooms."