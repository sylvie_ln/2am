cutey = true;
script=@"!set flag_password,0
!if global.flag_hacked==1
 Welcome to the Hacker's World.
 Don't tell Sylvie and Hubol about this, but I've got access to some illegal items with my hacking.
 Stuff that was cut from the game, and stuff you were never supposed to see....
!elif 1
 !bag oBunneyEars
 !if flag_bag==1
  !bag oGiftShopShirt
  !if flag_bag==1
   !bag o100TonShoes
   !if flag_bag==1
    Password accepted.... Welcome to the Hacker's World.
    Don't tell Sylvie and Hubol about this, but I've got access to some illegal items with my hacking.
    Stuff that was cut from the game, and stuff you were never supposed to see....
    You can access my trading screen now, even if you aren't wearing the password. Check it out.
    !set global.flag_hacked,1
	!set flag_password,1
   !fi
  !fi
 !fi
 !if flag_password==0
  !if flag_hacker_talk<5
   Incorrect password. Please enter the correct password.
   !set flag_hacker_talk,flag_hacker_talk+1
  !else
   Did you forget the password? Here are the security questions:
   > What's kind of ears did your first pet have?\n> What's written on that funny shirt you used to wear in high school?\n> What's the most uncomfortable pair of boots you've ever owned?
  !fi
 !fi
!fi"
name="Geniux"

trade_add_item(oGenericItemTemplate);
trade_add_item(oVehicleTemplate);
trade_add_item(oIllegalUpgradeChip);
trade_add_item(oStar);
trade_add_item(oSpike);

// PRE TRADE
// When you first open the trade screen.
orig_text = "All items are pay what you want, I hacked my trading interface to accept any items.";
// When you select an item other than the selected one.
finish_text = "You want a different item?"
// When you deselect an item.
dont_text = "You don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hacking in progress..."
// When you select something you traded to an NPC
no_tradebacks_text = "Feel free to trade back for it."
// Prefix before item name
thatsmy_text = ""

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Great, cool."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Great, cool."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = no_text

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Great, cool."

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Have fun with this."
// When you take your item back after the NPC accepts your offer.
back_text = "Are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You can't back out of the trade, but you can trade me back later."
// When the NPC begins taking you items.
thank_text = "I'll be taking this."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks, happy hacking."
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks happy hacking."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hacking is great, stealing ain't."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Don't steal."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thanks."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some stolen items."
// When you pick up an item that you stole.
yes_give_text = "The trade won't end until you give back the stolen items."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You still have some stolen items."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "You still have some stolen items."