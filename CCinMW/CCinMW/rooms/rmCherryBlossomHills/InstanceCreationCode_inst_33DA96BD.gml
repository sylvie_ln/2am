script=@"Meep meep meep....
When you get lost in a dead end, you might find the memory of a past traveller, or maybe an unusual item.
When you discover a memory, your air will refill a little bit, and you will get a tiny boost to your maximum air.
Memorys tend to be closer to the entrance than the unusual-items."