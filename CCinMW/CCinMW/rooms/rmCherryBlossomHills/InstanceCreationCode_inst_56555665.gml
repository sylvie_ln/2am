script=@"
!if global.flag_legendary_tree==0
 My friend sent a letter to say she wants to meet me under the Legendary Tree.
 But when I came, she was nowhere to be found. What is this mystery?
 !bag oSparkleShroom
 !if flag_bag==1
  Hey, that is the SparkleShroom in your bag. Try to stand under the Legendary Tree with this....
 !elif 1
  It is said something will happen if you come to the Legendary Tree with the golden SparkleShroom. Will you bring it?
 !fi
!elif global.flag_legendary_tree==1
 !set cutscene,1
 !set oChar.npc,id
 !set oChar.image_xscale,-1
 Sunny!!!
 !set global.flag_legendary_tree,2
 !move -36,0,1/3
 Wahh!!! I was so worried....
 !speaker Sunny
 ..K-Karie....
 !speaker Karie
 You're always on time and early!! I knew it had to be a mystery when you weren't here!!
 I was so scared that you disappeared into the unknown world....
 You're my best friend.... What would I do if you were gone!!!
 !speaker Sunny
 ...Y-yeah....
 !move 0,0,2
 !speaker Karie
 Sunny, what was the letter for? Why did you want to meet me under the strange Legendary Tree that is said to have mysterious romantic powers?
 !speaker Sunny
 Oh....
 Well.... 
 !move 0,0,2
 I just wanted to say....
 !move 0,0,10
 You're my best friend too, Karie. And I don't know what I'd do without you either.
 !speaker Karie
 Oh!! 
 Yeah!! 
 Let's be best friends until the time ends.
 When I'm with you, I always feel like smiling!
 !speaker Sunny
 Me too....
 !move 0,0,2
 Hey, Karie, I think that person is the one who helped me come back to this world...
 !speaker Karie
 Oh!!!!!!!! 
 !move 36,0,1/3
 Thank you so much!!!!!!!
 !speaker Sunny
 I think I should give it to her.
 !speaker Karie
 !if global.difficulty==0
  Give what? 
  Oh yeah, that object... 
  Well, what else are we going to do with it?
 !elif global.difficulty==1
  No way! This world is so tricky. Make her trade for it.
 !elif global.difficulty==2
  No way! This world is very difficult. Make her trade a big amount for it!
 !fi
 !speaker Sunny
 We found an object that seems wonderful. I think you should have it because of your kindness.
 !if global.difficulty==0
  Thank you for everything you said to me. 
  Please take this with you.
 !elif 1
  But, well, Karie said I should trade it to you instead....
  Thank you for everything you said to me. Please trade for this lovely item.
 !fi
 !throw oHandOfLeafs,30,4
 !speaker Karie
 !set cutscene,0
!elif 1
 You saved my best friend Sunny from a strange mystery. Thank you from the bottom of my heart.
!fi"
name="Karie"

trade_add_item(oLetter);

// PRE TRADE
// When you first open the trade screen.
orig_text = "I didn't bring anything with me except the letter, so we can't really trade....";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Yeah..."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back."
// Prefix before item name
thatsmy_text = "It's a "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Huh??"
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Why are you trying to trade me for this? What a mystery?"
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text