script = "Hey! I'm just a little thundercloud. Get out of here!"
trade_add_item(oHandOfStorms);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Trade with me, if you dare. Trade with me or you're in for a scare."
// When you select an item other than the selected one.
finish_text = "Guh?! You want something else?!"
// When you deselect an item.
dont_text = "Gah, don't mess with me."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "It's mine now, hee hee."
// Prefix before item name
thatsmy_text = "That's my "
// NPC bag is empty after evaluating offer

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't care. Do you have anything that Looks Like Me? Like a stone statue of my forme...."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Hey, I don't mind that rock. It looks just like me."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = no_text
// When you offer more than one of the exact item.
multiple_exact_text = "Just want one of 'em."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Don't need that other junk you're offering!"

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Not trading this!!"
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Get out with that trash."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Heeyaaaah! Let's trade!"
// When you take your item back after the NPC accepts your offer.
back_text = "Backing out??!"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Huh?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Can't take it back now, it's mine hahaha."
// When the NPC begins taking you items.
thank_text = "I'll be taking this, hehe."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks. Smell you later."
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks I guess. I hope I don't smell you later ever again."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "What. What are you doing"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Thiefs never prosper."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Creep."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "Give back all items."
// When you pick up an item that you stole.
yes_give_text = "Yes, give it."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "No way, you can keep that, give me what you said you'd give me."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No way, I don't want that, give me what you said you'd give me."