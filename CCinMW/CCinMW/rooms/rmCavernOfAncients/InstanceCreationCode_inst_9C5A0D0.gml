script = @"An ancient inscription is on the statue.
Sunset comes and the kitten's in motion. Out of the bay and into the ocean.
!if global.flag_switch_4==0
 A switch is below the inscription, pointing to the right. Will you flip the switch? \ask{Yes}{No}
 !if answer==1
  !set global.flag_switch_4,1
  Now the switch is pointing to the left.
 !fi
!else
 A switch is below the inscription, pointing to the left. Will you flip the switch? \ask{Yes}{No}
 !if answer==1
  !set global.flag_switch_4,0
  Now the switch is pointing to the right.
 !fi
!fi"