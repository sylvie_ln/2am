script=@"The Cavern of Ancients. Some say it is a shrine to the villanous Grand Witch. Others simply say that it is a ruins. Who is right? Will you solve the mystery?
I have some hints for the quest you are on. Do you want to learn of them? \ask{Yes}{No}
!if answer==1
 This is an interesting area. There are some fun and silly things to be found, even though it's a historical site.
 I recommend checking out the Mushroom Zone to the left. You can find WideShrooms there, which are a useful item in many ways.
 There are two other Mushroom Zones where rare shrooms can be found. One is rather hidden, and one is in plain sight, but both are locked up. You can buy keys to unlock them in the Citey Center.
 But you might find it hard to get very far in these zones. I suggest visiting the Utility Shop in the Citey Center to get some useful items for exploration.
 You might also want to visit the Laboratorey in the Cherry Blossom Hills. They have inventions that help with exploration, although the materials are hard to find.
 That's all I have to say. I have some more specific hints, but you'll need to pay for them.
!fi"
name="Cavey"

trade_add_item(oHint,"Where's the PoisonShroom?","This hint costs 1 SM.",
trade_standard_costs(),1);
trade_add_item(oHint,"Winning the Race?","This hint costs 2 SM.",
trade_standard_costs(),2);
trade_add_item(oHint,"The Secret Tomb?","This hint costs 3 SM.",
trade_standard_costs(),3);
trade_add_item(oHint,"A Rare Shop?","This hint costs 4 SM.",
trade_standard_costs(),4);
trade_add_item(oHint,"Gambler's Heart?","This hint costs 5 SM.",
trade_standard_costs(),5);

// PRE TRADE
// When you first open the trade screen.
orig_text = "These hints might be a little helpful.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different kind of hint?"
// When you deselect an item.
dont_text = "Oh, you don't want this hint?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I can't trade it back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Please use standard SM currency to purchase hints."
// When you offer a low-rank item.
low_text = "Thanks, a basic item."
// When you offer a mid-rank item.
medium_text = "Thanks, a slightly large item."
// When you offer a high-rank item.
high_text = "Thanks, a tiny little item."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "You're overpaying a little, take some of the stuff out."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "That's not enough mony."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "You need a little more mony."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Please use standard SM currency to purchase hints."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Please only use standard SM currency to purchase hints."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No, that's not for trade."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Sorry, I don't accept that offer."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "Please use standard SM currency to purchase hints."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Cool! I hope you find the hint helpful on your quest."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Huh, expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Oh, you can't back out of the trade now...."
// When the NPC begins taking you items.
thank_text = "I'll take these items now."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks, see you later!"
// When a trade ends and you attempted to steal.
complete_steal_text = "So long."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Please don't steal."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "How dishonest."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thanks."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, give that one back.."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can have it, but give back everything you put up for trade."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that, just the stuff you put up for trade originally."