script=@"You've done well to reach my secret hell. 
A different rare mushroom you might seek is sold here each day of the Week."
name="Mococo"



var day;
switch(current_weekday) {
	case 0: day = oSpringShroom; break;
	case 1: day = oPoisonShroom; break;
	case 2: day = oKittyShroom; break;
	case 3: day = oRainbowShroom; break;
	case 4: day = oSparkleShroom; break;
	case 5: day = oFloatShroom; break;
	case 6: day = oSpeedShroom; break;
}

trade_recet(name);

repeat(3) {
	trade_add_item(day);	
}

/*
Springy Sunday
Poison Monday
Kitty Tuesday 
Rainbow Wednesday
Sparkly Thursday
Floating Friday
Speedy Saturday
*/
// PRE TRADE
// When you first open the trade screen.
orig_text = "Rare items abound in the shop you've found.";
// When you select an item other than the selected one.
finish_text = "These items are the same, but I'll play your game."
// When you deselect an item.
dont_text = "Don't want this item? I won't fight 'em."
// When the NPC evaluates your offer
hmm_text = "Let me see what you've shown to me."
// When you select something you traded to an NPC
no_tradebacks_text = "Won't trade it back, please don't attack."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't want it, looks haunted."
// When you offer a low-rank item.
low_text = "A basic shroom in the room."
// When you offer a mid-rank item.
medium_text = "Interesting choice, a shroom with voice."
// When you offer a high-rank item.
high_text = "Now that shroom's small, great for all."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Too much stuff, more than enough."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Don't be cheap, pile on the heap."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "A little more will settle the score."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Take out the trash, don't get a rash."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Your offer's a mess, I must confess."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "That's exactly what I've been looking for!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = no_text
// When you offer more than one of the exact item.
multiple_exact_text = "I just want one of those!"
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Don't need anything extra, just give me that one item!"

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "This I can't trade, put away your blade."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Your offer's bad, but don't be sad."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "A trade so great, it can't wait."
// When you take your item back after the NPC accepts your offer.
back_text = "Backing out, not very stout."
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer? Got more in the coffer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late, this trade is great."
// When the NPC begins taking you items.
thank_text = "I'll take it, won't fake it."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks for the trade, see you again in this glade."
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks for the trade, see you again in this glade.... I guess."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Please don't steal, or I'll squeal."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "You're a thief, turn over a leaf."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "You gave it back, but you're a hack."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "Thanks a lot, but there's more you ought."
// When you pick up an item that you stole.
yes_give_text = "Give that back, or I'll give you a whack."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "That you can keep, but I hope you can't sleep."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "Don't want that, you silly hat."