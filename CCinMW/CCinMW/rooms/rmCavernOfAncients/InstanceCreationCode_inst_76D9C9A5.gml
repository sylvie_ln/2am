script=@"
!if global.flag_racerunner==1
 I have retired from Racing. You are the Racing Queen today.
!elif 1
 !if flag_racing==0
  The Race Runner has never been beaten in a race. So, you would like to challenge the racing? \ask{Yes}{No}
  !if answer==1 
    I will outrun you easily. 
    !user 3
  !elif 1
    A wise decision.
  !fi
 !elif flag_racing==2
   !if race_winner=="+string(oChar.id)+@"
    !if global.difficulty==0
     Unbelievable, your the racing queen?! Take my precious item.
     !user 4
	!elif 1
     Unbelievable, your the racing queen?! I will let you trade for my precious item.
	!fi
    !throw oHandOfThunder,45,4
    !set global.flag_racerunner,1
   !elif 1
    Of course I have won. See you next time.
    !user 4
   !fi
 !fi
!fi"
name="Slash"


if global.flag_racerunner == 0 {
	trade_add_item(oHandOfThunder,"One of the legendary 12 Hands of the Clock. You must be lightning-fast to win it.");
} else if global.difficulty == 1 {
	trade_add_item(oHandOfThunder,"Hand of Thunder","One of the legendary 12 Hands of the Clock. Trade me 1x SpeedShroom for it.",
	trade_cost(oSpeedShroom,2),2);
} else if global.difficulty == 2 {
	trade_add_item(oHandOfThunder,"Hand of Thunder","One of the legendary 12 Hands of the Clock. Trade me 2x SpeedShroom for it.",
	trade_cost(oSpeedShroom,2),4);
}
trade_add_lootbox(oSpeedShroom,2,5); // 3:16

refuse_text = "No...."
no_text = "No...."
worthless_text = "You have to show your racing skills to earn it."
dont_text = "You are not fast enough?"
thatsmy_text = "That's the "


// PRE TRADE

orig_alt_text = "Racing queen, spare me a bean."
if global.flag_racerunner == 0 {
	orig_text = "I have a great prize for fast-racing.";
} else {
	orig_text = orig_alt_text;
}
// For alt text, add a hook in oInTheBag as well.

// When you select an item other than the selectd one.
finish_text = "You want a different item?"
// When you deselect an item.
dont_text = "Hm."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I'm keeping this."
// Prefix before item name
thatsmy_text = "That's my "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't want that."
// When you offer a mid-rank item.
medium_text = "That item seems speedy."
// When you offer exactly what they want.
too_many_text = "Even I can't handle that much speed. Take some out."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I've got a need for more speed." 
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I've got a need for more speed." 
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "There's nothing I need but speed. Take out the other stuff."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "There's nothing I need but speed. Take out the other stuff."


// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No waaay, I won't trade this!"
// All items are garbage 
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Hey, that's not what I want."
// Special worthless_text for the first item added to NPC's bag.
if global.flag_racerunner == 0 {
	first_worthless_text = "Heh, this is not for trade, only the fast can have it."
} else {
	first_worthless_text = worthless_text
}
// Special worthless text used if you offer an item without selecting anything"
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Let's do this trade quickly."
// When you take your item back after the NPC accepts your offer.
back_text = "Backing out? You're slowing things down."
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer? That's not very fast of you."
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Too late, we're past the finish line."
// When the NPC begins taking you items.
thank_text = "I'll take your stuff with great speed."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks, this is great."
// When a trade ends and you attempted to steal.
complete_steal_text = "You were a fool to challenge me with this theft."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, not so fast."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Impressive speed, but you should not steal from me."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you."
//// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "Return the items you promised quickly."
// When you pick up an item that you stole.
yes_give_text = "That, put that back to me."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep it. But quickly give back the promised items you took."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I'm not interested in that, only what you originally offered."
