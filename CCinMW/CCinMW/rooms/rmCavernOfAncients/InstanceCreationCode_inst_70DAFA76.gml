script = @"An ancient inscription is on the statue.
Morning sun and the kitten's awake. Run to the river that's just by the lake.
!if global.flag_switch_2==0
 A switch is below the inscription, pointing to the right. Will you flip the switch? \ask{Yes}{No}
 !if answer==1
  !set global.flag_switch_2,1
  Now the switch is pointing to the left.
 !fi
!else
 A switch is below the inscription, pointing to the left. Will you flip the switch? \ask{Yes}{No}
 !if answer==1
  !set global.flag_switch_2,0
  Now the switch is pointing to the right.
 !fi
!fi"