script=@"My hobby is creating cars. Lately, I've been working on wizard style cars.
!if global.flag_wizardmobile==0
 However, what I can't seem to create is a car that can swim in the blue ocean.
 If you come into possession of such a car, I'd love to trade for it.
!elif 1
 You gave me an amazing car that swims like a fish. I hope this inspires me to create new kinds of cars in the future.
!fi"
name="Wizaron"

trade_add_item(oWizardmobile);
trade_add_item(oWizardmobile);
trade_add_item(oWizardmobile);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Welcome to the land of wizards.";
// When you select an item other than the selected one.
finish_text = "You want a different car? To be honest, they're all identical."
// When you deselect an item.
dont_text = "Oh, you don't want this amazing vehicle?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't really want to trade it back."
// Prefix before item name
thatsmy_text = "Look at this "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "That's not what I'm looking for in this trade as a wizard."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Hey, that looks so much like a fish that can swim in the ocean."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "That's simply not a fish like car."
// When you offer more than one of the exact item.
multiple_exact_text = "Don't give me so much fish."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Wizard doesn't want anything except the fish."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No matter what you offer, I'm not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "A wizard has no need for such trash."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Amazing, so there was a fish car after all. Please trade it to me."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Too late to back out now, wizard."
// When the NPC begins taking you items.
thank_text = "I'll value this item."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks so much!"
// When a trade ends and you attempted to steal.
complete_steal_text = "You attempted to steal. You can no longer become a wizard now."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, that's not a magic spell I've ever seen."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Stealing is against the wizard code..."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Okay...."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised, fiend."
// When you pick up an item that you stole.
yes_give_text = "Please give back that item you promised to me."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep it. But you should give back the items you promised to me."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No, that's not one of the items you originally put up for trade...."