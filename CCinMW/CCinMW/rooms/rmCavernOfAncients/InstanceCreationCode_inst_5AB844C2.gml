script=@"Some archeologists believe the columns of the Cavern represent the limbs of the Grand Witch Eivlys, while the kittys represent her loyal familiars....
Who's Eivlys you ask, heh, you don't wanna know. All I can say is that I am not one of her Followers...."
name="Archaea"

trade_add_item(oBlueShard);
trade_add_lootbox(oSpringShroom,2,4); // 2:15
trade_add_lootbox(oSpringShroom,2,4); // 2:15
// PRE TRADE
// When you first open the trade screen.
orig_text = "I am only an amateur archeologist, what do you need from me?";
// When you select an item other than the selected one.
finish_text = "Oh?"
// When you deselect an item.
dont_text = "Hm."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back."
// Prefix before item name
thatsmy_text = "This is a "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "For this, I'd like a different item-type."
// When you offer a low-rank item.
low_text = "A precious artefact?! You must give it to me...."
// When you offer a mid-rank item.
medium_text = "That item is so springy and blue."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't need all of these items."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need more items than that."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Maybe a little more items than this."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Only some of this looks interesting to me. Take out the uninteresting things."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Only some of this looks interesting to me. Take out the uninteresting things."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I won't trade this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "I reject this offer."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "This doesn't look interesting as an artefact."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Please, trade me this."
// When you take your item back after the NPC accepts your offer.
back_text = "Aah, you're backing out."
// When you offer more stuff after the NPC accepts your offer.
more_text = "Hm, expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late to back out of this trade."
// When the NPC begins taking you items.
thank_text = "Thank you."
// When a trade ends and you didn't attempt to steal.
complete_text = "Now I must get back to my archeological research."
// When a trade ends and you attempted to steal.
complete_steal_text = "I spit on you, thief."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Don't you steal this."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "How tragic."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Good, you returned everything."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised...."
// When you pick up an item that you stole.
yes_give_text = "Yes, give back that one."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep what I gave you. Give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No, give me the items you promised originally."