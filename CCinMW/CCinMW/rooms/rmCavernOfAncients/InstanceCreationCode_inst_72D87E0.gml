script = @"An ancient inscription is on the statue.
And it's just like the ocean, under the moon. It's the same as the emotion that I get from you.
!if global.flag_switch_3==0
 A switch is below the inscription, pointing downwards. Will you flip the switch? \ask{Yes}{No}
 !if answer==1
  !set global.flag_switch_3,1
  Now the switch is pointing upwards.
 !fi
!else
 A switch is below the inscription, pointing upwards. Will you flip the switch? \ask{Yes}{No}
 !if answer==1
  !set global.flag_switch_3,0
  Now the switch is pointing downwards.
 !fi
!fi"