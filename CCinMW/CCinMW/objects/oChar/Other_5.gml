event_inherited();
ds_list_sylvie_destroy(npcs);
game_set_speed(global.default_room_speed,gamespeed_fps);
if room == rmCoolMuseum and !global.return_to_title and !restarting {
	with oMuseumControl {
		event_user(1);
	}
}
with oShifter {
	event_user(1);
}

var whitelist = [
	rmCylveyCitey,
	rmCylveyClockTower,
	rmTowerTop,
	rmCherryBlossomHills,
	rmCanyonCliffs,
	rmCavernOfAncients,
	rmCoolMuseum,
	rmMushZone,
	rmTitle,
];
var ws = array_length_1d(whitelist);
var next_on_wl = false;
for(var i=0; i < ws; i++) {
	if global.current_room == room_get_name(whitelist[i]) {
		next_on_wl = true;
		break;
	}
}
var this_on_wl = false;
for(var i=0; i<ws; i++) {
	if room == whitelist[i] {
		this_on_wl = true;
		break;
	}
}
var allowed = (global.entering_door and next_on_wl and this_on_wl) or (this_on_wl and global.return_to_title);
if allowed {
	event_perform(ev_alarm,7);
}

if global.return_to_title {
	event_perform_object(oInit,ev_other,ev_user12);
}
global.return_to_title = false;
global.entering_door = false;