var pxs = image_xscale;
var pib = image_blend;
if poison > 0 {
	image_xscale = sign(pxs)*poison_xs;
	image_blend = merge_color(c_purple,c_white,poison_color);
	poison_xs += (1 - poison_xs)*0.1;
	poison_color += (1-poison_color)*0.1;
	poison--;	
	if poison <= 0 {
		poison = 0;
		image_xscale = pxs;
		image_blend = pib;
	}
}
var db = (dead and bunney); 
var dbys = (db ? -1 : 1);
if bag_has_item(oBunneyEars) {
	draw_sprite_ext(sBunneyEars,0,x,db ? bbox_bottom+4 :bbox_top-4,
	image_xscale,dbys*image_yscale,image_angle,image_blend,image_alpha);
}
if bag_has_item(oMiniBunney) {
	draw_sprite_ext(sMiniBunney,0,x,db ? bbox_bottom+8 : bbox_top-8,
	image_xscale,dbys*image_yscale,image_angle,image_blend,image_alpha);
}
if bag_has_item(oChickenWings) {
	draw_sprite_ext(sCharWings,image_index,x,y,
	image_xscale,dbys*image_yscale,image_angle,image_blend,image_alpha);
}

draw_self();
image_xscale = pxs;
image_blend = pib;

var yp = y;
if room == rmMushZone and (global.assist_air_drain != 0) and !dead {
	var air_text = "AIR "+string(round(air/max_air*100));
	var xp = x-((string_width(air_text)-string_width(" ")) div 2);
	yp += 9
	//yp -= 9+8
	draw_set_font(global.neco_font);
	draw_set_color(c_black);
	draw_text(xp+1,yp,air_text);	
	draw_text(xp-1,yp,air_text);	
	draw_text(xp,yp-1,air_text);	
	draw_text(xp,yp+1,air_text);	
	draw_text_color(xp,yp,air_text,c_white,c_white,make_color_rgb(0xcb,0xdb,0xfc),make_color_rgb(0xcb,0xdb,0xfc),1);	
}

// Debug drawin'
if false {
	var b_text = string(x)+","+string(y);
	var xp = x-((string_width(b_text)-string_width(" ")) div 2);
	yp += 9;
	draw_set_font(global.neco_font);
	draw_set_color(c_black);
	draw_text(xp+1,yp,b_text);	
	draw_text(xp-1,yp,b_text);	
	draw_text(xp,yp-1,b_text);	
	draw_text(xp,yp+1,b_text);	
	draw_text_color(xp,yp,b_text,c_white,c_white,make_color_hsl_240(210,239,200),make_color_hsl_240(210,239,200),1);	
}

if instance_exists(npc) and !conversing and !riding
and !npc_ignore(npc)
and !(npc.object_index == oPuzzlePiece and !npc.visible) 
and !paused {
	
	play_npc_option_threshold_sound();
	
	var b = 4;
	var cl = camera_get_view_x(view_camera[0]);
	var ct = camera_get_view_y(view_camera[0]);
	var cr = cl+camera_get_view_width(view_camera[0]);
	var cb = ct+camera_get_view_height(view_camera[0])-20;
	
	draw_set_font(global.sylvie_font_bold);
	var xp = npc.x;
	var yp = npc.y-16-(npc.sprite_height/2)
	if room == rmMushZone {
		yp -= 9;	
	}
	if yp-24 < ct {
		yp = npc.bbox_bottom+16+b;
	}
	var t1 = chr(ord("~")+edir.up)+" "+npc.act1;
	var t2 = chr(ord("~")+edir.down)+" "+npc.act2;
	var w = max(string_width(t1),string_width(t2));
	xp = clamp(xp,cl+b+(w div 2),cr-b-(w div 2));
	draw_set_color(c_black);
	draw_text_centered(xp+1,yp-16,t1);
	draw_text_centered(xp+1,yp,t2);
	draw_text_centered(xp-1,yp-16,t1);
	draw_text_centered(xp-1,yp,t2);
	draw_text_centered(xp,yp+1-16,t1);
	draw_text_centered(xp,yp+1,t2);
	draw_text_centered(xp,yp-1-16,t1);
	draw_text_centered(xp,yp-1,t2);
	draw_set_color(c_white);
	draw_text_centered(xp,yp-16,t1);
	draw_text_centered(xp,yp,t2);
}