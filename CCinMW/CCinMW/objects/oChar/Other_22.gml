///@description Update jump & speed values

var gspeed = round(30 * (global.assist_gamespeed == 0 ? 1/10 : global.assist_gamespeed));
if game_get_speed(gamespeed_fps) != gspeed {
	game_set_speed(gspeed, gamespeed_fps);
}
var jump_power = (global.assist_jump_power > 1 ? power(global.assist_jump_power,global.assist_jump_power) : global.assist_jump_power);
var grav_power = (global.assist_gravity > 1 ? sqr(global.assist_gravity) : global.assist_gravity);
grav = (base_grav * grav_power);
var gravy = grav == 0 ? base_grav : grav;
bounce = sqrt(2*gravy*4);
climber_bounce = sqrt(2*gravy*6);
jmp = sqrt(2*gravy*((33+(bag_item_count(oSpringShroom)*4))*jump_power));
kjmp = sqrt(2*gravy*(8*jump_power));
poison_jmp = sqrt(2*gravy*(4*jump_power));
if heavy { gravy *= 2; grav = (grav_power == 0 ? 0 : gravy); jmp = sqrt(2*gravy*((17+(bag_item_count(oSpringShroom)*2))*jump_power)); }

walkoff_time = (global.default_room_speed div 10)+((global.default_room_speed div 15)*bag_item_count(oFloatShroom));

spd = ((3.33+((1/6)*bag_item_count(oSpeedShroom)))*global.assist_speed_power);
acc = spd/8;
fric = acc;
spd += fric;
acc += fric;

switch(global.assist_air_bonus) {
	case 0: air_multiplier = 0; break;
	case 0.5: air_multiplier = 9; break;
	case 1: air_multiplier = 27; break;
	case 1.5: air_multiplier = 45; break;
	case 2: air_multiplier = 90; break;
}
