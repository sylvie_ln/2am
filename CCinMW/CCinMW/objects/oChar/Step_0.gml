event_user(12);
var walkoff_time = self.walkoff_time;
if hover { walkoff_time *= hover_multiplier; }
var acc = self.acc;
if hover { acc /= lerp(2,1,min(spd,abs(hv))/spd); }
vv_prev = vv;
var grav = self.grav;
var jmp = self.jmp;

paused =
(instance_exists(oShifter) and oShifter.shifting) or
(instance_exists(oInTheBag)) or 
is_pause_menu_open();

if dead and outside_view() {
	alarm[aChar.force_respawn] = max(alarm[aChar.force_respawn],respawn_time);	
}

if paused or (global.closed_bag and alarm[5] >= 0){
	exit;
}


var at_door = place_meeting(x,y,oDoor);
if !at_door {
	var memory = instance_place(x,y,oMemory);
	if memory != noone {
		if memory.sprite_index == sMemoryRevived {
			at_door = true;	
		}
	}
}
if room == rmMushZone and at_door {
	last_door_x = oRoomGen.xp;
	last_door_y = oRoomGen.yp;
}

conversing = instance_exists(npc) and (npc.talking or npc.alarm[10] > 0 or npc.cutscene);

if !dead {
	if input_pressed("Shroom") {
		if !conversing and !riding and instance_exists(oEivlysCurse) {
			npc = oEivlysCurse.id;
			if !conversing {
				with npc {
					event_user(1);	
				}
			}
		} else if !conversing and !riding and !global.closed_bag and room != rmLegendaryTree and !global.flag_finale {
			var bag = instance_create_depth(0,0,depth,oInTheBag);
			bag.mode = 1;
			bag.holding = false;
			bag.original = false;
			with bag { event_user(0); }
			exit;
		}
	}

	if fall_through > 0 {
		fall_through--;	
	}
	if npc_buffer > 0 {
		npc_buffer--;	
		if npc_buffer == 0 {
			npc_buffer_from_press = false;
		}
	}
	if input_pressed("Down") and !instance_exists(npc) {
		if fall_through == 0 {
			fall_through = room_speed div 10;
			if ong { vv = max(vv,0.5); }
		}
		npc_buffer = npc_buffer_max;
		npc_buffer_from_press = true;
		if ong {
			walkoff_timer = walkoff_time;
		}
	}
}

ong = collision_at(x,y+1) and (springey_springed or vv >= 0);
conversing = instance_exists(npc) and (npc.talking or npc.alarm[10] > 0 or npc.cutscene);

var kittey_count = bag_item_count(oKittyShroom);
if ong {
	consecutive_bounce_count = 0;
	if !springey_springed {
		walkoff_timer = 0;
	}
	jumped = false;
	kittey_jumps = kittey_count
	kittey_max = kittey_jumps;
}
if !ong and vv > 0 {
	springey_springed = false;
}

if kittey_count > kittey_max {
	kittey_jumps += kittey_count - kittey_max;
	kittey_max = kittey_count;
}
if kittey_jumps > kittey_count {
	kittey_jumps = kittey_count;	
}

if !conversing {
	var ladder = instance_place(x,y,oLadder);
	var snakey = false;
	if ladder == noone {
		ladder = instance_place(x,y,oSnakeRope);	
		if instance_exists(ladder) {
			if ladder.mask_index == sSnakeHitbox {
				snakey = true and room == rmMushZone;
			} else {
				ladder = noone;	
			}
		}
	}
	if dead or ladder == noone or (!snakey and !ladder.upblocker) {
		if !ong {
			if walkoff_timer >= walkoff_time {
				vv += grav;
			}
			walkoff_timer++;
		}
	} else if snakey or ladder.upblocker {
		if input_held("Up") {
			vv -= grav * (snakey ? 2 : 1);
		} 
		if input_held("Down") and !ong {
			vv += grav * (snakey ? 2 : 1);	
		}
	}	
}

/*
if keyboard_check_pressed(ord("R")) {
	event_perform(ev_alarm,aChar.respawn);
	exit;
}
*/

if dead {
	var prefix = bunney ? "sBunney" : "sChar";
	if dead_stage == -1 {
		dead_stage++;	
	}
	if dead_stage == 0 {
		sprite_index = asset_get_index(prefix+"Die");
	} 
	if dead_stage == 1 {
		mask_index = asset_get_index(prefix+"DieMask");
		if ong {
			dead_stage++;	
		} else {
			sprite_index = asset_get_index(prefix+"DieFall");
		}
	} 
	if dead_stage == 2 {
		sprite_index = asset_get_index(prefix+"DieLand");
		if !ong {
			dead_stage = 1;	
			image_speed = 1;
			alarm[aChar.respawn] = -1;
		}
	}
	event_user(eChar.apply_friction);
	event_user(eChar.do_movement);
	exit;
}

var npc_types = [oShroom,oNPC];
var filter_shroom = input_held_time("Shroom") > global.default_room_speed;
if filter_shroom { npc_types = [oNPC]; }
var nti = 0;
repeat(array_length_1d(npc_types)) {
	ds_list_clear(npcs);
	var num = instance_place_list(x,y
	+(npc_buffer > 0 and npc_buffer != npc_buffer_max and npc_buffer_from_press ? 
	2-max(2,npc_buffer_max-npc_buffer) : 0)
	,npc_types[nti],npcs,true);
	if filter_shroom {
		var filtered = ds_list_create();
		for(var i=0; i<num; i++) {
			if !object_is_ancestor(npcs[|i].object_index,oShroom) {
				ds_list_add(filtered,npcs[|i]);
			}
		}
		ds_list_copy(npcs,filtered);
		ds_list_destroy(filtered);
		num = ds_list_size(npcs);
	}
	if !conversing and !riding {
		if num == 0 and npc_buffer <= 0 { 
			npc = noone; 
		}
		else if num == 1 { 
			if !npc_ignore(npcs[|0]) {
				npc = npcs[|0]; 
			} else if npc_buffer <= 0 {
				npc = noone;	
			}
		}
		else {
			if npc != noone and instance_exists(npc) {
				var dist = round(point_distance(x,y,npc.x,npc.y));
				if !npc.visible { dist = 1024; }
			} else {
				var dist = 1024;	
			}
			var nothing_found = true;
			for(var i=num-1; i>=0; i--) {
				if npc_ignore(npcs[|i]) { 
					continue;
				}
				nothing_found = false;
				var nn = npcs[|i];
				var pdist = round(point_distance(x,y,nn.x,nn.y));
				if !nn.visible { pdist = 1024; }
				if pdist < dist {
					npc = nn;
				}
			}
			if nothing_found and npc_buffer <= 0  { 
				npc = noone; 
			}
		}
	}
	if npc != noone { break; }
	nti++;
}
var done = false;
if alarm[4] < 0 {
	with npc {
		if place_meeting(x,y,oChar) {
			other.npc_buffer = other.npc_buffer_max;
		}
		if cutscene and !talking { continue; }
		if alarm[10] > 0 { continue; }
		if (!talking and input_pressed("Up"))
		or (talking and input_pressed(oInput.act1)) {
			event_user(1);	
			if !talking { 
				done = true;	
			}
		} 
		if !talking and input_pressed("Down") {
			event_user(2);	
		}
	}
}

conversing = instance_exists(npc) and (npc.talking or npc.alarm[10] > 0 or npc.cutscene);
if conversing or done {
	exit;
}


var running = input_held("Left") or input_held("Right")
var rate = max(1,(global.default_room_speed div 5)-bag_item_count(oSpeedShroom));
if bag_has_item(oSpeedShroom) and running and speed_tick mod rate == 0 {	
	var xpos = hv > 0 ? sylvie_random(bbox_left-1,bbox_left+1) : sylvie_random(bbox_right-1,bbox_right+1);
	var ypos = sylvie_random(bbox_bottom-2,bbox_bottom);
	if ong {
		var sparkle = instance_create_depth(xpos,ypos,depth+1,oSparkle);
		sparkle.time = (global.default_room_speed div 4);
		sparkle.color = merge_color(c_blue,c_white,0.25);
		sparkle.image_index = irandom_range(1,sparkle.image_number-2);
		sparkle.hv = -hv/2;
		sparkle.vv = sylvie_random(-3,-1);
		sparkle.grav = 0.25;
	}
}
if running { 
	speed_tick++; 
} else {
	speed_tick = 0;	
}

if bag_has_item(oPoisonShroom) {
	poison_tick++;
} else {
	poison_tick = 1;	
}
if poison_tick mod (global.default_room_speed div 6) == 0 {
	repeat(bag_item_count(oPoisonShroom))  {
		var xpos = sylvie_random(bbox_left,bbox_right);
		var ypos = sylvie_random(bbox_top,bbox_top+3);
		var sparkle = instance_create_depth(xpos,ypos,depth+1,oSparkle);
		sparkle.time = (global.default_room_speed div 2);
		sparkle.color = c_purple;
		sparkle.image_blend = c_black;
		sparkle.image_index = irandom_range(1,sparkle.image_number-2);
		sparkle.hv = (sparkle.x-x)/(-4);
		sparkle.grav = -0.15;
	}
}

if poison_tick mod ((global.default_room_speed*6)/(clamp(bag_item_count(oPoisonShroom),1,6))) == 0 {
	poison = global.default_room_speed div 2;
	poison_xs = 3;
	poison_color = 0;
	vv -= poison_jmp;
	hv *= poison_mult;
	audio_sound_pitch(sndPoisonCough,sylvie_choose(10,10.5,11,11.5,12)/10);
	play_sound(sndPoisonCough);
}

if bag_has_item(oFloatShroom)  {
	var xpos = sylvie_random(bbox_left,bbox_right);
	var ypos = sylvie_random(bbox_bottom,bbox_bottom+3);
	if !ong and walkoff_timer < walkoff_time {
		var sparkle = instance_create_depth(xpos,ypos,depth+1,oSparkle);
		sparkle.time = (global.default_room_speed div 2);
		sparkle.color = merge_color(c_green,c_lime,0.5);
		sparkle.image_index = irandom_range(1,sparkle.image_number-2);
		sparkle.hv = (sparkle.x-x)/(-4);
		sparkle.grav = 0.1;
	}
}

if bag_has_item(oSparkleShroom) or bag_has_item(oRainbowShroom){
	var xpos = sylvie_random(bbox_left,bbox_right);
	var ypos = sylvie_random(bbox_top,bbox_bottom);
	if (hv != 0 or vv != 0)
	and collision_rectangle(xpos-1,ypos-1,xpos+1,ypos+1,oSparkle,false,true) == noone {
		var sparkle = instance_create_depth(xpos,ypos,depth+1,oSparkle);
		sparkle.time = ((global.default_room_speed div 3)*(bag_item_count(oSparkleShroom)))+((global.default_room_speed div 6)*bag_item_count(oRainbowShroom));
		var intensity = lerp(220,120,clamp((bag_item_count(oRainbowShroom)-1)/5,0,1));
		sparkle.color = 
		bag_has_item(oRainbowShroom) ? sylvie_choose(
										make_color_hsl_240(0,239,intensity),
										make_color_hsl_240(20,239,intensity),
										make_color_hsl_240(40,239,intensity),
										make_color_hsl_240(80,239,intensity),
										make_color_hsl_240(120,239,intensity),
										make_color_hsl_240(150,239,intensity),
										make_color_hsl_240(180,239,intensity),
										make_color_hsl_240(190,239,intensity),
										make_color_hsl_240(210,239,intensity),
										make_color_hsl_240(230,239,intensity))
									 : c_white;
		sparkle.image_index = irandom_range(1,sparkle.image_number-2);
		sparkle.grav = -0.05;
	}
}

if !riding {
	if bag_has_item(oJetpack) {
		if input_held("Jump") {
			if heavy {
				vv -= grav-(grav/4);
			} else {
				if vv > grav { vv -= grav; }
				vv -= grav*2;
			}
			if room == rmMushZone {
				air_base -= global.default_room_speed/6;
			}
		} 
	}
}

var refill_air = at_door or place_meeting(x,y,oEmergencyBubble);

if room == rmMushZone {
	if !refill_air {
		if !riding {
			var drain = global.assist_air_drain;
			if drain == 2 { drain = 3; }
			if drain == 1.5 { drain = 2; }
			air_base -= (1/(power(2,bag_item_count(oOxygenEgg)/3))) * drain;
		}
	} else {
		air_base = max_air;	
	}
	air = air_base + air_reserves;
	if air <= 0 {
		air = 0;
		die(self);
		exit;
	}
}

play_character_air_sound(refill_air);


if input_pressed("Bounce") {
	//global.bouncey = !global.bouncey;	
	//pegasus = !pegasus;
}

var left = input_held("Left");
var right = input_held("Right");
var hdir = right-left;
if left and right {
	hdir = (input_held_time("Left") <= input_held_time("Right")) ? -1 : 1;
}

if !riding or (riding and place_free(x,y)) {
	if !pegasus {
		if abs(hv+acc*hdir) <= spd or hdir != sign(hv) {
			if hover {
				if hdir != sign(hv) and abs(hv) <= spd {
					hv += (acc*(1-clamp(abs(hv)/spd,0,1)))*hdir;
				} else {
					hv += acc*hdir;	
				}
			} else {
				hv += acc*hdir;
			}
		} else {
			if abs(hv) < spd {
				if hdir > 0 {
					hv = spd;	
				} else if hdir < 0 {
					hv = -spd;	
				}
			}
		}
	} else {
		image_speed = 1;
		if pegasus_timer < pegasus_time and (pegasus_timer > 0 or hdir != 0) {
			pegasus_timer++;
			if hdir != 0 and (input_pressed("Left") or input_pressed("Right") or pegasus_timer == global.default_room_speed div 3) {
				pegasus_hdir = hdir;
			}
			if pegasus_timer > global.default_room_speed div 3 and pegasus_hdir == 0 {
				pegasus_timer = 0;	
			}
			image_speed = 2;
			if pegasus_timer == pegasus_time {
				pegasus_hdir = image_xscale;	
			}
		} else {	
			image_speed = 2;
			if hv == 0 or (hv != 0 and (hdir != -pegasus_hdir)) {
				/*if pegasus_hdir == 1 {
					hv = max(hv,(spd+pegasus_boost)*pegasus_hdir);
				} else if pegasus_hdir == -1 {
					hv = min(hv,(spd+pegasus_boost)*pegasus_hdir);
				}*/
				if pegasus_hdir != 0 {
					hv = (spd+pegasus_boost)*pegasus_hdir;
				} else {
					pegasus_timer = 0;	
				}
			} else {
				pegasus_timer = 0;
				pegasus_hdir = 0;
			}
		}
	}

	event_user(eChar.apply_friction);
	var jumped_tf = false;
	if jump_buffer_timer > 0 {
		jump_buffer_timer--;	
	}
	var pressed_jump = input_pressed("Jump");
	if pressed_jump or input_held("Jump") {
		if pressed_jump {
			jump_buffer_timer = jump_buffer_time;
		}
		if ((ong and jump_buffer_timer > 0) 
		    or (pressed_jump and inf_jump) 
			or (!ong and pressed_jump and walkoff_timer < walkoff_time)
			or (wings))  {
			if !(wings and !ong) {
				play_sound(sndCharJump);
			}
			vv = -jmp;	
			jumped = true;
			jumped_tf = true;
			walkoff_timer = walkoff_time;
			jump_buffer_timer = 0;
		}
	}

	if global.closed_bag and input_held("Jump") and (ong or inf_jump or walkoff_timer < walkoff_time) {
		vv = -jmp;
		jumped = true;	
		jumped_tf = true;
		walkoff_timer = walkoff_time;
		jump_buffer_timer = 0;
		global.closed_bag = false;
	}
	
	if jumped and walkoff_timer == walkoff_time and !(wings and !ong) {
		if bag_has_item(oSpringShroom) {	
			var ypos = bbox_bottom;
			var xpos = bbox_left+1;
			for(i=0; i<min(5,bag_item_count(oSpringShroom)); i++) {
				var sparkle = instance_create_depth(xpos,ypos,depth+1,oSparkle);
				sparkle.time = (global.default_room_speed div 5);
				sparkle.color = merge_color(c_aqua,c_blue,0.25);
				sparkle.image_index = irandom_range(1,sparkle.image_number-2);
				sparkle.hv = -abs(sparkle.x-x)/2;
				sparkle.vv = clamp((8-abs(sparkle.x-x))/2,1,4);
				xpos++;
			}
			var xpos = bbox_right;
			for(i=0; i<min(4,bag_item_count(oSpringShroom)); i++) {
				var sparkle = instance_create_depth(xpos,ypos,depth+1,oSparkle);
				sparkle.time = (global.default_room_speed div 5);
				sparkle.color = merge_color(c_aqua,c_blue,0.25);
				sparkle.image_index = irandom_range(1,sparkle.image_number-2);
				sparkle.hv = abs(sparkle.x-x)/2;
				sparkle.vv = clamp((8-abs(sparkle.x-x))/2,1,4);
				xpos--;
			}
		}
	}
	
	if !jumped_tf and pressed_jump and kittey_jumps > 0 {
		audio_sound_pitch(sndKittensJump,1+0.1*clamp(kittey_max-kittey_jumps,0,kittey_max));
		play_sound(sndKittensJump);
		kittey_jumps--;	
		vv = -kjmp;	
		jump_buffer_timer = 0;
		var vvvvvv = 1;
		var it = 200;
		var colors = [
			make_color_hsl_240(0,239,it),
			make_color_hsl_240(20,239,it),
			make_color_hsl_240(40,239,it),
			make_color_hsl_240(80,239,it),
			make_color_hsl_240(160,239,it),
			make_color_hsl_240(190,239,it),
			make_color_hsl_240(220,239,it),
		];
		repeat(7) {
			var xpos = sylvie_random(bbox_left,bbox_right);
			var ypos = bbox_bottom;
			var sparkle = instance_create_depth(xpos,ypos,depth+1,oSparkle);
			sparkle.time = (global.default_room_speed div 2);
			sparkle.color = colors[vvvvvv-1];
			sparkle.image_index = irandom_range(1,sparkle.image_number-2);
			sparkle.vv = (vvvvvv++)/2;
			sparkle.hv = -hv * (vvvvvv)/14;
			sparkle.grav = 0.1;
		}
	}
	
	/*
	if jumped and walkoff_timer == walkoff_time and global.assist_pop_mushrooms {
		with collision_rectangle(bbox_left-2,bbox_bottom,bbox_right+2,bbox_bottom+2,oBasicShroom,false,true) {
			if object_index == oBasicShroom and beside(other,1,-1) {
				instance_destroy();	
				if room != rmCoolMuseum {
					if generated and instance_exists(oRoomGen) {
						oRoomGen.local_shroom_deletion_map[? room_key(oRoomGen.xp,oRoomGen.yp)+pos_key(xt,yt)] = object_index;
					}
					if placed and !instance_exists(oRoomGen) and !always_respawn {
						global.shroom_deletion_map[? get_that_room()+pos_key(xstart,ystart)] = true;
					}
				}
				with instance_create_depth(x,y,oChar.depth-1,oBasicPop) {
					image_index = other.image_index;	
				}
			}
		}	
	}
	*/

	if jumped and input_released("Jump") and vv < 0 {
		vv /= 2;
		jumped = false;
	}
}

event_user(eChar.do_movement);

if !quiet {
	play_character_step_sound();
}

if hdir != 0 {
	image_xscale = hdir;	
}

var spr = "";
var prefix = "sChar";
if bunney {
	prefix = "sBunney";
}
if !ong and walkoff_timer >= walkoff_time and pegasus_timer == 0 {
	if vv <= 0 {
		spr = "J";
	} else {
		spr = "F";	
	}
} else {
	if hdir != 0 
	or pegasus_timer != 0
	or (hover and abs(hv) >= 0.5)
	or (hover and !ong) {
		spr = "R";	
	} else {
		spr = "S";
	}
}
var spr = asset_get_index(prefix+spr);
if sprite_exists(spr) { sprite_index = spr; }

ong_prev = ong;


if is_pause_key_pressed() and !global.closed_bag and room != rmLegendaryTree and !global.flag_finale {
	instance_create_depth(0, 0, -1000, oPauseMenu);
	exit;
}

if !input_held("Jump") {
	global.closed_bag = false;	
}