///@description Door enterer
sylvie_shuffle();
if phase == 0 {
	var bc = 0;
	while true {
		bc++;
		var i = sylvie_random(0,instance_number(oNPC)-1);
			//disabled_show_debug_message("trying door "+string(i));
		with instance_find(oNPC,i) {
			if !sprite_exists(sprite_index) { continue; }
			if !ds_map_exists(global.trade_map,name) { continue; }
			if object_is_ancestor(object_index,oDoor) { continue; }
			if object_is_ancestor(object_index,oShroom) { continue; }
			if object_index == oIntangibleNPC { continue; }
			if object_index == oIntangibleDoor { continue; }
			if object_index == oHandDoor { continue; }
			if object_index == oEivlysCurse { continue; }
			other.x = x;
			other.y = y;
			other.npc = id;
			event_user(2);	
			other.alarm[2] = 3;
			other.phase++;
			exit;
		}
		if bc > instance_number(oNPC)*4 {
			alarm[2] = 3;
			phase++;
			npc = noone;
			exit;	
		}
	}
} else if phase == 1 {
	with npc {
		event_user(2);	
	}
	alarm[2] = 3;
	phase++;
} else {
	while true {
		var i = sylvie_random(0,instance_number(oDoor)-1);
			//disabled_show_debug_message("trying door "+string(i));
		with instance_find(oDoor,i) {
			if !is_undefined(global.lock_flag[? tag]) { continue; }
			if rm == room { continue; }
			if object_index == oIntangibleDoor { continue; }
			if object_index == oHandDoor { continue; }
			if object_index == oTreePortal { continue; }			
			other.x = x;
			other.y = y;
			other.npc = id;
			event_user(2);	
			exit;
		}
	}
}
