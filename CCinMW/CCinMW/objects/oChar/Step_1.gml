global.play_time_usec += delta_time;

pegasus = bag_has_item(oPegasusBoots);
hover = bag_has_item(oHoverHeels);
climber = bag_has_item(oClimbingCleats);
springey = bag_has_item(oSpringSneakers);
heavy = bag_has_item(o100TonShoes);
becames_bunney = bunney;
bunney = bag_has_item(oBunneyAmulet);
quiet = bag_has_item(oBunneySlippers);
wings = bag_has_item(oChickenWings);

if becames_bunney != bunney {
	sprite_index = bunney ? sBunneyS : sCharS;	
}


if !pegasus {
	pegasus_timer = 0;
	pegasus_hdir = 0;	
}

if !springey {
	springey_springed  = false;	
}

var spike = instance_place(x,y,oSpike);
if spike != noone {
	die(spike);
}

air_reserves = ds_map_size(global.memory_dead)*air_multiplier;