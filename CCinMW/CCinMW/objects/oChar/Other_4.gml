if global.door_tag != "" {
	var door = noone;
	with oDoor {
		if tag == global.door_tag {
			door = self;
		}
	}
	if door == noone {
		//show_message("No door with tag "+global.door_tag+" found in "+room_get_name(room));
	} else {
		x = door.x;
		y = door.y;
	}
}
while !collision_at(x,y+1) {
	y++;	
}

bx = (floor(x/global.view_width))*global.view_width;
by = (floor(y/(global.view_height-20)))*(global.view_height-20);
camera_set_view_pos(view_camera,bx,by);
with oCursor { event_user(2); }

if instance_exists(oRoomGen) {
	last_door_x = oRoomGen.xp;
	last_door_y = oRoomGen.yp;	
}
alarm[4] = room_speed div 10;

/*phase = 0;
alarm[2] = 4;