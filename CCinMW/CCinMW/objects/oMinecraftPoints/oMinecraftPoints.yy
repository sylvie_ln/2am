{
    "id": "9e4d1162-91fd-45ff-b11c-5eb9df3c2275",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinecraftPoints",
    "eventList": [
        {
            "id": "565b13c1-6daa-42a7-b755-5e4bc609bd95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9e4d1162-91fd-45ff-b11c-5eb9df3c2275"
        },
        {
            "id": "43c404c5-9138-4cd5-9e38-277411b5211a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e4d1162-91fd-45ff-b11c-5eb9df3c2275"
        },
        {
            "id": "b7a7515d-e6b3-43ad-b1f4-dcfb4438779f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e4d1162-91fd-45ff-b11c-5eb9df3c2275"
        },
        {
            "id": "4db14b4e-3853-4784-86cc-c35a9750be16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "9e4d1162-91fd-45ff-b11c-5eb9df3c2275"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}