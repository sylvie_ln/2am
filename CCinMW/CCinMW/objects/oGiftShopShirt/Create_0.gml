event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A hilarious shirt that says \"Stupid\" with an arrow pointing upwards, you can't stop laughing at it...."

damp = 0.75;
bounce = damp;
fric = 0.95;
grav /= 2;

image_speed = 0;