///@description

// Inherit the parent event
event_inherited();

if room == rmMushZone and !(placed or generated) {
	thrown_timer = thrown_time;
}