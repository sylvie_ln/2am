///@description

// Inherit the parent event

if room == rmMushZone and thrown_timer <= 0  and !(placed or generated) {
	instance_create_depth(x,y,depth,oFlash)
	with instance_create_depth(x,y,depth,oFlarekitten) {
		x = other.x;
		y = other.y;
	}
	play_sound(sndSpawnFlarekitten);
	instance_destroy();
	exit;
}

event_inherited();