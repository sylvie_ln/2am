///@description Init world variables

/// WORLD VARIABLES: (not constant, not options, not contract)
// flags and stuff
show_debug_message("Initializing world variables.");
global.flag_minecart = 0;
global.flag_rainbow = 0;
global.flag_racerunner=0;
global.flag_legendary_tree = 0;
global.flag_treasure_hunter = 0;
global.flag_kittey = 0;
global.flag_gap = 0;
global.flag_cheesey = 0;
global.flag_fishcar = 0;
global.flag_wizardmobile = 0;
global.flag_snowkeety = 0;
global.flag_sylvietruck = 0;
global.flag_house = 0;
global.flag_horse = 0;
global.flag_pinkpaperclip = 0;
global.flag_chicen = 0;
global.flag_puzzler = 0;
global.flag_lies = 0;
global.flag_bridge_canyon = 0;
global.flag_bridge_cherry = 0;
global.flag_ladder_canyon = 0;
global.flag_ladder_cavern = 0;
global.flag_tomb = 0;
global.flag_switch_1 = 0;
global.flag_switch_2 = 1;
global.flag_switch_3 = 0;
global.flag_switch_4 = 1;
global.flag_paperclips = 0;
global.flag_dice = 0;
global.flag_hacked = 0;
global.flag_door_darkness = 0;
global.flag_door_light = 0;
global.flag_door_tears = 0;
global.flag_door_flames = 0;
global.flag_door_earth = 0;
global.flag_door_leafs = 0;
global.flag_door_sky = 0;
global.flag_door_storms = 0;
global.flag_door_dreams = 0;
global.flag_door_lies = 0;
global.flag_door_moon = 0;
global.flag_door_thunder = 0;

global.flag_hand_darkness = 0;
global.flag_hand_light = 0;
global.flag_hand_tears = 0;
global.flag_hand_flames = 0;
global.flag_hand_earth = 0;
global.flag_hand_leafs = 0;
global.flag_hand_sky = 0;
global.flag_hand_storms = 0;
global.flag_hand_dreams = 0;
global.flag_hand_lies = 0;
global.flag_hand_moon = 0;
global.flag_hand_thunder = 0;

// Heist flags
global.heist_count = 0;
global.heist_stage = 0;
global.heist_date = date_create_datetime(1991,5,15,6,6,6);
global.heist_date_string = "";
global.heist_item = object_get_name(noone);
global.heist_ii = 0;

// Position and door and room
global.warp_x = undefined;
global.warp_y = undefined;
global.mushzone_params = -1;
global.door_tag = "";
global.current_room = "rmCylveyCitey";
global.previous_room = "rmCylveyCitey";

// Miscellaney
global.kicks = 0;
global.bag = "SmallBag"
global.radio_position = -1;
global.memory_disks_init = false;
global.disk_num = 0;
global.play_time_usec = 0;

// Short term temporaries, NOT saved
global.closed_bag = false;
global.do_deletion = true;
global.thrown_dice_count = 0;
global.flag_decap = 0;
global.flag_finale = 0;
global.return_to_title = false;
global.entering_door = false;
global.mute = false;

// No longer used in the game!
global.bouncey = true;
global.first_bag = false;