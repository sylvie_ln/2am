///@description Memorey
show_debug_message("Initializing memories.");
var mm = global.memorys;
/*
for(var i=1; i<=300; i++) {
	mm[?"Jucy"+string(i)] = "Jucy jiggler #"+string(i);
}
*/
mm[?"Byclock"] = @"Okay... this clock tower is weird. It's so annoying to get around. 
But at least I've confirmed there really is a Mushroom Zone here. 
Fact checking all my hints has been a pain... When that "+"\"travelling spirit\" arrives, I hope they'll find the hints helpful."+@"
This place gives me the creeps. I'm getting out of here.
* Byclock, the Cylvey Citey's hint giver."
mm[?"Flaire"] = @"That blasted Taco Sack owner is at it again.
"+"\"Oh sorry, we're out of ingredients.\" You're out of ingredients every time I come here!"+@"
Do you even actually source your own ingredients anymore? Or do you make your customers do it?!
"+(global.shroom_shuffle ? 
   "Is this even the right place to find PoisonShrooms? Something feels unusual about this world, like the mushrooms have been shuffled around." : 
   "")+@"
Forget this. I'll go hang around in the Citey and see if someone will trade me a taco.
* Flaire, lover of cheesey tacos."
mm[?"Plunch"]= @"That kitten seller in the hills... she's driving me wild.
She wears this awful purple sack with a cat face on it all the time, yet, she's somehow incredibly attractive.
I wanna ask her out. Even though she doesn't know how to dress herself.
"+(global.shroom_shuffle ? 
  "I'm looking for a rare breed of kittey to impress her, but I have a strange feeling that the world's mushrooms have been shuffled. Will I even find any kitteys here?" : 
  "I'm looking for a rare breed of kittey to impress her, but all I'm finding is your standard shorthairs....")+@"
* Plunch, the humble plushey maker."
mm[?"Stocs"] = @"A world of money, cash and dollars....
That strange person said the form of this hell-dimension reflects your inner desires.
If that's true, this is certainly exactly what I've been looking for. 
They also said something about being eternally cursed. Like I care when I'm so rich with dollars.
If they materialize in front of me again, maybe I'll give them a little tip as thanks. Hah, yeah right!
* Stocs, a demon of money and business."
mm[?"Rainbis"] = @"It's supposed to be true that the RainbowShroom is found in this area, according to local legends.
And yet, I have searched as far as I can in this Mushroom Zone, and did not find one.
"+(global.shroom_shuffle ? 
   "Something seems odd about the whole world. It feels like even the usual mushrooms are shuffled around to different places." :
   "")+@"
I think I am too old for this kind of adventure. I hope someone can bring a RainbowShroom to me.
Before I die, I'd like to see a rainbow just once.
* Rainbis, who wants to see a beautiful sight."
mm[?"Wirte"] = @"There are quite a few of these lovely mushrooms here.
The little green shriveley ones that others call "+"\"Worthless Shrooms\"."+@"
If those are worthless, then I must be worthless as well.
I'm just tired of being hated by others, seeing the things I like berated, and hating myself.
I think these should be called "+"\"Wonderful Shrooms\". And I need to learn how to see myself as wonderful."+@"
* Wirte, who feels at home among the trash."
mm[?"Uuella"] = @"Yes, that's a great extension. Keep it up, Snakey.
When I became an engineer, I never thought I'd end up training snakes to extend like ropes.
But you have to use the best quality materials. These snakes have a special skin that makes them extremely easy to climb.
You know, I have to train the flarekittens as well. And I spend a lot of time blowing bubbles. 
It's like I'm some kind of clown of the circus now....
The princess said someone new might come to this world. If they do, I hope they realize how useful my utilities are.
* Uuella, the utility shop owner."	
mm[?"Lockme"] = @"It's pretty peaceful up here. In the clouds.
It's nice to just sit in a little nook and carve some keys.
Everyone thinks I have a knife because I like stabbing people. While that's true, it's also good for keymaking.
The other day, I even carved a key out of a rose stem. It's too flimsy to use, but I'll see if I can scam someone with it.
I hope other people realize how relaxing it is here, and it becomes a popular vacation spot. 
Then I'll jack up the price of my Cloud Keys and get loaded.
* Lockme, the unusual key shop owner."
mm[?"Uncomus"] = @"Woooooowow!!! Wow!!!!!
Now this is the best place to find the legendary stuff that is not quite common, and not quite rare. 
That's right, I'm talking about the one and only uncommon items!
You've got TallShrooms, WideShrooms, TinyShrooms and WorthlessShrooms all in one zone!!!
Uncommon things, the joy you brings! Let's party!!
* Uncomus, the uncommon mushroom seller."
mm[?"Galfio"] = @"This dimension I created is quite nice, I must say. That wretched girl's magic isn't bad.
But I cannot forgive her for tricking me with that weaselly contract.
Once I obtain the shards, I can begin my revenge.
While I wait, though, I might mess with a few of the townsfolk. Hee hee.
* Galfio, holder of the key of the gap."
mm[?"Pipi"] = @"I found a tasty looking, fully formed burger on the ground. Of course, I ate it right away.
Then I passed out, and came to in this strange dimension.
Now it seems like almost everything I touch turns into a red paperclip. What is going on?
The only item I've found that doesn't become a horrible red paperclip is WorthlessShrooms.
I guess that's not so bad. WorthlessShrooms have a nice flavour and crunch to them.
* Pipi, the careless red paperclip hater."
mm[?"Creamy"] = @"I'm getting so frustrated meow.
I heard that kitteys can be found here. But I can't discover a single one.
"+(global.shroom_shuffle ? 
  "It also feels like there's been some magic that shuffled the mushrooms around. Maybe kitteys are just in some far off lands that I'll never find now." :
  "")+@"
And the person outside only sells extremely expensive breeds I can't afford.
I want a kittey more than anything in the world, so much that I feel like crying.
I will go back to the Citey and cry. If someone brings me a kittey one day, they will become my best friend.
* Creamy, who really loves kitteys."
mm[?"Beleve"] = @"This place seems to have both tall and wide mushrooms growing freely.
That's great. I can really try creating large Bags of power here.
I've almost perfected the power that I call the MegaBag. It is the biggest Bag physically possible by the laws of this world.
It requires so much power to create though. I wonder if it will ever be created....
* Beleve, the well-practiced Bagsmith."
mm[?"Sweets"] = @"I like to come here sometimes.
I leave bunney items lying around in far-off corners as an advertisement for my shop, hehe.
I really love the cuteness of bunneys. I hope my shop will make everyone realize how joyful it is to spend time with a bunney.
After all, I'm a bunney that took a human form when I entered the world of Eivlys. So I want everyone to love both bunneys and humans.
* Sweets, the lovely bunney keeper."
mm[?"Cherie"] = 
(global.shroom_shuffle ? 
 @"Seems like uncommon mushrooms have been shuffled around in this world. What a pain.
I make my hints by whittling down TallShrooms into a question mark shape. Are there still TallShrooms in the hills?"  :
 @"Another tall mushroom, nice. They're a little hard to find here, but I lost my keys, so I can't get in the locked zones....
I make my hints by whittling these down into a question mark shape. They're just the right size for it.")+@"
I heard from the princess that someone is coming to this world, but she doesn't know when. 
"+(global.shroom_shuffle ? "Couldn't you have waited until they get here to shuffle things around?" : "")+@"
When that person gets here, I wonder if they'll use my great hints.
* Cherie, hint giver of the Cherry Blossom Hills."
mm[?"Geniux"] = @"Mushroom Zone Generation Algorithm.... Hacked.
Great, I can see the locations of everything. Not just rare mushrooms, but memories and bonus items too.
And I found some interesting stuff in the game files. Items used in dev that were dummied out of the final version.
That spirit from another world will have some fun with these. If they can figure out the password, that is.
It was a little too easy to break the security, but that's just how things are, when your a hackmaster.
* Geniux, the enigmatic hacker."
mm[?"Lamia"] = 
@"Gah, these TallShrooms take up so much space in my bag.
"+(global.shroom_shuffle ? 
 "Apparently uncommon mushrooms were shuffled or something, so I had to do some digging to even find out where to source them now." :
 "")+@"
I need them to build the walls of my Houses. But they're such a pain to carry around.
If I only had a Truck, I could simply load a bunch in the back and drive along.
But where am I supposed to find a Truck in this bizarre world?! I shouldn't have signed that damn contract...
* Lamia, the frustrated house builder."
mm[?"Arianne"] = @"I am a guardian of an area. I'm strictly forbidden from entering the area I guard.
But during lunchtime, I'm allowed to go to this nearby zone and pet the kitteys.
"+(global.shroom_shuffle ? 
@"Though I can't find any today. Maybe the shrooms have been shuffled around....
Still, being here makes me so nostalgic. When the person I love was still around, we used to go here together and play with the fluffy kitteys."
:
@"It makes me so nostalgic. When the person I love was still around, we used to go here together and play with the fluffy kitteys."
)+@"
I wish I knew what happened to her. She's the one told me to guard the area, but I never understood why.
I'll follow her orders to the end. But my deepest wish is for her to come back, hold my hand, and take me to play with the kitteys.
* Arianne, the romantic guardian."
mm[?"Katey"] = @"Doot doot doo... got a date with a hot lady. She sells plusheys in the Citey, or something.
She has no fashion sense, and doesn't even realize for a second how trendy it is for shopkeeps to wear a sack with your store logo on it these days.
But she's fun to spend time with, despite not being quite as classy as I.
"+(global.shroom_shuffle ? 
  @"We've been running around the zone together. I told her there are kitteys here, but with the shrooms shuffled around, I actually don't even know where to find them anymore.
She's fully bought it though and is super excited to see a kittey in the wild." :
  @"We've been running around the zone together, looking for kitteys to pet. Every time we find one, I make something up about how it's an incredibly rare and exotic breed.
Pretty sure she's falling for it, hook, line and sinker. She always goes, "+"\"Oh that's so interesting, tell me more!\" with a wide smile.")+@"
Yeah, I guess she's a little gullible. But I feel like this is going to lead to a great relationship.
* Katey, the pretentious kitten seller."
mm[?"Bunnisse"] = @"I am known as one of the Followers of Eivlys.
We are a group of three, who meet in this zone every night of the full moon.
My specialty is of course, the connection between the magic of Eivlys and the Bunneys of the Cherry Blossom Hills.
It is said the Bunneys granted Eivlys her world-creation magic, and know all the secrets of the Mushroom Zones she created.
Were it possible to speak to Bunneys, perhaps we could learn some deep secrets.
* Bunnisse, a mysterious follower of Eivlys."
mm[?"Chicen"] = @"A chicken so high in the sky and clouds. That's me.
With my wings, I can fly freely and go anywhere. It's the ultimate item.
Oh, I heard something interesting from the princess. A new spirit will enter the world sometime soon.
I doubt they will obtain my great wings, though. The Trading Quest they must complete is quite long and arduous. Cluck cluck cluck.
* Chicen, a little animal that soars the skies."
mm[?"Laborie"] = @"Hehehe, various parts and items can be found in this location.
I need them for my inventions that will take over the world by storm.
Not content with simpley getting several PhDs in science, I went on to invent many incredibly evil things.
That is why I built the famous Laboratorey in the Cherry Blossom Hills to house my villaineous inventions.
But for some reason, I'm having trouble hiring research assistants. Probably because my inventions are too intimidating, gahaha.
* Laborie, menacing founder of the Laboratorey."
mm[?"Searchey"] = @"I'm just a researcher at the Laboratorey. I went to grad school with the founder when she was getting one of her many PhDs.
I can't keep up with her though. She's invented so much, when all I've created is a single fish car.
And I'm having so much trouble keeping my research notes organized. I wish some kind of clip for papers was something that's been invented.
My life is just a bit of a mess right now. I'm too focused on research to take care of myself and maintain my relationships with old friends.
I really look up to the founder. Although she's quite evil, she is smart, collected and cool. If I could be more like her, maybe I'd be happy with myself.
* Searchey, anxious researcher at the Laboratorey."
mm[?"Stormy"] = @"Gahaha! This cloud area is where I feel at home, since I am myself a cloud.
I strike fear into everyone's hearts with my thunder and lightning. It's fun to electrocute them all, hehe.
Recently though.... I don't want to admit it, but I've been feeling a little lonely.
I wish more people would come visit me. So I can zap 'em! Bwahaha!
* Stormy, the rambunctious thundercloud."
mm[?"Karie"] = @"I'm worried about my best friend in the world.
Lately, she has been spending so much time in this dangerous zone, and we don't talk as much.
I went in here myself to see what's so wonderful about it. But it just seems like a dangerous area to me.
Supposedly something called the SparkleShroom is here. Would she want this item? Why? It's such a mystery.
"+(global.shroom_shuffle ? 
  "And is she even looking in the right place when all the Shrooms are Shuffled around?" : "")+@"
I really want her to be happy because she's my best friend in the universe and world. When I see her again, I'll give her a big hug.
* Karie, a gentle and caring friend."
mm[?"Sunny"] = 
(global.shroom_shuffle ? 
@"I don't know where it is... that special golden mushroom. It's supposed to be in this zone, but it seems mushrooms have been shuffled around.":
@"I've finally found it, that golden mushshroom they talk about in the tales around here.")+@"
"+"\"Stand under the Legendary Tree, and give the Sparkle Shroom to the person you love...\""+@" 
Then, I guess it's all supposed to work out somehow. Nobody seems to remember the rest of that legend, but they all talk about it as if it had a happy ending.
These days, whenever we hang out, I feel like I'm on the brink of breaking down and crying. If she doesn't look at me enough, I get anxious and start to hate myself.
I have to bring this to a close before I just fall apart. 
"+(global.shroom_shuffle ? "As soon as I find that shroom, I'll send the letter." : 
"I'll send the letter tonight.")+@"
At the very least, I hope you still want to be my friend.
* Sunny, trapped in a hopeless love."
mm[?"Tressa"] = @"Magical balls...
Somehow, even though my main purpose in life is to be a Treasure Hunter, I've become an expert in magical balls.
For example, I am one of the few who knows they can only be summoned with the power of WideShrooms or TallShrooms.
Those aren't found in the Canyon, only basic and tiny shrooms grow in the Mushroom Zones here.
"+(global.shroom_shuffle ? 
  @"Or so I thought? It seems like some villain's gone and shuffled the shrooms around, so I don't know my head from my tail anymore.
If I can find some tiny mushrooms, I'll cook a delicious stew. I hope they're still here...." : 
"So why am I in this zone? Well, the tiny ones are great in a stew. If I can find a bunch, I'll be able to cook a delicious meal this evening.")+@"
* Tressa, the intrepid treasure hunter."
mm[?"Crisis"] = @"I'm THE worldsbest at making shoes. I can make so many different kinds.
But everyone just wants to tell me that I look like a little strawberry! It's not fair.
I wish people would focus on my skills and not simply my cute appearance.
Whatever, I will continue crafting the best shoes ever! Your loss if you underestimate my shoes!
* Crisis, superbly skilled shoemaker."
mm[?"Canyetta"] = @"Whew, I've been working on these hints all day. I need a break.
I've been running around all day interviewing people. The minecart person refused to tell me how to get a license, but I figured it out eventually.
The princess said someone's coming to this world pretty soon. I wonder if they'll even bother looking at all these hints.
A hint that nobody looks at is like a flower that sits in a forgotten vase until it wilts....
Ooh, I should write that one down. My poetry just keeps getting better.
* Canyetta, the Canyon's hint giver and aspiring poet."
mm[?"Graal"] = @"Hee hee hee.... I am one of the Followers of Eivlys.
We are a group of three villains, who meet in this very zone every night of the full moon....
I'm known as a real demon of hell.... I enjoy watching people suffer, ahahaha.
Tear off their arms. Tear off their legs. And watch them scream as I use my violent magic spells on them, hahaha, I love just thinking about it.
I actually don't know much about Eivlys. I only became a Follower so I could possess one of her body parts, this crimson shard dripping with her blood.
I wish I could have torn this shard off her body myself while she begs me for mercy, euyahaha!!!
* Graal, a sadistic follower (?) of Eivlys."
mm[?"Junky"] = @"
There's something that makes me feel so peaceful about hunting for SpringShrooms.
Most of the rare kinds of mushrooms in this world have this ethereal, magical quality.
But a SpringShroom is something like a mechanical spare part. 
It holds just a little bit of the world together. It looks unnecessary, but something would break if it was removed.
I think most people are "+"\"spare parts\", in this sense. They hold together their little section of the world."+@"
"+(global.shroom_shuffle ? 
 "...You know, I haven't actually come across a SpringShroom yet. Am I just having bad luck, or did someone shuffle the mushrooms around again?" 
 : "")+@"
* Junky, the melancholic junk shop owner."
mm[?"Mineria"] = @"My minecart business has been going down the drain lately.
Nobody seems to want to get a minecart license nowadays. And it's just too dangerous to let people drive without one.
You, who is reading this memory. Yes, I'm speaking to you directly.
Please get a minecart license and come ride one of my wonderful carts.
* Mineria, the minecart ride manager."
mm[?"Rockout"] = @"This place is pretty rockin'.
Can't find a dang RainbowShroom anywhere though. So I'm leaving.
"+(global.shroom_shuffle ? 
  @"Heard there's some kind of Shroom Shuffle going on, so the RainbowShroom might not even be in this zone..." : "")+@"
I'm not cut out for this mushroom hunting business. Rocks are more of my jam.
There's this cutey in the Citey who wants a RainbowShroom. I wonder if they'd be okay with a colourful rock instead....
* Rockout, who loves rocks deeply."
mm[?"Jealousy"] = @"The geological structure of the rocks in here is really so interesting.
The rocks seem to contain what are called "+"\"Fragments of Rainbow\"."+@"
These have some kind of magical resonance power. I must study it closely.
Oh, but the floating rocks outside are amazing as well. How do they float so high in the sky?
This canyon is a geologist's paradise. I regretted it at first, but in the end, I'm glad I came to this world.
* Jealousy, the expert geologist."
mm[?"Truckster"] = 
(global.shroom_shuffle ? "Hmm, wasn't this supposed to be a good place to find SpringShrooms? Did things get shuffled again or something?"
: @"Great, I think I've got all the SpringShrooms I need.")+@"
I come down here to get parts for Truck repairs, I'm good friends with the junk shop owner.
I've been Trucking for as long as I can remember. I really want a change of pace.
But I can't find anyone who wants a Truck. Most people in this world don't even know much about Vehicles at all, let alone Trucks.
I've heard rumors about someone new moving in. Maybe they'll be interested....
* Truckster, the haggard old truck driver."
mm[?"TroubleAndStrife"] = @"Ah, this place is so nostalgic. Hey, I heard something funny while we were walking here.
Oh, what is it?
Remember when we simultaneously proposed to each other? That whole big scene under the Legendary Tree?
Wow, that's ancient history. But of course I remember. Like it was yesterday.
Well, apparently it's turned into something of a local legend. If you meet under the tree, and give your lover a sparkly golden shroom, you'll never get divorced.... something like that.
Hee hee! That's amazing. Why'd they leave out the bit where you showed up riding a horse though?
I know! That was the best part. Maybe it's because I totally fumbled the landing when I tried to dramatically jump off.
You rolled a good distance. Your arms and legs were all dirty, heehee.
Haha.... well, I hope if some young couple follows in our footsteps, they'll end up as happy as we are.
Me too. I've loved all 30 years of this.
H-hey! We almost bonked heads! Don't come in for a kiss so suddenly!
* Trouble and Strife, wife and wife."
mm[?"Chestor"] = @"Ku ku ku! Look out, world!
I'm finally moving out of this awful zone! Got myself a nice new spot on the clifftop!
Can't wait for a certified Treasure Hunter to plunder my contents! It's gonna kick ass!
Later, losers! I'd say it was nice knowing you, but it actually SUCKED!
* Chestor, the Magnificent Mimic."
mm[?"Cavey"] = 
(global.shroom_shuffle ? "Okay, time to sit down on the ground and think." :
@"Okay, time to sit down on this WideShroom and think.")+@"
What would be good to give as hints?
The Secret Tomb puzzle is a given. And that RaceRunner is tricky to beat....
I wonder if I should talk about the dice gamble. That one has a great reward.
Oh, there's not enough time.... The princess said they'll be showing up in this world any day now!
* Cavey, the Cavern's hint giver."
mm[?"Gooney"] = @"Gooney gooney goo.... leave an item for you.
I'm leaving my Gift Shop items around this zone to advertise my wonderful shop.
Maybe I'll even leave a few in other areas too, hee hee.
I will have so many customers and they'll all be able to have lovely memorys of their time in the caverns.
Hey, you know, recently I've been hearing about some kind of wandering spirit that will come to this world.
I hope they will make some wonderful memorys here.
* Gooney, the memorious Gift Shop merchant."
mm[?"Wizaron"] = @"Wizard style cars are known to be among the fastest cars.
To test their speed, I like to come race them against the SpeedShrooms here.
SpeedShrooms are blue, much like the great blue oceans in my hometown.
I'd like to go back there and go for a swim. But this world is nice as well.
"+(global.shroom_shuffle ? @"...Hey, you know, I haven't actually come across a SpeedShroom yet today.
Did some kind of great Wizard shuffle the shrooms around?" : "")+@"
* Wizaron, who builds great wizard style cars."
mm[?"Slash"] =@"Every day, I've trained wildly to hone my racing skills.
Yet I can't seem to match the supersonic speed of the shrooms in this zone.
"+(global.shroom_shuffle ? "Though today, those speedy shrooms don't seem to be around. Wonder if there's been a shuffle." : "")+@"
I guess humans just aren't built for speed. Even in my home world, there were animals that can outspeed humans by an enormous factor.
Still, I believe I am now faster than all of the humans in this world. Will some kind of Racing Queen ever appear that can defeat me?
* Slash, the rapid race runner."
mm[?"Archaea"] = @"I am known as one of the Followers of Eivlys.
We are a group of three, who meet in this zone every night of the full moon.
I am quite interested in the theory of how Eivlys constructed this world.
It's often said that the Mushroom Zones were created using powerful magic, while she built the surrounding areas with her bare hands.
Even if it's a Grand Witch, it's amazing to me that someone could have built all this.
I've been deeply investigating this cavern to try to understand the heart of Eivlys. If only I could meet her somehow.
* Archaea, a studious follower of Eivlys."
mm[?"Queen"] = @"An clouds area hidden in the sky. The perfect place to lie down in my bikini and get a tan.
My daughter asked me to manage the museum once the spirit gets here. It sounds tough, but I think my magic level is high enough to do it.
I'll have to keep an eye on that spirit's travels, and summon copies of the items they find....
I doubt I'll have time to come here and relax anymore. Well, as my daughter said, this is for Eiv's sake, so we're kind of obligated.
* Queen, the sexy and cool museum curator."
mm[?"Alicia"] = @"I'm simply a thief at heart who has lived my whole life by tricking, lying and stealing.
And now it's finally going to come in handy. It seems like the museum is reopening and it's going to be stocked full of treasures soon.
Someone big must be coming to this world again. I can't wait to pull off some huge heists with them.
Oh yeah, but the princess said something weird. It was like:
"+"\"You have to keep your heists to one a day, and one item at a time. And don't tell my mom about this meeting!\""+@"
I wonder what that's about. Well, the princess is one of the few who can still speak with Eivlys, so it must be important.
* Alicia, the thief from the darkness."
mm[?"Bella"] = @"Yawn.... I'm so sick of searching for ingredients.
Poison shrooms used to grow near the entrance, but it seems like you have to go way far to get them now.
"+(global.shroom_shuffle ? "And I think there's been a shuffle anyways. I can feel it in my bones. PoisonShrooms might not even grow here anymore." : "")+@"
I've tried to go without them, but's simply impossible to make a good taco without that kick of deadly poison.
Maybe I can start running a promotion. Bring me two PoisonShrooms for a free meal deal!
* Bella, owner and manager of Taco Sacc."
mm[?"Lady"] = 
(global.shroom_shuffle ? "I've been trying to find a PoisonShroom all morning, but I guess there's been a shuffle. Ha, my lucky streak continues." :
@"Ah, there's one of those tasty poisonous shrooms.")+@"
They say they have a 1 in 1000 chance of killing you each time you eat one.
I like those odds. If Evey was still around, I'd make a bet with her, that I'll die of natural causes before the poison gets me.
Though I've lost everything I own, I still have the heart of a gambler. I can't get over the thrill of putting my life on the line.
* Lady, the old gambling fool."
mm[?"Mococo"] = @"I met this stranger who offered me danger.
They said I could go to a dimension where all rare shrooms can be found.... on the ground....
Ah, forget the rhyming thing, it's not like anyone's listening to my thoughts.
So, they had these creepy robes on, and they told me I could enter this hell dimension, but I'd be eternally cursed and never find love.
I don't know about that curse though. I think me and the taco lady have a pretty good thing going.
And I can stock up on rares so easily here. So it's pretty much a complete win for me, you cultist-looking weirdo. Hee hee.
* Mococo, of the secret rare shroom shop."
mm[?"Zupzel"] =@"I believe the entrance to the legendary Secret Tomb must be somewhere in the upper cavern.
But where? Is it in this zone, or somewhere outside?
Is it perhaps related to the four statues on the bottom level? This puzzle is so intriguing.
Even if I never solve this mystery, I feel inspired to create my own puzzle for others to have fun with.
I hope to meet another puzzle fan soon and share this love of puzzles.
* Zupzel, the curious puzzler."
mm[?"Country"] = @"I'm on the hunt for the Secret Tomb of legends, so I came down here to inspect the Four Statues of legends.
I am sure they have something to do with the secret puzzle.
But then I got hungrey, so I went into this zone to get some PoisonShrooms. There's a deal where you can get a free taco if you get two of them.
"+(global.shroom_shuffle ? 
"I can't find them though, what is going on? Are things shuffled around in this world?" : "")+@"
It's rather annoying to get around this zone. I wish I had a Horse to ride on.
There is nothing I want in the world more than a Horse. It's my one purpose in life.
If I find the Secret Tomb and it doesn't have a Horse inside, I will scream.
* Country, the horse girl."
mm[?"Mooncry"] = @"My role in this story is to be eternally imprisoned in this tower.
But Eivie isn't so cruel as to not let me wander a bit when the princess isn't around.
This zone... the "+"\"Corpse Facade\"? Hee hee, that girl comes up with such weird names."+@"
Anyways, I like the aesthetic here. It feels both striking and hostile.
"+(global.shroom_shuffle ? "I think I'll just relax on the floor until I have to get back to playing prisoner." :
"I think I'll just relax on one of these FloatShrooms until I have to get back to playing prisoner.")+@"
* Mooncry, sealed in the clock tower."
mm[?"Princess"] = @"Doesn't look like Eivlys is around here. Where did she run off to?
She's been super busy lately, but I can usually find her in this zone....
I think she said preparations are almost complete. That's great.
I hope her big experiment turns out well. I know she's been feeling a little anxious about it.
I'll miss spending time with her once it all begins. But even if she stuck around, I think I'd be too busy to chat....
You're making a big sacrifice here, Eiv. Or perhaps you already made one a long time ago.
But I know that it doesn't matter to you as long as your creations make others happy.
* Princess, the vessel."
mm[?"Eivlys"] = @"It's been a long day. Time to write the final memory.
I'll hide it in the deepest depths of my favourite zone.
Creating this world taught me a harsh lesson about magic. It's easy to summon forth these vast, near-infinite spaces....
But without the touch of a human heart, they feel dull and gray. Magic is truly powerless without love.
I got so frustrated, tweaking my spells over and over, casting thousands of these labyrinthine caverns into the abyss.
And now, as it all comes to an end, am I even really satisfied?
When I started, I thought I'd be able to create something really wonderful with magic. But I don't trust it anymore. It's a tool, one that can supplement but never replace my hands.
Hee hee, I wonder how many people will even read this message.
* Eivlys."
