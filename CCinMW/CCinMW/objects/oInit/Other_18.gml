///@description Destroy data structures
show_debug_message("Destroying data structures.");
// global.initial_bag_grid_map doesn't need to be reset
/// global.sticker_grid_cache_map doesn't need to be reset
// global.bag_grid_map is a map of grids, needs special handling
for(var k=ds_map_find_first(global.bag_grid_map); !is_undefined(k); k=ds_map_find_next(global.bag_grid_map,k)) {
	ds_grid_destroy(global.bag_grid_map[? k]);	
}
ds_map_destroy(global.npc_bag_map);
ds_map_destroy(global.trade_map);
ds_map_destroy(global.item_map);
ds_map_destroy(global.explored);
ds_map_destroy(global.collected);
ds_map_destroy(global.memory_dead);
ds_map_destroy(global.rare_dead);
ds_map_destroy(global.cute_dead);
ds_map_destroy(global.shroom_deletion_map);
ds_map_destroy(global.shroom_insertion_map);

ds_map_destroy(global.memorys);

ds_map_destroy(global.lock_item);
ds_map_destroy(global.lock_name);
ds_map_destroy(global.lock_flag);

if is_array(global.memory_particles) {
	ds_list_destroy(global.memory_particles[0]);
	ds_list_destroy(global.memory_particles[1]);
	ds_list_destroy(global.memory_particles[2]);
}

if global.mushzone_params != -1 {
	ds_map_destroy(global.mushzone_params);	
}

ds_list_destroy(global.bag_contents); 
if global.bag_contents_backup != -1 {
	ds_list_destroy(global.bag_contents_backup); 
}

// Preserve memory disk 0
var backup0 = ds_map_create();
var disk0 = global.memory_disks[|0];
for(var i=0; i<sprite_get_number(sFloppy); i++) {
	for(var j=0; j<array_length_1d(global.area_names); j++) {
		var list = ds_list_create();
		ds_list_copy(list,disk0[? global.area_names[j]])
		ds_map_add_list(backup0,global.area_names[j],list);
	}
}
ds_list_destroy(global.memory_disks);
global.memory_disks = ds_list_create();
ds_list_add(global.memory_disks,backup0);
ds_list_mark_as_map(global.memory_disks,0);