///@description Reset the world
event_perform_object(oInit,ev_other,ev_user8); // Clean out all the data structures

event_perform_object(oInit,ev_other,ev_user4); // Reinit world variables

event_perform_object(oInit,ev_other,ev_user5); // Reinit all the data structures

event_perform_object(oInit,ev_other,ev_user0); // Reinit the trades

event_perform_object(oInit,ev_other,ev_user6); // Reinit the memorys

event_perform_object(oInit,ev_other,ev_user7); // Lock the doors
