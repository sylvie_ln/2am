///@description Reset control options

global.gamepad_on = false;
global.gamepad_deadzone = 50;
global.cursor_control = 0;