num = sprite_get_number(sprite_index);
var xscale = image_xscale;
var yscale = image_yscale;
image_xscale = 1;
image_yscale = 1;
for(var i=0; i<xscale; i++) {
	for(var j=0; j<yscale; j++) {
		if j == 0 {
			ii[i,j] = minecart_random(1,num-1);
		} else {
			ii[i,j] = 0;	
		}
	}
}
image_xscale = xscale;
image_yscale = yscale;
alarm[0] = global.default_room_speed div 3;