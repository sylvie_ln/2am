{
    "id": "5f982993-aa9b-44a8-b96b-4ade835f74e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMushroomQuesoGrande",
    "eventList": [
        {
            "id": "ff353dd3-fa18-47be-ba88-9fa2331cce50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f982993-aa9b-44a8-b96b-4ade835f74e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0567e8f-0aeb-4242-b5f0-ee2d8b8dcbe4",
    "visible": true
}