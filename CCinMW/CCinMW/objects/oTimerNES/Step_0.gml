time += delta_time;
if time > t[phase] {
	phase++;
	if phase > 2 {
		instance_destroy();	
		if global.flag_tomb != 1 {
			execute_on_room_start([create_above_char,oHandOfMoon]);
			global.flag_tomb = 1;
		}
		room_go(rmCavernOfAncients);
		exit;
	} else {
		room_goto_next();
	}
}