if room == rmSecretTomb and !global.mute_music {
	audio_stop_sound(bgmSecretTomb);
	audio_sound_gain(bgmSecretTomb,global.bgm_volume,0);
	audio_play_sound(bgmSecretTomb,1000,false);
}