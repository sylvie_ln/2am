event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "The legendary Spring Sneakers, they will put a spring in your step."

damp = 0.95;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!