{
    "id": "5ab5ad8b-c0a5-4569-a0d3-f4dc2f05fbba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpringSneakers",
    "eventList": [
        {
            "id": "6ad9cf1d-bf73-42e7-8ea2-7ddb5c12a576",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ab5ad8b-c0a5-4569-a0d3-f4dc2f05fbba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a492a75-ae81-4cee-8430-54dd8d877b1a",
    "visible": true
}