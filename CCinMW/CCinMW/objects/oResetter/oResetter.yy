{
    "id": "b0c3b6d4-5818-4ba6-a936-3432a67db2e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oResetter",
    "eventList": [
        {
            "id": "500728de-44b5-41a9-8d8b-080c5bc6a6bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0c3b6d4-5818-4ba6-a936-3432a67db2e0"
        },
        {
            "id": "680e76c5-d7af-4c15-bc74-83bd7ed0733d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b0c3b6d4-5818-4ba6-a936-3432a67db2e0"
        },
        {
            "id": "a42f8eb8-9456-41d6-9732-30e9757dcfe6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "b0c3b6d4-5818-4ba6-a936-3432a67db2e0"
        },
        {
            "id": "c2f640e1-6e64-4f9e-9cd9-3ac22f70e385",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b0c3b6d4-5818-4ba6-a936-3432a67db2e0"
        },
        {
            "id": "2bf349a3-83e8-48b7-9170-cf1957069f78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b0c3b6d4-5818-4ba6-a936-3432a67db2e0"
        },
        {
            "id": "64984d0f-91f3-4bf6-a8a4-a019b5a9ab54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b0c3b6d4-5818-4ba6-a936-3432a67db2e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}