if !active { exit; }
var hpos = x;
var vpos = y;
var typed = string_upper(string_letters(keyboard_string));
if typed_prev != typed {
	play_sound(sndContractPenWrite);
	typed_prev = typed;
}
var txt = desc+"\nPress Escape to back out.\nType secret word \""+word+"\"\nto proceed: "+typed;
draw_text(x,y,txt);
if typed == word {
	count++;
	if count > time {
		switch(string_upper(word)) {
			case "CONTROL":
				event_perform_object(oInit,ev_other,ev_user3);
				with oInput {
					event_user(0);
				}
				save_options();
				save_keyconf();
			break;
			case "DEFAULT":
				event_perform_object(oInit,ev_other,ev_user2);
				audio_sound_gain(global.bgm,global.bgm_volume,0);
				window_set_fullscreen(global.fullscreen);
				if !global.fullscreen {
					window_set_scale(global.scale);
				}
				alarm[1] = 1;
				save_options();
				save_keyconf();
			break;
			case "REBIRTH":
				event_perform_object(oInit,ev_other,ev_user12);
				save_game();
			break;
			case "GOODBYE":
				file_delete(global.game_save_file);
				for(var i=0; i<ds_list_size(global.world_list); i++) {
					file_delete(global.world_list[|i]+global.game_save_file);	
				}
				file_delete(global.options_save_file);
				file_delete(global.keys_save_file);
				file_delete(global.world_save_file);
				game_end();
			break;
		}
		play_sound(sndMenuReset);
		instance_create_depth(0,0,0,oFlash);
		active = false;
		with parent { event_user(0); }
		exit;
	}
} else {
	count = 0;	
}