if !active { exit; }
if input_pressed("Escape") {
	play_menu_backward();
	active = false;
	with parent { event_user(0); }
	exit;	
}