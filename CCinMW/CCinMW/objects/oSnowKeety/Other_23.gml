var left = input_held("Left");
var right = input_held("Right");
var lp = input_pressed("Left");
var rp = input_pressed("Right");
var hdir = right-left;
if left and right {
	hdir = (input_held_time("Left") <= input_held_time("Right")) ? -1 : 1;
}

var ong = collision_at(x,y+1) and (bounced or vv >= 0);
if hdir != 0 {
	image_xscale = hdir;
	hv += image_xscale*acc;
	if abs(hv) < 0.5+acc {
		hv 	= hdir*(0.5+acc);
	}
	if ong {
		vv = -hop;
	}
}
do_friction = ong;
if input_held("Jump") and ong {
	height += plus;
	vv = -sqrt(2*grav*height);
}
if input_released("Jump") and vv < 0 {
	vv /= 2;
}
if ong and !input_held("Jump") {
	height = plus+1;	
}