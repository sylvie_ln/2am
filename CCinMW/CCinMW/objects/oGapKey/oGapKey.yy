{
    "id": "761c97b9-b417-4fd2-b621-91f76df348e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGapKey",
    "eventList": [
        {
            "id": "a73d4554-8aaf-43de-89d2-34f63a49350b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "761c97b9-b417-4fd2-b621-91f76df348e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "35257004-6737-4d61-9548-910f46977f48",
    "visible": true
}