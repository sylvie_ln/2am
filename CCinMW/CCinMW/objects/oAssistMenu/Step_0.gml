if !active { exit; }

if input_pressed_repeat("Up") {
	options_index--;
	play_menu_cursor_move();
}
if input_pressed_repeat("Down") {
	options_index++;
	play_menu_cursor_move();
}

options_index = int_cycle_inclusive(0, options_index, options_length - 1);

var option = options[options_index];

if input_pressed("Escape") or (option == done and input_pressed("Jump")) {
	play_menu_backward();
	active = false;
	with parent { event_user(0); }
	exit;
}

var has_choices = options[1] != noone and options[2] != noone;

if !has_choices {
	exit;
}

var choices = option[2];
if choices == noone { exit; }
var choices_length = array_length_1d(choices);

var index = option_choice_indices[options_index];
var orig_index = index;
if input_pressed_repeat("Left") {
	index--;
}
if input_pressed_repeat("Right") {
	index++;
}
option_choice_indices[options_index] = index;
if option_choice_indices[options_index] < 0 {
	play_sound(sndMenuProblem);
	option_choice_indices[options_index] = 0;
} else
if option_choice_indices[options_index] >= choices_length {
	play_sound(sndMenuProblem);
	option_choice_indices[options_index] = choices_length - 1;
} else if orig_index != index {
	play_menu_adjust();	
}