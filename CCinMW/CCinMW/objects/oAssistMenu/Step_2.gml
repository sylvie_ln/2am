for (var i = 0; i < options_length; i++) {
	var option = options[i];
	if option[1] == noone {
		continue;
	}
	var choices = option[2];
	var choice_index = option_choice_indices[i];
	var choice = choices[choice_index];
	variable_global_set(option[1], choice);
}