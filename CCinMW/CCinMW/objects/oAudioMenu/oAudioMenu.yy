{
    "id": "d6c9706b-abf8-4d19-9a75-1619dfecc672",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAudioMenu",
    "eventList": [
        {
            "id": "770fff73-0cdb-4c77-80db-c7b352bc0cb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6c9706b-abf8-4d19-9a75-1619dfecc672"
        },
        {
            "id": "7df39602-6a81-4a7a-b7d6-b12c6a48b27c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d6c9706b-abf8-4d19-9a75-1619dfecc672"
        },
        {
            "id": "3c9a9750-1422-4da3-80c0-d0615a52f1ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "d6c9706b-abf8-4d19-9a75-1619dfecc672"
        },
        {
            "id": "70d87131-8976-4486-b4c6-f574fb3f16e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d6c9706b-abf8-4d19-9a75-1619dfecc672"
        },
        {
            "id": "f2552a0d-256e-4793-8652-00e972994182",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d6c9706b-abf8-4d19-9a75-1619dfecc672"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}