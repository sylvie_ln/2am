event_inherited();

upblocker = false;
pushable = false;
carriable = false;
pusher = false;
carrier = false;

script = @"An emergency bubble that allows you to refill air in Mushroom Zones.
When used inside a zone, you can't pick it back up, since the atmosphere there makes the bubble's surface extra fragile."

damp = 0.5;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!