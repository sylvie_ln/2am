{
    "id": "388e05ee-1940-4d27-a883-d808131f8403",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinecartLicense",
    "eventList": [
        {
            "id": "84e0aa95-34c3-4e3d-a1a0-1b062c9891f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "388e05ee-1940-4d27-a883-d808131f8403"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8facc5ff-263b-4b15-bea5-fbaa24f1e97b",
    "visible": true
}