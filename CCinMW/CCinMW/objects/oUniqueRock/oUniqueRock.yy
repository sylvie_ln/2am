{
    "id": "965014f9-fa13-4ffb-b2eb-8832ef3b32c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oUniqueRock",
    "eventList": [
        {
            "id": "1d8af8eb-25aa-45de-95d1-afbb912063ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "965014f9-fa13-4ffb-b2eb-8832ef3b32c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1fd1a2de-75a7-4eb6-8017-f002c92248d0",
    "visible": true
}