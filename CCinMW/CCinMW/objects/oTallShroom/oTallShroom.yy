{
    "id": "177283a1-09a8-4bbb-b71b-2c7c5d4df6bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTallShroom",
    "eventList": [
        {
            "id": "fc016e5a-e73c-4122-867c-377d5679dda8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "177283a1-09a8-4bbb-b71b-2c7c5d4df6bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
    "visible": true
}