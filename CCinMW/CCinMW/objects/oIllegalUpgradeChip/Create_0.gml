event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "An illegally hacked upgrade chip with the protections bypassed. When combined with a mapping implant, allows you to see rare item locations in Mushroom Zone maps."

damp = 0.75;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!