{
    "id": "48c12c2f-c326-4494-b6f7-dae1ed760e9d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlonck",
    "eventList": [
        {
            "id": "ec5a4e7c-5003-44dc-bd31-6783974ac088",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "0f883bf0-2671-4284-a0d2-d8e6dc3a9958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "4c332945-289c-4855-9c51-69f938fa0893",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "5ceb9b4d-69ee-4826-9f91-e5795a93508d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "467c7524-0400-42e6-a659-bc7ca4d62a2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "0e85761b-46e8-4d10-9e1b-76d1628dbb7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "1ef41b94-2750-416c-bd8d-4516cba7afcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "2cce9d71-e91e-42ab-a6a5-f0a4260186d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "92fb63c7-6e69-47ae-a87a-d2c350e20088",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        },
        {
            "id": "2d8549b8-c801-4bf7-a290-772a0077a2b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "48c12c2f-c326-4494-b6f7-dae1ed760e9d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d575649-441d-43d2-86d9-f646b788338d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c403f000-51c7-489a-a516-a8fee8b8eee4",
    "visible": true
}