event_inherited();
top = false;
num = sprite_get_number(sprite_index);
if layer_exists("Overlay") {
	overlay = layer_background_get_sprite(layer_background_get_id("Overlay"));
	depth = layer_get_depth("Overlay")-20;
} else {
	depth = 20;
	overlay = -1;
}
just_created = true;
alarm[0] = 1;
fade = false;
fake = false;

dofade = false;
faded = false;
ff = 1;

ii_only = false;
outer = false;
number = 0;