event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "The legendary 100Ton Boots, they are so stylish and cool."

damp = 0.5;
bounce = damp;
fric = 0.95;
grav *= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!