{
    "id": "e39da0cf-fd5c-4ac9-8a92-9c4393693e89",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o100TonShoes",
    "eventList": [
        {
            "id": "53c8a942-9f77-4d23-a67a-55dbe7313b9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e39da0cf-fd5c-4ac9-8a92-9c4393693e89"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28935559-b33b-4dd4-a71f-cf6a084b3871",
    "visible": true
}