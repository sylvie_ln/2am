if bag_has_item(oBunneyEars) {
	draw_sprite_ext(sBunneyEarsNES,0,x,bbox_top-4,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
if bag_has_item(oMiniBunney) {
	draw_sprite_ext(sMiniBunneyNES,0,x,bbox_top-8,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
if bag_has_item(oChickenWings) {
	draw_sprite_ext(sCharWingsNES,image_index,x,y,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}

draw_self();