var ong = !place_free(x,y+1);
if !ong {
	vv += grav;	
	if room != rmSecretTomb2 and vv > 4 { vv = 4; }
}

if !move(hv,0) {
	hv = -hv;	
}
if !move(vv,1) {
	vv = 0;
	hv = spd;
}

if place_meeting(x+24,y,oBoneNES) and ong {
	vv = -sqrt(2*grav*33);
}

if room == rmSecretTomb2 {
	var cx = camera_get_view_x(view_camera[0]);
	var cy = camera_get_view_y(view_camera[0]);
	cy = clamp(cy+4,0,room_height-global.view_height);
	camera_set_view_pos(view_camera[0],cx,cy);
	vv = 4.35;
}

if place_meeting(x+32,y,oBlockNES) {
	if acc != 0 {
		acc += 0.2;	
	}
	if hv != 0 {
		hv = 0;
		camera_set_view_target(view_camera[0],noone);
		acc = 1;
	}
	var cx = camera_get_view_x(view_camera[0]);
	cx = clamp(cx+spd*acc,0,room_width-global.view_width);
	camera_set_view_pos(view_camera[0],cx,0);
}

var bunney = bag_has_item(oBunneyAmulet);
if ong {
	if hv != 0 {
		sprite_index = bunney ? sBunneyRNES : sCharRNES;
	} else {
		sprite_index = bunney ? sBunneySNES :sCharSNES;
	}
} else {
	if vv < 0 {
		sprite_index = bunney ? sBunneyJNES :sCharJNES;
	} else {
		sprite_index = bunney ? sBunneyFNES :sCharFNES;	
	}
}