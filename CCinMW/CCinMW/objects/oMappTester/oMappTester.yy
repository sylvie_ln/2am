{
    "id": "8a1785ad-35f0-4d38-8411-3ea1b9ad10ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMappTester",
    "eventList": [
        {
            "id": "14189551-dd1a-40d9-9ebc-c5394a927b57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a1785ad-35f0-4d38-8411-3ea1b9ad10ac"
        },
        {
            "id": "d6a5df7a-271b-4276-aa99-d5b69848660d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8a1785ad-35f0-4d38-8411-3ea1b9ad10ac"
        },
        {
            "id": "3dcd86f8-dcbb-4395-ad18-a831e8cceeb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "8a1785ad-35f0-4d38-8411-3ea1b9ad10ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}