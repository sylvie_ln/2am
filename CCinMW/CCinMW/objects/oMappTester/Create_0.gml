sylvie_shuffle();
step = -1;
scale = 2;
surfy = -1;
global.dead_end = 0;
var nodes = list_create(
mapp_mxn(2,2,2,2,global.sylvie_seed+sylvie_random(0,1024)),
mapp_mxn(2,2,2,2,global.sylvie_seed+sylvie_random(0,1024)),
mapp_mxn(2,2,2,2,global.sylvie_seed+sylvie_random(0,1024)),
mapp_mxn(2,2,2,2,global.sylvie_seed+sylvie_random(0,1024)),
);
var n;
n = nodes[|1];
n[@ninfo.xp] = 2;
n = nodes[|2];
n[@ninfo.yp] = 2;
n = nodes[|3];
n[@ninfo.xp] = 2;
n[@ninfo.yp] = 2;

top = mapp_link(list_create(nodes[|0],nodes[|1]),global.sylvie_seed+sylvie_random(0,1024),1);
bot = mapp_link(list_create(nodes[|2],nodes[|3]),global.sylvie_seed+sylvie_random(0,1024),1);
node = mapp_link(list_create(top,bot),global.sylvie_seed+sylvie_random(0,1024),2);
ds_list_destroy(nodes);
x = scale-3;
y = scale-3;