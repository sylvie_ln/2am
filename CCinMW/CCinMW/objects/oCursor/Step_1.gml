cx = camera_get_view_x(view_camera[0]);
cy = camera_get_view_y(view_camera[0]);
cw = camera_get_view_width(view_camera[0]);
ch = camera_get_view_height(view_camera[0]);
cl = cx;
ct = cy;
cr = cl+cw;
cb = ct+ch;

var offset_me = os_type == os_macosx;
if offset_me and (true_mouse(0) != px) or (true_mouse(1) != py) {
	offset[0] += (true_mouse(0) - px)/global.scale;
	offset[1] += (true_mouse(1) - py)/global.scale;
	offset[0] = clamp(offset[0],cl-global.scale,cr+global.scale);
	offset[1] = clamp(offset[1],ct-global.scale,cb-global.scale);
}
px = true_mouse(0);
py = true_mouse(1);

