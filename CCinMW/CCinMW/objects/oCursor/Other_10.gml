if global.cursor_control == 1 { exit; }
var moved = input_pressed("Left") or input_pressed("Right") or input_pressed("Up") or input_pressed("Down");
var pressed = input_pressed("Jump");
if (moved or (pressed and !wait_until_moved)) and !active {
	if alarm[0] >= 0 {
		alarm[1] = alarm[0]; 
	} else {
		event_user(1);
	}
}
if wait_until_moved and pressed { wait_until_moved = false; }
if mouse_check_button_pressed(mb_left) { active = false; wait_until_moved = true; }
if !active { exit; }
var offset_me = os_type == os_macosx;
var spd = 4 * global.scale;
if input_held("Jump") {
	spd = global.scale;
}	
var ofl = cl-16;
var onl = cl;
var ofr = cr;
var onr = cr-16;
var oft = ct-16;
var ont = ct;
var ofb = cb;
var onb = cb-16;
var outer = offset[0] >= ofr or offset[0] <= ofl 
		or offset[1] <= oft or offset[1] >= ofb;
if input_held("Left") {
	if offset_me {
		if outer { offset[0] = ofr; }
		if offset[0] >= ofr { offset[1] = clamp(offset[1],ont,onb); }
		offset[0] -= spd/global.scale;
	} else {
		display_mouse_set(true_mouse(0)-spd,true_mouse(1));
	}
}
if input_held("Right") {
	if offset_me {
		if outer { offset[0] = ofl; }
		if offset[0] <= ofl { offset[1] = clamp(offset[1],ont,onb); }
		offset[0] += spd/global.scale;
	} else {
		display_mouse_set(true_mouse(0)+spd,true_mouse(1));
	}
}
if input_held("Up") {
	if offset_me {
		if outer { offset[1] = ofb; }
		if offset[1] >= ofb { offset[0] = clamp(offset[0],onl,onr); }
		offset[1] -= spd/global.scale;
	} else {
		display_mouse_set(true_mouse(0),true_mouse(1)-spd);
	}
}
if input_held("Down") {
	if offset_me {
		if outer { offset[1] = oft; }
		if offset[1] <= oft { offset[0] = clamp(offset[0],onl,onr); }
		offset[1] += spd/global.scale;
	} else {
		display_mouse_set(true_mouse(0),true_mouse(1)+spd);
	}
}