///@description
active = false;
wait_until_moved = false;
offset = [global.view_width div 2, 16+(global.view_height div 2)];
px = true_mouse(0);
py = true_mouse(1)

cx = camera_get_view_x(view_camera[0]);
cy = camera_get_view_y(view_camera[0]);
cw = camera_get_view_width(view_camera[0]);
ch = camera_get_view_height(view_camera[0]);
cl = cx;
ct = cy;
cr = cl+cw;
cb = ct+ch;