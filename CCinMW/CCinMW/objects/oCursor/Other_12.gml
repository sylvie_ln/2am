if os_type == os_macosx {
	cx = camera_get_view_x(view_camera[0]);
	cy = camera_get_view_y(view_camera[0]);
	cw = camera_get_view_width(view_camera[0]);
	ch = camera_get_view_height(view_camera[0]);
	cl = cx;
	ct = cy;
	cr = cl+cw;
	cb = ct+ch;
	offset[0] += cl;
	offset[1] += ct;
}