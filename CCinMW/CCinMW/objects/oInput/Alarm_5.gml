/// @description Blocker
if blocked { 
	unblock = true;
	show_debug_message("Fullscreen switch complete");
	if !global.fullscreen {
		window_set_scale(global.scale);
		alarm[1] = 1;
	}
} else {
	blocked = true;
	show_debug_message("Blocking...");
	alarm[5] = room_speed;	
}