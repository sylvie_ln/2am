///@description Init

event_inherited();

// Prepare script for processing
if ds_exists(lines,ds_type_list) {
	ds_list_sylvie_destroy(lines);	
	lines = -1;
}
if ds_exists(labels,ds_type_map) {
	ds_map_destroy(labels);
	labels = -1;
}

labels = ds_map_create();
var conditionals;
var elifs;
var nesting = 0;
var max_nesting = 0;
var if_stack = ds_stack_create();
lines = string_split(string_trim(script),global.nl);
for(var i=0; i<ds_list_size(lines); i++) {
    var line = string_split_once(string_trim(lines[|i])," ");
    line[0] = string_trim(line[0]);
    line[1] = string_trim(line[1]);
    if line[0] == "!if" {
        nesting++;
        if nesting > max_nesting {
            conditionals[nesting] = 1;
            elifs[nesting] = 1;
            max_nesting = nesting;
        } else {
            conditionals[nesting]++;
        }
        ds_stack_push(if_stack,i);
    }
    if line[0] == "!elif" {
        var new_line = array_create(2);
        new_line[0] = "!jump";
        new_line[1] = "_fi"+string(nesting)+"_"+string(conditionals[nesting]);
        ds_list_insert(lines,i,new_line);
        i++;
        
        var new_line = array_create(2);
        new_line[0] = "_elif"+string(nesting)+"_"+string(elifs[nesting])+":";
        new_line[1] = "";
        ds_list_insert(lines,i,new_line);
        i++;
        
        line[0] = "";
        
        var j = ds_stack_pop(if_stack);
        var if_line = lines[|j];
        if_line[0] = "!jumpf";
        var cond = string_split_once(if_line[1],",");
        cond = cond[0];
        if_line[1] = cond+",_elif"+string(nesting)+"_"+string(elifs[nesting]);
        ds_list_replace(lines,j,if_line);
        
        elifs[nesting]++;
        ds_stack_push(if_stack,i);
    
    }
    if line[0] == "!else" {
        var new_line = array_create(2);
        new_line[0] = "!jump";
        new_line[1] = "_fi"+string(nesting)+"_"+string(conditionals[nesting]);
        ds_list_insert(lines,i,new_line);
        i++;
        
        line[0] = "_else"+string(nesting)+"_"+string(conditionals[nesting])+":";
        line[1] = "";
        
        var j = ds_stack_pop(if_stack);
        var if_line = lines[|j];
        if_line[0] = "!jumpf";
        var cond = string_split_once(if_line[1],",");
        cond = cond[0];
        if_line[1] = cond+",_else"+string(nesting)+"_"+string(conditionals[nesting]);
        ds_list_replace(lines,j,if_line);
    }
    if line[0] == "!fi" {
        line[0] = "_fi"+string(nesting)+"_"+string(conditionals[nesting])+":";
        line[1] = "";
        if ds_stack_size(if_stack) == nesting {
            var j = ds_stack_pop(if_stack);
            var if_line = lines[|j];
            if_line[0] = "!jumpf";
            var cond = string_split_once(if_line[1],",");
            cond = cond[0];
            if_line[1] = cond+",_fi"+string(nesting)+"_"+string(conditionals[nesting]);
            ds_list_replace(lines,j,if_line);
        }
        nesting--;
    }
    ds_list_replace(lines,i,line);
}

for(var i=0; i<ds_list_size(lines); i++) {
    var line = lines[|i];
    while is_label(line[0]) {
        labels[? get_label(line[0])] = i-1;  
        line = string_split_once(string_trim(line[1])," ");
        ds_list_replace(lines,i,line);
    }
}
ds_stack_destroy(if_stack);
current_line = 0;