event_inherited();
name = "Changemi"

lines = -1;
labels = -1;

flag_surprise=0;

disappear = false;

speaker = self;

script =
@"Juicy juicey JIGGLER"
stop = false;
talking = false;
textbox = noone;

surfy = -1;
stencil = -1;
surfy2 = -1;
stencil2 = -1;
depth = 10;

screen_percent = 0.5;

generated = false;
placed = true;
xt = x;
yt = y;
just_created = true;

cutey = true;
shake = 0;

act1 = "Talk"
act2 = "Trade"

cutscene = false;

restock_count = 0;
must_restock = true;

throw_ns = false;

first_item = noone;


// PRE TRADE
// When you first open the trade screen.
orig_text = "Let's trade baby.";
orig_alt_text = orig_text;
// When you select an item other than the selectd one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back."
// Prefix before item name
thatsmy_text = "That's my "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't want that item...."
// When you offer a low-rank item.
low_text = "I guess that item is okay...."
// When you offer a mid-rank item.
medium_text = "That item seems good."
// When you offer a high-rank item.
high_text = "Yes! I love that item!"
// When you offer exactly what they want.
too_many_text = "I don't want that much stuff!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I want more items than that." 
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Maybe a little more items...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Take out the garbage and I'll consider!"
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Less trash, more good stuff, and I'll think about it."

// EXACT TRADES
// When you offer more than one of the exact item.
exact_text = "That's exactly what I've been looking for!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = no_text
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold, 
// and you haven't offered the exact item.
multiple_exact_text = "I just want one of those!"
// When all of your offered items are garbage (unwanted).
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Don't need anything extra, just give me that one item!"

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No matter what you offer, I'm not trading this."
// All items are garbage 
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Your offer is worthless to me."
// Special worthless_text for the first item added to NPC's bag.
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything"
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Looks good to me! Let's trade!"
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Um, you can't back out of the trade now...."
// When the NPC begins taking you items.
thank_text = "I'll take your stuff now!"
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks for the trade!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks for the trade...."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Please don't steal..."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Wow, dishonest person..."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you...."
//// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised...."
// When you pick up an item that you stole.
yes_give_text = "Yes, please give back the item you promised...."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can have it, but give back the items you promised...."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want it!! Give me the items you promised...."
