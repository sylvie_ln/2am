event_inherited();
if ds_exists(lines,ds_type_list) {
	ds_list_sylvie_destroy(lines);	
	lines = -1;
}
if ds_exists(labels,ds_type_map) {
	ds_map_destroy(labels);
	labels = -1;
}
if ds_map_exists(global.trade_map,name) {
	trade_cleanup();
}