event_inherited();
if cutey {
	if surface_exists(stencil) { surface_free(stencil); }
	if surface_exists(surfy) { surface_free(surfy); }
	stencil = -1;
	surfy = -1;
	if surface_exists(stencil2) { surface_free(stencil2); }
	if surface_exists(surfy2) { surface_free(surfy2); }
	stencil2 = -1;
	surfy2 = -1;
}
if ds_exists(lines,ds_type_list) {
	ds_list_sylvie_destroy(lines);	
	lines = -1;
}
if ds_exists(labels,ds_type_map) {
	ds_map_destroy(labels);
	labels = -1;
}
if ds_map_exists(global.trade_map,name) {
	trade_cleanup();
}