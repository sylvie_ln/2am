{
    "id": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPC",
    "eventList": [
        {
            "id": "0b77cb07-2597-4122-9e32-24c5ca478b66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        },
        {
            "id": "264dfa7f-c0ae-4934-95ce-4a6220bf8cdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        },
        {
            "id": "898f18e5-440c-4588-87e9-f14b33dc2472",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        },
        {
            "id": "dd4a0154-ea94-44f2-a0bc-6f2174e58482",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        },
        {
            "id": "138a09f5-6c0b-4d0e-9753-175b3d6f68c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        },
        {
            "id": "62035e2e-7ba0-4f13-96bc-706263b42141",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        },
        {
            "id": "009356a8-3bea-426e-9387-0f6a3ad01ea3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        },
        {
            "id": "50c8777c-e51e-4bda-b042-5808d0c95c35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        },
        {
            "id": "9779c94d-99b9-4140-80ac-0451f43e640a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f688b8-dd4d-4d42-96ce-776174c8176b",
    "visible": true
}