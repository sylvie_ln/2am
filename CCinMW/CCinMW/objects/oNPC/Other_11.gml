///@description Talk
if !talking {
    talking = true;
}
if talking {
    if instance_exists(textbox) {
        if textbox.finished {   
            if textbox.answer == -1 and !ds_list_empty(textbox.questions) {
                textbox.answer = floor(ds_list_size(textbox.questions)/2);
            } else { 
                with textbox {
                    instance_destroy();
                }
                textbox = noone;
            }
        } else {
            with textbox {
                event_user(eTextbox.finish);
            }
        }
    } 
    if !instance_exists(textbox) {
        while true {
            if current_line >= ds_list_size(lines) {
                current_line = 0;
                talking = false;
                text = "";
                break;
            }   
            var line = lines[|current_line];
            text = execute_line(line);
            current_line++;
            if stop {
                stop = false;
                talking = false;
                break;
            }
            if text != "" {
                break;
            }
        }
		if talking == false {
			if disappear { 
				if object_index == oSurpriseBox {
					with instance_create_depth(x,y,depth,oBasicPop)	{
						image_index = other.image_index;	
					}
				}
				instance_destroy(); 
			}
		}
        if text != "" {
            textbox = instance_create_depth(x,y,depth,oTextbox);
            textbox.text = text;
			textbox.max_width = round(screen_percent*global.view_width);
            textbox.npc = speaker;
            with textbox {
                event_user(eTextbox.init);
            }   
        }
    }
}