event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = @"> Memory Disks.\nA Memory Disk is an incredible new data-storage format that can hold up to 1.44 MB of binary-information. It was invented at the Cherry Blossom Hills Laboratorey.
> Purpose of Memory Disks.\nIn a Mushroom Zone, you might encounter memories. A Memory Disk can record the location of a memory. You will appear at this location when you reenter the Mushroom Zone. This is a revolution in Mushroom Zone exploration.
> Use of Memory Disks.\nPlace the Memory Disk in your Bag. When you examine a memory, you will be asked if you want to record its location. Note that, as explained in the next section of this Manual, only one location per Mushroom Zone can be recorded.
> Limitations of Memory Disks.\n(Part I)\nMemory Disks contain 12 Sectors of Write-Once Memory (WOM). Each Sector is calibrated for a different Mushroom Zone. Therefore, only one location per Mushroom Zone can be recorded. The information on a Memory Disk cannot be reset or overwritten. Please buy another one.
> Limitations of Memory Disks.\n(Part II)\nYou cannot enter a Mushroom Zone while carrying two or more Memory Disks in your Bag. This would cause fatal data corruption and destroy the world. A protection is in place to prevent Mushroom Zone entry in this case.
> Number of Memory Disks.\nThe Cherry Blossom Hills Laboratorey has the technology to produce up to 10 Memory Disks. After this, no more will be produced. At the time of writing, we have produced two: Disk #1 and the prototype Disk #0.
However, Disk #0 was stolen. If you find it, tear it into scraps immediately and do not attempt to use it."

damp = 0.75;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!