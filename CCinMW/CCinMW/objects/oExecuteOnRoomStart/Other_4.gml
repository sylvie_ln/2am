if variable_instance_exists(self, "script") && is_array(script) && array_length_1d(script) >= 1 && script_exists(script[0])
	script_execute_ext(script);
	
instance_destroy();