if global.world_name == "EIVLYS" { exit; }
if oTitleMenu.subsub { exit; }
var difficulty_str = ["Normal", "Tricky", "Difficult"];
var shuffle_str = ["Standard", "Shuffled"];
var txt =
global.world_name+@"
World Rank: "+difficulty_str[global.difficulty]+"\n"
	+"Shroom Style: "+shuffle_str[global.shroom_shuffle]+"\n"
	+"Zone Layout: #"+string(global.zone_layout);
	draw_text(x,y,txt);