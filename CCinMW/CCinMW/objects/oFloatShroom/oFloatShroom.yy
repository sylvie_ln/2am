{
    "id": "0cbf5b0e-b25e-4a0a-adfe-d022891eaa62",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFloatShroom",
    "eventList": [
        {
            "id": "ff4c7f72-868a-49f1-8b5c-7bfcabdecd28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0cbf5b0e-b25e-4a0a-adfe-d022891eaa62"
        },
        {
            "id": "9212a98a-076d-4fb4-b99d-13d8262764d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0cbf5b0e-b25e-4a0a-adfe-d022891eaa62"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2ee024c8-e0ef-4f88-8d86-0b95509e3bb2",
    "visible": true
}