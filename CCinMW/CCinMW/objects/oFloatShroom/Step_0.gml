if paused { exit; }




if thrown_timer > 0 {
	thrown_timer--;	
}

if !move(hv,0) {
	hv = -hv;	
}

if !move(vv,1) {
	vv = -vv;
}


if thrown_timer <= 0 {
	hv = hv*fric;
	if abs(hv) < 0.5 {
		hv = 0;	
	}
	vv = vv*fric;
	if abs(vv) < 0.5 {
		vv = 0;	
	}
}