{
    "id": "e624a8ff-e98f-4f95-9d97-16a50775348d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oVehicleTemplate",
    "eventList": [
        {
            "id": "a0128d20-1bde-470a-b1d9-6240735ded6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e624a8ff-e98f-4f95-9d97-16a50775348d"
        },
        {
            "id": "1770e424-4931-4c4b-ba9d-ebc3ff9a7b7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "e624a8ff-e98f-4f95-9d97-16a50775348d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "65ef3ec7-0bb9-430d-b919-9b85a40a68c9",
    "visible": true
}