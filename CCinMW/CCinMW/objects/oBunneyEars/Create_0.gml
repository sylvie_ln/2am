event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "Cutey bunney ears, they will make you look almost as cute as a real bunney."

damp = 0.75;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!