if !on {
	with other { instance_destroy(); }
	on = true;
	if name == "canyon" and global.flag_bridge_canyon {
		with oNPC {
			if name == "Tressa"	{
				x -= 88;
				y -= (424-88);
			}
		}	
	}
	variable_global_set("flag_ladder_"+name,true);
	instance_create_depth(0,0,0,oFlash);
	play_sound(sndBallBridgeLadderUse);
}