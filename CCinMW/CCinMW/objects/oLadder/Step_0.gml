if on {
	sprite_index = sLadderOn;
	upblocker=true;
	image_alpha = 1;	
} else {
	sprite_index = sLadderOff;
	upblocker=false;
	if bag_has_item(oLadderOrb) or instance_exists(oLadderOrb) {
		image_alpha = (3+2*sin(2*pi*(current_time/3000)))/10;
	} else {
		image_alpha = 0;	
	}
}