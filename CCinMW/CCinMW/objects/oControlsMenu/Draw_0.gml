if !active { exit; }
draw_clear(c_black);
var yp = y+4;
for(var i=0; i<control_options.last; i++) {
	draw_set_font(global.neco_font);
	var xo = x+12;
	var xp = xo;
	switch(i) {
		case control_options.back:
		//xp -= 4;
		yp = y+global.view_height-16;
		case control_options.key_config:
		yp += 4;
		case control_options.gamepad_deadzone:
		case control_options.gamepad_on:
		case control_options.cursor_control:
		skip = 8;
		draw_set_font(global.neco_font);
		break;
		default:
		if i-1 == control_options.key_config {
			yp += 4;	
		}
		skip = 16;
		draw_set_font(global.neco_font);
	}
	if i == selected and (i <= control_options.key_config or i >= control_options.back) {
		draw_text(xp-8,yp,"*");
	}
	if i != control_options.key_config {
		draw_text(xp,yp,words[i]);
	}
	var pxp = xp;
	var constart = 64;
	var conjump = 60;
	var conheader = ["Keyboard","KBoard Alt","Gamepad","GPad Alt"];
	var pos = 0;
	var xp = xo+constart;
	if i == control_options.key_config {
		repeat((global.gamepad_on ? array_length_1d(conheader) : 2)) {
			draw_text(xp,yp,"* "+conheader[pos]);
			pos++;
			xp += conjump;
		}
	}
	xp = pxp+string_width(words[i])+16;
	var boost = 96;
	var fluff = string_width("Sylvie");
	switch(i) {
		case control_options.back:
		break;
		case control_options.key_config:
		break;
		case control_options.gamepad_on:
			xp = xo+boost;
			if selected == i {
				draw_text(xp-8,yp,"<");
			}
			draw_text_centered(xp+(fluff div 2),yp,(global.gamepad_on ? "Yes" : "No"));
			if selected == i {
				draw_text(xp+fluff,yp,">")
			}
		break;
		case control_options.gamepad_deadzone:
			xp = xo+boost;
			if selected == i {
				draw_text(xp-8,yp,"<");
			}
			draw_text_centered(xp+(fluff div 2),yp,string(global.gamepad_deadzone)+"%");
			if selected == i {
				draw_text(xp+fluff,yp,">")
			}
		break;
		case control_options.cursor_control:
			var cctxt = "";
			switch(global.cursor_control) {
				case 0:	cctxt = "Off"; break;
				case 1:	cctxt = "On"; break;
			}
			xp = xo+boost;
			if selected == i {
				draw_text(xp-8,yp,"<");
			}
			draw_text_centered(xp+(fluff div 2),yp,cctxt);
			if selected == i {
				draw_text(xp+fluff,yp,">")
			}
		break;
		default:
			var xp = xo+constart;
			var list = oInput.actions[? act[i]];
			for(var j=0; j<(global.gamepad_on ? ds_list_size(list) : 2); j++) {
				var istr = input_string_sprite(list[|j]);
				var inp = list[|j];
				if inp[|0] == input_kind.waiting {
					draw_set_alpha(0.5);
				} 
				//disabled_show_debug_message(istr);
				if selected_key[i] == j  and selected == i {
					draw_set_color(c_gray)
					draw_text(xp,yp+1,"*");
					draw_text_sprite(xp+string_width("* "),yp+1,istr);
					draw_set_color(c_white)
					draw_text(xp,yp,"*");
				}
				draw_text_sprite(xp+string_width("* "),yp,istr);
				draw_set_alpha(1);
				var pxp = xp;
				xp += string_width_sprite(istr);
				/*
				if j < ds_list_size(list)-1 {
					istr = ", ";	
					draw_text(xp,yp,istr);
					//xp += string_width(istr);
				}
				*/
				xp = pxp+conjump;
			}
	}
	yp += skip;
}
draw_set_font(global.neco_font);
var inp = oInput.actions[? "Jump"];
var k1 = inp[|0];
var k2 = inp[|1];
var inp = oInput.actions[? "Shroom"];
var k3 = inp[|0];
var k4 = inp[|1];
var inp = oInput.actions[? "Left"];
var l = inp[|0];
var inp = oInput.actions[? "Right"];
var r = inp[|0];
switch(selected) {
	case control_options.back:
		var t = "Press Confirm to go back."
	break;
	case control_options.key_config:
		var t = "You are simpley a JIGGLER."
	break;
	case control_options.gamepad_on:
		var t = "Toggle whether gamepad input is accepted."
	break;
	case control_options.gamepad_deadzone:
		var t = "Adjust gamepad stick deadzone."
	break;
	case control_options.cursor_control:
		var t = "Toggle whether keyboard/gamepad can move the Cursor."
	break;
	default:
		if waiting {
			if selected_key[selected] != 0 {
				var t = "Enter an input, or press Shift+Esc to unset the control."	
			} else {
				var t = "Enter an input, or press Shift+Esc for the default."	
				
			}
		} else {
			var t = "Press Confirm "
			+"to assign "
			+(selected_key[selected] < 2 ? "a key." : "a gamepad input.")
		}
}
draw_text_sprite(x+global.view_width-4-(string_width_sprite(t)),y+global.view_height-12,t);