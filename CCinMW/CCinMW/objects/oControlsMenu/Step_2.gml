if !active { exit; }
if waiting {
	if keyboard_check(vk_shift) and keyboard_check_pressed(vk_escape) {
		if selected_key[selected] == 0 {
			input_assign_key(act[selected],defkey[selected]);
		} else {
			input_assign_unset(act[selected]);
		}
		waiting = false;
		play_menu_backward();
		exit;
	}
	if selected_key[selected] < 2 {
		if !keyboard_check(vk_shift) {
			var key = input_any_key();
			if key != -1 {
				input_assign_key(act[selected],key);
				waiting = false;
				play_menu_backward();
				exit;
			}
		}
		if keyboard_check_released(vk_shift) {
			input_assign_key(act[selected],vk_shift);
			waiting = false;
			play_menu_backward();
			exit;
		}
	} else {
		if !keyboard_check(vk_shift) or keyboard_check_released(vk_shift) {
			var key = input_any_key();
			if key != -1 {
				var list = oInput.actions[? act[selected]];
				ds_list_copy(list[|selected_key[selected]],previous);
				waiting = false;
				play_menu_backward();
				exit;
			}
		}
		var button = input_any_gamepad_button();
		if button != -1 {
			input_assign_gamepad_button(act[selected],button);
			waiting = false;
			play_menu_backward();
			exit;
		}
		var axis_dir = input_any_gamepad_axis();
		var axis = axis_dir[0];
		var dir = axis_dir[1];
		if axis != -1 {
			input_assign_gamepad_axis(act[selected],axis,dir);
			waiting = false;
			play_menu_backward();
			exit;
		}
		var hat_dir = input_any_gamepad_hat();
		var hat = hat_dir[0];
		var dir = hat_dir[1];
		if dir != 0 {
			input_assign_gamepad_hat(act[selected],hat,dir);
			waiting = false;
			play_menu_backward();
			exit;
		}
	}
	exit;	
}

if input_pressed("Jump") {
	switch(selected) {
		case control_options.back: 
			active = false;
			play_menu_backward();
			with parent { event_user(0); }
		break;
		case control_options.key_config:
		case control_options.gamepad_on:
		case control_options.gamepad_deadzone:
		break;
		default:
			if !waiting {
				var list = oInput.actions[? act[selected]];
				ds_list_copy(previous,list[|selected_key[selected]]);
				input_assign_wait(act[selected],selected_key[selected]);
				waiting = true;
				play_menu_forward();
				exit;
			}
	}
} else if input_pressed("Escape") {
	if selected != control_options.back {
		play_menu_cursor_move();
		selected = control_options.back	
	} else {
		active = false;
		play_menu_backward();
		with parent { event_user(0); }	
	}
} 
if input_pressed_repeat("Left") {
	switch(selected) {
		case control_options.back:
		case control_options.key_config:
		break;
		case control_options.gamepad_on:
			global.gamepad_on = !global.gamepad_on;
			play_menu_adjust();
		break;
		case control_options.gamepad_deadzone:
			if global.gamepad_deadzone > 95 {
				global.gamepad_deadzone = 100;	
			}
			global.gamepad_deadzone -= 5;
			if global.gamepad_deadzone < 5 {
				global.gamepad_deadzone = 1;	
			}
			play_menu_adjust();
		break;
		case control_options.cursor_control:
			global.cursor_control = int_cycle_inclusive(0,--global.cursor_control,1);
			play_menu_adjust();
		break;
		default:
			selected_key[selected]--;
			if selected_key[selected] < 0 {
				selected_key[selected] = 0;
			}
			play_menu_adjust();
	}
} 

if input_pressed_repeat("Right") {
	switch(selected) {
		case control_options.back:
		case control_options.key_config:
		break;
		case control_options.gamepad_on:
			global.gamepad_on = !global.gamepad_on;
			play_menu_adjust();
		break;
		case control_options.gamepad_deadzone:
			if global.gamepad_deadzone < 5 {
				global.gamepad_deadzone = 0;	
			}
			global.gamepad_deadzone += 5;
			if global.gamepad_deadzone > 95 {
				global.gamepad_deadzone = 99;	
			}
			play_menu_adjust();
		break;
		case control_options.cursor_control:
			global.cursor_control = int_cycle_inclusive(0,++global.cursor_control,1);
			play_menu_adjust();
		break;
		default:
			var size = ds_list_size(oInput.actions[? act[selected]]);
			selected_key[selected]++;
			if selected_key[selected] >= (global.gamepad_on ? size : 2) {
				selected_key[selected] = (global.gamepad_on ? size : 2)-1;
			}
			play_menu_adjust();
	}
}

if input_pressed_repeat("Up") {
	var prev_selected = selected;
	selected--;
	if selected == control_options.key_config {
		selected--;	
	}
	if selected < 0 {
		selected = control_options.last-1;
	}
	if act[selected] != "" {
		var size = ds_list_size(oInput.actions[? act[selected]]);
		selected_key[selected] = min((global.gamepad_on ? size : 2)-1,selected_key[prev_selected]);
		
	}
	play_menu_cursor_move();
}

if input_pressed_repeat("Down") { 
	var prev_selected = selected;
	selected++;
	if selected == control_options.key_config {
		selected++;	
	}
	if selected >= control_options.last {
		selected = 0
	}
	if act[selected] != "" {
		var size = ds_list_size(oInput.actions[? act[selected]]);
		selected_key[selected] = min((global.gamepad_on ? size : 2)-1,selected_key[prev_selected]);
	}
	play_menu_cursor_move();
} 