{
    "id": "f74ce6c4-9a00-4542-9049-99ab642a33e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGuardianShield",
    "eventList": [
        {
            "id": "efac6d1e-65e8-4218-9b45-ec6dcfeb63ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f74ce6c4-9a00-4542-9049-99ab642a33e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30aefb07-d5c7-492a-9b20-10b1f525ccae",
    "visible": true
}