///@description Ride
if !instance_exists(oChar) { exit; }
if riding {
	solid = false;
	riding = false;
	oChar.riding = false;
	oChar.depth = odepth;
	
	pushable = true;
	carriable = true;
	pusher = true;
	carrier = true;
	precise_upblocker = true;
} else {
	var prepos = [oChar.x,oChar.y];
	oChar.x = x+vehicle_pos[0];
	oChar.y = y+vehicle_pos[1];
	with oChar {
		if !place_free(x,y) {
			x = prepos[0];
			y = prepos[1];
			play_sound(sndCannotPlaceItemInBag);
			exit;	
		}
	}
	odepth = oChar.depth;
	oChar.depth = depth+1;
	solid = true;
	riding = true;
	oChar.riding = true;
	oChar.hv = 0;
	oChar.vv = 0;
	if object_index == oWizardmobile {
		image_xscale = oChar.image_xscale;
	}
	
	pushable = false;
	carriable = false;
	pusher = false;
	carrier = false;
	precise_upblocker = false;
}