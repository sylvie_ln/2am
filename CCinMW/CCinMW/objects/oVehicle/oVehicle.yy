{
    "id": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oVehicle",
    "eventList": [
        {
            "id": "efc80a80-12ca-420a-9d3b-462a11534b93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca"
        },
        {
            "id": "00410113-978c-4efb-9cbc-b468cfb7ba49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca"
        },
        {
            "id": "bfdc6f77-5f88-4acc-8aec-880faffdbf5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca"
        },
        {
            "id": "d91ca891-0e70-4a8f-87d4-160e1ae6a77a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca"
        },
        {
            "id": "6739d732-a331-4c02-b312-a4d3ba3d5937",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}