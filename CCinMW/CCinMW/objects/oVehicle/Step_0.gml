if paused { exit; }

var xp = x;
var yp = y;


if riding {
	solid_boundary = false;
	mask_index = ride_mask;
	var xs = image_xscale;
	event_user(13);	
} else {
	solid_boundary = true;	
}

if !riding and mask_index != item_mask {
	mask_index = item_mask;
	if collision_at(x,y) and !collision_at(x,y+1) {
		mask_index = ride_mask;	
	}
}

event_inherited();
if bounced and abs(vv) < damp+dampboost {
	vv = 0;
}

if riding and instance_exists(oChar) {
	oChar.x += x-xp;
	oChar.y += y-yp;
	if xs != image_xscale {
		oChar.x = x+vehicle_pos[0]*image_xscale;
	}
}

