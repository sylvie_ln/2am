event_inherited();

upblocker = false;
pushable = false;
carriable = false;
pusher = false;
carrier = false;

damp = 0;
bounce = 0;
fric = 0.95;

enum hints {
	BiggerBag,
	CheeseyTreat,
	RainbowShroom,
	ClimbingTower,
	TowerPrisoner,
	OverpricedKitteys,
	FloatShroom,
	LegendaryTree,
	StormyDemeanor,
	ShadyIndividual,
	MinecartLicense,
	SpringShroom,
	SomethingSparkly,
	CrossingCanyon,
	CanyonSky,
	PoisonShroom,
	WinningRace,
	SecretTomb,
	RareShop,
	GamblersHeart,
	last
}

var wideshroom_area = "Cavern of Ancients";
var tallshroom_area = "Cherry Blossom Hills";
var tinyshroom_area = "Canyon Cliffs";
switch(get_unco_shroom(unco_shroom_type.wide)) {
	case oTallShroom:	tallshroom_area = "Cavern of Ancients"; break;
	case oTinyShroom:	tinyshroom_area = "Cavern of Ancients"; break;
}
switch(get_unco_shroom(unco_shroom_type.tall)) {
	case oTinyShroom:	tinyshroom_area = "Cherry Blossom Hills"; break;
	case oWideShroom:	wideshroom_area = "Cherry Blossom Hills"; break;
}
switch(get_unco_shroom(unco_shroom_type.tiny)) {
	case oTallShroom:	tallshroom_area = "Canyon Cliffs"; break;
	case oWideShroom:	wideshroom_area = "Canyon Cliffs"; break;
}
hint_script[hints.BiggerBag]
=@"You might find that your Bag can't hold a lot of items. In particular, your basic Bag can't carry more than one WideShroom or TallShroom at a time, which is very inconvenient!
There is a Bagsmith in the lower left area of the Citey who can help you. Bring her WideShrooms and TallShrooms and she can use them to upgrade your Bag.
WideShrooms are native to the Mushroom Zones of the "+wideshroom_area+@", and TallShrooms are native to the Mushroom Zones of the "+tallshroom_area+@". You won't find them many other places.
You can also purchase WideShrooms and TallShrooms from UnCoMus, the Uncommon Mushroom Shop in the Citey Center."
var rainbowshroom = 
"This is quite a hard item to get. It's natively found in a Mushroom Zone high in the sky of the Canyon Cliffs. This is a hard area to reach, and you'll also need a key to enter the zone."
if get_rare_shroom(rare_shroom_type.float) == oRainbowShroom {
	var rainbowshroom = 
	"This item is actually in a strange place. It's in the Mushroom Zone hidden inside the Clock Tower. You'll need a key to enter the zone, and it might be hard to reach without certain Hands of the Clock."
} else
if get_rare_shroom(rare_shroom_type.kitty) == oRainbowShroom {
	var rainbowshroom = 
	"This item is in a cute location. It's in a Mushroom Zone in the Cherry Blossom Hills that requires a key to enter. Also, the zone is hidden behind a secret passage!"
} else
if get_rare_shroom(rare_shroom_type.poison) == oRainbowShroom {
	var rainbowshroom = 
	"This item is the Mushroom Zone in the lower right area of the Cavern of Ancients. It requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.sparkle) == oRainbowShroom {
	var rainbowshroom = 
	"This item is in the Mushroom Zone on the left side of the Cherry Blossom Hills. It requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.speeder) == oRainbowShroom {
	var rainbowshroom = 
	"This item is in an unusual place. It's found in a Mushroom Zone nestled in the upper right area of the Cavern of Ancients. It's somewhat hard to reach, and it requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.spring) == oRainbowShroom {
	var rainbowshroom = 
	"This item is a bit hidden. It's found in a Mushroom Zone in the lower left area of the Canyon Cliffs. The entrance to the zone is hidden behind a secret passage, and it requires a key to enter."
} 
hint_script[hints.RainbowShroom]
=@"Someone in the upper left area of the Citey wants a rare mushroom known as the RainbowShroom.
"+rainbowshroom+@"
There are other ways to get a RainbowShroom, but it's a secret!"
hint_script[hints.CheeseyTreat]
=@"In the upper right area of the Citey, someone wants a cheesey treat.
This treat can be found at a restaurant in the lowest area of the Cavern of Ancients.
You'll have to gather ingredients for the owner though, which might be a bit of a hassle."
hint_script[hints.ClimbingTower]
=@"The Cylvey Clock Tower can be climbed without any items that aid mobility, but it's rather difficult.
You might want to wait until you can bounce or jump higher to tackle it. Note that you can't throw items inside the tower, so that won't help!
You'll need certain Hands of the Clock to make progress through the tower. I won't tell you which ones exactly. Explore and figure it out!
There's also a Mushroom Zone inside, which again needs certain Hands to reach, as well as a key to open. 
Finally, I'll note that two of the 12 Hands of the Clock are hidden in the tower itself. If you've found ten Hands in the outer world, you're ready to climb the tower.
You can reach the top without all 12 Hands, but will you really be able to restore the flow of time if some of them are missing? A Clock needs 12 Hands to work."
hint_script[hints.TowerPrisoner]
=@"To reach the very top of the tower, you'll need to solve the riddle posed by the prisoner in the upper right to get the Hand of Lies.
Don't pay attention to the meaning of the story she tells you. It's really just a word game. Or maybe you could call it a letter game.
Hidden in the text somewhere is what you need to trade for the Hand. Look carefully."
hint_script[hints.CrossingCanyon]
=@"Near the entrance to the Canyon is a Treasure Hunter who is on the lookout for amazing items.
And up in the Canyon's sky is a Treasure Chest containing the amazing Hand of Light. It's a match made in heaven.
You might be able to climb up to the Canyon's upper area with various tricks, but it's not so easy for the Treasure Hunter.
Help her out by using the Magic Balls she sells to create a Magic Bridge across the Canyon's Great Gap, and then a Magic Ladder up to the sky."
var springshroom = @"Well, have you met the Junk Shop owner? They frequently need springs as parts for repairs. So, they set up shop near an area where SpringShrooms can be found.
However, you'll have to find a secret passage to get there. And you'll need a key to enter the zone as well."
if get_rare_shroom(rare_shroom_type.float) == oSpringShroom {
	var springshroom = 
	"Well, this is in a strange place. It's in the Mushroom Zone hidden inside the Clock Tower. You'll need a key to enter the zone, and it might be hard to reach without certain Hands of the Clock."
} else
if get_rare_shroom(rare_shroom_type.kitty) == oSpringShroom {
	var springshroom = 
	@"Well, you know the owner of the Kittenarium in the Cherry Blossom Hills? Her kitteys like to play around with springs, so she set up shop in a place where they're easily found. 
	There's actually a Mushroom Zone by her shop, hidden in a secret room. The zone also requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.poison) == oSpringShroom {
	var springshroom = 
	@"The owner of the Taco Sack restaurant in the Cavern of Ancients is a big spring fan. The Mushroom Zone to the right of her shop has SpringShrooms.
	However, note that the zone is locked, so you'll need a key."
} else
if get_rare_shroom(rare_shroom_type.sparkle) == oSpringShroom {
	var springshroom = 
	"It seems you can find these in the Mushroom Zone on the left side of the Cherry Blossom Hills. It requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.speeder) == oSpringShroom {
	var springshroom = 
	"Apparently these are in the Mushroom Zone up in the upper right of the Cavern of Ancients. It's tricky to reach, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.rainbow) == oSpringShroom {
	var springshroom = 
	"This is found high in the sky, in the Mushroom Zone at the top of the Canyon Cliffs. It's difficult to get to, and you need a key to enter the zone."
} 
hint_script[hints.SpringShroom]
=@"You might have met a few people who want a SpringShroom. This amazing item is not only useful for trades, but slightly increases your jump height.
But where do you find such a rare item in this world?
"+springshroom;
hint_script[hints.MinecartLicense]
=@"There's a fun Minecart Adventure you can go on in the Canyon. However, you need a license to drive a minecart.
In the Citey, right near the Canyon entrance, there's someone who has a license you can trade for.
If you aren't sure what to trade them, try to figure out what kind of items they like."
var sparkleshroom = @"You'll need to find the SparkleShroom, which is native to one of the locked Mushroom Zones in the Cherry Blossom Hills. It's to the left of the entrance to the Hills."
if get_rare_shroom(rare_shroom_type.float) == oSparkleShroom {
	var sparkleshroom = 
	@"You'll need to find the SparkleShroom, which is in a strange area: the Mushroom Zone hidden inside the Clock Tower. You'll need a key to enter the zone, and it might be hard to reach without certain Hands of the Clock."
} else
if get_rare_shroom(rare_shroom_type.kitty) == oSparkleShroom {
	var sparkleshroom = 
	@"You'll need to find the SparkleShroom. It's in the Mushroom Zone in the lower right area of the Cherry Blossom Hills.
	Did you know there's a Mushroom Zone there? It is actually hidden behind a secret passage. It also needs a key to enter."
} else
if get_rare_shroom(rare_shroom_type.poison) == oSparkleShroom {
	var sparkleshroom = 
	@"You'll need to find the SparkleShroom in the lower right Mushroom Zone of the Cavern of Ancients. It's locked though, so bring a key."
} else
if get_rare_shroom(rare_shroom_type.spring) == oSparkleShroom {
	var sparkleshroom = 
	@"You'll need to find the SparkleShroom, which is in a Mushroom Zone in the lower left part of the Canyon Cliffs. The zone is hidden behind a secret passage though, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.speeder) == oSparkleShroom {
	var sparkleshroom = 
	@"You'll need to find the SparkleShroom, in a Mushroom Zone high up in the upper right of the Cavern of Ancients. It's hard to reach, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.rainbow) == oSparkleShroom {
	var sparkleshroom = 
	@"You'll need to find the SparkleShroom, in the Mushroom Zone up in the sky of the Canyon Cliffs. It's quite hard to reach, and you need a key to enter the zone."
} 
hint_script[hints.SomethingSparkly]
=@"There are two people in the Canyon Cliffs who are big fans of Rocks. One of them has a very Unique Rock that seems valuable.
They say that they'll trade the Unique Rock for a shroom that sparkles. But what's such a shroom??
"+sparkleshroom;
hint_script[hints.CanyonSky]
=@"There are a lot of interesting things in the upper area of the canyon. For example, you can find a mysterious geologist who holds the Hand of Sky. 
There's also a treasure chest containing the Hand of Light, but she's a little rude, so I doubt she'll open up for anyone but a genuine Treasure Hunter.
But what I want to talk about most is the person who owns a Truck, and the pair of wifes looking for a House to settle down in.
These people are part of a great sequence of wonderful trades, leading to a reward with legendary power.
The sequence begins in the Cylvey Citey. Bring a worthless item to the person in the lower right, the one who hates red paperclips.
Completing the sequence will get you a certain great item. With two of that item, you can trade for the power of legends.
But how do you get a second such item? For that, you'll have to go back to where it all began, and help out the holder of the hated red clips."
var poisonshroom = @"Naturally, the restaurant was built near a Mushroom Zone where PoisonShrooms grow natively. It's just over to the right. But you'll need a key to enter it."
if get_rare_shroom(rare_shroom_type.float) == oPoisonShroom {
	var poisonshroom = 
	@"These grow somewhere rather unusual. There is a Mushroom Zone inside the Clock Tower. You'll need a key to enter the zone, and it might be tricky to reach without certain Hands of the Clock."
} else
if get_rare_shroom(rare_shroom_type.kitty) == oPoisonShroom {
	var poisonshroom = 
	@"These grow in a somewhat hidden place. There's a hidden passage in the lower right of the Cherry Blossom Hills leading to a Mushroom Zone. You'll need a key to get in."
} else
if get_rare_shroom(rare_shroom_type.sparkle) == oPoisonShroom {
	var poisonshroom = 
	@"These can be found in the Mushroom Zone on the left side of the Cherry Blossom Hills. You will need a key to enter it."
} else
if get_rare_shroom(rare_shroom_type.spring) == oPoisonShroom {
	var poisonshroom = 
	@"These can be found in a Mushroom Zone in the lower left part of the Canyon Cliffs. The zone is through a hidden passage though, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.speeder) == oPoisonShroom {
	var poisonshroom = 
	@"These can be found a little bit away from the restaurant itself. There's a Mushroom Zone in the upper right of the Cavern of Ancients that's a little tricky to find. It also requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.rainbow) == oPoisonShroom {
	var poisonshroom = 
	@"These are found quite far away. There's a Mushroom Zone way up in the sky of the Canyon Cliffs. It's quite hard to get to, and you need a key to enter."
}
hint_script[hints.PoisonShroom]
=@"The owner of Taco Sacc wants PoisonShrooms to make a lovely dish. Where can you find this rare shroom?
"+poisonshroom;
var speedshroom = @"These amazingly fast shrooms are found in the Mushroom Zone in the upper right area of the Cavern. It's a little tricky to get up there though, and it requires a key to enter."
if get_rare_shroom(rare_shroom_type.float) == oSpeedShroom {
	var speedshroom = 
	@"These amazingly fast shrooms are found in the Mushroom Zone inside the Clock Tower. You'll need a key to enter the zone, and it might be tricky to reach without certain Hands of the Clock."
} else
if get_rare_shroom(rare_shroom_type.kitty) == oSpeedShroom {
	var speedshroom = 
	@"These amazingly fast shrooms are found in a somewhat hidden place. There's a hidden passage in the lower right of the Cherry Blossom Hills leading to a Mushroom Zone. You'll need a key to get in."
} else
if get_rare_shroom(rare_shroom_type.sparkle) == oSpeedShroom {
	var speedshroom = 
	@"These amazingly fast shrooms are found in the Mushroom Zone on the left side of the Cherry Blossom Hills. You will need a key to enter it."
} else
if get_rare_shroom(rare_shroom_type.spring) == oSpeedShroom {
	var speedshroom = 
	@"These amazingly fast shrooms are found in a Mushroom Zone in the lower left part of the Canyon Cliffs. The zone is through a hidden passage though, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.poison) == oSpeedShroom {
	var speedshroom = 
	@"These amazingly fast shrooms are found in a Mushroom Zone in the lower right area of the Cavern of Ancients. It needs a key to enter."
} else
if get_rare_shroom(rare_shroom_type.rainbow) == oSpeedShroom {
	var speedshroom = 
	@"These amazingly fast shrooms are found in a far away land. There's a Mushroom Zone way up in the sky of the Canyon Cliffs. It's quite hard to get to, and you need a key to enter."
}
hint_script[hints.WinningRace]
=@"The RaceRunner has never been defeated in a race. Maybe you will be the first to win. But how?
Well, there are a few methods that will work. One way is to collect SpeedShrooms. Simply having them in your bag will increase your speed!
"+speedshroom+@"
Alternatively, you can use Shoes. In the Canyon Cliffs, near the entrance, there's a shoe shop. One of their items, the Pegasus Shoes, lets you dash quickly.
You'll need a SpeedShroom to trade for the shoes. Beware though, the shoes are so fast but they are hard to control.
One other option is to use a Vehicle to win the race. But it can be hard to find such Vehicles, and some of them aren't even fast enough to win."
hint_script[hints.SecretTomb]
=@"In the upper right area of the Cavern of Ancients, it's rumored there is a Secret Tomb. What could be inside?
Just getting to the upper right is a bit tricky. If you have a Ladderia's Ball, you can create a ladder up there. You can get these from the Treasure Hunter in the Canyon Cliffs.
If you don't have a ladder, there's a secret passage you can take.
Once you're there, you'll find a riddle inscribed on a statue that you must solve. The riddle refers to the four Kitteys in the bottom right of the Cavern.
If you're having trouble solving the riddle, read onwards. But I encourage you to try to solve it yourself first. \ask{Read onwards}{Stop reading}
!if answer==1
 The upper Kittey statue represents the moon, hence the crescent moon button on it.
 The four Kittey statues at the bottom describe the movements of a kitten throughout the day. The starting point is the early morning.
 First, make note of which statue refers to which time of the day. Then, flip the switches on the morning, afternoon, and evening statues so that each switch points to the statue with the next part of the day.
 The statue that represents the late night talks about the ocean under the moon. The switch here should point upwards - directly at the upper statue that represents the moon.
 When the switches are arranged correctly, pressing the crescent moon button will open the tomb entrance! Isn't that a great puzzle?!
!fi"
hint_script[hints.RareShop]
=@"There's apparently a shop that sells rare mushrooms in the world. It turns out it's actually in this Cavern of Ancients.
Look for a person who seems to be impossible to reach. Then search the room underneath for a secret passage leading upwards.
The rare mushrooms are expensive though, and a different mushroom is sold each day of the week, so if the shop doesn't have the right mushroom, you might have to wait a while."
hint_script[hints.GamblersHeart]
=@"A gambler who threw away everything except the dice they bet their life on.
If you throw all three dice at once, and they each land on a 6, something amazing might happen.
But like the gambler says, there's only a 1 in 216 chance of this happening. Is it worth the trouble?
If you can find an item somewhere that increases your luck, maybe it will be easier. Such an item exists in the Cherry Blossom Hills."
hint_script[hints.StormyDemeanor]
=@"In the upper left sky of the Cherry Blossom Hills, there's an angry little thundercloud. Unfortunately, this cloud happens to have one of the legendary Hands of the Clock. How can you convince her to trade it?
Well, she's very vain, and happens to love items that look like her. Something like a statue of herself would be perfect.
You probably can't find such a statue, but in the Canyon Cliffs, you can find an item with a similar shape and colour."
var sparkleshroom = @"SparkleShrooms can be found in the Mushroom Zone to the left of the entrance to the Hills. It's a little awkward to get up there, but you can hop on an item, or use a Bridget's Ball to create a bridge. You'll also need a key to enter."
if get_rare_shroom(rare_shroom_type.float) == oSparkleShroom {
	var sparkleshroom = 
	@"SparkleShrooms grow in a rather strange area. There is a Mushroom Zone hidden inside the Clock Tower! You'll need a key to enter the zone, and it might be hard to reach without certain Hands of the Clock."
} else
if get_rare_shroom(rare_shroom_type.kitty) == oSparkleShroom {
	var sparkleshroom = 
	@"SparkleShrooms grow in the Mushroom Zone in the lower right area of the Cherry Blossom Hills, which is hidden through a secret passage. You also need a key to enter it."
} else
if get_rare_shroom(rare_shroom_type.poison) == oSparkleShroom {
	var sparkleshroom = 
	@"SparkleShrooms grow in the lower right Mushroom Zone of the Cavern of Ancients. It's locked though, so bring a key."
} else
if get_rare_shroom(rare_shroom_type.spring) == oSparkleShroom {
	var sparkleshroom = 
	@"SparkleShrooms grow in a Mushroom Zone in the lower left part of the Canyon Cliffs. The zone is hidden behind a secret passage though, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.speeder) == oSparkleShroom {
	var sparkleshroom = 
	@"SparkleShrooms grow in a somewhat obscure Mushroom Zone, high up in the upper right of the Cavern of Ancients. It's hard to reach, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.rainbow) == oSparkleShroom {
	var sparkleshroom = 
	@"SparkleShrooms grow in the difficult-to-reach Mushroom Zone high and far up in the sky of the Canyon Cliffs. You need a key to enter the zone."
} 
hint_script[hints.LegendaryTree]
=@"The Legendary Tree is a golden tree that grows at the peak of the Cherry Blossom Hills. It's said that if you confess your love to someone under this tree, you'll both be blessed with a wonderful life.
It is also said that there is a mystical connection between this tree and the SparkleShroom, a rare golden mushroom native to the Hills.
"+sparkleshroom+@"
Once you have the SparkleShroom, stand under the Legendary Tree, and maybe a heart-pounding event will happen."
var floatshroom = @"This is found in a rather strange location. The Cylvey Clock Tower actually contains a Mushroom Zone within it, and the FloatShroom is found there.
You'll probably need one of these three Hands of the Clock to reach it: the Hand of Storms, the Hand of Sky, or the Hand of Thunder. You might be able to find a way to get to it without a Hand though. You'll also need a key to enter the zone."
if get_rare_shroom(rare_shroom_type.sparkle) == oFloatShroom {
	var floatshroom = 
	@"This is found in the Mushroom Zone on the left side of the Cherry Blossom Hills. You will need a key to enter."
} else
if get_rare_shroom(rare_shroom_type.kitty) == oFloatShroom {
	var floatshroom = 
	@"This is found in the Mushroom Zone in the lower right area of the Cherry Blossom Hills, which is hidden through a secret passage. You also need a key to enter it."
} else
if get_rare_shroom(rare_shroom_type.poison) == oFloatShroom {
	var floatshroom = 
	@"This is found in the lower right Mushroom Zone of the Cavern of Ancients. It's locked though, so bring a key."
} else
if get_rare_shroom(rare_shroom_type.spring) == oFloatShroom {
	var floatshroom = 
	@"This is found in a Mushroom Zone in the lower left part of the Canyon Cliffs. The zone is hidden behind a secret passage though, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.speeder) == oFloatShroom {
	var floatshroom = 
	@"This is found in a somewhat obscure Mushroom Zone, high up in the upper right of the Cavern of Ancients. It's hard to reach, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.rainbow) == oFloatShroom {
	var floatshroom = 
	@"This is found in the difficult-to-reach Mushroom Zone high and far up in the sky of the Canyon Cliffs. You need a key to enter the zone."
} 
hint_script[hints.FloatShroom]
=@"The Laboratorey scientist requires a FloatShroom for some of their inventions. 
You might have seen other people who want this rare floating mushroom. It can really make you float more if you have them.
"+floatshroom+@"
Could there perhaps be another way to get these floating shrooms? I wonder."
hint_script[hints.ShadyIndividual]
=@"There's a strange person in the bottom left area of the Cherry Blossom Hills. They won't talk to you unless you have a password.
If you simply keep talking to them, you might be able activate the Forgot your Password feature and see the security questions.
The password is a little strange though. Instead of an actual word, the password consists of three specific items, which you must have in your bag.
If you can't figure it out, don't worry about it. This person doesn't have anything that's needed to complete your quest to restore the flow of time."
var kittyshroom = @"It turns out that there's actually a secret passage right by her shop. It leads to a Mushroom Zone where KitteyShrooms live, so you can find some for yourself. You'll need a key to enter the zone though."
if get_rare_shroom(rare_shroom_type.sparkle) == oKittyShroom {
	var kittyshroom = 
	@"They're actually right nearby, in the Mushroom Zone on the left side of the Cherry Blossom Hills. You will need a key to enter."
} else
if get_rare_shroom(rare_shroom_type.float) == oKittyShroom {
	var kittyshroom = 
	@"Such Kitteys are found in a strange location. They are in a Mushroom Zone inside the bowels of the Cylvey Clock Tower.
	You will need a key to enter, and it might be hard to reach if you don't have certain Hands of the Clock."
} else
if get_rare_shroom(rare_shroom_type.poison) == oKittyShroom {
	var kittyshroom = 
	@"Kitteys can be found in the lower right Mushroom Zone of the Cavern of Ancients. It's locked though, so bring a key."
} else
if get_rare_shroom(rare_shroom_type.spring) == oKittyShroom {
	var kittyshroom = 
	@"Kitteys like to roam in a Mushroom Zone in the lower left part of the Canyon Cliffs. The zone is hidden behind a secret passage though, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.speeder) == oKittyShroom {
	var kittyshroom = 
	@"Kitteys are typically found in a rather unusual Mushroom Zone that is in the upper right of the Cavern of Ancients. It's a little tricky to get there, and requires a key to enter."
} else
if get_rare_shroom(rare_shroom_type.rainbow) == oKittyShroom {
	var kittyshroom = 
	@"Kitteys are known to live high in the sky in  a Mushroom Zone at the top of the Canyon Cliffs. It's somewhat hard to get there, and you need a key to enter the zone."
} 
hint_script[hints.OverpricedKitteys]
=@"There's someone in the bottom right areas of the Cherry Blossom Hills who sells KitteyShrooms. However, they're absurdly expensive, so I wouldn't recommend buying them.
She claims her shop is the only place in the world where you can find KitteyShrooms, but that can't be true. Where does she source her KitteyShrooms from then?
"+kittyshroom;


image_speed = 0;