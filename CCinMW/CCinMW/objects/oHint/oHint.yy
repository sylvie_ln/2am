{
    "id": "a2bd5d15-a194-40b1-a80d-aab1d032bbbc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHint",
    "eventList": [
        {
            "id": "112dfefd-3767-4791-801c-2761749fe00a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2bd5d15-a194-40b1-a80d-aab1d032bbbc"
        },
        {
            "id": "91bc8cd6-e88a-4eec-b142-8634f3cd808e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a2bd5d15-a194-40b1-a80d-aab1d032bbbc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
    "visible": true
}