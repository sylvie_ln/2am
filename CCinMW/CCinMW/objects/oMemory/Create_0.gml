event_inherited();

cutey = false;
name = "CrystalMemory"

image_speed = 1;
dead = false;

act1 = "Touch"
act2 = "Gaze"
if bag_has_item(oMemoryDisk) {
	act2 = "Record";	
}
memory_id = "";
base_script = 
@"From a distant time,\nsomeone's memory."
script = base_script;

gaze = "Meow"

if global.memory_particles == -1 {
	// color, dir, spd
	global.memory_particles = [
	ds_list_create(),
	ds_list_create(),
	ds_list_create(),
	];
	var hue = 0;
	var dir = 0;
	var spd = 1/13;
	repeat(21) {
		ds_list_add(global.memory_particles[0],make_color_hsl_240(hue,239,150));
		hue += 240 div 21;
	}
	repeat(36) {
		ds_list_add(global.memory_particles[1],dir);
		dir += 360 div 36;
	}
	repeat(13) {
		ds_list_add(global.memory_particles[2],spd);
		spd += 1/13;
	}
}