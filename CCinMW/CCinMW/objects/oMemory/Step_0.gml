
event_inherited();

if alarm[0] >= 0 or alarm[1] >= 0 {
	var color = global.memory_particles[0];
	var dir = global.memory_particles[1];
	var spd = global.memory_particles[2];
	var count = 0;
	var fluff = 1+(alarm[0] div (global.default_room_speed div 2));
	if alarm[1] >= 0 {
		var fluff = 4;	
	}
	repeat(fluff) {
		if alarm[0] >= 0 {
			with oChar {
				if air_base < max_air {
					//air_base+=2.31;
					air_base+=1;
					if air_base >= max_air {
						air_base = max_air;	
					}
					air = air_base + air_reserves;
				}
			}
		}
		var time = max(alarm[0],alarm[1])+count;
		var green = alarm[1] >= 0;
		with instance_create_depth(x,y,depth-1,oHeartParticle) {
			if green {
				image_blend = c_lime;
			} else {
				image_blend = color[|time mod ds_list_size(color)];
			}
			direction = dir[|time mod ds_list_size(dir)];
			acc = spd[|time mod ds_list_size(spd)];
			speed = (1/acc)/2
		}
		count+=17;
	}
}

if !talking {
	if script == gaze {
		script = old_script;
		event_user(0);
	}
}
var is_dead = ds_map_exists(global.memory_dead,memory_id);
if is_dead and bag_has_item(oMemoryDisk) {
	var mem_ii = bag_item_ii(oMemoryDisk);
	var mem_map = global.memory_disks[|mem_ii];
	var mem_pair = mem_map[?oRoomGen.area];
	if mem_pair[|0] == oRoomGen.xp and mem_pair[|1] == oRoomGen.yp {
		sprite_index = sMemoryRevived;
		act2 = "Warp";	
		gaze = @"You have recorded this spot on your Memory Disk. If you wish, you may use the power to warp out of this Mushroom Zone. \ask{Yes}{No}
		!if answer==1
		 !user 4
		!fi"
	} else if mem_pair[|0] == oRoomGen.startx and mem_pair[|1] == oRoomGen.starty {
		act2 = "Record";	
		gaze = @"Will you record this spot on your Memory Disk?\nYou can only record one spot per zone, and it cannot be overwritten or reset. \ask{Yes}{No}
		!if answer==1
		 !user 3
		!fi"
	} else {
		act2 = "Complain";
		gaze = @"You cannot use your Memory Disk here. It already has data for another location in the Mushroom Zone. Do you want to complain about this? \ask{Yes}{No} 
		!if answer==1
		  You stomp around and yell, but nobody hears your pathetic cries.
		!elif 1
		  You decide to accept your fate.
		!fi";
	}
} else {
	if is_dead {
		sprite_index = sMemoryDead;	
	} else {
		sprite_index = sMemory;	
	}
	act2 = "Gaze";
	gaze = "It's captivating.";
}
