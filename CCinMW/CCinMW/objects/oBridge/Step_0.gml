if on {
	sprite_index = sBridgeOn;
	upblocker=true;
	image_alpha = 1;	
} else {
	sprite_index = sBridgeOff;
	upblocker=false;
	if bag_has_item(oBridgeOrb) or instance_exists(oBridgeOrb) {
		image_alpha = (3+2*sin(2*pi*(current_time/3000)))/10;
	} else {
		image_alpha = 0;	
	}
}