/*
[Normal End]
Time has begun to pass once more. But something feels wrong....
Happy days end abruptly, and sadness seems to last an eternity.

From somewhere, a girl cries out for help.
(Light) Her light is extinguished.
(Darkness) Darkness engulfs her heart.
(Lies) Her lie spirals out of control.
(Tears) Tears stream down her twisted face.
(Dreams) Her broken dreams lie forgotten.
(Flames) Her flames of jealousy burn wildly.
(Leafs) Her body is crumpled like a leaf.
(Earth) The earth under her feet crumbles.
(Moon) The moon ceases to guide her.
(Storms) A furious storm beats down on her.
(Sky) A vast lonely sky surrounds her.
(Thunder) The crashing thunder terrifies her.

Is this just the cruel nature of time?
Or did you leave something unfinished?
Either way, this is the end of your story.


[True End]
Time has begun to pass once more.
And so, everyone's carefree, gentle days return.

A girl stands nervously at the gate between worlds.
She thinks about turning away, but .
Slowly, she raises a hand.

From somewhere, you hear a knock.
This is the beginning of your story.

CLOCKWORK CALAMITY in MUSHROOM WORLD
*/