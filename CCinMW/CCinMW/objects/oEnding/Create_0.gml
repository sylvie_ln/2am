phase = 0;
factor = 0;
spd = 0.1;
acc = 0.1;
u = 1000000;
time = 0;
audio_stop_sound(sndTowerPeakRumble);
audio_stop_sound(sndTowerPeakBellToll);
audio_stop_sound(sndTowerPeakTick1);
audio_stop_sound(sndTowerPeakTick2);
layer_background_blend(layer_background_get_id("Background"),global.bg_color);

if bag_has_item(oBunneyAmulet) {
	sprite_index = sBunneyF;
} else {
	sprite_index = sCharF;	
}
thrown = false;

all_hands = 
global.flag_door_darkness
and global.flag_door_dreams
and global.flag_door_earth
and global.flag_door_flames
and global.flag_door_leafs
and global.flag_door_lies
and global.flag_door_light
and global.flag_door_moon
and global.flag_door_sky
and global.flag_door_storms
and global.flag_door_tears
and global.flag_door_thunder;
//all_hands = true;
beat = 2000000;
var i = 0;
t[i++] = 10750000;
t[i++] = 12575000;
t[i++] = 16000000; // Citey

npcfirst = i;
for(var k=0; k<8; k++) {
	npc[i,k] = t[i-1]+(k*beat);
}
npctext[i] = [
[
@"A Beautiful Rainbow
* Rainbis",sRainbis],
[
@"Wants a Taco
* Flaire",sFlaire],
[
@"Shopping District
* Uncomus, Lockme, Uuella, & Stocs",sUncomus, sLockme, sUuella, sStocs],
[
@"Soft Soft Soft Kittey
* Creamy",sCreamy],
[
@"Red Clip Hater
* Pipi",sRedPaperclip, sRedPaperclip, sPipi, sRedPaperclip, sRedPaperclip],
[
@"Creepy Fellow
* Galfio",sGalfio, sGreenShard, sBlueShard, sRedShard],
[
@"Legendary Bagsmith
* Beleve",sBeleve, sTallBagUpgrade, sWideBagUpgrade, sLargeBagUpgrade],
[
@"The Hint Team
* Byclock, Cherie, Cavey, & Canyetta", sByclock, sCherie, sCavey, sCanyetta],
];
t[i++] = 24000000; 
t[i++] = 32000000; // Cherry
for(var k=0; k<8; k++) {
	npc[i,k] = t[i-1]+(k*beat);
}
npctext[i] = [
[
@"Keeper Of Bunneys
* Sweets",sMiniBunney, sBunneyPin, sBunneySlippers, sSweets, sBunneyAmulet, sMagicCarrot, sBunneyEars],
[
@"Guardian Of Area
* Arianne",sArianne],
[
@"Buy A House
* Lamia",sLamia, sHouse],
[
@"Perfect Couple?
* Katey & Plunch",sKatey, sSylviePlushey, sPlunch],
[
@"The Evil Laboratorey
* Laborie & Searchey", sLaborie, sFloppy, sFishCar, sSearchey],
[
@"Creatures Of The Sky
* Chicen & Stormy", sChicen, sStormy],
[
@"Hope In My Heart
* Sunny & Karie", sSunny, sLocket, sLetter, sKarie],
[
@"The Ultimate Hacker
* Geniux", sGeniux],
];
t[i++] = 40000000;
t[i++] = 48000000; // Canyon
for(var k=0; k<8; k++) {
	npc[i,k] = t[i-1]+(k*beat);
}
npctext[i] = [
[
@"Treasure Adventure
* Chestor & Tressa",sChestor,sBridgeOrb,sTressa,sLadderOrb],
[
@"Strawberry Shoes
* Crisis",sCrisis,sPegasusBoots,sClimbingCleats,sHoverHeels,s100TonShoes,sSpringSneakers],
[
@"Wonderful Melancholy
* Wirte & Junky",sWorthlessShroom,sWirte,sWorthlessShroom,[sJunkParts,0],sJunky,[sJunkParts,2]],
[
@"Mine Some Diamonds
* Mineria",sMineria,sMinecraft],
[
@"Rock Rock On
* Rockout",sBoringRock,sBoringRock,sRockout,sBoringRock],
[
@"Great Geologist
* Jealousy",sJealousy,sUniqueRock],
[
@"On The Road Again
* Truckster ",sTruckster,sLargeTruck],
[
@"I Love My Wife!
* Trouble & Strife",sTroubleAndStrife],
];
t[i++] = 56000000;
t[i++] = 64000000; // Cavern

for(var k=0; k<8; k++) {
	npc[i,k] = t[i-1]+(k*beat);
}
npctext[i] = [
[
@"Lovely Gifts
* Gooney",sGooney,sGiftShopCoffeeMug,sGiftShopKitteyPlush,sGiftShopPostcard,sGiftShopSnowGlobe,sGiftShopCavernRock,sGiftShopShirt],
[
@"The Wizardest Cars
* Wizaron",sWizardMobile,sWizaron,sWizardMobile],
[
@"Searching For The Tomb
* Zupzel & Country",sZupzel,sCountry],
[
@"Extreme Speed
* Slash",sSlash],
[
@"Followers Of Eivlys
* Bunnisse, Archaea, & Graal ",sBunnisse,sArchaea,sGraal],
[
@"Curator Vs. Thief
* Queen & Alicia ",sQueen, sTrendyTote, sAlicia],
[
@"Gamble My Life
* Lady",sLady,[sDice,10],[sDice,10],[sDice,10]],
[
@"A Rare Pair
* Bella & Mococo",sBella,sMushroomQuesoGrande,sMococo],
];
t[i++] = 72000000;
t[i++] = 80000000; // Mushzone
for(var k=0; k<8; k++) {
	npc[i,k] = t[i-1]+(k*beat);
}
npctext[i] = [
[
@"Common & Uncommon
* Basic, Tall, Wide, Worthless & Tiny",
[sBasicShroom,0],[sTallShroom,1],[sWideShroom,2],sWorthlessShroom,[sTinyShroom,4]],
[
@"So Colorful
* Rainbow Shroom",sRainbowShroom],
[
@"So Beautiful
* Sparkle Shroom",sSparkleShroom],
[
@"So Springey
* Spring Shroom",sSpringShroom],
[
@"So Floatey
* Float Shroom",sFloatShroom],
[
@"So Speedey
* Speed Shroom",sSpeedShroom],
[
@"So Kitteyish
* Kittey Shroom",[sKittyShroom,6]],
[
@"So Deadley
* Poison Shroom",sPoisonShroom],
];
t[i++] = 88000000;
t[i++] = 96000000; // Tower
for(var k=0; k<8; k++) {
	npc[i,k] = t[i-1]+(k*beat);
}
npctext[i] = [
[
@"Sealed In The Tower
* Mooncry",sMooncry],
[
@"Burning with Sadness
* Hands of Tears & Flames",[sHands,hand.tears],[sHands,hand.flames]],
[
@"The Natural World
* Hands of Leafs & Earth",[sHands,hand.leafs],[sHands,hand.earth]],
[
@"A Beautiful Sight
* Hands of Sky & Moon",[sHands,hand.sky],[sHands,hand.moon]],
[
@"Struck From Above
* Hands of Storms & Thunder",[sHands,hand.storms],[sHands,hand.thunder]],
[
@"A Human's Heart
* Hands of Lies & Dreams",[sHands,hand.lies],[sHands,hand.dreams]],
[
@"Eternal Struggle
* Hands of Light & Darkness",[sHands,hand.light],[sHands,hand.darkness],],
[
@"Your Representative
* Princess",[sCharS,0,-1],sCursor],
];
t[i++] = 104000000;
t[i++] = 112000000; // Finale
t[i++] = 120000000;
npclast = i;
t[i++] = 128000000;
t_total = i;
time_total = t[t_total-1];
i = 0;
death = 128000000 + (u*3);
text[i++] = [
"Credits",
""
];
text[i++] = [
"Gamedesign Princess",
"Sylvie"
];
text[i++] = [
"Gamedesign Consultant",
"Hubol"
]
text[i++] = [
"Programming Lead",
"Sylvie"
]
text[i++] = [
"Programming Assistant",
"Hubol"
]
text[i++] = [
"Characters and Story",
"by Sylvie"
]
text[i++] = [
"Great Ideas",
"by Hubol"
]
text[i++] = [
"Music Arrange & Vocals",
"by Hubol"
]
text[i++] = [
"Two Secret Songs",
"by Sylvie"
]
text[i++] = [
"Lovely Lyrics",
"by Sylvie"
]
text[i++] = [
"Lyrics Helper",
"Hubol"
]
text[i++] = [
"All the Sound Effects",
"by Hubol"
]
text[i++] = [
"All the Graphics",
"by Sylvie"
]
if all_hands {
	text[i++] = [
	"",
	"She spent a lot of time creating this world.\nThank you for your gentle exploration."
	]
	text[i++] = [
	"",
	"Maybe you'll meet again in some far away time.\nUntil then, please keep her in your memory."
	]
} else {
	text[i++] = [
	"",
	"Though the flow of time has been restored,\nit is a little stilted and uneven."
	]
	text[i++] = [
	"",
	"Perhaps there's more to discover in this world.\nBut for now, have a good rest."
	]
}
text_total = i;
text_pos = 0;

stop = 0;
char_pos = -16;
char_start = 32;
char_end = 91; //2*(room_height div 3)-8;

eivlys = false;
eivlys_offset = 4;
eivlys_offset2 = 4;
eivlys_start_y = room_height + 32;
eivlys_y = eivlys_start_y;
dancing = false;