if phase == 1 {
	draw_set_color(make_color_rgb(0xEE,0x1C,0x24));
	var vpos = 99;	
	draw_line(-1,vpos,factor,vpos);
	draw_line(room_width-1-factor,vpos,room_width-1,vpos);
} if phase > 1 {
	if !dancing { 
		sprite_index = sCharR; 
		var cx = room_width div 5;
		var cy = char_end;
		if !thrown { 
			if bag_has_item(oBunneyEars) {
				with instance_create_depth(cx,cy,depth+1,oBunneyEars) {
					hv = lengthdir_x(6,90+60);
					vv = lengthdir_y(6,90+60);
					solid_boundary = false;
				}
			}
			if bag_has_item(oMiniBunney) {
				with instance_create_depth(cx,cy,depth+1,oMiniBunney) {
					hv = lengthdir_x(6,90+30);
					vv = lengthdir_y(6,90+30);
					solid_boundary = false;
				}
			}
			if bag_has_item(oChickenWings) {
				with instance_create_depth(cx,cy,depth+1,oChickenWings) {
					hv = lengthdir_x(6,90-30);
					vv = lengthdir_y(6,90-30);
					solid_boundary = false;
				}
			}
			if bag_has_item(oBunneyAmulet) {
				with instance_create_depth(cx,cy,depth+1,oBunneyAmulet) {
					hv = lengthdir_x(6,90-60);
					vv = lengthdir_y(6,90-60);
					solid_boundary = false;
				}
			}
			thrown = true;
		}
	}
	draw_set_color(c_white);
	if text_pos < text_total {
		var a = text[text_pos];
		var hpos = 8
		var vpos = 2*(room_height div 3)+8;
		var dist = t[6] - t[5];
		var range = (0.25 * dist);
		var lerpo = lerp(room_height-vpos-4,-20,power(min((dist),2*(time-t[phase-1]))/(dist),1));
		if phase == 2 { lerpo = 0; }
		else if t[phase]-time < range {
			draw_set_alpha(sqr((t[phase]-time)/range));
			if time > time_total {
				draw_set_alpha(0);	
			}
		}
		draw_set_font(global.sylvie_font_bold);
		if lerpo == 0 {
			draw_text_centered(room_width div 4,vpos+lerpo,a[0]);
		} else{
			draw_text(hpos,vpos+lerpo,a[0]);
		}
		draw_set_font(global.sylvie_font);
		if text_pos >= text_total-2 {
			draw_text(8,room_height-string_height(a[1])-8,a[1]);
		} else {
			draw_text(hpos,vpos+16+lerpo,a[1]);
		}
		draw_set_alpha(1);	
		//draw_set_valign(fa_top);
	}
}

var npcphase = (((phase-1) div 2)*2);
if phase >= npcfirst and phase < npclast-1 {
	var nt = npctext[npcphase+1];
	var npos = clamp((time-t[npcphase]) div beat,0,7);
	var ndata = nt[npos];
	var ntext = ndata[0];
	draw_set_font(global.neco_font);
	//draw_text(0,0,string(time-t[npcphase]));
	//draw_text_centered(3*(room_width div 4),3*(room_height div 4),ntext);
	var numspr = array_length_1d(ndata)-1;
	var widthspr =0;
	var psi = sprite_index;
	var pii = image_index;
	var pxy = [x,y];
	var foff = 0;
	for(var i=1; i<=numspr; i++) {
		if is_array(ndata[i]) {
			var ar = ndata[i];
			sprite_index = ar[0];
			image_index = ar[1];
		} else {
			sprite_index = ndata[i];
		}
			if i == 1 { foff = sprite_xoffset; }
		widthspr += sprite_width; //bbox_right-bbox_left+1;
	}
	draw_set_halign(fa_right);
	draw_text(room_width-8,3*(room_height div 4),ntext);
	draw_set_halign(fa_left);
	var spry = char_end;
	x = round((0.777)*room_width);
	var skip = 1;
	x -= (widthspr - (skip*numspr)) div 2;
	// += foff;
	for(var i=1; i<=numspr; i++) {
		if is_array(ndata[i]) {
			var ar = ndata[i];
			sprite_index = ar[0];
			image_index = ar[1];
			if array_length_1d(ar) > 2 {
				image_xscale = ar[2];
			} else {
				image_xscale = 1;	
			}
		} else {
			sprite_index = ndata[i];
			image_index = 0;
			image_xscale = 1;	
		}
		y = spry+8-(bbox_bottom-y+1);
		x += sprite_xoffset;
		draw_self();
		x += (sprite_width*image_xscale)-sprite_xoffset+skip; //bbox_right-bbox_left+1+skip;
	}
	sprite_index = psi;
	image_index = pii;
	image_xscale = 1;
	x = pxy[0];
	y = pxy[1];
}

var cx = room_width div 5;
if !dancing {
	if abs(char_pos-char_start)<0.4 {
		if stop == 0 {
			stop = time;	
		}
		var cy = 
				lerp(char_start,char_end,min(time-stop,t[1]-stop)/(t[1]-stop));
		if phase <= 1 {
			if bag_has_item(oBunneyEars) {
			draw_sprite_ext(sBunneyEars,0,cx,cy-5-4,
			image_xscale,image_yscale,image_angle,image_blend,image_alpha);
			}
			if bag_has_item(oMiniBunney) {
				draw_sprite_ext(sMiniBunney,0,cx,cy-5-8,
				image_xscale,image_yscale,image_angle,image_blend,image_alpha);
			}
			if bag_has_item(oChickenWings) {
				draw_sprite_ext(sCharWings,image_index,cx,cy,
				image_xscale,image_yscale,image_angle,image_blend,image_alpha);
			}
		}
		draw_sprite(sprite_index,image_index,
				cx,
				cy);
	} else {
		char_pos += (char_start - char_pos) * 0.1;
		var cy = char_pos;
		
		if phase <= 1 {
			if bag_has_item(oBunneyEars) {
			draw_sprite_ext(sBunneyEars,0,cx,cy-5-4,
			image_xscale,image_yscale,image_angle,image_blend,image_alpha);
			}
			if bag_has_item(oMiniBunney) {
				draw_sprite_ext(sMiniBunney,0,cx,cy-5-8,
				image_xscale,image_yscale,image_angle,image_blend,image_alpha);
			}
			if bag_has_item(oChickenWings) {
				draw_sprite_ext(sCharWings,image_index,cx,cy,
				image_xscale,image_yscale,image_angle,image_blend,image_alpha);
			}
		}
		draw_sprite(sprite_index,image_index,
				cx,
				char_pos);
	}
}
if !all_hands { exit; }
if eivlys {
	var eivlys_final_y = char_end+eivlys_offset2-1;
	if abs(eivlys_y-eivlys_final_y)>0.4 {
		eivlys_y += (eivlys_final_y-eivlys_y) * 0.05;
		draw_sprite(sEivlysStill,image_index,cx+eivlys_offset,eivlys_y);
	} else {
		if !dancing {
			dancing = true;
			sprite_index = sEivlysDance;
		}
		draw_sprite(sprite_index,image_index,cx+eivlys_offset,eivlys_final_y);
	}
}