{
    "id": "345a1742-ef9a-4f7e-a687-5822c48acdb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBiggHanddss",
    "eventList": [
        {
            "id": "c4c2ee66-2733-4d52-a945-744342bef445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "345a1742-ef9a-4f7e-a687-5822c48acdb0"
        },
        {
            "id": "7db82128-faaf-4f09-af15-f5163addc48a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "345a1742-ef9a-4f7e-a687-5822c48acdb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
    "visible": true
}