{
    "id": "584e467d-fb08-4abd-aa0d-30ffa3d011d5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBoomBox",
    "eventList": [
        {
            "id": "4870a6e1-7006-43f6-8521-3d29a323109a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "584e467d-fb08-4abd-aa0d-30ffa3d011d5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98bcd1a7-d2c4-4f23-bdbc-86830e2c3a19",
    "visible": true
}