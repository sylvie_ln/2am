var xp = x+xo;
var yp = y+floor(image_index);
if bag_has_item(oBunneyEars) {
	draw_sprite_ext(sBunneyEars,0,xp,yp-5-4,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
if bag_has_item(oMiniBunney) {
	draw_sprite_ext(sMiniBunney,0,xp,yp-5-8,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
if bag_has_item(oChickenWings) {
	draw_sprite_ext(sCharWings,image_index,xp,yp,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
draw_sprite_ext(bag_has_item(oBunneyAmulet) ? sBunneyF : sCharF,image_index,x+xo,y+floor(image_index),1,1,image_angle,c_white,1);
draw_self();