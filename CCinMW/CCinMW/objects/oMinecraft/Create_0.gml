event_inherited();

count = 0;
paused = false;

if !global.mute_sfx {
	audio_sound_gain(sndMinecartMove,global.sfx_volume*get_gain(sndMinecartMove),0);
	move_sound = audio_play_sound(sndMinecartMove, 0, true);
} else {
	move_sound = noone;	
}

xo = 0;