{
    "id": "74453a93-8ea4-429c-8481-153ec039452f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBunneyKeeper",
    "eventList": [
        {
            "id": "06d9bb47-fc3c-4176-b7ae-abb3aed03e4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74453a93-8ea4-429c-8481-153ec039452f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "326bce32-36bc-4b0a-8ab8-c6d54defdf21",
    "visible": true
}