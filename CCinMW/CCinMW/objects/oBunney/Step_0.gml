///@description
if shake > 0 {
	shake -= 16/(global.default_room_speed/2);
	if shake <= 0{
		shake = 0;	
	}
}
if shake == 0 and !talking and script != old_script {
	script = old_script
	event_user(0);	
}

if paused { exit; }

var ong = collision_at(x,y+1);
if !ong {
	vv += grav;
}

if bag_has_item(oMagicCarrot) {
	target = oChar;
} else if bag_npc_has_item(oMagicCarrot,oBunneyKeeper) {
	target = oBunneyKeeper;
} else if instance_exists(oMagicCarrot) {
	target = oMagicCarrot;
} else {
	target = noone;	
}
var can_see = false;
with target {
	can_see = !outside_view();	
}
if can_see and instance_exists(target) {
	if abs(x-target.x) > 8 {
		acc = grav;	
		if acc > maxspd { acc = maxspd; }
	} else {
		acc = 0;
	}
	hv += sign(target.x-x)*acc;
	if hv > maxspd {
		hv = maxspd;
	}
} else {
	if ong and hv != spd {
		if hv == 0 { hv = sylvie_choose(-1,1); }
		hv = sign(hv)*spd;	
	}
}

if !move(hv,0) {
	hv = -hv;	
}

jmp = sqrt(2*grav*sylvie_choose(20,36,52));

if !move(vv,1) {
	if vv > 0 {
		if solid_at(x,y+1) or blocker_at(x,y+1) {
			vv = 0;
		} else {
			vv = -vv*bounce;	
			if abs(vv) < jmp {
				vv = -jmp;
			}
		}
	} else {
		vv = 0;	
	}
}

if sign(hv) != 0 {
	image_xscale = sign(hv);	
}

if (ong and !collision_at(x,y+1))
or (ong and collision_at(x+96*sign(hv),y)) {
	vv = -jmp;
}