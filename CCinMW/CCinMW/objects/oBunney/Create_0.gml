event_inherited();

damp = 0.6;
bounce = damp;
fric = 0.95;

jmp = sqrt(2*grav*sylvie_choose(20,36,52));
spd = 1+(sylvie_random(33,66)/100);
hv = sylvie_choose(spd,-spd);

solid_boundary = true;
very_solid_boundary = true;
paused = false;

depth = 5;

var seed = ystart*10000+xstart;
sylvie_set_seed(seed*241);
name = "Bunney"+string(seed);
//disabled_show_debug_message(name);

old_script = script;

act2 = "Fluff"

cutey = false;

waiting = false;
target = oBunneyKeeper;
acc = 0;
maxspd=2*spd;

image_blend = c_white;
var type = sylvie_choose(0,0,0,1);
if type == 0 {
	if sylvie_choose(true,false) {
		var l = 120;
		image_blend = merge_color(
		image_blend,
		sylvie_choose(
		make_color_hsl_240(10,239,l+10),
		make_color_hsl_240(20,239,l+10),
		make_color_hsl_240(40,239,l),
		make_color_hsl_240(80,239,l),
		make_color_hsl_240(120,239,l),
		make_color_hsl_240(150,239,l+10),
		make_color_hsl_240(180,239,l+10),
		make_color_hsl_240(190,239,l),
		make_color_hsl_240(200,239,l),
		make_color_hsl_240(220,239,l),
		),
		sylvie_choose(0,1/8,1/8,1/8));
	}
	var l = 210;
	color =
	sylvie_choose(
	make_color_hsl_240(10,239,l),
	make_color_hsl_240(20,239,l),
	make_color_hsl_240(40,239,l),
	make_color_hsl_240(80,239,l),
	make_color_hsl_240(120,239,l),
	make_color_hsl_240(150,239,l),
	make_color_hsl_240(180,239,l),
	make_color_hsl_240(190,239,l),
	make_color_hsl_240(200,239,l),
	make_color_hsl_240(220,239,l),
	);
} else if type == 1 {
	var l = 120;
	image_blend =
	sylvie_choose(
	make_color_hsl_240(10,239,l+10),
	make_color_hsl_240(20,239,l+10),
	make_color_hsl_240(40,239,l),
	make_color_hsl_240(80,239,l),
	make_color_hsl_240(120,239,l),
	make_color_hsl_240(150,239,l+10),
	make_color_hsl_240(180,239,l+20),
	make_color_hsl_240(190,239,l+20),
	make_color_hsl_240(200,239,l+10),
	make_color_hsl_240(220,239,l+10),
	);
	var hu = ((color_get_hue(image_blend)/256)*240);
	var hue = nmod(hu+sylvie_choose(-10,-5),240);
	var l = 110;
	color =
	make_color_hsl_240(hue,239,l);
}
overlay = 
sylvie_choose([c_white,0],[c_white,0],[c_ltgray,0.1],[c_gray,0.25],[c_dkgray,0.25],[c_black,0.5]);