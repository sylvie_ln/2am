{
    "id": "33a4187f-01b7-4d6e-b0c3-b59f352567ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDice",
    "eventList": [
        {
            "id": "22e8ba24-fcff-4acb-978a-6ae6c0e929f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33a4187f-01b7-4d6e-b0c3-b59f352567ce"
        },
        {
            "id": "22ae68aa-81b1-409f-9e2f-9d058f855e4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "33a4187f-01b7-4d6e-b0c3-b59f352567ce"
        },
        {
            "id": "6b03f5bf-f98f-4574-b910-5c78a582e15a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "33a4187f-01b7-4d6e-b0c3-b59f352567ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
    "visible": true
}