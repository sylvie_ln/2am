event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "The legendary rainbow shroom is here, it has such vivid colors.\nPassive Effect: Lovely Rainbow Trail"

damp = 0.9;
bounce = damp;
fric = 0.99;


image_speed = 0;