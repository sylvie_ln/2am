{
    "id": "9d45f9f2-8d8a-49c7-9736-049c31ed8a4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRainbowShroom",
    "eventList": [
        {
            "id": "c2244132-8050-47f5-9a36-44fbcb024911",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d45f9f2-8d8a-49c7-9736-049c31ed8a4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d620b6c-8cb0-4c10-8f85-3337483872c6",
    "visible": true
}