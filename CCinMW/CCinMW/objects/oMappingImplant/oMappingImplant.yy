{
    "id": "b64a7b19-720b-4e60-8d7d-634d970ee502",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMappingImplant",
    "eventList": [
        {
            "id": "a080ccc7-1145-4dae-a729-4397113db08a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b64a7b19-720b-4e60-8d7d-634d970ee502"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "da3fd45f-e9bc-47f6-890a-8047ffa138de",
    "visible": true
}