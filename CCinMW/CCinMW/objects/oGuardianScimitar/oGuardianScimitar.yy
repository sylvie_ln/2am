{
    "id": "885bdbac-836b-4bdf-b5fc-c7819e63b743",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGuardianScimitar",
    "eventList": [
        {
            "id": "163c4047-d056-4697-8f5b-186258fe703b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "885bdbac-836b-4bdf-b5fc-c7819e63b743"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "91641331-9363-49b5-bec3-8d6ac40de8f8",
    "visible": true
}