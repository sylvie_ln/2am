{
    "id": "006a22b9-c171-4a2e-a1eb-6dfa03d6cc74",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oOverlayNES",
    "eventList": [
        {
            "id": "eaf27d23-65c2-410b-976e-def42cf13509",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "006a22b9-c171-4a2e-a1eb-6dfa03d6cc74"
        },
        {
            "id": "4d0ec32d-0329-44c2-8a83-694102b073d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "006a22b9-c171-4a2e-a1eb-6dfa03d6cc74"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a3f1bc94-870e-4b24-a535-64ebcad231e6",
    "visible": true
}