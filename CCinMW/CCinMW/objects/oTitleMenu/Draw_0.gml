if pause_mode and subsub { 
	if oAudioMenu.active or oGraphicsMenu.active or oControlsMenu.active or oAssistMenu.active {
		exit; 
	}
}
draw_set_font(global.neco_font);
draw_set_color(c_white);
if !pause_mode {
	var txt = "Another world by sylvie and hubol."
	draw_text(room_width-4-string_width(txt),room_height-4-string_height(txt),txt);
}
var hpos = x;
var vpos = y;
//disabled_show_debug_message([menu,submenu]);

if menu == quit and subsub {
	vpos += 20;
}
var txt = header[submenu == 0 ? 0 : menu, min(1,submenu)];
draw_text(hpos,vpos,txt);
hpos += 10;
vpos += string_height(txt);
if submenu == 0 {
	var stop = menus;
	for(var i=0; i<stop; i++) {
		if i == menu {
			draw_text(hpos-10,vpos,">");
		}
		txt = option[i,0];
		draw_text_emph(hpos,vpos,txt);
		vpos += string_height(txt);
	}
} else {
	var stop = submenus[menu];
	for(var i=1; i<stop; i++) {
		if i == submenu {
			draw_text(hpos-10,vpos,">");
		}
		txt = option[menu,i];
		draw_text_emph(hpos,vpos,txt);
		vpos += string_height(txt);
	}
}
if menu == quit and subsub {
	draw_set_alpha(lerp(0,1,(fade_time-alarm[0])/fade_time));
	draw_set_color(c_black);
	draw_rectangle(0,0,room_width,room_height,false);	
	draw_set_alpha(1);
	draw_set_color(c_white);
}
if menu == play and submenu == 1 {
	var hpos = oFileDisplay.x;
	var vpos = oFileDisplay.y;
	var location = "???"
	switch(asset_get_index(global.current_room)) {
		case rmCylveyCitey: location = "Cylvey Citey"; break;
		case rmCylveyClockTower: location = "Cylvey Clock Tower"; break;
		case rmTowerTop: location = "Cylvey Clock Tower ~ Peak"; break;
		case rmCherryBlossomHills: location = "Cherry Blossom Hills"; break;
		case rmCanyonCliffs: location = "Canyon Cliffs"; break;
		case rmCavernOfAncients: location = "Cavern of Ancients"; break;
		case rmCoolMuseum: location = "Cool Museum"; break;
		case rmMushZone: 
			if ds_map_exists(global.mushzone_params,"name") {
				location = global.mushzone_params[?"name"]+" ~ Mushroom Zone";
			}
		break;
	}
	var u = 1000000;
	var time_seconds = floor(global.play_time_usec/u);
	var time_minutes = time_seconds div 60;
	var time_hours = time_minutes div 60;
	time_seconds = time_seconds mod 60;
	time_minutes = time_minutes mod 60;
	var time_minutes_str = (time_minutes <= 9 and time_hours > 0 ? "0" : "") + string(time_minutes);
	var time_seconds_str = (time_seconds <= 9 and time_minutes > 0 ? "0" : "") + string(time_seconds);
	var time_str = 
	 (time_hours > 0   ? string(time_hours)+"h" : "")
	+(time_minutes > 0 ? time_minutes_str+"m"   : "")
	+(time_seconds_str+"s");
	var txt = "Location: "+location+"\nPlaytime: "+time_str;
	draw_text(hpos,vpos,txt);
	hpos += 6;
	vpos += string_height(txt)+10;
	var skip = 14;
	draw_title_hand(sHands,hand.darkness,hpos,vpos,
	global.flag_hand_darkness,global.flag_door_darkness)
	hpos += skip;
	draw_title_hand(sHands,hand.dreams,hpos,vpos,
	global.flag_hand_dreams,global.flag_door_dreams)
	hpos += skip;
	draw_title_hand(sHands,hand.earth,hpos,vpos,
	global.flag_hand_earth,global.flag_door_earth)
	hpos += skip;
	draw_title_hand(sHands,hand.flames,hpos,vpos,
	global.flag_hand_flames,global.flag_door_flames)
	hpos += skip;
	draw_title_hand(sHands,hand.leafs,hpos,vpos,
	global.flag_hand_leafs,global.flag_door_leafs)
	hpos += skip;
	draw_title_hand(sHands,hand.lies,hpos,vpos,
	global.flag_hand_lies,global.flag_door_lies);
	hpos += skip;
	draw_title_hand(sHands,hand.light,hpos,vpos,
	global.flag_hand_light,global.flag_door_light);
	hpos += skip;
	draw_title_hand(sHands,hand.moon,hpos,vpos,
	global.flag_hand_moon,global.flag_door_moon);
	hpos += skip;
	draw_title_hand(sHands,hand.sky,hpos,vpos,
	global.flag_hand_sky,global.flag_door_sky);
	hpos += skip;
	draw_title_hand(sHands,hand.storms,hpos,vpos,
	global.flag_hand_storms,global.flag_door_storms)
	hpos += skip;
	draw_title_hand(sHands,hand.tears,hpos,vpos,
	global.flag_hand_tears,global.flag_door_tears)
	hpos += skip;
	draw_title_hand(sHands,hand.thunder,hpos,vpos,
	global.flag_hand_thunder,global.flag_door_thunder)
	hpos += skip;
}