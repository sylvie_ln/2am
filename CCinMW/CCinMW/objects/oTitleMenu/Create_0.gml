var i=0;
var j=0;
header[i,j] = "What is your wish?"
 play=i;
 option[i++,j++] = "|Enter| the world."
 header[i-1,j] = "Are you ready?"
  option[i-1,j++] = "Let's |begin|."
  enter_go_idx = [i-1,j-1];
  option[i-1,j++] = "Go |back|."
  enter_back_idx = [i-1,j-1];
 submenus[i-1] = j;
 var j=0;
 options=i;
 option[i++,j++] = "Configure some |options|."
 header[i-1,j] = "Which options interest you?"
  option[i-1,j++] = "|Audio| options."
  audio_idx = [i-1,j-1];
  option[i-1,j++] = "|Graphic| options."
  graphic_idx = [i-1,j-1];
  option[i-1,j++] = "|Control| options."
  control_idx = [i-1,j-1];
  option[i-1,j++] = "|Playful| options."
  playful_idx = [i-1,j-1];
  option[i-1,j++] = "I'm |done|."
  options_back_idx = [i-1,j-1];
 submenus[i-1] = j;
 var j=0;
 option[i++,j++] = "|Reset| or destroy things."
 header[i-1,j] = "Make your choice carefully."
  option[i-1,j++] = "Reset my |controls|."
  reset_control_idx = [i-1,j-1];
  option[i-1,j++] = "Reset other |options|."
  reset_option_idx = [i-1,j-1];
  option[i-1,j++] = "Reset the |world|."
  reset_world_idx = [i-1,j-1];
  option[i-1,j++] = "|Tear up my contract.|"
  reset_contract_idx = [i-1,j-1];
  option[i-1,j++] = "I'm |finished|."
  reset_back_idx = [i-1,j-1];
 submenus[i-1] = j;
 var j=0;
 new_world = i;
 option[i++,j++] = "Something |wonderful|."
 if global.eivlys_clear {
	header[i-1,j] = "Eivlys wants you to try\nvisiting another world."
    option[i-1,j++] = "|Sure|, let's take a look."
	new_world_idx = [i-1,j-1];
    option[i-1,j++] = "|Not right now.|"
	new_world_back_idx = [i-1,j-1];
 } else {
	header[i-1,j] = "This option is locked\nuntil you meet Eivlys."
    option[i-1,j++] = "|I don't care.|"
	new_world_idx = [i-1,j-1];
    option[i-1,j++] = "Okay, I'll |wait|."
	new_world_back_idx = [i-1,j-1];
 }
 submenus[i-1] = j;
 var j=0;
 quit = i;
 option[i++,j++] = "I'm |leaving|."
 header[i-1,j] = "I wonder if I'll ever see you again."
 submenus[i-1] = j;
menus=i;

menu=0;
submenu=0;
subsub = false;

pause_mode = false;

fade_time = global.default_room_speed*3;

			
			
//load_game();