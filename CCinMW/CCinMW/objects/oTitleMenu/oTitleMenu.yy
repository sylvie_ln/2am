{
    "id": "c93f5cab-ba2b-4c21-8593-79642a3ead26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitleMenu",
    "eventList": [
        {
            "id": "3d998565-eeed-439c-bd24-f0c75bfd7299",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c93f5cab-ba2b-4c21-8593-79642a3ead26"
        },
        {
            "id": "423e02f7-3a45-4284-be76-9cc9f9c7fe38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c93f5cab-ba2b-4c21-8593-79642a3ead26"
        },
        {
            "id": "b6a7e7a3-159a-4ae0-af1f-be4c8f0acf84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c93f5cab-ba2b-4c21-8593-79642a3ead26"
        },
        {
            "id": "901a756b-2802-4b72-b4e6-29d4a33fef94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c93f5cab-ba2b-4c21-8593-79642a3ead26"
        },
        {
            "id": "da7b7213-9c55-444d-bf93-5b57c020bb57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c93f5cab-ba2b-4c21-8593-79642a3ead26"
        },
        {
            "id": "950f410c-2f48-4d7d-a112-acfdcae6038c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c93f5cab-ba2b-4c21-8593-79642a3ead26"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}