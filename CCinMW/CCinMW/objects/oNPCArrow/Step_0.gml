if place_meeting(x,y,oChar) {
	if !active {
		with oNPC {
			active = false;	
		}
	}
	active = true;
} else if active and active_timer <= 0 {
	active_timer = active_time;	
}
if active_timer > 0 {
	active_timer--;
	if active_timer == 0 {
		active = false;	
	}
}
if instance_exists(oShifter) and oShifter.shifting {
	active = false;	
}