if surface_exists(stencil) { surface_free(stencil); }
if surface_exists(surfy) { surface_free(surfy); }
stencil = -1;
surfy = -1;