if shift_dir != 0 {
	shifting = true;
	switch(shift_dir) {
		case edir.up: 
			shift_time = global.rheight div shift_spd; 
		break;
		case edir.down:
			shift_time = global.rheight div shift_spd; 
		break;
		case edir.left:
			shift_time = global.rwidth div shift_spd;
		break;
		case edir.right:
			shift_time = global.rwidth div shift_spd;
		break;
	}
	switch(shift_dir) {
		case edir.up: 
			yoff -= global.rheight;
		break;
		case edir.down:
			yoff += global.rheight; 
		break;
		case edir.left:
			xoff -= global.rwidth;
		break;
		case edir.right:
			xoff += global.rwidth;
		break;
	}
	if instance_exists(oRoomGen) {	
		xoff = nmod(xoff,global.rwidth*oRoomGen.wwidth);
		yoff = nmod(yoff,global.rheight*oRoomGen.wheight);
	}
}