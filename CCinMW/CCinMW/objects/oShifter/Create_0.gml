shifting = false;
shift_dir = 0;
shift_spd = 16;
shift_time = 0;
area = "";

if room != rmMushZone {
	instance_create_depth(0,0,0,oOriginPoint);	
}

if !instance_exists(oRoomGen) {
 sylvie_ds_tracker_print();	
}

global.room_started = false;

depth = -999;

layers = ds_list_sylvie_create();

h=0;
s=239;
l=120;
enter = false;
//layer_background_blend(layer_background_get_id("Background"),make_color_hsl_240(h,s,l));

if bag_has_item(oMemoryDisk) and room == rmMushZone {
	var mem_ii = bag_item_ii(oMemoryDisk);
	var mem_map = global.memory_disks[|mem_ii];
	var aname = global.mushzone_params[? "name"]
	var mem_pair = mem_map[?aname];
	global.warp_x = mem_pair[|0];
	global.warp_y = mem_pair[|1];
}

xoff = 0;
yoff = 0;
if !is_undefined(global.warp_x) {
	xoff = global.warp_x*global.rwidth;
	yoff = global.warp_y*global.rheight;
	xoff = nmod(xoff,global.rwidth*oRoomGen.wwidth);
	yoff = nmod(yoff,global.rheight*oRoomGen.wheight);
}
