/*
if keyboard_check_pressed(vk_home) {
	enter = true;
	keyboard_string="";
}
if enter {
	area=keyboard_string
	if keyboard_check_pressed(vk_enter) {
		enter = false;
	}
} else {
	if keyboard_check(ord("1")) {
		h++;	
	}
	if keyboard_check(ord("2")) {
		s++;	
	}
	if keyboard_check(ord("3")) {
		l++;	
	}
	if keyboard_check(ord("Q")) {
		h--;	
	}
	if keyboard_check(ord("W")) {
		s--;	
	}
	if keyboard_check(ord("E")) {
		l--;	
	}
}
layer_background_blend(layer_background_get_id("Background"),make_color_hsl_240(h,s,l));
*/

if shifting {
	with oThingy {
		switch(other.shift_dir) {
			case edir.up:
				y += other.shift_spd;
			break;
			case edir.down: 
				y -= other.shift_spd;
			break;
			case edir.left: 
				x += other.shift_spd; 
			break;
			case edir.right: 
				x -= other.shift_spd;
			break;
		}
		if instance_exists(oRoomGen) and object_is_ancestor(object_index,oShroom) 
		and not 
		(object_index == oFlarekitten
		or object_is_ancestor(object_index,oHand)){
			x = nmod(x,global.rwidth*oRoomGen.wwidth);
			y = nmod(y,global.rheight*oRoomGen.wheight);
		}
	}
	switch(shift_dir) {
		case edir.up:
			y += shift_spd;
			shift_layers(0,shift_spd);
		break;
		case edir.down: 
			y -= shift_spd;
			shift_layers(0,-shift_spd);
		break;
		case edir.left: 
			x += shift_spd; 
			shift_layers(shift_spd,0);
		break;
		case edir.right: 
			x -= shift_spd;
			shift_layers(-shift_spd,0);
		break;
	}
	shift_time--;
	if shift_time <= 0 {
		shifting = false;	
		if instance_exists(oRoomGen) {
			//disabled_show_debug_message("Finishing shift to "+room_key(oRoomGen.xp,oRoomGen.yp));
			with oThingy {
				if object_is_ancestor(object_index,oShroom) {
					//disabled_show_debug_message(object_get_name(object_index)+" "+string(id)+" "+pos_key(x,y)+" gen="+string(generated)+" sav="+string(saved));
				}
				if !generated { continue; }
				if bbox_right < 0 or bbox_left >= global.rwidth or bbox_bottom < 0 or bbox_top >= global.rheight {
					if not
					(object_index == oChar
					or object_index == oFlarekitten
					or object_is_ancestor(object_index,oHand)) {
						instance_destroy();
						continue;
					}
				}		
				xt = x;
				yt = y;
			}
			with oRoomGen {
				event_user(15);
			}
			with oChar {
				with instance_place(x,y,oBlonck) {
					instance_destroy();	
				}
			}
		}
		//disabled_show_debug_message("Done shift");
	}
	
}

play_room_shift_sound(shifting);