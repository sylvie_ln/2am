draw_set_color(c_navy);
if room = rmCylveyClockTower or room == rmTowerTop {
	draw_set_color(c_black);	
}
draw_rectangle(camera_get_view_x(view_camera[0]),camera_get_view_y(view_camera[0])+global.rheight,camera_get_view_x(view_camera[0])+room_width,camera_get_view_y(view_camera[0])+global.rheight+20,false);
draw_set_color(c_white);
draw_set_font(global.sylvie_font);
draw_text_centered(camera_get_view_x(view_camera[0])+(global.view_width div 2),camera_get_view_y(view_camera[0])+(global.view_height-17),(area == "") ? (oRoomGen.area + " ~ Mushroom Zone") : area);