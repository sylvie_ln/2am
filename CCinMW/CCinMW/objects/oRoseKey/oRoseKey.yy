{
    "id": "4e2ef0e9-bcdf-4799-a73a-6d0a4c1bfe04",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRoseKey",
    "eventList": [
        {
            "id": "d9d37f04-3559-4b7b-9387-81ffbd40aec6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e2ef0e9-bcdf-4799-a73a-6d0a4c1bfe04"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20734242-6070-44a9-9d4d-d7a790e32e5b",
    "visible": true
}