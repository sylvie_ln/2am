if view_visible[0] {
	var cx = camera_get_view_x(view_camera[0]);
	var cy = camera_get_view_y(view_camera[0]);
} else {
	var cx = 0;
	var cy = 0;
}
var cw = global.view_width;
var ch = global.view_height;
draw_set_color(image_blend);
draw_set_alpha(time/total);
draw_rectangle(cx,cy,cx+cw,cy+ch,false);
draw_set_alpha(1);