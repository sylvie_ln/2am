active = false;
parent = noone;
desc=@"Enter a name of at least 5 symbols.
Each name determines a world. 
Some names might determine worlds 
that are identical other than name.
Each named world gets its own save.
The original world is named EIVLYS.
After typing at least 5 symbols,
end the name with a ! mark,
and wait for a few seconds.
You will enter the world."

rank=@"Each world has a Rank.
Normal: Like the original world.
Tricky: Memories, rare shrooms,
& rare items are farther out in 
zones on average. There are extra 
trades needed to get all Hands.
Difficult: Memories & rare things 
are even farther out on average.
Extra trades need more items.
Use + and - symbols to adjust rank."

style=@"Each world has a Shroom Style.
Standard: Like the original world.
Shuffled: Changes the zones where 
rare & uncommon shrooms appear.
Discover the changes using hints 
or simply by exploring the zones.
Use ? symbols to adjust style."

layout=@"Each world has a Zone Layout.
#0 is the original layout.
#1 to #99 are different layouts.
Odd number layouts are more odd,
while even number layouts are 
more like the original world.
Force a layout with 0 to 9 symbols."

help=false;

word="JIGGLER"
count = 0;
time = global.default_room_speed;
typed_prev = "";

history=ds_list_size(global.world_list);