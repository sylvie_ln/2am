{
    "id": "8e7cebc0-961f-44bf-8493-c43cfaa6ceb9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWorldWrangler",
    "eventList": [
        {
            "id": "8e37284c-005c-40cf-974b-009e4d8f2841",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e7cebc0-961f-44bf-8493-c43cfaa6ceb9"
        },
        {
            "id": "302f0447-4690-4493-ac37-6c9a61141425",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8e7cebc0-961f-44bf-8493-c43cfaa6ceb9"
        },
        {
            "id": "e4c14425-de41-48a3-b1e4-c20443db1f92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "8e7cebc0-961f-44bf-8493-c43cfaa6ceb9"
        },
        {
            "id": "9741ac73-c692-4c29-930c-501ed1531919",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8e7cebc0-961f-44bf-8493-c43cfaa6ceb9"
        },
        {
            "id": "4e1748b4-59a6-4719-8e96-58db6e5a260e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8e7cebc0-961f-44bf-8493-c43cfaa6ceb9"
        },
        {
            "id": "239f578d-be79-4f57-86de-1c3d37a6cd4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8e7cebc0-961f-44bf-8493-c43cfaa6ceb9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}