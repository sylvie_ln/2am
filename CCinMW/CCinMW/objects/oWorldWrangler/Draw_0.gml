if !active { exit; }
draw_set_color(c_black);
draw_rectangle(0,0,room_width,room_height,false);
var hpos = x;
var vpos = y;
var exclaim = string_length(typed_prev) > 5 and string_char_at(typed_prev,string_length(typed_prev)) == "!";
if exclaim {
	typed = typed_prev;
} else {
	var typed = string_upper(keyboard_string);
	keyboard_string = typed;
	if typed_prev != typed {
		play_sound(sndContractPenWrite);
		typed_prev = typed;
	}
}
draw_set_font(global.neco_font);
draw_set_color(c_white);
var world_txt = "";
if string_length(typed) >= 5 {
	var dc = string_char_at(typed,2);
	var sc = string_char_at(typed,4);
	if ord("A") <= ord(dc) and ord(dc) <= ord("J") {
		var difficulty = 0;
	} else 
	if ord("K") <= ord(dc) and ord(dc) <= ord("T") {
		var difficulty = 1;
	} else 
	if ord("U") <= ord(dc) and ord(dc) <= ord("Z") {
		var difficulty = 2;
	} else {
		var difficulty = ord(dc) mod 3;
	}
	difficulty += string_count("+",typed);
	difficulty -= string_count("-",typed);
	difficulty = nmod(difficulty,3);
	var difficulty_str = ["Normal", "Tricky", "Difficult"];
	if ord("A") <= ord(sc) and ord(sc) <= ord("M") {
		var shuffle = 0;
	} else 
	if ord("N") <= ord(sc) and ord(sc) <= ord("Z") {
		var shuffle = 1;
	} else {
		var shuffle = ord(sc) mod 2;
	}
	shuffle += string_count("?",typed);
	shuffle = nmod(shuffle,2);
	var shuffle_str = ["Standard", "Shuffled"];
	// djb2 hash function
	var hash = 5831;
	var i = 0;
	while(i <= string_length(typed)) {
		var c = string_char_at(typed,++i);
		if c == "+" or c == "-" or c == "?" or c == "!" or (ord("0") <= ord(c) and ord(c) <= ord("9")) {
			continue;	
		}
		hash = ((hash << 5) + hash) + ord(c);
		hash = hash mod power(2,32);
	}
	global.shuffle_seed = hash;
	global.bg_color = make_color_hsl_240((shuffle ? 239-nmod(hash,240) : nmod(hash,240)),239-(30*difficulty),10+(10*(2-difficulty)))
	var zone_layout = nmod((hash+61),100);	
	var digi = string_digits(typed);
	if digi != "" {
		zone_layout = nmod(real(digi),100);	
	}
	var world_txt = "World Rank: "+difficulty_str[difficulty]+"\n"
	+"Shroom Style: "+shuffle_str[shuffle]+"\n"
	+"Zone Layout: #"+string(zone_layout);
}
if help {
	draw_set_halign(fa_right);
	draw_text(room_width-2,2,rank);
	draw_text(room_width-2,room_height-2-string_height(layout),layout);
	draw_text(room_width-8,(room_height div 2),world_txt);
	draw_set_halign(fa_left);
	draw_text(2,2,desc);
	draw_text(2,room_height-2-string_height(style),style);
	txt = "Press Escape to close\nthis useful-help.\nOr simply begin typing."
	draw_text_centered(room_width div 2,(room_height div 2),txt);
	draw_text(8,(room_height div 2)+8,typed);
} else {
	var txt = 
	@"Here you may enter other worlds.
	Type a name of 5 letters or more.
	End with a ! mark to enter the world.
	
	"+
	(!global.eivlys_clear ?
	@"It's recommended to complete the original world with 
	all Hands of the Clock before you enter other worlds. 
	But she will not stop you.
	
	" : "")+"Press Escape to back out.\nType HELP for extra help.\nScroll through past worlds with Up & Down.\n \n"+" "+typed+" ";
	draw_text_centered(room_width div 2,(room_height div 3)-(string_height(txt) div 2),txt);
	draw_text_centered(room_width div 2,room_height-string_height(world_txt)-4,world_txt);
}
var done = false;
var word = typed;
if word == "HELP" or exclaim {
	done = true;	
}
if done {
	count++;
	if count > time div 2 {
		if help == 0 and word == "HELP" {
			help=1;	
			typed_prev = "";
			play_sound(sndMenuReset);
			instance_create_depth(0,0,0,oFlash);
			keyboard_string = "";
			exit;
		} else if count > time div 3 {
			help = 0;
			typed_prev = "";
			keyboard_string = "";
			if word == "EIVLYS!" {
				global.world_name = "EIVLYS";
				global.world_filename = "";
				global.bg_color = c_black;
			} else {
				global.world_name = string_copy(word,1,string_length(word)-1);
				global.world_filename = 
				string_replace_all(
				string_replace_all(
				string_replace_all(
				base64_encode(word),
				"+","_62"),
				"/","_63"),
				"=","");
			}
			global.difficulty = difficulty;
			global.zone_layout = zone_layout;
			global.world_offset = oMapp.dump_seed_offsets[global.zone_layout];
			with oMapp {
				fluffesque(zone_layout,difficulty);
			}
			global.shroom_shuffle = shuffle;
			if global.shroom_shuffle {
				sylvie_set_seed(global.shuffle_seed+global.world_offset);
				ds_list_copy(global.rare_shrooms,global.rare_shrooms_base);
				ds_list_copy(global.unco_shrooms,global.unco_shrooms_base);
				sylvie_list_permute(global.rare_shrooms);
				sylvie_list_permute(global.unco_shrooms);
			} else {
				ds_list_copy(global.rare_shrooms,global.rare_shrooms_base);
				ds_list_copy(global.unco_shrooms,global.unco_shrooms_base);
			}
			layer_background_blend(layer_background_get_id("Background"),global.bg_color);
			if global.world_filename != "" {
				if ds_list_find_index(global.world_list,global.world_filename) == -1 {
					ds_list_add(global.world_list,global.world_filename);
				}
			}
			save_world();
			event_perform_object(oInit,ev_other,ev_user12);
			load_game();
			save_game();
			play_sound(sndMenuReset);
			instance_create_depth(0,0,0,oFlash);
			active = false;
			with parent { event_user(0); menu = play; submenu = 0; }
			exit;
		}
	}
} else {
	count = 0;	
}