if !active { exit; }
if input_pressed("Escape") {
	play_menu_backward();
	if help {
		help = 0;	
	} else {
		active = false;
		with parent { event_user(0); }
	}
	exit;	
}
var change = false;
if keyboard_string == "" {
	history = ds_list_size(global.world_list);	
}
if input_pressed_repeat("Up") {
	if keyboard_check(vk_nokey) or keyboard_check(vk_up) {
		history--;
		change = true;
	}
}
if input_pressed_repeat("Down") {
	if keyboard_check(vk_nokey) or keyboard_check(vk_down) {
		history++;
		change = true;
	}
}
history = nmod(history,ds_list_size(global.world_list)+1);
if history != ds_list_size(global.world_list) and change {
	keyboard_string = global.world_list[|history];
	keyboard_string = 
	string_replace_all(
	string_replace_all(
	base64_decode(keyboard_string),
	"_62","+"),
	"_63","/");
	if string_char_at(keyboard_string,string_length(keyboard_string)) == "!" {
		keyboard_string = string_copy(keyboard_string,1,string_length(keyboard_string)-1);
	}
} else if change {
	keyboard_string = "";	
}