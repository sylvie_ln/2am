{
    "id": "d61fffe3-da1f-4f78-a207-1f06a2b7b97c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitleNES",
    "eventList": [
        {
            "id": "2e694e77-5c43-4654-b8bc-7b6b000b24f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d61fffe3-da1f-4f78-a207-1f06a2b7b97c"
        },
        {
            "id": "ecce247b-88d1-4a45-aab4-4f36a1f5d3bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d61fffe3-da1f-4f78-a207-1f06a2b7b97c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "60326fbd-c89f-46eb-ac87-530004e81557",
    "visible": true
}