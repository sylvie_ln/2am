{
    "id": "b5ae46e6-634d-4949-8e60-c34365bdc360",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlockNES",
    "eventList": [
        {
            "id": "f0acac0d-ee95-433a-be1a-05f2358746fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b5ae46e6-634d-4949-8e60-c34365bdc360"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1f7c779b-8dd3-42a5-9939-4c90cfc35727",
    "visible": true
}