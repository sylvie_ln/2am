event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "The legendary Pegasus Shoes, they make you run fast, but at what cost?"

damp = 0.5;
bounce = damp;
fric = 0.95;
//grav /= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!