draw_set_color(c_white)

var hpos = 4;
var vpos = 4;
var skip = 6;
var size = 8;
var i = 0;
var poff = [0,3,7,11];

for(var j=0; j<array_length_1d(contract_box); j++) {
	contract_box[j] = undefined;	
}
#macro contract_title draw_set_font(global.sylvie_font_bold); draw_text_centered(cx,vpos,txt); vpos += string_height(txt)+skip;
#macro contract_centered draw_set_font(global.neco_font); draw_text_centered(cx,vpos,txt); vpos += string_height(txt)+skip;
#macro contract_left_nobox draw_set_font(global.neco_font); draw_text(hpos+4+size,vpos,txt); vpos += string_height(txt)+skip;
#macro contract_left draw_set_font(global.neco_font); contract_box[poff[page]+i] = [hpos,vpos+(string_height(txt) div 2)-(size div 2),hpos+size,vpos+(string_height(txt) div 2)+(size div 2)]; var cb = contract_box[poff[page]+i]; if contract_hover[poff[page]+i] { draw_set_color(c_dkgray); draw_rectangle(cb[0],cb[1],cb[2],cb[3],false); draw_set_color(c_white) } draw_rectangle(cb[0],cb[1],cb[2],cb[3],true); if contract_signed[poff[page]+i] { draw_sprite(sCheckmark,0,cb[0]+((cb[2]-cb[0]) div 2),cb[1]+((cb[3]-cb[1]) div 2)) }  i++; contract_left_nobox
#macro contract_header draw_set_font(global.sylvie_font_bold); draw_text(hpos,vpos,txt); vpos += string_height(txt)+skip;

draw_set_font(global.neco_font);
var bsize = [48,24];
var bbor = 4;
next_button = [room_width-bsize[0]-bbor,room_height-bsize[1]-bbor,room_width-bbor,room_height-bbor];	

var txt = "> Next  ";
if page == 3 { 
	txt = "* Accept  " 
} 
if page != 3 or can_accept {
	if next_hover {
		draw_set_color(c_dkgray);
		draw_rectangle(next_button[0],next_button[1],next_button[2],next_button[3],false);
		draw_set_color(c_white);
	}
	draw_rectangle(next_button[0],next_button[1],next_button[2],next_button[3],true);
	draw_text_centered(room_width-bsize[0]+(bsize[0]div 2),room_height-bsize[1]+bbor,txt);
}

if page > 0 {
	back_button = [bbor,room_height-bsize[1]-bbor,bsize[0]+bbor,room_height-bbor];	
	if back_hover {
		draw_set_color(c_dkgray);
		draw_rectangle(back_button[0],back_button[1],back_button[2],back_button[3],false);
		draw_set_color(c_white);
	}
	draw_rectangle(back_button[0],back_button[1],back_button[2],back_button[3],true);
	var txt = "< Back";
	draw_text_centered(bbor+(bsize[0]div 2),room_height-bsize[1]+bbor,txt);
}

if page == 0 {
	var txt = "Contract of the Grand Witch Eivlys"
	contract_title

	txt =
	@"Hello, spirit from another World.";
	contract_centered

	txt = 
	@"If you wish to enter the World created by Eivlys, 
	you must accept the terms of this Contract. 
	Please mark each box on the left to indicate acceptance."
	contract_centered

	txt = "Section I. Form of the Representatives"
	contract_header

	txt =
	@"My representative in the World 
	will be the following Character."
	draw_sprite(sCharS,0,cx,vpos+8);
	contract_left

	txt=
	@"I will also sometimes be represented 
	by the following Cursor."
	draw_sprite(sCursor,0,cx+20,vpos);
	contract_left
	
	
	draw_rectangle(hpos,vpos,room_width-bsize[0]-bbor*2,room_height-bbor,true);
	hpos += 4;
	vpos += 2;
	txt=
	@"(Optional) I wish to bind my Soul to the 
	Character and become one with her."
	contract_left
	
	txt =
	@"If the above box is not checked, the Character will maintain 
	a separate existence from you while acting on your behalf."
	hpos -= 12;
	contract_left_nobox
	hpos += 12;
	hpos -= 4;
} else if page == 1 {
	
	vpos += 4;
	txt = "Section II. Methods of Control"
	contract_header
	
	txt = @"By default, the following controls are set up for me:"
	contract_left
	
	txt = 
	@"> I can Move with the Arrow Keys or WASD.
	> I can Jump or Select using X or Space.
	> I can Open or Close my Bag using C or Control.
	> I can Pause or Cancel using Escape."
	contract_left_nobox
	
	txt=
	@"The controls above can be modified in the Options Menu, 
	accessible from the Title Screen or by Pausing in the World."
	contract_left

    txt=
	@"The Cursor can be controlled with either the 
	Move and Select buttons, or a Mouse (recommended)."
	contract_left

    txt=
	@"When using the Move buttons to control the Cursor, holding 
	the Select button slows down the movement speed."
	contract_left
} else if page == 2 {
	vpos += 4;
	txt = "Section III. Progress in the World"
	contract_header
	
	txt =
	@"Progress in the World is auto-saved invisibly whenever I
	enter a door or return to the title screen. Closing the game
	through other methods will not trigger an auto-save, beware."
	contract_left
	
	txt = 
	@"The World may become very difficult (though not impossible)
	to progress in if I make poor decisions, such as leaving a 
	key item in a hard-to-reach location."
	contract_left
	txt =
	@"The World may become very tedious to progress in if I forget
	what I have done and what is left to do. Taking notes or
	recording a video of my travels may remedy this."
	contract_left
	txt =
	@"Discovering everything of interest that exists in the World,
	also known as 100% completion, is a very time-consuming task. 
	Eivlys does not expect this of anyone."
	contract_left
} else if page == 3 {	
	vpos += 8;
	txt = "Finale"
	contract_title 
	
	txt = 
	@"Please contact the Game Developers 
	if the terms of the contract are breached. 
	For example, if progress seems impossible, 
	or the controls are not working correctly.
	Breaches of contract are considered Bugs."
	contract_centered
	
	vpos += 12;
	txt = 
	@"In exchange for passage into her World,
	I offer my Soul to the Grand Witch Eivlys."
	contract_title 
	
	draw_set_color(c_white);
	draw_set_font(global.neco_font);
	txt = 
	@"Click here and type your name."
	var ww = (string_width(txt) div 2);
	var hh = (string_height(txt));
	if name != "" {
		draw_set_color(c_red);
		draw_set_font(global.sylvie_font);
		txt = name;	
	}
	if naming {
		draw_set_color(c_red);
		draw_set_font(global.sylvie_font);
		txt = keyboard_string;
		draw_sprite(sRedCat,image_index,cx+(string_width(txt) div 2)+8,vpos+hh+skip-8);
		if cx+(string_width(txt) div 2)+16 > room_width {
			keyboard_string = string_delete(keyboard_string,1,1);
		}
	}
	name_box = [cx-ww,vpos,cx+ww,vpos+hh+skip];
	draw_text_centered(cx,vpos,txt); vpos += hh+skip;
	draw_line_color(name_box[0],vpos,cx,vpos,c_black,c_red);
	draw_line_color(cx,vpos,name_box[2],vpos,c_red,c_black);
	if !naming and name_hover {
		draw_set_alpha(0.5);
		draw_rectangle_color(name_box[0],name_box[1]-2,cx,name_box[3],c_black,c_red,c_red,c_black,false);
		draw_rectangle_color(cx+1,name_box[1]-2,name_box[2],name_box[3],c_red,c_black,c_black,c_red,false);
		draw_set_alpha(1);
	}
	
	if !naming and name != "" and !signed {
		vpos += 16;
		draw_set_color(c_red);
		txt = "Please mark all non-optional items to proceed."
		contract_centered
	}
}
init = true;