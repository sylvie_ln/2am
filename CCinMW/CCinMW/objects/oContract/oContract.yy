{
    "id": "16afa10b-b50e-4834-a2cd-c759190f1666",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oContract",
    "eventList": [
        {
            "id": "c8ca77a6-96f6-4a94-9005-90de6296f322",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16afa10b-b50e-4834-a2cd-c759190f1666"
        },
        {
            "id": "2a77e14c-50da-4e46-9842-6b3fb0d21b26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "16afa10b-b50e-4834-a2cd-c759190f1666"
        },
        {
            "id": "ae424020-e78b-42f7-a2cf-41fc959a05f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "16afa10b-b50e-4834-a2cd-c759190f1666"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3efd235b-f4c5-4753-abac-6c0a44a7da39",
    "visible": true
}