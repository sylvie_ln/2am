
if !naming {
	with oCursor {
		event_user(0);	
	}
}
if !init { exit; }

var click_press = mouse_check_button_pressed(mb_left) or input_pressed("Jump");

name_hover = false;
if page == 3 and !naming {
	name_hover = point_in_rectangle(smouse_x,smouse_y,name_box[0],name_box[1],name_box[2],name_box[3]);
	if name_hover and (!naming and click_press) {
		play_sound(sndContractPenOpen);
		naming = true;
		keyboard_string = name;
	}
} else if naming and ((!name_hover and (click_press and !input_pressed("Jump"))) or input_pressed("Escape") or keyboard_check_pressed(vk_enter)) {
	naming = false;
	name = keyboard_string;
}

if naming {
	if keyboard_string != keyboard_string_prev {
		play_sound(sndContractPenWrite);
	}
	keyboard_string_prev = keyboard_string;
	exit;
}

back_hover = false;	
if page > 0 {
	back_hover = point_in_rectangle(smouse_x,smouse_y,back_button[0],back_button[1],back_button[2],back_button[3]);
}
if back_hover and (!naming and click_press) {
	play_sound(sndContractPagePrevious);
	page--;	
}

next_hover = point_in_rectangle(smouse_x,smouse_y,next_button[0],next_button[1],next_button[2],next_button[3]);
if next_hover and (!naming and click_press) {
	if page < 3 {
		play_sound(sndContractPageNext);
		page++;
	} else if can_accept {
		play_sound(sndContractPenClose);
		with instance_create_depth(0,0,0,oFlash) {
			image_blend = c_red;
		}
		global.contract_signed = true;
		global.contract_name = name;
		global.contract_soul_bond = contract_signed[2];
		save_game();
		room_goto(rmTitle);
	}
}

for(var i=0; i<array_length_1d(contract_box); i++) {
	var cb = contract_box[i];
	if is_undefined(cb) { continue; }
	contract_hover[i] = point_in_rectangle(smouse_x,smouse_y,cb[0],cb[1],cb[2],cb[3]);
	if contract_hover[i] and click_press {
		contract_signed[i] = 1-contract_signed[i];
		play_sound(sndContractPenWrite);
	}
}

signed = true;
for(var i=0; i<array_length_1d(contract_signed); i++) {
	if i == 2 { continue; }
	if !contract_signed[i] { signed = false; }
}
if name == "" { signed = false; }

can_accept = (signed and page == 3 and !naming and string_replace_all(name," ","") != "");