event_inherited();
cutey = false;
call = false;
image_speed = 0;
script = @"
!if call==0
 The stone grave reads:
 "
required_hand = noone;
word=""
switch(image_index) {
	case hand.darkness:
		script += @"Here lies the darkness of Eivlys.\nCall forth her pathetic deeds."
		required_hand = "oHandOfDarkness"
		word = "darkness"
	break;
	case hand.light:
		script += @"Here lies the light of Eivlys.\nCall forth her innocent love, the one that could never be fulfilled, and let it shine once more."
		required_hand = "oHandOfLight"
		word = "light"
	break;
	case hand.tears:
		script += @"Here lie the tears of Eivlys.\nCall forth her bitter sadness."
		required_hand = "oHandOfTears"
		word = "tears"
	break;
	case hand.flames:
		script += @"Here lie the flames of Eivlys.\nCall forth her burning jealousy."
		required_hand = "oHandOfFlames"
		word = "flames"
	break;
	case hand.leafs:
		script += @"Here lie the leafs of Eivlys.\nCall forth her vines of bondage."
		required_hand = "oHandOfLeafs"
		word = "leafs"
	break;
	case hand.thunder:
		script += @"Here lies the thunder of Eivlys.\nCall forth her raging anguish."
		required_hand = "oHandOfThunder"
		word = "thunder"
	break;
	case hand.lies:
		script += @"Here lie the lies of Eivlys.\nCall forth her gentle words."
		required_hand = "oHandOfLies"
		word = "lies"
	break;
	case hand.sky:
		script += @"Here lies the sky of Eivlys.\nCall forth her vast emptiness."
		required_hand = "oHandOfSky"
		word = "sky"
	break;
	case hand.earth:
		script += @"Here lies the earth of Eivlys.\nCall forth her supportive nature."
		required_hand = "oHandOfEarth"
		word = "earth"
	break;
	case hand.storms:
		script += @"Here lie the storms of Eivlys.\nCall forth her swirling confusion."
		required_hand = "oHandOfStorms"
		word = "storms"
	break;
	case hand.moon:
		script += @"Here lies the moon of Eivlys.\nCall forth her memory of its light."
		required_hand = "oHandOfMoon"
		word = "moon"
	break;
	case hand.dreams:
		script += @"Here lie the dreams of Eivlys.\nCall forth her foolish desires."
		required_hand = "oHandOfDreams"
		word = "dreams"
	break;
}
script += @"
!else
 !handcheck "+required_hand+@"
 !if flag_handcheck==1
  !set cutscene,1
  Come forth, "+word+@" of Eivlys.
  !call "+string(image_index)+@"
  !set cutscene,0
 !else
  You cry out for help, but your words disappear into the darkness.
 !fi
!fi"
act1="Read";
act2="Call";

if variable_global_get("flag_door_"+word) {
	instance_change(oDoor,false);
	image_speed = 1;
	act1 = "Examine"
	act2 = "Enter"
	script = "Proceed."
}