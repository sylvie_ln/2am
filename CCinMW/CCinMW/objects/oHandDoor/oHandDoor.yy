{
    "id": "2f277f51-3486-4b03-80b5-39b04d4956a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHandDoor",
    "eventList": [
        {
            "id": "26f92ed5-aa5c-49ef-b6f7-24078bd22852",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f277f51-3486-4b03-80b5-39b04d4956a6"
        },
        {
            "id": "c1d66d9d-76ef-4950-8f48-c2cbc78d2b2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "2f277f51-3486-4b03-80b5-39b04d4956a6"
        },
        {
            "id": "f6ad71d2-f7fc-45f4-8548-9f1fb2f78709",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2f277f51-3486-4b03-80b5-39b04d4956a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
    "visible": true
}