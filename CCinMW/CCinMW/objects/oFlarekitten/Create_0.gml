///@description

// Inherit the parent event
event_inherited();

upblocker = false;
pushable = false;
carriable = false;
pusher = false;
carrier = false;
solid = false;

acc = 0;
maxspd = 4;

target_obj = oChar;
target = [0,0];

kittey_vision = 4;

searched = false;
search_pos = [0,0];
x = oChar.x + sylvie_choose(-32,32);
y = -20;
grav *= 1.5