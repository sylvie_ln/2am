if paused { exit; }
x += hv;
y += vv;

if room == rmMushZone {
	var rare_in_room = noone;
	with oShroom {
		if generated and rare {
			rare_in_room = id;	
		}
	}
	if rare_in_room != noone {
		target_obj = rare_in_room;
		searched = true;
	}
	if !searched {
		var kittens = ds_queue_create();
		var start_xp = oRoomGen.xp;
		var start_yp = oRoomGen.yp;
		var w = oRoomGen.wwidth;
		var h = oRoomGen.wheight;
		var mappdata = oRoomGen.mapper.mapp;
		var raredata = oRoomGen.uncommon_map;
		var luckdata = oRoomGen.luck_map;
		ds_queue_enqueue(kittens,[start_xp,start_yp,0,0]);
		var kittens_visited = ds_grid_sylvie_create(w,h);
		ds_grid_clear(kittens_visited,false);
		kittens_visited[# start_xp,start_yp] = true;
		var found = false;
		while !ds_queue_empty(kittens) {
			var kitten = ds_queue_dequeue(kittens);
			var border = mappdata[# kitten[0],kitten[1]] & 0xf;
			var visits = [[0,-1,edir.up],[0,1,edir.down],[-1,0,edir.left],[1,0,edir.right]];
			if (raredata[# kitten[0],kitten[1]] == 1 
			 or (oRoomGen.area == "Cloudy Mixture" and raredata[# kitten[0],kitten[1]] >= 1 and raredata[# kitten[0],kitten[1]] <= 3)   
			 or (oRoomGen.area == "Cataclysmic Fountain" and raredata[# kitten[0],kitten[1]] != 0))
			
			and (global.flag_dice or luckdata[# kitten[0],kitten[1]] == 1) {
				var rare_removed =
					ds_map_exists(global.rare_dead,oRoomGen.area+"_"+room_key(kitten[0],kitten[1]));
				if !rare_removed {
					found = true;
					break;	
				}
			}
			if kitten[2]+1 > kittey_vision { continue; }
			for(var v=0; v<4; v++) {
				var visit = visits[v];
				if(visit[2] & border == 0) { continue; }
				var nkx = nmod(kitten[0]+visit[0],w);
				var nky = nmod(kitten[1]+visit[1],h);
				if kittens_visited[# nkx,nky] { continue; }
				var newkitten = [nkx,nky,kitten[2]+1,(kitten[3] << 4)+visit[2]];
				//disabled_show_debug_message(newkitten);
				ds_queue_enqueue(kittens,newkitten);
				kittens_visited[# newkitten[0],newkitten[1]] = true;
			}
		}
		if found {
			var n = kitten[3];
			var pdir = 0;
			while(n > 0) {
				var pdir = n & 0xf;
				n = (n >> 4);
			}
			if pdir != 0 {
				var data = mappdata[# start_xp, start_yp];
				var amount = round(log2(pdir))*8;
				var border_position = ((data >> 4) >> amount) & 15;
				var border_opening  = ((data >> 8) >> amount) & 15;
				var cs = oRoomGen.cell_size;
				var axis = (pdir <= edir.down) ? 1 : 0;
				var pbor = 32;
				target = [
					(1-axis)*((pdir == edir.left or pdir == edir.up) ? pbor : room_width-pbor)
					+ axis*(((border_position+(border_opening div 2))*cs)+(cs div 2)),
					(axis)*((pdir == edir.left or pdir == edir.up) ? pbor : room_height-pbor)
					+ (1-axis)*(((border_position+(border_opening div 2))*cs)+(cs div 2)),
				];
				//disabled_show_debug_message(target);
				target_obj = noone;
			}
		}
		ds_queue_destroy(kittens);
		ds_grid_sylvie_destroy(kittens_visited);
		search_pos = [start_xp,start_yp];
		searched = true;
		//disabled_show_debug_message(object_get_name(target_obj));
	}
	if searched and (oRoomGen.xp != search_pos[0] or oRoomGen.yp != search_pos[1]) and !rare_in_room {
		searched = false;	
		target_obj = oChar;
	}
}

if instance_exists(target_obj) {
	target = [target_obj.x, target_obj.bbox_top - 12];	
} else if target_obj != noone {
	target_obj = oChar;	
}

var dir = point_direction(x,y,target[0],target[1]);
if point_distance(x,y,target[0],target[1]) > 4 {
	acc += grav;	
	if acc > maxspd { acc = maxspd; }
} else {
	acc = 0;
}
hv += lengthdir_x(acc,dir);
vv += lengthdir_y(acc,dir);
var spd = point_distance(0,0,hv,vv);
if spd > maxspd {
	hv = (hv/spd)*maxspd;
	vv = (vv/spd)*maxspd;
}
