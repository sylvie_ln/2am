{
    "id": "1a87f32d-e652-48d3-9d22-6bb49f0bdf25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHandOfDreams",
    "eventList": [
        {
            "id": "42eba711-1fc4-47f8-ad9d-d44e2645980a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1a87f32d-e652-48d3-9d22-6bb49f0bdf25"
        }
    ],
    "maskSpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
    "overriddenProperties": null,
    "parentObjectId": "3cf2a5e5-8deb-4ca2-bdc0-e75f47cf3912",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
    "visible": true
}