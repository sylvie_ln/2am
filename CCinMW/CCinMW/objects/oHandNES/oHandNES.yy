{
    "id": "e20a8c1b-8ab3-4502-b774-00b69ccca8f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHandNES",
    "eventList": [
        {
            "id": "4c39b5f0-2c6c-49e3-b10c-9ef0255f408c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e20a8c1b-8ab3-4502-b774-00b69ccca8f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
    "visible": true
}