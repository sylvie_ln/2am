{
    "id": "376d1036-c08d-4ac2-aa66-e551035f0f27",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oJunkParts",
    "eventList": [
        {
            "id": "9c59d16c-067a-44ef-8946-64fb1ac600c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "376d1036-c08d-4ac2-aa66-e551035f0f27"
        },
        {
            "id": "9797ac22-a8a2-42ae-b1af-9dd29023afc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "376d1036-c08d-4ac2-aa66-e551035f0f27"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8d584385-a198-4280-b0bc-dc86dace15a0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ce2b9366-662d-45b8-b271-1535524137e9",
    "visible": true
}