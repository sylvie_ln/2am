event_inherited();

if global.flag_decap {
	play_sound(sndDecapShroom);
}

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A little junk that can be used for repairs and parts."

damp = 0.75;
bounce = damp;
fric = 0.95;
//grav *= 2;

image_speed = 0;
if room == rmMushZone {
	image_index = sylvie_random(0,image_number-1);	
}

/// In oInit, define a trade entry for this item!