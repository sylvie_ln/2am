event_inherited();

script = @"
!set cutscene,1
You feel a sharp black wind cut at your heart. Something, or someone, doesn't want you to use items in this place.
!set cutscene,0
"