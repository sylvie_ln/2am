event_inherited();
subpixel = [0,0];
blocker = false;
upblocker = false;
precise_upblocker = false;
solid_boundary = false;
very_solid_boundary = false;

pusher = false;
pushable = false;
push_array = array_create(32,0);
carrier = false;
carriable = false;
carried_this_step = [0,0];
no_carry = false;

fall_through = 0;

grav = 0.15;
hv = 0;
vv = 0;

actors = ds_list_sylvie_create();