var bgm = -1;
switch(room) {
	case rmTitle: bgm = bgmTitle; break;
	case rmCylveyCitey: bgm = bgmCitey; break;
	case rmCherryBlossomHills: 
		bgm = "bgmCherry";
		var day;
		switch(current_weekday) {
		    case 0: day = "Sunday"; break;
		    case 1: day = "Monday"; break;
		    case 2: day = "Tuesday"; break;
		    case 3: day = "Wednesday"; break;
		    case 4: day = "Thursday"; break;
		    case 5: day = "Friday"; break;
		    case 6: day = "Saturday"; break;
	    }
		bgm += day;
		bgm = asset_get_index(bgm);
	break;
	case rmCanyonCliffs: bgm = bgmCanyon; break;
	case rmCavernOfAncients: bgm = bgmCavern; break;
	case rmMushZone: bgm = bgmMush; break;
	case rmCylveyClockTower: bgm = bgmClock; break;
	case rmCoolMuseum: bgm = bgmCoolMuseum; break;
	case rmEnding: bgm = bgmMedley; break;
	case rmMinecraft: bgm = bgmMinecart; break;
}
no_radio = 0;
if room == rmSecretTomb 
or room == rmSecretTomb2
or room == rmSecretTomb3
or room == rmMinecraft
or room == rmTowerTop
or room == rmLegendaryTree 
or room == rmEnding 
or room == rmTitle {
	no_radio = 1;
	stop_radio_bgm();	
}
play_bgm(bgm);
if global.flag_legendary_tree == 1 {
	if audio_exists(global.bgm_instance) {
		audio_sound_gain(global.bgm_instance,0,0);	
		audio_sound_gain(global.bgm_instance,1,5000);	
	}
	if audio_exists(global.radio_bgm_instance) {
		audio_sound_gain(global.radio_bgm_instance,0,0);	
		audio_sound_gain(global.radio_bgm_instance,1,5000);	
	}
}
start = true;