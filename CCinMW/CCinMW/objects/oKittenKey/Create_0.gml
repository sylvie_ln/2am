event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A lovely, cute key shaped like a kitten. Because of the strange shape, it can't really be used as a key."

damp = 1;
bounce = damp;
fric = 1;
//grav *= 2

image_speed = 0;

/// In oInit, define a trade entry for this item!