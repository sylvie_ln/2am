if paused {exit;}
if waiting { 
	exit;
}

x += hv;
y += vv;

var around = false;
var dist = 81;
with oClonkTower {
	if !outside_view() {
		around = true;
	}
	dist = point_distance(x,y,other.x,other.y);
}
if around or dist <= 80 {
	target = oClonkTower;
	orbit = 20;
	maxspd = 2;
}
var dir = point_direction(x,y,target.x,target.y);
if point_distance(x,y,target.x,target.y) > orbit {
	if !towards {
		acc = 0;	
	}
	towards = true;
	acc += grav;	
	if acc > maxspd { acc = maxspd; }
} else {
	if towards {
		acc = 0;
		offset = sylvie_random(24,36);
	}
	towards = false;
	acc += grav;	
	if acc > maxspd { acc = maxspd; }
	dir += 180+offset;
}
hv += lengthdir_x(acc,dir);
vv += lengthdir_y(acc,dir);
var spd = point_distance(0,0,hv,vv);
if spd > maxspd {
	hv = (hv/spd)*maxspd;
	vv = (vv/spd)*maxspd;
}