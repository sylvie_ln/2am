event_inherited();

upblocker = false;
pushable = false;
carriable = false;
pusher = false;
carrier = false;

waiting = false;
target = oChar;
acc = 0;
maxspd = 4;
towards = true;
offset = 0;
orbit = 40;

enum hand {
	darkness,
	light,
	tears,
	flames,
	leafs,
	thunder,
	lies,
	sky,
	earth,
	storms,
	moon,
	dreams
}

switch(image_index) {
	case hand.darkness:
		script = "The Hand of Darkness. One of the Twelve Hands of the Clock.\n"+
		"You feel as you will be swallowed up by the deep black shadow of this hand....!"
	break;
	case hand.light:
		script = "The Hand of Light. One of the Twelve Hands of the Clock.\n"+
		"The soft bright glow warms your heart. It seems to chase away all sadness.";
	break;
	case hand.tears:
		script = "The Hand of Tears. One of the Twelve Hands of the Clock.\n"+
		"You feel a deep melancholy as you gaze at it, and your eyes begin to water.";
	break;
	case hand.flames:
		script = "The Hand of Flames. One of the Twelve Hands of the Clock.\n"+
		"It burns ferociously, as if it is angry at the world. Anger swells in your heart as well.";
	break;
	case hand.leafs:
		script = "The Hand of Leafs. One of the Twelve Hands of the Clock.\n"+
		"A gentle hand with a lush green color. You could stare at the beauty of nature forever.";
	break;
	case hand.thunder:
		script = "The Hand of Thunder. One of the Twelve Hands of the Clock.\n"+
		"You are afraid you will be shocked if you touch it. It glows, sparkles and cracks.";
	break;
	case hand.lies:
		script = "The Hand of Lies. One of the Twelve Hands of the Clock.\n"+
		"It gives a deep uneasy feeling. You are not sure whether honesty and truth can be trusted.";
	break;
	case hand.sky:
		script = "The Hand of Sky. One of the Twelve Hands of the Clock.\n"+
		"The vast expanse of the sky has been compressed into this small hand. It is overwhelming to look at.";
	break;
	case hand.earth:
		script = "The Hand of Earth. One of the Twelve Hands of the Clock.\n"+
		"It gives you a sense of stability and strength. This hand is the foundation of the world.";
	break;
	case hand.storms:
		script = "The Hand of Storms. One of the Twelve Hands of the Clock.\n"+
		"Complex feelings brew in your heart. You feel as if you are being torn apart by the winds.";
	break;
	case hand.moon:
		script = "The Hand of Moon. One of the Twelve Hands of the Clock.\n"+
		"The romantic light of the crescent moon. You wish someone else could see it.";
	break;
	case hand.dreams:
		script = "The Hand of Dreams. One of the Twelve Hands of the Clock.\n"+
		"What are the dreams in your heart? Will they ever come true?";
	break;
}

image_speed = 0;