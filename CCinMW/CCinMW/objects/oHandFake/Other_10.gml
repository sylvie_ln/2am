
switch(image_index) {
	case hand.darkness:
		script = "A cheap plastic model of the Hand of Darkness."
	break;
	case hand.light:
		script = "A tacky plastic model of the Hand of Light."
	break;
	case hand.tears:
		script = "An ugly plastic model of the Hand of Tears."
	break;
	case hand.flames:
		script = "A misshapen plastic model of the Hand of Flames."
	break;
	case hand.leafs:
		script = "A flimsy plastic model of the Hand of Leafs."
	break;
	case hand.thunder:
		script = "A crappy plastic model of the Hand of Thunder."
	break;
	case hand.lies:
		script = "A fake plastic model of the Hand of Lies."
	break;
	case hand.sky:
		script = "A shoddy plastic model of the Hand of Sky."
	break;
	case hand.earth:
		script = "A dirty plastic model of the Hand of Earth."
	break;
	case hand.storms:
		script = "A terrible plastic model of the Hand of Storms."
	break;
	case hand.moon:
		script = "A dull plastic model of the Hand of Moon."
	break;
	case hand.dreams:
		script = "A worthless plastic model of the Hand of Dreams."
	break;
}
event_inherited();

