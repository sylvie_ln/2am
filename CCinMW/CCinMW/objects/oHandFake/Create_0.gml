event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A plastic model of one of the 12 Hands of the Clock."

damp = 0.75;
bounce = damp;
fric = 0.95;


image_speed = 0;

/// In oInit, define a trade entry for this item!