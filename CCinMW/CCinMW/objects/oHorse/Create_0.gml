event_inherited();

act1 = "Ride";

damp = 0.25;
bounce = damp;
fric = 0.95;
spd = 5.7;
acc = 1/3;
hdamp = false;
grav /= 2;
jmp=sqrt(2*grav*36);

vehicle_pos = [0,-5];

/// In oInit, define a trade entry for this item!