{
    "id": "f4a8a8f3-f327-4133-952e-2884a5e3c863",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSign",
    "eventList": [
        {
            "id": "084c058d-6ea3-45d4-af65-941f8b55ef4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4a8a8f3-f327-4133-952e-2884a5e3c863"
        },
        {
            "id": "1f8a22f7-7181-469b-b75b-3f78c331b694",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f4a8a8f3-f327-4133-952e-2884a5e3c863"
        },
        {
            "id": "5a3b8359-e695-4987-9125-12eb25246b0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "f4a8a8f3-f327-4133-952e-2884a5e3c863"
        },
        {
            "id": "549c229d-b8ea-4296-b763-f61047e70833",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "f4a8a8f3-f327-4133-952e-2884a5e3c863"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ce39b26c-c603-4138-be14-4f1667663d54",
    "visible": true
}