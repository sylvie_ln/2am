{
    "id": "f7f5e5c3-f701-4629-9785-6e622bc74246",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCloudKey",
    "eventList": [
        {
            "id": "76a23f97-64ca-46af-87b6-0aca5e2e42ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7f5e5c3-f701-4629-9785-6e622bc74246"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7e67751-7eed-4db3-9fcc-6c895d325cc2",
    "visible": true
}