event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A little key shaped like a cloud. It also has the consistency of a cloud, so there's probably only a certain door it can unlock."

damp = 0.75;
bounce = damp;
fric = 0.95;
grav /= 2

image_speed = 0;

/// In oInit, define a trade entry for this item!