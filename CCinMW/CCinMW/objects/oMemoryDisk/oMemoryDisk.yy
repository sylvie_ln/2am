{
    "id": "e8eae1cf-445d-42a8-9ed8-21f941084ad3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMemoryDisk",
    "eventList": [
        {
            "id": "a2e14e13-a1d8-4f6e-a25e-c41bc3e8c7a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8eae1cf-445d-42a8-9ed8-21f941084ad3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5900f4f-0255-481f-821f-935f466519ac",
    "visible": true
}