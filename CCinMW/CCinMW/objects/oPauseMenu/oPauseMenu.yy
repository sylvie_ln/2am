{
    "id": "4afa8922-c8eb-45b4-872b-22248f44a78c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPauseMenu",
    "eventList": [
        {
            "id": "7ad6b73e-e504-4198-9a94-aca1aba39eef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4afa8922-c8eb-45b4-872b-22248f44a78c"
        },
        {
            "id": "477e6768-abdc-42df-99e9-2b1da2068294",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4afa8922-c8eb-45b4-872b-22248f44a78c"
        },
        {
            "id": "558a9f49-9577-449c-8ebb-3d219bd5a401",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4afa8922-c8eb-45b4-872b-22248f44a78c"
        },
        {
            "id": "d3306305-bffa-4005-902c-024aa5938773",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4afa8922-c8eb-45b4-872b-22248f44a78c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}