life = 0;

resume = "Let's |continue| the adventure.";
return_to_door = "Take me back to the |last entrance|.";
options = "I'd like to change some |options|.";
quit_to_title = "I'll |leave| this world for now.";

//if room == rmMushZone
	actions = [ resume, return_to_door, options, quit_to_title ];
//else
//	actions = [ resume, return_to_door, options, quit_to_title ];
	
current_action_index = 0;
play_sound(sndPauseMenuOpen);

active = true;