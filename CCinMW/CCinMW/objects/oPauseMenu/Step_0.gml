if !active { exit; }
if life > 0 && is_pause_key_pressed()
	event_user(0);

life++;

var prev_action_index = current_action_index;

if input_pressed("Up")
	current_action_index--;
else if input_pressed("Down")
	current_action_index++;
	
current_action_index = int_cycle_inclusive(0, current_action_index, array_length_1d(actions) - 1);

if prev_action_index != current_action_index
{
	play_sound(sndDialogSelect);
}

if instance_exists(oShifter) and oShifter.shifting { exit; }
if input_pressed("Jump")
{
	var current_action = actions[current_action_index];
	switch (current_action)
	{
		case resume:
			event_user(0);
			break;
		case return_to_door:
			play_sound(sndGoToDoor);
			restart_zone();
			break;
		case options:
			active = false;
			cx = camera_get_view_x(view_camera[0]);
			cy = camera_get_view_y(view_camera[0]);
			var xp = cx+100;
			var yp = cy+60;
			if !instance_exists(oAudioMenu) {
				instance_create_depth(xp,yp,depth-1,oAudioMenu);
			}
			if !instance_exists(oGraphicsMenu) {
				instance_create_depth(xp,yp,depth-1,oGraphicsMenu);
			}
			if !instance_exists(oControlsMenu) {
				instance_create_depth(cx,cy,depth-1,oControlsMenu);
			}
			if !instance_exists(oAssistMenu) {
				instance_create_depth(xp,yp,depth-1,oAssistMenu);
			}
			with instance_create_depth(xp,yp,depth-1,oTitleMenu) {
				pause_mode = true;
				menu = oTitleMenu.options;
				submenu = 1;	
			}
			
			current_action_index = 0;
			break;
		case quit_to_title:
			global.return_to_title = true;
			room_goto(rmTitle);
			break;
	}
}