
cx = camera_get_view_x(view_camera[0]);
cy = camera_get_view_y(view_camera[0]);
cw = camera_get_view_width(view_camera[0]);
ch = camera_get_view_height(view_camera[0]);

centerx = cx + cw / 2;
centery = cy + ch / 2;

draw_set_alpha(0.9);
draw_circle_color(centerx,centery,point_distance(0, 0, cw, ch) + 1,merge_color(c_white,c_black,0.75),c_black,false);
draw_set_alpha(1);

if !active { exit; }
draw_set_font(global.neco_font);
draw_set_color(c_white);

var txt = "Time for a little break."
draw_set_alpha(0.25);
draw_text_centered(centerx+1, cy + 30+1, txt);
draw_text_centered(centerx+1, cy + 30, txt);
draw_text_centered(centerx, cy + 30+1, txt);
draw_set_alpha(1);
draw_text_centered(centerx, cy + 30, txt);

var ty = 0;
var actions_size = array_length_1d(actions);
for (var i = 0; i < actions_size; i++)
{
	draw_text_emph((cx+90) - (current_action_index == i ? string_width("> ") : 0), 
		cy + 70 + ty, 
		current_action_index == i ? ("> " + actions[i]) : actions[i]);
	ty += 17;
}
