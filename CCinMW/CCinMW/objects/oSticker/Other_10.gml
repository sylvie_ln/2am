
event_user(1);
laser_me = true;
if oInTheBag.mode != 2 {
	if instance_exists(shroom) {
		shroom_type = shroom.object_index;
	}
	if place_meeting(x,y,oSticker) {
		instance_destroy();	
		exit;
	}
	if place_meeting(x,y,oBag) {
		instance_destroy();	
		exit;
	}
	if place_meeting(x,y,oBagInterior) {
		if !place_meeting(x,y,oSticker) {
			in_bag = true;
		} else {
			instance_destroy();
			exit;
		}
	}
	
	if oInTheBag.hover_done {
		instance_destroy();
		exit;
	}
	if room == rmCylveyClockTower and !place_meeting(x,y,oBagInterior) {
		instance_destroy();
		exit;
	}
		
	oInTheBag.holding = false;
} else {
	if instance_exists(shroom) {
		shroom_type = shroom.object_index;
	}
	if place_meeting(x,y,oSticker) {
		instance_destroy();	
		exit;
	}
	if oInTheBag.hover_quit {
		instance_destroy();
		exit;
	}
	var npc_bag_int = noone;
	with oBagInterior {
		if type == 1 { npc_bag_int = self;}
	}
	if place_meeting(x,y,oBag) or (npc_bag_int != noone and place_meeting(x,y,npc_bag_int)) {
		instance_destroy();	
		exit;
	}
	var bag = instance_place(x,y,oBagInterior) 
	if bag != noone {
		if !place_meeting(x,y,oSticker) {
			if bag.type == 0 {
				in_bag = 1;
			} else {
				in_bag = -1;
			}
		} else {
			instance_destroy();
			exit;
		}
	}
	oInTheBag.holding = false;
}