{
    "id": "bded2c77-a777-40e8-a8cd-4cc3a7b72395",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSticker",
    "eventList": [
        {
            "id": "0528cf87-e1f2-4152-a286-d59502966b24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bded2c77-a777-40e8-a8cd-4cc3a7b72395"
        },
        {
            "id": "a5d2716d-7b50-47c3-a829-c21e61e1f350",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "bded2c77-a777-40e8-a8cd-4cc3a7b72395"
        },
        {
            "id": "e57af6c2-831e-4a28-9587-01027df9cc98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "bded2c77-a777-40e8-a8cd-4cc3a7b72395"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}