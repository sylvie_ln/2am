{
    "id": "df6c1366-f3ff-41a5-bc31-50f8d88f693d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBunneyAmulet",
    "eventList": [
        {
            "id": "4b811f0d-0b12-46c0-977c-aed2cfb0354c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df6c1366-f3ff-41a5-bc31-50f8d88f693d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e8c6f6dd-9217-4324-9ad0-3379e2b0ef33",
    "visible": true
}