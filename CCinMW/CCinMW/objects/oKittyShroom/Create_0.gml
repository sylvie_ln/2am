event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A squishey and fluffey kittey shroom, it is so cute.\nPassive Effect: Kittey Kittey Jump!"

damp = 0.6;
bounce = damp;
fric = 0.95;
grav /= 2;

jmp = sqrt(2*grav*81);
spd = 1;
hv = sylvie_choose(spd,-spd);

if room != rmCoolMuseum {
	image_index = sylvie_irandom(sprite_get_number(sprite_index)-1);
}
image_speed = 0;