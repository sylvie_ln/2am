event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A foreign currency called dollars. It is known for being packaged in comically large bags."

damp = 0.5;
bounce = damp;
fric = 0.95;
grav *= 4;

image_speed = 0;

/// In oInit, define a trade entry for this item!