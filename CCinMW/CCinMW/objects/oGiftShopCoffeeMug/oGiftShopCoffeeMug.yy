{
    "id": "49a2241b-604d-4792-b072-f2d9a1e2a7bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGiftShopCoffeeMug",
    "eventList": [
        {
            "id": "0b8072a4-26a8-4b7e-a2a7-b826119b9a54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "49a2241b-604d-4792-b072-f2d9a1e2a7bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e451e01-7b28-475b-b2af-41327c48a609",
    "visible": true
}