{
    "id": "d98355fa-ed01-4b02-b403-05fc2a016a3a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMiniBunney",
    "eventList": [
        {
            "id": "00014cfb-ab50-4640-ad1a-aaf626b00e99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d98355fa-ed01-4b02-b403-05fc2a016a3a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7f036cd-dbfa-4109-bad9-41553dc4909e",
    "visible": true
}