{
    "id": "409fa220-a5a5-418e-b86a-129207239f85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMuseum",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "48c12c2f-c326-4494-b6f7-dae1ed760e9d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
    "visible": true
}