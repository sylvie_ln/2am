var sc = (global.scale == -1 ? global.max_scale : global.scale);
var hpos = 
global.fullscreen ? (dw-sc*global.view_width) div 2 : 0;
var vpos = 
global.fullscreen ? (dh-sc*global.view_height) div 2 : 0;
global.game_pos = [hpos,vpos];
var gw = sc*global.view_width;
var gh = sc*global.view_height
if global.fullscreen and global.scale != global.max_scale {
	draw_set_color(c_border);
	//draw_sprite_tiled(sFSBG,0,0,0)
	draw_rectangle(hpos-b,vpos-b,hpos+gw+b-1,vpos+gh+b-1,false)
	draw_set_color(c_black);
	draw_rectangle(hpos,vpos,hpos+gw-1,vpos+gh-1,false)
}
surface_resize(application_surface,global.view_width,global.view_height);
gpu_set_blendmode_ext(bm_one,bm_inv_src_alpha);
draw_surface_stretched(application_surface,
hpos,vpos,
gw,gh);
gpu_set_blendmode(bm_normal)