if (round(true_mouse(0)) != px or round(true_mouse(1)) != py or mouse_check_button_pressed(mb_left)) and !start and ps == global.scale and pf == global.fullscreen {
	if (!(instance_exists(oCursor) and oCursor.active) and !instance_exists(oInTheBag))
	or room == rmContract
	or room == rmMinecraft {
		window_set_cursor(cr_default);
		alarm[0] = room_speed;
	}
	if os_type == os_macosx {
		with oCursor { offset = [mouse_x,mouse_y]; }
	}
	mcount=0;
} else {
	mcount++;
	if mcount > room_speed {
		event_perform(ev_alarm,0);
		mcount = 0;
	}
}
var wmx = window_mouse_get_x();
var wmy = window_mouse_get_y();
var ww = window_get_width();
var wh = window_get_height();
if wmx < 0 or wmx >= ww or wmy < 0 or wmy >= wh {
	window_set_cursor(cr_default);	
} else if instance_exists(oInTheBag) {
	window_set_cursor(cr_none);	
}
px = round(true_mouse(0));
py = round(true_mouse(1));
ps = global.scale;
pf = global.fullscreen;
start = false;