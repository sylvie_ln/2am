{
    "id": "09390596-9ddb-4190-be13-17a4a97aa117",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCanyonDark",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "48c12c2f-c326-4494-b6f7-dae1ed760e9d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "da43a3f2-eaae-4b39-b783-4b39b0b9e8f4",
    "visible": true
}