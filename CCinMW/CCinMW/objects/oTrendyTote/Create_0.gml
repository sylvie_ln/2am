event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A very trendy tote bag. Your not supposed to actually be able to get one."
damp = 0.75;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!