{
    "id": "acabb1dd-885b-4889-a4a3-f1aa7317f798",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSnakeRope",
    "eventList": [
        {
            "id": "9f26996b-5749-46f6-bfe2-c7b0c14898a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "acabb1dd-885b-4889-a4a3-f1aa7317f798"
        },
        {
            "id": "bf061b00-08f8-4cbd-8cbf-aa1a733f5f8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "acabb1dd-885b-4889-a4a3-f1aa7317f798"
        },
        {
            "id": "a15f7473-5ab9-42c4-9b99-a81b7d6fd236",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "acabb1dd-885b-4889-a4a3-f1aa7317f798"
        },
        {
            "id": "1624f7e0-5fee-4561-8163-b6e52f1fadf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "acabb1dd-885b-4889-a4a3-f1aa7317f798"
        },
        {
            "id": "a325837f-34c2-4b00-9d21-fa100e3bb6f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "acabb1dd-885b-4889-a4a3-f1aa7317f798"
        },
        {
            "id": "5e8883e8-1d4f-488f-8ea7-cfe16daebff4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "acabb1dd-885b-4889-a4a3-f1aa7317f798"
        }
    ],
    "maskSpriteId": "f534d4cb-120b-4bbe-8460-51f496df1d01",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f534d4cb-120b-4bbe-8460-51f496df1d01",
    "visible": true
}