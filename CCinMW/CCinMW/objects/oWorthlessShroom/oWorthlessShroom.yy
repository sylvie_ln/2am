{
    "id": "4039b875-03f8-43e7-9790-efbfe3fb45a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWorthlessShroom",
    "eventList": [
        {
            "id": "8a7f0464-d81a-44ae-b3eb-8802620d92d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4039b875-03f8-43e7-9790-efbfe3fb45a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c91a7e70-cc93-4d5a-b2f0-13b69fa3cb7b",
    "visible": true
}