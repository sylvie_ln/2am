///@description
if global.flag_dice == 0 and global.thrown_dice_count >= 3 {
	var six = 0;
	with oDice {
		if floor(image_index) == 10 and image_speed == 0 and thrown_dice and settled {
			six++;
		}
	}
	if six == global.thrown_dice_count and global.flag_dice == 0 {
		global.flag_dice = 1; 
		alarm[0] = global.default_room_speed;
	}
}