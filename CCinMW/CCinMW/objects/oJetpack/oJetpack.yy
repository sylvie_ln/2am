{
    "id": "b683087f-e6b8-4088-bc91-71820fb0f20a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oJetpack",
    "eventList": [
        {
            "id": "67d82d1f-0a96-4c8d-acf4-e5fbc3c3888e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b683087f-e6b8-4088-bc91-71820fb0f20a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "88e21c0b-72e1-4ddd-ac18-b8358423a783",
    "visible": true
}