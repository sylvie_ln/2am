event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A jetpack that's fun to use and cute. It rapidly consumes air to create thrust-propulsion in a very scientific manner. Hold Jump while wearing it to jet."

damp = 0.5;
bounce = damp;
hdamp = false;
fric = 0.95;
grav = -grav/2;

image_speed = 0;

/// In oInit, define a trade entry for this item!