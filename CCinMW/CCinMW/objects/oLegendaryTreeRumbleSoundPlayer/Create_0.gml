if !global.mute_sfx {
	audio_sound_gain(sndLegendaryTreeRumble,global.sfx_volume*get_gain(sndLegendaryTreeRumble),0);
	sound = audio_play_sound(sndLegendaryTreeRumble, -1, true);
	audio_sound_gain(sound, 0, 0);
} else {
	sound = noone;	
}