{
    "id": "7d05b860-4bb7-44b7-90b7-b4caee6815a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLegendaryTreeRumbleSoundPlayer",
    "eventList": [
        {
            "id": "f815eea3-2fd8-412c-8e98-ceca54358cec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d05b860-4bb7-44b7-90b7-b4caee6815a4"
        },
        {
            "id": "ef69cb70-1980-4547-81ca-91600c34a6ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "7d05b860-4bb7-44b7-90b7-b4caee6815a4"
        },
        {
            "id": "6b924856-e1db-48b7-9429-162bc5e47cff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d05b860-4bb7-44b7-90b7-b4caee6815a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}