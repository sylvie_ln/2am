if !active { exit; }
draw_set_font(global.neco_font);
var hpos = x;
var vpos = y;
var mlen = 0;
for(var i=0; i<total; i++) {
	var txt = option[i];
	draw_text(hpos,vpos,txt);	
	vpos += string_height(txt);
	mlen = max(mlen,string_width(txt));
}
hpos += mlen+10;
vpos = y;
for(var i=0; i<total; i++) {
	var choices = self.choices[i];
	var ww = 24;
	if i == selected {
		//var r = 2;
		draw_text(x-7,vpos,"*");
		//draw_circle(x-5,vpos+3,r,false);
	}
	switch(i) {
		case graphic_options.scale:
			var txt = string(choices[option_choice[i]])+"x";
		break;
		case graphic_options.fullscreen:
			var txt = choices[option_choice[i]] ? "Yes" : "No";
		break;
		default:
			continue;
	}
	if i == selected {
		draw_text(hpos-6,vpos,"<");
		draw_text(hpos+ww,vpos,">");
	}
	draw_text_centered(hpos+(ww div 2),vpos,txt);	
	vpos += string_height(option[i]);
}