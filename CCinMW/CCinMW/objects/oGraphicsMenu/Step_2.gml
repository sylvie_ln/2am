if !active { exit; }
if input_pressed("Escape")
or (selected == graphic_options.done and input_pressed("Jump")) {
	active = false;
	play_menu_backward();
	with parent { event_user(0); }
	exit;
}

if input_pressed_repeat("Up") {
	selected--;
	if selected < 0 {
		selected = total-1;	
	}
	play_menu_cursor_move();
}
if input_pressed_repeat("Down") {
	selected++;
	if selected >= total {
		selected = 0;
	}
	play_menu_cursor_move();
}
var change = false;
if input_pressed_repeat("Left") {
	option_choice[selected]--;
	if option_choice[selected] < 0 {
		if array_length_1d(choices[selected]) == 2 {
			option_choice[selected] = 1;	
			change = true;	
			play_menu_adjust();
		} else {
			option_choice[selected] = 0;
			play_menu_error();
		}
	} else {
		change = true;	
		play_menu_adjust();
	}
}
if input_pressed_repeat("Right") {
	option_choice[selected]++;
	if option_choice[selected] >= array_length_1d(choices[selected]) {
		if array_length_1d(choices[selected]) == 2 {
			option_choice[selected] = 0;	
			change = true;
			play_menu_adjust();
		} else {
			option_choice[selected] = array_length_1d(choices[selected])-1;	
			play_menu_error();
		}
	} else {
		change = true;
		play_menu_adjust();
	}
}

if change {
	var was_fs = global.fullscreen;
	var ca = choices[selected];
	if option_var[selected] != "" {
		variable_global_set(option_var[selected],ca[option_choice[selected]]);
	}
	switch(selected) {
		case graphic_options.fullscreen:
			if os_type != os_windows and !global.fullscreen and global.scale >= global.max_scale {
				global.scale = global.max_scale -1;	
				option_choice[graphic_options.scale] = max(0,array_length_1d(choices[graphic_options.scale])-2);	
			}
			window_set_fullscreen(global.fullscreen);
			if os_type == os_macosx and oInput.alarm[5] < 0 {
				with oInput { event_perform(ev_alarm,5); }
			}
		case graphic_options.scale:
			if !global.fullscreen {
				window_set_scale(global.scale);
				if was_fs and os_type == os_macosx {
					// Do nothing
				} else {
					alarm[1] = 1;
				}
			}
		break;
	}
}