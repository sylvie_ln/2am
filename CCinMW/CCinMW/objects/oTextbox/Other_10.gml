/// @description Finish printing text
can_play_type_sound = false;

do {
    event_perform(ev_step,ev_step_normal);
} until finished;

can_play_type_sound = true;

if global.assist_textspeed != 2 {
	play_sound(sndDialogForce);
} else {
	tick = 1;
	play_textbox_type_sound(npc);
}