draw_set_font(global.neco_font);
// calculate textbox dimensions and positions
var left = npc.x - (width div 2) - border;
var right = npc.x + (width div 2) + border;
var top = npc.bbox_top - height - border*3;
var bottom = npc.bbox_top - border;
var left_min = camera_get_view_x(view_camera[0])+border;
var right_max = camera_get_view_x(view_camera[0])+global.view_width-border;
var top_min = camera_get_view_y(view_camera[0])+border;
var bottom_max = camera_get_view_y(view_camera[0])+global.view_height-border;
if top < top_min {
	top = npc.bbox_bottom + border;
	bottom = npc.bbox_bottom + height + border*3;
}
var shift;
if left < left_min {
    shift = left_min-left;
    left += shift;
    right += shift;
}
if right > right_max {
    shift = right_max-right;
    left += shift;
    right += shift;
}
if top < top_min  {
    shift = top_min-top;
    top += shift;
    bottom += shift;
}
if bottom > bottom_max {
    shift = bottom_max-bottom;
    top += shift;
    bottom += shift;
}

// draw question answer boxes
if finished and !ds_list_empty(questions) {
    var question_total_width = 0;
    var question_width;
    var question_pos;
    for(var i=0; i<ds_list_size(questions); i++) {
        var question = questions[|i];
        question_width[i] = string_width(question)+border*2;
        question_pos[i] = (question_width[i] div 2) + question_total_width;
        question_total_width += question_width[i];
        if i < ds_list_size(questions)-1 {
            question_total_width += border;
        }
    }
    var hpos = npc.x - (question_total_width div 2);
    hpos = max(hpos,left_min);
    for(var i=0; i<ds_list_size(questions); i++) {
        var qleft = hpos+question_pos[i]-(question_width[i] div 2);
        var qright = hpos+question_pos[i]+(question_width[i] div 2);
        var qtop = bottom+border;
        var qbottom = bottom+border*3+string_height("Fluffey");
        draw_set_color(c_black);
        draw_roundrect(qleft,qtop,qright,qbottom,false);
        draw_set_color(c_white);
        if i == answer {
            draw_set_alpha(0.25);
            draw_roundrect(qleft,qtop,qright,qbottom,false);
            draw_set_alpha(1);
        }
        draw_roundrect(qleft,qtop,qright,qbottom,true);
    }
    for(var i=0; i<ds_list_size(questions); i++) {
        draw_text_centered(hpos+question_pos[i],bottom+border*2,questions[|i]);
    }
}

// draw the main textbox
draw_set_color(c_black);
draw_roundrect(left,top,right,bottom,false);
draw_set_color(c_white);
draw_roundrect(left,top,right,bottom,true);
// to do text effects like color changing, we need to draw text character by character
draw_set_halign(fa_left);
var hpos = left+border;
var vpos = top+border;
var lb = 0;
for(var i=1; i<=string_length(prefix); i++) {
	if added_lb[?i] { lb++; }
    if ds_map_exists(effects,i-lb) {
        var effect = effects[? i-lb];
        switch(effect[0]) {
            case text_effects.color:
                draw_set_color(effect[1]);
            break;
        }   
    }
    if string_copy(prefix,i,string_length("\n")) == "\n" {
        vpos += string_height("\n");
        hpos = left+border;
    } else {
        var c = string_char_at(prefix,i);
        draw_text(hpos,vpos,c);
        hpos += string_width(c);   
    }
}
draw_set_halign(fa_left);