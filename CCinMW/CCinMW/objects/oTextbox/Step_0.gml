// set the finished flag if we've reached the end of the text
if pos > string_length(text) {
    finished = true;
}

// Selecting answers to questions
if finished and !ds_list_empty(questions) {
    if input_pressed("Left") {
        if answer == -1 {
            answer = 0;
        } else {
            answer--;
            if answer < 0 {
                answer = ds_list_size(questions)-1;
            }
        }
    }
    if input_pressed("Right") {
        if answer == -1 {
            answer = ds_list_size(questions)-1;
        } else {
            answer++;
            if answer >= ds_list_size(questions) {
                answer = 0;
            }
        }
    }
}

play_dialog_select_sound();

var skippy = 1;
if global.assist_textspeed == 1.5 {
	skippy = global.default_room_speed div 7;
}
var waity = 1;
if global.assist_textspeed == 0.5 {
	waity = global.default_room_speed div 15;	
} else if global.assist_textspeed == 0 {
	waity = global.default_room_speed div 5;	
}

if stepc mod waity == 0 {
	var first = true;
	repeat(skippy) {
		// display text one character at a time
		while pos <= string_length(text) {
		    var c = string_char_at(text,pos);
    
		    // if you hit a linebreak, add it to the string, then skip over whitespace
		    if pos == ds_queue_head(linebreaks) {
				added_lb[? pos] = true;
		        ds_queue_dequeue(linebreaks);
		        prefix += "\n";
		        while ord(c) <= ord(" ") {
		          pos++;
		          c = string_char_at(text,pos);
		        }
		        continue;
		    }
    
		    // otherwise, add one character to the displayed text
		    prefix += c;
		    pos++;
			added_lb[? pos] = false;
	
			if (first and can_play_type_sound) {
				play_textbox_type_sound(npc);
				first = false;
			}
		    break;
		}
	}
}
stepc++;