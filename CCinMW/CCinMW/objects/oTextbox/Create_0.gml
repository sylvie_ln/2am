text = "Juicy JIGGLER"
npc = noone;
prefix = "";
pos = 1;
width = -1;  // calculated in user event 1
height = -1; // calculated in user event 1
max_width = global.view_width;
border = 6;
linebreaks = ds_queue_create();
finished = false;
questions = ds_list_sylvie_create();
answer = -1;
can_play_type_sound = true;

effects = ds_map_create();
added_lb = ds_map_create();
enum text_effects {
    color
}

enum eTextbox {
    finish,
    init
}

stepc=0;