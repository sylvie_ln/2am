{
    "id": "89f343da-3a76-4f39-9d0d-0feae8f9387e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oInTheBag",
    "eventList": [
        {
            "id": "9c21d8e2-06ce-4915-81b8-0bd82f62e44b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "a78a2fee-a801-477a-b7f2-b219f7905345",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "8b94fdca-eba5-4a5e-a760-f52cdebecd6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "99381670-dd38-4fff-b768-113069ca7753",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "1e4df68d-7d8b-4c17-834b-c2936fe9d6a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "820f9680-0113-4fcf-a8b3-f208adf3b02d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "2f1c1162-109f-4bb8-882e-6090d9361d87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "b7bad2a8-42ba-4135-9415-bf566dc94265",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "321abab6-30bb-4e0e-bf6f-1f54dc349665",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "6a57ff49-f80c-4498-b761-b4f9cc3f4dbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "886ecb9e-c414-4c18-86c6-5c19bf9d8b98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "836bc262-cd4b-4c71-87b1-7f9e9cdfe589",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "f0472a5e-1172-4137-b42a-46ffe9eacf3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "b6dde6dd-1de4-4b31-8552-f868cf8d0f7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 2,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "95e731bf-486a-4754-8f10-00eb1222f1a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        },
        {
            "id": "fdfe09e3-b5d9-4d78-8a64-c1ef964f7de0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "89f343da-3a76-4f39-9d0d-0feae8f9387e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}