draw_set_alpha(0.9);
draw_circle_color(xp,yp,point_distance(xp,yp,cr,cb)+1,merge_color(c_white,c_black,0.75),c_black,false);
draw_set_alpha(1);


if mode == 0 {
	draw_sprite(sPutItInTheBag,0,cl,ct);
} else if mode == 1 {
	draw_sprite(sGottaSortMyBag,0,cl,ct);
} else if mode == 2 {
	draw_sprite(sTradingTime,0,cl,ct);
}
//draw_text(cl,ct + (ch div 2),"original: "+string(original));
//draw_text(cl,ct + (ch div 2)+16,"shroom type: "+object_get_name(shroom_type));
if global.first_bag and mode != 2 {
	draw_sprite(sUseYaMouse,0,cl,cb);
}

if mode != 2 {
	if holding and cursor_in_window {
		var xx = smouse_x + holding_offset_x;
		var yy = smouse_y + holding_offset_y;
		sprite_index = sticker;
		var fo = 4;
		var dir = round(point_direction(oBag.x,oBag.y,xx,yy));
		if !place_meeting(xx,yy,oBag) and !place_meeting(xx,yy,oBagInterior) {
			draw_set_color(c_white);
			draw_line_color(
			xx,
			yy,
			xx+lengthdir_x(max(xx-cl,cr-xx)+fo,dir),
			yy+lengthdir_y(max(yy-ct,cb-yy)+fo,dir),
			c_red,c_fuchsia);
			//var xt=clamp(xx+lengthdir_x(round(dx*ff+fo)+dout,dir),cl,cr);
			//var yt=clamp(yy+lengthdir_y(round(dy*ff+fo)+dout,dir),ct,cb);
			/*
			draw_line_color(
			xt-lengthdir_x(fo,ddir),yt-lengthdir_y(fo,ddir),
			xt+lengthdir_x(fo+(global.view_width div 2),ddir),
			yt+lengthdir_y(fo+(global.view_height div 2),ddir),
			c_red,c_fuchsia);
			draw_triangle_color(
				xt,yt,
				xt-lengthdir_x(dout,dir-fa),
				yt-lengthdir_y(dout,dir-fa),
				xt-lengthdir_x(dout,dir+fa),
				yt-lengthdir_y(dout,dir+fa),
				c_red,c_white,c_white,
				false);
			*/
		}
		sprite_index = -1;
	} 
	with oSticker {
		if !laser_me { continue; }
		var xx = x;
		var yy = y;
		var fo = 4;
		var dir = round(point_direction(oBag.x,oBag.y,xx,yy));
		if !in_bag {
			draw_set_color(c_white);
			draw_line_color(
			xx,
			yy,
			xx+lengthdir_x(max(xx-other.cl,other.cr-xx)+fo,dir),
			yy+lengthdir_y(max(yy-other.ct,other.cb-yy)+fo,dir),
			c_red,c_fuchsia);	
		}
	}
}

if mode == 2 and instance_exists(npc) {
	with npc {
		var px = x;
		var py = y;
		image_xscale = 2;
		image_yscale = 2;
		x = other.cl+4+8*image_xscale;
		y = other.cb-4-8*image_yscale;
		
		event_perform(ev_draw,0);
		
		//text = "Juicy JIGGLER\n10x BasicShroom\nA juicey little friendley JIGGLER.\nNo warranty."
		event_perform_object(oNPCArrow,ev_other,ev_user0);
		image_xscale = 1;
		image_yscale = 1;
		x = px;
		y = py;
	}
	play_trade_npc_sound();
	var bears, minib, chice, buney;
	var bears = false;
	var minib = false;
	var chice = false;
	var buney = false;
	with oSticker {
		if shroom_type == oBunneyEars and in_bag == 1 { bears = true; }
		if shroom_type == oMiniBunney and in_bag == 1 { minib = true; }
		if shroom_type == oChickenWings and in_bag == 1 { chice = true; }
		if shroom_type == oBunneyAmulet and in_bag == 1 { buney = true; }	
	}
	if bears {
		draw_sprite_ext(sBunneyEars,0,cr-4-16,cb-4-16-10-8,
		-2,2,image_angle,image_blend,image_alpha);
	}
	if minib {
		draw_sprite_ext(sMiniBunney,0,cr-4-16,cb-4-16-10-16,
		-2,2,image_angle,image_blend,image_alpha);
	}
	if chice {
		draw_sprite_ext(sCharWings,0,cr-4-16,cb-4-16,
		-2,2,image_angle,image_blend,image_alpha);
	}

	draw_sprite_ext(buney ? sBunneyS : sCharS,0,cr-4-16,cb-4-16,-2,2,0,c_white,1);
}

