var thrown_items_count = 0;

global.first_bag = false;
global.closed_bag = true;
if mode == 2 {
	npc.text = npc.orig_text;	
}
if done {
	if mode == 2 {
		ds_list_sylvie_destroy(global.bag_contents);
		global.bag_contents = ds_list_sylvie_create();
	} else {
		for(var i=0; i<ds_list_size(global.bag_contents); i++) {
			var bc_info = global.bag_contents[|i];
			if asset_get_index(bc_info[|bag_contents_info.type]) == oSpike {
				continue;	
			}
			ds_list_sylvie_destroy(bc_info);
			ds_list_delete(global.bag_contents,i);
			i--;
		}	
	}
}

if done {
	with shroom {
		if room != rmCoolMuseum {
			if generated and instance_exists(oRoomGen) {
				oRoomGen.local_shroom_deletion_map[? room_key(oRoomGen.xp,oRoomGen.yp)+pos_key(xt,yt)] = object_index;
				if rare {
					global.rare_dead[? oRoomGen.area+"_"+room_key(oRoomGen.xp,oRoomGen.yp)] = true;
				} 
				if cute {
					global.cute_dead[? oRoomGen.area+"_"+room_key(oRoomGen.xp,oRoomGen.yp)] = true;
				}
			}
			if placed and !instance_exists(oRoomGen) and !always_respawn {
				global.shroom_deletion_map[? get_that_room()+pos_key(xstart,ystart)] = true;
			}
		}
		generated = false;
		placed = false;	
	}
}

if mode != 2 {
	with oSticker {
		if !in_bag and shroom_type == oDice {	
			global.thrown_dice_count = 0;
			with oDice {
				thrown_dice = false;	
			}
		}
	}
}
with oSticker { 
	if other.done {
		if in_bag {
			global.collected[? object_get_name(shroom_type)+"_"+string(floor(image_index))] = true;
			enum bag_contents_info {
				xp,
				yp,
				type,
				ii,
				last
			}
			if other.mode == 2 or shroom_type != oSpike {
				ds_list_add(global.bag_contents,list_create(x-oBag.x,y-oBag.y,object_get_name(shroom_type),image_index));
				ds_list_mark_as_list(global.bag_contents,ds_list_size(global.bag_contents)-1);
			}
			if shroom_type == oHandOfDarkness {
				global.flag_hand_darkness = 1;	
			}
			if instance_exists(shroom) {
				with shroom { 
					instance_destroy(); 
				}
			}
		} else {
			if other.mode != 2 or (other.mode == 2 and in_bag == 0) {
				if !instance_exists(shroom) {
					shroom = instance_create_depth(oChar.x,oChar.y,depth,shroom_type);
					shroom.placed = false;
				}
				with shroom {
					thrown_items_count++;
					if object_index == oDice {
						thrown_dice = true;
						settled = false;
						global.thrown_dice_count++;	
						//disabled_show_debug_message(global.thrown_dice_count);
					}
					if object_index == oSnakeRope {
						mask_index = sSnakeHitbox;
						image_yscale = 1;
					}
					image_index = other.image_index;
					if object_index == oPuzzlePiece {
						visible = true;
						upblocker = true;
						pushable = true;
						carriable = true;
						pusher = true;
						carrier = true;
					}
					var dir = round(point_direction(oBag.x,oBag.y,other.x,other.y));
					var spd = 6;
					if !(object_index == oHandOfDarkness and waiting) {
						x = oChar.x;
						y = oChar.y;
					}
					/*
					var factor = (dir mod 90);
					if factor <= 45 {
						factor = factor/45;
					} else {
						factor = 2-(factor/45);	
					}
					factor += 1;
					*/
					var factor = 1;
					hv = lengthdir_x(spd,dir);
					vv = lengthdir_y(spd*factor,dir);
					//show_debug_message([1,x,y]);
					if dir >= 180+30 and dir <= 360-30 {
						var prey = y;
						y = oChar.bbox_bottom + y - bbox_top + 1;
						if solid_at(x,y) { y = prey; }
						if !input_held("Jump") {
							oChar.hv = clamp(oChar.hv,-2,2);
						} else {
							oChar.walkoff_timer = oChar.walkoff_time	
						}
						oChar.vv = 0;
						throwdown = true;	
					}
					//show_debug_message([2,oChar.bbox_bottom,bbox_top]);
					var pre = [x,y];
					var try = [dir,dir-180,dir-180+45,dir-180-45,dir-90,dir+90];
					for(var i=0; i<array_length_1d(try); i++) {
						var dir = try[i];
						var max_shift = 8+point_distance(0,0,lengthdir_x(bbox_right-bbox_left,dir),lengthdir_y(bbox_bottom-bbox_top,dir))/2;
						//disabled_show_debug_message(max_shift);
						while true {
							if !solid_at(round(x),round(y)) {
								x = round(x);
								y = round(y);
								i = array_length_1d(try);
								break;
							}
							x += lengthdir_x(1,dir);
							y += lengthdir_y(1,dir);
							max_shift--;
							if max_shift < 0 {
								x = pre[0];
								y = pre[1];
								break;
							}
						}
					}
					//show_debug_message([3,x,y]);
					if outside_view() {
						x = pre[0];
						y = pre[1];
					}
					//show_debug_message([4,x,y]);
					
					if object_index == oSnakeRope {
						x = oChar.x;
						y = oChar.y;
					}
					
					thrown_timer = thrown_time;
					if false and spinny {
						throwspin = true;	
						spin_dir = sign(hv)*sign(vv);
						if spin_dir == 0 { spin_dir = sylvie_choose(-1,1); }
					}
					no_carry = true;
					
					event_user(0);
				}
			}
		}
	}
	instance_destroy(); 
}

with oBag { instance_destroy(); }
with oBagInterior { instance_destroy(); }

if thrown_items_count > 0
	play_sound(sndThrowBagItems);
else
	play_sound(sndBagClose);


/// NOTEs: 
/// Racerunner flag doesn't use this system.
/// Minecart flag doesn't use this system.
switch(what_you_got) {
	case oLensOfLies:
		global.flag_puzzler = 1;
	break;
	case oFishCar:
		global.flag_fishcar = 1;
	break;
	case oWizardmobile:
		global.flag_wizardmobile = 1;
	break;
	case oSnowKeety:
		global.flag_snowkeety = 1;
	break;
	case oSylvieTruck:
		global.flag_sylvietruck = 1;
	break;
	case oHouse:
		global.flag_house = 1;
		npc.orig_text = npc.orig_alt_text;
	break;
	case oHorse:
		global.flag_horse = 1;
	break;
	case oPinkPaperclip:
		if npc.name == "Country" {
			global.flag_pinkpaperclip = 1;
			npc.orig_text = npc.orig_alt_text;
		}
	break;
	case oChickenWings:
		global.flag_chicen = 1;
	break;
	case oGapKey:
		global.flag_gap = 1;
		npc.orig_text = npc.orig_alt_text;
	break;
	case oHandOfThunder:
		global.flag_hand_thunder = 1;
	break;
	case oHandOfLeafs:
		global.flag_hand_leafs = 1;
	break;
	case oHandOfEarth:
		global.flag_hand_earth = 1;
	break;
	case oHandOfLight:
		global.flag_hand_light = 1;
	break;
	case oHandOfFlames:
		global.flag_cheesey = 1;
		global.flag_hand_flames = 1;
		npc.orig_text = npc.orig_alt_text;
	break;
	case oHandOfTears:
		global.flag_kittey = 1;
		global.flag_hand_tears = 1;
	break;
	case oHandOfLies:
		global.flag_lies = 1;
		global.flag_hand_lies = 1;
	break;
	case oHandOfDreams:
		global.flag_rainbow = 1;
		global.flag_hand_dreams = 1;
		with npc {
			event_user(1);	
		}
	break;
	case oHandOfStorms:
		global.flag_hand_storms = 1;
	break;
	case oHandOfSky:
		global.flag_hand_sky = 1;
	break;
	case oSurpriseBox:
		if npc.name == "Rainbis" {
			with npc {
				flag_too_bad = 1;
				event_user(1);	
			}	
		}
		if npc.name == "Creamy" {
			global.flag_kittey = 1;
		}
	break;
	case oDecapService:
		bag_remove_item(oDecapService);
		global.flag_decap = 1;
		with npc {
			event_user(1);	
		}
	break;
	case oRedPaperclip:
		var has_clips = false;
		if ds_map_exists(global.npc_bag_map,npc.name) {
			var bag_sticker_list = global.npc_bag_map[? npc.name];
			for(var i=0; i<ds_list_size(bag_sticker_list); i++) {
				var st_info = bag_sticker_list[|i];
				var st_type = asset_get_index(st_info[|npc_bag_sticker_info.type]);
				if st_type == oRedPaperclip {
					has_clips = true;
					break;
				}
			}
		}
		if !has_clips {
			global.flag_paperclips = 1;
			npc.orig_text = npc.orig_alt_text;
			with npc {
				event_user(1);	
			}
		}
	break;
}

if global.heist_stage == 2 {
	with npc {
		if name == "Alicia" {
			event_user(1);	
		}
	}
}

with oChar {
	alarm[5] = 1;
}

with oCursor {
	active = false;	
}