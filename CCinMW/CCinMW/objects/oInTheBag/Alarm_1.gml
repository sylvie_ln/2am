trade_ok = false;
trade_was_ok = false;
var nothing_selected = false;
with oSticker { wanted = false; }
if heist {
	trade_lock = true;
	with oSticker {
		if in_bag == 0 and owner == 1 {
			wanted = true;	
			global.heist_item = object_get_name(shroom_type);
			global.heist_ii = image_index;
			global.heist_stage = 2;
			var current = date_current_datetime();
			global.heist_date = date_inc_day(date_create_datetime(date_get_year(current),date_get_month(current),date_get_day(current),0,0,0),1);
			global.heist_date_string = "";
			switch(date_get_month(global.heist_date)) {
				case 1:  global.heist_date_string += "January "; break;
				case 2:  global.heist_date_string += "February "; break;
				case 3:  global.heist_date_string += "March "; break;
				case 4:  global.heist_date_string += "April "; break;
				case 5:  global.heist_date_string += "May "; break;
				case 6:  global.heist_date_string += "June "; break;
				case 7:  global.heist_date_string += "July "; break;
				case 8:  global.heist_date_string += "August "; break;
				case 9:  global.heist_date_string += "September "; break;
				case 10: global.heist_date_string += "October "; break;
				case 11: global.heist_date_string += "November "; break;
				case 12: global.heist_date_string += "December "; break;
			}
			global.heist_date_string += string(date_get_day(global.heist_date));
			global.heist_date_string += " "+string(date_get_year(global.heist_date));
		}
	}
	exit;
}
if holding {
	alarm[1] = global.default_room_speed;
} else {
	var empty_bag = true;
	with oSticker {
		if in_bag == 0 and owner == -1 {
			other.selected = id;
		}	
	}
	with oSticker {
		if other.selected != noone {
			var trade_item = other.selected.item;
		} else if in_bag == -1 {
			nothing_selected = true;
			var trade_item = item;
		}  else { continue; }
		var empty_bag = false;
		var threshold = trade_item[|item_data.threshold];
		var costmap = trade_item[|item_data.cost];
		var scor = 0;
		var exact = false;
		var garbage = false;
		var minigarbage = false;
		var multiple_exact = false;
		var good = false;
		var too_much = false;
		var too_much_count = 0;
		with oSticker {
			if in_bag != 0 { continue; }
			if item == trade_item { continue; }
			if ds_map_exists(costmap,object_get_name(shroom_type)) {
				// this sticker is worth something
				good = true;
				var value = costmap[? object_get_name(shroom_type)];
				if value == 0 {
					if !exact {
						exact = true;
					} else if !minigarbage {
						multiple_exact = true;
					}
				} else {
					minigarbage = true;
					multiple_exact = false;
					if scor >= threshold {
						too_much_count++;
						if too_much_count >= 3 {
							too_much = true;	
						}
					}
					scor += value;
				}
			} else {
				// this sticker is garbage
				garbage = true;
				multiple_exact = false;
				minigarbage = true;
			}
		}
		if other.npc.object_index == oHacker {
			exact = true;
			multiple_exact = false;
			minigarbage = false;
			too_much = false;
			garbage = false;
		}
		if exact or scor >= threshold {
			if other.selected == noone {
				other.selected = id;	
			}
			break;	
		}
	}
	if empty_bag {
		npc.text = npc.nothing_text;
	} else if exact or scor >= threshold {
		if multiple_exact {
			npc.text = npc.multiple_exact_text;
		} else if exact and minigarbage {
			npc.text = npc.minigarbage_text;
		} else if too_much {
			npc.text = npc.too_many_text;
		} else if garbage {
			npc.text = npc.garbage_text;
		} else {
			npc.text = npc.lets_text;
			with oSticker {
				if in_bag == 0 and owner == 1 {
					wanted = true;	
				}
			}
			trade_ok = true;
			if selected.in_bag == -1 {
				trade_selected = selected;
				orig_pos = [selected.x,selected.y];
				if orig_pos[0] == hex {
					was_on_head = true;	
					head_item_height = abs(selected.bbox_bottom-selected.bbox_top);
				} else {
					was_on_head = false;
					head_item_height = 0;
				}
				item_restock = trade_item[|item_data.restock];
				target_pos = [xp,yp];
				var stw = selected.bbox_right-selected.bbox_left;
				var sth = selected.bbox_bottom-selected.bbox_top;
				var count = 32;
				while collision_rectangle(
					target_pos[0]-(stw div 2),
					target_pos[1]-(sth div 2),
					target_pos[0]+(stw div 2),
					target_pos[1]+(sth div 2),oSticker,false,true) {
					target_pos[0] += sylvie_choose(-stw,0,stw);
					target_pos[1] += sylvie_choose(-sth,0,sth);
					target_pos[0] = clamp(target_pos[0],xp-40,xp+40);
					target_pos[1] = clamp(target_pos[1],yp-48,yp+32);
					count--;
					if count < 0 { target_pos = [xp,yp]; break; }
				}
			}
		}
	} else {
		if scor != 0 and abs(scor-threshold) <= 2 {
			npc.text = npc.almost_text;
		} else if garbage {
			if good {
				npc.text = npc.garbage_few_text;	
			} else {
				if !nothing_selected and asset_get_index(trade_item[|item_data.obj]) == first_item {
					npc.text = npc.first_worthless_text;
				} else if nothing_selected {
					npc.text = npc.worthless_nothing_text;	
				} else {
					npc.text = npc.worthless_text;
				}
			}
		} else {
			npc.text = npc.too_few_text; 	
		}
	}
	offer_evaluated = true;
}