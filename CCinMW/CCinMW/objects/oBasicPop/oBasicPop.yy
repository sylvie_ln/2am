{
    "id": "50fc8600-bfc6-4834-8b0b-b403835836d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBasicPop",
    "eventList": [
        {
            "id": "e4fa3550-f5c8-401e-a253-ba1910756995",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "50fc8600-bfc6-4834-8b0b-b403835836d2"
        },
        {
            "id": "fc18fc3e-e391-46df-9382-716e51f4d5cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "50fc8600-bfc6-4834-8b0b-b403835836d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d575649-441d-43d2-86d9-f646b788338d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
    "visible": true
}