{
    "id": "9b1ef094-f8e2-45a2-8e29-023187c3538f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRaceRunner",
    "eventList": [
        {
            "id": "a3f2f8aa-b227-4748-9be2-f4a9ddea96c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "9b1ef094-f8e2-45a2-8e29-023187c3538f"
        },
        {
            "id": "b03e93a5-30a4-4dca-a08d-02ad4c4e5262",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9b1ef094-f8e2-45a2-8e29-023187c3538f"
        },
        {
            "id": "26fb197a-ace4-495d-8247-9a920324551b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "9b1ef094-f8e2-45a2-8e29-023187c3538f"
        },
        {
            "id": "da16a299-8ee1-429e-b51f-fd8ceae68e6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "9b1ef094-f8e2-45a2-8e29-023187c3538f"
        },
        {
            "id": "45ba1084-6b06-4009-be47-418bee13b292",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9b1ef094-f8e2-45a2-8e29-023187c3538f"
        },
        {
            "id": "a9394652-e735-48b4-94b7-22898901a886",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "9b1ef094-f8e2-45a2-8e29-023187c3538f"
        },
        {
            "id": "a46ba01b-5367-404b-a21b-669c7f8641d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "9b1ef094-f8e2-45a2-8e29-023187c3538f"
        },
        {
            "id": "4faa3b9c-413f-4a19-91e2-07173cc76831",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "9b1ef094-f8e2-45a2-8e29-023187c3538f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f688b8-dd4d-4d42-96ce-776174c8176b",
    "visible": true
}