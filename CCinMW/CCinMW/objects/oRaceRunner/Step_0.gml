if paused { exit; }
if race_winner == noone {
	if bbox_right > oRaceFinishLine.x {
		race_winner = self;
	}
	if oChar.bbox_right > oRaceFinishLine.x {
		race_winner	= oChar.id;
	}
}
if flag_racing == 1 {
	var bb = lerp(1,boost,boost_timer/boost_time);
	if boost_timer > 0 {
		boost_timer--;
	}
	x += lengthdir_x(spd*bb,dir);
	y += lengthdir_y(spd*bb,dir);	
	if tick >= time {
		if x < oRaceFinishLine.x+16 {
			x = oRaceFinishLine.x;
			y = oRaceFinishLine.y+24;
			dir = point_direction(oRaceFinishLine.x,oRaceFinishLine.y+24,oRaceEndPoint.x,oRaceEndPoint.y);
			dist = point_distance(oRaceFinishLine.x,oRaceFinishLine.y+24,oRaceEndPoint.x,oRaceEndPoint.y);
			tick = 0;
			time = floor(dist/spd);
		} else {
			x = oRaceEndPoint.x;
			y = oRaceEndPoint.y;
			flag_racing = 2;
		}
	}
	tick+=bb;
}
if flag_racing == 3 {
	x += lengthdir_x(spd,dir);
	y += lengthdir_y(spd,dir);	
	if tick >= time {
		flag_racing = 0;
		x = oRaceStartPoint.x;
		y = oRaceStartPoint.y;
		event_user(5);
	}
	tick++;
}