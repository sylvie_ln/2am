event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "Sproing, it's a wonderful springy shroom.\nPassive Effect: Super Spring Jump"

damp = 0.95;
bounce = damp;
fric = 0.95;


image_speed = 0;