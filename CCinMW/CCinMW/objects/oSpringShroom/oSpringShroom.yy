{
    "id": "8d584385-a198-4280-b0bc-dc86dace15a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpringShroom",
    "eventList": [
        {
            "id": "a47d7a7c-7efa-4efa-b64c-a1e2d590f735",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d584385-a198-4280-b0bc-dc86dace15a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aeef3519-ad38-4007-9af2-f78a4dc3adcb",
    "visible": true
}