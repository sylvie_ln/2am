{
    "id": "02e0ea9a-72e5-4847-a25b-64698f7117b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGreenShard",
    "eventList": [
        {
            "id": "9bced82a-c16b-4a8c-b8f0-219e5dc2b6e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02e0ea9a-72e5-4847-a25b-64698f7117b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "42c4dc57-c6b4-42df-923d-19befea7bcbe",
    "visible": true
}