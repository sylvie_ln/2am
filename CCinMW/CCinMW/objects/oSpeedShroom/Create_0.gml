event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "This shroom speeds around so fast, you can barely keep up....\nPassive Effect: Sonic Speed"

damp = 0.6;
bounce = damp;
fric = 0.95;

grav /= 2;
jmp = sqrt(2*grav*36);
spd = 6.66;
hv = spd//sylvie_choose(spd,-spd);

image_speed = 0;