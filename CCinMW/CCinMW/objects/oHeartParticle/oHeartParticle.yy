{
    "id": "3b856e80-c511-4cfe-ade8-5224cce56c59",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHeartParticle",
    "eventList": [
        {
            "id": "7abc400c-f3ce-4a3e-b559-ff8504f6fe1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3b856e80-c511-4cfe-ade8-5224cce56c59"
        },
        {
            "id": "a9c96eaa-83fb-4ab7-bf57-2d9d440b1a2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3b856e80-c511-4cfe-ade8-5224cce56c59"
        },
        {
            "id": "9a51731f-1c46-4d95-a654-c74884df4fa6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3b856e80-c511-4cfe-ade8-5224cce56c59"
        },
        {
            "id": "3ef21bd3-4fd5-4ac7-a9d0-f592ddf796b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3b856e80-c511-4cfe-ade8-5224cce56c59"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d125949d-d049-4d28-8e5f-25b9ae17ee23",
    "visible": true
}