event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A lovely postcard featuring the Cavern of Ancients, it has a kittey on it, and it says \"Wish you werehere\" in a cursive font."

damp = 0.75;
bounce = damp;
fric = 0.95;
grav /= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!