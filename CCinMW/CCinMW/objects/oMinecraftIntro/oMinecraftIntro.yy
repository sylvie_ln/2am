{
    "id": "2ae6af0e-83d7-490d-9ad2-3fed505f4d13",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinecraftIntro",
    "eventList": [
        {
            "id": "b7fa7c8e-f636-475f-b4ed-b76862a671a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ae6af0e-83d7-490d-9ad2-3fed505f4d13"
        },
        {
            "id": "590a8f30-1886-46da-9ff3-061081ed2f91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2ae6af0e-83d7-490d-9ad2-3fed505f4d13"
        },
        {
            "id": "9394befd-35af-42a7-9ac6-4bbb73182b6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "2ae6af0e-83d7-490d-9ad2-3fed505f4d13"
        },
        {
            "id": "72a5ba73-b80c-4756-9979-613d7a38f6d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2ae6af0e-83d7-490d-9ad2-3fed505f4d13"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
    "visible": true
}