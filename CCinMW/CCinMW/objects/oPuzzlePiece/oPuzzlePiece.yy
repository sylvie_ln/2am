{
    "id": "5bd2fd2e-7e82-4028-afda-1d86f3ebdc58",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPuzzlePiece",
    "eventList": [
        {
            "id": "66d247f8-2daa-4113-ad8a-4446acc92e61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5bd2fd2e-7e82-4028-afda-1d86f3ebdc58"
        },
        {
            "id": "69d03525-2080-4d8f-9326-eac7057bd1bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5bd2fd2e-7e82-4028-afda-1d86f3ebdc58"
        },
        {
            "id": "3dfd2ff2-dea1-4115-8051-9c48720e391d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5bd2fd2e-7e82-4028-afda-1d86f3ebdc58"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "faebf91e-0da4-4229-935a-15f0d3936ca1",
    "visible": false
}