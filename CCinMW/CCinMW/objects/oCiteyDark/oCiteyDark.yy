{
    "id": "910b26dd-c51d-4dfb-bb6c-63372cd6be4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCiteyDark",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "48c12c2f-c326-4494-b6f7-dae1ed760e9d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "19b3a9f3-2a01-43c5-a223-eebc3eaa0b90",
    "visible": true
}