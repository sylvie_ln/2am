{
    "id": "a62eaf01-0768-4814-b23c-bd668ab8bc34",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGiftShopCavernRock",
    "eventList": [
        {
            "id": "f7f0695e-ba05-4ea0-bda4-24b451cbd0a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a62eaf01-0768-4814-b23c-bd668ab8bc34"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cda4ff8c-76d5-400a-a2fe-eb7453307099",
    "visible": true
}