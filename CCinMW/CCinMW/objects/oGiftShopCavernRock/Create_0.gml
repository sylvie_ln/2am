event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A rock from the walls of the Cavern of Ancients with a smiley face carved into it."

damp = 0.5;
bounce = damp;
fric = 0.95;
//grav *= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!