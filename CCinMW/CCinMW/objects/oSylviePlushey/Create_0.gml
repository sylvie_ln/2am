event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A sweet little doll. Very cute and squishy, although it's not popular."

damp = 0.95;
bounce = damp;
fric = 0.95;
grav /= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!