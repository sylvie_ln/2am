event_inherited();
if wait {
	mask_index = sBlackRose;
	if oChar.x >= x {
		lock = true;
	}
	if lock {
		oChar.x = x;
		if oChar.y >= y {
			event_user(1);
			oChar.npc = self;
			oChar.y = y;
			if oChar.bunney {
				oChar.sprite_index = sBunneyS;
			} else {
				oChar.sprite_index = sCharS;
			}
			//oChar.image_xscale = -1;
			wait = false;
		} else {
			mask_index = sBlackRoseMask;
		}
	}
	mask_index = sBlackRoseMask;
}
