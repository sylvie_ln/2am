{
    "id": "d86a02d9-4aa4-4283-a8ae-d06893e85f8b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSparkle",
    "eventList": [
        {
            "id": "5a00010d-1863-4a87-a5b3-1558f1de338c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d86a02d9-4aa4-4283-a8ae-d06893e85f8b"
        },
        {
            "id": "895efa33-ece3-41d1-9586-b70da8963667",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d86a02d9-4aa4-4283-a8ae-d06893e85f8b"
        },
        {
            "id": "c58e356f-ec7a-4ee3-b6e9-a976b4fbe95a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d86a02d9-4aa4-4283-a8ae-d06893e85f8b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d575649-441d-43d2-86d9-f646b788338d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f81c8118-729f-493c-9426-340f078cd9cc",
    "visible": true
}