if room == rmLegendaryTree {
	draw_sprite_ext(sSparkle,image_index,x,y,1,1,0,c_black,1);
} else {
	draw_self();
	draw_sprite_ext(sSparkleOutline,image_index,x,y,image_xscale,image_yscale,image_angle,color,image_alpha);
}