sylvie_shuffle();
for(var i=0; i<3; i++) {
	for(var j=0; j<3; j++) {
		count[i,j] = 0;
		total[i,j] = 0;
	}
}
with oShroom {
	var key = object_get_name(object_index)+"_"+string(floor(image_index));
	if object_index == oHandFake {
		switch(image_index) {
			case hand.darkness:
				key = "oHandOfDarkness_"+string(floor(image_index));
			break;
			case hand.light:
				key = "oHandOfLight_"+string(floor(image_index));
			break;
			case hand.tears:
				key = "oHandOfTears_"+string(floor(image_index));
			break;
			case hand.flames:
				key = "oHandOfFlames_"+string(floor(image_index));
			break;
			case hand.leafs:
				key = "oHandOfLeafs_"+string(floor(image_index));
			break;
			case hand.thunder:
				key = "oHandOfThunder_"+string(floor(image_index));
			break;
			case hand.lies:
				key = "oHandOfLies_"+string(floor(image_index));
			break;
			case hand.sky:
				key = "oHandOfSky_"+string(floor(image_index));
			break;
			case hand.earth:
				key = "oHandOfEarth_"+string(floor(image_index));
			break;
			case hand.storms:
				key = "oHandOfStorms_"+string(floor(image_index));
			break;
			case hand.moon:
				key = "oHandOfMoon_"+string(floor(image_index));
			break;
			case hand.dreams:
				key = "oHandOfDreams_"+string(floor(image_index));
			break;
		}
		event_user(0);
	}
	var xpos = (x-oOriginPoint.x) div global.view_width;
	var ypos = (y-oOriginPoint.y) div global.view_height;
	other.total[xpos,ypos]++;
	if !ds_map_exists(global.collected,key) {
		//if false {
			instance_destroy();	
		//}
	} else {
		other.count[xpos,ypos]++;
	}
}

with oShroom {
	if object_index == oJetpack { continue; }
	if object_index == oSpike { continue; }
	if object_index == oPuzzlePiece { continue; }
	if object_index == oSnakeRope { continue; }
	while !collision_at(x,y+1) {
		y++;
	}
}

with oSign {
	var xpos = (x-oOriginPoint.x) div global.view_width;
	var ypos = (y-oOriginPoint.y) div global.view_height;
	var meow = "\n"+string(oMuseumControl.count[xpos,ypos])+"/"+string(oMuseumControl.total[xpos,ypos])+" Item Completion";
	var pos = string_pos(global.nl,script);
	if pos == 0 {
		script+=meow;
	} else {
		script = string_insert(meow,script,pos);
	}
	event_user(0);
}

if global.bag_contents_backup == -1 {
	global.bag_contents_backup = global.bag_contents;
	global.bag_contents = ds_list_sylvie_create();
}