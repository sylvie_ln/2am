{
    "id": "c754e711-e4dc-4d1d-9066-3890cde1e886",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTinyShroom",
    "eventList": [
        {
            "id": "02c1ab91-f375-4682-bef5-f08a3ff1fa9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c754e711-e4dc-4d1d-9066-3890cde1e886"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
    "visible": true
}