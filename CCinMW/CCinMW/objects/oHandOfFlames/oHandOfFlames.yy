{
    "id": "4977a4a1-0aaa-4c6d-9120-bb73b4a2fefe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHandOfFlames",
    "eventList": [
        {
            "id": "321328f2-3ace-44db-a494-041d0b0c74ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4977a4a1-0aaa-4c6d-9120-bb73b4a2fefe"
        }
    ],
    "maskSpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
    "overriddenProperties": null,
    "parentObjectId": "3cf2a5e5-8deb-4ca2-bdc0-e75f47cf3912",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
    "visible": true
}