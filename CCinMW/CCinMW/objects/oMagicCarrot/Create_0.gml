event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A carrot that glows with a mysterious power. It is said that it cannot be eaten, yet grants the emotions of eating a carrot when you touch it."

damp = 0.9;
bounce = damp;
fric = 0.95;

grav /= 4;

image_speed = 0;

/// In oInit, define a trade entry for this item!