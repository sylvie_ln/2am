event_inherited();

act1 = "Ride";

damp = 0.5;
bounce = damp;
fric = 0.99;
acc = 1/8;
vacc = 2;
hdamp = false;
jumping = false;

vehicle_pos = [-1,-4];

/// In oInit, define a trade entry for this item!