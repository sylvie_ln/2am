var left = input_held("Left");
var right = input_held("Right");
var lp = input_pressed("Left");
var rp = input_pressed("Right");
var hdir = right-left;
if left and right {
	hdir = (input_held_time("Left") <= input_held_time("Right")) ? -1 : 1;
}

if lp { image_xscale = -1; }
if rp { image_xscale =  1; }


var ong = collision_at(x,y+1) and (bounced or vv >= 0);
if ong or bounced {
	jumping = false;
	vacc = 2;
}
if hdir != 0 {
	hv += image_xscale*acc;
	if abs(hv) < 0.5+acc {
		hv 	= hdir*(0.5+acc);
	}
}

do_friction = true;
if input_held("Jump") and (ong or jumping) {
	if ong {
		vv = -2;	
		jumping = true;
	} else if vv <= 0 {
		vv = -(grav+vacc);
		vacc *= 0.98;
		if vacc < 0.01 {
			vacc = 0;	
		}
	}
}
if input_released("Jump") {
	jumping = false;
}