event_inherited();

script = 
@"The cool star collectable that was removed from the game very early on. There was a brief time where the whole game was just going to be about traversing Mushroom Zones to try to collect stars.
If you can read this, you must be a hacker!"
