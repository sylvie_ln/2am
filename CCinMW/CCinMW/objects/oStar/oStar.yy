{
    "id": "191170df-059d-493f-a5c9-7f1b4334d507",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStar",
    "eventList": [
        {
            "id": "c1b4c9c3-b4ea-4ca9-8174-6c89278c9f9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "191170df-059d-493f-a5c9-7f1b4334d507"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0cbf5b0e-b25e-4a0a-adfe-d022891eaa62",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ccc0e7b1-452f-4461-ab35-7574524dcb5b",
    "visible": true
}