{
    "id": "72998663-ad5f-467b-8710-fa0a8b81f74e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoubleJump",
    "eventList": [
        {
            "id": "2f5af92a-f02d-423f-b38a-6b9ca5fea46d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "72998663-ad5f-467b-8710-fa0a8b81f74e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "65ef3ec7-0bb9-430d-b919-9b85a40a68c9",
    "visible": true
}