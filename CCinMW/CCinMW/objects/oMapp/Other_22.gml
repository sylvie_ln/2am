///@description Memoris!

mem = [
// Creative Laughter (Cherry low)
["Cherie", "Sweets", "Uuella", "Lamia", "Geniux"],
// Cackling Bust (Canyon near entrance)
["Tressa","Canyetta","Crisis","Mineria","Wirte"],
// Cornered Wilderness (Cavern upper left)
["Gooney","Cavey","Bunnisse","Archaea","Graal"],
// Corpse Facade (Clock)
["Byclock","Uncomus","Beleve","Mooncry"],
// Cordless Funeral (Cavern upper right)
["Slash","Wizaron","Alicia","Zupzel",],
// Crumbled Hearth (Canyon lower left)
["Junky","Laborie","Searchey","Truckster"],
// Cuter Willows (Cherry lower right)
["Creamy","Arianne","Plunch","Katey"],
// Captive Automaton (Cavern lower right)
["Flaire","Bella","Country","Lady"],
// Careless Desire (Cherry left)
["Sunny","Karie","TroubleAndStrife","Princess"],
// Caustic Puddle (Canyon upper)
["Rainbis","Rockout","Jealousy","Chestor"],
// Cataclysmic Fountain (Citey lower)
["Stocs","Mococo","Pipi","Galfio"],
// Cloudy Mixture (Citey upper)
["Lockme","Stormy","Chicen","Queen"],
]


item = [
// Creative Laughter (Cherry low)
[oBunneySlippers,oMiniBunney,oSnakeRope,oBunneyEars,oJIGGLER,],
// Cackling Bust (Canyon near entrance)
[oSylviePlushey,oBottledFlare,oGiftShopSnowGlobe,oBoringRock,[-1,get_unco_shroom(unco_shroom_type.tiny)]],
// Cornered Wilderness (Cavern upper left)
[oGiftShopPostcard,oGiftShopCoffeeMug,oDrummerPlushey,oGiftShopShirt,oGiftShopCavernRock],
// Corpse Facade (Clock)
[oJunkParts,[-1,get_rare_shroom(rare_shroom_type.float)],oSpringSneakers,oJunkParts,[-1,get_rare_shroom(rare_shroom_type.float)],oClimbingCleats,oJunkParts,[-1,get_rare_shroom(rare_shroom_type.float)],oFishCar],
// Cordless Funeral (Cavern upper right)
[oBottledFlare,oTrendyTote,[-1,get_rare_shroom(rare_shroom_type.speeder)],oBottledFlare,oTrendyTote,[-1,get_rare_shroom(rare_shroom_type.speeder)],oWizardmobile,oPegasusBoots],
// Crumbled Hearth (Canyon lower left)
[oJunkParts,oBoringRock,oJunkParts,oBottledFlare,[-1,get_rare_shroom(rare_shroom_type.spring)]],
// Cuter Willows (Cherry lower right)
[oBottledFlare,oGiftShopKitteyPlush,oKittenKey,oGuardianShield,[-1,get_rare_shroom(rare_shroom_type.kitty)]],
// Captive Automaton (Cavern lower right)
[oJunkParts,oSnakeRope,oDice,[-1,get_rare_shroom(rare_shroom_type.poison)],[-1,get_rare_shroom(rare_shroom_type.poison)],oSnakeRope,oJunkParts,oMushroomQuesoGrande],
// Careless Desire (Cherry left)
[oRoseKey,oJunkParts,o100TonShoes,[-1,get_rare_shroom(rare_shroom_type.sparkle)],oJunkParts,oMagicCarrot],
// Caustic Puddle (Canyon upper)
[oJunkParts,oSnakeRope,oLadderOrb,oBridgeOrb,[-1,get_rare_shroom(rare_shroom_type.rainbow)]],
// Cataclysmic Fountain (Citey lower)
[oDollars,oRedPaperclip,oUniqueRock,oGuardianScimitar,oGenericItemTemplate],
// Cloudy Mixture (Citey upper)
[oJunkParts,oRegularKey,oJunkParts,oRegularKey,oHoverHeels,oJunkParts,],
]