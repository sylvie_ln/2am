var u = 1000000;

if step == 0 {
	sylvie_set_seed(666);
	var name = "Creative Laughter"
	style = mapstyle.easygoing;
} else 
if step == 1 {
	sylvie_set_seed(12345);
	var name = "Cackling Bust"	
	style = mapstyle.easygoing;
} else
if step == 2 {
	sylvie_set_seed(101010);
	var name = "Cornered Wilderness"
	style = mapstyle.easygoing;
} else
if step == 3 {
	sylvie_set_seed(9);
	var name = "Corpse Facade"
	style = mapstyle.easygoing;
} else
if step == 4 {
	sylvie_set_seed(1066610);
	var name = "Cordless Funeral"
	style = mapstyle.easygoing;
} else
if step == 5 {
	sylvie_set_seed(123456);
	var name = "Crumbled Hearth"
	style = mapstyle.easygoing;
} else
if step == 6 {
	sylvie_set_seed(666666);
	var name = "Cuter Willows"
	style = mapstyle.easygoing;
} else
if step == 7 {
	sylvie_set_seed(1033310);
	var name = "Captive Automaton"
	style = mapstyle.easygoing;
} else
if step == 8 {
	sylvie_set_seed(363636);
	var name = "Careless Desire"
	style = mapstyle.easygoing;
} else
if step == 9 {
	sylvie_set_seed(1234567);
	var name = "Caustic Puddle"
	style = mapstyle.easygoing;
} else
if step == 10 {
	sylvie_set_seed(4444);
	var name = "Cataclysmic Fountain"
	style = mapstyle.easygoing;
} else
if step == 11 {
	sylvie_set_seed(2222);
	var name = "Cloudy Mixture"
	style = mapstyle.easygoing;
} 
else
if step == 12 {
	if dumpme {
		show_debug_message("Completed Sivy_"+string(current_dump)+"_"+string(difficulty)+".txt");
		//file_text_close(dump);	
		difficulty++;
		if difficulty > 2 {
			current_dump++;
			difficulty = 0;
		}
		if current_dump < number_to_dump {
			show_debug_message("Starting Sivy_"+string(current_dump)+"_"+string(difficulty)+".txt");
			dump = file_text_open_write("Sivy_"+string(current_dump)+"_"+string(difficulty)+".txt");
			step = 0;
			exit;
		} else {
			game_end();	
			exit;
		}
	}
	exit;
}

if state == "idle" {
	i = 0;
	j = 0;
	c = 0;
	fill = false;
	done = false;
	state = "waiting";
	if current_dump < array_length_1d(dump_seed_offsets) {
		seed_offset = dump_seed_offsets[current_dump];
	} else {
		seed_offset = get_timer();
	}
	stepseed = random_get_seed();
	stepname = name;
	zone_chaos = current_dump mod 2;
	sylvie_set_seed(stepseed+seed_offset);
	show_debug_message(name+" start "+string(get_timer()/u));
} else if state == "waiting" {;
	if dumpme {
		event_user(1);
	} else {
		event_user(style);	
	}
	if done { 
		state = "done";
	}
} else if state == "done" {
	if dumpme {
		mapmapp[? name] = mapp[ninfo.data];
			cutey_write_grid(mapp[ninfo.data],"mapmapp[? \""+name+"\"]","0");
			ds_grid_destroy(mapp[ninfo.data]);
		mapmapp[? name+"_door"] = doormapp;
			cutey_write_grid(doormapp,"mapmapp[? \""+name+"_door\"]","0");
			ds_grid_destroy(doormapp);
		mapmapp[? name+"_item"] = itemmapp;
			cutey_write_grid(itemmapp,"mapmapp[? \""+name+"_item\"]","0");
			ds_grid_destroy(itemmapp);
		mapmapp[? name+"_luck"] = luckmapp;
			cutey_write_grid(luckmapp,"mapmapp[? \""+name+"_luck\"]","0");
			ds_grid_destroy(luckmapp);
		mapmapp[? name+"_lore"] = loremapp;
			cutey_write_grid(loremapp,"mapmapp[? \""+name+"_lore\"]","\"\"");
			ds_grid_destroy(loremapp);
		mapmapp[? name+"_cute"] = cutemapp;
			cutey_write_grid(cutemapp,"mapmapp[? \""+name+"_cute\"]","\"\"");
			ds_grid_destroy(cutemapp);
	}
	show_debug_message(name+" end "+string(get_timer()/u));
	if dumpme {
		file_text_close(dump);	
		//show_debug_message("Append to Sivy_"+string(current_dump)+"_"+string(difficulty)+".txt");
		if step < 11 {
			dump = file_text_open_append("Sivy_"+string(current_dump)+"_"+string(difficulty)+".txt");	
		}
	}
	if !global.contract_signed {
		var startt = get_timer();
		var tt = 0;
		while tt < (5/12)*u {
			tt += get_timer()-startt;
			startt = get_timer();
		}
	}
	progress = 0;
	fluffy = sylvie_random(0,288/24);
	step++;
	state = "idle";
}