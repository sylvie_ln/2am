///@description Customeow
var time = get_timer();

var nw = 2;
var nh = 2;

if fill == 0 {
	if i+j mod 7 == 0 {
		progress = clamp((j*w+i)/(2*w*h),0,1);
	}
} else {
	if c mod 7 == 0 {
		progress = clamp(((w*h)+c)/(2*w*h),0,1);
	}
}

sylvie_set_seed(stepseed+seed_offset);

unco[0] = [2,3,3]; // laughter
rare[0] = [3,4,4];

unco[1] = [2,3,3]; // bust
rare[1] = [3,4,4];

unco[2] = [2,3,3]; // wilderness
rare[2] = [3,4,4];

unco[3] = [3,5,6]; // facade
rare[3] = [4,7,9];

unco[4] = [2,4,5]; // funeral
rare[4] = [5,7,9];

unco[5] = [2,4,5]; // hearth
rare[5] = [4,7,9];

unco[6] = [2,4,5]; // willows
rare[6] = [5,7,9];

unco[7] = [2,4,5]; // automaton
rare[7] = [5,7,9];

unco[8] = [2,4,5]; // desire
rare[8] = [5,7,9];

unco[9] = [2,4,5]; // puddle
rare[9] = [4,7,9];

unco[10] = [3,6,7]; // fountain
rare[10] = [4,6,7];

unco[11] = [3,6,7]; // mixture
rare[11] = [4,6,7];

var uncoa = unco[step];
unco_freq = uncoa[difficulty];
var rarea = rare[step];
rare_freq = rarea[difficulty];

door_freq = 1;

if zone_chaos {
	switch(step) {
		case 0: // Creative Laughter (Cherry)
			//            x    y   x   y   y       x
			var arrs = [[20],[20],[4],[4],[2,2],[2,2]];
			var links = [25,16,2,1,0,0];
			var ow = 0;
			var oh = 0;
		break;
		case 1: // Cackling Bust (Canyon)
			var arrs = [[20],[20],[4],[4],[2,2],[2,2]];
			var links = [25,16,2,1,0,0];
			var ow = [5,8];
			var oh = [4,6];
		break;
		case 2: // Cornered Wilderness (Cavern)
			var arrs = [[20],[20],[4],[4],[2,2],[2,2]];
			var links = [25,16,2,1,0,0];
			var ow = [2,4];
			var oh = [2,3];
		break;
		case 3: // Corpse Facade (Clock)
			var arrs = [[20],[20],[4],[4],[1,2,1],[1,2,1]];
			var links = [25,16,2,1,0,0];
			var ow = 0;
			var oh = 0;
		break;
		case 4: // Cordless Funeral (Speedshroom)
			var arrs = [[20],[20],[4],[4],[1],[4]];
			var links = [25,16,2,1,0,0];
			var ow = [2,4];
			var oh = [2,3];
		break;
		case 5: // Crumbled Hearth (Springshroom)
			var arrs = [[20],[20],[4],[4],[1],[4]];
			var links = [25,16,2,1,0,0];
			var ow = [5,8];
			var oh = [4,6];
		break;
		case 6: // Cuter Willows (KitteyShroom)
			var arrs = [[20],[20],[4],[4],[1],[4]];
			var links = [25,16,2,1,0,0];
			var ow = 0;
			var oh = 0;
		break;
		case 7: // Captive Automaton (Poisonshroom)
			var arrs = [[20],[20],[20],[4],[4],[1]];
			var links = [25,16,7,1,0,0];
			var ow = [2,4];
			var oh = [2,3];
		break;
		case 8: // Careless Desire (Sparkleshroom)
			var arrs = [[20],[20],[20],[4],[4],[1]];
			var links = [25,16,7,1,0,0];
			var ow = 0;
			var oh = 0;
		break;
		case 9: // Caustic Puddle (Rainbowshroom)
			var arrs = [[20],[20],[20],[4],[4],[1]];
			var links = [25,16,7,1,0,0];
			var ow = [5,8];
			var oh = [4,6];
		break;
		case 10: // Cataclysmic Fountain
			var arrs = [[20],[20],[4],[4],[4],[4]];
			var links = [25,16,1,1,0,0];
			var ow = [2,4];
			var oh = [2,3];
		break;
		case 11: // Cloudy Mixture
			var arrs = [[20],[20],[4],[4],[4],[4]];
			var links = [25,16,1,1,0,0];
			var ow = 0;
			var oh = 0;
		break;
	}
} else {
	switch(step) {
		case 0: // Creative Laughter (Cherry)
			//            x    y   x   y   y       x
			var arrs = [[50],[50],[5],[5],[2,3],[2,3]];
			var links = [25,16,2,1,1,1];
			var ow = [4,6];
			var oh = [3,5];
		break;
		case 1: // Cackling Bust (Canyon)
			var arrs = [[50],[50],[5],[5],[2,3],[2,3]];
			var links = [25,16,2,1,1,1];
			var ow = [5,8];
			var oh = [4,6];
		break;
		case 2: // Cornered Wilderness (Cavern)
			var arrs = [[50],[50],[5],[5],[2,3],[2,3]];
			var links = [25,16,2,1,1,1];
			var ow = [3,5];
			var oh = [2,4];
		break;
		case 3: // Corpse Facade (Clock)
			var arrs = [[50],[50],[5],[5],[2,3],[3,2]];
			var links = [25,16,1,1,0,0];
			var ow = 0;
			var oh = 0;
		break;
		case 4: // Cordless Funeral (Speedshroom)
			var arrs = [[50],[50],[5],[5],[1],[5]];
			var links = [25,16,1,1,0,0];
			var ow = [3,5];
			var oh = [2,4];
		break;
		case 5: // Crumbled Hearth (Springshroom)
			var arrs = [[50],[50],[5],[5],[1],[5]];
			var links = [25,16,1,1,0,0];
			var ow = [5,8];
			var oh = [4,6];
		break;
		case 6: // Cuter Willows (KitteyShroom)
			var arrs = [[50],[50],[5],[5],[1],[5]];
			var links = [25,16,1,1,0,0];
			var ow = [4,6];
			var oh = [3,5];
		break;
		case 7: // Captive Automaton (Poisonshroom)
			var arrs = [[50],[50],[25],[5],[5],[1]];
			var links = [25,16,9,1,0,0];
			var ow = [4,6];
			var oh = [3,5];
		break;
		case 8: // Careless Desire (Sparkleshroom)
			var arrs = [[50],[50],[25],[5],[5],[1]];
			var links = [25,16,9,1,0,0];
			var ow = [5,8];
			var oh = [4,6];
		break;
		case 9: // Caustic Puddle (Rainbowshroom)
			var arrs = [[50],[50],[25],[5],[5],[1]];
			var links = [25,16,9,2,0,0];
			var ow = [3,5];
			var oh = [2,4];
		break;
		case 10: // Cataclysmic Fountain
			var arrs = [[50],[50],[5],[5],[4,1],[1,4]];
			var links = [25,16,1,1,0,0];
			var ow = 0;
			var oh = 0;
		break;
		case 11: // Cloudy Mixture
			var arrs = [[50],[50],[5],[5],[4,1],[4,1]];
			var links = [25,16,1,1,0,0];
			var ow = 0;
			var oh = 0;
		break;
	}
}

while true {
	if fill == 0 {
		if dumpme {
			var hixlist = ds_list_create();
			var hixa = arrs[0];
			var hixl = array_length_1d(hixa);
			var hicx = 0;
			for(var hix=0; hix<w; hix+=hixa[(hicx++) mod hixl]) {
				//disabled_show_debug_message("hix: "+string(hix));
				var hiylist = ds_list_create();
				var hiya = arrs[1];
				var hiyl = array_length_1d(hiya);
				var hicy = 0;
				for(var hiy=0; hiy<h; hiy+=hiya[(hicy++) mod hiyl]) {
					//disabled_show_debug_message(" hiy: "+string(hiy));
					var mexlist = ds_list_create();
					var mexa = arrs[2]
					var mexl = array_length_1d(mexa);
					var mecx = 0;
					for(var	mex=hix; mex<hix+hixa[hicx mod hixl]; mex+=mexa[(mecx++) mod mexl]) {
						//disabled_show_debug_message("  mex: "+string(mex));
						var meylist = ds_list_create();
						var meya = arrs[3];
						var meyl = array_length_1d(meya);
						var mecy = 0;
						for(var	mey=hiy; mey<hiy+hiya[hicy mod hiyl]; mey+=meya[(mecy++) mod meyl]) {
							//disabled_show_debug_message("   mey: "+string(mey));
							var loylist = ds_list_create();
							var loya = arrs[4];
							var loyl = array_length_1d(loya);
							var locy = 0;
							for(var loy=mey; loy<mey+meya[mecy mod meyl]; loy+=loya[(locy++) mod loyl]) {
								//disabled_show_debug_message("    loy: "+string(loy));
								var loxlist = ds_list_create();
								var loxa = arrs[5];
								var loxl = array_length_1d(loxa);
								var locx = 0;
								for(var lox=mex; lox<mex+mexa[mecx mod mexl]; lox+=loxa[(locx++) mod loxl]) {
									//disabled_show_debug_message("     lox: "+string(lox));
									var nw = loxa[locx mod loxl];
									var nh = loya[locy mod loyl];
									var node = mapp_mxn(nw,nh,ow,oh,stepseed+seed_offset+sylvie_random(0,1024));
									node[ninfo.xp] = lox;
									node[ninfo.yp] = loy;
									//disabled_show_debug_message("     loxnode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
									ds_list_add(loxlist,node);
								}
								node = mapp_link(loxlist,stepseed+seed_offset+sylvie_random(0,1024),links[5]);
								node[ninfo.yp] = loy;
								//disabled_show_debug_message("    loynode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
								ds_list_add(loylist,node);
							}
							node = mapp_link(loylist,stepseed+seed_offset+sylvie_random(0,1024),links[4]);
							node[ninfo.xp] = mex;
							node[ninfo.yp] = mey;
							//disabled_show_debug_message("   meynode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
							ds_list_add(meylist,node);
						}
						node = mapp_link(meylist,stepseed+seed_offset+sylvie_random(0,1024),links[3]);
						node[ninfo.xp] = mex;
						//disabled_show_debug_message("  mexnode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
						ds_list_add(mexlist,node);
					}
					node = mapp_link(mexlist,stepseed+seed_offset+sylvie_random(0,1024),links[2]);
					node[ninfo.xp] = hix;
					node[ninfo.yp] = hiy;
					//disabled_show_debug_message(" hiynode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
					ds_list_add(hiylist,node);
				}
				node = mapp_link(hiylist,stepseed+seed_offset+sylvie_random(0,1024),links[1]);
				node[ninfo.xp] = hix;
				//disabled_show_debug_message("hixnode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
				ds_list_add(hixlist,node);
			}
			mapp = mapp_link(hixlist,stepseed+seed_offset+sylvie_random(0,1024),links[0]);
		}
		switch(step) {
		    case 0: global.sylvie_seed = -5627848; break;
		    case 1: global.sylvie_seed = 270755750; break;
		    case 2: global.sylvie_seed = -1601121997; break;
		    case 3: global.sylvie_seed = 1085197338; break;
		    case 4: global.sylvie_seed = -1935107271; break;
		    case 5: global.sylvie_seed = 1884342148; break;
		    case 6: global.sylvie_seed = 204562096; break;
		    case 7: global.sylvie_seed = -449520369; break;
		    case 8: global.sylvie_seed = -2030662219; break;
		    case 9: global.sylvie_seed = 115897508; break;
		    case 10: global.sylvie_seed = -642726069; break;
		    case 11: global.sylvie_seed = -1416288349; break;
		}
		global.sylvie_seed += seed_offset;
		var oo = offsets[? stepname];
		fill_setup((w div 2)+oo[0], (h div 2)+oo[1]);
	}  else if fill == 1 {
		if !dumpme { fill = 2; } else {
			fill_loop(door_freq,unco_freq,rare_freq);
		}
	} else if fill > 1 {
		done = true;
		fill_cleanup();
		break;
	}
}