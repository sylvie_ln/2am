{
    "id": "687b52bb-f47e-47e4-b0f2-a75b65ca3909",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMapp",
    "eventList": [
        {
            "id": "808f8635-c62b-4afb-b7c1-24ce7b1e7697",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "c062e16d-21a2-40bf-af65-495d2413362d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "9aa035b9-173b-40e0-9cc7-ce6395a3fa63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "b4d18caf-e94c-436d-a38c-3915d2e127b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "a75e9103-b87b-4937-a450-e9c08ae771c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "30354200-e8e0-4f70-bf85-1da89173ecd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "35706992-4dbe-49cf-9708-3764b586c99f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "15b38c32-dcf3-4fd9-80c5-6bc07814253e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "66f82354-9d53-4fd9-92d6-83f2d168552d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 22,
            "eventtype": 7,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        },
        {
            "id": "3a372d40-99ac-4b9f-921c-65ae1dd25286",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "687b52bb-f47e-47e4-b0f2-a75b65ca3909"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}