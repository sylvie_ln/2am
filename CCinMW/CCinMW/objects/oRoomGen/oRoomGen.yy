{
    "id": "ec8ca128-4cc0-4200-b27e-7dc3961869ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRoomGen",
    "eventList": [
        {
            "id": "6563003f-365c-4be6-ab5e-e2f27638b94f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        },
        {
            "id": "0ba3ba7b-88bd-4708-a8cb-184e55de1d9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        },
        {
            "id": "3663f9c3-a101-4c3f-8c1d-89c0786a4775",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        },
        {
            "id": "34857913-a069-48b1-823a-e59c4a69e007",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        },
        {
            "id": "e5dcc664-1b14-4e72-90f5-5a6b6c4d3996",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        },
        {
            "id": "fd8a247a-b012-4d89-844b-90a16ce2b17e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        },
        {
            "id": "f9c24446-dca0-4e9c-a0cd-1baf37eeb4d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        },
        {
            "id": "be293c07-bb75-4281-8cf0-883f0b717e5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        },
        {
            "id": "fe53d8fc-79ae-4a4c-a703-a4b29a44ca30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "ec8ca128-4cc0-4200-b27e-7dc3961869ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}