/// @description Room add
sylvie_set_seed(room_seed(xp,yp));
var lucky = luck_map[# xp,yp];
var very_lucky = false;
if global.flag_dice != 0 { 
	very_lucky = lucky;
	lucky = true; 
} 
var xx = 0; 
var yy = 0;
var w = global.rwidth div cell_size;
var h = global.rheight div cell_size;
var decay = 1;
switch(shift_dir) {
	case edir.up: yy -= h; break;
	case edir.down: yy += h; break;
	case edir.left: xx -= w; break;
	case edir.right: xx += w; break;
	case 0: 
		if !is_undefined(global.warp_x) {
			xx = (xp-global.warp_x)*w;
			yy = (yp-global.warp_y)*h;
		} else {
			xx = (xp-startx)*w;
			yy = (yp-starty)*h;
		}
	break;
}
var burger = [[],[]];
array_copy(burger[0],0,border_position,0,array_length_1d(border_position));
array_copy(burger[1],0,border_opening,0,array_length_1d(border_opening));

var eivlys_room = (border_type == 0) and area == "Careless Desire" and xp == 7 and yp == 0;

global.block_counter = 0;
catfood(border_type,xx,yy,w,h,decay,burger);
sylvie_set_seed(room_seed(xp,yp))
sylvie_random(0,1024);

//room_map[? room_key(xp,yp)] = border;
//gen_map[? room_key(xp,yp)] = [border_position,border_opening,border_type];

//gen_door = (xp == 0 and yp == 0) or sylvie_random(0,1) == 0;


ds_list_clear(valid_blocks);
ds_list_clear(all_blocks);
ds_map_clear(blocks_by_id);

with oThingy {
	event_user(14);	
}

ds_list_sort(all_blocks,true);
sylvie_list_shuffle(all_blocks);

for(var i=0; i<ds_list_size(all_blocks); i++) {
	with (blocks_by_id[?all_blocks[|i]]) {
		event_user(13);
	}	
}

sylvie_list_shuffle(valid_blocks);


if door_map[# xp,yp] {
	var door = place_thingy(oDoor);
	if door == noone {
		var door = place_thingy_force(oDoor,xx,yy);
	}
	if door != noone {
		with door {
			tag = global.door_tag;
			rm = asset_get_index(global.previous_room);
			script = "This door leads to a safe place."
			old_script = script;
			event_user(0);
		}
		if !is_undefined(global.warp_x) {
			if xp == global.warp_x and yp == global.warp_y {
				if !instance_exists(oChar) {
					instance_create_depth(door.x,door.y,0,oChar);	
				}
			}
		} else if xp == startx and yp == starty {
			if !instance_exists(oChar) {
				instance_create_depth(door.x,door.y,0,oChar);	
			}
		}
		if instance_exists(door) {
			if !eivlys_room {
				kitten3(door,border_type);	
			}
			with door {
				while !collision_at(x,y+1) {
					y += 1;	
				}
			}
		}
	}
}

sylvie_set_seed(room_seed(xp,yp))

if lore_map[# xp,yp] != "" {
	var mem = place_thingy(oMemory);
	if mem == noone {
		var mem = place_thingy_force(oMemory,xx,yy);
	}
	if mem != noone {
		with mem { 
			tag = global.door_tag;
			rm = asset_get_index(global.previous_room);
			memory_id = other.lore_map[# other.xp,other.yp];
			event_perform(ev_other,ev_room_start)
			event_user(0); 
		}
		
		if !is_undefined(global.warp_x) {
			if xp == global.warp_x and yp == global.warp_y {
				if !instance_exists(oChar) {
					instance_create_depth(mem.x,mem.y,0,oChar);	
				}
			}
		} 
		
		if instance_exists(mem) {
			if !eivlys_room {
				kitten3(mem,border_type);	
			} 
			with mem {
				while !collision_at(x,y+1) {
					y += 1;	
				}
			}
		}
	}
}

sylvie_set_seed(room_seed(xp,yp))

if cute_map[# xp,yp] != "" {
	var tem = place_thingy(asset_get_index(cute_map[# xp,yp]));
	//disabled_show_debug_message(tem);
	if tem == noone {
		var tem = place_thingy_force(asset_get_index(cute_map[# xp,yp]),xx,yy);
	}
	//disabled_show_debug_message(tem);
	if tem != noone {
		with tem { 
			event_user(0); 
		}
		if instance_exists(tem) {
			if !eivlys_room {
				kitten3(tem,border_type);	
			}
			with tem {
				cute = true;
				while !collision_at(x,y+1) {
					y += 1;	
				}
			}
		}
	}
}

sylvie_set_seed(room_seed(xp,yp))
/*
if ds_map_exists(rare_map,k) {
	var shroom = place_thingy(rare_map[?k]);
	with shroom { event_user(0); }
}
*/
if uncommon_map[# xp,yp] != 0 and lucky {
	/*
	global.area_names = [
	0  "Cloudy Mixture", 
	1  "Cataclysmic Fountain",
	2  "Cornered Wilderness",
	3  "Captive Automaton",
	4  "Cackling Bust",
	5  "Caustic Puddle",
	6  "Crumbled Hearth",
	7  "Creative Laughter",
	8  "Cuter Willows",
	9  "Careless Desire",
	10 "Corpse Facade",
	11 "Cordless Funeral",
	];
	*/
	var cherry = [noone,rare_shroom,get_unco_shroom(unco_shroom_type.tall),get_unco_shroom(unco_shroom_type.tall),get_unco_shroom(unco_shroom_type.tall),oWorthlessShroom];
	var cavern = [noone,rare_shroom,get_unco_shroom(unco_shroom_type.wide),get_unco_shroom(unco_shroom_type.wide),get_unco_shroom(unco_shroom_type.wide),oWorthlessShroom];
	var canyon = [noone,rare_shroom,get_unco_shroom(unco_shroom_type.tiny),get_unco_shroom(unco_shroom_type.tiny),get_unco_shroom(unco_shroom_type.tiny),oWorthlessShroom];
	var unco = [noone,rare_shroom,
	oWorthlessShroom,get_unco_shroom(unco_shroom_type.tiny),get_unco_shroom(unco_shroom_type.tall),get_unco_shroom(unco_shroom_type.wide)
	];
	switch(oRoomGen.area) {
		case "Cloudy Mixture":
			unco = [noone,sylvie_choose(get_rare_shroom(rare_shroom_type.spring),get_rare_shroom(rare_shroom_type.float)),get_rare_shroom(rare_shroom_type.spring),get_rare_shroom(rare_shroom_type.float),sylvie_choose(get_unco_shroom(unco_shroom_type.tall),get_unco_shroom(unco_shroom_type.tall),get_unco_shroom(unco_shroom_type.wide),get_unco_shroom(unco_shroom_type.wide),get_unco_shroom(unco_shroom_type.tiny)),sylvie_choose(get_unco_shroom(unco_shroom_type.tiny),get_unco_shroom(unco_shroom_type.tiny),get_unco_shroom(unco_shroom_type.tall),get_unco_shroom(unco_shroom_type.wide))]	
		break;
		case "Cataclysmic Fountain":
			unco = [noone,sylvie_choose(get_rare_shroom(rare_shroom_type.kitty),get_rare_shroom(rare_shroom_type.sparkle),get_rare_shroom(rare_shroom_type.poison),get_rare_shroom(rare_shroom_type.rainbow),get_rare_shroom(rare_shroom_type.spring),get_rare_shroom(rare_shroom_type.speeder)),sylvie_choose(get_rare_shroom(rare_shroom_type.spring),get_rare_shroom(rare_shroom_type.float)),sylvie_choose(get_rare_shroom(rare_shroom_type.sparkle),get_rare_shroom(rare_shroom_type.rainbow)),sylvie_choose(get_rare_shroom(rare_shroom_type.kitty),get_rare_shroom(rare_shroom_type.speeder)),sylvie_choose(get_rare_shroom(rare_shroom_type.poison),get_rare_shroom(rare_shroom_type.float))]
		break;
		case "Cornered Wilderness":
		case "Captive Automaton":
		case "Cordless Funeral":
			unco = cavern;
		break;
		case "Creative Laughter":
		case "Cuter Willows":
		case "Careless Desire":
			unco = cherry;
		break;
		case "Cackling Bust":
		case "Caustic Puddle":
		case "Crumbled Hearth":
			unco = canyon;
		break;
		case "Corpse Facade":
		break;
	}
	if level == 1 {
		unco = [noone,rare_shroom,oWorthlessShroom,oWorthlessShroom,oWorthlessShroom,oWorthlessShroom]	
	}
	if unco != noone {
		if uncommon_map[# xp,yp] == -1 {
			var shroom = place_thingy(oWorthlessShroom);
		} else {
			var shroom = place_thingy(unco[uncommon_map[# xp,yp]]);
		}
		if shroom == noone {
			shroom = place_thingy_force(unco[uncommon_map[# xp,yp]],xx,yy);
		}
		with shroom { event_user(0); }
		if instance_exists(shroom) {
			if !eivlys_room {
				kitten3(shroom,border_type);	
			}
			if shroom.object_index == rare_shroom 
			or (area == "Cloudy Mixture" and (shroom.object_index == get_rare_shroom(rare_shroom_type.spring) or shroom.object_index == get_rare_shroom(rare_shroom_type.float)))
			or (area == "Cataclysmic Fountain") {
				shroom.rare = true;
			}
		}
	}
}
sylvie_set_seed(room_seed(xp,yp))

var num = common_map[# xp, yp];
if very_lucky { num++; }
if eivlys_room {
	with place_thingy(oGiftShopKitteyPlush) { event_user(0); }
	with place_thingy(oSylviePlushey)  { event_user(0); }
	with place_thingy(oKittenKey)  { event_user(0); }
	with place_thingy(oSpike)  { event_user(0); }
	with place_thingy(oGiftShopKitteyPlush)  { event_user(0); }
	with place_thingy(oSylviePlushey) { event_user(0); }
	with place_thingy(oSpike)  { event_user(0); }
	with place_thingy(oBottledFlare)  { event_user(0); }
	with place_thingy(oSpike)  { event_user(0); }
	with place_thingy(oGiftShopKitteyPlush)  { event_user(0); }
	with place_thingy(oSylviePlushey)  { event_user(0); }
	with place_thingy(oSpike)  { event_user(0); }
} else {
	repeat(num) {
		var shroom = place_thingy(oBasicShroom);
		if shroom == noone { break; }
		with shroom { event_user(0); }
	}
}
sylvie_set_seed(room_seed(xp,yp))

with oShroom {
	if just_created	{
		var xt = x;
		var yt = y;
		switch(other.shift_dir) {
			case edir.up: yt += global.rheight; break;
			case edir.down: yt -= global.rheight; break;
			case edir.left: xt += global.rwidth; break;
			case edir.right: xt -= global.rwidth; break;
		}
		//disabled_show_debug_message("checking "+string(other.local_shroom_deletion_map)+" " + room_key(other.xp,other.yp)+pos_key(xt,yt))
		if other.local_shroom_deletion_map[?room_key(other.xp,other.yp)+pos_key(xt,yt)] == object_index {
			//disabled_show_debug_message("deleting" + room_key(other.xp,other.yp)+pos_key(xt,yt))
			instance_destroy();	
			if rare {
				global.rare_dead[? oRoomGen.area+"_"+room_key(oRoomGen.xp,oRoomGen.yp)] = true;
			} 
			if cute {
				global.cute_dead[? oRoomGen.area+"_"+room_key(oRoomGen.xp,oRoomGen.yp)] = true;
			}
		}
	}
}

with oShroom {
	just_created = false;	
	var xt = x;
	var yt = y;
	switch(other.shift_dir) {
		case edir.up: yt += global.rheight; break;
		case edir.down: yt -= global.rheight; break;
		case edir.left: xt += global.rwidth; break;
		case edir.right: xt -= global.rwidth; break;
	}
	if object_index == oKittyShroom or object_index == oSpeedShroom {
		generated = false;	
		other.local_shroom_deletion_map[? room_key(other.xp,other.yp)+pos_key(xt,yt)] = object_index;
		global.rare_dead[? oRoomGen.area+"_"+room_key(oRoomGen.xp,oRoomGen.yp)] = true;
	}
}

if !instance_exists(oChar) {
	with oThingy {
		if !object_is_ancestor(object_index,oBlonck) {
			instance_create_depth(x,y,0,oChar);	
			break;
		}
	}
}

global.explored[?mush_key(xp,yp)] = true;

if !(instance_exists(oShifter) and oShifter.shifting){
	event_user(15);
}