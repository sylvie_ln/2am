// top row
var vpos = 10;
for(var hpos=10; hpos<global.rwidth; hpos +=20) {
	var blonck = collision_rectangle(hpos-2,vpos-2,hpos+2,vpos+2,oBlonck,false,true);
	if blonck != noone {
		var xo = 0;
		//if hpos == 10 { xo = -20; }
		//if hpos == global.rwidth-10 { xo = 20; }
		with instance_create_depth(blonck.x+xo,blonck.y-20,blonck.depth,blonck.object_index) {
			event_user(13);		
			outer = true;
			depth -= 1;
		}
	}
}
// bottom row
var vpos = global.rheight-10;
for(var hpos=10; hpos<global.rwidth; hpos +=20) {
	var blonck = collision_rectangle(hpos-2,vpos-2,hpos+2,vpos+2,oBlonck,false,true);
	if blonck != noone {
		var xo = 0;
		//if hpos == 10 { xo = -20; }
		//if hpos == global.rwidth-10 { xo = 20; }
		with instance_create_depth(blonck.x+xo,blonck.y+20,blonck.depth,blonck.object_index) {
			event_user(13);		
			outer = true;
			depth -= 1;
		}
	}
}
// left column
var hpos = 10;
for(var vpos=10; vpos<global.rheight; vpos +=20) {
	var blonck = collision_rectangle(hpos-2,vpos-2,hpos+2,vpos+2,oBlonck,false,true);
	if blonck != noone {
		var yo = 0;
		//if vpos == 10 { yo = -20; }
		//if vpos == global.rheight-10 { yo = 20; }
		with instance_create_depth(blonck.x-20,blonck.y+yo,blonck.depth,blonck.object_index) {
			event_user(13);		
			outer = true;
			depth -= 1;
		}
	}
}

// right column
var hpos = global.rwidth-10;
for(var vpos=10; vpos<global.rheight; vpos +=20) {
	var blonck = collision_rectangle(hpos-2,vpos-2,hpos+2,vpos+2,oBlonck,false,true);
	if blonck != noone {
		var yo = 0;
		//if vpos == 10 { yo = -20; }
		//if vpos == global.rheight-10 { yo = 20; }
		with instance_create_depth(blonck.x+20,blonck.y+yo,blonck.depth,blonck.object_index) {
			event_user(13);	
			outer = true;
			depth -= 1;
		}
	}
}