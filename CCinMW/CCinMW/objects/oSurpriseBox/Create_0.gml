event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

damp = 0.75;
bounce = damp;
fric = 0.95;

surprise = [
	[oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,],
	[oBasicShroom,oBasicShroom,oBasicShroom,oTinyShroom,],
	[oBasicShroom,oBasicShroom,oBasicShroom,oTinyShroom,oTinyShroom,],
	[oTinyShroom,oTinyShroom,oTinyShroom,oTinyShroom,oTinyShroom,],
	[oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oTinyShroom,oTinyShroom,oTinyShroom,],
	[oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oTinyShroom,oTinyShroom,oTinyShroom,oTinyShroom,oTinyShroom,],
	[oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oBasicShroom,oTinyShroom,oTinyShroom,oTinyShroom,oTinyShroom,oTinyShroom,]
]

image_speed = 0;

/// In oInit, define a trade entry for this item!