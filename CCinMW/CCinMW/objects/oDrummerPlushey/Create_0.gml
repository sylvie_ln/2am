event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "The lovely Drummer character, now in plushey form. It's big and cuddley, but only somewhat popular."

damp = 0.75;
bounce = damp;
fric = 0.95;
//grav *= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!