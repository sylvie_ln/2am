event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "An \"anime-styled\" bunney pin you can wear on your clothes. Looks just like a real bunney. Said to be an extremely lucky item by all."

damp = 0.75;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!