{
    "id": "949fcf48-2860-493a-9142-69f1968f7e17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHoverHeels",
    "eventList": [
        {
            "id": "6a8a7f61-b396-4840-92de-ba8238ada0d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "949fcf48-2860-493a-9142-69f1968f7e17"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5e00ff79-31ae-4d78-bda1-1c0d22d4a64f",
    "visible": true
}