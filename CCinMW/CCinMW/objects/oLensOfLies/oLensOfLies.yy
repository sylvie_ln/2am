{
    "id": "d65a7476-e8b7-43b1-8599-c265e084ba79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLensOfLies",
    "eventList": [
        {
            "id": "b4ee7f1e-e0cc-47a5-b1a4-7de925538a33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d65a7476-e8b7-43b1-8599-c265e084ba79"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68df445d-b94b-49d7-905e-be0d7f82660d",
    "visible": true
}