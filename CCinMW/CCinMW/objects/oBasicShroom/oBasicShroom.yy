{
    "id": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBasicShroom",
    "eventList": [
        {
            "id": "b451f5b4-3137-4de7-8159-4994e03da67f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4b180942-75c0-43f8-b03f-f6c18264ffeb"
        },
        {
            "id": "a9fcdba0-99d1-49e9-9cf8-d1c338fe40e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4b180942-75c0-43f8-b03f-f6c18264ffeb"
        },
        {
            "id": "1f84b83d-e990-4d5b-b636-93d97b845fb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4b180942-75c0-43f8-b03f-f6c18264ffeb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c8ff8d82-948f-4875-b52e-6b0807ea0545",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
    "visible": true
}