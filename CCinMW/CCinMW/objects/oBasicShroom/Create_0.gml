event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

always_respawn = false;

script = "Simpley a basic shroom, you enjoy its lovely color."

dampdamp = 0.01;
dampboost = 0;
damp = 0.75;
bounce = damp;
fric = 0.95;
grav *= 2;

spinny = true;

hdamp = true;
bounced = false;

riding = false;
do_friction = false;

if object_index == oBasicShroom 
or object_index == oTallShroom  
or object_index == oWideShroom  
or object_index == oTinyShroom {
	if room != rmCoolMuseum {
		image_index = sylvie_irandom(sprite_get_number(sprite_index)-1);
	}
}
image_speed = 0;