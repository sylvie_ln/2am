///@description

// Inherit the parent event

var ia_prev = image_angle;
if !paused {
	image_angle = round(draw_angle/90)*90;
}
event_inherited();
image_angle = ia_prev;

