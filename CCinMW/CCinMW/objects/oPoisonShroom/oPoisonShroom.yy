{
    "id": "c907153e-90d7-4fef-92af-8f4797f19c16",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPoisonShroom",
    "eventList": [
        {
            "id": "b7cc5c86-aacf-4bc4-b0af-4931b5ad5553",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c907153e-90d7-4fef-92af-8f4797f19c16"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "253507f4-0e3a-4d42-9013-77ac79eac4b6",
    "visible": true
}