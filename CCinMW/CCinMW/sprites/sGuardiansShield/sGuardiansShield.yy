{
    "id": "30aefb07-d5c7-492a-9b20-10b1f525ccae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGuardiansShield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 11,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbd08e25-0c6d-430e-b40b-07ef312790fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30aefb07-d5c7-492a-9b20-10b1f525ccae",
            "compositeImage": {
                "id": "78d4734e-0c0c-4bae-8be6-7e8407bd16eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbd08e25-0c6d-430e-b40b-07ef312790fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "589e93c2-13f9-46e8-875d-50eb7458a1c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbd08e25-0c6d-430e-b40b-07ef312790fb",
                    "LayerId": "1b0aec9f-30bc-4e10-85b8-894b3b8f98fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1b0aec9f-30bc-4e10-85b8-894b3b8f98fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30aefb07-d5c7-492a-9b20-10b1f525ccae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}