{
    "id": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSurpriseBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bebbe4a0-1d6f-457c-badc-da8e3242d711",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "compositeImage": {
                "id": "6bd8102a-a7b5-4735-b999-368c8e2b7f15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bebbe4a0-1d6f-457c-badc-da8e3242d711",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50ddcfe-5da7-4b54-9385-473df05045dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bebbe4a0-1d6f-457c-badc-da8e3242d711",
                    "LayerId": "d2c5b0e6-cb11-46a3-80ed-2ef8ea3dd436"
                },
                {
                    "id": "a9571c75-d4f3-4812-a122-49abb5bf941e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bebbe4a0-1d6f-457c-badc-da8e3242d711",
                    "LayerId": "03b53356-8c8d-4114-ae28-343ae804135b"
                }
            ]
        },
        {
            "id": "c44fc1b6-34c4-4255-953e-ebf4bae27fb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "compositeImage": {
                "id": "53d857c2-f044-4383-8858-01296e0fb397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c44fc1b6-34c4-4255-953e-ebf4bae27fb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25433b2-cc17-45fb-a689-6301d873008c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c44fc1b6-34c4-4255-953e-ebf4bae27fb3",
                    "LayerId": "03b53356-8c8d-4114-ae28-343ae804135b"
                },
                {
                    "id": "e0f869db-34a4-44ca-932b-4db8a31f3655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c44fc1b6-34c4-4255-953e-ebf4bae27fb3",
                    "LayerId": "d2c5b0e6-cb11-46a3-80ed-2ef8ea3dd436"
                }
            ]
        },
        {
            "id": "52d24d21-80d8-4d00-93be-ad47d06b71b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "compositeImage": {
                "id": "4491710c-75c1-422c-903c-d63d42c225f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d24d21-80d8-4d00-93be-ad47d06b71b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98508943-93ef-4693-bb03-d6bea4686757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d24d21-80d8-4d00-93be-ad47d06b71b8",
                    "LayerId": "d2c5b0e6-cb11-46a3-80ed-2ef8ea3dd436"
                },
                {
                    "id": "389412df-842f-46b1-9652-954c53078cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d24d21-80d8-4d00-93be-ad47d06b71b8",
                    "LayerId": "03b53356-8c8d-4114-ae28-343ae804135b"
                }
            ]
        },
        {
            "id": "12f096dc-6e36-4bcb-a1e7-c9dd77bce49f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "compositeImage": {
                "id": "236a9931-4521-449d-88a6-86813cd264e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f096dc-6e36-4bcb-a1e7-c9dd77bce49f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "274eb830-5dea-451d-a9e0-8c2e3f6efd10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f096dc-6e36-4bcb-a1e7-c9dd77bce49f",
                    "LayerId": "d2c5b0e6-cb11-46a3-80ed-2ef8ea3dd436"
                },
                {
                    "id": "f7fc7877-a6a6-4cba-946a-4cb741736126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f096dc-6e36-4bcb-a1e7-c9dd77bce49f",
                    "LayerId": "03b53356-8c8d-4114-ae28-343ae804135b"
                }
            ]
        },
        {
            "id": "97feb685-902c-4d4c-81cb-cff82aa48a20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "compositeImage": {
                "id": "9c9b4c62-056b-4e34-b9c1-823b6d039e27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97feb685-902c-4d4c-81cb-cff82aa48a20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddd78d48-d447-4e0f-8bb3-752c6ccef7dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97feb685-902c-4d4c-81cb-cff82aa48a20",
                    "LayerId": "d2c5b0e6-cb11-46a3-80ed-2ef8ea3dd436"
                },
                {
                    "id": "7bb347c6-04b5-448f-8d3e-519598cc2632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97feb685-902c-4d4c-81cb-cff82aa48a20",
                    "LayerId": "03b53356-8c8d-4114-ae28-343ae804135b"
                }
            ]
        },
        {
            "id": "d2a2f78d-63a3-4f66-84ed-4a682b3f8edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "compositeImage": {
                "id": "e20da96b-851e-4bbb-a59e-b62ee5e977b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a2f78d-63a3-4f66-84ed-4a682b3f8edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "745e96eb-c816-4c3a-8ccb-fb9b7b245621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a2f78d-63a3-4f66-84ed-4a682b3f8edf",
                    "LayerId": "d2c5b0e6-cb11-46a3-80ed-2ef8ea3dd436"
                },
                {
                    "id": "d69fc111-f961-4df7-9910-13d5248af3fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a2f78d-63a3-4f66-84ed-4a682b3f8edf",
                    "LayerId": "03b53356-8c8d-4114-ae28-343ae804135b"
                }
            ]
        },
        {
            "id": "e771b176-961d-4d10-a6c2-c041b8f4c7d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "compositeImage": {
                "id": "9c9897b5-6f47-4ad1-9985-4644b8bd4ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e771b176-961d-4d10-a6c2-c041b8f4c7d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d08df6a3-5a73-44fb-b037-e1cb18d952b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e771b176-961d-4d10-a6c2-c041b8f4c7d7",
                    "LayerId": "d2c5b0e6-cb11-46a3-80ed-2ef8ea3dd436"
                },
                {
                    "id": "06060150-5acb-49b0-a90a-7dc98e46a009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e771b176-961d-4d10-a6c2-c041b8f4c7d7",
                    "LayerId": "03b53356-8c8d-4114-ae28-343ae804135b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d2c5b0e6-cb11-46a3-80ed-2ef8ea3dd436",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 90,
            "visible": true
        },
        {
            "id": "03b53356-8c8d-4114-ae28-343ae804135b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}