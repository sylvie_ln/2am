{
    "id": "ce39b26c-c603-4138-be14-4f1667663d54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26710155-53d8-4faf-8750-934311d1b9dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce39b26c-c603-4138-be14-4f1667663d54",
            "compositeImage": {
                "id": "b541366a-2609-40a2-a8d4-6170e0b6f791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26710155-53d8-4faf-8750-934311d1b9dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "039c9805-433b-41e1-b2d5-665b563f5b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26710155-53d8-4faf-8750-934311d1b9dd",
                    "LayerId": "13302fed-d649-4e49-af8f-251860bd0ee1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "13302fed-d649-4e49-af8f-251860bd0ee1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce39b26c-c603-4138-be14-4f1667663d54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}