{
    "id": "e61f45bb-351a-446d-b950-49fa8105334c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopKitteyPlushSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcd7c905-c679-45e1-ab43-e1c61e75155b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e61f45bb-351a-446d-b950-49fa8105334c",
            "compositeImage": {
                "id": "43e9af53-ea17-4179-ab70-5ae009bd6f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcd7c905-c679-45e1-ab43-e1c61e75155b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d3a7e3-c139-48c6-a311-702b25918789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcd7c905-c679-45e1-ab43-e1c61e75155b",
                    "LayerId": "476c3a43-9c26-4db4-b4dc-04fade077168"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "476c3a43-9c26-4db4-b4dc-04fade077168",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e61f45bb-351a-446d-b950-49fa8105334c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}