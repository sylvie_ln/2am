{
    "id": "ef302f04-3b7c-4f9f-bdbb-5402a1c6d69b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnakeRopeSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e8fdb25-2e33-4751-9434-029806522f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef302f04-3b7c-4f9f-bdbb-5402a1c6d69b",
            "compositeImage": {
                "id": "4418e897-8b83-4616-b84e-ae696b1b4140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8fdb25-2e33-4751-9434-029806522f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5c84c3a-bc7f-4378-9ab5-3ea469e21061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8fdb25-2e33-4751-9434-029806522f42",
                    "LayerId": "18a8efa7-e3eb-416e-ab62-a7c811864c07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "18a8efa7-e3eb-416e-ab62-a7c811864c07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef302f04-3b7c-4f9f-bdbb-5402a1c6d69b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}