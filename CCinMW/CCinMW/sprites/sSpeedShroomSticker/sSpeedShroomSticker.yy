{
    "id": "a874599d-5793-4c51-aa57-9813012d1891",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpeedShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4d6d2aa-4f83-4b71-89fd-d8ac336f7bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a874599d-5793-4c51-aa57-9813012d1891",
            "compositeImage": {
                "id": "7ee8ccc5-d332-49e7-b4de-a041d80f4177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d6d2aa-4f83-4b71-89fd-d8ac336f7bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58c9ac45-f711-43f4-94b7-cf61c995fc43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d6d2aa-4f83-4b71-89fd-d8ac336f7bb8",
                    "LayerId": "56036644-a0ea-4eae-9103-f6e25c392cc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "56036644-a0ea-4eae-9103-f6e25c392cc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a874599d-5793-4c51-aa57-9813012d1891",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}