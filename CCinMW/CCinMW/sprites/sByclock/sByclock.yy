{
    "id": "d76f0a57-c8cc-4848-a718-50c5515e6b2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sByclock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "136a64b5-6bfe-4e98-accb-e33f9e8bfb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76f0a57-c8cc-4848-a718-50c5515e6b2e",
            "compositeImage": {
                "id": "0d155b42-f992-45a9-81cf-c7b256da4c65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136a64b5-6bfe-4e98-accb-e33f9e8bfb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c90bf80a-7e61-409d-aa07-7dcc3ed5d7f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136a64b5-6bfe-4e98-accb-e33f9e8bfb87",
                    "LayerId": "cd8f0e41-ab48-4693-9c27-ecf9f3934142"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cd8f0e41-ab48-4693-9c27-ecf9f3934142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d76f0a57-c8cc-4848-a718-50c5515e6b2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}