{
    "id": "cfdb6622-e95a-46b5-b4e3-61053c7ea388",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLadderOrb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07725ba0-1048-4911-b96a-99048f315f07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfdb6622-e95a-46b5-b4e3-61053c7ea388",
            "compositeImage": {
                "id": "f77c62a5-125b-418b-8038-5eb84b5c537e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07725ba0-1048-4911-b96a-99048f315f07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f9e3f6-0348-4825-89b9-ca8f35b82e48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07725ba0-1048-4911-b96a-99048f315f07",
                    "LayerId": "42303198-bddf-4cbf-bcfe-45ef48b0fb40"
                },
                {
                    "id": "184d01e2-0553-4f57-a87b-13e82c1e24ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07725ba0-1048-4911-b96a-99048f315f07",
                    "LayerId": "1141b4a8-8044-498a-9919-a2701d9216d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "42303198-bddf-4cbf-bcfe-45ef48b0fb40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfdb6622-e95a-46b5-b4e3-61053c7ea388",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1141b4a8-8044-498a-9919-a2701d9216d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfdb6622-e95a-46b5-b4e3-61053c7ea388",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4294948864,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}