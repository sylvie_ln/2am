{
    "id": "4a27b9e7-c26b-4e8d-9c54-027dbb23def9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSmallBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9dc24cc-0bfa-449d-8f9b-2114bf9eb1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a27b9e7-c26b-4e8d-9c54-027dbb23def9",
            "compositeImage": {
                "id": "808c88d4-7d62-4b7b-94c5-79b27af051ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9dc24cc-0bfa-449d-8f9b-2114bf9eb1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77a20323-86cc-49d6-8cd0-ba6d7912f186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9dc24cc-0bfa-449d-8f9b-2114bf9eb1f9",
                    "LayerId": "2e074d89-b31d-4dae-a3ee-4922c082d21b"
                },
                {
                    "id": "00918c36-20b8-47db-8998-5ecb5b05f580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9dc24cc-0bfa-449d-8f9b-2114bf9eb1f9",
                    "LayerId": "810b394c-a3e4-486d-9a71-dda6c997435d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2e074d89-b31d-4dae-a3ee-4922c082d21b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a27b9e7-c26b-4e8d-9c54-027dbb23def9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "810b394c-a3e4-486d-9a71-dda6c997435d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a27b9e7-c26b-4e8d-9c54-027dbb23def9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 40
}