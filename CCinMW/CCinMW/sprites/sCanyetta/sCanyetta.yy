{
    "id": "603da1dc-c1f3-4a8d-bfce-5e9549e03e7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanyetta",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2320b9f-11e4-4f10-b399-3b2a4fb0526d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603da1dc-c1f3-4a8d-bfce-5e9549e03e7a",
            "compositeImage": {
                "id": "6a6440e1-e67b-4ff0-b866-96fbfb184464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2320b9f-11e4-4f10-b399-3b2a4fb0526d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54566f9d-6675-4e3e-a306-7fe5137c4391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2320b9f-11e4-4f10-b399-3b2a4fb0526d",
                    "LayerId": "07136c4c-0d88-4ed2-bf4c-bff08199f282"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "07136c4c-0d88-4ed2-bf4c-bff08199f282",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "603da1dc-c1f3-4a8d-bfce-5e9549e03e7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}