{
    "id": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEivlysDance",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 3,
    "bbox_right": 41,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e373764-6b9b-47ac-a7e9-77fb2f35cf15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "57b96a91-2908-4f50-a683-4c4521c23a9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e373764-6b9b-47ac-a7e9-77fb2f35cf15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca7e56b9-e405-4dcc-8cda-9cde88beaa1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e373764-6b9b-47ac-a7e9-77fb2f35cf15",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "97fd94ef-83cf-4999-a3a7-e93d3a969ba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "6e43be7c-9138-4b5d-b6d4-e1e314b08ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97fd94ef-83cf-4999-a3a7-e93d3a969ba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b077eb-e7ae-452d-aa02-0408cdbe87b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97fd94ef-83cf-4999-a3a7-e93d3a969ba5",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "77f4b733-ed91-4ca4-9c6a-8cada7a36793",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "f95857b1-a1d4-4bde-b297-6e697f02baa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77f4b733-ed91-4ca4-9c6a-8cada7a36793",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5169230c-97c1-4fd8-bcb9-98cd2009d624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77f4b733-ed91-4ca4-9c6a-8cada7a36793",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "c675c84e-c7db-4700-9e82-5070ee667318",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "b979a603-d6fc-4ea0-af5c-324c75811be4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c675c84e-c7db-4700-9e82-5070ee667318",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5981c448-e63d-4e8a-9354-f0ccf335025f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c675c84e-c7db-4700-9e82-5070ee667318",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "44aebac5-5dbf-424c-af49-05477791cd6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "e52670c2-5f76-489b-8901-1e2ee2df4fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44aebac5-5dbf-424c-af49-05477791cd6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc340df-30b5-49a0-8d00-b2b2f264dcd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44aebac5-5dbf-424c-af49-05477791cd6f",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "1df21185-de49-4726-ac6a-6405f8e2b777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "41b1300b-47ad-426b-a196-3755120c2fea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df21185-de49-4726-ac6a-6405f8e2b777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d5e954-76f6-42a5-ac2d-34d37879ae59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df21185-de49-4726-ac6a-6405f8e2b777",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "a412c3ad-c93f-4e5f-bb5e-d2d42c715c69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "d04785e8-98dd-4c05-8ff3-44ea8def8bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a412c3ad-c93f-4e5f-bb5e-d2d42c715c69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649c902e-1cdc-4d03-9d0f-83346a8d441d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a412c3ad-c93f-4e5f-bb5e-d2d42c715c69",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "6c7a4aae-348a-493e-a396-8a50072c7ef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "4a1c0c12-f244-4d98-8b84-cbc4fc47ae23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c7a4aae-348a-493e-a396-8a50072c7ef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db7b8890-4bc0-45bd-bae7-c71571014114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7a4aae-348a-493e-a396-8a50072c7ef7",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "42e5fd66-622d-483c-8930-52c8fabd1576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "5ae32a73-ae85-46ab-a796-11dd7e7be698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42e5fd66-622d-483c-8930-52c8fabd1576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "204822ec-1700-4cfc-831d-bd343e4468c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42e5fd66-622d-483c-8930-52c8fabd1576",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "47e67d3d-6307-4d4f-bb84-282791f2a2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "02abd661-5c3d-4cfa-a3e9-ece14a0cfce5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e67d3d-6307-4d4f-bb84-282791f2a2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c9fb29-1774-4328-b3db-6b5a03a5a872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e67d3d-6307-4d4f-bb84-282791f2a2fc",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "a84849d3-c1b7-4c43-8ab5-487e2cf650a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "edc5d152-5de3-420b-a521-dffd7b53ac49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84849d3-c1b7-4c43-8ab5-487e2cf650a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "479ea17d-e2ce-463f-b54b-a6bf1b222bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84849d3-c1b7-4c43-8ab5-487e2cf650a2",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "babc18fa-c1cf-4747-96d0-e3461c16ca4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "e0b6abdc-e7c1-4777-a862-eed1c8c4be2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "babc18fa-c1cf-4747-96d0-e3461c16ca4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "996e2bf2-9d3e-44df-8875-2234888c39a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "babc18fa-c1cf-4747-96d0-e3461c16ca4a",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "d08deb0e-d33f-4ccf-8500-9349cdbc3750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "56d01a47-d794-465d-8cd1-0da455b079df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08deb0e-d33f-4ccf-8500-9349cdbc3750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8f070cf-1c2a-4a46-a489-0ba4fcd9e746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08deb0e-d33f-4ccf-8500-9349cdbc3750",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "97673b09-6486-4496-8f45-d0a8c56a553d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "949f904d-39dc-484d-a63c-459d9ad7c14f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97673b09-6486-4496-8f45-d0a8c56a553d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aec57b6d-4398-4eed-8ab8-31b7d0cad32d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97673b09-6486-4496-8f45-d0a8c56a553d",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "d2768989-c7c8-41f6-9211-c318c1d7c562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "448ae0d1-a462-43af-9d09-8791c9f21865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2768989-c7c8-41f6-9211-c318c1d7c562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7251e030-f913-4213-bc4e-6c213264fb77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2768989-c7c8-41f6-9211-c318c1d7c562",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "430f28fd-67c1-4f49-8b2a-971e6e6c257a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "b47e281e-41b6-427c-a7aa-506eca16c8d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430f28fd-67c1-4f49-8b2a-971e6e6c257a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f93265-4a4b-4b2d-9a44-2db91299f064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430f28fd-67c1-4f49-8b2a-971e6e6c257a",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "5eb62680-3dbb-4bff-a335-c2c61945b094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "7d4f0921-ab2a-4c37-a0af-a233915ee007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb62680-3dbb-4bff-a335-c2c61945b094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79c1943-dc09-4fd4-b254-949b77d19426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb62680-3dbb-4bff-a335-c2c61945b094",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "845631d7-ce71-41a1-8ee7-6dc279aba4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "bde408f7-236a-4ede-9cb9-a7656c699092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "845631d7-ce71-41a1-8ee7-6dc279aba4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2281362-9f94-4ece-a854-a3286ab1a3a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "845631d7-ce71-41a1-8ee7-6dc279aba4ad",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "07594192-ed41-4c6a-9aa0-7e39502f40d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "2d066b3d-236a-4571-bd44-c153a3411096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07594192-ed41-4c6a-9aa0-7e39502f40d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d09332-81de-435c-ba4a-effbb3816fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07594192-ed41-4c6a-9aa0-7e39502f40d2",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        },
        {
            "id": "e0565a8e-4ee4-4731-9df7-85e314bd105f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "compositeImage": {
                "id": "8cd3894d-6d22-4089-b848-1b2e8330befc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0565a8e-4ee4-4731-9df7-85e314bd105f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24ef9661-05d3-472a-8d86-b082d8fafee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0565a8e-4ee4-4731-9df7-85e314bd105f",
                    "LayerId": "9e5c7222-dac7-4963-9787-1a32368afaca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "9e5c7222-dac7-4963-9787-1a32368afaca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e459f1d-77db-4aca-b938-8f24f07b30bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 19,
    "yorig": 27
}