{
    "id": "d0afbdc5-1aec-4efd-9dea-b5e141dd9e8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDollars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "edcdbecb-abdc-4371-bd85-136e899fec1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0afbdc5-1aec-4efd-9dea-b5e141dd9e8e",
            "compositeImage": {
                "id": "5d8642d1-6cfe-4709-923a-fe75fba2e976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edcdbecb-abdc-4371-bd85-136e899fec1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30e8df0-60ae-4b10-a36e-9a67fdde6970",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edcdbecb-abdc-4371-bd85-136e899fec1b",
                    "LayerId": "8d741d25-1115-4dc8-81fc-2786820f4e2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8d741d25-1115-4dc8-81fc-2786820f4e2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0afbdc5-1aec-4efd-9dea-b5e141dd9e8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}