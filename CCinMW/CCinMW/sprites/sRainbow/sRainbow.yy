{
    "id": "a702b82e-8a92-4aaf-8745-69f2f77c5234",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRainbow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 34,
    "bbox_right": 301,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3ea2ba0-e51c-47b9-8a90-ed97b93a47de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702b82e-8a92-4aaf-8745-69f2f77c5234",
            "compositeImage": {
                "id": "c7ab42d5-9898-4ff2-bb4f-d5bb6b167057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ea2ba0-e51c-47b9-8a90-ed97b93a47de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d724047e-cf53-42f8-a5bb-e68511a4ae74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ea2ba0-e51c-47b9-8a90-ed97b93a47de",
                    "LayerId": "d2b9d05a-ad03-4afd-91a2-be6b60232f9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "d2b9d05a-ad03-4afd-91a2-be6b60232f9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a702b82e-8a92-4aaf-8745-69f2f77c5234",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}