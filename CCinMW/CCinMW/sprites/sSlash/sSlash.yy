{
    "id": "1f1028d6-563f-4cc0-88a5-13472186e62a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8beef85-551b-439e-b35a-4466be128df2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f1028d6-563f-4cc0-88a5-13472186e62a",
            "compositeImage": {
                "id": "8f7fb337-2fc3-46a7-9aca-80008ba736bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8beef85-551b-439e-b35a-4466be128df2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b81e50b-6c91-4ff0-8fea-15cf4ca843f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8beef85-551b-439e-b35a-4466be128df2",
                    "LayerId": "63cdd11e-0d7e-4a8d-a59b-2dd7ac68906d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "63cdd11e-0d7e-4a8d-a59b-2dd7ac68906d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f1028d6-563f-4cc0-88a5-13472186e62a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}