{
    "id": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fd36890-4685-45db-9539-c6f0d841cec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
            "compositeImage": {
                "id": "7573a919-4749-496b-a42b-6511e0c40dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd36890-4685-45db-9539-c6f0d841cec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5185b40-a89c-47e3-8674-e16b9896ec73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd36890-4685-45db-9539-c6f0d841cec2",
                    "LayerId": "c07ed374-9ac4-4e5d-a02d-900d50b4445b"
                }
            ]
        },
        {
            "id": "9ed70445-9f3e-42d2-b3ed-f17fab43c3ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
            "compositeImage": {
                "id": "48ac092f-4e46-4295-9b81-d1192f659900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ed70445-9f3e-42d2-b3ed-f17fab43c3ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a4c148a-41d1-450c-b0ee-3bb9ad67274a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ed70445-9f3e-42d2-b3ed-f17fab43c3ca",
                    "LayerId": "c07ed374-9ac4-4e5d-a02d-900d50b4445b"
                }
            ]
        },
        {
            "id": "1f9703aa-66e0-45de-9e78-3f872e9c64da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
            "compositeImage": {
                "id": "4e7a9d2c-678d-4f2f-a9ea-8caa553a4de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f9703aa-66e0-45de-9e78-3f872e9c64da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b56fc0ad-3af1-4cc5-9659-eb1c459576dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f9703aa-66e0-45de-9e78-3f872e9c64da",
                    "LayerId": "c07ed374-9ac4-4e5d-a02d-900d50b4445b"
                }
            ]
        },
        {
            "id": "5b7a2147-106a-4048-8b17-42c5ff065745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
            "compositeImage": {
                "id": "826e187c-7188-4518-9dc1-7c0a279e4720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7a2147-106a-4048-8b17-42c5ff065745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0033c9ce-b15c-4ea4-830f-0efe18c3f944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7a2147-106a-4048-8b17-42c5ff065745",
                    "LayerId": "c07ed374-9ac4-4e5d-a02d-900d50b4445b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c07ed374-9ac4-4e5d-a02d-900d50b4445b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}