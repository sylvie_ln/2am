{
    "id": "c4e23fa8-d10b-4279-af83-9f4692fa03f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWideBagUpgrade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "821714af-e498-4ce5-a8ce-650ba7e12be5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4e23fa8-d10b-4279-af83-9f4692fa03f8",
            "compositeImage": {
                "id": "cf680c59-e280-4eab-8052-38b7dd2a951b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "821714af-e498-4ce5-a8ce-650ba7e12be5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8012e6f8-abd0-4ca6-b5b4-4d897f3bbaf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "821714af-e498-4ce5-a8ce-650ba7e12be5",
                    "LayerId": "f3e747c8-5dbd-4782-bb36-9e1080063e65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f3e747c8-5dbd-4782-bb36-9e1080063e65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4e23fa8-d10b-4279-af83-9f4692fa03f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}