{
    "id": "8ffba4ac-beef-4771-8b67-400423e44bd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClockSpikeL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 29,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a72f88eb-ec20-4509-ae8e-a756c4c6aec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ffba4ac-beef-4771-8b67-400423e44bd4",
            "compositeImage": {
                "id": "fb296b8c-7c6f-498b-a636-470a5d2df87a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a72f88eb-ec20-4509-ae8e-a756c4c6aec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6550f7be-0ba8-4bcf-8630-274f732fba3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72f88eb-ec20-4509-ae8e-a756c4c6aec6",
                    "LayerId": "6b56c2e1-0180-4863-aca6-a7c53b97f581"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "6b56c2e1-0180-4863-aca6-a7c53b97f581",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ffba4ac-beef-4771-8b67-400423e44bd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}