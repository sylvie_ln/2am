{
    "id": "b41371ab-49b9-4d4c-85aa-c14737f44bfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sQuit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 3,
    "bbox_right": 77,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81e970fd-bb03-433e-878d-f3d4ea2d42c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b41371ab-49b9-4d4c-85aa-c14737f44bfe",
            "compositeImage": {
                "id": "310795ca-20ae-416b-a1e7-424be67abedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e970fd-bb03-433e-878d-f3d4ea2d42c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb251af-9dee-456c-94f4-292a2a69d247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e970fd-bb03-433e-878d-f3d4ea2d42c1",
                    "LayerId": "1d80242e-fae1-4ee4-aa7c-4db8281ea48c"
                }
            ]
        },
        {
            "id": "96d69c21-616a-491d-a454-6f2e6c26eea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b41371ab-49b9-4d4c-85aa-c14737f44bfe",
            "compositeImage": {
                "id": "d04720a3-2b4b-4a15-b248-79bc3c1ff923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d69c21-616a-491d-a454-6f2e6c26eea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2e016e0-151d-404d-bbbd-89a170a04133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d69c21-616a-491d-a454-6f2e6c26eea8",
                    "LayerId": "1d80242e-fae1-4ee4-aa7c-4db8281ea48c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "1d80242e-fae1-4ee4-aa7c-4db8281ea48c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b41371ab-49b9-4d4c-85aa-c14737f44bfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 79,
    "yorig": 47
}