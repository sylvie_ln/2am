{
    "id": "debc84e1-c804-4b7e-8fec-c65054aaebee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRoseKeySticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3d9a5af-9b7b-48a6-bba1-060694b94983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "debc84e1-c804-4b7e-8fec-c65054aaebee",
            "compositeImage": {
                "id": "df1bc619-6ed6-43ca-b12b-5e603c5f8d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3d9a5af-9b7b-48a6-bba1-060694b94983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1e6abc4-f476-4225-bbda-a461964376ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3d9a5af-9b7b-48a6-bba1-060694b94983",
                    "LayerId": "d8fbb4da-d669-471b-9305-78ebfc3a671c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d8fbb4da-d669-471b-9305-78ebfc3a671c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "debc84e1-c804-4b7e-8fec-c65054aaebee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}