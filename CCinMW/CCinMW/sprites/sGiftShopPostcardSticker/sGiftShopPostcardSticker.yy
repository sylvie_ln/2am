{
    "id": "7a2cf8d6-ebe5-434c-9a4f-fda6f78a6cc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopPostcardSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67985e4d-6da2-4d61-9c27-bc5c2db19cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a2cf8d6-ebe5-434c-9a4f-fda6f78a6cc9",
            "compositeImage": {
                "id": "a0a81e95-e2b0-441a-b53c-4e4915817607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67985e4d-6da2-4d61-9c27-bc5c2db19cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c39810-be33-4aa6-a6d4-0dbb355dbc95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67985e4d-6da2-4d61-9c27-bc5c2db19cef",
                    "LayerId": "c9c161df-fc40-4ae8-9703-0c25a9a579b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c9c161df-fc40-4ae8-9703-0c25a9a579b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a2cf8d6-ebe5-434c-9a4f-fda6f78a6cc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}