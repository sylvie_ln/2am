{
    "id": "aeef3519-ad38-4007-9af2-f78a4dc3adcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpringShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81e1ac34-4902-44d4-8f3c-7d432fa5765b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeef3519-ad38-4007-9af2-f78a4dc3adcb",
            "compositeImage": {
                "id": "6455977b-72bc-4c67-bf26-b7f567b24bb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e1ac34-4902-44d4-8f3c-7d432fa5765b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b29aba79-6d5b-4ffa-ae27-0378c3e375ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e1ac34-4902-44d4-8f3c-7d432fa5765b",
                    "LayerId": "aaed7e2d-a818-40ec-91ef-a40391aba82f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "aaed7e2d-a818-40ec-91ef-a40391aba82f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aeef3519-ad38-4007-9af2-f78a4dc3adcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}