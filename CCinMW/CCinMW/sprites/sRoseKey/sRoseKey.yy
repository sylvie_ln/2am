{
    "id": "20734242-6070-44a9-9d4d-d7a790e32e5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRoseKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33d28f22-d682-4d73-90f5-8c7e4d811560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20734242-6070-44a9-9d4d-d7a790e32e5b",
            "compositeImage": {
                "id": "99bf0c1f-aa5f-4430-b35c-6a3f58d5cab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d28f22-d682-4d73-90f5-8c7e4d811560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bf1f718-8a7d-4712-8ee6-46f301e46adc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d28f22-d682-4d73-90f5-8c7e4d811560",
                    "LayerId": "d812ab68-7b38-429f-a23d-8486f48fa786"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d812ab68-7b38-429f-a23d-8486f48fa786",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20734242-6070-44a9-9d4d-d7a790e32e5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}