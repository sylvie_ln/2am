{
    "id": "c97fb599-72c5-4868-874c-0cd7a2945ae9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHorseSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9186418-b858-47ad-9dfc-639948f2158d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c97fb599-72c5-4868-874c-0cd7a2945ae9",
            "compositeImage": {
                "id": "e50d36c6-aada-4c25-b536-e2b9900eef22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9186418-b858-47ad-9dfc-639948f2158d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b2756d5-6757-40f4-b6e7-f4b14cbeb8fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9186418-b858-47ad-9dfc-639948f2158d",
                    "LayerId": "0ba379e1-7d90-43fb-a777-1af695634ebc"
                },
                {
                    "id": "371bb1ea-7fb2-4d6d-b884-21520d0e7140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9186418-b858-47ad-9dfc-639948f2158d",
                    "LayerId": "037d5063-98c0-462b-8c99-dbdf02c272f5"
                },
                {
                    "id": "ec844e72-1a16-4900-bdc3-dbc247cb54d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9186418-b858-47ad-9dfc-639948f2158d",
                    "LayerId": "b387bef2-9583-4d93-ad49-5a664a0758b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0ba379e1-7d90-43fb-a777-1af695634ebc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c97fb599-72c5-4868-874c-0cd7a2945ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "037d5063-98c0-462b-8c99-dbdf02c272f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c97fb599-72c5-4868-874c-0cd7a2945ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b387bef2-9583-4d93-ad49-5a664a0758b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c97fb599-72c5-4868-874c-0cd7a2945ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 24
}