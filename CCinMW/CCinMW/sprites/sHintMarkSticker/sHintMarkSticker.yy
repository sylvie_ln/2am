{
    "id": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHintMarkSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfc53351-c0c7-4739-89d8-57a8ebce0af5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "bed0d80e-51e0-499b-a47b-1fd1b16ad7cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfc53351-c0c7-4739-89d8-57a8ebce0af5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc42e279-d4b7-42cd-9b70-850796301bcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfc53351-c0c7-4739-89d8-57a8ebce0af5",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "2fe0ce8d-db5f-4c84-8c1f-8c82d1568c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "6030a598-6be6-49d6-ac51-0d07eb09f0ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe0ce8d-db5f-4c84-8c1f-8c82d1568c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "761f1181-b74c-4fa5-bbac-9740a68f16b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe0ce8d-db5f-4c84-8c1f-8c82d1568c04",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "3d049b2c-bf0f-4e07-9b48-9442761c4631",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "4c52f477-0f7b-4fb2-8c10-092f00cb77cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d049b2c-bf0f-4e07-9b48-9442761c4631",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "956c711c-e577-4843-86e0-f989e034be53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d049b2c-bf0f-4e07-9b48-9442761c4631",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "84c5b9de-231e-4469-8129-f359c12d0fbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "f9bd79c0-e817-4097-98e1-41b4bb7a2234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c5b9de-231e-4469-8129-f359c12d0fbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f24a02-a10e-40e8-8cc7-66f10c64dd15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c5b9de-231e-4469-8129-f359c12d0fbb",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "c251e7b7-f672-4daf-b0fc-7979ff3ddf3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "147450ec-2bcc-45f5-9484-a05aa197704d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c251e7b7-f672-4daf-b0fc-7979ff3ddf3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9fb9f0d-ac15-418a-8d16-a737a8be3b72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c251e7b7-f672-4daf-b0fc-7979ff3ddf3a",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "8848d13c-1323-4e19-9cb0-95b01ca3ce34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "d863e137-c4de-4387-85dd-aa5a7579719b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8848d13c-1323-4e19-9cb0-95b01ca3ce34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "851d64dd-c26f-408c-a0af-822bebf71d9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8848d13c-1323-4e19-9cb0-95b01ca3ce34",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "8f9d8822-1bca-49a5-be55-ce3c4841c78b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "90120dcb-d116-422a-8b97-286a8508f69f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f9d8822-1bca-49a5-be55-ce3c4841c78b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aabf919-c6f1-490f-82d8-e28bd3f12d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f9d8822-1bca-49a5-be55-ce3c4841c78b",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "ed67bdd9-38a4-46ac-b673-2fe8729a5a94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "6c0f1ff2-95ff-4eea-a8b3-40dd2f95b6e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed67bdd9-38a4-46ac-b673-2fe8729a5a94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4fc3722-f63a-4138-b10a-e55c3fcb2f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed67bdd9-38a4-46ac-b673-2fe8729a5a94",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "c1aeed1b-b3cd-4359-ac84-e7072a00d289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "5c56b398-ad61-468b-a263-849e310ec498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1aeed1b-b3cd-4359-ac84-e7072a00d289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2484a486-b97d-4a7c-9506-1ecec5821b2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1aeed1b-b3cd-4359-ac84-e7072a00d289",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "066e026b-b95d-4f19-a655-c061210aa727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "c1aa06c6-ba07-4073-a706-cd7ace38470f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "066e026b-b95d-4f19-a655-c061210aa727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86308aa6-5f17-41bc-ab22-d4b1414223e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "066e026b-b95d-4f19-a655-c061210aa727",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "e7fc703f-e727-44d8-8d3a-7ac7afad0033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "58e3dc2b-351c-442b-b39b-4df324e66420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7fc703f-e727-44d8-8d3a-7ac7afad0033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbd3f8c-8e41-4e4f-be9c-d54d7ad70ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7fc703f-e727-44d8-8d3a-7ac7afad0033",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "17e8858c-12c3-4052-b3f8-8f6e305ea0eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "1d070820-34da-422f-bdaf-d4fb89c74169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e8858c-12c3-4052-b3f8-8f6e305ea0eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac1be033-01de-48d9-af29-790c1a42a9c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e8858c-12c3-4052-b3f8-8f6e305ea0eb",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "bc17ad3c-c593-4158-84ed-aca1cbc414f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "1a59532f-3d59-420e-880a-fae3a587e86a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc17ad3c-c593-4158-84ed-aca1cbc414f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "651ed5e9-beff-4f83-8d12-78fbfcc9b4e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc17ad3c-c593-4158-84ed-aca1cbc414f2",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "f5094fe6-6070-4fc3-9b5a-00bd30154ad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "cc0b5ac8-f051-40bc-9458-f1abaf9e6c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5094fe6-6070-4fc3-9b5a-00bd30154ad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88970d20-25e9-4c55-b827-f9709cf5c079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5094fe6-6070-4fc3-9b5a-00bd30154ad4",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "a802b643-90a0-4da2-862a-3407fb22c626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "11cf6c2b-cf24-4e62-896d-267410d98d2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a802b643-90a0-4da2-862a-3407fb22c626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ab97a9-95fd-4e72-b43a-046c9eb7d249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a802b643-90a0-4da2-862a-3407fb22c626",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "d09a2710-453c-4cc2-8dea-cba5593b6558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "c7ddf26e-00fc-45ef-a6f0-3258cef29759",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09a2710-453c-4cc2-8dea-cba5593b6558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4d21128-6174-44ba-a121-c193771251fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09a2710-453c-4cc2-8dea-cba5593b6558",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "9fe8b3b4-210f-4553-9853-cc895aea34bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "17d29f24-208c-4fc3-b71d-9fa6ed162875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fe8b3b4-210f-4553-9853-cc895aea34bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c732ed99-efe4-48a8-b34c-86cdc8e1505c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fe8b3b4-210f-4553-9853-cc895aea34bc",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "0dfae991-59b8-4cb1-9c7c-76ee2fb0701b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "fe040afe-a774-4c7c-853d-3a80b1e3c390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dfae991-59b8-4cb1-9c7c-76ee2fb0701b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d1e8cdc-7898-4728-9188-a82b4533dea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dfae991-59b8-4cb1-9c7c-76ee2fb0701b",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "8878e8f9-4923-4ad6-924d-4b07bd96e88e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "b0ff5965-0f7a-4119-84ea-92e1d5addbb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8878e8f9-4923-4ad6-924d-4b07bd96e88e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6d316dc-6f45-4581-87b6-1069ad3bcfdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8878e8f9-4923-4ad6-924d-4b07bd96e88e",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "36b7b3dd-f73f-407f-8253-037d2828ef86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "5b2b791e-8c4b-42e2-b490-1d54bdfd17d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b7b3dd-f73f-407f-8253-037d2828ef86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53842069-cb5b-4114-8f03-24ef4a24e390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b7b3dd-f73f-407f-8253-037d2828ef86",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "bce74721-b452-4ce8-aba4-ebc13ec1bb2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "e59c1b80-0a3a-4380-b005-39220a2e63d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bce74721-b452-4ce8-aba4-ebc13ec1bb2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b236b9-71ba-4723-b8a0-59fdc6358f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bce74721-b452-4ce8-aba4-ebc13ec1bb2a",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "7f06b011-7abd-4043-ba8a-07bf67af5056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "5a34dc92-b15c-459a-9335-4c0ea0226e17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f06b011-7abd-4043-ba8a-07bf67af5056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7464a5f9-e9fc-4b66-a8ca-f1dd8079a7ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f06b011-7abd-4043-ba8a-07bf67af5056",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "6390df01-cc02-46ac-a3e6-46c4bef8ef99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "85354d44-4bf7-4db2-bb75-3830625467cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6390df01-cc02-46ac-a3e6-46c4bef8ef99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50bc36b2-3a63-43cb-ad36-f8e5c0ee6127",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6390df01-cc02-46ac-a3e6-46c4bef8ef99",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "62b3de7f-ba59-4618-8d69-d818cbf4a389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "33a108c8-bf59-4e8b-8097-1e72065c1e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b3de7f-ba59-4618-8d69-d818cbf4a389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc5c536a-d616-436b-850a-a0a68eeb46bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b3de7f-ba59-4618-8d69-d818cbf4a389",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "ce9e9647-bf42-4fd3-9b46-6278fd1e958c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "2062a43c-dacc-4b86-bc31-49a368d92ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce9e9647-bf42-4fd3-9b46-6278fd1e958c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acffd128-dad7-4e0e-97dd-dd79270aed03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce9e9647-bf42-4fd3-9b46-6278fd1e958c",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "5985261d-3c00-42e9-9f11-1db3699a2bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "d1c1d158-8ec0-4848-a98a-e0b1d2e78992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5985261d-3c00-42e9-9f11-1db3699a2bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65e49f21-de8b-4964-8526-17d1f7f48a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5985261d-3c00-42e9-9f11-1db3699a2bb8",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "d0b9172e-7097-4e5b-b960-0eef4ba221e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "b2318b57-d098-4b39-a8fc-4b0b98ddc571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0b9172e-7097-4e5b-b960-0eef4ba221e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c83faead-d666-418d-a5e2-f066bd88ed66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0b9172e-7097-4e5b-b960-0eef4ba221e0",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "ba690937-e08c-424b-a257-b775226388e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "f2ed4951-2f32-43eb-9ad3-2fed94c3cf69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba690937-e08c-424b-a257-b775226388e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "211a313b-608e-4b92-ac42-d818a4cad8c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba690937-e08c-424b-a257-b775226388e2",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "6f6c878f-8bc2-4c42-82ee-764d40d6faca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "49c5071b-ed88-4f3a-ba31-18eed1126f53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f6c878f-8bc2-4c42-82ee-764d40d6faca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c9d966-3fb2-4b58-bca7-51ab0c66fc86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f6c878f-8bc2-4c42-82ee-764d40d6faca",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        },
        {
            "id": "60f609ce-7ba3-46ae-afe6-122a5ca41475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "compositeImage": {
                "id": "b6f0dcab-7b7f-4266-84d2-dc15b25e8e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60f609ce-7ba3-46ae-afe6-122a5ca41475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4dea56-10dd-4fe2-8fbd-a5ce7236e41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60f609ce-7ba3-46ae-afe6-122a5ca41475",
                    "LayerId": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "af5c5075-c0e6-4f5d-8af4-729c4634f8e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a7cfed9-1671-46f7-b2ea-c58b6d584acf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}