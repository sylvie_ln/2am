{
    "id": "f77ba2a9-1de0-4458-a18f-24c51b42002a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b5dfff7-ce6c-4993-a8b2-03ac8486d506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f77ba2a9-1de0-4458-a18f-24c51b42002a",
            "compositeImage": {
                "id": "58c5546d-4616-442d-85c0-c2f2cb2e8b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b5dfff7-ce6c-4993-a8b2-03ac8486d506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c54c1c-3d6a-4a9d-b0ae-6b5560ab2b7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b5dfff7-ce6c-4993-a8b2-03ac8486d506",
                    "LayerId": "b2e420bf-4624-45d6-b8a4-d4c0558b64af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "b2e420bf-4624-45d6-b8a4-d4c0558b64af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f77ba2a9-1de0-4458-a18f-24c51b42002a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}