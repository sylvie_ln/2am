{
    "id": "b0a364c2-da7d-42be-ba52-79115efccd7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAliciaBunney",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44eca1c4-6d66-4185-82ea-940ff6687362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a364c2-da7d-42be-ba52-79115efccd7c",
            "compositeImage": {
                "id": "b15a3c4a-5a34-49a9-b852-7412cc2e4130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44eca1c4-6d66-4185-82ea-940ff6687362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2622e35b-b40f-4ea7-8e5b-68ad2d2092f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44eca1c4-6d66-4185-82ea-940ff6687362",
                    "LayerId": "9e23b52c-38e7-483e-b94a-f264abbe965d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9e23b52c-38e7-483e-b94a-f264abbe965d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0a364c2-da7d-42be-ba52-79115efccd7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}