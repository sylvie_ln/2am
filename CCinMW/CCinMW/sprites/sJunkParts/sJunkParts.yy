{
    "id": "ce2b9366-662d-45b8-b271-1535524137e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJunkParts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "428a5fe0-3b66-4876-ad97-eebb5bebd1d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce2b9366-662d-45b8-b271-1535524137e9",
            "compositeImage": {
                "id": "9b0f12d7-eb91-4e4b-bbc8-c727869e977c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "428a5fe0-3b66-4876-ad97-eebb5bebd1d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e776ecb-19fa-487f-bd15-cedfef56d3d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "428a5fe0-3b66-4876-ad97-eebb5bebd1d4",
                    "LayerId": "cd6e63c1-127f-4b17-a66e-d33a86db11e4"
                }
            ]
        },
        {
            "id": "2675cad0-3e46-4679-a62f-e712f5a85cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce2b9366-662d-45b8-b271-1535524137e9",
            "compositeImage": {
                "id": "f31259b3-231c-4829-8a55-27a32a7211b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2675cad0-3e46-4679-a62f-e712f5a85cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00675688-8505-4179-a965-f9ef87ee644f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2675cad0-3e46-4679-a62f-e712f5a85cea",
                    "LayerId": "cd6e63c1-127f-4b17-a66e-d33a86db11e4"
                }
            ]
        },
        {
            "id": "1f53807c-16b5-4e8f-8c35-d56080a52c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce2b9366-662d-45b8-b271-1535524137e9",
            "compositeImage": {
                "id": "7491601f-9f82-465d-ae4f-95ae46837a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f53807c-16b5-4e8f-8c35-d56080a52c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1201bb45-2ab7-4b14-85fd-483031805dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f53807c-16b5-4e8f-8c35-d56080a52c18",
                    "LayerId": "cd6e63c1-127f-4b17-a66e-d33a86db11e4"
                }
            ]
        },
        {
            "id": "17fa50e2-8ade-4795-b42a-d82a5e98a79d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce2b9366-662d-45b8-b271-1535524137e9",
            "compositeImage": {
                "id": "a6bda5d7-4ff5-4cf1-b00b-cfee0cafcb0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17fa50e2-8ade-4795-b42a-d82a5e98a79d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53e48a69-ea03-48dd-ac32-449c32cdf6d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17fa50e2-8ade-4795-b42a-d82a5e98a79d",
                    "LayerId": "cd6e63c1-127f-4b17-a66e-d33a86db11e4"
                }
            ]
        },
        {
            "id": "1baa8697-7939-4d28-989e-1e4c4379731d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce2b9366-662d-45b8-b271-1535524137e9",
            "compositeImage": {
                "id": "a99d2b3d-f80e-4301-b413-0416303e1aad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1baa8697-7939-4d28-989e-1e4c4379731d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76dfbbce-6524-4cd8-b8df-e7c7e95224f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1baa8697-7939-4d28-989e-1e4c4379731d",
                    "LayerId": "cd6e63c1-127f-4b17-a66e-d33a86db11e4"
                }
            ]
        },
        {
            "id": "c85fe967-16be-4048-ae8e-8416bf3d420b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce2b9366-662d-45b8-b271-1535524137e9",
            "compositeImage": {
                "id": "3bd76331-7512-4bf2-9325-52502ce8fe19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c85fe967-16be-4048-ae8e-8416bf3d420b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb89e935-708e-47be-b40e-bde4349edc37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c85fe967-16be-4048-ae8e-8416bf3d420b",
                    "LayerId": "cd6e63c1-127f-4b17-a66e-d33a86db11e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cd6e63c1-127f-4b17-a66e-d33a86db11e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce2b9366-662d-45b8-b271-1535524137e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        1090519039,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}