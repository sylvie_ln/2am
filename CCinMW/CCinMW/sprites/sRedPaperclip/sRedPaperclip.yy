{
    "id": "8692fd52-a864-4e28-b67f-2b80d090960b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRedPaperclip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08ab4554-ed8b-4446-9ba4-4ae0f75c9a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8692fd52-a864-4e28-b67f-2b80d090960b",
            "compositeImage": {
                "id": "acc620f6-198b-4715-88fc-59c8fe667acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ab4554-ed8b-4446-9ba4-4ae0f75c9a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1447aa6e-8450-4cf9-aff5-327fbf516414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ab4554-ed8b-4446-9ba4-4ae0f75c9a4c",
                    "LayerId": "846d02ff-e18a-4900-82d0-30a95547a414"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "846d02ff-e18a-4900-82d0-30a95547a414",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8692fd52-a864-4e28-b67f-2b80d090960b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}