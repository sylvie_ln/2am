{
    "id": "491dcb86-4c04-4edb-8a1b-4e32b95694df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGuardiansScimitarSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 10,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a2a479a-04af-4eb4-99a0-03ee089413fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "491dcb86-4c04-4edb-8a1b-4e32b95694df",
            "compositeImage": {
                "id": "40ef717e-c459-4f05-b02c-6e8d56baf83c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a2a479a-04af-4eb4-99a0-03ee089413fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b46e65-5610-4ac0-835c-50fa740916ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a2a479a-04af-4eb4-99a0-03ee089413fc",
                    "LayerId": "29081f0d-8786-4e2e-971e-e58f97f8c1f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "29081f0d-8786-4e2e-971e-e58f97f8c1f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "491dcb86-4c04-4edb-8a1b-4e32b95694df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 6,
    "yorig": 10
}