{
    "id": "8facc5ff-263b-4b15-bea5-fbaa24f1e97b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecartLicense",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72f42d4f-fb01-4cbc-a175-a4b2e346de78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8facc5ff-263b-4b15-bea5-fbaa24f1e97b",
            "compositeImage": {
                "id": "34f30ae3-876c-433e-9274-77adbaa33313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f42d4f-fb01-4cbc-a175-a4b2e346de78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3054937-c1fc-4120-bb8a-3721d297dfbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f42d4f-fb01-4cbc-a175-a4b2e346de78",
                    "LayerId": "c5edaf83-c827-458e-8504-aaee36ef7d6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c5edaf83-c827-458e-8504-aaee36ef7d6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8facc5ff-263b-4b15-bea5-fbaa24f1e97b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 12
}