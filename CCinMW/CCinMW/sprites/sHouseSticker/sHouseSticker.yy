{
    "id": "9974760c-15fe-41bd-9000-07f60034a5bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouseSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df6d6c8d-7c9c-4113-9d20-3935551b95bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9974760c-15fe-41bd-9000-07f60034a5bf",
            "compositeImage": {
                "id": "22f0d5a8-f195-48bd-bae3-36da896bbf9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df6d6c8d-7c9c-4113-9d20-3935551b95bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f81843-2492-41a8-baaa-e7ecfcfe555f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6d6c8d-7c9c-4113-9d20-3935551b95bb",
                    "LayerId": "b1d6381d-9c0b-4ca7-b894-adf612171459"
                },
                {
                    "id": "1db83729-b765-4ef6-a16f-87b6b6481fcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6d6c8d-7c9c-4113-9d20-3935551b95bb",
                    "LayerId": "0db4cb34-b89e-4a89-b49b-fb93f1068b7f"
                },
                {
                    "id": "34412f4a-aa2c-4917-a0e7-1ebb5f99dbd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6d6c8d-7c9c-4113-9d20-3935551b95bb",
                    "LayerId": "80147bba-0dd5-48e9-b5c4-1ad36d8205ca"
                },
                {
                    "id": "c963bf49-53b5-4da4-93d5-8da3a0d9d0fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6d6c8d-7c9c-4113-9d20-3935551b95bb",
                    "LayerId": "6aca4e43-8466-4e6e-a7c3-4ac3512628bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b1d6381d-9c0b-4ca7-b894-adf612171459",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9974760c-15fe-41bd-9000-07f60034a5bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0db4cb34-b89e-4a89-b49b-fb93f1068b7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9974760c-15fe-41bd-9000-07f60034a5bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "80147bba-0dd5-48e9-b5c4-1ad36d8205ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9974760c-15fe-41bd-9000-07f60034a5bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 25,
            "visible": true
        },
        {
            "id": "6aca4e43-8466-4e6e-a7c3-4ac3512628bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9974760c-15fe-41bd-9000-07f60034a5bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}