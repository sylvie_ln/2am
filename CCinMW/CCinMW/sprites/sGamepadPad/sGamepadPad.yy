{
    "id": "cd9eca1c-13b9-4c64-a92f-bdd4d9b99a1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16a3ab18-57ed-431b-9fbc-9004b72b156e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd9eca1c-13b9-4c64-a92f-bdd4d9b99a1e",
            "compositeImage": {
                "id": "2a36a6db-8230-4ba1-bab2-3a6b639fe957",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a3ab18-57ed-431b-9fbc-9004b72b156e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4341e958-2281-4c79-a736-624858a9451c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a3ab18-57ed-431b-9fbc-9004b72b156e",
                    "LayerId": "1b774577-bee9-4f1b-8a44-040372064fa0"
                },
                {
                    "id": "e4e1581f-e7af-42c0-8153-88a218fb5436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a3ab18-57ed-431b-9fbc-9004b72b156e",
                    "LayerId": "ebcee626-03a2-4b8a-b487-d4d4d688a682"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "1b774577-bee9-4f1b-8a44-040372064fa0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd9eca1c-13b9-4c64-a92f-bdd4d9b99a1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ebcee626-03a2-4b8a-b487-d4d4d688a682",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd9eca1c-13b9-4c64-a92f-bdd4d9b99a1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}