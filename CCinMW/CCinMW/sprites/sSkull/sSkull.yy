{
    "id": "6e3cd88c-6c10-4a4c-85e0-fe0241eb9c69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSkull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6d9867a-28a3-4c9e-b1c2-3a1904f5ec6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e3cd88c-6c10-4a4c-85e0-fe0241eb9c69",
            "compositeImage": {
                "id": "1deed176-c1f5-46b5-9330-7aa55d488478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d9867a-28a3-4c9e-b1c2-3a1904f5ec6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f1dfd4-a65f-4999-b545-42a210694591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d9867a-28a3-4c9e-b1c2-3a1904f5ec6b",
                    "LayerId": "b392e823-ffbe-414d-a664-8b19a56afd7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b392e823-ffbe-414d-a664-8b19a56afd7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e3cd88c-6c10-4a4c-85e0-fe0241eb9c69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}