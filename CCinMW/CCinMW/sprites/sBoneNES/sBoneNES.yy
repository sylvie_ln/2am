{
    "id": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoneNES",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d501f13-17e3-470b-bf88-6e3c10e4c0bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "compositeImage": {
                "id": "a5b4acc5-94a0-4f37-8477-79b440af7fa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d501f13-17e3-470b-bf88-6e3c10e4c0bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9127674b-f263-4fc3-af3b-9d0fa53c7846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d501f13-17e3-470b-bf88-6e3c10e4c0bf",
                    "LayerId": "a1197718-f898-4ba5-a5d7-1cd7a73877dd"
                }
            ]
        },
        {
            "id": "730a7bd6-5499-4a27-96f9-473c8c7c0c79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "compositeImage": {
                "id": "b617aae0-e103-4a21-8cd4-fdf5f35bba05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "730a7bd6-5499-4a27-96f9-473c8c7c0c79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88e2476-a99e-4859-a5a0-ee0af052f381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "730a7bd6-5499-4a27-96f9-473c8c7c0c79",
                    "LayerId": "a1197718-f898-4ba5-a5d7-1cd7a73877dd"
                }
            ]
        },
        {
            "id": "7068add4-673b-404c-a3e9-18e7fa4dc987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "compositeImage": {
                "id": "ebcfe7ea-8b05-47ea-a204-496de064fc75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7068add4-673b-404c-a3e9-18e7fa4dc987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "983ee403-9cda-4b8f-897f-f020025cc3f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7068add4-673b-404c-a3e9-18e7fa4dc987",
                    "LayerId": "a1197718-f898-4ba5-a5d7-1cd7a73877dd"
                }
            ]
        },
        {
            "id": "0ea727e1-79f5-46c5-9dba-16918212754b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "compositeImage": {
                "id": "49cb81a5-7ec2-4971-8807-38a6bb84a5a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea727e1-79f5-46c5-9dba-16918212754b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8398f5a6-1744-4c00-bf31-fb6543b2e4e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea727e1-79f5-46c5-9dba-16918212754b",
                    "LayerId": "a1197718-f898-4ba5-a5d7-1cd7a73877dd"
                }
            ]
        },
        {
            "id": "d4779c92-13b7-48b2-af6f-f68909b045c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "compositeImage": {
                "id": "a424bb5b-22f5-4c47-960d-17ef5cc7cf48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4779c92-13b7-48b2-af6f-f68909b045c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5c3042-72cb-4364-9ea2-7d5540939f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4779c92-13b7-48b2-af6f-f68909b045c0",
                    "LayerId": "a1197718-f898-4ba5-a5d7-1cd7a73877dd"
                }
            ]
        },
        {
            "id": "9159fa75-df98-43db-ab5f-0ddaa298ec3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "compositeImage": {
                "id": "d7d1cbbd-df18-4e00-8913-4dfab2b1803f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9159fa75-df98-43db-ab5f-0ddaa298ec3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19f754eb-d457-4195-b1be-5026742ecf38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9159fa75-df98-43db-ab5f-0ddaa298ec3a",
                    "LayerId": "a1197718-f898-4ba5-a5d7-1cd7a73877dd"
                }
            ]
        },
        {
            "id": "fff0ccf5-1995-4c86-bc5f-57720e060f01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "compositeImage": {
                "id": "c0dfe66b-a654-4fe8-8585-2368c59cf79b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fff0ccf5-1995-4c86-bc5f-57720e060f01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75950a42-0428-4348-ba77-4f1dce2fc56c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fff0ccf5-1995-4c86-bc5f-57720e060f01",
                    "LayerId": "a1197718-f898-4ba5-a5d7-1cd7a73877dd"
                }
            ]
        },
        {
            "id": "91c68ee0-d8f7-4bc3-b943-bb1be5779c3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "compositeImage": {
                "id": "8362eebc-c0ce-4cd1-9468-da6d139f75c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c68ee0-d8f7-4bc3-b943-bb1be5779c3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7109e2a-7678-4f14-9f30-e4c83b95345b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c68ee0-d8f7-4bc3-b943-bb1be5779c3d",
                    "LayerId": "a1197718-f898-4ba5-a5d7-1cd7a73877dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a1197718-f898-4ba5-a5d7-1cd7a73877dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8c5d0bc-a789-42a8-88a6-3dc4469aa1a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}