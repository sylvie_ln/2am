{
    "id": "bcd60b32-28ce-459d-b683-f89ecd64c4ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMemoryDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a3b15e8-3494-4ba0-8980-6fcd7067e27c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcd60b32-28ce-459d-b683-f89ecd64c4ac",
            "compositeImage": {
                "id": "b9e36aa4-9db0-474f-b101-511d178dcb27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a3b15e8-3494-4ba0-8980-6fcd7067e27c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba1723f-fcc9-450a-92cf-391f15ac986b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a3b15e8-3494-4ba0-8980-6fcd7067e27c",
                    "LayerId": "032a30c1-8d2f-444c-a705-5fda5856e005"
                },
                {
                    "id": "76a87df0-a188-4e28-89db-25047dffd2e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a3b15e8-3494-4ba0-8980-6fcd7067e27c",
                    "LayerId": "9ab643e9-105f-44c2-9a98-ec02dd3944d8"
                },
                {
                    "id": "aaae3385-4018-4132-ba44-dc9db7d56a2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a3b15e8-3494-4ba0-8980-6fcd7067e27c",
                    "LayerId": "1dc625e6-ffcd-491b-a99f-89df6cfe9109"
                },
                {
                    "id": "b80ac748-5d86-4cb9-8f52-a7a86130bffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a3b15e8-3494-4ba0-8980-6fcd7067e27c",
                    "LayerId": "39538aee-0a2b-4009-b09d-c4cb55e4e0ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "032a30c1-8d2f-444c-a705-5fda5856e005",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcd60b32-28ce-459d-b683-f89ecd64c4ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "9ab643e9-105f-44c2-9a98-ec02dd3944d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcd60b32-28ce-459d-b683-f89ecd64c4ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "1dc625e6-ffcd-491b-a99f-89df6cfe9109",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcd60b32-28ce-459d-b683-f89ecd64c4ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "39538aee-0a2b-4009-b09d-c4cb55e4e0ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcd60b32-28ce-459d-b683-f89ecd64c4ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}