{
    "id": "b6659363-605f-4802-912b-7a076fce16e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanyonDark1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3ec17d4-7b16-419d-8872-b7337c7c5aaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6659363-605f-4802-912b-7a076fce16e9",
            "compositeImage": {
                "id": "cb406ff6-b62b-4d25-823c-f9fade6a341a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ec17d4-7b16-419d-8872-b7337c7c5aaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62bcc129-e1df-48b5-9123-2513f7ffb2af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ec17d4-7b16-419d-8872-b7337c7c5aaa",
                    "LayerId": "bc2781ad-2609-47eb-83f5-bb725f3438c8"
                }
            ]
        },
        {
            "id": "d55021b9-9bbe-4111-a6a8-ff37e9d17bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6659363-605f-4802-912b-7a076fce16e9",
            "compositeImage": {
                "id": "7f0c574a-919f-4d4d-9d19-aa5022bb9ca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d55021b9-9bbe-4111-a6a8-ff37e9d17bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5efb3777-4e26-4dfc-ad63-04997fb41d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d55021b9-9bbe-4111-a6a8-ff37e9d17bbc",
                    "LayerId": "bc2781ad-2609-47eb-83f5-bb725f3438c8"
                }
            ]
        },
        {
            "id": "f8fc4e11-ac44-4285-86aa-2fc5ad42bd19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6659363-605f-4802-912b-7a076fce16e9",
            "compositeImage": {
                "id": "3830a157-1944-4aed-9257-722f34048e4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8fc4e11-ac44-4285-86aa-2fc5ad42bd19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f93265ef-ca29-41be-a60a-465bd87f648f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8fc4e11-ac44-4285-86aa-2fc5ad42bd19",
                    "LayerId": "bc2781ad-2609-47eb-83f5-bb725f3438c8"
                }
            ]
        },
        {
            "id": "3e01388f-9a2a-425e-8b28-f220e4e3dbc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6659363-605f-4802-912b-7a076fce16e9",
            "compositeImage": {
                "id": "4fb20956-103b-454d-b3ac-9dce48d0f6c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e01388f-9a2a-425e-8b28-f220e4e3dbc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fb6cb84-5ce8-4fe6-bcfa-4ff45e38b817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e01388f-9a2a-425e-8b28-f220e4e3dbc6",
                    "LayerId": "bc2781ad-2609-47eb-83f5-bb725f3438c8"
                }
            ]
        },
        {
            "id": "7f98f4dd-42af-497c-8422-5bb588f9d6fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6659363-605f-4802-912b-7a076fce16e9",
            "compositeImage": {
                "id": "be80e925-ae5e-459e-b1eb-3d73ef79b6c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f98f4dd-42af-497c-8422-5bb588f9d6fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4417d9-9a2c-4c6d-b8c1-70b9176a581f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f98f4dd-42af-497c-8422-5bb588f9d6fb",
                    "LayerId": "bc2781ad-2609-47eb-83f5-bb725f3438c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bc2781ad-2609-47eb-83f5-bb725f3438c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6659363-605f-4802-912b-7a076fce16e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}