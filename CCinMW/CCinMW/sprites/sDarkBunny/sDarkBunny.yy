{
    "id": "721bbf71-ebb1-4de2-ae6c-606c1bf2c9b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDarkBunny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f160f4e-1603-4f32-84a9-24113d19dd0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "721bbf71-ebb1-4de2-ae6c-606c1bf2c9b6",
            "compositeImage": {
                "id": "06ec9697-0ab6-46fe-941f-1f1968379d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f160f4e-1603-4f32-84a9-24113d19dd0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2b69cf-a61f-4391-8147-66b07037ce63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f160f4e-1603-4f32-84a9-24113d19dd0d",
                    "LayerId": "26679343-0408-4d76-a42b-9640f9867299"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "26679343-0408-4d76-a42b-9640f9867299",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "721bbf71-ebb1-4de2-ae6c-606c1bf2c9b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}