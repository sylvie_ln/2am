{
    "id": "84147faa-634a-4928-ae2b-c348d701d37b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharWingsNES",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6345bb63-fe87-43d0-bf7f-67b1bd789c7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84147faa-634a-4928-ae2b-c348d701d37b",
            "compositeImage": {
                "id": "3d6d4a45-e197-4044-80d5-a20e78ad42c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6345bb63-fe87-43d0-bf7f-67b1bd789c7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d342470-472d-4cb2-a976-471f1d36f761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6345bb63-fe87-43d0-bf7f-67b1bd789c7c",
                    "LayerId": "e2b2810a-be18-49c1-a80b-56c0d302dc45"
                }
            ]
        },
        {
            "id": "eb26df93-a557-4fe2-95d6-e393fcd1d277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84147faa-634a-4928-ae2b-c348d701d37b",
            "compositeImage": {
                "id": "d581f9d1-051c-44e0-91f4-ce7a1b335830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb26df93-a557-4fe2-95d6-e393fcd1d277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec204e2-24ac-4b70-b029-1fb1b09733e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb26df93-a557-4fe2-95d6-e393fcd1d277",
                    "LayerId": "e2b2810a-be18-49c1-a80b-56c0d302dc45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e2b2810a-be18-49c1-a80b-56c0d302dc45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84147faa-634a-4928-ae2b-c348d701d37b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 8
}