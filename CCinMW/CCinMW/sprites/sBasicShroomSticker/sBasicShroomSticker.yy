{
    "id": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBasicShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9b17eff-e472-4c2c-8cb5-30c36bafc456",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
            "compositeImage": {
                "id": "34a37bb0-4d8c-4ab3-a1b5-b9c3bd8a1d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9b17eff-e472-4c2c-8cb5-30c36bafc456",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "615b403c-e416-47c0-b11d-326590d8bcff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9b17eff-e472-4c2c-8cb5-30c36bafc456",
                    "LayerId": "f0a7d473-2d0c-4df6-a67a-fe0f34d12833"
                }
            ]
        },
        {
            "id": "a131ec15-b3f0-49e7-a39f-e1b7e939a188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
            "compositeImage": {
                "id": "652280f3-d9b4-40e1-83f3-549f4e39f0e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a131ec15-b3f0-49e7-a39f-e1b7e939a188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "416016ac-bc5e-4712-ba49-4690cdafaa8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a131ec15-b3f0-49e7-a39f-e1b7e939a188",
                    "LayerId": "f0a7d473-2d0c-4df6-a67a-fe0f34d12833"
                }
            ]
        },
        {
            "id": "03db1043-211c-4877-aaee-0055a1e96d3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
            "compositeImage": {
                "id": "eca6a356-4a99-4dff-b88c-d205374b0eae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03db1043-211c-4877-aaee-0055a1e96d3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a796f284-a6b5-4292-96ea-63861bcc5a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03db1043-211c-4877-aaee-0055a1e96d3d",
                    "LayerId": "f0a7d473-2d0c-4df6-a67a-fe0f34d12833"
                }
            ]
        },
        {
            "id": "132b4dd4-698f-49d7-b375-3061c1be0e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
            "compositeImage": {
                "id": "705d7a5d-62d1-4117-acf3-892a4c4fb2dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132b4dd4-698f-49d7-b375-3061c1be0e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87defb21-d076-456b-822a-46d42fec10ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132b4dd4-698f-49d7-b375-3061c1be0e3e",
                    "LayerId": "f0a7d473-2d0c-4df6-a67a-fe0f34d12833"
                }
            ]
        },
        {
            "id": "be921e1a-cad4-4912-b82b-f429f0cca6e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
            "compositeImage": {
                "id": "1f91be41-84e9-4a6a-ab8f-bf9fa9080d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be921e1a-cad4-4912-b82b-f429f0cca6e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29feeb6f-e36f-4a26-a581-6bcfdad79431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be921e1a-cad4-4912-b82b-f429f0cca6e7",
                    "LayerId": "f0a7d473-2d0c-4df6-a67a-fe0f34d12833"
                }
            ]
        },
        {
            "id": "5d336e6c-1200-417a-857b-4b745ee484c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
            "compositeImage": {
                "id": "97231e8a-4378-411f-ad61-05f5190406b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d336e6c-1200-417a-857b-4b745ee484c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d8f0c6c-55af-411e-92c0-5d5dc0813dfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d336e6c-1200-417a-857b-4b745ee484c9",
                    "LayerId": "f0a7d473-2d0c-4df6-a67a-fe0f34d12833"
                }
            ]
        },
        {
            "id": "8cf9cbaa-33d5-4f31-bf8c-624a4123d47d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
            "compositeImage": {
                "id": "e18f3ee0-0de5-42d3-b4d0-318ff19db4a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cf9cbaa-33d5-4f31-bf8c-624a4123d47d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01225c00-9365-40b1-8b54-a223d1ff8733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cf9cbaa-33d5-4f31-bf8c-624a4123d47d",
                    "LayerId": "f0a7d473-2d0c-4df6-a67a-fe0f34d12833"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f0a7d473-2d0c-4df6-a67a-fe0f34d12833",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f41b5ea-8086-4b57-b4c6-485d40b686bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901972,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}