{
    "id": "7306aa8c-235c-4f21-a956-c5e561bd1ab5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharDieLand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dac7abf-0fb4-449d-bd69-0c85155914e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7306aa8c-235c-4f21-a956-c5e561bd1ab5",
            "compositeImage": {
                "id": "a87ba019-2683-4fa9-9339-ed1ac3699191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dac7abf-0fb4-449d-bd69-0c85155914e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472ad487-44ff-46a3-8a05-4d0e9b752613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dac7abf-0fb4-449d-bd69-0c85155914e4",
                    "LayerId": "7b424bdd-b2b6-4a04-a959-eec192591340"
                }
            ]
        },
        {
            "id": "7b641ebf-64f2-4033-bcc8-2760fda96e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7306aa8c-235c-4f21-a956-c5e561bd1ab5",
            "compositeImage": {
                "id": "6fde6513-9b3b-47e0-96d1-73f6a942e529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b641ebf-64f2-4033-bcc8-2760fda96e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a2c87ae-2a9c-4e78-b675-bd4a4fccea01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b641ebf-64f2-4033-bcc8-2760fda96e3c",
                    "LayerId": "7b424bdd-b2b6-4a04-a959-eec192591340"
                }
            ]
        },
        {
            "id": "516df1df-1891-4633-84bc-5b1db7e152c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7306aa8c-235c-4f21-a956-c5e561bd1ab5",
            "compositeImage": {
                "id": "ff589349-d213-42f6-b967-920a3de526d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516df1df-1891-4633-84bc-5b1db7e152c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaeadb59-9f1f-47dd-b6c6-bcebd6e198a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516df1df-1891-4633-84bc-5b1db7e152c0",
                    "LayerId": "7b424bdd-b2b6-4a04-a959-eec192591340"
                }
            ]
        },
        {
            "id": "eb2145f0-e3e8-4945-9db7-8b15b1200c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7306aa8c-235c-4f21-a956-c5e561bd1ab5",
            "compositeImage": {
                "id": "d85c6548-4aa7-44be-a8d6-8eaf3c17be93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb2145f0-e3e8-4945-9db7-8b15b1200c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56599faa-02ba-4ea3-ae79-1cfe8125664e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb2145f0-e3e8-4945-9db7-8b15b1200c18",
                    "LayerId": "7b424bdd-b2b6-4a04-a959-eec192591340"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7b424bdd-b2b6-4a04-a959-eec192591340",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7306aa8c-235c-4f21-a956-c5e561bd1ab5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}