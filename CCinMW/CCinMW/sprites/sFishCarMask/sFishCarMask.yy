{
    "id": "dad2386d-1d23-413e-baa4-9725be2ceb43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFishCarMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eddc62b7-ca84-4b61-a7fd-e612d63b3195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dad2386d-1d23-413e-baa4-9725be2ceb43",
            "compositeImage": {
                "id": "fedfeb22-ba2e-4449-888e-4ecf5f059638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eddc62b7-ca84-4b61-a7fd-e612d63b3195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f1f8e23-89b3-4bc7-b9ec-45d4dad014ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eddc62b7-ca84-4b61-a7fd-e612d63b3195",
                    "LayerId": "718f89fe-9d96-427f-ba92-6e0ed9bee036"
                },
                {
                    "id": "940b2c38-8fe2-4b48-be3c-b400b66e7235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eddc62b7-ca84-4b61-a7fd-e612d63b3195",
                    "LayerId": "13083b80-f563-4179-8790-e117ba72bf84"
                },
                {
                    "id": "8e49ea20-2fc1-4944-930f-44454130bb21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eddc62b7-ca84-4b61-a7fd-e612d63b3195",
                    "LayerId": "9d2a73d5-47da-4ed1-b0f2-4fc785ac4009"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "9d2a73d5-47da-4ed1-b0f2-4fc785ac4009",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dad2386d-1d23-413e-baa4-9725be2ceb43",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "718f89fe-9d96-427f-ba92-6e0ed9bee036",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dad2386d-1d23-413e-baa4-9725be2ceb43",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "13083b80-f563-4179-8790-e117ba72bf84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dad2386d-1d23-413e-baa4-9725be2ceb43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}