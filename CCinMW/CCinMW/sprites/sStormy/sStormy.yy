{
    "id": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStormy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81f36590-8224-4eec-ac98-bf9fb5a83005",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "069bdd63-3df4-46ca-88de-1111fe5646da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f36590-8224-4eec-ac98-bf9fb5a83005",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b39a21f-2211-4c64-a853-065660352604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f36590-8224-4eec-ac98-bf9fb5a83005",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "2254dfc9-41ae-4060-b579-d138261c8649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "0441eefc-4d43-41f9-8d6f-fdc2208c7d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2254dfc9-41ae-4060-b579-d138261c8649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46d2095f-b9da-47c1-9f48-ec7d3bdd17b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2254dfc9-41ae-4060-b579-d138261c8649",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "87ec525e-d46c-4ae9-b604-ccafbe481719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "b2d804f2-c816-4813-93a2-db10e8f599fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87ec525e-d46c-4ae9-b604-ccafbe481719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84799708-aae1-4de3-bbe1-0f841fcefc06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87ec525e-d46c-4ae9-b604-ccafbe481719",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "e26ca208-edd3-4fbc-8b0c-6e327cc97ead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "2223c533-bd6a-469b-b3a4-8a35e80476ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26ca208-edd3-4fbc-8b0c-6e327cc97ead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb6817b-d576-4328-a2ca-0c7380dfa42d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26ca208-edd3-4fbc-8b0c-6e327cc97ead",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "52f63abc-db2b-41e9-83ae-82f20792d891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "cf7e24bc-0d0d-479e-b0aa-e30b4d639136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52f63abc-db2b-41e9-83ae-82f20792d891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb84fc6a-2907-4352-97d2-33e060fd0322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52f63abc-db2b-41e9-83ae-82f20792d891",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "9d3d6077-38b0-4ac5-b157-3eb2523d8cb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "9695620b-3bd6-44cc-8a01-f91b4653d50e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d3d6077-38b0-4ac5-b157-3eb2523d8cb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b20df0b-0b19-459c-99af-07bef1917f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d3d6077-38b0-4ac5-b157-3eb2523d8cb0",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "80633ced-cd03-4f40-ae85-63aa86fc1228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "74943d69-c6d2-4afe-a422-40bbd74cf69e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80633ced-cd03-4f40-ae85-63aa86fc1228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5393a5b-3c96-4ee7-80ef-ef594c312f98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80633ced-cd03-4f40-ae85-63aa86fc1228",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "38638f82-5c1e-48f6-b326-d8ff7f5af396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "9d4f9b2c-a15a-4741-97e5-ddf83a502fdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38638f82-5c1e-48f6-b326-d8ff7f5af396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12ec37a4-6b28-4e69-a377-a978646dd05e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38638f82-5c1e-48f6-b326-d8ff7f5af396",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "9d9c99af-5323-41c1-a14f-0dfa3fa5e505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "497bcd16-d351-496b-9a93-e77201c85fac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d9c99af-5323-41c1-a14f-0dfa3fa5e505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06669e9-20ac-4954-aec4-fac1e654250e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d9c99af-5323-41c1-a14f-0dfa3fa5e505",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "4f5c70b7-f7aa-4928-a75a-8c3656544f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "cd2f6eb5-37ca-44be-b84a-dce14e02fba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f5c70b7-f7aa-4928-a75a-8c3656544f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d5b586-222d-4fbd-a85b-34be871e71a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f5c70b7-f7aa-4928-a75a-8c3656544f43",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "8e1c3f72-0ea0-4fd5-9802-7e782080802c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "42f195b4-ae02-4a70-8f11-a183f35f98c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e1c3f72-0ea0-4fd5-9802-7e782080802c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5426f9f8-d247-475d-bbca-5b9a1dda11cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e1c3f72-0ea0-4fd5-9802-7e782080802c",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "7d581a7d-59e5-4068-840b-3de3083cc699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "f1686614-020c-4db1-ad60-7a3084c0d07f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d581a7d-59e5-4068-840b-3de3083cc699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63f497d-34d6-447b-803e-d4592f89c8ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d581a7d-59e5-4068-840b-3de3083cc699",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "34fffaf4-8377-499c-9228-a74f0978e60b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "33290944-8f84-4ba7-bb93-1f849d94b6e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34fffaf4-8377-499c-9228-a74f0978e60b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caa5693a-f605-4cba-86f5-f854d60a9943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34fffaf4-8377-499c-9228-a74f0978e60b",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "ac648932-ceaf-4c9d-a0f9-19ca022f0d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "29f7ff1a-66d8-4661-8ce8-9e430f892a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac648932-ceaf-4c9d-a0f9-19ca022f0d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd287d4d-4374-4a24-86be-8939bdf8ed6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac648932-ceaf-4c9d-a0f9-19ca022f0d0b",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "af9a7de3-578d-4309-9947-cf7a8c221034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "99b264ff-a210-4823-92b9-273e9bd6334f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af9a7de3-578d-4309-9947-cf7a8c221034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d66523c-6f6f-429b-a694-4d7239a2fb84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af9a7de3-578d-4309-9947-cf7a8c221034",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "09b38fdf-484f-4006-892e-047679155a26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "6aef1673-a1ae-4955-b365-bf12fb11912a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09b38fdf-484f-4006-892e-047679155a26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b1dbb9e-4077-41b7-8f0e-099b1393f925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09b38fdf-484f-4006-892e-047679155a26",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "be4a96df-6bec-41da-b1d5-c3dfb4a12d29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "a870bed9-bc96-42b4-a672-c75eae833a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4a96df-6bec-41da-b1d5-c3dfb4a12d29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "694783c0-c4b1-48c7-88b2-eecf003c7ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4a96df-6bec-41da-b1d5-c3dfb4a12d29",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "0d727ee7-b3de-4d78-8664-5e5da3a19ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "82f2af30-7c38-4b8a-be28-d22d05a27743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d727ee7-b3de-4d78-8664-5e5da3a19ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea79c1e-09a3-4a20-b72d-225aad3b779d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d727ee7-b3de-4d78-8664-5e5da3a19ecd",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "0384d906-dd6c-4e75-a6f4-34fdf173553d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "197ea276-6349-4387-9342-4626e467351b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0384d906-dd6c-4e75-a6f4-34fdf173553d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bef97099-6edb-40b2-8581-4c553b380cf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0384d906-dd6c-4e75-a6f4-34fdf173553d",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        },
        {
            "id": "8d470d0a-e597-43b2-b8d7-0c0e637385e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "compositeImage": {
                "id": "a928fc92-44a3-45c1-b5b6-83046b2524eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d470d0a-e597-43b2-b8d7-0c0e637385e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca512ba-4b98-409f-ae6f-0ff37714fd76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d470d0a-e597-43b2-b8d7-0c0e637385e1",
                    "LayerId": "cbf4b10e-6a74-4cac-b458-9553edb39d16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cbf4b10e-6a74-4cac-b458-9553edb39d16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c01b0d62-a515-42d5-8fdf-44598d8dd67a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}