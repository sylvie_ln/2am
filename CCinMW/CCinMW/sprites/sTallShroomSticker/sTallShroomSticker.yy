{
    "id": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTallShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1393f495-c637-418d-8fce-1334112a378e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
            "compositeImage": {
                "id": "a26caef5-c77f-41e4-8964-f9f86efc6e03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1393f495-c637-418d-8fce-1334112a378e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f6a1301-b86f-4783-ae9b-9f10f9c9d840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1393f495-c637-418d-8fce-1334112a378e",
                    "LayerId": "41353854-0d04-4780-a9a5-a5123613730e"
                }
            ]
        },
        {
            "id": "aff8d6eb-fd18-46f5-87fc-a195ceafa312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
            "compositeImage": {
                "id": "04ed2f0e-03c2-412a-9343-733df36a281f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aff8d6eb-fd18-46f5-87fc-a195ceafa312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b9accb7-120e-45be-bb04-c5dd27740b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aff8d6eb-fd18-46f5-87fc-a195ceafa312",
                    "LayerId": "41353854-0d04-4780-a9a5-a5123613730e"
                }
            ]
        },
        {
            "id": "de996837-ba3b-495b-8edb-ab44f9357d91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
            "compositeImage": {
                "id": "cb802a2a-5fa0-4b0d-a8a6-eada0556b712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de996837-ba3b-495b-8edb-ab44f9357d91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dadd51d-a33d-4617-8150-500ccc7f39bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de996837-ba3b-495b-8edb-ab44f9357d91",
                    "LayerId": "41353854-0d04-4780-a9a5-a5123613730e"
                }
            ]
        },
        {
            "id": "a33e2d7f-eb7c-4110-a455-7193a00c81ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
            "compositeImage": {
                "id": "6d4a98f1-9028-4518-93de-b11ad058714b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33e2d7f-eb7c-4110-a455-7193a00c81ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed10df9-a35b-4d5e-86fd-ceef4d0cec1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33e2d7f-eb7c-4110-a455-7193a00c81ec",
                    "LayerId": "41353854-0d04-4780-a9a5-a5123613730e"
                }
            ]
        },
        {
            "id": "6393db11-0077-48c7-97ff-e6981e459016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
            "compositeImage": {
                "id": "0158e4c7-43d5-4cc4-9016-005078db529e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6393db11-0077-48c7-97ff-e6981e459016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3dbde61-f552-4cbe-a3c4-db20139c1cba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6393db11-0077-48c7-97ff-e6981e459016",
                    "LayerId": "41353854-0d04-4780-a9a5-a5123613730e"
                }
            ]
        },
        {
            "id": "3d9984e8-34cb-44d5-a7b1-332f8fca913f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
            "compositeImage": {
                "id": "2cbe658d-2bad-41cf-b9f9-dd884e1203b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d9984e8-34cb-44d5-a7b1-332f8fca913f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48acd079-41f9-4dc5-a89c-98ce454bf7f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d9984e8-34cb-44d5-a7b1-332f8fca913f",
                    "LayerId": "41353854-0d04-4780-a9a5-a5123613730e"
                }
            ]
        },
        {
            "id": "b65107a4-14d4-4217-a03c-acd2f116f6ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
            "compositeImage": {
                "id": "acbb23ec-a79a-4488-bd11-18c2480c29d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65107a4-14d4-4217-a03c-acd2f116f6ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b31000-3660-445e-a88e-afc7a2e5429a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65107a4-14d4-4217-a03c-acd2f116f6ed",
                    "LayerId": "41353854-0d04-4780-a9a5-a5123613730e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "41353854-0d04-4780-a9a5-a5123613730e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "779773e7-25ed-45aa-a5d8-1fb06ad826cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}