{
    "id": "71c19693-f3bf-46b1-b760-183788407abb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUncomus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd61fa04-00b8-4a1f-ae64-bb987be15756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71c19693-f3bf-46b1-b760-183788407abb",
            "compositeImage": {
                "id": "3d1b9841-52a2-4e7c-887e-68ec53c37953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd61fa04-00b8-4a1f-ae64-bb987be15756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f60e608-89ef-489f-b4b9-9e8eba61877b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd61fa04-00b8-4a1f-ae64-bb987be15756",
                    "LayerId": "c36e3a00-da62-4b01-97cc-de3cffce0707"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c36e3a00-da62-4b01-97cc-de3cffce0707",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71c19693-f3bf-46b1-b760-183788407abb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}