{
    "id": "7835eae5-abc1-419f-8905-9a4bb94552ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEmergencyBubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f4150ef-e7c6-4dc6-80f3-3bce6950ed5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7835eae5-abc1-419f-8905-9a4bb94552ac",
            "compositeImage": {
                "id": "b5ffdf42-9562-46f5-87d2-b1d9b7633269",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4150ef-e7c6-4dc6-80f3-3bce6950ed5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ecfb8c9-c293-4869-b976-802ad68600be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4150ef-e7c6-4dc6-80f3-3bce6950ed5f",
                    "LayerId": "c2c14127-508b-45af-b58d-c613c5d35ddc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c2c14127-508b-45af-b58d-c613c5d35ddc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7835eae5-abc1-419f-8905-9a4bb94552ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}