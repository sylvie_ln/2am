{
    "id": "b56726bd-c1ac-481b-91fb-573383b19803",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBurningFlare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8db8a70c-4c49-46fb-99e5-c709e0831ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b56726bd-c1ac-481b-91fb-573383b19803",
            "compositeImage": {
                "id": "e26b29a7-a264-45f2-b6d0-eca3bff9d04a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db8a70c-4c49-46fb-99e5-c709e0831ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "090c012c-3e3b-47f7-823b-03223e18d944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db8a70c-4c49-46fb-99e5-c709e0831ea9",
                    "LayerId": "af5feb26-5205-4619-a8e7-ff3ad139f77a"
                }
            ]
        },
        {
            "id": "4d0e5fee-dd1e-43c5-a871-8bb2b6ed448d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b56726bd-c1ac-481b-91fb-573383b19803",
            "compositeImage": {
                "id": "6a34ba42-e515-42b2-bd06-8f8892e07b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d0e5fee-dd1e-43c5-a871-8bb2b6ed448d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21c7cea-115c-49f4-8470-3517e5b501d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d0e5fee-dd1e-43c5-a871-8bb2b6ed448d",
                    "LayerId": "af5feb26-5205-4619-a8e7-ff3ad139f77a"
                }
            ]
        },
        {
            "id": "5afc0584-d1c9-4820-8717-f80fe1b9424a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b56726bd-c1ac-481b-91fb-573383b19803",
            "compositeImage": {
                "id": "95336eb1-415d-4521-b497-9c0eeb32daab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5afc0584-d1c9-4820-8717-f80fe1b9424a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705b4f47-e5d3-4220-a1cf-faad8901c14d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5afc0584-d1c9-4820-8717-f80fe1b9424a",
                    "LayerId": "af5feb26-5205-4619-a8e7-ff3ad139f77a"
                }
            ]
        },
        {
            "id": "23c8ec8a-6283-42be-b425-b61f0e28605e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b56726bd-c1ac-481b-91fb-573383b19803",
            "compositeImage": {
                "id": "35e584c1-1162-4110-98ac-4ba3ac898464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23c8ec8a-6283-42be-b425-b61f0e28605e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "216ae988-9b04-4853-8479-25c21e6771ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23c8ec8a-6283-42be-b425-b61f0e28605e",
                    "LayerId": "af5feb26-5205-4619-a8e7-ff3ad139f77a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "af5feb26-5205-4619-a8e7-ff3ad139f77a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b56726bd-c1ac-481b-91fb-573383b19803",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}