{
    "id": "1a492a75-ae81-4cee-8430-54dd8d877b1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpringSneakers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbf51f41-4231-4504-a596-81b3dd8c140a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a492a75-ae81-4cee-8430-54dd8d877b1a",
            "compositeImage": {
                "id": "011f1bca-b93e-48f1-97d1-2ca74c260d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbf51f41-4231-4504-a596-81b3dd8c140a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d89320a6-6962-4858-b78d-b4f63ea4da86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbf51f41-4231-4504-a596-81b3dd8c140a",
                    "LayerId": "8a36174e-ceb1-4a97-b67b-8eed174f3847"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8a36174e-ceb1-4a97-b67b-8eed174f3847",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a492a75-ae81-4cee-8430-54dd8d877b1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}