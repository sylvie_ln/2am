{
    "id": "530c4459-2779-48d0-a98a-4d8b6d2a7207",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHELL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 21,
    "bbox_right": 96,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10293e81-3206-4c69-a5f8-d2826d792a5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "530c4459-2779-48d0-a98a-4d8b6d2a7207",
            "compositeImage": {
                "id": "bd345baf-cec7-43cf-9d09-85e2ebc79710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10293e81-3206-4c69-a5f8-d2826d792a5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbbace8b-91b4-4a4e-882d-2e445a3d4a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10293e81-3206-4c69-a5f8-d2826d792a5e",
                    "LayerId": "436753a6-fc6b-4ec9-8528-872f54ebce8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "436753a6-fc6b-4ec9-8528-872f54ebce8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "530c4459-2779-48d0-a98a-4d8b6d2a7207",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 25
}