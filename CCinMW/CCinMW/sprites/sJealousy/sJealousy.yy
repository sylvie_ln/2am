{
    "id": "6f33b205-7115-4e61-ae52-f99edcec6214",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJealousy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "171f859c-600f-43f0-a672-dce21c9465a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f33b205-7115-4e61-ae52-f99edcec6214",
            "compositeImage": {
                "id": "021eb06d-cb48-40ce-b106-88271af9d35c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "171f859c-600f-43f0-a672-dce21c9465a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c92e2db9-52a8-45ff-9908-bec32877de8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "171f859c-600f-43f0-a672-dce21c9465a7",
                    "LayerId": "5bb3c483-552d-4a1b-a145-e35ab1712de7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5bb3c483-552d-4a1b-a145-e35ab1712de7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f33b205-7115-4e61-ae52-f99edcec6214",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}