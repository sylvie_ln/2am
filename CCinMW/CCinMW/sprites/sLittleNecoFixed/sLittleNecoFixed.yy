{
    "id": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLittleNecoFixed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "906bd0a8-6b79-404c-94f4-b287dd7b69e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "4e375c34-faaa-415d-a860-73385e52bb80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "906bd0a8-6b79-404c-94f4-b287dd7b69e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51efaa21-1b42-442e-bf62-79a946ce13b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "906bd0a8-6b79-404c-94f4-b287dd7b69e6",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "7d55572e-dc1a-4343-a1d3-98e85de7696b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "9baa2d74-24f8-4311-bfff-764920c9c40c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d55572e-dc1a-4343-a1d3-98e85de7696b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8fc9422-ada3-43d2-b48b-79412134373c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d55572e-dc1a-4343-a1d3-98e85de7696b",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "b7ee8745-8967-4510-b036-4bd16db712fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "484cdd54-996a-408f-a80a-a02f34337196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ee8745-8967-4510-b036-4bd16db712fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c71c095c-f86d-4f2b-8909-b02bfe42ec4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ee8745-8967-4510-b036-4bd16db712fc",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "109cf186-6fb0-4dd5-b951-351ecde87292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "045091f5-cf41-4e21-867c-ca02217fccdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109cf186-6fb0-4dd5-b951-351ecde87292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f940ec4-b5d3-4210-a983-ed7f21ccd64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109cf186-6fb0-4dd5-b951-351ecde87292",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "ea4ed79c-8ab9-4062-9d5a-72934d6781ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "bcdcc5da-26cb-4075-98b4-b95a68c9aca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea4ed79c-8ab9-4062-9d5a-72934d6781ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f2f5bc-0124-4641-a58e-0aee21f84e78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea4ed79c-8ab9-4062-9d5a-72934d6781ba",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "e5eaeac6-aee6-4641-ab3c-ee6eab7f2a53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "874aa8d9-ca0e-4fc9-a849-149c77bb2b56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5eaeac6-aee6-4641-ab3c-ee6eab7f2a53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "877c81c0-97d3-427b-a985-1518cc507a91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5eaeac6-aee6-4641-ab3c-ee6eab7f2a53",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "f81eb191-813f-46f7-8385-cd8678654863",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "dbfb134f-385a-42e2-bd10-6ddb056ba435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f81eb191-813f-46f7-8385-cd8678654863",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "783638d4-c14d-4ab7-91ca-40eba51d8aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f81eb191-813f-46f7-8385-cd8678654863",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "b709796b-debb-4241-b5cf-d047692ad16d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "0c1992a7-05c9-48b3-b51c-1e5b96159977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b709796b-debb-4241-b5cf-d047692ad16d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5efcf078-e535-45eb-964a-073be3a0850f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b709796b-debb-4241-b5cf-d047692ad16d",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "b70ee5fc-427f-4dfb-a91f-83d97dcdc3be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "7c9a6d07-1690-4418-b85d-f86f1e8d8633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b70ee5fc-427f-4dfb-a91f-83d97dcdc3be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d0af7d8-3d40-41f0-a40a-883674018dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b70ee5fc-427f-4dfb-a91f-83d97dcdc3be",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "68d7b386-8517-485b-a3ac-196cc15c6f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "49d8c4a4-fca1-4c96-8f98-f70d17a27e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d7b386-8517-485b-a3ac-196cc15c6f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bf1b4d8-c7d2-46cd-b6a7-2b038e35526d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d7b386-8517-485b-a3ac-196cc15c6f5f",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "e5ae2845-96a2-486a-862d-6157685d9af4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "66dba81a-9fec-4f5c-a166-930c09164134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5ae2845-96a2-486a-862d-6157685d9af4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3273b49-8b99-4f47-8116-8d64741d2134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5ae2845-96a2-486a-862d-6157685d9af4",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "64b2603d-18f6-450d-ba8c-d54d8015c98f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "57fd7676-af51-4ecb-99d4-f87c065e9532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64b2603d-18f6-450d-ba8c-d54d8015c98f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1f784f-3113-4787-97b8-e43b6a9790aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64b2603d-18f6-450d-ba8c-d54d8015c98f",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "2f4e20bb-873c-4bce-bcff-2ee79a9559fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "00ab5dbf-6dc1-47aa-8b06-3aa5f3fc0461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f4e20bb-873c-4bce-bcff-2ee79a9559fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c093ac-3ec1-46b7-aa53-ba9450d1325a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f4e20bb-873c-4bce-bcff-2ee79a9559fa",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "0a628a2b-3016-488d-9f7d-db552c2cfd59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "62ce1bf3-76ad-4d74-b8d7-6d891e8ff156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a628a2b-3016-488d-9f7d-db552c2cfd59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c015528-c1f2-4854-a277-ee0d78ee7e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a628a2b-3016-488d-9f7d-db552c2cfd59",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "6d0e4405-43eb-4c6c-8132-400a1407e773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "3bd528d1-31c9-41cc-8506-4802819d1a92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d0e4405-43eb-4c6c-8132-400a1407e773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e61c191-f9b4-4f00-b81f-9ef8b5ec9180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d0e4405-43eb-4c6c-8132-400a1407e773",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "a6b477e0-2716-4b4d-9d68-d9e1ffbab868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "92fe76e5-2b56-4c9c-a300-55646bf4c642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6b477e0-2716-4b4d-9d68-d9e1ffbab868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1f13ecd-5c32-4fcf-86b3-010059919dcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6b477e0-2716-4b4d-9d68-d9e1ffbab868",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "a407755f-29d3-4e2d-b26c-08df583174a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "5a99de7a-6e86-4222-afa6-98021a0c8933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a407755f-29d3-4e2d-b26c-08df583174a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c6db251-8d6e-4107-af5a-35096436a8d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a407755f-29d3-4e2d-b26c-08df583174a0",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "51ed0aaf-c0e4-43e5-b42b-db2f80bffcb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "4017929f-6f6f-48d7-bfc8-eed31ae42d42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ed0aaf-c0e4-43e5-b42b-db2f80bffcb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c046e38-bcf8-4fe0-bc23-3d01203f2132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ed0aaf-c0e4-43e5-b42b-db2f80bffcb9",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "948a23c4-52b1-475a-842a-5a5ad0fe5c0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "9997432e-93f3-4bb1-b48a-0f09682e20fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "948a23c4-52b1-475a-842a-5a5ad0fe5c0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3e79fbe-3eb1-4208-b555-325763c4925d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "948a23c4-52b1-475a-842a-5a5ad0fe5c0d",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "5c407015-abaa-423c-a33b-10d9ce40cc3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "625c7644-79df-4400-9efa-9346b8b1a7bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c407015-abaa-423c-a33b-10d9ce40cc3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a140451d-8241-4506-b31e-5f39fa62add4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c407015-abaa-423c-a33b-10d9ce40cc3c",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "6189e5fa-71d5-47e5-973e-6ed272c73fc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "7a2dc574-decb-4f3f-85e7-fad6ac237d0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6189e5fa-71d5-47e5-973e-6ed272c73fc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a918dd-2663-4888-a9fa-666136d9ef15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6189e5fa-71d5-47e5-973e-6ed272c73fc5",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "0fb8e475-99f9-4243-a2c3-36dcb93ef033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "472c3f9e-624d-4335-b7da-c02c175d1d81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb8e475-99f9-4243-a2c3-36dcb93ef033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba6429f5-d1d1-409a-9f76-ff2ac09789fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb8e475-99f9-4243-a2c3-36dcb93ef033",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "9aa4e4ef-613c-4a97-9af9-1c7ac4c7a986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "b492c26f-0680-4574-a8c2-6432e12622f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa4e4ef-613c-4a97-9af9-1c7ac4c7a986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a91384-1631-457f-92c9-3b246120ce0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa4e4ef-613c-4a97-9af9-1c7ac4c7a986",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "e324bc54-4cce-470a-856b-1f071de20bbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "5ce01cfd-3f82-425a-b973-2137580e2dd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e324bc54-4cce-470a-856b-1f071de20bbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7c11872-fb0a-4419-9b4b-fda4083d77d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e324bc54-4cce-470a-856b-1f071de20bbd",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "4c0943a7-b664-478c-8856-caa965dd7ff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "7b101f3c-1472-4011-8e20-e61d4ce97b52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c0943a7-b664-478c-8856-caa965dd7ff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eabd5b0-2880-4a48-b2a1-b6aba8da8499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c0943a7-b664-478c-8856-caa965dd7ff9",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "dfd3f622-94c5-47bb-9cf5-3d4db0df17da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "0fba1c97-ea53-4e43-b559-43c3a767ff20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfd3f622-94c5-47bb-9cf5-3d4db0df17da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "debbbcf9-73f0-45d5-99bf-76bbe8504c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfd3f622-94c5-47bb-9cf5-3d4db0df17da",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "08adf375-8f83-432f-b527-afb22939d265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "c0b4ad9f-0c5b-4b9d-808b-6d4626495b3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08adf375-8f83-432f-b527-afb22939d265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da750c79-8cee-4730-b5f7-a88f527aa292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08adf375-8f83-432f-b527-afb22939d265",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "d4afd026-c93e-4183-9a99-762f7876905a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "9f9bfc81-dbf7-4bda-96fc-ad31f73b1509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4afd026-c93e-4183-9a99-762f7876905a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8749e7-940a-4d85-a709-b5b33941b806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4afd026-c93e-4183-9a99-762f7876905a",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "ddd586b1-9713-496e-b51c-c3be66c4db42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "c52a525b-4510-4471-9788-6846f322dfca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddd586b1-9713-496e-b51c-c3be66c4db42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e4613cf-b221-4351-8840-87ec01ed7776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddd586b1-9713-496e-b51c-c3be66c4db42",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "ca040286-c397-4dde-b4f6-e6b678178e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "16372a13-a0e1-41b0-9c9a-aed8119544fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca040286-c397-4dde-b4f6-e6b678178e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3580066-4344-45fe-873e-4b48fad1f562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca040286-c397-4dde-b4f6-e6b678178e06",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "4b35980e-567f-4d59-b1ed-2a999a4309d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "d491c6f8-cb5e-4f77-9465-9ca02e5d717d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b35980e-567f-4d59-b1ed-2a999a4309d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4419a224-17a2-4899-8f83-9c36859649f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b35980e-567f-4d59-b1ed-2a999a4309d4",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "c7d0bf80-0492-49d7-86e9-301943d7206b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "dacfe8af-cf90-44a9-851f-51db13758a86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d0bf80-0492-49d7-86e9-301943d7206b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e75763a3-9339-47c6-ad1e-f68040e2ac1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d0bf80-0492-49d7-86e9-301943d7206b",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "fc2d9819-80d7-4e0b-8329-c0568a15bde3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "645dc236-5306-4f50-b13b-d292040902e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2d9819-80d7-4e0b-8329-c0568a15bde3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5856660-5366-4752-ace0-38020d2334bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2d9819-80d7-4e0b-8329-c0568a15bde3",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "09365aa4-c66c-41b2-9ab9-6056168e6380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "ca12e985-e9cf-4f9c-8fde-700c7b8db287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09365aa4-c66c-41b2-9ab9-6056168e6380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4580ff88-043a-4234-a4cc-4dc9d7b18e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09365aa4-c66c-41b2-9ab9-6056168e6380",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "b454f8cf-5804-4845-bf73-66bd871d38aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "358d4bfa-6ea7-4311-9de1-ec6561b8b42c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b454f8cf-5804-4845-bf73-66bd871d38aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7df556e8-a4fc-4c87-885b-274950fbbb6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b454f8cf-5804-4845-bf73-66bd871d38aa",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "17e754d0-1777-4d18-914c-791e9892037e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "34f1bdd8-b66a-4c6f-9275-f6035bca375f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e754d0-1777-4d18-914c-791e9892037e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87cb5441-1ef4-400d-81a4-12ca61664416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e754d0-1777-4d18-914c-791e9892037e",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "dc87ad0d-8a50-46f9-8a66-4372d82fede7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "67808358-e253-4f45-8fbc-209ec1fec206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc87ad0d-8a50-46f9-8a66-4372d82fede7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c84e4289-1928-41c3-8598-7632fa3a64e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc87ad0d-8a50-46f9-8a66-4372d82fede7",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "5c3b8b0c-5a3c-44f3-99fb-bab2b59a990d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "46373989-eb7f-4891-9225-d08bee6a96a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c3b8b0c-5a3c-44f3-99fb-bab2b59a990d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32924a38-1e2e-4211-8a3b-9995d08ead50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c3b8b0c-5a3c-44f3-99fb-bab2b59a990d",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "8f61a7f6-1d3d-4788-88b8-d2c1b47f71e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "7f119455-4fe8-4946-9e83-ff9e5a249ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f61a7f6-1d3d-4788-88b8-d2c1b47f71e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b565eaa-d522-4197-bd85-9b6a82e821fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f61a7f6-1d3d-4788-88b8-d2c1b47f71e5",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "6daceffd-2a0a-4c7a-9d3a-ef8f2efcec22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "42e829ce-4f28-4480-982a-d2b196caf6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6daceffd-2a0a-4c7a-9d3a-ef8f2efcec22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9903783b-bd68-4289-b850-594bf7ebfafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6daceffd-2a0a-4c7a-9d3a-ef8f2efcec22",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "3cc2dda1-a7a3-4b50-9609-79ea30e843de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "204c2763-d0db-4fb2-80f0-92bb2612dd5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc2dda1-a7a3-4b50-9609-79ea30e843de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2981b0e-10ec-47cf-a7d2-9768fbf9bec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc2dda1-a7a3-4b50-9609-79ea30e843de",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "563c33ea-52d1-4803-9009-87efc66d9ff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "ad8aea44-782f-40bb-811a-d56431c89c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "563c33ea-52d1-4803-9009-87efc66d9ff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16b5bf73-9ba0-4dc1-83d7-48aba6a8c97d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "563c33ea-52d1-4803-9009-87efc66d9ff4",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "bf1af062-c530-46cf-821a-2126a33282b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "6f475f15-52a7-4d9d-9c39-146003af0474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf1af062-c530-46cf-821a-2126a33282b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09e39e0-6bfa-4258-9e74-9ce87d2b4176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf1af062-c530-46cf-821a-2126a33282b5",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "e19c44ac-29a6-4d02-bdb8-b83010dcb97d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "9c8cc588-8d5f-4b6c-9341-e611cbca4bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e19c44ac-29a6-4d02-bdb8-b83010dcb97d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c61274-7405-4663-9e8b-19a1faa9a505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e19c44ac-29a6-4d02-bdb8-b83010dcb97d",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "7f2ecab8-da7f-4f42-b492-032f799d745e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "5ed43e54-d0f5-4574-a7d8-e392805f4450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f2ecab8-da7f-4f42-b492-032f799d745e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec2338b-64f2-486a-be60-329683d601fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f2ecab8-da7f-4f42-b492-032f799d745e",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "1780f8a7-c0d3-402f-861d-dec941f5b08d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "71c8dc21-5b43-43b6-a389-edfd35741d29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1780f8a7-c0d3-402f-861d-dec941f5b08d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d823df8b-78c1-450a-b840-28dc8c46fda6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1780f8a7-c0d3-402f-861d-dec941f5b08d",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "0905a03b-4e54-492f-9825-d9cdf4de2d2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "90b0b089-2dce-4d14-8572-9ec095fceb6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0905a03b-4e54-492f-9825-d9cdf4de2d2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54a0069c-542b-4d29-bdf9-d511fecfca02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0905a03b-4e54-492f-9825-d9cdf4de2d2a",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "bf0b9a63-5ffd-4647-8129-74cdf518565b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "3a56c3e5-5659-496e-8b86-1c6651b28354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf0b9a63-5ffd-4647-8129-74cdf518565b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb90e6a-1a9e-4200-8b87-a64a97e027eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf0b9a63-5ffd-4647-8129-74cdf518565b",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "262ceea4-b52d-40e7-b72d-87c3ed746b16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "58111d7d-f04c-4c19-8cc6-f86ad00dbbc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "262ceea4-b52d-40e7-b72d-87c3ed746b16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75dbae13-074d-4455-abc1-34149a0542b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "262ceea4-b52d-40e7-b72d-87c3ed746b16",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "f4dcad0f-ac31-4ef6-8f3d-02378e382e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "bb4f1429-0fe7-4799-95e8-828a004abbd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4dcad0f-ac31-4ef6-8f3d-02378e382e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51756027-4498-4cf2-be40-04aea02c3fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4dcad0f-ac31-4ef6-8f3d-02378e382e69",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "85256ec1-15b6-460c-8939-3934fddb0ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "ce758eb1-1953-4a86-bc9e-9d2559a0d459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85256ec1-15b6-460c-8939-3934fddb0ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d740b76-3649-47e8-9d47-efb14a9ab478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85256ec1-15b6-460c-8939-3934fddb0ef1",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "4bab954b-9edd-4f15-b3bb-5b47ed8fd824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "8de14edb-0752-417e-8c06-191ed24066af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bab954b-9edd-4f15-b3bb-5b47ed8fd824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "091b40cf-8f29-4e05-9fef-b7e109c80f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bab954b-9edd-4f15-b3bb-5b47ed8fd824",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "be26c380-fe0b-4aea-812d-79ca61923cb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "09c1485d-181b-4494-a021-e23448fe3b8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be26c380-fe0b-4aea-812d-79ca61923cb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71575dcc-d1a2-4b9c-9a6e-4fe4d26e4e18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be26c380-fe0b-4aea-812d-79ca61923cb4",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "2f201a5a-eeb2-4e62-9d22-56afb00444a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "4f8528a4-10c5-489d-949d-cb664bde4467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f201a5a-eeb2-4e62-9d22-56afb00444a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff128ae-1272-4b23-a2c1-623b903981e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f201a5a-eeb2-4e62-9d22-56afb00444a1",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "e0f885e7-1d07-4a71-bee1-d1dccb7faef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "7f733f82-1d91-4402-981f-6950e518dc7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0f885e7-1d07-4a71-bee1-d1dccb7faef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "925686fd-cde0-4fc7-97a8-f35483476165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0f885e7-1d07-4a71-bee1-d1dccb7faef4",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "fc535dcd-b2af-4a06-a5f8-be71f5a052e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "3815acc8-bcdb-4409-a576-4f9a663fd0e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc535dcd-b2af-4a06-a5f8-be71f5a052e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "858d313a-9807-4203-a81a-eac772da3080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc535dcd-b2af-4a06-a5f8-be71f5a052e0",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "59903f36-7a88-4a17-a4aa-95988a4e110f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "8a6074ed-6410-4d0f-8c17-603acb1e3737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59903f36-7a88-4a17-a4aa-95988a4e110f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa146e22-cfc6-4635-bb10-fecc7d25083e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59903f36-7a88-4a17-a4aa-95988a4e110f",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "8b6cfb7b-b4fb-42c4-9d71-2f78c30dda8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "584f76fb-cbdb-431b-8bef-3b0c7f57ba14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b6cfb7b-b4fb-42c4-9d71-2f78c30dda8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42162dab-c011-462f-86ca-0296f928e525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b6cfb7b-b4fb-42c4-9d71-2f78c30dda8e",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "2ae7bec1-1a58-4a3b-93d7-62e5f896de2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "375dd447-72d2-40f2-9bec-5972cc0c2f34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae7bec1-1a58-4a3b-93d7-62e5f896de2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "701f1601-446d-4ae9-a5d2-dcfa9a399567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae7bec1-1a58-4a3b-93d7-62e5f896de2b",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "bb4a8449-5959-4b1d-9a28-c9029c3b6480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "5cf3000e-e59c-47d4-b076-2bd702880ddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb4a8449-5959-4b1d-9a28-c9029c3b6480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d6ef2c9-001b-4266-825b-010e89c9512e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb4a8449-5959-4b1d-9a28-c9029c3b6480",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "9533ef61-415d-41a9-ada8-b9cb5e5ee709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "d3adeb5c-c44c-4108-97c5-e34671e6f331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9533ef61-415d-41a9-ada8-b9cb5e5ee709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc1a63be-a23b-4c3e-a361-de44fda01113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9533ef61-415d-41a9-ada8-b9cb5e5ee709",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "f2e97f9b-f3ab-4896-914c-64e6b7a22122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "395f839a-5f22-4fe4-ba64-daab48c7a857",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e97f9b-f3ab-4896-914c-64e6b7a22122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b676774e-ffbd-4e92-b0f1-7449e2c9295d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e97f9b-f3ab-4896-914c-64e6b7a22122",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "074ce5d3-1b18-40d9-9128-92de4d5983b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "1cfe5be9-08eb-425d-90a5-f0f4f31bd8e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074ce5d3-1b18-40d9-9128-92de4d5983b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb8aea3c-5761-4ce4-8696-06b89fdfeead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074ce5d3-1b18-40d9-9128-92de4d5983b0",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "f7f7d375-baa5-4f9c-8639-179b27d05a3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "960ea9e4-ad72-47bf-99f1-d11bc645c3c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f7d375-baa5-4f9c-8639-179b27d05a3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a5778b2-e81f-423e-99b5-a6c95cbde936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f7d375-baa5-4f9c-8639-179b27d05a3a",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "f165d7b5-7dbd-475f-928c-7344b5b8d4bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "6011f6aa-bb8c-4998-9b73-e56a818987a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f165d7b5-7dbd-475f-928c-7344b5b8d4bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08c713a6-8559-4942-8931-46c01993bee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f165d7b5-7dbd-475f-928c-7344b5b8d4bb",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "974aa50b-c063-4feb-938a-282e3c8f0558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "ed4d8f6b-b672-4da6-8a3b-36487b610eda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "974aa50b-c063-4feb-938a-282e3c8f0558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06f31d8d-17e0-4d6c-9b97-dbf932740611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "974aa50b-c063-4feb-938a-282e3c8f0558",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "121bd3e7-a11a-4ebb-845d-c397452b0b1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "2c417a8a-97f9-45cd-8139-95d297ecee21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "121bd3e7-a11a-4ebb-845d-c397452b0b1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ba464c8-abf6-443e-8fb1-2b715cef7006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "121bd3e7-a11a-4ebb-845d-c397452b0b1b",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "b93a835a-542f-4fa7-aa2d-7bec9ad06682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "d4dcf0ca-379d-48aa-8909-31377bb4fc08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b93a835a-542f-4fa7-aa2d-7bec9ad06682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f020be0-89a2-42b3-b5a9-55a80e394c01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b93a835a-542f-4fa7-aa2d-7bec9ad06682",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "a7e3c114-44aa-47c4-916f-6b69b8f63c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "09554c92-c483-49ba-aa7f-20d924ac46e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e3c114-44aa-47c4-916f-6b69b8f63c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4abe9741-51aa-4051-ad37-34b08f45555f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e3c114-44aa-47c4-916f-6b69b8f63c73",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "4c81f5a4-9791-4d37-89fb-c9e4d432a88c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "a4b350fd-c655-4714-817b-6eec03d84d7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c81f5a4-9791-4d37-89fb-c9e4d432a88c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a3a94d-8da9-4826-bfd6-9b8c9f0a1d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c81f5a4-9791-4d37-89fb-c9e4d432a88c",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "354d53f8-29e6-4045-bfc6-0c0f3eb26a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "638ad736-1c94-43a6-a99f-738bf1d4eade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "354d53f8-29e6-4045-bfc6-0c0f3eb26a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2febc58-639d-4d0f-a078-5e13e5555729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "354d53f8-29e6-4045-bfc6-0c0f3eb26a0c",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "55a30b45-b95b-4ce6-98cb-119a459a5732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "c6afeb78-f9b9-41d0-b597-7075ffe8a47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55a30b45-b95b-4ce6-98cb-119a459a5732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab73dc7e-de82-4960-a40a-f4554b44e7c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55a30b45-b95b-4ce6-98cb-119a459a5732",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "11035540-12c7-43f0-94fc-36208e7a748f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "4f5489b2-cd95-471f-89ac-de4a92da6b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11035540-12c7-43f0-94fc-36208e7a748f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54f2b02e-5cbe-4246-a01f-41640d9b60e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11035540-12c7-43f0-94fc-36208e7a748f",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "3c6f3389-86b5-438f-91cb-1fdb1cb3694c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "a593bc4d-3df6-472e-b682-20da05af9bd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c6f3389-86b5-438f-91cb-1fdb1cb3694c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aecc45c0-d221-4156-b352-212f023e6ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c6f3389-86b5-438f-91cb-1fdb1cb3694c",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "33406fed-4d9d-491c-932f-6b05bda2385d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "207dc3f5-3dbe-4d76-b684-700b94acd375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33406fed-4d9d-491c-932f-6b05bda2385d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17621f9f-4b6d-427b-a6a2-2ddfc2948164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33406fed-4d9d-491c-932f-6b05bda2385d",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "865ee8b1-8c63-4df0-9f7c-3a8fac73c1c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "d07aa0ae-0348-4e84-945c-af188e127d7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "865ee8b1-8c63-4df0-9f7c-3a8fac73c1c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fc38906-3ffd-417d-bf1d-7a7458d388c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "865ee8b1-8c63-4df0-9f7c-3a8fac73c1c8",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "06b4a2d4-6fd9-4763-993c-245da6d13a4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "23d48994-eeec-45b2-bc9b-fe3929c481b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06b4a2d4-6fd9-4763-993c-245da6d13a4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bca8138-22c3-4366-88bf-6fb108318547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06b4a2d4-6fd9-4763-993c-245da6d13a4e",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "17c63efb-9c46-4c73-be09-b664bab9dd73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "49582a62-1a7a-4ef7-a087-617aef8f6d5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17c63efb-9c46-4c73-be09-b664bab9dd73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b82dd2c-a080-4ed7-b7d0-e44c32bb117e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17c63efb-9c46-4c73-be09-b664bab9dd73",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "af12f55d-bd85-4b9a-b052-647a5a8afabd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "5a7b3854-a646-40d1-a521-7daddeb36345",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af12f55d-bd85-4b9a-b052-647a5a8afabd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b376a44e-22d8-4d22-bc1d-65c14f6573f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af12f55d-bd85-4b9a-b052-647a5a8afabd",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "6ef8425d-6236-4766-bc39-57224b40e929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "3576993d-298a-4dd4-a5b8-e25dc060d3ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef8425d-6236-4766-bc39-57224b40e929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd8f081-4d64-41d7-96c8-3ea1889ed692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef8425d-6236-4766-bc39-57224b40e929",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "f935d7be-3114-42f0-98cf-4d1c7585b089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "ec13881f-5fbf-402a-9ee3-265b5ab465c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f935d7be-3114-42f0-98cf-4d1c7585b089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c3064ba-7d55-400f-806e-fb163524bff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f935d7be-3114-42f0-98cf-4d1c7585b089",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "67296f35-0ab8-47fd-94db-3958256ae127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "1f490ed7-6090-49da-b5a0-eea1fad437ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67296f35-0ab8-47fd-94db-3958256ae127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25743c0f-a73d-43a7-bca4-51fac7ab3a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67296f35-0ab8-47fd-94db-3958256ae127",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "4f7c1cf1-ed1a-4177-8c16-76ab90f5c578",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "e8847ea9-6b35-4f8d-8180-ef67ca63ddc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7c1cf1-ed1a-4177-8c16-76ab90f5c578",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f242cf0-3887-4656-a9e5-4da5c1bce82f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7c1cf1-ed1a-4177-8c16-76ab90f5c578",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "38bc6634-4238-4302-812f-988535666c9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "575dc40d-b956-4b44-8584-9cae1f09b09f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38bc6634-4238-4302-812f-988535666c9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d50a4d8e-15b8-4487-9f58-51acfb5bb209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38bc6634-4238-4302-812f-988535666c9a",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "f51d759b-63d5-4142-ab0c-a893f6dfcdb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "f5f27e86-c8c9-428c-9aa2-323578067c63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f51d759b-63d5-4142-ab0c-a893f6dfcdb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "420ac493-c557-47b3-9ba7-308dc93f4168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f51d759b-63d5-4142-ab0c-a893f6dfcdb8",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "8ba77fe0-0edd-4596-b260-286f168b481b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "45257506-a1d3-4a8d-9edc-6640e3e93cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba77fe0-0edd-4596-b260-286f168b481b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37cbbdf-9f48-4a31-9275-16225a4833a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba77fe0-0edd-4596-b260-286f168b481b",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "a9af5658-d40e-4236-964e-f3d6a3b3729b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "b278e17e-ad3f-4011-9de3-50c2d67a1408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9af5658-d40e-4236-964e-f3d6a3b3729b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2c19b84-811d-4302-a07a-578d999b8fba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9af5658-d40e-4236-964e-f3d6a3b3729b",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "7d97d308-63e8-4914-8b27-bc616388d3bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "1bec29ab-f6f9-4127-9cab-eca076cb84b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d97d308-63e8-4914-8b27-bc616388d3bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3823c6d2-b1b1-4d4b-a615-179ed2874f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d97d308-63e8-4914-8b27-bc616388d3bb",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "89d87333-6e07-444b-9fee-473a7ff2d67c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "f036a5d0-2c4d-459d-b1b5-f9715bbb78e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d87333-6e07-444b-9fee-473a7ff2d67c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4dc606e-c2be-4e01-ba63-1e21d8ed3b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d87333-6e07-444b-9fee-473a7ff2d67c",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "6dd9bcdf-2f99-4977-be03-477294bd57ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "1eee1280-d81b-4597-ab27-33c0bb134731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd9bcdf-2f99-4977-be03-477294bd57ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e73a09-9780-40ba-987f-39e65bd1ff47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd9bcdf-2f99-4977-be03-477294bd57ed",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "4e4a627c-5bc4-4ba7-a3e1-3a7f7b1fcfe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "a417315c-f49d-48a3-901d-e249aeb03ff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e4a627c-5bc4-4ba7-a3e1-3a7f7b1fcfe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1310dc59-8d15-48f9-b64e-b372351490e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e4a627c-5bc4-4ba7-a3e1-3a7f7b1fcfe0",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "5004c354-2ed1-4171-bc6b-83732481c7e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "93bc3ecb-0f52-4105-bd9c-dc32a9938bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5004c354-2ed1-4171-bc6b-83732481c7e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf829e34-5905-46da-909b-59bcfcaa52c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5004c354-2ed1-4171-bc6b-83732481c7e5",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "1bcc468a-fb40-4ffd-a905-b9fe2c98b1aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "ab1d1da5-9e1f-4ffc-98ab-b2bd5d72dcd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bcc468a-fb40-4ffd-a905-b9fe2c98b1aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43783777-693b-4bd2-a5c1-70535eae6d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bcc468a-fb40-4ffd-a905-b9fe2c98b1aa",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "7fceb4bd-1dad-4751-b2f0-4eeac7956c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "678276a5-5f3f-46f3-8cae-f89a1f64f7bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fceb4bd-1dad-4751-b2f0-4eeac7956c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59251515-e2a2-41ec-a772-f410516a5c1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fceb4bd-1dad-4751-b2f0-4eeac7956c6a",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        },
        {
            "id": "784b4e5b-874a-4e9e-b57d-357ce8b681e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "compositeImage": {
                "id": "4f080404-255a-43a6-b293-7fcf5a6ce8ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "784b4e5b-874a-4e9e-b57d-357ce8b681e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bbe1bb1-a9b8-4593-ad50-7c1789fb1155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "784b4e5b-874a-4e9e-b57d-357ce8b681e3",
                    "LayerId": "4de10294-bf6d-41e9-82ba-c797acbe2db8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4de10294-bf6d-41e9-82ba-c797acbe2db8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11ac8bde-fe41-48ec-a578-c995c7f3bb7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}