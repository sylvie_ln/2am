{
    "id": "3f01817d-5051-4f87-978f-cb4b7530fc0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLoadingAple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 0,
    "bbox_right": 331,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d4fc663-7fb4-4adc-9485-f41abb1b0dcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f01817d-5051-4f87-978f-cb4b7530fc0f",
            "compositeImage": {
                "id": "330822c9-e69a-44a1-a6ae-3d5c3637c791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4fc663-7fb4-4adc-9485-f41abb1b0dcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56acc022-4e6f-428a-a1ff-d2a0d907e9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4fc663-7fb4-4adc-9485-f41abb1b0dcd",
                    "LayerId": "b3acac10-9dc0-49f3-93b2-79230c440675"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "b3acac10-9dc0-49f3-93b2-79230c440675",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f01817d-5051-4f87-978f-cb4b7530fc0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 332,
    "xorig": 132,
    "yorig": 55
}