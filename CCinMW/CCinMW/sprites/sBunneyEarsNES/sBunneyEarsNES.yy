{
    "id": "b6f5f2a3-4efb-43ba-a03d-aa1934d6dfb3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyEarsNES",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f57b841-453e-41c1-b375-a0ff04dcc192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f5f2a3-4efb-43ba-a03d-aa1934d6dfb3",
            "compositeImage": {
                "id": "7ef64222-4ee8-4315-90fb-4e331ac544e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f57b841-453e-41c1-b375-a0ff04dcc192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04251154-402f-4181-aa52-fde781d85904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f57b841-453e-41c1-b375-a0ff04dcc192",
                    "LayerId": "845c1ef1-318e-4ead-8dc5-611a0eb6c468"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "845c1ef1-318e-4ead-8dc5-611a0eb6c468",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6f5f2a3-4efb-43ba-a03d-aa1934d6dfb3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}