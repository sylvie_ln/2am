{
    "id": "f253fe59-bea5-42af-ae42-652390040ced",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTallBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc247e78-f823-4156-ae55-c90cc6d854ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f253fe59-bea5-42af-ae42-652390040ced",
            "compositeImage": {
                "id": "b498d56a-756d-4193-a879-eaae789d074a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc247e78-f823-4156-ae55-c90cc6d854ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b05a66-c3b2-474e-bc5a-27c1cf31f8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc247e78-f823-4156-ae55-c90cc6d854ea",
                    "LayerId": "8c6faca0-b69d-4a6d-851a-808b67d40335"
                },
                {
                    "id": "fdf96094-e5cc-4613-a030-532e56f5e31c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc247e78-f823-4156-ae55-c90cc6d854ea",
                    "LayerId": "6bf76ad3-5175-4a9e-881d-858eff91e188"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "8c6faca0-b69d-4a6d-851a-808b67d40335",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f253fe59-bea5-42af-ae42-652390040ced",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6bf76ad3-5175-4a9e-881d-858eff91e188",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f253fe59-bea5-42af-ae42-652390040ced",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 60
}