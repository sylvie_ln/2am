{
    "id": "f7f036cd-dbfa-4109-bad9-41553dc4909e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMiniBunney",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29cac682-b46c-473d-8cb6-535e671c17a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7f036cd-dbfa-4109-bad9-41553dc4909e",
            "compositeImage": {
                "id": "444e7a55-f550-437c-a212-ac079c939144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29cac682-b46c-473d-8cb6-535e671c17a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "917b3fee-ccd8-43a0-a729-8a4a20457158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29cac682-b46c-473d-8cb6-535e671c17a3",
                    "LayerId": "05ff14a2-0c8f-42dd-865e-68721dc6451c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "05ff14a2-0c8f-42dd-865e-68721dc6451c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7f036cd-dbfa-4109-bad9-41553dc4909e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}