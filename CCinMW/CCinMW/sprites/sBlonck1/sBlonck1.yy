{
    "id": "c71ff400-2e5c-4583-90af-82677a9744aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlonck1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e7fc7fc-8630-47f1-87b0-b6ddbe742d5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c71ff400-2e5c-4583-90af-82677a9744aa",
            "compositeImage": {
                "id": "a4caf84f-908c-49d5-9e24-2d923cd74803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7fc7fc-8630-47f1-87b0-b6ddbe742d5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8d6517-5b64-4361-bba0-79d0e3410205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7fc7fc-8630-47f1-87b0-b6ddbe742d5c",
                    "LayerId": "41fe35c2-3255-4924-a68c-1ca40570204f"
                }
            ]
        },
        {
            "id": "31d60011-b05c-47ac-a1b2-354ca1fe2c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c71ff400-2e5c-4583-90af-82677a9744aa",
            "compositeImage": {
                "id": "8ffa561f-17af-477c-a001-f755c4657a3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d60011-b05c-47ac-a1b2-354ca1fe2c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b906065-9bcb-4ccb-95ab-b7054dfa5abb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d60011-b05c-47ac-a1b2-354ca1fe2c02",
                    "LayerId": "41fe35c2-3255-4924-a68c-1ca40570204f"
                }
            ]
        },
        {
            "id": "d4c64f27-b683-4d7f-b94a-0ac72cadf04f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c71ff400-2e5c-4583-90af-82677a9744aa",
            "compositeImage": {
                "id": "6edf855c-5cb7-4d29-825e-70a3853ce10a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c64f27-b683-4d7f-b94a-0ac72cadf04f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d26a223-012b-4692-9bd3-21d22d91be3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c64f27-b683-4d7f-b94a-0ac72cadf04f",
                    "LayerId": "41fe35c2-3255-4924-a68c-1ca40570204f"
                }
            ]
        },
        {
            "id": "1414b132-39b2-4d50-a08d-c7f2aa404d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c71ff400-2e5c-4583-90af-82677a9744aa",
            "compositeImage": {
                "id": "826ff405-e5cc-4dfb-9cbe-d585678b8b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1414b132-39b2-4d50-a08d-c7f2aa404d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9942d09-db8e-41d7-9439-3fe173820147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1414b132-39b2-4d50-a08d-c7f2aa404d4e",
                    "LayerId": "41fe35c2-3255-4924-a68c-1ca40570204f"
                }
            ]
        },
        {
            "id": "d4390069-3ede-40f3-815b-9ab570b3e032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c71ff400-2e5c-4583-90af-82677a9744aa",
            "compositeImage": {
                "id": "2ee1a25d-e306-4612-af5c-7724a5ce8642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4390069-3ede-40f3-815b-9ab570b3e032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05d69dc3-2319-4a67-8347-8256d0563be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4390069-3ede-40f3-815b-9ab570b3e032",
                    "LayerId": "41fe35c2-3255-4924-a68c-1ca40570204f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "41fe35c2-3255-4924-a68c-1ca40570204f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c71ff400-2e5c-4583-90af-82677a9744aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}