{
    "id": "b1d57156-5bdb-447f-b63d-866eff1bfc22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLady",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5733e8e-1357-4ee3-8bda-bcf6b5c676d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1d57156-5bdb-447f-b63d-866eff1bfc22",
            "compositeImage": {
                "id": "656dca17-0024-4b6a-82ae-6d1b2232c3bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5733e8e-1357-4ee3-8bda-bcf6b5c676d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd70f75-70b0-41f7-9e5a-5ad131f841ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5733e8e-1357-4ee3-8bda-bcf6b5c676d6",
                    "LayerId": "ab972677-dded-41d4-b42c-b0b842e9ae1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ab972677-dded-41d4-b42c-b0b842e9ae1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1d57156-5bdb-447f-b63d-866eff1bfc22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}