{
    "id": "36da6698-a114-4ce3-93f1-e431de663638",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "380542fd-48fc-4466-83f7-0c2595d38325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36da6698-a114-4ce3-93f1-e431de663638",
            "compositeImage": {
                "id": "a18ff663-6e76-423e-b4e7-3a71b773fcb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "380542fd-48fc-4466-83f7-0c2595d38325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ed8a419-d03f-4f4d-bb6e-ca84899d400f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380542fd-48fc-4466-83f7-0c2595d38325",
                    "LayerId": "e2d67720-f81a-45f5-a53b-4209357def77"
                },
                {
                    "id": "a6944f0c-dede-4dec-a6d2-244d4ef27bac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380542fd-48fc-4466-83f7-0c2595d38325",
                    "LayerId": "881e1cca-81ae-46b1-82e3-4fa2061c566a"
                },
                {
                    "id": "e703c0a0-4810-492d-b8cc-5471b4312ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380542fd-48fc-4466-83f7-0c2595d38325",
                    "LayerId": "7e1cf197-46aa-435f-961a-9a84789f71c1"
                },
                {
                    "id": "b24eca6d-8fff-46f1-b9a4-143895b47463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380542fd-48fc-4466-83f7-0c2595d38325",
                    "LayerId": "68df40d9-296a-4d58-810f-1c11186120dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "68df40d9-296a-4d58-810f-1c11186120dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36da6698-a114-4ce3-93f1-e431de663638",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e2d67720-f81a-45f5-a53b-4209357def77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36da6698-a114-4ce3-93f1-e431de663638",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7e1cf197-46aa-435f-961a-9a84789f71c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36da6698-a114-4ce3-93f1-e431de663638",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 25,
            "visible": true
        },
        {
            "id": "881e1cca-81ae-46b1-82e3-4fa2061c566a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36da6698-a114-4ce3-93f1-e431de663638",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}