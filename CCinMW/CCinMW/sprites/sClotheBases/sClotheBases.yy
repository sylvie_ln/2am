{
    "id": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClotheBases",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c582cd9-fdb0-4883-85c7-67d1e698b8c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "compositeImage": {
                "id": "d0720eec-ccd9-450a-a26b-2ad16ab00d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c582cd9-fdb0-4883-85c7-67d1e698b8c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df2cf355-84d0-4eb4-a0d5-f5ec6f65efe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c582cd9-fdb0-4883-85c7-67d1e698b8c3",
                    "LayerId": "e688db49-8f3c-4b03-9f31-7468d15e64cc"
                },
                {
                    "id": "ddfcd2da-c78b-4c6e-89e7-9bb7e1ece4b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c582cd9-fdb0-4883-85c7-67d1e698b8c3",
                    "LayerId": "da50a8db-f382-411d-b545-44f0a5fee5bb"
                }
            ]
        },
        {
            "id": "13527d2b-982e-4715-9617-7c5ec029de85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "compositeImage": {
                "id": "45441986-acb9-4ede-a8b6-5775aa0b50c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13527d2b-982e-4715-9617-7c5ec029de85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8a9fbe-f329-46f3-90ef-c325edfd80fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13527d2b-982e-4715-9617-7c5ec029de85",
                    "LayerId": "e688db49-8f3c-4b03-9f31-7468d15e64cc"
                },
                {
                    "id": "39f10fde-33df-4583-8bd0-79df329238e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13527d2b-982e-4715-9617-7c5ec029de85",
                    "LayerId": "da50a8db-f382-411d-b545-44f0a5fee5bb"
                }
            ]
        },
        {
            "id": "1fde9c2b-a016-4a40-a311-3e7a382f0c06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "compositeImage": {
                "id": "871cae22-b4de-4a6e-abea-fefa6713cc73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fde9c2b-a016-4a40-a311-3e7a382f0c06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc951f73-3bcd-4849-93f1-543f2b6208a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fde9c2b-a016-4a40-a311-3e7a382f0c06",
                    "LayerId": "e688db49-8f3c-4b03-9f31-7468d15e64cc"
                },
                {
                    "id": "22ca2fa3-af8f-4110-8aa2-9fb7c6f8a499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fde9c2b-a016-4a40-a311-3e7a382f0c06",
                    "LayerId": "da50a8db-f382-411d-b545-44f0a5fee5bb"
                }
            ]
        },
        {
            "id": "51ca4824-edb9-4385-816b-d12875611a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "compositeImage": {
                "id": "60877d5d-3b30-4118-819e-459af6186f0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ca4824-edb9-4385-816b-d12875611a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b15b914c-6905-4b85-95a8-b70a6084a357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ca4824-edb9-4385-816b-d12875611a92",
                    "LayerId": "e688db49-8f3c-4b03-9f31-7468d15e64cc"
                },
                {
                    "id": "6f3c3a36-5ca3-4c4d-b3de-0f327d4ce734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ca4824-edb9-4385-816b-d12875611a92",
                    "LayerId": "da50a8db-f382-411d-b545-44f0a5fee5bb"
                }
            ]
        },
        {
            "id": "d117dcf5-836e-492a-9d14-d810becb27e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "compositeImage": {
                "id": "a7bea7ec-5b0e-4358-b701-ed5aac7a0b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d117dcf5-836e-492a-9d14-d810becb27e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f50aac48-c29d-43a2-88c2-89f0490968c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d117dcf5-836e-492a-9d14-d810becb27e6",
                    "LayerId": "e688db49-8f3c-4b03-9f31-7468d15e64cc"
                },
                {
                    "id": "63ce80bc-88a3-460b-a4fd-c1b9572e13e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d117dcf5-836e-492a-9d14-d810becb27e6",
                    "LayerId": "da50a8db-f382-411d-b545-44f0a5fee5bb"
                }
            ]
        },
        {
            "id": "40d7d4f8-8ea1-4570-96ca-b6d3f6fbed42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "compositeImage": {
                "id": "246431e5-09c6-4884-a5c4-84c1ad869925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40d7d4f8-8ea1-4570-96ca-b6d3f6fbed42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cff7b640-1afd-4612-a8bc-23e8e57fbc94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d7d4f8-8ea1-4570-96ca-b6d3f6fbed42",
                    "LayerId": "e688db49-8f3c-4b03-9f31-7468d15e64cc"
                },
                {
                    "id": "8ef6abd4-1384-471a-8bf8-5575085c52f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d7d4f8-8ea1-4570-96ca-b6d3f6fbed42",
                    "LayerId": "da50a8db-f382-411d-b545-44f0a5fee5bb"
                }
            ]
        },
        {
            "id": "4f7b1694-1a68-497e-ba1d-bb832aaa212a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "compositeImage": {
                "id": "fc665bea-2e70-4df2-a406-98e283dadba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7b1694-1a68-497e-ba1d-bb832aaa212a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ffdc1d-d8b7-474e-9bbe-1620f8f2c6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7b1694-1a68-497e-ba1d-bb832aaa212a",
                    "LayerId": "e688db49-8f3c-4b03-9f31-7468d15e64cc"
                },
                {
                    "id": "4c199472-1d78-4a10-aea8-1b06e17a1855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7b1694-1a68-497e-ba1d-bb832aaa212a",
                    "LayerId": "da50a8db-f382-411d-b545-44f0a5fee5bb"
                }
            ]
        },
        {
            "id": "39aa58a1-9427-4ca8-bdfa-fefe4d500cdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "compositeImage": {
                "id": "ff6e600a-c4b8-4d12-a905-fa70d9fa5d85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39aa58a1-9427-4ca8-bdfa-fefe4d500cdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c967ab-0958-46d0-8631-d478b9ed669d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39aa58a1-9427-4ca8-bdfa-fefe4d500cdf",
                    "LayerId": "e688db49-8f3c-4b03-9f31-7468d15e64cc"
                },
                {
                    "id": "06c89eda-1f8b-4be0-b7d8-6ea63db6e461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39aa58a1-9427-4ca8-bdfa-fefe4d500cdf",
                    "LayerId": "da50a8db-f382-411d-b545-44f0a5fee5bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e688db49-8f3c-4b03-9f31-7468d15e64cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "da50a8db-f382-411d-b545-44f0a5fee5bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78f0ba9e-45e3-440c-bed9-8c71d049afb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}