{
    "id": "1f52abc5-e999-414e-909a-a9867f7e74b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLargeBagInterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 7,
    "bbox_right": 90,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4c06b17-2a4f-4a17-b0ba-506e4964bef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f52abc5-e999-414e-909a-a9867f7e74b5",
            "compositeImage": {
                "id": "ab77d5c5-c00d-48bb-9c6c-95b0f67061ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4c06b17-2a4f-4a17-b0ba-506e4964bef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c44ee463-5962-40eb-aab8-c6e2c59b1f76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4c06b17-2a4f-4a17-b0ba-506e4964bef5",
                    "LayerId": "b4997084-814e-4bf9-bfba-543058739852"
                },
                {
                    "id": "9ca73a5e-b8cf-4c3c-9701-915d57c0b337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4c06b17-2a4f-4a17-b0ba-506e4964bef5",
                    "LayerId": "2ac3876d-d59f-440e-b119-fce03d4f045a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b4997084-814e-4bf9-bfba-543058739852",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f52abc5-e999-414e-909a-a9867f7e74b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "2ac3876d-d59f-440e-b119-fce03d4f045a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f52abc5-e999-414e-909a-a9867f7e74b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 60
}