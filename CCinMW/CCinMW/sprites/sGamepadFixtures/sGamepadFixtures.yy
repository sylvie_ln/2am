{
    "id": "ca03554a-4cf1-4fc2-8193-576e916c52a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadFixtures",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 12,
    "bbox_right": 85,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "679d70e2-dd19-4300-ad4f-dc1c1c709290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca03554a-4cf1-4fc2-8193-576e916c52a3",
            "compositeImage": {
                "id": "9ab32af0-8f94-427e-bf06-a6600bbffc3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "679d70e2-dd19-4300-ad4f-dc1c1c709290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8d62cc4-6613-4d2c-b3d1-78c6bf75cae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "679d70e2-dd19-4300-ad4f-dc1c1c709290",
                    "LayerId": "87c29ed3-a70a-482c-9f26-db6eed8a3f21"
                },
                {
                    "id": "084ab813-97e2-45f8-85f9-2e0d9ef576e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "679d70e2-dd19-4300-ad4f-dc1c1c709290",
                    "LayerId": "640a7c90-77cf-49fc-94f1-53c1c5507dbd"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "87c29ed3-a70a-482c-9f26-db6eed8a3f21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca03554a-4cf1-4fc2-8193-576e916c52a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "640a7c90-77cf-49fc-94f1-53c1c5507dbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca03554a-4cf1-4fc2-8193-576e916c52a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}