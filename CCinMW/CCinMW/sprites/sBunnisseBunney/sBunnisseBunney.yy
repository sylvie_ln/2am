{
    "id": "8e82bb2c-a158-454b-96d0-25eb2ee66b85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunnisseBunney",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dbd599b-5203-4239-913f-6035465dee59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e82bb2c-a158-454b-96d0-25eb2ee66b85",
            "compositeImage": {
                "id": "15a4b0b3-3a35-42c8-bad9-2be528a698ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dbd599b-5203-4239-913f-6035465dee59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c61d4b4-3f4b-4dcf-a082-2d895720ab44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dbd599b-5203-4239-913f-6035465dee59",
                    "LayerId": "aecfaf98-5892-40c4-861b-3cfa27eef67b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "aecfaf98-5892-40c4-861b-3cfa27eef67b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e82bb2c-a158-454b-96d0-25eb2ee66b85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}