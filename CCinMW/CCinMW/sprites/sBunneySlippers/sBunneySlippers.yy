{
    "id": "f8afb35c-062f-4fb6-97dd-df3c4dfabd3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneySlippers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e04222f6-2276-40bb-aff1-d9d3aff38756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8afb35c-062f-4fb6-97dd-df3c4dfabd3d",
            "compositeImage": {
                "id": "0671492d-4415-4371-a254-a02af6d1273d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e04222f6-2276-40bb-aff1-d9d3aff38756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "809b45e7-818b-4c77-8922-8acf75d79a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e04222f6-2276-40bb-aff1-d9d3aff38756",
                    "LayerId": "46978d0f-50df-464e-b0bc-553c02d7e042"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "46978d0f-50df-464e-b0bc-553c02d7e042",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8afb35c-062f-4fb6-97dd-df3c4dfabd3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}