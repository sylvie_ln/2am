{
    "id": "1e451e01-7b28-475b-b2af-41327c48a609",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopCoffeeMug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ab1f389-5049-46c8-90ad-11498d66fb48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e451e01-7b28-475b-b2af-41327c48a609",
            "compositeImage": {
                "id": "eba168a0-a692-4cea-bf4f-472a78913c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ab1f389-5049-46c8-90ad-11498d66fb48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3819f819-d8a7-4f22-b922-a9e9cbeba15c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ab1f389-5049-46c8-90ad-11498d66fb48",
                    "LayerId": "8f9bf83c-8325-4beb-940e-145beba9e1ea"
                },
                {
                    "id": "e08a92ef-cb0b-4446-b5b8-45cec38ee4ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ab1f389-5049-46c8-90ad-11498d66fb48",
                    "LayerId": "599f5c0e-458c-45b0-9c06-1aca6661ad43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "599f5c0e-458c-45b0-9c06-1aca6661ad43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e451e01-7b28-475b-b2af-41327c48a609",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "8f9bf83c-8325-4beb-940e-145beba9e1ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e451e01-7b28-475b-b2af-41327c48a609",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}