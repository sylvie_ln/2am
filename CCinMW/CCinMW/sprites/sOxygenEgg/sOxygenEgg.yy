{
    "id": "80b931ed-23f0-4dfb-9ffd-ade5efc708b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOxygenEgg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9556dcc-aaa8-49e7-a70a-e4d737f7501f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80b931ed-23f0-4dfb-9ffd-ade5efc708b3",
            "compositeImage": {
                "id": "7c6fffdc-2a4f-4d4f-848d-4924e0a2e096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9556dcc-aaa8-49e7-a70a-e4d737f7501f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3ea7c8-557f-4489-9457-3611d60c83a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9556dcc-aaa8-49e7-a70a-e4d737f7501f",
                    "LayerId": "a21a246f-c6ec-41e4-9a2f-d3832a30abe2"
                }
            ]
        },
        {
            "id": "d7bae9da-f3ec-4426-b216-f47b420391f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80b931ed-23f0-4dfb-9ffd-ade5efc708b3",
            "compositeImage": {
                "id": "a215b689-beb6-4e85-a69d-dd82b213210c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7bae9da-f3ec-4426-b216-f47b420391f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97225a2c-e034-432d-a16e-9b4681c7a961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7bae9da-f3ec-4426-b216-f47b420391f9",
                    "LayerId": "a21a246f-c6ec-41e4-9a2f-d3832a30abe2"
                }
            ]
        },
        {
            "id": "4f89185f-3888-4c2e-a539-fd8ed4ba5f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80b931ed-23f0-4dfb-9ffd-ade5efc708b3",
            "compositeImage": {
                "id": "51bda150-516d-4f02-99e0-332591bbb1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f89185f-3888-4c2e-a539-fd8ed4ba5f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ce69156-8eed-408e-9e2f-41d343f186a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f89185f-3888-4c2e-a539-fd8ed4ba5f20",
                    "LayerId": "a21a246f-c6ec-41e4-9a2f-d3832a30abe2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a21a246f-c6ec-41e4-9a2f-d3832a30abe2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80b931ed-23f0-4dfb-9ffd-ade5efc708b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}