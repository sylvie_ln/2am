{
    "id": "ed898af8-a287-4ee6-8ad2-bc94b1ab4c1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRaceFinish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fbbd2e6-6c5a-47de-9862-567d6b17e2c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed898af8-a287-4ee6-8ad2-bc94b1ab4c1c",
            "compositeImage": {
                "id": "181db02e-5263-43fe-96e5-2d648ca27bdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fbbd2e6-6c5a-47de-9862-567d6b17e2c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13cda658-f7a4-43dc-9d91-2121bbc43203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fbbd2e6-6c5a-47de-9862-567d6b17e2c1",
                    "LayerId": "4f2f8f2a-a0be-4bc4-b5b9-f16973d90152"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4f2f8f2a-a0be-4bc4-b5b9-f16973d90152",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed898af8-a287-4ee6-8ad2-bc94b1ab4c1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}