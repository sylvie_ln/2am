{
    "id": "68aaea22-6d2d-40f5-9001-210748e61962",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDollarsSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fb6a93a-d209-47f6-b7fc-7bb6d3e33786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68aaea22-6d2d-40f5-9001-210748e61962",
            "compositeImage": {
                "id": "39e65d8b-432e-4087-9318-23426277c28b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb6a93a-d209-47f6-b7fc-7bb6d3e33786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28fb9fe1-0972-4c7a-8b2b-a3878f6ea68f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb6a93a-d209-47f6-b7fc-7bb6d3e33786",
                    "LayerId": "71b9ed79-8e73-49a6-83fc-eac64b0d5f50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "71b9ed79-8e73-49a6-83fc-eac64b0d5f50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68aaea22-6d2d-40f5-9001-210748e61962",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}