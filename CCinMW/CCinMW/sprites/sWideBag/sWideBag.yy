{
    "id": "5eef5d49-be5a-491b-b701-673efa4fed3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWideBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5a6aeff-c875-487d-a0c9-82be1ee01490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eef5d49-be5a-491b-b701-673efa4fed3c",
            "compositeImage": {
                "id": "12e28694-1d57-45e6-a2a4-a1e00ab3e242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5a6aeff-c875-487d-a0c9-82be1ee01490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecb04ac3-93dc-4c5f-bdf4-b381a5bf0488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5a6aeff-c875-487d-a0c9-82be1ee01490",
                    "LayerId": "3256a80d-1dde-4de7-95ee-dcfa1325b2d6"
                },
                {
                    "id": "55646673-360d-427a-b58a-57b9c157f98e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5a6aeff-c875-487d-a0c9-82be1ee01490",
                    "LayerId": "059bada7-e273-40a6-b004-80a609581dfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3256a80d-1dde-4de7-95ee-dcfa1325b2d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5eef5d49-be5a-491b-b701-673efa4fed3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "059bada7-e273-40a6-b004-80a609581dfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5eef5d49-be5a-491b-b701-673efa4fed3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 40
}