{
    "id": "c49b7679-9356-4e70-8516-be378d03d2ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTroubleAndStrife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5adbf450-afcd-4d58-953e-6be1dfd3ce52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c49b7679-9356-4e70-8516-be378d03d2ac",
            "compositeImage": {
                "id": "384cba5d-89dc-4543-bd01-b7b3284a1ead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5adbf450-afcd-4d58-953e-6be1dfd3ce52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36a22990-05be-449f-9515-1761dd5d2914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5adbf450-afcd-4d58-953e-6be1dfd3ce52",
                    "LayerId": "b22f182f-616d-470d-9d55-94df6209692c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b22f182f-616d-470d-9d55-94df6209692c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c49b7679-9356-4e70-8516-be378d03d2ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}