{
    "id": "13db29d6-39fd-4350-b598-8d1391d6d6bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoringRock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25a2cf02-4f6b-4c0b-af4e-047f896573e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13db29d6-39fd-4350-b598-8d1391d6d6bc",
            "compositeImage": {
                "id": "926150d2-be7d-4d5b-b6f3-1d1d13152273",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a2cf02-4f6b-4c0b-af4e-047f896573e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64dd5213-b97a-4b4f-bbab-028e4be749b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a2cf02-4f6b-4c0b-af4e-047f896573e1",
                    "LayerId": "e166032b-7eda-4e12-b707-a293e2449cf3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e166032b-7eda-4e12-b707-a293e2449cf3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13db29d6-39fd-4350-b598-8d1391d6d6bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}