{
    "id": "2cdd4ded-f601-4514-a223-55d1d49d9e49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLamia",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "843151b4-3982-450d-8510-591f54484d73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cdd4ded-f601-4514-a223-55d1d49d9e49",
            "compositeImage": {
                "id": "e47b7e36-33fe-4fa5-83cb-863091ccf8a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843151b4-3982-450d-8510-591f54484d73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7a0b8ca-edde-4b39-b9f7-790556402a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843151b4-3982-450d-8510-591f54484d73",
                    "LayerId": "fb324320-f30e-4fc2-b17b-70d43d03cbea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fb324320-f30e-4fc2-b17b-70d43d03cbea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cdd4ded-f601-4514-a223-55d1d49d9e49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}