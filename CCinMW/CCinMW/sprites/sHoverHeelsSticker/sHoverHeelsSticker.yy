{
    "id": "878b6e55-6356-459c-bd91-e59bb48135b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHoverHeelsSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e45c5cb9-0364-476e-9a38-f19df3a654ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "878b6e55-6356-459c-bd91-e59bb48135b3",
            "compositeImage": {
                "id": "37af2134-a67c-4bac-bac1-86c45a51707c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e45c5cb9-0364-476e-9a38-f19df3a654ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed2a20ae-38a7-4911-95e5-8f4ee246ef58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e45c5cb9-0364-476e-9a38-f19df3a654ab",
                    "LayerId": "a1a48f9a-85ad-40c0-aace-153d563baca5"
                },
                {
                    "id": "26f9d256-7c99-4c31-9478-a621226dfe96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e45c5cb9-0364-476e-9a38-f19df3a654ab",
                    "LayerId": "cfdb19c3-12d8-472c-8d6e-52c484ab2547"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a1a48f9a-85ad-40c0-aace-153d563baca5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "878b6e55-6356-459c-bd91-e59bb48135b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "cfdb19c3-12d8-472c-8d6e-52c484ab2547",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "878b6e55-6356-459c-bd91-e59bb48135b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}