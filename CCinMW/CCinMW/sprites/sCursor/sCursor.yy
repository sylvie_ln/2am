{
    "id": "9ffd656f-ac06-4f90-bd90-24c82f486ae2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63b85f2c-235c-4264-83cb-ac526ae27906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ffd656f-ac06-4f90-bd90-24c82f486ae2",
            "compositeImage": {
                "id": "62359767-9a2c-4244-a171-a22fd9440043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b85f2c-235c-4264-83cb-ac526ae27906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13edb19f-e287-4b3e-9d98-b24e7b3ce375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b85f2c-235c-4264-83cb-ac526ae27906",
                    "LayerId": "c3a775d7-18ad-4129-a083-dbbd82c8ccb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c3a775d7-18ad-4129-a083-dbbd82c8ccb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ffd656f-ac06-4f90-bd90-24c82f486ae2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}