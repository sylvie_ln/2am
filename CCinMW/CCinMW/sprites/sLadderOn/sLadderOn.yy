{
    "id": "05a7f084-b848-4249-8540-b573d3c97fa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLadderOn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9dfd383f-9f47-4e52-930f-578a714d9ec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05a7f084-b848-4249-8540-b573d3c97fa8",
            "compositeImage": {
                "id": "b59ddae6-f671-4d69-bb08-5f1db5448de2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dfd383f-9f47-4e52-930f-578a714d9ec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ba5ee9-8cd6-4671-80fb-212b6120b792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dfd383f-9f47-4e52-930f-578a714d9ec9",
                    "LayerId": "7dcf8a98-3031-4bdc-ac08-416ec94a2e61"
                }
            ]
        },
        {
            "id": "2d3718ca-6ad8-4183-b974-672e43e15a6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05a7f084-b848-4249-8540-b573d3c97fa8",
            "compositeImage": {
                "id": "ccc9c12a-4cdf-43c8-9842-efffb85f2712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d3718ca-6ad8-4183-b974-672e43e15a6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c703c8-e2de-48c3-ae48-cadc91a59bb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d3718ca-6ad8-4183-b974-672e43e15a6b",
                    "LayerId": "7dcf8a98-3031-4bdc-ac08-416ec94a2e61"
                }
            ]
        },
        {
            "id": "e41f0965-65c7-4b77-bbdf-6aad1361e6d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05a7f084-b848-4249-8540-b573d3c97fa8",
            "compositeImage": {
                "id": "b1c3e594-9eae-4312-a015-635dcb8202c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e41f0965-65c7-4b77-bbdf-6aad1361e6d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1436552f-e675-4279-bfeb-0bd8057efa7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e41f0965-65c7-4b77-bbdf-6aad1361e6d8",
                    "LayerId": "7dcf8a98-3031-4bdc-ac08-416ec94a2e61"
                }
            ]
        },
        {
            "id": "e267d111-4243-4e6d-8ff8-f7f9db66e9c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05a7f084-b848-4249-8540-b573d3c97fa8",
            "compositeImage": {
                "id": "e23fc407-800e-4f45-ac2b-dc74f3506a2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e267d111-4243-4e6d-8ff8-f7f9db66e9c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c4230b-7693-491c-833d-8be4eed8e188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e267d111-4243-4e6d-8ff8-f7f9db66e9c4",
                    "LayerId": "7dcf8a98-3031-4bdc-ac08-416ec94a2e61"
                }
            ]
        },
        {
            "id": "9f125143-7531-4c27-8655-5079e37ef608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05a7f084-b848-4249-8540-b573d3c97fa8",
            "compositeImage": {
                "id": "9aabf89c-7732-434a-bed9-302d1a68b8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f125143-7531-4c27-8655-5079e37ef608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25fadef0-beba-4b1f-95e5-7d296c6068f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f125143-7531-4c27-8655-5079e37ef608",
                    "LayerId": "7dcf8a98-3031-4bdc-ac08-416ec94a2e61"
                }
            ]
        },
        {
            "id": "51b47f44-b40c-45d1-9625-f76e2440ef6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05a7f084-b848-4249-8540-b573d3c97fa8",
            "compositeImage": {
                "id": "2b54956f-2a72-47ae-b336-bd7bf95c26e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51b47f44-b40c-45d1-9625-f76e2440ef6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f72a321-0eda-4211-89b7-afae090ca22a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51b47f44-b40c-45d1-9625-f76e2440ef6d",
                    "LayerId": "7dcf8a98-3031-4bdc-ac08-416ec94a2e61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7dcf8a98-3031-4bdc-ac08-416ec94a2e61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05a7f084-b848-4249-8540-b573d3c97fa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4294948864,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}