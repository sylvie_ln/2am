{
    "id": "2e5274a4-d1ab-4046-9e71-f7779059e571",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sArchaea",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5da97d9d-3bd2-4505-996c-de805a0514fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e5274a4-d1ab-4046-9e71-f7779059e571",
            "compositeImage": {
                "id": "0d59228f-d3e8-4510-b922-108ae4be984d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5da97d9d-3bd2-4505-996c-de805a0514fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c2e661e-62e1-4fd8-bea8-dbf625e19ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5da97d9d-3bd2-4505-996c-de805a0514fc",
                    "LayerId": "f0a8dfa9-8bf1-4b12-bc99-6617c3d5fc8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f0a8dfa9-8bf1-4b12-bc99-6617c3d5fc8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e5274a4-d1ab-4046-9e71-f7779059e571",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}