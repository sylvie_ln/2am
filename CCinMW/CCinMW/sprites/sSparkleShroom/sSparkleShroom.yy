{
    "id": "de6ced25-9c1f-462f-87d8-1dd53d5c9773",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSparkleShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99ff24fb-1727-4b3e-bf77-e7aeba0c7830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de6ced25-9c1f-462f-87d8-1dd53d5c9773",
            "compositeImage": {
                "id": "909a4e41-e85d-468c-bf82-9ebd038ba5cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ff24fb-1727-4b3e-bf77-e7aeba0c7830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a67b813c-3b89-41f9-9343-c1664018beba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ff24fb-1727-4b3e-bf77-e7aeba0c7830",
                    "LayerId": "dd68f7b2-1b24-4116-a415-753cf9a3fd73"
                },
                {
                    "id": "2e96e424-fc8b-4825-99d1-2321409b63f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ff24fb-1727-4b3e-bf77-e7aeba0c7830",
                    "LayerId": "00c28034-76da-4866-89eb-c34f1ff8ea4f"
                },
                {
                    "id": "1a010ae5-9a5e-4c5a-8160-c22fa0a344af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ff24fb-1727-4b3e-bf77-e7aeba0c7830",
                    "LayerId": "6b6d7171-66fa-4d3a-aa88-3cb806a85b57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "00c28034-76da-4866-89eb-c34f1ff8ea4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de6ced25-9c1f-462f-87d8-1dd53d5c9773",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6b6d7171-66fa-4d3a-aa88-3cb806a85b57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de6ced25-9c1f-462f-87d8-1dd53d5c9773",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "dd68f7b2-1b24-4116-a415-753cf9a3fd73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de6ced25-9c1f-462f-87d8-1dd53d5c9773",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}