{
    "id": "41cae0b4-a6f2-4256-9fe8-bc1761cf6ca2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHorse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "edf8e5f2-fc56-461c-808c-a9fa99396d1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41cae0b4-a6f2-4256-9fe8-bc1761cf6ca2",
            "compositeImage": {
                "id": "13604711-dd92-4c4c-b6a3-1515eb66c519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf8e5f2-fc56-461c-808c-a9fa99396d1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4201c80-0423-4ae9-9452-c0c09b9a8f49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf8e5f2-fc56-461c-808c-a9fa99396d1d",
                    "LayerId": "23936aeb-d129-447a-964b-0f8f73f29155"
                },
                {
                    "id": "eaa16014-d2f3-49c7-9f2c-cec03df0bfac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf8e5f2-fc56-461c-808c-a9fa99396d1d",
                    "LayerId": "734a875d-59d8-4a98-8973-3ce52a2e2ace"
                },
                {
                    "id": "611be977-fcf6-4b4f-adcc-c33ca64ae161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf8e5f2-fc56-461c-808c-a9fa99396d1d",
                    "LayerId": "ec704967-3931-47b5-8525-7881ffa308a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ec704967-3931-47b5-8525-7881ffa308a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41cae0b4-a6f2-4256-9fe8-bc1761cf6ca2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "23936aeb-d129-447a-964b-0f8f73f29155",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41cae0b4-a6f2-4256-9fe8-bc1761cf6ca2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "734a875d-59d8-4a98-8973-3ce52a2e2ace",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41cae0b4-a6f2-4256-9fe8-bc1761cf6ca2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}