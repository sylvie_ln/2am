{
    "id": "76b91c71-8525-4f21-a0e6-b0733a625c47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNPCBagInterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 18,
    "bbox_right": 110,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb6f5402-2cf1-4694-902f-6ac810e5404a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76b91c71-8525-4f21-a0e6-b0733a625c47",
            "compositeImage": {
                "id": "99fb6240-4649-42e3-a5a1-e69b3ccf643f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6f5402-2cf1-4694-902f-6ac810e5404a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01fffba2-febc-46ba-bf1c-dbd094a1cf19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6f5402-2cf1-4694-902f-6ac810e5404a",
                    "LayerId": "7f253757-a5b5-4ab4-abd2-0f8f2d8b7445"
                },
                {
                    "id": "63e40424-61e8-439d-928d-4746800ff594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6f5402-2cf1-4694-902f-6ac810e5404a",
                    "LayerId": "d9d36c6c-ff9c-4e33-96c8-5923c58dba22"
                },
                {
                    "id": "910031d7-11f1-45ef-8ce0-5cbb5f3909a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6f5402-2cf1-4694-902f-6ac810e5404a",
                    "LayerId": "9fffdb0f-1472-4895-96de-5b6931ea3877"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7f253757-a5b5-4ab4-abd2-0f8f2d8b7445",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76b91c71-8525-4f21-a0e6-b0733a625c47",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "d9d36c6c-ff9c-4e33-96c8-5923c58dba22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76b91c71-8525-4f21-a0e6-b0733a625c47",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9fffdb0f-1472-4895-96de-5b6931ea3877",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76b91c71-8525-4f21-a0e6-b0733a625c47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}