{
    "id": "498ddd90-557c-40db-a406-afbfe95e4f5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTinyShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c8e7bed-b5af-427c-88af-503ba9befbb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498ddd90-557c-40db-a406-afbfe95e4f5b",
            "compositeImage": {
                "id": "1936f653-fa9b-45d7-9669-0c50c10e630e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c8e7bed-b5af-427c-88af-503ba9befbb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06ab2aa0-0d4d-4b63-aa15-f6bb82873ec5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8e7bed-b5af-427c-88af-503ba9befbb6",
                    "LayerId": "c0268ac6-2371-4c04-95a1-d0f4b67c148d"
                }
            ]
        },
        {
            "id": "174045ec-b713-4e06-9ffc-45ddc2430af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498ddd90-557c-40db-a406-afbfe95e4f5b",
            "compositeImage": {
                "id": "3061291b-4965-4721-af56-0f22e93403d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "174045ec-b713-4e06-9ffc-45ddc2430af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536e38d7-fbd1-4919-bf58-db6a786d4919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "174045ec-b713-4e06-9ffc-45ddc2430af7",
                    "LayerId": "c0268ac6-2371-4c04-95a1-d0f4b67c148d"
                }
            ]
        },
        {
            "id": "fa399580-286b-49c0-b01c-2cd345c45eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498ddd90-557c-40db-a406-afbfe95e4f5b",
            "compositeImage": {
                "id": "5e8d8a92-cf7a-45b3-944a-0055d22d775e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa399580-286b-49c0-b01c-2cd345c45eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3adda9c5-8270-46c8-ad30-9f345ef0d185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa399580-286b-49c0-b01c-2cd345c45eb3",
                    "LayerId": "c0268ac6-2371-4c04-95a1-d0f4b67c148d"
                }
            ]
        },
        {
            "id": "a2c0592f-3755-4980-bb99-9256ccff3b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498ddd90-557c-40db-a406-afbfe95e4f5b",
            "compositeImage": {
                "id": "47dbe3e3-6f2a-4b96-b3d9-00538dd1554d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2c0592f-3755-4980-bb99-9256ccff3b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f858d850-79f0-4b58-934d-7e76d57c410a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2c0592f-3755-4980-bb99-9256ccff3b8b",
                    "LayerId": "c0268ac6-2371-4c04-95a1-d0f4b67c148d"
                }
            ]
        },
        {
            "id": "f3f24199-2185-47b1-8103-74d170f1ca21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498ddd90-557c-40db-a406-afbfe95e4f5b",
            "compositeImage": {
                "id": "e4dfb04c-fe01-47d1-b696-18d6c1524c8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f24199-2185-47b1-8103-74d170f1ca21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d23eda-83a9-4b53-9679-8586da79d74b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f24199-2185-47b1-8103-74d170f1ca21",
                    "LayerId": "c0268ac6-2371-4c04-95a1-d0f4b67c148d"
                }
            ]
        },
        {
            "id": "e965c06f-c173-4a2e-a3d0-6f17ee017df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498ddd90-557c-40db-a406-afbfe95e4f5b",
            "compositeImage": {
                "id": "bbd68c2d-1cb8-4d6a-accb-dbdda23d5bec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e965c06f-c173-4a2e-a3d0-6f17ee017df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72211189-eff8-445d-945c-e2b35197b32d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e965c06f-c173-4a2e-a3d0-6f17ee017df7",
                    "LayerId": "c0268ac6-2371-4c04-95a1-d0f4b67c148d"
                }
            ]
        },
        {
            "id": "9e8569cb-472e-47af-8b44-907f39c9d9e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498ddd90-557c-40db-a406-afbfe95e4f5b",
            "compositeImage": {
                "id": "85ae20bd-616b-41e5-9878-d0bac218fbf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e8569cb-472e-47af-8b44-907f39c9d9e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a001689-e877-45e0-a589-4c6fbec8c73d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e8569cb-472e-47af-8b44-907f39c9d9e4",
                    "LayerId": "c0268ac6-2371-4c04-95a1-d0f4b67c148d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c0268ac6-2371-4c04-95a1-d0f4b67c148d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "498ddd90-557c-40db-a406-afbfe95e4f5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}