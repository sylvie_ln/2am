{
    "id": "649bcc84-5d10-4afa-aca6-53e67bc72b32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadRBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 68,
    "bbox_right": 76,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b163b94-e523-434f-bd57-e6d7d1f3c0d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "649bcc84-5d10-4afa-aca6-53e67bc72b32",
            "compositeImage": {
                "id": "5f780a23-2a00-4e37-a4a4-8b207af2e72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b163b94-e523-434f-bd57-e6d7d1f3c0d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "722b5397-24a8-4444-b39e-4da220dc695a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b163b94-e523-434f-bd57-e6d7d1f3c0d5",
                    "LayerId": "8d91dc72-ee61-4f97-ad51-df1d5bab2f95"
                },
                {
                    "id": "920d590b-f810-48e7-bfcf-e53a48abcfc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b163b94-e523-434f-bd57-e6d7d1f3c0d5",
                    "LayerId": "516e9aa9-289a-4580-b6e7-1d65cb1268d7"
                }
            ]
        },
        {
            "id": "a30d4472-904e-4ae8-a90e-0942763ba652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "649bcc84-5d10-4afa-aca6-53e67bc72b32",
            "compositeImage": {
                "id": "a4a809d6-75c8-451c-9c8c-b6157ba6c694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a30d4472-904e-4ae8-a90e-0942763ba652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa5c265-59ca-41a7-a920-1ae399e0ea06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30d4472-904e-4ae8-a90e-0942763ba652",
                    "LayerId": "8d91dc72-ee61-4f97-ad51-df1d5bab2f95"
                },
                {
                    "id": "74d101eb-77d9-43fb-8e55-edfa3d1f15ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30d4472-904e-4ae8-a90e-0942763ba652",
                    "LayerId": "516e9aa9-289a-4580-b6e7-1d65cb1268d7"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "8d91dc72-ee61-4f97-ad51-df1d5bab2f95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "649bcc84-5d10-4afa-aca6-53e67bc72b32",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "516e9aa9-289a-4580-b6e7-1d65cb1268d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "649bcc84-5d10-4afa-aca6-53e67bc72b32",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}