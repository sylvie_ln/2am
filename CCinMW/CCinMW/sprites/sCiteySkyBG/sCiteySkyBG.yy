{
    "id": "dcbf4866-0dad-47b4-bbe0-5551163d8fbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCiteySkyBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24433667-0084-48f2-9cc7-45ca1423c5c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcbf4866-0dad-47b4-bbe0-5551163d8fbc",
            "compositeImage": {
                "id": "c1e4a857-9d14-4e2a-8904-436187f5f308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24433667-0084-48f2-9cc7-45ca1423c5c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bdc7831-e92b-43f9-b1df-9523ccd9d404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24433667-0084-48f2-9cc7-45ca1423c5c4",
                    "LayerId": "ba3da248-3792-42c5-9037-8e1289d46f9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "ba3da248-3792-42c5-9037-8e1289d46f9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcbf4866-0dad-47b4-bbe0-5551163d8fbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}