{
    "id": "ffda9d50-04aa-4914-99b0-5c3d68e51f04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGeniuxBunney",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e394cd5-9f51-431e-a958-23b6163bedac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffda9d50-04aa-4914-99b0-5c3d68e51f04",
            "compositeImage": {
                "id": "c4a7e91a-8a64-487a-a012-1493590b6246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e394cd5-9f51-431e-a958-23b6163bedac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6d4108-4d07-432f-931b-2e58ea7c94a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e394cd5-9f51-431e-a958-23b6163bedac",
                    "LayerId": "b54230d3-793a-4c8b-a3c8-f7b81fa7c938"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b54230d3-793a-4c8b-a3c8-f7b81fa7c938",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffda9d50-04aa-4914-99b0-5c3d68e51f04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}