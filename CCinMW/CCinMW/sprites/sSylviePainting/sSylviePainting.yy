{
    "id": "9be33ae6-6f08-4702-a804-7efa7072e18d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSylviePainting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83cc42f5-9a8e-4ba3-93c3-30fb69848c93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9be33ae6-6f08-4702-a804-7efa7072e18d",
            "compositeImage": {
                "id": "6f319104-99c5-42cb-a3e0-26e76c2f6c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83cc42f5-9a8e-4ba3-93c3-30fb69848c93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162652e7-c70e-4a75-959f-fadd2d0fdc27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83cc42f5-9a8e-4ba3-93c3-30fb69848c93",
                    "LayerId": "f9d72314-1db5-4eab-87be-c70a6d635d91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "f9d72314-1db5-4eab-87be-c70a6d635d91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9be33ae6-6f08-4702-a804-7efa7072e18d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}