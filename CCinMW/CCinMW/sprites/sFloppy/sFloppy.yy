{
    "id": "a5900f4f-0255-481f-821f-935f466519ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFloppy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c0ee859-44d5-4e09-9e8d-72411ab097f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "2b19c8fe-3475-48d2-8cff-0a03162e60fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c0ee859-44d5-4e09-9e8d-72411ab097f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea52bb1a-b68b-4021-a6bc-1ee05b1790f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c0ee859-44d5-4e09-9e8d-72411ab097f4",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "f17c1986-eb67-4ae2-961b-2409a96b647f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "81cecbe7-bef8-48ef-a19d-43ff6f1bce07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f17c1986-eb67-4ae2-961b-2409a96b647f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b09dbbed-662e-4a73-bd43-6105f3ec0a06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f17c1986-eb67-4ae2-961b-2409a96b647f",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "25807af2-0034-479c-b820-d519ab15a614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "c4bb8843-fb93-4988-af3c-f6f66132fba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25807af2-0034-479c-b820-d519ab15a614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "846bb7bd-941e-4d06-ba4c-69f659cd1ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25807af2-0034-479c-b820-d519ab15a614",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "a174c868-2d7a-4563-95b3-0db6adb57d09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "82301e4e-e2e2-446d-9c0e-e8bbe1422c92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a174c868-2d7a-4563-95b3-0db6adb57d09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65f19c84-404c-4aed-91ea-ad06e45c556c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a174c868-2d7a-4563-95b3-0db6adb57d09",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "8b48592d-4e4b-4526-8291-ed320421b860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "adbcc6ae-ed23-4507-b11b-fa9d9a20e94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b48592d-4e4b-4526-8291-ed320421b860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c728cd13-ed69-4137-970e-78ab4d83608b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b48592d-4e4b-4526-8291-ed320421b860",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "369748fe-21f3-4a53-8982-e06146fef3af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "8f5ef564-e858-42cc-b63c-4130cf303c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "369748fe-21f3-4a53-8982-e06146fef3af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444452e9-079d-40e6-8b1f-59b893146856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "369748fe-21f3-4a53-8982-e06146fef3af",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "9a1a1aa1-5911-41db-ab25-92461442199e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "85f30416-9d33-4b59-b2a2-95d0233e95b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a1a1aa1-5911-41db-ab25-92461442199e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79f5393-075d-42b4-87bf-c49c17aceddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a1a1aa1-5911-41db-ab25-92461442199e",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "0d7fe2b2-2080-481d-9546-7d5a57b88a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "48f64eb0-7c7d-4bbf-a2d0-dbfccdb2e16a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d7fe2b2-2080-481d-9546-7d5a57b88a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5491ffef-ccaf-4321-abed-e199b6e9c91c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d7fe2b2-2080-481d-9546-7d5a57b88a93",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "cd01826c-4e17-406c-a68f-06ebd41191d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "4caa460f-5ff7-461f-97b4-beff08a9f988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd01826c-4e17-406c-a68f-06ebd41191d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b4f4bf6-496d-429f-85ed-d8ea36e27157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd01826c-4e17-406c-a68f-06ebd41191d0",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        },
        {
            "id": "9d6639f2-2323-4015-9f3d-e5860d58465d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "compositeImage": {
                "id": "c70a2599-6c0e-429b-8b2f-74d68681de28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d6639f2-2323-4015-9f3d-e5860d58465d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c22e36a8-b08a-4794-a1e1-afbbbfa76c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d6639f2-2323-4015-9f3d-e5860d58465d",
                    "LayerId": "88537cba-d66d-4999-a326-62ba85a3faa3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "88537cba-d66d-4999-a326-62ba85a3faa3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5900f4f-0255-481f-821f-935f466519ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}