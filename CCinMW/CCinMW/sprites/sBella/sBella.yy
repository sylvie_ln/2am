{
    "id": "6413cbed-1426-4aa6-8cfc-6920000e7e53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBella",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d0075f3-3cd2-4d71-82b6-21b9754a3143",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6413cbed-1426-4aa6-8cfc-6920000e7e53",
            "compositeImage": {
                "id": "54397b1f-ed6b-4069-bbd1-e40b3092c2d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0075f3-3cd2-4d71-82b6-21b9754a3143",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c1d52b-d4dc-42f3-b309-44a448d88164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0075f3-3cd2-4d71-82b6-21b9754a3143",
                    "LayerId": "ac9063ee-f8af-42b9-bcb1-63d1b7834288"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ac9063ee-f8af-42b9-bcb1-63d1b7834288",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6413cbed-1426-4aa6-8cfc-6920000e7e53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}