{
    "id": "55fbfcf3-24f3-4c65-ab9a-189d45c29148",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnakeHitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51a36094-38e6-4f63-9c99-553edea18b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55fbfcf3-24f3-4c65-ab9a-189d45c29148",
            "compositeImage": {
                "id": "01e1e251-6a07-4d4f-97b1-f2d38231da96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a36094-38e6-4f63-9c99-553edea18b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "473d3b48-850a-4df4-848a-f0197986109d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a36094-38e6-4f63-9c99-553edea18b0d",
                    "LayerId": "2ab8a249-63e2-4da1-8e8f-b81258c56882"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2ab8a249-63e2-4da1-8e8f-b81258c56882",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55fbfcf3-24f3-4c65-ab9a-189d45c29148",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}