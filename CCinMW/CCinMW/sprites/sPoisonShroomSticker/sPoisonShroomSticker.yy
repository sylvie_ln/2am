{
    "id": "04b5b24d-9600-469d-b07d-6aa4f27188c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPoisonShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "512f3b25-6ca6-473c-8aa5-c1da299c9d7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04b5b24d-9600-469d-b07d-6aa4f27188c2",
            "compositeImage": {
                "id": "477d07d5-767a-4b75-bb37-72471f1b2aa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "512f3b25-6ca6-473c-8aa5-c1da299c9d7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7143492-7338-4e53-8316-ae2b78c9ea87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "512f3b25-6ca6-473c-8aa5-c1da299c9d7c",
                    "LayerId": "a61c6af7-54fe-49e5-b0e9-d352801e541b"
                },
                {
                    "id": "6680b922-6522-43db-bfc5-8978229cb6cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "512f3b25-6ca6-473c-8aa5-c1da299c9d7c",
                    "LayerId": "f6ccfe5e-f3ed-49aa-9bd7-c4a1b387a8ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a61c6af7-54fe-49e5-b0e9-d352801e541b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04b5b24d-9600-469d-b07d-6aa4f27188c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f6ccfe5e-f3ed-49aa-9bd7-c4a1b387a8ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04b5b24d-9600-469d-b07d-6aa4f27188c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}