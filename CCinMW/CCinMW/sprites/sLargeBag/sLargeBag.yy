{
    "id": "9abb4e4f-5178-4f6e-9f19-9d262c63502c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLargeBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "484eb97a-e824-4bfc-bfe1-824f5e9f700b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9abb4e4f-5178-4f6e-9f19-9d262c63502c",
            "compositeImage": {
                "id": "e482b880-34dd-4c99-9df5-5610a3a998bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "484eb97a-e824-4bfc-bfe1-824f5e9f700b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f5e1b1-a322-46d5-b205-cd8e549079a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "484eb97a-e824-4bfc-bfe1-824f5e9f700b",
                    "LayerId": "a91bc04b-0f42-4d19-8281-85d074ebf672"
                },
                {
                    "id": "c24c143c-35e0-4b78-81cc-ad57db6d0759",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "484eb97a-e824-4bfc-bfe1-824f5e9f700b",
                    "LayerId": "68494c44-0cf5-40d4-a99d-6ff76e77d1c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "a91bc04b-0f42-4d19-8281-85d074ebf672",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9abb4e4f-5178-4f6e-9f19-9d262c63502c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "68494c44-0cf5-40d4-a99d-6ff76e77d1c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9abb4e4f-5178-4f6e-9f19-9d262c63502c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 60
}