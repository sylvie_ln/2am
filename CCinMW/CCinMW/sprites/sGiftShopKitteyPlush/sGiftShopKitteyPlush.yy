{
    "id": "0c3fc0c0-dcbf-4ad6-8ac4-855112e15d02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopKitteyPlush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21c55d70-4a49-482c-8061-d11c3746d1a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c3fc0c0-dcbf-4ad6-8ac4-855112e15d02",
            "compositeImage": {
                "id": "a9fab690-1692-45d9-8c81-e11687c854a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21c55d70-4a49-482c-8061-d11c3746d1a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8de54158-ebb2-40d9-9af3-970f5872c83f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21c55d70-4a49-482c-8061-d11c3746d1a1",
                    "LayerId": "0a7085e7-8cb6-4245-9f3a-e1eceda7c4c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0a7085e7-8cb6-4245-9f3a-e1eceda7c4c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c3fc0c0-dcbf-4ad6-8ac4-855112e15d02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}