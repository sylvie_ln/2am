{
    "id": "07be824e-f032-4c68-a5ac-b4758298886f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHintMark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ffdc4f0-e356-4c70-8d9a-c853cb91e897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "db3c7873-2dc5-4bef-88e5-b1ba0595b525",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ffdc4f0-e356-4c70-8d9a-c853cb91e897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b23447a-4c15-433c-86ad-b2ea35f0d07d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ffdc4f0-e356-4c70-8d9a-c853cb91e897",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "ba4c73b4-69bf-4d0d-b23d-4721395ce938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "d73cfc75-6925-43b6-b8df-b9e914addd3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba4c73b4-69bf-4d0d-b23d-4721395ce938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "764a31d4-f414-48c2-9f9e-086104b815b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4c73b4-69bf-4d0d-b23d-4721395ce938",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "26909c29-6abf-46ec-a985-f2dadd1adf5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "822c06f8-cb6f-4668-b202-f960a70ca515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26909c29-6abf-46ec-a985-f2dadd1adf5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dbcd777-4113-48b7-b7a8-6c4d53c23d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26909c29-6abf-46ec-a985-f2dadd1adf5e",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "7c2f6edd-8566-42c6-831c-793b3c1e00fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "729070f0-396a-4267-be65-8862e7e1088d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c2f6edd-8566-42c6-831c-793b3c1e00fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "366fb07a-1431-45f6-888f-765d88762687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c2f6edd-8566-42c6-831c-793b3c1e00fc",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "83f0a81c-4be6-4905-a8f7-1cd9ed9f78e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "e220206a-ef74-4551-80ca-19313aaac3b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83f0a81c-4be6-4905-a8f7-1cd9ed9f78e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d614fe3f-07c6-4a46-be5f-57cb5065958b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83f0a81c-4be6-4905-a8f7-1cd9ed9f78e0",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "d4c7487e-3c42-49ca-94f6-aaa1d73a5f86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "1de57ec5-9be5-4341-885f-fbd6fe0c682c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c7487e-3c42-49ca-94f6-aaa1d73a5f86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a93cf83f-b9e3-46e1-b210-ce74b28d8f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c7487e-3c42-49ca-94f6-aaa1d73a5f86",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "c1fd3989-c260-4b59-b8c9-22111330d2d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "85be82d7-eab6-4d3a-ad04-faa37be27395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1fd3989-c260-4b59-b8c9-22111330d2d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "440378dc-9806-4746-aae7-6c944ad4a777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1fd3989-c260-4b59-b8c9-22111330d2d7",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "68e20240-c92c-4331-8128-adfb6be0bc49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "9a9862de-ff4f-48ca-a416-6515a0c492db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e20240-c92c-4331-8128-adfb6be0bc49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f967c3e5-7cdd-4603-8f8b-1328214a1f28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e20240-c92c-4331-8128-adfb6be0bc49",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "212bc6d5-a8be-48b0-b6bd-bc925a39a3d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "a3393cce-ef90-4b25-bdc6-39c86e893267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "212bc6d5-a8be-48b0-b6bd-bc925a39a3d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "331ff80b-a0d6-4277-b5a2-581c1c2e8d24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "212bc6d5-a8be-48b0-b6bd-bc925a39a3d6",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "3845ec08-e3e2-413b-9ef5-54121bb76b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "bcfbc428-6bb2-4d43-a311-9e9ac2e212c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3845ec08-e3e2-413b-9ef5-54121bb76b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f151c775-c227-4c38-a471-f264ab3d76fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3845ec08-e3e2-413b-9ef5-54121bb76b1d",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "01179c5d-c1d1-4654-9b72-c5d2e12c7ad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "99c5cd5b-b035-46a9-bcfe-8a538838f7ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01179c5d-c1d1-4654-9b72-c5d2e12c7ad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4994226-cbc9-4f3e-823c-cb4673df0783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01179c5d-c1d1-4654-9b72-c5d2e12c7ad4",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "bd5178d3-28c6-4d4a-8153-8c6ec5295d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "9f13f1fa-7ca5-4aab-9af5-0db55b5e7c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd5178d3-28c6-4d4a-8153-8c6ec5295d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f0df94-aede-4552-b0d9-75a442c7be4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd5178d3-28c6-4d4a-8153-8c6ec5295d11",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "e6783adb-00da-4bdb-8b3e-639a227feb7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "89cc31ba-55fd-4f09-92eb-6cf4c9b79ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6783adb-00da-4bdb-8b3e-639a227feb7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9205ad2-5568-4ef2-bd4e-7c93c3a580fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6783adb-00da-4bdb-8b3e-639a227feb7e",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "06940a04-3015-4521-a448-7b92c3b4ace2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "f63f40fd-2fc4-4849-8ac2-6912d09f4db2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06940a04-3015-4521-a448-7b92c3b4ace2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "362d7c90-0fb9-45fc-95e0-fafda82abc39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06940a04-3015-4521-a448-7b92c3b4ace2",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "8510d36f-8354-4aee-b912-72dca44d2c26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "e6aeb2e3-4484-445f-9c7d-35b6ccfaf399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8510d36f-8354-4aee-b912-72dca44d2c26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caec952f-8e35-44e3-8998-13d40e517378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8510d36f-8354-4aee-b912-72dca44d2c26",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "5ac2fba2-e2df-48bd-bb30-19bc771e5fff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "b6f09b9f-a1a7-475d-9e2b-64737326dda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ac2fba2-e2df-48bd-bb30-19bc771e5fff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7c17681-31a0-43b0-8318-1f2382fd5414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ac2fba2-e2df-48bd-bb30-19bc771e5fff",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "bb8958e2-2b19-4a0f-8cbe-cecd6c4a6cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "a5d79eda-c48a-4c5f-a946-fa457da68c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb8958e2-2b19-4a0f-8cbe-cecd6c4a6cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff1adf04-a416-4dec-a203-ff410ed24661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb8958e2-2b19-4a0f-8cbe-cecd6c4a6cda",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "8c24e667-5b6d-4cfe-b99b-e09bee1e6ac4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "79e77314-514c-4ffe-a93d-c4675fd0a0e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c24e667-5b6d-4cfe-b99b-e09bee1e6ac4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6395ef44-d26d-471f-a67c-22127fbf159b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c24e667-5b6d-4cfe-b99b-e09bee1e6ac4",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "98b32f41-85c7-47bd-8bca-d6f7fdf837f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "420962c1-9b59-449c-8062-005dd194543e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b32f41-85c7-47bd-8bca-d6f7fdf837f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6174ab-aa0c-4b1f-bab9-ff5ad78b23b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b32f41-85c7-47bd-8bca-d6f7fdf837f6",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "c3841908-61d7-4d3c-8a7a-3ead8901aba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "6bfd4509-fe24-4d18-b805-784f36c7558f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3841908-61d7-4d3c-8a7a-3ead8901aba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfed3e6d-8e9f-4f77-baf4-6aabf7ea13c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3841908-61d7-4d3c-8a7a-3ead8901aba2",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "5492ec0d-c9cf-4a7e-ac8c-119d803b8e32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "e091cd47-557d-4f22-a428-a79d1939d7ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5492ec0d-c9cf-4a7e-ac8c-119d803b8e32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545e3b50-38d7-4d37-b639-3935a5a8a6bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5492ec0d-c9cf-4a7e-ac8c-119d803b8e32",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "88e6f059-bca1-4b27-a817-8d4e0365e79f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "161e95b8-718a-4eeb-a515-e169d023c52c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88e6f059-bca1-4b27-a817-8d4e0365e79f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a363797-e66c-4017-a0a2-5cfa0c795ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88e6f059-bca1-4b27-a817-8d4e0365e79f",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "ccfed80b-21e1-4fdc-b21a-c14dfaf3aa9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "5a158b1f-3d85-4cac-a51d-9e2028c5611b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccfed80b-21e1-4fdc-b21a-c14dfaf3aa9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e8baaab-b339-483f-80d4-1eee8d4c675c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccfed80b-21e1-4fdc-b21a-c14dfaf3aa9b",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "305a1059-9777-4a49-966b-5d1db3c575ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "cacc3783-cc72-4857-b4f4-42bbd35104c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305a1059-9777-4a49-966b-5d1db3c575ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "428e54a9-54d1-47fa-8f89-7a34bd659fb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305a1059-9777-4a49-966b-5d1db3c575ce",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "ce66b8ea-403e-4d83-8cd7-46ebec972537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "d064ef5c-cbe4-4dbe-94c9-6766c8ae77d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce66b8ea-403e-4d83-8cd7-46ebec972537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1e9ecb-0666-46f8-b996-f7975eccaf39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce66b8ea-403e-4d83-8cd7-46ebec972537",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "d713871e-baf7-47fb-ad51-97ca51d8207a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "2269add8-4771-44e8-858f-da143b4e66a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d713871e-baf7-47fb-ad51-97ca51d8207a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c4230d-082e-4d98-89f0-189f003af9e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d713871e-baf7-47fb-ad51-97ca51d8207a",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "a94480e5-fdad-4128-8c8d-dc012bbcb4fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "1ae52449-d2a8-49fd-9a60-559d1f0b25de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a94480e5-fdad-4128-8c8d-dc012bbcb4fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a3819f-b958-4374-add4-01944772f191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a94480e5-fdad-4128-8c8d-dc012bbcb4fd",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "56ed44cd-247b-4d8e-b96f-ff7e8f5e762a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "e100993a-7851-43fa-b01d-8ba4f8786e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ed44cd-247b-4d8e-b96f-ff7e8f5e762a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf01064-f507-4d01-bc5c-958dda2a7692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ed44cd-247b-4d8e-b96f-ff7e8f5e762a",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "91bb57b0-19fd-4f2d-a820-84a665287bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "c04b49e6-c5b7-4288-838f-4bdb4eb02ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91bb57b0-19fd-4f2d-a820-84a665287bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b3381f-4b0f-4453-b223-1fce003a318b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91bb57b0-19fd-4f2d-a820-84a665287bb3",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        },
        {
            "id": "d43ab418-07b0-4ea4-9aec-e5d2f5e260d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "compositeImage": {
                "id": "fc339455-b830-465b-99c0-94c30950765e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d43ab418-07b0-4ea4-9aec-e5d2f5e260d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac4f1746-dab0-4025-a8bf-673a82e85bcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d43ab418-07b0-4ea4-9aec-e5d2f5e260d4",
                    "LayerId": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6f979478-0ffc-400a-a2b1-3aefab0d6ef5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07be824e-f032-4c68-a5ac-b4758298886f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}