{
    "id": "962c0ba6-0aeb-4811-958b-c392fffb9135",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTowerTopStars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 5,
    "bbox_right": 314,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "compositeImage": {
                "id": "828afc13-161e-4a21-a4e6-51ad8fec9310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc8c0d2-e7b2-452b-a68e-343b93a93f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "54333880-f9e7-4f51-af32-478eb3b4c54b"
                },
                {
                    "id": "0457a1b4-0112-49b6-9277-790bfd075eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "b6b6217f-b25e-46ab-8877-9ea59edded9c"
                },
                {
                    "id": "6b9fca08-6621-4284-88a2-e9cf07002c69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "55c3b69a-092f-448d-97a7-91be595089d7"
                },
                {
                    "id": "6467139c-e97a-44df-85e4-3715e5443e1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "dbc07ab7-7989-4eb1-bbf4-bafc6083c613"
                },
                {
                    "id": "03d23efd-df6d-4f26-a122-1504c83068bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "3fd5dc98-ecc6-4ea0-9c7a-fdcb0875d9ba"
                },
                {
                    "id": "e1141da5-30be-49fa-a236-9c479c454624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "ada0fd7c-4655-4560-917d-5aac1f27c572"
                },
                {
                    "id": "a575238a-17bd-449b-a69e-7dfc7a486f60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "8e6de8cf-19f0-40e5-8d99-4b1140764bf9"
                },
                {
                    "id": "77008334-a5f9-44c3-925e-f3a2cb378968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "ac2a44e5-6e90-4720-b16d-986b0f9ce32e"
                },
                {
                    "id": "4bd813ce-0bb8-46e5-b2fe-6d3414be56bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042b4ae-521e-4d5c-957c-dd01db8507bb",
                    "LayerId": "f795b563-c023-4202-a292-258f09917553"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "54333880-f9e7-4f51-af32-478eb3b4c54b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "Nothing",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b6b6217f-b25e-46ab-8877-9ea59edded9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "BigBell",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "55c3b69a-092f-448d-97a7-91be595089d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "Bell",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "dbc07ab7-7989-4eb1-bbf4-bafc6083c613",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "Stars",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3fd5dc98-ecc6-4ea0-9c7a-fdcb0875d9ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "Guide",
            "opacity": 25,
            "visible": false
        },
        {
            "id": "ada0fd7c-4655-4560-917d-5aac1f27c572",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "Numbers",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "8e6de8cf-19f0-40e5-8d99-4b1140764bf9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "ClockFace",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "ac2a44e5-6e90-4720-b16d-986b0f9ce32e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "Spikes",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "f795b563-c023-4202-a292-258f09917553",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962c0ba6-0aeb-4811-958b-c392fffb9135",
            "blendMode": 0,
            "isLocked": false,
            "name": "Sky",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 80
}