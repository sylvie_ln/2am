{
    "id": "65ef3ec7-0bb9-430d-b919-9b85a40a68c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a38701b5-afc7-4eb2-bd26-d04a0fc5bb53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65ef3ec7-0bb9-430d-b919-9b85a40a68c9",
            "compositeImage": {
                "id": "6eeaa2a5-bec5-4123-b969-38d2b61f1d65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a38701b5-afc7-4eb2-bd26-d04a0fc5bb53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5406fd59-9d10-4fb2-9ec8-0dcece41eb00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a38701b5-afc7-4eb2-bd26-d04a0fc5bb53",
                    "LayerId": "772ddc5f-172e-47bd-8f70-fca22643ccad"
                },
                {
                    "id": "f9edd468-4d86-418a-a44a-f27e87688bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a38701b5-afc7-4eb2-bd26-d04a0fc5bb53",
                    "LayerId": "c3d2f743-e241-4901-ad14-d7197013a37f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c3d2f743-e241-4901-ad14-d7197013a37f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65ef3ec7-0bb9-430d-b919-9b85a40a68c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "772ddc5f-172e-47bd-8f70-fca22643ccad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65ef3ec7-0bb9-430d-b919-9b85a40a68c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}