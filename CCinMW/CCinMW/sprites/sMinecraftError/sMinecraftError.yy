{
    "id": "8c756fde-75c6-49a3-9c8c-fe1ecd59bf20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecraftError",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 74,
    "bbox_right": 147,
    "bbox_top": 134,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c1d307b-68f2-4fa8-beeb-450e9953fa35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c756fde-75c6-49a3-9c8c-fe1ecd59bf20",
            "compositeImage": {
                "id": "a60a1823-83a3-4c22-924f-05b1d8bd88d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c1d307b-68f2-4fa8-beeb-450e9953fa35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67e4ea81-d12c-464e-82ae-af9eab2450b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c1d307b-68f2-4fa8-beeb-450e9953fa35",
                    "LayerId": "5a1a8a24-fd06-47e5-9155-36b8b85033ef"
                }
            ]
        },
        {
            "id": "5763d518-9a94-430b-b864-d3d2a2e411ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c756fde-75c6-49a3-9c8c-fe1ecd59bf20",
            "compositeImage": {
                "id": "e37d3dc1-bebd-452a-a085-fc8f226db911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5763d518-9a94-430b-b864-d3d2a2e411ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c331412c-3f68-4f8f-83cf-05a9d9eed2f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5763d518-9a94-430b-b864-d3d2a2e411ec",
                    "LayerId": "5a1a8a24-fd06-47e5-9155-36b8b85033ef"
                }
            ]
        },
        {
            "id": "00a7d810-f125-4b77-9d0f-20104cf2256f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c756fde-75c6-49a3-9c8c-fe1ecd59bf20",
            "compositeImage": {
                "id": "0249f009-320c-4b49-b071-6716008b1eb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00a7d810-f125-4b77-9d0f-20104cf2256f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be5d04a3-987d-4263-a02c-788b7bfbb850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a7d810-f125-4b77-9d0f-20104cf2256f",
                    "LayerId": "5a1a8a24-fd06-47e5-9155-36b8b85033ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 164,
    "layers": [
        {
            "id": "5a1a8a24-fd06-47e5-9155-36b8b85033ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c756fde-75c6-49a3-9c8c-fe1ecd59bf20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 232,
    "xorig": 116,
    "yorig": 82
}