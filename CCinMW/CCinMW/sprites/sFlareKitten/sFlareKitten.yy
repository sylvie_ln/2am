{
    "id": "210ded78-a5ba-4da9-acba-66a6839c2fc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlareKitten",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "296310d2-8820-4026-ad46-7448c940572b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "210ded78-a5ba-4da9-acba-66a6839c2fc6",
            "compositeImage": {
                "id": "3f49fb74-e14d-4f11-a8e5-c42a241b727c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "296310d2-8820-4026-ad46-7448c940572b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32dcfbb8-bbbe-42f6-b9d8-058e2ce3498f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "296310d2-8820-4026-ad46-7448c940572b",
                    "LayerId": "e3405a59-f6bd-4b52-8d17-617c9f9d65b6"
                }
            ]
        },
        {
            "id": "5cfba979-503f-4d86-8aff-48e904f322bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "210ded78-a5ba-4da9-acba-66a6839c2fc6",
            "compositeImage": {
                "id": "5c754984-2851-4705-b309-7772ecd6c81a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cfba979-503f-4d86-8aff-48e904f322bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d983fb-a0ac-45e7-92e3-2c4d4a2dbda2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cfba979-503f-4d86-8aff-48e904f322bc",
                    "LayerId": "e3405a59-f6bd-4b52-8d17-617c9f9d65b6"
                }
            ]
        },
        {
            "id": "68ab1313-0756-4ba3-a9fa-a17e4ac24f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "210ded78-a5ba-4da9-acba-66a6839c2fc6",
            "compositeImage": {
                "id": "ec1de190-63dc-4366-998f-a8a2d4ac9b45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68ab1313-0756-4ba3-a9fa-a17e4ac24f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52edeb17-614d-427a-8088-f7d31a937b11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68ab1313-0756-4ba3-a9fa-a17e4ac24f46",
                    "LayerId": "e3405a59-f6bd-4b52-8d17-617c9f9d65b6"
                }
            ]
        },
        {
            "id": "8a6e8f4a-10e4-44cb-a492-0572bbe623e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "210ded78-a5ba-4da9-acba-66a6839c2fc6",
            "compositeImage": {
                "id": "b018ba9a-6538-4bbd-b122-d963ed6526d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a6e8f4a-10e4-44cb-a492-0572bbe623e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "455f5b96-095d-40b9-9905-e9b6aea244b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a6e8f4a-10e4-44cb-a492-0572bbe623e5",
                    "LayerId": "e3405a59-f6bd-4b52-8d17-617c9f9d65b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e3405a59-f6bd-4b52-8d17-617c9f9d65b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "210ded78-a5ba-4da9-acba-66a6839c2fc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}