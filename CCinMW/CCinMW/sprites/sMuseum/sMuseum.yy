{
    "id": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMuseum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c430c446-5ec5-48b0-a555-3b80c3103926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
            "compositeImage": {
                "id": "d4662c6b-a8e1-48eb-9c5c-7734e0584ca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c430c446-5ec5-48b0-a555-3b80c3103926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "730cf2d8-80ef-4c6e-b459-74f0b929a20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c430c446-5ec5-48b0-a555-3b80c3103926",
                    "LayerId": "5fe5b965-388f-4b41-89ec-c623c699516d"
                },
                {
                    "id": "901596a8-0b3b-4dd1-b790-9a5ef84e6c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c430c446-5ec5-48b0-a555-3b80c3103926",
                    "LayerId": "7c4680c0-9366-48cb-91da-c10fd5e2cc61"
                }
            ]
        },
        {
            "id": "909f0b04-7cb1-4bc5-9c5a-11f89ee156bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
            "compositeImage": {
                "id": "8f188b63-2792-4ad0-820b-c9623a237020",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909f0b04-7cb1-4bc5-9c5a-11f89ee156bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8cf57d4-3a47-422c-ad95-a108e78dfe44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909f0b04-7cb1-4bc5-9c5a-11f89ee156bc",
                    "LayerId": "5fe5b965-388f-4b41-89ec-c623c699516d"
                },
                {
                    "id": "a2be2224-d25b-483e-86d5-349b89d224f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909f0b04-7cb1-4bc5-9c5a-11f89ee156bc",
                    "LayerId": "7c4680c0-9366-48cb-91da-c10fd5e2cc61"
                }
            ]
        },
        {
            "id": "c841fbcc-89f3-4a3c-bb3b-2251feb2d2bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
            "compositeImage": {
                "id": "8c246344-1514-4568-bff9-e496d9987417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c841fbcc-89f3-4a3c-bb3b-2251feb2d2bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a478ab86-fe58-44b7-baba-1b3f85159c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c841fbcc-89f3-4a3c-bb3b-2251feb2d2bf",
                    "LayerId": "5fe5b965-388f-4b41-89ec-c623c699516d"
                },
                {
                    "id": "51adcd27-fd15-4b3a-9a62-fe5ae91eefed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c841fbcc-89f3-4a3c-bb3b-2251feb2d2bf",
                    "LayerId": "7c4680c0-9366-48cb-91da-c10fd5e2cc61"
                }
            ]
        },
        {
            "id": "10d4d02e-ebfd-42e0-b0d1-7beb3251f5f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
            "compositeImage": {
                "id": "5e6a0621-2f10-4f5c-b9bd-49c05836305c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d4d02e-ebfd-42e0-b0d1-7beb3251f5f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "131f06ef-2986-4cb8-891e-12c51cdaecac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d4d02e-ebfd-42e0-b0d1-7beb3251f5f1",
                    "LayerId": "5fe5b965-388f-4b41-89ec-c623c699516d"
                },
                {
                    "id": "d4bb3226-3656-4e14-b952-882c058b0b22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d4d02e-ebfd-42e0-b0d1-7beb3251f5f1",
                    "LayerId": "7c4680c0-9366-48cb-91da-c10fd5e2cc61"
                }
            ]
        },
        {
            "id": "94c6c009-3432-4da0-b1e2-c31566dc9676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
            "compositeImage": {
                "id": "f92d2415-5a39-4950-808a-3bd65a99ed1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94c6c009-3432-4da0-b1e2-c31566dc9676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0187b8b-9c77-4f20-aaab-bf86225ae832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94c6c009-3432-4da0-b1e2-c31566dc9676",
                    "LayerId": "5fe5b965-388f-4b41-89ec-c623c699516d"
                },
                {
                    "id": "fa1a370a-acfe-40b9-9f20-3d1f2266c78b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94c6c009-3432-4da0-b1e2-c31566dc9676",
                    "LayerId": "7c4680c0-9366-48cb-91da-c10fd5e2cc61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5fe5b965-388f-4b41-89ec-c623c699516d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 25,
            "visible": true
        },
        {
            "id": "7c4680c0-9366-48cb-91da-c10fd5e2cc61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d33ecb3e-3ba2-4cf4-a755-89af9c168c2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        1090519039,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4284768238,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}