{
    "id": "621d4542-c2af-4c6f-a3ae-1c1035ba2955",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMiniBunneyNES",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e18ee385-d756-40e2-82bc-d7232584c11f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "621d4542-c2af-4c6f-a3ae-1c1035ba2955",
            "compositeImage": {
                "id": "9f3b9e73-dfa8-46d1-91b4-f88860062742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e18ee385-d756-40e2-82bc-d7232584c11f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77a97cc-7a17-4506-a90b-7e0d1c5c7763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e18ee385-d756-40e2-82bc-d7232584c11f",
                    "LayerId": "530e4f17-d39d-4b73-a290-c757d42e7687"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "530e4f17-d39d-4b73-a290-c757d42e7687",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "621d4542-c2af-4c6f-a3ae-1c1035ba2955",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}