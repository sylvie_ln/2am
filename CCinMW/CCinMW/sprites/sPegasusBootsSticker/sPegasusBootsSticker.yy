{
    "id": "6130861a-c12f-4797-bba1-3286570f5ce1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPegasusBootsSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27ffa87d-a6ba-42e7-b701-856bb957213c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6130861a-c12f-4797-bba1-3286570f5ce1",
            "compositeImage": {
                "id": "adb8db38-4811-481b-b4f0-e145076a042a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27ffa87d-a6ba-42e7-b701-856bb957213c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95f53754-4290-4d35-a8f9-5a1976d8d949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27ffa87d-a6ba-42e7-b701-856bb957213c",
                    "LayerId": "0ccae6e8-58af-4067-9421-0031458c2d5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0ccae6e8-58af-4067-9421-0031458c2d5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6130861a-c12f-4797-bba1-3286570f5ce1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}