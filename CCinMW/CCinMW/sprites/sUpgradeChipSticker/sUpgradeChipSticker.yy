{
    "id": "d58a5692-d181-4fd7-93c4-fe4610f0ca43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUpgradeChipSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "665fe55e-342a-44b3-bd50-babb74ee2d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d58a5692-d181-4fd7-93c4-fe4610f0ca43",
            "compositeImage": {
                "id": "59075cfa-b729-456d-9295-f593f0634e62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "665fe55e-342a-44b3-bd50-babb74ee2d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90f7ecb7-6c2f-468e-b02b-8591e38321ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "665fe55e-342a-44b3-bd50-babb74ee2d43",
                    "LayerId": "08c9cb5a-50cc-482f-aec2-cd188b6a7f3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "08c9cb5a-50cc-482f-aec2-cd188b6a7f3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d58a5692-d181-4fd7-93c4-fe4610f0ca43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}