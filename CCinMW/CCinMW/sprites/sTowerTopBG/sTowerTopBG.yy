{
    "id": "88ea6312-3f3c-454a-9505-93b8b878de94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTowerTopBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 104,
    "bbox_right": 215,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02d9942f-f299-4812-a131-1235df92e1bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "compositeImage": {
                "id": "bebbf360-b812-4f99-95b8-938abdbce43e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b664cecc-b094-441b-852b-14535c252810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "8948b15a-a39e-45a9-99c8-d94b337f9ab7"
                },
                {
                    "id": "4fb41cb3-62e5-4477-a284-5d71f332298d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "daf63e01-b006-46bc-943f-5202c2185604"
                },
                {
                    "id": "50089502-82d0-4669-afb7-ccbd1426eaed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "89b53c3c-5c85-4dd3-b582-907bcf3562dd"
                },
                {
                    "id": "8b728e47-705f-4f56-8c0d-2d5c7eefd85d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "3adcb8be-42a5-4738-a662-8ac3e98cbf14"
                },
                {
                    "id": "eac6f85d-4159-4a81-b44b-33aea94e3761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "022a6345-6db8-4173-a58d-984af1c75ca1"
                },
                {
                    "id": "d0fe6ac2-41a6-4a67-b644-22cf72bd789b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "5c615a2c-2b66-40b5-b579-3b8fbf81f9d1"
                },
                {
                    "id": "ddaffcf7-8e9a-4eac-84d7-704c3a228de1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "3b38465a-c823-4265-9508-7e64b66d804e"
                },
                {
                    "id": "485a36ed-3874-4f30-83f3-29e79bbf4c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "64e8df75-a806-47f4-97b6-6faed4e08dc4"
                },
                {
                    "id": "75a74c45-03c7-4f8f-9d73-cf2c705b714d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9942f-f299-4812-a131-1235df92e1bd",
                    "LayerId": "5cf3bc1a-db91-4b3d-b95d-8d59c4e306e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "5cf3bc1a-db91-4b3d-b95d-8d59c4e306e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Nothing",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "64e8df75-a806-47f4-97b6-6faed4e08dc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": false,
            "name": "BigBell",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "5c615a2c-2b66-40b5-b579-3b8fbf81f9d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Bell",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "daf63e01-b006-46bc-943f-5202c2185604",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": true,
            "name": "Stars",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "3adcb8be-42a5-4738-a662-8ac3e98cbf14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Guide",
            "opacity": 25,
            "visible": false
        },
        {
            "id": "022a6345-6db8-4173-a58d-984af1c75ca1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Numbers",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3b38465a-c823-4265-9508-7e64b66d804e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": false,
            "name": "ClockFace",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "89b53c3c-5c85-4dd3-b582-907bcf3562dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Spikes",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "8948b15a-a39e-45a9-99c8-d94b337f9ab7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88ea6312-3f3c-454a-9505-93b8b878de94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Sky",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 80
}