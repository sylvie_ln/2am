{
    "id": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHandNES",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9071123-6ac1-4d5a-b7bc-d60161db18c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "57b4afde-28c1-4d79-9aab-bc0d1ad42489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9071123-6ac1-4d5a-b7bc-d60161db18c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f466f959-880c-43a7-bfc9-e469b9cf43e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9071123-6ac1-4d5a-b7bc-d60161db18c4",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "d0ce48fb-0bc6-44cf-aaea-3d36d949f54f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "350a476d-e1de-4fc9-902e-de98c40024c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0ce48fb-0bc6-44cf-aaea-3d36d949f54f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8d97b2-b74d-4ebc-b709-d74dceffbe8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0ce48fb-0bc6-44cf-aaea-3d36d949f54f",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "0ccd466e-c2c1-43fb-a18a-3543527b9b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "a04b8677-5c20-45f7-9dc9-239561bd8780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ccd466e-c2c1-43fb-a18a-3543527b9b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0bcf0d-0427-45ea-b46f-65f1411ad685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ccd466e-c2c1-43fb-a18a-3543527b9b13",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "a8527d2d-83b2-4d81-8946-2ab449114bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "32320899-aa63-4f63-84a5-1f8c38aadcd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8527d2d-83b2-4d81-8946-2ab449114bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07d9f5c1-1cf4-4343-bd1d-66c56c347b8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8527d2d-83b2-4d81-8946-2ab449114bc2",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "8c363c5e-7c2f-4e2d-a0bd-e1a76a58ef77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "315619fd-95b9-49e2-880d-e4fbffab8e42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c363c5e-7c2f-4e2d-a0bd-e1a76a58ef77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f051164-677f-49ac-9936-d88e6437499c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c363c5e-7c2f-4e2d-a0bd-e1a76a58ef77",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "2382bc22-11b5-4b29-8cef-dea6486547f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "f4bca061-3859-4d75-a3ec-9c813412798f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2382bc22-11b5-4b29-8cef-dea6486547f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13b403c5-4f6d-4472-9209-25b6ea2f48d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2382bc22-11b5-4b29-8cef-dea6486547f0",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "50364617-bf50-4550-8d5b-83fab51c3330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "944b47b9-9a80-4be8-9d5b-38c34ddca351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50364617-bf50-4550-8d5b-83fab51c3330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016ca506-4d26-4c3d-84b0-0356069ba860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50364617-bf50-4550-8d5b-83fab51c3330",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "816b0be4-e243-4106-a733-461a6b43491d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "55aaf707-db47-42fe-8039-e325ec8d8c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "816b0be4-e243-4106-a733-461a6b43491d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e1029d2-2861-47f0-8a90-71557f0ca01c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "816b0be4-e243-4106-a733-461a6b43491d",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "32a58763-2794-4192-ada4-5e8d500c1e33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "4bbd8dcf-215b-4f45-873c-dd33d3532a8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a58763-2794-4192-ada4-5e8d500c1e33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37121542-726c-4d18-94e7-baa6ff4f67d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a58763-2794-4192-ada4-5e8d500c1e33",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "29d5250d-a73b-4a8b-beef-4877c02afff8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "0d11edd3-6099-4001-9ae4-d4847d9cc270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d5250d-a73b-4a8b-beef-4877c02afff8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "facb1f46-e0c7-40c3-8ad4-820a0186989f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d5250d-a73b-4a8b-beef-4877c02afff8",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "34f16005-4413-43e9-a466-68b8f176af15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "945c0fce-a39b-4064-a970-e9e3cbe0dce5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34f16005-4413-43e9-a466-68b8f176af15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a4c46d-57c2-40fd-bf15-c2c15498a3b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34f16005-4413-43e9-a466-68b8f176af15",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        },
        {
            "id": "bf7b9fa7-9dfa-4222-b5f3-c67939fc6616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "compositeImage": {
                "id": "d2d6f0bc-b130-4c64-9c43-e63bcdd66285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf7b9fa7-9dfa-4222-b5f3-c67939fc6616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa7ac0c-cee7-4cf4-8dbe-cb80e9d76f35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf7b9fa7-9dfa-4222-b5f3-c67939fc6616",
                    "LayerId": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "95218f6c-e7d6-4fa7-b5ad-1b6f0669efec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a38c0614-bfd1-4649-967b-31dc6fa8d2fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}