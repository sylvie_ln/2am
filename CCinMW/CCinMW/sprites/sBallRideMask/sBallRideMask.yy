{
    "id": "2ac5ad20-7f26-4790-a6b4-fdca7f317833",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBallRideMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": -16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "516e197c-f02b-4acd-a2ff-56809cdc67e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ac5ad20-7f26-4790-a6b4-fdca7f317833",
            "compositeImage": {
                "id": "06328683-c7fd-48a4-bbdf-fcb90b9d4006",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516e197c-f02b-4acd-a2ff-56809cdc67e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224043c1-3a97-4202-87f5-c12486d3893b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516e197c-f02b-4acd-a2ff-56809cdc67e1",
                    "LayerId": "44823fdc-01b3-4eff-ae13-7b1cba3d13c7"
                },
                {
                    "id": "1993661b-6c60-4516-986f-39f9b9860026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516e197c-f02b-4acd-a2ff-56809cdc67e1",
                    "LayerId": "e51f4ce0-e0dc-43ea-89f5-af7e9372aeec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "44823fdc-01b3-4eff-ae13-7b1cba3d13c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ac5ad20-7f26-4790-a6b4-fdca7f317833",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e51f4ce0-e0dc-43ea-89f5-af7e9372aeec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ac5ad20-7f26-4790-a6b4-fdca7f317833",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}