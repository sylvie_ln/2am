{
    "id": "e06810fb-ec87-4426-8575-ee60035cd150",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlunch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5709aca3-e982-4b18-a5cd-572c2df10f9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e06810fb-ec87-4426-8575-ee60035cd150",
            "compositeImage": {
                "id": "95cdbabc-8329-4b26-8241-9b5ac855221d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5709aca3-e982-4b18-a5cd-572c2df10f9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7cdc91-950a-4fb9-a6b6-372e29a1a119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5709aca3-e982-4b18-a5cd-572c2df10f9e",
                    "LayerId": "b47e8685-f168-43a5-a74b-99e34d351b7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b47e8685-f168-43a5-a74b-99e34d351b7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e06810fb-ec87-4426-8575-ee60035cd150",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}