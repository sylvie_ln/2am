{
    "id": "6c58c345-fb1a-461a-93d7-3de132e1dcfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55719edf-2ba5-4f9d-8a51-1328ebcfce1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c58c345-fb1a-461a-93d7-3de132e1dcfd",
            "compositeImage": {
                "id": "68950c27-4d4d-4f7a-9f0d-2d2bef7baeff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55719edf-2ba5-4f9d-8a51-1328ebcfce1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "643e477e-8fd0-44f8-85f1-877f75974748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55719edf-2ba5-4f9d-8a51-1328ebcfce1b",
                    "LayerId": "70938bff-4409-4b39-8e18-25d138a057ce"
                }
            ]
        },
        {
            "id": "1f2c6546-0550-47cb-9aaf-4f25dd920a4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c58c345-fb1a-461a-93d7-3de132e1dcfd",
            "compositeImage": {
                "id": "d99f7b8b-1109-462f-9030-65a7b490362a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2c6546-0550-47cb-9aaf-4f25dd920a4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c0e954-f1a7-4fac-9705-7c8966beac9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2c6546-0550-47cb-9aaf-4f25dd920a4e",
                    "LayerId": "70938bff-4409-4b39-8e18-25d138a057ce"
                }
            ]
        },
        {
            "id": "aee59ef3-e537-4fc4-af7c-8be6c6554ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c58c345-fb1a-461a-93d7-3de132e1dcfd",
            "compositeImage": {
                "id": "8612ffcf-aa1c-4a46-b465-108b7c6e9ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee59ef3-e537-4fc4-af7c-8be6c6554ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "887908af-97f7-490a-ac95-c89def35cfe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee59ef3-e537-4fc4-af7c-8be6c6554ea4",
                    "LayerId": "70938bff-4409-4b39-8e18-25d138a057ce"
                }
            ]
        },
        {
            "id": "1c1c1ba3-1587-4228-9daa-e5d3c8d10e4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c58c345-fb1a-461a-93d7-3de132e1dcfd",
            "compositeImage": {
                "id": "01a28780-f704-4c08-b9a0-7f2ae5b3dfa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c1c1ba3-1587-4228-9daa-e5d3c8d10e4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6e3bc8-72ed-4dc2-b0ec-8827d1a0398b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c1c1ba3-1587-4228-9daa-e5d3c8d10e4b",
                    "LayerId": "70938bff-4409-4b39-8e18-25d138a057ce"
                }
            ]
        },
        {
            "id": "3c267b9a-a4e3-48e5-8ee8-03cf9160f7b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c58c345-fb1a-461a-93d7-3de132e1dcfd",
            "compositeImage": {
                "id": "d1ccc3d8-aaee-4e40-b85b-fe87348ac04b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c267b9a-a4e3-48e5-8ee8-03cf9160f7b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "539eee16-706d-4b4b-861c-e0a918824c7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c267b9a-a4e3-48e5-8ee8-03cf9160f7b7",
                    "LayerId": "70938bff-4409-4b39-8e18-25d138a057ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "70938bff-4409-4b39-8e18-25d138a057ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c58c345-fb1a-461a-93d7-3de132e1dcfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}