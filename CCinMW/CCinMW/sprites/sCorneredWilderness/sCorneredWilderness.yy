{
    "id": "e4b30d37-bf44-4f1d-a90d-ba7629a13034",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCorneredWilderness",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69f2198c-7399-476b-9fd0-af805310b0b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4b30d37-bf44-4f1d-a90d-ba7629a13034",
            "compositeImage": {
                "id": "62bb9005-915a-4c95-9f9a-83a708928855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69f2198c-7399-476b-9fd0-af805310b0b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdb15a16-7cdb-43ca-8860-61f16365ce0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69f2198c-7399-476b-9fd0-af805310b0b5",
                    "LayerId": "cc885444-f82d-480f-83dc-39e008d64717"
                }
            ]
        },
        {
            "id": "3ab71b2c-7683-4191-a9a3-a96466b21a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4b30d37-bf44-4f1d-a90d-ba7629a13034",
            "compositeImage": {
                "id": "57ff0af7-1179-4edf-9d0d-710e72a11082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ab71b2c-7683-4191-a9a3-a96466b21a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2269fe3e-b22c-4e17-9c18-637fb5e3e7e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab71b2c-7683-4191-a9a3-a96466b21a93",
                    "LayerId": "cc885444-f82d-480f-83dc-39e008d64717"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "cc885444-f82d-480f-83dc-39e008d64717",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4b30d37-bf44-4f1d-a90d-ba7629a13034",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}