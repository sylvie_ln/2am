{
    "id": "5b399c27-b158-4c90-b1ce-18480dd62d74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanyonCave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e63972fe-5698-4827-b28e-1f4ba2e18064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b399c27-b158-4c90-b1ce-18480dd62d74",
            "compositeImage": {
                "id": "08d02b2e-5718-4fba-9d8b-363167e00d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e63972fe-5698-4827-b28e-1f4ba2e18064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7522ac13-12e9-4112-a787-2e293639274e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e63972fe-5698-4827-b28e-1f4ba2e18064",
                    "LayerId": "0764f94b-e8e1-4669-b007-8de22f782dec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0764f94b-e8e1-4669-b007-8de22f782dec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b399c27-b158-4c90-b1ce-18480dd62d74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}