{
    "id": "d6702b38-99ee-4655-b374-4bd0e49593ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDarkness",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f5b6838-43cc-45c9-960e-1c7dfbc52286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6702b38-99ee-4655-b374-4bd0e49593ab",
            "compositeImage": {
                "id": "1b1ced56-543d-40d7-a02e-5759ea2b3f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f5b6838-43cc-45c9-960e-1c7dfbc52286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db33e196-0b81-41b7-989f-ed6caee4bd44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f5b6838-43cc-45c9-960e-1c7dfbc52286",
                    "LayerId": "a0791a38-2eb1-441e-b72e-af977642b3dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "a0791a38-2eb1-441e-b72e-af977642b3dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6702b38-99ee-4655-b374-4bd0e49593ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}