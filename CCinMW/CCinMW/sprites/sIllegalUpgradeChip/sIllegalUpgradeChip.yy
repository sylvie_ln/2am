{
    "id": "3e960114-f332-49a5-a401-7cb55db57b2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIllegalUpgradeChip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "489362bf-3a20-43c1-a8f5-35ebe121c6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e960114-f332-49a5-a401-7cb55db57b2a",
            "compositeImage": {
                "id": "91c9fae5-df37-4c38-b1ad-548db68d34b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "489362bf-3a20-43c1-a8f5-35ebe121c6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6159d37b-75b6-4cb8-bc43-21ea17f52178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "489362bf-3a20-43c1-a8f5-35ebe121c6b5",
                    "LayerId": "a0aa88ca-0f32-4ddf-9793-4a228e6afdd7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a0aa88ca-0f32-4ddf-9793-4a228e6afdd7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e960114-f332-49a5-a401-7cb55db57b2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}