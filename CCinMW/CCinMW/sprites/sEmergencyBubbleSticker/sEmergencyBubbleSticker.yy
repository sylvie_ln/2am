{
    "id": "75efbaff-a57d-499c-a2cc-6e1d43307f5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEmergencyBubbleSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06e81b38-f875-4fa4-bcca-179998d648ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75efbaff-a57d-499c-a2cc-6e1d43307f5e",
            "compositeImage": {
                "id": "43587302-9999-4811-8919-a048c5124db4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e81b38-f875-4fa4-bcca-179998d648ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35285a8b-58a7-4d01-aac9-2176b375b7ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e81b38-f875-4fa4-bcca-179998d648ff",
                    "LayerId": "6b263033-7c10-4d01-a352-1adb2ee4699d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6b263033-7c10-4d01-a352-1adb2ee4699d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75efbaff-a57d-499c-a2cc-6e1d43307f5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 9
}