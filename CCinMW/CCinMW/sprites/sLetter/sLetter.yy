{
    "id": "43d9de6e-1cdb-4d68-9389-967924e82337",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLetter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a8a0654-3fdd-46f5-86fe-aeb05ccfcb7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d9de6e-1cdb-4d68-9389-967924e82337",
            "compositeImage": {
                "id": "8b17135f-d977-469b-ab3a-bcb84cff5053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a8a0654-3fdd-46f5-86fe-aeb05ccfcb7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9036f09b-708d-4003-b603-891ec883aaf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a8a0654-3fdd-46f5-86fe-aeb05ccfcb7c",
                    "LayerId": "741c987c-b392-4133-9db0-7464a81005a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "741c987c-b392-4133-9db0-7464a81005a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43d9de6e-1cdb-4d68-9389-967924e82337",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}