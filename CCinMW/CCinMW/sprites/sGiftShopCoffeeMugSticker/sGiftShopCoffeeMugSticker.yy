{
    "id": "a40bfac2-20e2-4779-b1d0-1f49c2d4bf5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopCoffeeMugSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d7b71f2-18de-4651-a4b9-253f418dadcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a40bfac2-20e2-4779-b1d0-1f49c2d4bf5a",
            "compositeImage": {
                "id": "6bb247e0-b1a9-4a7e-9b23-130b5c792f31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7b71f2-18de-4651-a4b9-253f418dadcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f654356f-a2fb-497f-8b3e-d41233b70760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7b71f2-18de-4651-a4b9-253f418dadcb",
                    "LayerId": "16989fb2-25f8-4e56-bb8b-d319969d0a53"
                },
                {
                    "id": "599af991-ccec-41af-96ba-06872dfd0a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7b71f2-18de-4651-a4b9-253f418dadcb",
                    "LayerId": "cf51f0e9-30e8-4271-83ea-ee68de0b9c11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "16989fb2-25f8-4e56-bb8b-d319969d0a53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a40bfac2-20e2-4779-b1d0-1f49c2d4bf5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "cf51f0e9-30e8-4271-83ea-ee68de0b9c11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a40bfac2-20e2-4779-b1d0-1f49c2d4bf5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}