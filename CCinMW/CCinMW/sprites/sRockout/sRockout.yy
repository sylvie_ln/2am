{
    "id": "fcb43c80-ee67-4312-8557-85fd48d6f303",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRockout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ad56321-aaff-40c6-b6be-7d8e9a046d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcb43c80-ee67-4312-8557-85fd48d6f303",
            "compositeImage": {
                "id": "8997ccda-0c9b-4e00-acfb-52a0b14db0f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ad56321-aaff-40c6-b6be-7d8e9a046d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8524e36-7eb3-4b66-9960-6a22096981ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ad56321-aaff-40c6-b6be-7d8e9a046d3e",
                    "LayerId": "1017b704-e470-41ad-b56e-44eb8646eddf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1017b704-e470-41ad-b56e-44eb8646eddf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcb43c80-ee67-4312-8557-85fd48d6f303",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}