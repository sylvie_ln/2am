{
    "id": "0fca0ae4-5da9-4137-aee6-0615a35234e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCrumbledHearth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "132f01c0-2f75-467f-b698-2c82184cae5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fca0ae4-5da9-4137-aee6-0615a35234e4",
            "compositeImage": {
                "id": "144d397b-f6af-4deb-9786-c5d00f998fb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132f01c0-2f75-467f-b698-2c82184cae5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a1282c-5275-46e0-b33a-796319bbcdb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132f01c0-2f75-467f-b698-2c82184cae5e",
                    "LayerId": "a0d3e753-0333-4c68-a3ea-98321a82e1c5"
                }
            ]
        },
        {
            "id": "26cebcbf-b300-41b4-b969-a424c98db42d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fca0ae4-5da9-4137-aee6-0615a35234e4",
            "compositeImage": {
                "id": "7e25dd64-448d-43a8-aa08-f237a364ee31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26cebcbf-b300-41b4-b969-a424c98db42d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dedf54cc-02f2-4bad-a721-dceef1034981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26cebcbf-b300-41b4-b969-a424c98db42d",
                    "LayerId": "a0d3e753-0333-4c68-a3ea-98321a82e1c5"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "a0d3e753-0333-4c68-a3ea-98321a82e1c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fca0ae4-5da9-4137-aee6-0615a35234e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}