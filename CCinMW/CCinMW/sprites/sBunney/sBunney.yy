{
    "id": "75d5e985-a4a8-4475-a0f1-303207124ffa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunney",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dec3c1e8-eaa8-46c8-b045-6d94d8d3a63c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d5e985-a4a8-4475-a0f1-303207124ffa",
            "compositeImage": {
                "id": "0cc6bbf6-5779-42cb-a190-ff0488606a88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec3c1e8-eaa8-46c8-b045-6d94d8d3a63c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ba3b4c-2616-4d0a-a52f-a9c84ce81925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec3c1e8-eaa8-46c8-b045-6d94d8d3a63c",
                    "LayerId": "a5fb5f3f-7383-4520-b67c-40e3a1b08dca"
                },
                {
                    "id": "448187c2-0f5c-4417-bb7b-d603c050252e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec3c1e8-eaa8-46c8-b045-6d94d8d3a63c",
                    "LayerId": "459d791d-f6ff-48a4-92c7-5cf9d65f2e1b"
                }
            ]
        },
        {
            "id": "97cc86a9-d9cd-4730-ba21-269ae1354cf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d5e985-a4a8-4475-a0f1-303207124ffa",
            "compositeImage": {
                "id": "9eab4e54-fbc8-4a45-9fe8-d833ae323a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97cc86a9-d9cd-4730-ba21-269ae1354cf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b700544d-0bf8-4636-af30-8084c349ed52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97cc86a9-d9cd-4730-ba21-269ae1354cf1",
                    "LayerId": "a5fb5f3f-7383-4520-b67c-40e3a1b08dca"
                },
                {
                    "id": "bc42931e-7b25-4caa-b087-11e7ffa0409e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97cc86a9-d9cd-4730-ba21-269ae1354cf1",
                    "LayerId": "459d791d-f6ff-48a4-92c7-5cf9d65f2e1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "459d791d-f6ff-48a4-92c7-5cf9d65f2e1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75d5e985-a4a8-4475-a0f1-303207124ffa",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a5fb5f3f-7383-4520-b67c-40e3a1b08dca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75d5e985-a4a8-4475-a0f1-303207124ffa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}