{
    "id": "dcbfdb7f-654f-4677-8477-948935906dba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sQueen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23d7dae8-bee3-425c-8060-e47c3bb706e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcbfdb7f-654f-4677-8477-948935906dba",
            "compositeImage": {
                "id": "12a869bc-bd1c-4c16-96ec-5351f2fb6a09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d7dae8-bee3-425c-8060-e47c3bb706e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc8588f-cce5-4a60-9bc3-9df468308e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d7dae8-bee3-425c-8060-e47c3bb706e9",
                    "LayerId": "395d00eb-f002-466a-9296-79d8ebf96d8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "395d00eb-f002-466a-9296-79d8ebf96d8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcbfdb7f-654f-4677-8477-948935906dba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}