{
    "id": "e3eeb1a1-4708-4805-81d7-427031200be7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDecos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fb40d3a-1101-42c7-9c9b-fb3c18d05e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "compositeImage": {
                "id": "8ebff938-2ed7-4b89-ac33-e2cfa022d057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fb40d3a-1101-42c7-9c9b-fb3c18d05e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e33f8cb-c1f0-43f2-ac39-4c1869fb061c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fb40d3a-1101-42c7-9c9b-fb3c18d05e5d",
                    "LayerId": "8350038e-0071-4f83-9468-c61d0898f1c5"
                },
                {
                    "id": "23066233-156f-4b61-997b-904006a5f9df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fb40d3a-1101-42c7-9c9b-fb3c18d05e5d",
                    "LayerId": "2be5c0c0-f470-4349-96b0-3334f28085ce"
                }
            ]
        },
        {
            "id": "03378b01-42f5-4453-b71b-ae626acfc886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "compositeImage": {
                "id": "5068ac0a-d159-46b9-bca8-a3775fa1c9aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03378b01-42f5-4453-b71b-ae626acfc886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc95fcb1-26d3-4211-ae91-d01f0eec026c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03378b01-42f5-4453-b71b-ae626acfc886",
                    "LayerId": "8350038e-0071-4f83-9468-c61d0898f1c5"
                },
                {
                    "id": "af39c9d2-7b89-4c21-af87-e32e0efe1f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03378b01-42f5-4453-b71b-ae626acfc886",
                    "LayerId": "2be5c0c0-f470-4349-96b0-3334f28085ce"
                }
            ]
        },
        {
            "id": "5115c853-e239-4f34-ba1c-540380373e03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "compositeImage": {
                "id": "e165b772-35af-41d8-b286-978146626c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5115c853-e239-4f34-ba1c-540380373e03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e1510f-01cb-4380-bb40-78e90a68ddeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5115c853-e239-4f34-ba1c-540380373e03",
                    "LayerId": "8350038e-0071-4f83-9468-c61d0898f1c5"
                },
                {
                    "id": "49f27d20-8d22-4b2b-8115-af59e1c592a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5115c853-e239-4f34-ba1c-540380373e03",
                    "LayerId": "2be5c0c0-f470-4349-96b0-3334f28085ce"
                }
            ]
        },
        {
            "id": "63885821-2543-4930-a9df-51b0ee4c684d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "compositeImage": {
                "id": "4bbf28ed-f698-4226-a071-4a46384cfc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63885821-2543-4930-a9df-51b0ee4c684d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75dbad3d-984d-4512-917d-cdc0fa456b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63885821-2543-4930-a9df-51b0ee4c684d",
                    "LayerId": "8350038e-0071-4f83-9468-c61d0898f1c5"
                },
                {
                    "id": "3a657aef-3afd-4ad3-a621-168d4a7d48ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63885821-2543-4930-a9df-51b0ee4c684d",
                    "LayerId": "2be5c0c0-f470-4349-96b0-3334f28085ce"
                }
            ]
        },
        {
            "id": "58f25f62-2252-4893-9a39-f6cb526e42d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "compositeImage": {
                "id": "99762728-100e-4caa-be38-242244c98b36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f25f62-2252-4893-9a39-f6cb526e42d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20777668-af2b-4961-bf2c-c8043a9ac94f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f25f62-2252-4893-9a39-f6cb526e42d3",
                    "LayerId": "8350038e-0071-4f83-9468-c61d0898f1c5"
                },
                {
                    "id": "1bee356f-9d0d-4998-b365-798bcae4f17b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f25f62-2252-4893-9a39-f6cb526e42d3",
                    "LayerId": "2be5c0c0-f470-4349-96b0-3334f28085ce"
                }
            ]
        },
        {
            "id": "96b6983f-8a1e-4b77-b8c3-47ecd5e9d9f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "compositeImage": {
                "id": "5b5d85fb-a556-40dd-9201-10d68a68746d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b6983f-8a1e-4b77-b8c3-47ecd5e9d9f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40cbde36-3ca2-40e2-afca-a4ba9f40f03e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b6983f-8a1e-4b77-b8c3-47ecd5e9d9f7",
                    "LayerId": "8350038e-0071-4f83-9468-c61d0898f1c5"
                },
                {
                    "id": "fcc4ca2a-cdcf-4ddd-9c76-0964629b9c74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b6983f-8a1e-4b77-b8c3-47ecd5e9d9f7",
                    "LayerId": "2be5c0c0-f470-4349-96b0-3334f28085ce"
                }
            ]
        },
        {
            "id": "6f060bde-4c02-490e-bb12-afe949d6c42a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "compositeImage": {
                "id": "5aafee3c-e61b-4284-95b4-c064494ac055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f060bde-4c02-490e-bb12-afe949d6c42a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24491d48-c826-46d5-85e1-098961e7308a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f060bde-4c02-490e-bb12-afe949d6c42a",
                    "LayerId": "8350038e-0071-4f83-9468-c61d0898f1c5"
                },
                {
                    "id": "f47fe763-567a-4ac3-b568-51e968caa01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f060bde-4c02-490e-bb12-afe949d6c42a",
                    "LayerId": "2be5c0c0-f470-4349-96b0-3334f28085ce"
                }
            ]
        },
        {
            "id": "a315eeaa-71c0-42fd-aa41-a41400dfe3bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "compositeImage": {
                "id": "2af38f7e-b4b4-45d1-8299-8e68ebaf1586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a315eeaa-71c0-42fd-aa41-a41400dfe3bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b5ec2ae-be51-4ab8-a26d-2d53b546293a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a315eeaa-71c0-42fd-aa41-a41400dfe3bd",
                    "LayerId": "8350038e-0071-4f83-9468-c61d0898f1c5"
                },
                {
                    "id": "158a5ad4-5743-43e1-a59b-3f517c7267df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a315eeaa-71c0-42fd-aa41-a41400dfe3bd",
                    "LayerId": "2be5c0c0-f470-4349-96b0-3334f28085ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8350038e-0071-4f83-9468-c61d0898f1c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2be5c0c0-f470-4349-96b0-3334f28085ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3eeb1a1-4708-4805-81d7-427031200be7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}