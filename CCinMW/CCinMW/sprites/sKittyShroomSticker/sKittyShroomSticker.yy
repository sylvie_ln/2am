{
    "id": "41d91330-e02f-44ba-a42c-026bf20b7de1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittyShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41e16a71-4c17-4b90-ade4-a77f60e79e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d91330-e02f-44ba-a42c-026bf20b7de1",
            "compositeImage": {
                "id": "76cb40a3-abfe-42d3-8a93-6a5d0d98278c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41e16a71-4c17-4b90-ade4-a77f60e79e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da57e12c-14c1-4880-86a6-bd13e48c5235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41e16a71-4c17-4b90-ade4-a77f60e79e1e",
                    "LayerId": "372b8327-eeff-4fdf-ae80-04bcd617d97e"
                }
            ]
        },
        {
            "id": "580ce8bb-5775-45f7-8f6d-13c17adb7c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d91330-e02f-44ba-a42c-026bf20b7de1",
            "compositeImage": {
                "id": "ab856b40-1f85-4733-b7e2-b05e26e82a7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "580ce8bb-5775-45f7-8f6d-13c17adb7c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7647cf-a323-4c3f-bb25-ea185f3c561a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "580ce8bb-5775-45f7-8f6d-13c17adb7c4b",
                    "LayerId": "372b8327-eeff-4fdf-ae80-04bcd617d97e"
                }
            ]
        },
        {
            "id": "7aebb802-d9e8-4f7a-a7a3-2c0139e49df0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d91330-e02f-44ba-a42c-026bf20b7de1",
            "compositeImage": {
                "id": "029f0a2f-9f4b-4a8e-a336-6e03f55bc6fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aebb802-d9e8-4f7a-a7a3-2c0139e49df0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d98e56-c75c-423a-82d8-f23577fa5337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aebb802-d9e8-4f7a-a7a3-2c0139e49df0",
                    "LayerId": "372b8327-eeff-4fdf-ae80-04bcd617d97e"
                }
            ]
        },
        {
            "id": "35d9b9da-fd09-48d1-a37c-4473d6dae493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d91330-e02f-44ba-a42c-026bf20b7de1",
            "compositeImage": {
                "id": "593c2f37-0187-42f7-a428-2679834280f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d9b9da-fd09-48d1-a37c-4473d6dae493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe9047a7-53a5-41c8-ad11-6e069b5d8e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d9b9da-fd09-48d1-a37c-4473d6dae493",
                    "LayerId": "372b8327-eeff-4fdf-ae80-04bcd617d97e"
                }
            ]
        },
        {
            "id": "f9e532d2-5e94-4a3c-bc1a-4505e535a75a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d91330-e02f-44ba-a42c-026bf20b7de1",
            "compositeImage": {
                "id": "e411c2cc-619e-4ce2-98cf-c670ef0ea8e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9e532d2-5e94-4a3c-bc1a-4505e535a75a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aac894a-9515-45a7-86bd-fac5401fffd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9e532d2-5e94-4a3c-bc1a-4505e535a75a",
                    "LayerId": "372b8327-eeff-4fdf-ae80-04bcd617d97e"
                }
            ]
        },
        {
            "id": "249a5fe4-5df1-44dd-a842-4025fc9480df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d91330-e02f-44ba-a42c-026bf20b7de1",
            "compositeImage": {
                "id": "401b73af-d1fa-42aa-94f7-b174d61bffe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "249a5fe4-5df1-44dd-a842-4025fc9480df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb04093-958f-4331-87e0-43b06d324992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "249a5fe4-5df1-44dd-a842-4025fc9480df",
                    "LayerId": "372b8327-eeff-4fdf-ae80-04bcd617d97e"
                }
            ]
        },
        {
            "id": "675c33ec-6ff3-45b6-8b51-3f0422b40f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d91330-e02f-44ba-a42c-026bf20b7de1",
            "compositeImage": {
                "id": "3e717241-5c73-4968-bacb-7e439f7b6aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "675c33ec-6ff3-45b6-8b51-3f0422b40f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbbe89c1-a5f3-41bd-9fbb-0d6413252886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "675c33ec-6ff3-45b6-8b51-3f0422b40f50",
                    "LayerId": "372b8327-eeff-4fdf-ae80-04bcd617d97e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "372b8327-eeff-4fdf-ae80-04bcd617d97e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41d91330-e02f-44ba-a42c-026bf20b7de1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}