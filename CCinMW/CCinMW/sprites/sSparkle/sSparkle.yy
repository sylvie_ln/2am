{
    "id": "f81c8118-729f-493c-9426-340f078cd9cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36862a53-b780-4125-bfc6-a33fae52e7a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81c8118-729f-493c-9426-340f078cd9cc",
            "compositeImage": {
                "id": "c697bfc3-7030-41d2-b825-c4915121a6e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36862a53-b780-4125-bfc6-a33fae52e7a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f51a648-218a-4417-a2fc-610e24113220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36862a53-b780-4125-bfc6-a33fae52e7a8",
                    "LayerId": "6cf4af97-0bea-4c92-b0ed-b6e1c866f3d7"
                }
            ]
        },
        {
            "id": "2f93b708-c375-4b50-a835-22c7ff241457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81c8118-729f-493c-9426-340f078cd9cc",
            "compositeImage": {
                "id": "95da987e-2e2f-4a93-b222-75fce7fa098f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f93b708-c375-4b50-a835-22c7ff241457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74af462b-7948-4bf0-9957-8e66b26d04dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f93b708-c375-4b50-a835-22c7ff241457",
                    "LayerId": "6cf4af97-0bea-4c92-b0ed-b6e1c866f3d7"
                }
            ]
        },
        {
            "id": "d3ab572a-d47a-491e-bccb-15a49048fcf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81c8118-729f-493c-9426-340f078cd9cc",
            "compositeImage": {
                "id": "4507bef6-3954-4df1-b34e-0c79d7ab74ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ab572a-d47a-491e-bccb-15a49048fcf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3e085ee-afa1-4b20-af70-bb512028bec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ab572a-d47a-491e-bccb-15a49048fcf8",
                    "LayerId": "6cf4af97-0bea-4c92-b0ed-b6e1c866f3d7"
                }
            ]
        },
        {
            "id": "679b6bc0-9614-4e25-be09-018c85afc2f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81c8118-729f-493c-9426-340f078cd9cc",
            "compositeImage": {
                "id": "206941de-e5f9-42a2-bb07-e53e61f29365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "679b6bc0-9614-4e25-be09-018c85afc2f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474913fb-1c60-48f0-94e4-4c68f8776bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "679b6bc0-9614-4e25-be09-018c85afc2f5",
                    "LayerId": "6cf4af97-0bea-4c92-b0ed-b6e1c866f3d7"
                }
            ]
        },
        {
            "id": "d5aa6044-94b2-434a-8ea1-4317cea56912",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81c8118-729f-493c-9426-340f078cd9cc",
            "compositeImage": {
                "id": "ecdc64cd-0487-41d6-a273-b206e7bd1e0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5aa6044-94b2-434a-8ea1-4317cea56912",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64d23ebe-defd-4e58-9ba6-4b1a98f08fe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5aa6044-94b2-434a-8ea1-4317cea56912",
                    "LayerId": "6cf4af97-0bea-4c92-b0ed-b6e1c866f3d7"
                }
            ]
        },
        {
            "id": "24a2f0a5-3634-480c-9841-a64b7f3ae856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81c8118-729f-493c-9426-340f078cd9cc",
            "compositeImage": {
                "id": "29fc0947-b5da-4b39-9836-831ef67675d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a2f0a5-3634-480c-9841-a64b7f3ae856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d452e6e3-8b5c-4cf3-9684-4e1e01fc945d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a2f0a5-3634-480c-9841-a64b7f3ae856",
                    "LayerId": "6cf4af97-0bea-4c92-b0ed-b6e1c866f3d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "6cf4af97-0bea-4c92-b0ed-b6e1c866f3d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f81c8118-729f-493c-9426-340f078cd9cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}