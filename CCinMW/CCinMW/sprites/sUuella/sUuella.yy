{
    "id": "fcaf3013-82ce-492a-92ff-7625e104572e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUuella",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e06bcde4-bd06-4183-ac8b-66a5dda0eda3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcaf3013-82ce-492a-92ff-7625e104572e",
            "compositeImage": {
                "id": "c5483451-cec7-4797-83f2-fb434f297fb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e06bcde4-bd06-4183-ac8b-66a5dda0eda3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2060650-a03c-44b4-90c9-5b507705d7dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e06bcde4-bd06-4183-ac8b-66a5dda0eda3",
                    "LayerId": "1b66c00c-77d8-4499-9088-e20f4bb2315d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1b66c00c-77d8-4499-9088-e20f4bb2315d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcaf3013-82ce-492a-92ff-7625e104572e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}