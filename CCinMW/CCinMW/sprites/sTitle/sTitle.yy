{
    "id": "4c8e93c8-6488-4908-84cc-57c35dcad176",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6e70ef9-ec86-46dd-8e09-1403b6e88cbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c8e93c8-6488-4908-84cc-57c35dcad176",
            "compositeImage": {
                "id": "c71ca841-8f98-4d6c-9697-83e002c6c3f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e70ef9-ec86-46dd-8e09-1403b6e88cbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c485a749-d348-42e2-915b-9227e9ba3db4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e70ef9-ec86-46dd-8e09-1403b6e88cbf",
                    "LayerId": "f6aa6b76-a261-47d0-a6a2-932671def0e9"
                },
                {
                    "id": "e8eb3b9b-7851-47a3-aaf4-19cf1ccba0ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e70ef9-ec86-46dd-8e09-1403b6e88cbf",
                    "LayerId": "bd33b16b-60ea-4462-b27a-53293c17325c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "f6aa6b76-a261-47d0-a6a2-932671def0e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c8e93c8-6488-4908-84cc-57c35dcad176",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bd33b16b-60ea-4462-b27a-53293c17325c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c8e93c8-6488-4908-84cc-57c35dcad176",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}