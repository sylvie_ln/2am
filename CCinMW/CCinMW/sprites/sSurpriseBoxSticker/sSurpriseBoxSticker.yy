{
    "id": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSurpriseBoxSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c41e3bb-01ef-4362-83a0-95a02b40c425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "compositeImage": {
                "id": "ce9ff540-6a30-4d75-a3e4-a0820616a21c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c41e3bb-01ef-4362-83a0-95a02b40c425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "305ebb4d-f7fd-48ba-aed4-4a867b5f333d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c41e3bb-01ef-4362-83a0-95a02b40c425",
                    "LayerId": "47ae8ea2-ed79-4417-8f34-5e067e697dce"
                },
                {
                    "id": "523ed38d-2d9c-4b08-99a8-5b07857360aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c41e3bb-01ef-4362-83a0-95a02b40c425",
                    "LayerId": "6600cc6d-091e-45ca-957a-d906739caa03"
                }
            ]
        },
        {
            "id": "e014f4bf-da5b-4629-ae1b-ca4921738e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "compositeImage": {
                "id": "5b2d8946-7e4e-40bf-9791-b147d35abf7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e014f4bf-da5b-4629-ae1b-ca4921738e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b850a419-4663-4ad0-9b66-428a5a62f4d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e014f4bf-da5b-4629-ae1b-ca4921738e31",
                    "LayerId": "47ae8ea2-ed79-4417-8f34-5e067e697dce"
                },
                {
                    "id": "8615e4ef-7d4e-4f47-bc06-1a17e91f853c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e014f4bf-da5b-4629-ae1b-ca4921738e31",
                    "LayerId": "6600cc6d-091e-45ca-957a-d906739caa03"
                }
            ]
        },
        {
            "id": "42039713-8529-4d43-b819-0cb7881f3398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "compositeImage": {
                "id": "5a1911e6-6cb0-41b2-a8e8-18e85efc7365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42039713-8529-4d43-b819-0cb7881f3398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d308d0e8-7c4e-473b-bcc9-04b6be44a3ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42039713-8529-4d43-b819-0cb7881f3398",
                    "LayerId": "47ae8ea2-ed79-4417-8f34-5e067e697dce"
                },
                {
                    "id": "de2c7b4f-b271-4763-9a6c-7884df2734e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42039713-8529-4d43-b819-0cb7881f3398",
                    "LayerId": "6600cc6d-091e-45ca-957a-d906739caa03"
                }
            ]
        },
        {
            "id": "9ce434f5-a9ad-4c1c-9fcb-223cc300020a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "compositeImage": {
                "id": "0464404b-2164-4f91-a310-c14aacdc772a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ce434f5-a9ad-4c1c-9fcb-223cc300020a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51f2a949-94f8-4711-a267-1a5ea7efa076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ce434f5-a9ad-4c1c-9fcb-223cc300020a",
                    "LayerId": "47ae8ea2-ed79-4417-8f34-5e067e697dce"
                },
                {
                    "id": "b8d163ec-3630-400a-a9a0-38a8b4be785a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ce434f5-a9ad-4c1c-9fcb-223cc300020a",
                    "LayerId": "6600cc6d-091e-45ca-957a-d906739caa03"
                }
            ]
        },
        {
            "id": "54438394-7cad-4fcc-8745-5de5117c03e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "compositeImage": {
                "id": "1e7182e5-fe32-4507-b865-2b4b13ed82e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54438394-7cad-4fcc-8745-5de5117c03e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56013e14-88f0-4802-867e-3b87e0d08e52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54438394-7cad-4fcc-8745-5de5117c03e4",
                    "LayerId": "47ae8ea2-ed79-4417-8f34-5e067e697dce"
                },
                {
                    "id": "e8399127-7ce8-448c-b144-45f2051b01c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54438394-7cad-4fcc-8745-5de5117c03e4",
                    "LayerId": "6600cc6d-091e-45ca-957a-d906739caa03"
                }
            ]
        },
        {
            "id": "dcdcf598-c9d6-463a-a285-21c5ccc5daa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "compositeImage": {
                "id": "b6b12b65-d1a7-46cf-907f-b29c3a53c65b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcdcf598-c9d6-463a-a285-21c5ccc5daa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d6c41f-797f-4442-a590-84d1a0d70fda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcdcf598-c9d6-463a-a285-21c5ccc5daa7",
                    "LayerId": "47ae8ea2-ed79-4417-8f34-5e067e697dce"
                },
                {
                    "id": "558ef0dc-b9e2-4384-991c-8215cd8788c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcdcf598-c9d6-463a-a285-21c5ccc5daa7",
                    "LayerId": "6600cc6d-091e-45ca-957a-d906739caa03"
                }
            ]
        },
        {
            "id": "e863c0b2-5d79-4c3f-ba3c-adf78aab9637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "compositeImage": {
                "id": "fc13408a-a38a-4574-86da-b4c970dbd281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e863c0b2-5d79-4c3f-ba3c-adf78aab9637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e149cbf-ef3e-4878-821b-8434563caaee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e863c0b2-5d79-4c3f-ba3c-adf78aab9637",
                    "LayerId": "47ae8ea2-ed79-4417-8f34-5e067e697dce"
                },
                {
                    "id": "760c7775-c518-432c-b9e3-6cc48972d27a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e863c0b2-5d79-4c3f-ba3c-adf78aab9637",
                    "LayerId": "6600cc6d-091e-45ca-957a-d906739caa03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "47ae8ea2-ed79-4417-8f34-5e067e697dce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 90,
            "visible": true
        },
        {
            "id": "6600cc6d-091e-45ca-957a-d906739caa03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09a3901e-41de-4fee-8a0b-e355cde1be7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}