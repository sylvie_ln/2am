{
    "id": "a4edcdc0-d32c-4e7a-904e-f820b29b8f0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLensOfLiesSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88f1b3ea-dd7e-4154-9e6b-fc03dfe31511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4edcdc0-d32c-4e7a-904e-f820b29b8f0f",
            "compositeImage": {
                "id": "20660ebc-559a-4b47-8ce3-980933c6969d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f1b3ea-dd7e-4154-9e6b-fc03dfe31511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba6cdb36-d06f-4d7d-81f4-727a3ca3307b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f1b3ea-dd7e-4154-9e6b-fc03dfe31511",
                    "LayerId": "abe5042b-904c-403a-9e58-8eab3d492685"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "abe5042b-904c-403a-9e58-8eab3d492685",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4edcdc0-d32c-4e7a-904e-f820b29b8f0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}