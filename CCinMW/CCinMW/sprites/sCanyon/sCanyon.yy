{
    "id": "0d36dd09-9896-42ee-b8fd-13d784827278",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanyon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2cae9e96-ccea-451c-b23c-f118a0a60717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d36dd09-9896-42ee-b8fd-13d784827278",
            "compositeImage": {
                "id": "7d7fe607-2228-4611-b97b-25d17954d030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cae9e96-ccea-451c-b23c-f118a0a60717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bfa99a1-6553-48f1-b651-9f5b338c7d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cae9e96-ccea-451c-b23c-f118a0a60717",
                    "LayerId": "6c2572b0-4d5d-4a1e-a5ab-54212738bb83"
                }
            ]
        },
        {
            "id": "f53440e1-340c-43c3-b53c-2af9139c055c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d36dd09-9896-42ee-b8fd-13d784827278",
            "compositeImage": {
                "id": "26c6c686-70a7-4218-9243-2f86c00185ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53440e1-340c-43c3-b53c-2af9139c055c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece3ecdc-93e0-4219-861f-e2319ee42929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53440e1-340c-43c3-b53c-2af9139c055c",
                    "LayerId": "6c2572b0-4d5d-4a1e-a5ab-54212738bb83"
                }
            ]
        },
        {
            "id": "573fffa4-1a3b-4f7e-a6a3-3be9560b785d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d36dd09-9896-42ee-b8fd-13d784827278",
            "compositeImage": {
                "id": "f9fcb3d0-a013-4ad3-a0a4-1ff3eb9ea20b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573fffa4-1a3b-4f7e-a6a3-3be9560b785d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6dab9df-3525-44c6-b137-e03e565ead42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573fffa4-1a3b-4f7e-a6a3-3be9560b785d",
                    "LayerId": "6c2572b0-4d5d-4a1e-a5ab-54212738bb83"
                }
            ]
        },
        {
            "id": "d5b6b5c2-ce1b-4e30-a28a-82428b1202f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d36dd09-9896-42ee-b8fd-13d784827278",
            "compositeImage": {
                "id": "bd739873-d297-477b-9f7c-95bfeb9f3413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5b6b5c2-ce1b-4e30-a28a-82428b1202f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c6b3c0-64b9-40b2-b136-6683dcfd1d5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5b6b5c2-ce1b-4e30-a28a-82428b1202f8",
                    "LayerId": "6c2572b0-4d5d-4a1e-a5ab-54212738bb83"
                }
            ]
        },
        {
            "id": "62584fea-06c4-4a8b-8862-b4119087f5f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d36dd09-9896-42ee-b8fd-13d784827278",
            "compositeImage": {
                "id": "eb43c877-d724-430d-87bb-104be40555b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62584fea-06c4-4a8b-8862-b4119087f5f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45deea8e-3ea3-4c03-8e3b-92684d672a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62584fea-06c4-4a8b-8862-b4119087f5f3",
                    "LayerId": "6c2572b0-4d5d-4a1e-a5ab-54212738bb83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6c2572b0-4d5d-4a1e-a5ab-54212738bb83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d36dd09-9896-42ee-b8fd-13d784827278",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}