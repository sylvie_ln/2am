{
    "id": "45de6582-6bb5-4cdd-88ba-f8cfbbceb3ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPrisson",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 8,
    "bbox_right": 110,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3e5e829-92af-45f3-a719-24a6c79a2742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45de6582-6bb5-4cdd-88ba-f8cfbbceb3ff",
            "compositeImage": {
                "id": "6c943557-cef7-4fe2-9674-ac71600337af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3e5e829-92af-45f3-a719-24a6c79a2742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "575dbef1-0d01-4bb4-a6c1-fdf646d0647f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3e5e829-92af-45f3-a719-24a6c79a2742",
                    "LayerId": "3f8c6135-831b-4dbc-af5c-cc1324c5cde6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "3f8c6135-831b-4dbc-af5c-cc1324c5cde6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45de6582-6bb5-4cdd-88ba-f8cfbbceb3ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 25
}