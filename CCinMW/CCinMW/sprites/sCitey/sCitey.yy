{
    "id": "badb860f-363b-4cd4-b488-0e66bf80c2f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCitey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bde654a-c9dc-4c16-9233-7469225511ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "badb860f-363b-4cd4-b488-0e66bf80c2f1",
            "compositeImage": {
                "id": "9be0fa01-5e70-4297-be89-5c8dbf95e5fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bde654a-c9dc-4c16-9233-7469225511ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85f9c6da-28f5-4d30-8ad6-d671193e9f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bde654a-c9dc-4c16-9233-7469225511ec",
                    "LayerId": "a5da2c8d-fa8e-4b64-8ac3-184ae3358a5d"
                },
                {
                    "id": "422e8bc8-5844-40ba-b4d8-b5d24977c088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bde654a-c9dc-4c16-9233-7469225511ec",
                    "LayerId": "ccc90c29-2043-49bf-a75c-468ac2402e66"
                }
            ]
        },
        {
            "id": "996fef11-0259-4e7a-a707-b5e9dc15a0cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "badb860f-363b-4cd4-b488-0e66bf80c2f1",
            "compositeImage": {
                "id": "42a3efcc-43fd-4f44-902b-bba1dec5dc17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "996fef11-0259-4e7a-a707-b5e9dc15a0cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72128c4a-fda0-44bc-97a7-744f70f60047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "996fef11-0259-4e7a-a707-b5e9dc15a0cf",
                    "LayerId": "a5da2c8d-fa8e-4b64-8ac3-184ae3358a5d"
                },
                {
                    "id": "03d66ad4-63e0-40e7-a4ba-650b86e44ea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "996fef11-0259-4e7a-a707-b5e9dc15a0cf",
                    "LayerId": "ccc90c29-2043-49bf-a75c-468ac2402e66"
                }
            ]
        },
        {
            "id": "5a81465a-2ddc-44ce-9486-ce2dd989a5fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "badb860f-363b-4cd4-b488-0e66bf80c2f1",
            "compositeImage": {
                "id": "1f061511-e752-4d13-affe-1041fc938cda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a81465a-2ddc-44ce-9486-ce2dd989a5fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "889dfc37-d7c5-43d9-96c8-b5b20289ed19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a81465a-2ddc-44ce-9486-ce2dd989a5fb",
                    "LayerId": "a5da2c8d-fa8e-4b64-8ac3-184ae3358a5d"
                },
                {
                    "id": "ae10c5f2-1a14-4b44-a3d3-40ba79b828e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a81465a-2ddc-44ce-9486-ce2dd989a5fb",
                    "LayerId": "ccc90c29-2043-49bf-a75c-468ac2402e66"
                }
            ]
        },
        {
            "id": "98e949d6-06a6-452b-b0bb-78eb8a3b7a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "badb860f-363b-4cd4-b488-0e66bf80c2f1",
            "compositeImage": {
                "id": "5ba74cab-4a52-42fb-9664-8c97ac60aca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98e949d6-06a6-452b-b0bb-78eb8a3b7a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bc14eb9-839c-4f40-899c-5b6bfb78839b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98e949d6-06a6-452b-b0bb-78eb8a3b7a3c",
                    "LayerId": "a5da2c8d-fa8e-4b64-8ac3-184ae3358a5d"
                },
                {
                    "id": "b28e2710-e812-4929-b234-3fc46ab4ebc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98e949d6-06a6-452b-b0bb-78eb8a3b7a3c",
                    "LayerId": "ccc90c29-2043-49bf-a75c-468ac2402e66"
                }
            ]
        },
        {
            "id": "0bd03dbe-7c57-4a3f-9c24-b4cf7a87e882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "badb860f-363b-4cd4-b488-0e66bf80c2f1",
            "compositeImage": {
                "id": "12cd1f8e-a599-42b3-98c8-8aefd96d3167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd03dbe-7c57-4a3f-9c24-b4cf7a87e882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c776d438-aad6-461d-9ff0-d7bef13b0a09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd03dbe-7c57-4a3f-9c24-b4cf7a87e882",
                    "LayerId": "a5da2c8d-fa8e-4b64-8ac3-184ae3358a5d"
                },
                {
                    "id": "2aed9580-e6d3-4d22-a653-4227076af2ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd03dbe-7c57-4a3f-9c24-b4cf7a87e882",
                    "LayerId": "ccc90c29-2043-49bf-a75c-468ac2402e66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ccc90c29-2043-49bf-a75c-468ac2402e66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "badb860f-363b-4cd4-b488-0e66bf80c2f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 25,
            "visible": true
        },
        {
            "id": "a5da2c8d-fa8e-4b64-8ac3-184ae3358a5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "badb860f-363b-4cd4-b488-0e66bf80c2f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        1090519039,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4284768238,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}