{
    "id": "8103d3cb-253a-4d7e-9eae-26747224cddf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHandsBigg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "208f7f3b-680e-42d2-8d82-43b1eb714722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "d3109eeb-efd5-410a-8a32-823b91224fd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208f7f3b-680e-42d2-8d82-43b1eb714722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16562da4-2289-4634-ad90-597ac9f504ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208f7f3b-680e-42d2-8d82-43b1eb714722",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "f21906a9-51bc-4d7a-9175-5e2b7acb7fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "ebfb921d-d751-4f5e-a5d9-892640f08c92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f21906a9-51bc-4d7a-9175-5e2b7acb7fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9f9f91-b99d-4911-86d6-1b427fa60628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f21906a9-51bc-4d7a-9175-5e2b7acb7fcd",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "a00e1a77-2dcd-4a1f-b3b9-4083de93e5d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "07ec8aeb-1e20-473b-a6d8-281fd615a737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00e1a77-2dcd-4a1f-b3b9-4083de93e5d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a41579dc-5698-4b99-b8f7-ae39a4bb6b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00e1a77-2dcd-4a1f-b3b9-4083de93e5d5",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "44bb8018-bf43-4a90-afcc-76c65ce79b65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "7517a3ba-ddd2-41ed-b1d1-fa8004285dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44bb8018-bf43-4a90-afcc-76c65ce79b65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdd73a65-9ba4-4854-94c1-a5339f69f7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44bb8018-bf43-4a90-afcc-76c65ce79b65",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "7c85259e-30ad-4775-8137-1cbf38d01eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "91d7516d-2e14-40bb-a0a3-b49356c1b590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c85259e-30ad-4775-8137-1cbf38d01eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d950b68-9899-4122-adde-531a73b0993a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c85259e-30ad-4775-8137-1cbf38d01eba",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "3fc3a1cd-4bfd-46fa-9652-0b650d887351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "b4975781-da36-4859-821d-6ec96de82083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc3a1cd-4bfd-46fa-9652-0b650d887351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9933ef80-fb93-4651-ae12-f10def2118ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc3a1cd-4bfd-46fa-9652-0b650d887351",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "8a68222a-dd70-44c3-bd18-34746c7f6b33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "11ec98f0-1527-4cde-8780-d20ac2e8cbc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a68222a-dd70-44c3-bd18-34746c7f6b33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767a721e-9590-466c-8cdc-4120653db7f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a68222a-dd70-44c3-bd18-34746c7f6b33",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "81eefc9d-4e8b-46be-953f-934c9d29dd1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "7a5ba9d6-a41e-41c0-83cf-eb99539fc19d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81eefc9d-4e8b-46be-953f-934c9d29dd1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae26822-c265-4a88-a003-0c00f82fcbbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81eefc9d-4e8b-46be-953f-934c9d29dd1d",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "f45d4575-3a61-4a2f-b912-78cf8a9f45eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "3f2e489d-fe23-465e-b545-9db9b937d665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f45d4575-3a61-4a2f-b912-78cf8a9f45eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7183f746-42b2-40ea-92cd-c713c186efc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f45d4575-3a61-4a2f-b912-78cf8a9f45eb",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "e8c62548-c5a2-441f-be4c-8eedd37cb069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "b2bcedef-f7ac-4f62-863f-b27ce96fc416",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8c62548-c5a2-441f-be4c-8eedd37cb069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33031cfd-904f-4e67-8392-5090dbe3846f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c62548-c5a2-441f-be4c-8eedd37cb069",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "ad8154d0-7f6e-4d19-a43f-2c9c05112ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "5aaa4822-96cd-47ca-af57-5609566a1ccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad8154d0-7f6e-4d19-a43f-2c9c05112ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c384cf0-63f2-43a4-a67f-4d7e406661ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8154d0-7f6e-4d19-a43f-2c9c05112ee7",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        },
        {
            "id": "e244ead1-d99b-4bbb-a4b8-0ad8206cdf82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "compositeImage": {
                "id": "6cf857b3-097c-4606-8c65-0a874546b895",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e244ead1-d99b-4bbb-a4b8-0ad8206cdf82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e936efe7-68f2-4653-b631-90411d968553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e244ead1-d99b-4bbb-a4b8-0ad8206cdf82",
                    "LayerId": "9f958cb5-ae4c-480e-b0ff-673b060a7cee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "9f958cb5-ae4c-480e-b0ff-673b060a7cee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8103d3cb-253a-4d7e-9eae-26747224cddf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4294901889,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 38
}