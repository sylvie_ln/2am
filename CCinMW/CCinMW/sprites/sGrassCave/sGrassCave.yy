{
    "id": "08b0d9e2-36ab-47e4-a9e1-a1660154fd46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrassCave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dceb2608-7bce-4464-bd59-1db8b55bcc04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08b0d9e2-36ab-47e4-a9e1-a1660154fd46",
            "compositeImage": {
                "id": "d427718b-2fff-49ce-8c32-90023f920956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dceb2608-7bce-4464-bd59-1db8b55bcc04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "665aa77b-1b7d-4715-b2d0-ca480e4d0190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dceb2608-7bce-4464-bd59-1db8b55bcc04",
                    "LayerId": "813fe071-ef6d-4c69-a9ac-4c04a9461f9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "813fe071-ef6d-4c69-a9ac-4c04a9461f9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08b0d9e2-36ab-47e4-a9e1-a1660154fd46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}