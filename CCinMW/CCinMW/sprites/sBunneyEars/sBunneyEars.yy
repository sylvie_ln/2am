{
    "id": "c56cc4da-2955-45ed-9152-f974fb08452d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyEars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f4b2a41-99b9-46e7-86bc-6cce01927bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c56cc4da-2955-45ed-9152-f974fb08452d",
            "compositeImage": {
                "id": "66c605fc-387b-4191-80eb-59939b65297d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f4b2a41-99b9-46e7-86bc-6cce01927bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21515c58-ffb8-40b6-95cf-5df4a3df9eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f4b2a41-99b9-46e7-86bc-6cce01927bc7",
                    "LayerId": "554da6aa-524b-4711-8b9a-6e8ac2df8747"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "554da6aa-524b-4711-8b9a-6e8ac2df8747",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c56cc4da-2955-45ed-9152-f974fb08452d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}