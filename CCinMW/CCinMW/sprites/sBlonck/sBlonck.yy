{
    "id": "c403f000-51c7-489a-a516-a8fee8b8eee4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlonck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f324834-8e76-4293-a230-2b2983fec32f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c403f000-51c7-489a-a516-a8fee8b8eee4",
            "compositeImage": {
                "id": "70d8b343-fd7a-4234-94a7-25bc8db82032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f324834-8e76-4293-a230-2b2983fec32f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d022c534-8f70-442f-8f6d-34a50d525ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f324834-8e76-4293-a230-2b2983fec32f",
                    "LayerId": "f421b61e-03fc-463d-9aae-59fcfa575e3e"
                }
            ]
        },
        {
            "id": "e8d76752-c72e-4df0-9896-926d59d234d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c403f000-51c7-489a-a516-a8fee8b8eee4",
            "compositeImage": {
                "id": "b11f76c0-9baa-437d-8fc9-dfca5b271b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d76752-c72e-4df0-9896-926d59d234d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a11977aa-b03b-453c-8dff-246512e330cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d76752-c72e-4df0-9896-926d59d234d8",
                    "LayerId": "f421b61e-03fc-463d-9aae-59fcfa575e3e"
                }
            ]
        },
        {
            "id": "18c5ecec-a7bb-4358-a4e7-eaf6c1f53854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c403f000-51c7-489a-a516-a8fee8b8eee4",
            "compositeImage": {
                "id": "40ca3262-f3a0-47ec-89ee-05c9e75856db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18c5ecec-a7bb-4358-a4e7-eaf6c1f53854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7baf25b-fd9b-43f3-8238-00d982f15d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18c5ecec-a7bb-4358-a4e7-eaf6c1f53854",
                    "LayerId": "f421b61e-03fc-463d-9aae-59fcfa575e3e"
                }
            ]
        },
        {
            "id": "f0c27cf7-63b9-4177-9bde-3e79b09c586b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c403f000-51c7-489a-a516-a8fee8b8eee4",
            "compositeImage": {
                "id": "d402a02f-17a6-4e7f-ad2c-d98db382d4e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c27cf7-63b9-4177-9bde-3e79b09c586b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "970ee460-eb49-4362-a4e7-66fa72dc4ed6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c27cf7-63b9-4177-9bde-3e79b09c586b",
                    "LayerId": "f421b61e-03fc-463d-9aae-59fcfa575e3e"
                }
            ]
        },
        {
            "id": "4016b48b-0b6b-429d-ac9a-efe79fbe09fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c403f000-51c7-489a-a516-a8fee8b8eee4",
            "compositeImage": {
                "id": "5216aa20-373a-48ee-96ed-0697fa495cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4016b48b-0b6b-429d-ac9a-efe79fbe09fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d8be9a0-1e95-4f8b-942e-4ed6be8dfc7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4016b48b-0b6b-429d-ac9a-efe79fbe09fa",
                    "LayerId": "f421b61e-03fc-463d-9aae-59fcfa575e3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "f421b61e-03fc-463d-9aae-59fcfa575e3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c403f000-51c7-489a-a516-a8fee8b8eee4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}