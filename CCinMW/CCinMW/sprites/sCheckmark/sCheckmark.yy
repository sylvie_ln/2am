{
    "id": "d1fd05ba-cba2-4f67-b252-ea7899732f33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCheckmark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 4,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "573ee5eb-89f4-4239-b442-295c7b15ae39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1fd05ba-cba2-4f67-b252-ea7899732f33",
            "compositeImage": {
                "id": "a2416eaf-3b98-4cd1-a6ae-d562ae2036c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573ee5eb-89f4-4239-b442-295c7b15ae39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15093611-0668-423c-8e0a-bcbd3de715ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573ee5eb-89f4-4239-b442-295c7b15ae39",
                    "LayerId": "e1d585de-5593-42ea-a620-5c4313bd8f8d"
                },
                {
                    "id": "86c82a52-446f-425a-8eae-3aa1312cf6bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573ee5eb-89f4-4239-b442-295c7b15ae39",
                    "LayerId": "7e4bb9ff-955e-4720-b025-c3518e0fb7d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e1d585de-5593-42ea-a620-5c4313bd8f8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1fd05ba-cba2-4f67-b252-ea7899732f33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7e4bb9ff-955e-4720-b025-c3518e0fb7d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1fd05ba-cba2-4f67-b252-ea7899732f33",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4286400494,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}