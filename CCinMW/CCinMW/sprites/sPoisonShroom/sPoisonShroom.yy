{
    "id": "253507f4-0e3a-4d42-9013-77ac79eac4b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPoisonShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60fad233-f644-4991-a6af-f21841a93b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "253507f4-0e3a-4d42-9013-77ac79eac4b6",
            "compositeImage": {
                "id": "962972e5-c79f-428a-972a-e05eedeaa654",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60fad233-f644-4991-a6af-f21841a93b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "671788a5-4a1f-46e7-bc0e-d262383839fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60fad233-f644-4991-a6af-f21841a93b38",
                    "LayerId": "11d8d3fa-4e48-4aa8-85a8-07688843f395"
                },
                {
                    "id": "86a5fe56-492f-4870-8b4d-f971f4617af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60fad233-f644-4991-a6af-f21841a93b38",
                    "LayerId": "52cc55d0-9eca-4689-b656-79890e21120c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "52cc55d0-9eca-4689-b656-79890e21120c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "253507f4-0e3a-4d42-9013-77ac79eac4b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "11d8d3fa-4e48-4aa8-85a8-07688843f395",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "253507f4-0e3a-4d42-9013-77ac79eac4b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}