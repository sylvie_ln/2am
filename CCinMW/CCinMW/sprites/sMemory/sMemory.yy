{
    "id": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMemory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af717af4-b45c-4ce2-8240-b56c5fbef686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "566b4af2-23a8-45ea-90c6-b29ea1d04072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af717af4-b45c-4ce2-8240-b56c5fbef686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0b98a51-1e9e-43ae-9aec-13fdf1d1b2d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af717af4-b45c-4ce2-8240-b56c5fbef686",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "baec3ec8-f428-40af-afcd-767f181f9f00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af717af4-b45c-4ce2-8240-b56c5fbef686",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "d4b6c21e-1d85-4f5a-b841-469a3079e5ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af717af4-b45c-4ce2-8240-b56c5fbef686",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "06b797d5-cf92-41e4-be66-b0bec5f50f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af717af4-b45c-4ce2-8240-b56c5fbef686",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "a623e138-28eb-4b27-ac88-04fa50216586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "7823bffd-9697-4700-8001-b7bc2d8a787f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a623e138-28eb-4b27-ac88-04fa50216586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8cff2c9-926f-4a87-aa5f-7a258fad96ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a623e138-28eb-4b27-ac88-04fa50216586",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "159ce5fb-b523-4521-a136-f5ded06b03a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a623e138-28eb-4b27-ac88-04fa50216586",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "4f28c9e6-9bd8-48a5-a645-d7a686d8a653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a623e138-28eb-4b27-ac88-04fa50216586",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "6c34bf08-7148-4c94-9af5-1da2bd432be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a623e138-28eb-4b27-ac88-04fa50216586",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "7834fd5d-ff47-4fea-a738-6a51c912d58c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "ff1ffd17-3eaf-482e-bd8c-2c3816a6bc6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7834fd5d-ff47-4fea-a738-6a51c912d58c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68d9b37-0f56-4dce-b784-6ecdb9ab5ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7834fd5d-ff47-4fea-a738-6a51c912d58c",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "c93dc160-124d-4ebf-a01d-15ec5a512a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7834fd5d-ff47-4fea-a738-6a51c912d58c",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "33745190-d3b5-473d-8038-6bf77b2d1d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7834fd5d-ff47-4fea-a738-6a51c912d58c",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "7e855ca6-6405-4772-b59d-12773609bb2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7834fd5d-ff47-4fea-a738-6a51c912d58c",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "d13f88d2-cabe-4d8f-abc7-e15cc280d135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "bc7e031d-4e47-41c6-95be-eb109086032c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d13f88d2-cabe-4d8f-abc7-e15cc280d135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d1b247-fb34-4dac-a4b4-b76b17898bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13f88d2-cabe-4d8f-abc7-e15cc280d135",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "912c6088-296b-43db-9943-51283fe5a3f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13f88d2-cabe-4d8f-abc7-e15cc280d135",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "35c344f9-a5b9-40c6-ab9b-c9d954c5f988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13f88d2-cabe-4d8f-abc7-e15cc280d135",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "2aa2900c-49c6-4b1a-8cb5-f76207de4d46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13f88d2-cabe-4d8f-abc7-e15cc280d135",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "48768f19-c212-4a61-8295-bb51874a1844",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "e2538fb9-8a38-4dbb-b663-54c4713f57f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48768f19-c212-4a61-8295-bb51874a1844",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "405edd8a-dc67-41ce-94a8-9f32ed946c7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48768f19-c212-4a61-8295-bb51874a1844",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "77eeeabc-9d6d-4dc1-8b07-4ff2022917a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48768f19-c212-4a61-8295-bb51874a1844",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "650eb9b9-965a-4ddb-874c-89e1071b143d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48768f19-c212-4a61-8295-bb51874a1844",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "502efcbb-2388-4fbe-83df-b00b5fe54abf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48768f19-c212-4a61-8295-bb51874a1844",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "71b481f9-4845-4a65-8100-8821b6292511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "3a8d7158-ed98-44c6-8981-dde91e1d1b1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b481f9-4845-4a65-8100-8821b6292511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a08e9854-3536-4e1d-a7ed-9963efa32916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b481f9-4845-4a65-8100-8821b6292511",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "16df4ddf-3196-4ec6-b383-82293f42c616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b481f9-4845-4a65-8100-8821b6292511",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "e431d3dc-f236-4a25-86f4-f9262defadee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b481f9-4845-4a65-8100-8821b6292511",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "0ad8faab-0be3-4152-af53-1d6745a23a2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b481f9-4845-4a65-8100-8821b6292511",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "47b5029c-8a9d-484f-82d8-549b45eaadbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "5c1394ef-d2a6-43a1-a06e-d360c0c69cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b5029c-8a9d-484f-82d8-549b45eaadbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c98257e1-bbec-4210-9824-86a1d7927540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b5029c-8a9d-484f-82d8-549b45eaadbf",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "2783ba44-c446-41e9-8eeb-68ae48436ccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b5029c-8a9d-484f-82d8-549b45eaadbf",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "288613f6-0c77-4f4c-b2a2-d497911ed73a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b5029c-8a9d-484f-82d8-549b45eaadbf",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "cc0abf0b-e1bb-4eb6-9936-9804a2f2b0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b5029c-8a9d-484f-82d8-549b45eaadbf",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "7eb1f3f7-9a0b-4584-881c-f6aa42ff262c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "9453e31c-5f54-4da4-9851-d2785d4d7bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eb1f3f7-9a0b-4584-881c-f6aa42ff262c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c16fc75-f32a-4f7c-bdcd-1f6cf1a85458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eb1f3f7-9a0b-4584-881c-f6aa42ff262c",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "817c42be-d36d-44ba-ae30-5d5ddf825fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eb1f3f7-9a0b-4584-881c-f6aa42ff262c",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "76155ae2-d925-450a-87b4-c6032960bb51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eb1f3f7-9a0b-4584-881c-f6aa42ff262c",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "50d5163c-bec6-408a-b310-e66f798e336a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eb1f3f7-9a0b-4584-881c-f6aa42ff262c",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "8c904f9d-db4b-4572-bc69-161cfe0febb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "760fbebf-f707-4449-8717-8d6b6ddd7896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c904f9d-db4b-4572-bc69-161cfe0febb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ce125c-17a9-4c40-a555-d8a9d2f94d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c904f9d-db4b-4572-bc69-161cfe0febb7",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "fd259fd2-7355-41b5-b317-bb933b04d50e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c904f9d-db4b-4572-bc69-161cfe0febb7",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "3c942bb6-a6c4-47fc-aada-8f4d0e45de2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c904f9d-db4b-4572-bc69-161cfe0febb7",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "684cd8ba-1af3-4dc6-ba2c-457e77cefc88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c904f9d-db4b-4572-bc69-161cfe0febb7",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "368d2246-ae3a-4221-a19d-b66c7bbdaa29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "95c0ac94-a721-4001-9bf9-3d7c3c37eec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368d2246-ae3a-4221-a19d-b66c7bbdaa29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8efd6b67-4915-475c-8774-5cc59d598b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368d2246-ae3a-4221-a19d-b66c7bbdaa29",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "69f7ab59-b84f-4517-866e-a68abc2a8c69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368d2246-ae3a-4221-a19d-b66c7bbdaa29",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "d3682e7e-7759-4215-b336-75f3ae88003d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368d2246-ae3a-4221-a19d-b66c7bbdaa29",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "6b688166-8b75-4905-afb5-c785b94f164d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368d2246-ae3a-4221-a19d-b66c7bbdaa29",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "6ff91886-e9dc-4455-8e94-bf237f2e6cd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "8acaefad-8787-45c9-b3a0-68a90e1d3a86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff91886-e9dc-4455-8e94-bf237f2e6cd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ed7dc90-8276-407e-9bb5-611081426160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff91886-e9dc-4455-8e94-bf237f2e6cd9",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "c3696001-d28b-41b0-a905-af18ecc0d0ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff91886-e9dc-4455-8e94-bf237f2e6cd9",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "fc3501e1-2bc4-48c7-8c22-171eb416e4f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff91886-e9dc-4455-8e94-bf237f2e6cd9",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "5e7200d9-0ad5-4e17-8331-6f61ceff42c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff91886-e9dc-4455-8e94-bf237f2e6cd9",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "cb9e9041-d17e-4611-8dc3-1c2dba358bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "5e4d0b2a-38fb-4184-8fea-8ec4ede49f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb9e9041-d17e-4611-8dc3-1c2dba358bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "131ceb10-7d69-4d49-aa14-d8a84f5df3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb9e9041-d17e-4611-8dc3-1c2dba358bcf",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "32331894-e983-41d8-b3da-6f4ee877c691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb9e9041-d17e-4611-8dc3-1c2dba358bcf",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "d5e69912-680e-4c08-afad-58a98fe45d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb9e9041-d17e-4611-8dc3-1c2dba358bcf",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "f899e883-bf14-47fc-9cda-2008c15d2535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb9e9041-d17e-4611-8dc3-1c2dba358bcf",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "e757a53c-9f39-4d65-a787-7c8d35eea5e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "644a627a-65fe-4f35-b702-805ef0d58fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e757a53c-9f39-4d65-a787-7c8d35eea5e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9dab232-02a1-4026-8dc5-1002855a8585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e757a53c-9f39-4d65-a787-7c8d35eea5e6",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "0afdd17f-6848-48db-9402-4bee45b38b09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e757a53c-9f39-4d65-a787-7c8d35eea5e6",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "a92dcdb3-7aea-4baa-a8eb-75202e8985cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e757a53c-9f39-4d65-a787-7c8d35eea5e6",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "82ff2822-5966-4227-977c-346d3856cee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e757a53c-9f39-4d65-a787-7c8d35eea5e6",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "3f3ea3e4-3184-4e12-bcec-586d433fa953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "ddd8c546-cfa1-4558-b092-556c3bd5d158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3ea3e4-3184-4e12-bcec-586d433fa953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd1f89e5-891c-49a5-8135-7b8319cfcd85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3ea3e4-3184-4e12-bcec-586d433fa953",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "dee9b58c-77e5-4974-8230-1e74d8a34c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3ea3e4-3184-4e12-bcec-586d433fa953",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "598aadb3-cf23-4d20-bf3b-6f3a98b4be7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3ea3e4-3184-4e12-bcec-586d433fa953",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "c1aa27f4-d2a0-4e56-86e3-75f3a341cf77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3ea3e4-3184-4e12-bcec-586d433fa953",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "aec00f6d-a86e-4940-a0f7-2d0784db5269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "234ca907-ac39-4a44-bc50-0c4ba9e28503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aec00f6d-a86e-4940-a0f7-2d0784db5269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b9488cf-3618-4ce8-ae51-61692cc5a8a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec00f6d-a86e-4940-a0f7-2d0784db5269",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "03e91bd1-e745-4ed1-aa16-4b4e20f4a3c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec00f6d-a86e-4940-a0f7-2d0784db5269",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "5997f83e-e847-4985-9ed4-0892c0968e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec00f6d-a86e-4940-a0f7-2d0784db5269",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "9b656b4e-4544-409d-ace1-f83405ee9605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec00f6d-a86e-4940-a0f7-2d0784db5269",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "92e6edbe-c113-4d80-9c86-32403200009b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "909bf1de-dcd5-491c-a97d-7aab682386e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92e6edbe-c113-4d80-9c86-32403200009b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0375d55a-e6f9-4412-b2eb-c412eeddafad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e6edbe-c113-4d80-9c86-32403200009b",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "021d5b7e-a44e-42a2-b99d-3a334d15c318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e6edbe-c113-4d80-9c86-32403200009b",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "081ed446-0dcf-4420-9480-d7826db6a23b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e6edbe-c113-4d80-9c86-32403200009b",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "f79a47e4-f63f-4b9b-b015-fe76919037d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e6edbe-c113-4d80-9c86-32403200009b",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "ac1e3e61-e6f0-4469-8ab7-be164702f719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "1c5e10e5-0e0e-4037-888e-c05a5428f3d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac1e3e61-e6f0-4469-8ab7-be164702f719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610ee46d-5d63-4fc4-8657-a5c6d39124b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac1e3e61-e6f0-4469-8ab7-be164702f719",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "d85588e7-1656-4be2-8361-b2c535d73e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac1e3e61-e6f0-4469-8ab7-be164702f719",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "89a32554-31b3-41ff-89ec-c11f50590376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac1e3e61-e6f0-4469-8ab7-be164702f719",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "94edfc89-4bf6-4cea-bf0f-03869b9054a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac1e3e61-e6f0-4469-8ab7-be164702f719",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "55ee97d6-21ae-46c7-859c-0fedc4ff17ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "b3d67d7d-d6c8-48a1-88b2-74ee9ed509e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ee97d6-21ae-46c7-859c-0fedc4ff17ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc15aaa3-8a9a-4a6e-b923-e6c35ff778cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ee97d6-21ae-46c7-859c-0fedc4ff17ef",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "a2d22fc0-35b5-4bf6-86d2-f8ec2bf9aab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ee97d6-21ae-46c7-859c-0fedc4ff17ef",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "9b828e30-6eae-4e7b-814f-426146b93846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ee97d6-21ae-46c7-859c-0fedc4ff17ef",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "2852d1a3-b917-48d2-8c4f-876643331e9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ee97d6-21ae-46c7-859c-0fedc4ff17ef",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "c179f62c-c224-4a84-9840-0c13a530c0cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "c1307f15-5ba6-4257-84ae-5c405dbee486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c179f62c-c224-4a84-9840-0c13a530c0cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5969a35b-d84a-4614-86e9-934f3aa6f9a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c179f62c-c224-4a84-9840-0c13a530c0cb",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "c912907f-713e-45fb-8012-b2722dfab76f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c179f62c-c224-4a84-9840-0c13a530c0cb",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "ae261a7c-f5a4-4aba-8dd2-2c0dfa0fc9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c179f62c-c224-4a84-9840-0c13a530c0cb",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "ea91f58a-620c-4e4c-b04f-b98457d8cf76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c179f62c-c224-4a84-9840-0c13a530c0cb",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "fc2e9105-e5c8-458b-8b08-ba033bc170f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "111d280b-7b93-4c08-ae2b-48e5c1ccc91b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2e9105-e5c8-458b-8b08-ba033bc170f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d2ba680-eb7d-4b86-ac13-948c0ec649bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2e9105-e5c8-458b-8b08-ba033bc170f5",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "74e19207-a8f9-4bff-b3fb-ce05a6ab331c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2e9105-e5c8-458b-8b08-ba033bc170f5",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "fb961d55-6d44-4898-b2c6-ed2cebc54796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2e9105-e5c8-458b-8b08-ba033bc170f5",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "243f6d34-4f67-418b-83a4-ee88050b4f7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2e9105-e5c8-458b-8b08-ba033bc170f5",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        },
        {
            "id": "7cb0e895-9b39-4b07-9ad9-0b3e811f3e0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "compositeImage": {
                "id": "f02dc5c0-3eeb-4f02-bb6c-5fc8182a90e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb0e895-9b39-4b07-9ad9-0b3e811f3e0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3060471-cd3d-4d01-91cc-808dd4e2bc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb0e895-9b39-4b07-9ad9-0b3e811f3e0c",
                    "LayerId": "763ba40a-1764-4e9d-9bb6-178039efe5f5"
                },
                {
                    "id": "08ba863a-e227-4039-95d4-2dbb4b92724c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb0e895-9b39-4b07-9ad9-0b3e811f3e0c",
                    "LayerId": "198b1977-e279-4677-a5bf-36ad698a6ef8"
                },
                {
                    "id": "149ea4bc-2c28-42f7-9e61-114f5afafaeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb0e895-9b39-4b07-9ad9-0b3e811f3e0c",
                    "LayerId": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7"
                },
                {
                    "id": "697b9680-a0c7-4fe0-93ba-52328ea3a713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb0e895-9b39-4b07-9ad9-0b3e811f3e0c",
                    "LayerId": "8e1eff03-6b7c-4176-a83f-94870b178127"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "763ba40a-1764-4e9d-9bb6-178039efe5f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "8e1eff03-6b7c-4176-a83f-94870b178127",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "198b1977-e279-4677-a5bf-36ad698a6ef8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "654ae9db-3eec-4df2-b62a-74f2a9f88fa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}