{
    "id": "ad1d9260-6f62-49ba-8993-30c12fc6797b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanyonSky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2564c3cb-5255-433c-8e4a-ddd16fdada52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad1d9260-6f62-49ba-8993-30c12fc6797b",
            "compositeImage": {
                "id": "269765d1-6e52-46df-a52b-ba43fa9d9d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2564c3cb-5255-433c-8e4a-ddd16fdada52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "157e94b8-a1a3-4e31-adc9-e2e6dc062bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2564c3cb-5255-433c-8e4a-ddd16fdada52",
                    "LayerId": "6b54304c-80d8-49d3-a3a9-dbcdbf5c9c35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6b54304c-80d8-49d3-a3a9-dbcdbf5c9c35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad1d9260-6f62-49ba-8993-30c12fc6797b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}