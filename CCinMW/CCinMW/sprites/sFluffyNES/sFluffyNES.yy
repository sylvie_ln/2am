{
    "id": "ce3398cb-a55b-4cd8-bccc-3f32e26581e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFluffyNES",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba902f98-f2dd-4a7f-bcc3-34347628a469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3398cb-a55b-4cd8-bccc-3f32e26581e0",
            "compositeImage": {
                "id": "8f2ec9e2-5ca8-4442-b232-159ab014527c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba902f98-f2dd-4a7f-bcc3-34347628a469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85baac2a-4880-4dbc-9394-0d9efe21927c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba902f98-f2dd-4a7f-bcc3-34347628a469",
                    "LayerId": "5d4a977b-cbf8-4b39-8528-490d4da4ce6a"
                }
            ]
        },
        {
            "id": "338e99a2-dba2-4e22-ad2a-5318da6341d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3398cb-a55b-4cd8-bccc-3f32e26581e0",
            "compositeImage": {
                "id": "33598252-82e6-4630-8739-de8059f4e29f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "338e99a2-dba2-4e22-ad2a-5318da6341d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7e6f512-e282-497f-b701-d8c0196f8de0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338e99a2-dba2-4e22-ad2a-5318da6341d1",
                    "LayerId": "5d4a977b-cbf8-4b39-8528-490d4da4ce6a"
                }
            ]
        },
        {
            "id": "a7fc15b6-898c-4537-930c-a306207ccec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3398cb-a55b-4cd8-bccc-3f32e26581e0",
            "compositeImage": {
                "id": "2cc15efd-508f-4b90-88fd-b840d18265eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fc15b6-898c-4537-930c-a306207ccec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "046e8837-8eb8-46c5-982e-95994ba01dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fc15b6-898c-4537-930c-a306207ccec9",
                    "LayerId": "5d4a977b-cbf8-4b39-8528-490d4da4ce6a"
                }
            ]
        },
        {
            "id": "59729e6a-996e-4bab-b7ae-27113104811f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3398cb-a55b-4cd8-bccc-3f32e26581e0",
            "compositeImage": {
                "id": "8f175fe2-fa80-4c4e-9356-2600d377baf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59729e6a-996e-4bab-b7ae-27113104811f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56afc862-ac5e-4566-9d7b-5d2b957c262e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59729e6a-996e-4bab-b7ae-27113104811f",
                    "LayerId": "5d4a977b-cbf8-4b39-8528-490d4da4ce6a"
                }
            ]
        },
        {
            "id": "86b5037a-ee25-4c85-9d92-0571d5414190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3398cb-a55b-4cd8-bccc-3f32e26581e0",
            "compositeImage": {
                "id": "97f75442-b94f-4bf3-85fe-c3f912672dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86b5037a-ee25-4c85-9d92-0571d5414190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7025baf-fecd-4a46-bbc4-5b425ac9063e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86b5037a-ee25-4c85-9d92-0571d5414190",
                    "LayerId": "5d4a977b-cbf8-4b39-8528-490d4da4ce6a"
                }
            ]
        },
        {
            "id": "3d775bd5-483a-4daf-9d85-749a555026f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3398cb-a55b-4cd8-bccc-3f32e26581e0",
            "compositeImage": {
                "id": "a3657092-94ae-4ccf-9d4f-ea4ee6011cce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d775bd5-483a-4daf-9d85-749a555026f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0afd612a-7f20-47bf-9747-684d5996f0b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d775bd5-483a-4daf-9d85-749a555026f1",
                    "LayerId": "5d4a977b-cbf8-4b39-8528-490d4da4ce6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5d4a977b-cbf8-4b39-8528-490d4da4ce6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce3398cb-a55b-4cd8-bccc-3f32e26581e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}