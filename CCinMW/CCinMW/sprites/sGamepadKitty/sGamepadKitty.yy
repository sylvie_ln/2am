{
    "id": "5805352d-a1a9-4fab-ab29-876d787fd0be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadKitty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adf5ebd7-b13e-46a6-bff1-ebad4030fe0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5805352d-a1a9-4fab-ab29-876d787fd0be",
            "compositeImage": {
                "id": "1d20f0e0-f2c9-42b9-b098-1ce4ad802fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adf5ebd7-b13e-46a6-bff1-ebad4030fe0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c51384-5418-48bc-8569-319b5f8cf23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf5ebd7-b13e-46a6-bff1-ebad4030fe0e",
                    "LayerId": "2cb56084-02ec-4876-acaa-4d05cb3f758f"
                },
                {
                    "id": "cfec55cc-22fb-40a5-b2e5-a8c464c44af2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf5ebd7-b13e-46a6-bff1-ebad4030fe0e",
                    "LayerId": "d9500317-1ef0-4781-b8f0-5596312e2e51"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "2cb56084-02ec-4876-acaa-4d05cb3f758f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5805352d-a1a9-4fab-ab29-876d787fd0be",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d9500317-1ef0-4781-b8f0-5596312e2e51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5805352d-a1a9-4fab-ab29-876d787fd0be",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}