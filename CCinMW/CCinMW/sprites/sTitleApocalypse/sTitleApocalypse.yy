{
    "id": "1d4ae308-e752-477e-9c7d-95039721e9ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleApocalypse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 81,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "423c78c9-bf39-4b00-a6f3-7b1d129a8b45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d4ae308-e752-477e-9c7d-95039721e9ff",
            "compositeImage": {
                "id": "c9e6d86d-78af-46db-9c83-d06fd4789039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423c78c9-bf39-4b00-a6f3-7b1d129a8b45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca400573-82f6-49be-9aae-b12598fb3aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423c78c9-bf39-4b00-a6f3-7b1d129a8b45",
                    "LayerId": "af3ff914-b9dd-4e57-a66a-0274925461d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "af3ff914-b9dd-4e57-a66a-0274925461d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d4ae308-e752-477e-9c7d-95039721e9ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}