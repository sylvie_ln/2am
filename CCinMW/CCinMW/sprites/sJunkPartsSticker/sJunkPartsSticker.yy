{
    "id": "6623aad9-dbdb-48e1-b1f2-d2cc447c93e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJunkPartsSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78489485-749a-4db2-baf2-06a04c42c05a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6623aad9-dbdb-48e1-b1f2-d2cc447c93e1",
            "compositeImage": {
                "id": "8a90271a-0d98-45f4-9f60-5c06259dccc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78489485-749a-4db2-baf2-06a04c42c05a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ccbdbe5-34c4-4333-8298-a9686130a5ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78489485-749a-4db2-baf2-06a04c42c05a",
                    "LayerId": "01d1889e-cd56-4ec1-8c05-e6d0ed245d55"
                }
            ]
        },
        {
            "id": "3b27c62f-a2b3-4ce6-a196-9e24e9b42d99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6623aad9-dbdb-48e1-b1f2-d2cc447c93e1",
            "compositeImage": {
                "id": "c371c303-4ce8-4ce4-a604-55902fe36236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b27c62f-a2b3-4ce6-a196-9e24e9b42d99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "484b6d88-1baf-45f3-b7c7-1360b0189088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b27c62f-a2b3-4ce6-a196-9e24e9b42d99",
                    "LayerId": "01d1889e-cd56-4ec1-8c05-e6d0ed245d55"
                }
            ]
        },
        {
            "id": "bbf90665-d9c9-492e-8e69-71d6022980da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6623aad9-dbdb-48e1-b1f2-d2cc447c93e1",
            "compositeImage": {
                "id": "4ce39e93-60ed-4d8a-bee4-d2e7eb297a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf90665-d9c9-492e-8e69-71d6022980da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d30ff909-f01a-4756-97b4-92d09646613c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf90665-d9c9-492e-8e69-71d6022980da",
                    "LayerId": "01d1889e-cd56-4ec1-8c05-e6d0ed245d55"
                }
            ]
        },
        {
            "id": "708449f2-ff35-4229-937a-ac3226094adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6623aad9-dbdb-48e1-b1f2-d2cc447c93e1",
            "compositeImage": {
                "id": "753fee55-6e57-4ddc-8c40-c7418396bdb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708449f2-ff35-4229-937a-ac3226094adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7490f542-0839-4a0f-9b3d-96b616fd0f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708449f2-ff35-4229-937a-ac3226094adb",
                    "LayerId": "01d1889e-cd56-4ec1-8c05-e6d0ed245d55"
                }
            ]
        },
        {
            "id": "615dfea2-f69c-4217-8617-1600cd0dd6e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6623aad9-dbdb-48e1-b1f2-d2cc447c93e1",
            "compositeImage": {
                "id": "4a355bd8-d3ab-4b3d-9568-08759e0c53f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "615dfea2-f69c-4217-8617-1600cd0dd6e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff50a1f3-e822-4d22-b1aa-88fc8dd9e01b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "615dfea2-f69c-4217-8617-1600cd0dd6e8",
                    "LayerId": "01d1889e-cd56-4ec1-8c05-e6d0ed245d55"
                }
            ]
        },
        {
            "id": "651d3f5c-7fd0-4b73-b124-d4b943ac230c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6623aad9-dbdb-48e1-b1f2-d2cc447c93e1",
            "compositeImage": {
                "id": "409dfe17-ebd1-4bfd-9bde-9c8c0432b01c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "651d3f5c-7fd0-4b73-b124-d4b943ac230c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8540efe-b5d7-4ddc-91c7-92d74f0800d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "651d3f5c-7fd0-4b73-b124-d4b943ac230c",
                    "LayerId": "01d1889e-cd56-4ec1-8c05-e6d0ed245d55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "01d1889e-cd56-4ec1-8c05-e6d0ed245d55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6623aad9-dbdb-48e1-b1f2-d2cc447c93e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        1090519039,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}