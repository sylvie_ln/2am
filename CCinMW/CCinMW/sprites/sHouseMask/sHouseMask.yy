{
    "id": "1320b02f-01ac-43fb-b728-93b9ea06ac72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouseMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43a5d049-fd71-47be-8a1a-c6d8ac4a5b41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1320b02f-01ac-43fb-b728-93b9ea06ac72",
            "compositeImage": {
                "id": "8dc4b2c7-4d59-4bd0-b4cd-2fe80ed5354c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a5d049-fd71-47be-8a1a-c6d8ac4a5b41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83dcebb9-4069-4449-ae71-c15dff308359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a5d049-fd71-47be-8a1a-c6d8ac4a5b41",
                    "LayerId": "1237e3cf-2229-4094-bf84-0471fe9b31b7"
                },
                {
                    "id": "3a9732da-967e-4f46-aab2-f399018345ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a5d049-fd71-47be-8a1a-c6d8ac4a5b41",
                    "LayerId": "6daf78d0-5fa0-44d1-8bd7-cb84782409f1"
                },
                {
                    "id": "29417429-6e0d-4115-8737-8d78658c0243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a5d049-fd71-47be-8a1a-c6d8ac4a5b41",
                    "LayerId": "02bc4486-deed-44f3-9a1e-4cb82e8be94d"
                },
                {
                    "id": "5ad056cd-f8ae-4a82-990b-7708ad1b1041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a5d049-fd71-47be-8a1a-c6d8ac4a5b41",
                    "LayerId": "09fae49f-e079-4b1e-afd6-abd298165853"
                },
                {
                    "id": "2a4b8a33-976a-4384-9f30-956abc1a6b3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a5d049-fd71-47be-8a1a-c6d8ac4a5b41",
                    "LayerId": "01de36d7-bfce-4482-bb79-5b5ddaf0f0bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "01de36d7-bfce-4482-bb79-5b5ddaf0f0bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1320b02f-01ac-43fb-b728-93b9ea06ac72",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1237e3cf-2229-4094-bf84-0471fe9b31b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1320b02f-01ac-43fb-b728-93b9ea06ac72",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "6daf78d0-5fa0-44d1-8bd7-cb84782409f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1320b02f-01ac-43fb-b728-93b9ea06ac72",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "02bc4486-deed-44f3-9a1e-4cb82e8be94d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1320b02f-01ac-43fb-b728-93b9ea06ac72",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 25,
            "visible": false
        },
        {
            "id": "09fae49f-e079-4b1e-afd6-abd298165853",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1320b02f-01ac-43fb-b728-93b9ea06ac72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}