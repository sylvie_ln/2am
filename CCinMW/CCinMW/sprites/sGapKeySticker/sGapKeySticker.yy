{
    "id": "633e8553-3b9c-40cf-98a7-da52f5b3606c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGapKeySticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d071dc65-9da8-4903-9806-7da522e342bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "633e8553-3b9c-40cf-98a7-da52f5b3606c",
            "compositeImage": {
                "id": "a9cbf49d-f02e-41fd-88d0-d401137c84b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d071dc65-9da8-4903-9806-7da522e342bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ca60fb-1813-43e3-8ac4-05c9254ab8ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d071dc65-9da8-4903-9806-7da522e342bd",
                    "LayerId": "f1c0ee11-af9c-461f-bf0f-059ffbf3410e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f1c0ee11-af9c-461f-bf0f-059ffbf3410e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "633e8553-3b9c-40cf-98a7-da52f5b3606c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}