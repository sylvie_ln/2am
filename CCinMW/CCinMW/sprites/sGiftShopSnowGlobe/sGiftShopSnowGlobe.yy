{
    "id": "5c17df20-ef08-42c4-a128-16c0c7c0ce60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopSnowGlobe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10ceefea-23fa-47ea-8440-1cd44cc65507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c17df20-ef08-42c4-a128-16c0c7c0ce60",
            "compositeImage": {
                "id": "7ce01c14-043c-4172-a62c-34787ddb146b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10ceefea-23fa-47ea-8440-1cd44cc65507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691d6b9d-d871-4dac-b6ca-434526a65a0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10ceefea-23fa-47ea-8440-1cd44cc65507",
                    "LayerId": "1005506b-a81c-4e8c-86ac-1e282f4eabf6"
                },
                {
                    "id": "a455d072-ab9a-47ae-a363-f0ec0c000415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10ceefea-23fa-47ea-8440-1cd44cc65507",
                    "LayerId": "181e5806-3af6-45d8-9c4b-e4f11751797f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "181e5806-3af6-45d8-9c4b-e4f11751797f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c17df20-ef08-42c4-a128-16c0c7c0ce60",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1005506b-a81c-4e8c-86ac-1e282f4eabf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c17df20-ef08-42c4-a128-16c0c7c0ce60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}