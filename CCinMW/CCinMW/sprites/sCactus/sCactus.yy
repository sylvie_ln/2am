{
    "id": "599e9c8b-9945-4c09-ba58-ca5895115dba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCactus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f11d23d-abe9-42c1-b8fb-92e715cf960e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "599e9c8b-9945-4c09-ba58-ca5895115dba",
            "compositeImage": {
                "id": "d3279309-d3a3-4d59-963d-8222933d0110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f11d23d-abe9-42c1-b8fb-92e715cf960e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "997cdff1-e6da-44df-8b67-f654debd94c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f11d23d-abe9-42c1-b8fb-92e715cf960e",
                    "LayerId": "211962ab-de59-4552-8ad2-357a3707f5bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "211962ab-de59-4552-8ad2-357a3707f5bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "599e9c8b-9945-4c09-ba58-ca5895115dba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}