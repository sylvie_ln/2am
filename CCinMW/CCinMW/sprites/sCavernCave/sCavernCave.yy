{
    "id": "df8319e1-4bd6-4a3e-ad7e-a87fa70c0de0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCavernCave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7d33a5c-5ae5-474d-918d-e19aa2b6ab7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df8319e1-4bd6-4a3e-ad7e-a87fa70c0de0",
            "compositeImage": {
                "id": "d0691f15-6f91-48b4-a3e0-ee80cbb21930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d33a5c-5ae5-474d-918d-e19aa2b6ab7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a555698-ad2e-4d5d-a363-163f53538574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d33a5c-5ae5-474d-918d-e19aa2b6ab7e",
                    "LayerId": "07bc7e55-a52a-4ae6-be05-3b1178f71679"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "07bc7e55-a52a-4ae6-be05-3b1178f71679",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df8319e1-4bd6-4a3e-ad7e-a87fa70c0de0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}