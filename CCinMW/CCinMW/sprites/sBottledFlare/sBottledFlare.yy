{
    "id": "42e54525-350d-4620-8d52-eabac4ad086b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBottledFlare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e603be4-20f5-4b99-9129-bb6cff246997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42e54525-350d-4620-8d52-eabac4ad086b",
            "compositeImage": {
                "id": "f2dd409f-0bc5-4713-b281-697edc015461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e603be4-20f5-4b99-9129-bb6cff246997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4498cc7-458b-41e9-a2e6-fd37ddff9b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e603be4-20f5-4b99-9129-bb6cff246997",
                    "LayerId": "c58e1e2c-8b15-4ae8-adef-963e233724fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c58e1e2c-8b15-4ae8-adef-963e233724fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42e54525-350d-4620-8d52-eabac4ad086b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}