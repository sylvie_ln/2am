{
    "id": "ab884b99-d517-4e33-a1b5-b764c30a8a9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMiniBunneySticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "875975bd-ecf9-4ccb-95c0-85a568134b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab884b99-d517-4e33-a1b5-b764c30a8a9c",
            "compositeImage": {
                "id": "94295e44-b296-4d2f-ac2a-a5993610fb01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875975bd-ecf9-4ccb-95c0-85a568134b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bcf5e84-0384-4168-a0d0-0b5356d00c12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875975bd-ecf9-4ccb-95c0-85a568134b8c",
                    "LayerId": "6f195595-628d-4490-88ab-3b5aab8fb305"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6f195595-628d-4490-88ab-3b5aab8fb305",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab884b99-d517-4e33-a1b5-b764c30a8a9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}