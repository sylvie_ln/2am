{
    "id": "17242380-d530-474f-aea6-73e79a2026ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHairs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2746509c-c646-422c-9fee-2de6f0328270",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "compositeImage": {
                "id": "eb9aca4a-1034-4f53-9a80-2e4312a3bb56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2746509c-c646-422c-9fee-2de6f0328270",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90557fd3-55a0-401d-957c-1ef5c483f06f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2746509c-c646-422c-9fee-2de6f0328270",
                    "LayerId": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc"
                },
                {
                    "id": "915e7b73-5e34-48da-adc2-56b8b28e0dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2746509c-c646-422c-9fee-2de6f0328270",
                    "LayerId": "ea5b0781-9231-4f29-a52d-d542afeff525"
                },
                {
                    "id": "82971867-be3c-46cd-92bd-cf67ee57486d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2746509c-c646-422c-9fee-2de6f0328270",
                    "LayerId": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0"
                }
            ]
        },
        {
            "id": "32c2c9c0-0e9f-462c-b5d9-c1d8329929fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "compositeImage": {
                "id": "ed665bb9-969d-4ad5-9bc9-6f5f073da50a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c2c9c0-0e9f-462c-b5d9-c1d8329929fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e086f1d0-39b7-409d-a775-0211440bb335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c2c9c0-0e9f-462c-b5d9-c1d8329929fe",
                    "LayerId": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc"
                },
                {
                    "id": "1eebd58e-c490-4513-882b-e916091812fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c2c9c0-0e9f-462c-b5d9-c1d8329929fe",
                    "LayerId": "ea5b0781-9231-4f29-a52d-d542afeff525"
                },
                {
                    "id": "82017f3a-a3b3-4642-b13d-669e3ab63406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c2c9c0-0e9f-462c-b5d9-c1d8329929fe",
                    "LayerId": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0"
                }
            ]
        },
        {
            "id": "2a0be74d-dd90-4d6c-90e0-011dff510985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "compositeImage": {
                "id": "d8f9548f-37d6-43c0-84f1-4a4c6dd6a0a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a0be74d-dd90-4d6c-90e0-011dff510985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "670cbf2e-99d8-4153-a58f-1f793ec2ccef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0be74d-dd90-4d6c-90e0-011dff510985",
                    "LayerId": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc"
                },
                {
                    "id": "2dd31f2a-c5e2-4223-ad03-7189f1cd0cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0be74d-dd90-4d6c-90e0-011dff510985",
                    "LayerId": "ea5b0781-9231-4f29-a52d-d542afeff525"
                },
                {
                    "id": "be84cc10-2e9d-458f-a738-11b68ecb524b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0be74d-dd90-4d6c-90e0-011dff510985",
                    "LayerId": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0"
                }
            ]
        },
        {
            "id": "033dc9fd-87e0-4165-8c1d-3cca8de35bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "compositeImage": {
                "id": "45098685-6c35-430b-896e-25a778615638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033dc9fd-87e0-4165-8c1d-3cca8de35bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc8af033-7b3c-4920-bb72-d4f199adabb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033dc9fd-87e0-4165-8c1d-3cca8de35bc2",
                    "LayerId": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc"
                },
                {
                    "id": "4efc549a-6f92-4bf5-92da-5778c513e91a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033dc9fd-87e0-4165-8c1d-3cca8de35bc2",
                    "LayerId": "ea5b0781-9231-4f29-a52d-d542afeff525"
                },
                {
                    "id": "ab956227-55a2-4768-8d77-56963acac609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033dc9fd-87e0-4165-8c1d-3cca8de35bc2",
                    "LayerId": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0"
                }
            ]
        },
        {
            "id": "5812a14a-cf61-48d4-a1cd-b4c57a87f11c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "compositeImage": {
                "id": "3b9836bb-ea71-44dd-bc17-86627f51fee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5812a14a-cf61-48d4-a1cd-b4c57a87f11c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41c30723-d479-40ef-a18c-35f653bfbb43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5812a14a-cf61-48d4-a1cd-b4c57a87f11c",
                    "LayerId": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc"
                },
                {
                    "id": "986d3340-5ec3-459a-a867-31292fa0d1d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5812a14a-cf61-48d4-a1cd-b4c57a87f11c",
                    "LayerId": "ea5b0781-9231-4f29-a52d-d542afeff525"
                },
                {
                    "id": "2eef204f-d651-403f-93e6-8949c838cd51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5812a14a-cf61-48d4-a1cd-b4c57a87f11c",
                    "LayerId": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0"
                }
            ]
        },
        {
            "id": "bea8e9db-f5d5-4115-adf9-25ffa7831014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "compositeImage": {
                "id": "89db2f9c-a3ae-4134-b429-937b2e2d33fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bea8e9db-f5d5-4115-adf9-25ffa7831014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9446d25a-8f50-4ef2-9ee5-ad9266990a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bea8e9db-f5d5-4115-adf9-25ffa7831014",
                    "LayerId": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc"
                },
                {
                    "id": "55c386ef-3da7-4f80-b0c7-a7edb98c88b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bea8e9db-f5d5-4115-adf9-25ffa7831014",
                    "LayerId": "ea5b0781-9231-4f29-a52d-d542afeff525"
                },
                {
                    "id": "ad63f4f3-97e9-437e-92cb-d789aabf60d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bea8e9db-f5d5-4115-adf9-25ffa7831014",
                    "LayerId": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0"
                }
            ]
        },
        {
            "id": "1f52c840-8ee2-41b4-a562-b10dd02f5a08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "compositeImage": {
                "id": "a77552d1-8395-4dea-ae3b-2b1a16a05e87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f52c840-8ee2-41b4-a562-b10dd02f5a08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59192794-ef2c-483b-9a7c-3f4fa97300d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f52c840-8ee2-41b4-a562-b10dd02f5a08",
                    "LayerId": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc"
                },
                {
                    "id": "a190264c-decf-47c1-a07b-0678c38e0753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f52c840-8ee2-41b4-a562-b10dd02f5a08",
                    "LayerId": "ea5b0781-9231-4f29-a52d-d542afeff525"
                },
                {
                    "id": "9f6151c5-8bb0-4103-a239-5e70dc3c5e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f52c840-8ee2-41b4-a562-b10dd02f5a08",
                    "LayerId": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0"
                }
            ]
        },
        {
            "id": "b0841bcc-8ec7-4b4e-8c21-dc83ee073f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "compositeImage": {
                "id": "b131c29a-eb93-4d18-81b3-f61d818f1058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0841bcc-8ec7-4b4e-8c21-dc83ee073f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec94c925-51b2-459d-aa2d-f31eda3f6d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0841bcc-8ec7-4b4e-8c21-dc83ee073f72",
                    "LayerId": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc"
                },
                {
                    "id": "19a6238e-f89a-4560-b393-93940d213446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0841bcc-8ec7-4b4e-8c21-dc83ee073f72",
                    "LayerId": "ea5b0781-9231-4f29-a52d-d542afeff525"
                },
                {
                    "id": "2b697c16-b40d-4383-a399-95b0a4af54af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0841bcc-8ec7-4b4e-8c21-dc83ee073f72",
                    "LayerId": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2f7b1025-b1fb-4ca1-8321-6a2cdbcfcaf0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f5c4c75c-b9f2-4aaa-a069-45017e1d2cdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "ea5b0781-9231-4f29-a52d-d542afeff525",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17242380-d530-474f-aea6-73e79a2026ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}