{
    "id": "e34aba32-2cea-494d-a909-a9a04b10600f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlackRose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae1fe8ff-eae4-4b3b-8480-4a4f317d2834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34aba32-2cea-494d-a909-a9a04b10600f",
            "compositeImage": {
                "id": "b4dfca2a-e9d9-4005-bdda-8d753c3afc6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae1fe8ff-eae4-4b3b-8480-4a4f317d2834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdee6546-e5b1-4128-aef1-cf60b2204fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae1fe8ff-eae4-4b3b-8480-4a4f317d2834",
                    "LayerId": "1aab25a8-e236-47df-84cb-a013031f8916"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1aab25a8-e236-47df-84cb-a013031f8916",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e34aba32-2cea-494d-a909-a9a04b10600f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}