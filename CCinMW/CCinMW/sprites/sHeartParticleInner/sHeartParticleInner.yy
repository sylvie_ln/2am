{
    "id": "9fa44ca8-b7fc-4fd0-b1ef-00931f0f1734",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeartParticleInner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3eb65dd-8ac7-4dcc-bafd-b39db9dd29ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fa44ca8-b7fc-4fd0-b1ef-00931f0f1734",
            "compositeImage": {
                "id": "3d8cab83-9dba-44be-a3e8-a291c62aaecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3eb65dd-8ac7-4dcc-bafd-b39db9dd29ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a01c871-5ab9-4391-a66e-bef8854bc454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3eb65dd-8ac7-4dcc-bafd-b39db9dd29ed",
                    "LayerId": "a8ee8027-6d4b-484c-a30e-a96a996b3874"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a8ee8027-6d4b-484c-a30e-a96a996b3874",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fa44ca8-b7fc-4fd0-b1ef-00931f0f1734",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}