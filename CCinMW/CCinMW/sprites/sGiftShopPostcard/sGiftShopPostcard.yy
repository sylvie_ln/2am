{
    "id": "9d345f38-c955-4189-a7ec-3fb71effbc31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopPostcard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7b71124-05ca-4598-8b57-f64384740627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d345f38-c955-4189-a7ec-3fb71effbc31",
            "compositeImage": {
                "id": "c1f2528b-d01d-4ce3-99e0-1b462fd63a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7b71124-05ca-4598-8b57-f64384740627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead14c93-1388-46da-98b6-6f2f69321096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b71124-05ca-4598-8b57-f64384740627",
                    "LayerId": "1b130cae-c443-4b83-a94b-664b11d6671b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1b130cae-c443-4b83-a94b-664b11d6671b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d345f38-c955-4189-a7ec-3fb71effbc31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}