{
    "id": "ccf47ffd-ec49-4d89-8bae-55a183898ad1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloudCanyon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb5156bc-5803-4a29-98d3-0a4881598d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf47ffd-ec49-4d89-8bae-55a183898ad1",
            "compositeImage": {
                "id": "3696797b-b462-4df9-b880-00eb69901bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb5156bc-5803-4a29-98d3-0a4881598d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d35ed5c9-3366-4994-b1d3-dc56625d3c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb5156bc-5803-4a29-98d3-0a4881598d78",
                    "LayerId": "4cb9cf2a-ddd3-478d-91c2-42bf81b4bfda"
                }
            ]
        },
        {
            "id": "8c408be5-5b2a-425f-960c-38e687c7ff56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf47ffd-ec49-4d89-8bae-55a183898ad1",
            "compositeImage": {
                "id": "9771556c-f137-46c5-9255-dcd94fce7f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c408be5-5b2a-425f-960c-38e687c7ff56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31548a6d-d4d8-4413-951c-2a1944e66b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c408be5-5b2a-425f-960c-38e687c7ff56",
                    "LayerId": "4cb9cf2a-ddd3-478d-91c2-42bf81b4bfda"
                }
            ]
        },
        {
            "id": "88d4d868-d3b9-4a09-b7df-bfd4fd482e32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf47ffd-ec49-4d89-8bae-55a183898ad1",
            "compositeImage": {
                "id": "3b441b0b-d692-4385-b823-8750ac0c76b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88d4d868-d3b9-4a09-b7df-bfd4fd482e32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd706689-806d-428c-9e98-0a43da7d3ec5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88d4d868-d3b9-4a09-b7df-bfd4fd482e32",
                    "LayerId": "4cb9cf2a-ddd3-478d-91c2-42bf81b4bfda"
                }
            ]
        },
        {
            "id": "96083267-e6fa-442d-9548-c6a2a179f4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf47ffd-ec49-4d89-8bae-55a183898ad1",
            "compositeImage": {
                "id": "5d25671e-97f3-45f7-9c36-1323484397aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96083267-e6fa-442d-9548-c6a2a179f4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3936eb3-ac16-47a6-b5ef-7e18bbcba024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96083267-e6fa-442d-9548-c6a2a179f4e0",
                    "LayerId": "4cb9cf2a-ddd3-478d-91c2-42bf81b4bfda"
                }
            ]
        },
        {
            "id": "3e396486-e34d-496a-ba85-191cba9e9998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf47ffd-ec49-4d89-8bae-55a183898ad1",
            "compositeImage": {
                "id": "ba83a33e-0e82-49b9-a84c-12afd177186e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e396486-e34d-496a-ba85-191cba9e9998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa6708e-6123-4430-ba48-f4aa46f07ca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e396486-e34d-496a-ba85-191cba9e9998",
                    "LayerId": "4cb9cf2a-ddd3-478d-91c2-42bf81b4bfda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4cb9cf2a-ddd3-478d-91c2-42bf81b4bfda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccf47ffd-ec49-4d89-8bae-55a183898ad1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}