{
    "id": "02fb89e3-1ca2-4aed-9e2c-5be9d97e4b9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91de58d6-8f94-4893-8b98-99b9dc099307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fb89e3-1ca2-4aed-9e2c-5be9d97e4b9b",
            "compositeImage": {
                "id": "08971e93-0586-470f-814d-ab8d851c7311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91de58d6-8f94-4893-8b98-99b9dc099307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4f692ca-b35b-4146-a14b-5869b9327f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91de58d6-8f94-4893-8b98-99b9dc099307",
                    "LayerId": "e42ebabc-c777-4586-b56c-5c42c447d116"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e42ebabc-c777-4586-b56c-5c42c447d116",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02fb89e3-1ca2-4aed-9e2c-5be9d97e4b9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}