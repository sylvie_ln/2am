{
    "id": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "005a3502-b8b7-4685-9cc5-4268464a084a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "c2297749-a543-4f62-9e86-d8906816d8e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "005a3502-b8b7-4685-9cc5-4268464a084a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d941813a-eadc-4792-b517-55aca43ccabe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "005a3502-b8b7-4685-9cc5-4268464a084a",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "aa37ebea-e1c1-4e5a-8969-a9afb3b97a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "d95e21e6-93c5-4c42-9d6f-5d8865d5d0e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa37ebea-e1c1-4e5a-8969-a9afb3b97a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a072f044-9a10-407f-9217-ec74bed9e1d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa37ebea-e1c1-4e5a-8969-a9afb3b97a05",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "73cf302e-7c07-469a-875d-9e591c819de4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "8db8f9c4-4812-4d9d-b99e-4e8bd459c683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73cf302e-7c07-469a-875d-9e591c819de4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28568db6-037a-436b-b323-bef7906b18f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73cf302e-7c07-469a-875d-9e591c819de4",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "459d5b4e-2f16-4c8d-a054-ec81a159a6d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "b485f7de-d221-44bb-90ce-9e606bc59f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "459d5b4e-2f16-4c8d-a054-ec81a159a6d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57669925-ffdd-49b8-864f-b10a6de62687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "459d5b4e-2f16-4c8d-a054-ec81a159a6d2",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "713273e8-1711-40d1-be79-195eb98f5a50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "63aeecf7-1c00-4fa4-866a-969c85d78af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "713273e8-1711-40d1-be79-195eb98f5a50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "338bae77-a2df-4a25-b23f-90ec1c92b87e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "713273e8-1711-40d1-be79-195eb98f5a50",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "b27361b9-93a4-4502-8edd-476d71c83049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "99b26317-1183-4702-b41f-d656da6b3695",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b27361b9-93a4-4502-8edd-476d71c83049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7d5bf9-7301-4129-8f65-49394212249b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b27361b9-93a4-4502-8edd-476d71c83049",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "7bbab9cf-f8d8-47ab-9dbd-ea186ddfc7ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "2a6d861e-7286-4f43-a473-575223c6cafe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bbab9cf-f8d8-47ab-9dbd-ea186ddfc7ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12a090ed-6931-46f1-8dc7-3fafd7661171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bbab9cf-f8d8-47ab-9dbd-ea186ddfc7ec",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "3398d6cd-33e3-4fd1-b2bc-98fe64cf7e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "e0b60bde-7aaa-4b50-b3a3-ec13723f98b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3398d6cd-33e3-4fd1-b2bc-98fe64cf7e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e421658c-74b5-4550-a18e-c6932bce74e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3398d6cd-33e3-4fd1-b2bc-98fe64cf7e06",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "f164efa9-8c34-43ed-9352-fc9bf850b736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "e2028c1e-43fe-4e8b-a357-5b66f19237b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f164efa9-8c34-43ed-9352-fc9bf850b736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efbf143e-1c56-4783-be89-efbb875d108e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f164efa9-8c34-43ed-9352-fc9bf850b736",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "26614a3d-3d6b-4be0-bd94-096b1729081e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "9ef79173-297e-40f0-8285-3041cbc5bd93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26614a3d-3d6b-4be0-bd94-096b1729081e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8147865-345d-4099-bae6-11905e19adbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26614a3d-3d6b-4be0-bd94-096b1729081e",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "a3a8ff33-596f-4763-a012-fcfd105670b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "06ba4bb6-734a-4bb5-8709-70285f1f16c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a8ff33-596f-4763-a012-fcfd105670b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a694f458-55e6-42e3-af7d-53d930fb340b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a8ff33-596f-4763-a012-fcfd105670b3",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        },
        {
            "id": "540b28cd-2313-48dd-9fb8-cda8f41aa0bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "compositeImage": {
                "id": "7445ba67-4770-4560-942e-d79b081f99a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "540b28cd-2313-48dd-9fb8-cda8f41aa0bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a22fba15-a31b-4b32-9f01-fc1b6e6948be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "540b28cd-2313-48dd-9fb8-cda8f41aa0bf",
                    "LayerId": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4285f8c0-bdfb-4ad7-aa32-1763e0956cf9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fddc9e99-23e6-4728-b32c-3c52a958fdb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}