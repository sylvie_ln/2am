{
    "id": "21619823-f851-42e6-80ba-f9ea8998ab86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGuardiansShieldSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 11,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81785dea-85d5-4fa5-b40d-84f2e58ad0cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21619823-f851-42e6-80ba-f9ea8998ab86",
            "compositeImage": {
                "id": "f2c4e9d9-bb21-4722-9f16-5501404ae37a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81785dea-85d5-4fa5-b40d-84f2e58ad0cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f23658c-e48c-470c-b588-b20e35013831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81785dea-85d5-4fa5-b40d-84f2e58ad0cf",
                    "LayerId": "93f16776-7ae4-4f0d-bd67-f97aa32558c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "93f16776-7ae4-4f0d-bd67-f97aa32558c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21619823-f851-42e6-80ba-f9ea8998ab86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}