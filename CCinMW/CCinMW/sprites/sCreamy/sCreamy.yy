{
    "id": "0f5ac03b-6519-4396-a3df-5ea2c9f9fac3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCreamy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac90dc27-6278-4ae9-a4ca-7b7858dd3fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f5ac03b-6519-4396-a3df-5ea2c9f9fac3",
            "compositeImage": {
                "id": "6370c42c-8a42-425e-94bb-25a9470c31fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac90dc27-6278-4ae9-a4ca-7b7858dd3fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e42897d-dcc6-474f-a2d8-4f7bb6dcc3cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac90dc27-6278-4ae9-a4ca-7b7858dd3fa0",
                    "LayerId": "01d12911-88d3-49b2-bf9d-00af382e54d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "01d12911-88d3-49b2-bf9d-00af382e54d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f5ac03b-6519-4396-a3df-5ea2c9f9fac3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 9,
    "yorig": 8
}