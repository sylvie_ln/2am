{
    "id": "d6f688b8-dd4d-4d42-96ce-776174c8176b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBody",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b9deaa8-421d-4eac-bac2-c549a3e59d82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f688b8-dd4d-4d42-96ce-776174c8176b",
            "compositeImage": {
                "id": "74cf60e4-4b3e-4e51-9444-92b7e8e9dc0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b9deaa8-421d-4eac-bac2-c549a3e59d82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed14a0c0-2be4-448e-bea4-f8edfac3d99f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b9deaa8-421d-4eac-bac2-c549a3e59d82",
                    "LayerId": "d761e142-d0ea-4a7e-9428-0d0f743fa941"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d761e142-d0ea-4a7e-9428-0d0f743fa941",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6f688b8-dd4d-4d42-96ce-776174c8176b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}