{
    "id": "e70668b4-28c9-4f57-8d1d-c53f2facd516",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoringCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "340cec03-b202-4700-afeb-9f7e515d5d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e70668b4-28c9-4f57-8d1d-c53f2facd516",
            "compositeImage": {
                "id": "299b77ff-9a55-40ec-80a6-da692888b83e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "340cec03-b202-4700-afeb-9f7e515d5d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ae39e53-f707-49cb-b65f-22d0f98f0eaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "340cec03-b202-4700-afeb-9f7e515d5d61",
                    "LayerId": "a654639a-2fc9-4bc1-b4ac-166a5a65746f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a654639a-2fc9-4bc1-b4ac-166a5a65746f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e70668b4-28c9-4f57-8d1d-c53f2facd516",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}