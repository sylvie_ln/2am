{
    "id": "e46334bb-95b8-41c3-91b7-a4060f02ef05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCordlessFuneral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c43b5398-4815-4ed1-bce7-461f2d25b229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e46334bb-95b8-41c3-91b7-a4060f02ef05",
            "compositeImage": {
                "id": "07e55394-e902-437c-898b-f781f3584bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c43b5398-4815-4ed1-bce7-461f2d25b229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ac5b36-84be-4ca0-a797-28fe65492fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c43b5398-4815-4ed1-bce7-461f2d25b229",
                    "LayerId": "cc9428b4-287f-4478-8551-345e517a48e5"
                }
            ]
        },
        {
            "id": "d1f28f3e-6109-486d-8b71-ddb3ce6f7f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e46334bb-95b8-41c3-91b7-a4060f02ef05",
            "compositeImage": {
                "id": "db5a1939-e4bd-4941-ba1e-86aa447ab269",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f28f3e-6109-486d-8b71-ddb3ce6f7f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6d4f0c3-e7a1-47cd-8d30-c2e0462786a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f28f3e-6109-486d-8b71-ddb3ce6f7f76",
                    "LayerId": "cc9428b4-287f-4478-8551-345e517a48e5"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "cc9428b4-287f-4478-8551-345e517a48e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e46334bb-95b8-41c3-91b7-a4060f02ef05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}