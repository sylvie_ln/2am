{
    "id": "1e907b34-cde3-445d-a233-70c967e3d9b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecraftPoints",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65e13a3a-728f-4616-8792-2a8141c0a07d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e907b34-cde3-445d-a233-70c967e3d9b5",
            "compositeImage": {
                "id": "27feefec-d565-4e2f-83f7-cb562d9a3b5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65e13a3a-728f-4616-8792-2a8141c0a07d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b25388-3c60-4162-bed0-bf5898880974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65e13a3a-728f-4616-8792-2a8141c0a07d",
                    "LayerId": "9575e600-4dd8-4da4-b9d7-aa339a47e7a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9575e600-4dd8-4da4-b9d7-aa339a47e7a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e907b34-cde3-445d-a233-70c967e3d9b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}