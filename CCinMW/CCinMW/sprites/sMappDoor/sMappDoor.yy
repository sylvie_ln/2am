{
    "id": "d7b8ed7c-b296-4629-97ae-0e03bdbac09e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMappDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "628ce35e-1f09-4c72-a0b9-a05d53bf68d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7b8ed7c-b296-4629-97ae-0e03bdbac09e",
            "compositeImage": {
                "id": "9ba301e8-355f-4633-be3c-628637f2056c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "628ce35e-1f09-4c72-a0b9-a05d53bf68d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81aa8e69-e27e-47ec-b991-0d3226c9072d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "628ce35e-1f09-4c72-a0b9-a05d53bf68d3",
                    "LayerId": "d912d195-0d7b-4072-98bc-96bebfbd20f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "d912d195-0d7b-4072-98bc-96bebfbd20f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7b8ed7c-b296-4629-97ae-0e03bdbac09e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 2
}