{
    "id": "a6870c77-a330-40ec-9529-2d909ad9d041",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLoadBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3ac02f5-8152-4d05-ae9f-c10a7bd13f60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6870c77-a330-40ec-9529-2d909ad9d041",
            "compositeImage": {
                "id": "f390a80e-4b5b-46e1-b952-4f5b2484f19c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3ac02f5-8152-4d05-ae9f-c10a7bd13f60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10fb4a75-7f1b-4453-9679-717861acb44d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3ac02f5-8152-4d05-ae9f-c10a7bd13f60",
                    "LayerId": "4e983286-4251-4ff2-bff9-dcb0a706e47b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4e983286-4251-4ff2-bff9-dcb0a706e47b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6870c77-a330-40ec-9529-2d909ad9d041",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}