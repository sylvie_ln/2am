{
    "id": "83a62301-4765-460d-8f78-21e1ea647f89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWideShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fdce533-d740-4c62-92c3-c777b48f06fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "compositeImage": {
                "id": "7e70d08a-de58-42a3-8a21-e0e78d7939cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fdce533-d740-4c62-92c3-c777b48f06fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e35cd6d-15a1-4931-b765-150ed02264cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fdce533-d740-4c62-92c3-c777b48f06fb",
                    "LayerId": "5bcb521e-b5ef-47a6-98fd-7996c8b3ced5"
                },
                {
                    "id": "ec5f65f8-9cf5-4569-a1a2-e3e2c8840d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fdce533-d740-4c62-92c3-c777b48f06fb",
                    "LayerId": "2db1decf-0574-43ea-865c-23adde74fc04"
                }
            ]
        },
        {
            "id": "2b211fc9-910c-4e45-89e4-e4745678c53e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "compositeImage": {
                "id": "a230b114-256b-4fd4-baaf-0c6411f8ff9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b211fc9-910c-4e45-89e4-e4745678c53e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "561f64cb-56c6-4a74-9f9d-625e4d8a76a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b211fc9-910c-4e45-89e4-e4745678c53e",
                    "LayerId": "2db1decf-0574-43ea-865c-23adde74fc04"
                },
                {
                    "id": "902d8faf-991f-42dc-b75c-4fab608dc688",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b211fc9-910c-4e45-89e4-e4745678c53e",
                    "LayerId": "5bcb521e-b5ef-47a6-98fd-7996c8b3ced5"
                }
            ]
        },
        {
            "id": "5c7ac24f-c749-4615-8b96-a51f22d03ff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "compositeImage": {
                "id": "3ddd7e5c-3f5a-4c6a-9c88-b0cf89e6bb9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7ac24f-c749-4615-8b96-a51f22d03ff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e98d778a-0651-4d8c-8928-0c2a3574d0ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7ac24f-c749-4615-8b96-a51f22d03ff9",
                    "LayerId": "2db1decf-0574-43ea-865c-23adde74fc04"
                },
                {
                    "id": "3faaf1c2-c429-4265-bb54-2ac80e7a4557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7ac24f-c749-4615-8b96-a51f22d03ff9",
                    "LayerId": "5bcb521e-b5ef-47a6-98fd-7996c8b3ced5"
                }
            ]
        },
        {
            "id": "5e7fa023-5605-4258-8025-a60e73d83ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "compositeImage": {
                "id": "9878ec99-191e-4d16-ac96-2d6536c8a092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7fa023-5605-4258-8025-a60e73d83ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaa4ad45-b244-4f7f-a2c6-8ff265b653fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7fa023-5605-4258-8025-a60e73d83ee7",
                    "LayerId": "2db1decf-0574-43ea-865c-23adde74fc04"
                },
                {
                    "id": "278a1ee9-3dc6-404f-bf9a-4c1885d84280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7fa023-5605-4258-8025-a60e73d83ee7",
                    "LayerId": "5bcb521e-b5ef-47a6-98fd-7996c8b3ced5"
                }
            ]
        },
        {
            "id": "a630e22e-576f-4328-9cab-8dace31e609c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "compositeImage": {
                "id": "2d0386a6-3e64-4bbc-81c9-8114c48bb30a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a630e22e-576f-4328-9cab-8dace31e609c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c7d5bc-828b-4857-97c1-ae8f692be9fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a630e22e-576f-4328-9cab-8dace31e609c",
                    "LayerId": "2db1decf-0574-43ea-865c-23adde74fc04"
                },
                {
                    "id": "60c23666-44bd-475e-b0f7-659a97250a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a630e22e-576f-4328-9cab-8dace31e609c",
                    "LayerId": "5bcb521e-b5ef-47a6-98fd-7996c8b3ced5"
                }
            ]
        },
        {
            "id": "2ea8c98e-8a9e-48d7-a077-d731a08b9329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "compositeImage": {
                "id": "246fd9e6-eec7-445c-9be3-f23e35517e00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ea8c98e-8a9e-48d7-a077-d731a08b9329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "191166ec-2902-4632-9c39-2a5438fb2d52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ea8c98e-8a9e-48d7-a077-d731a08b9329",
                    "LayerId": "2db1decf-0574-43ea-865c-23adde74fc04"
                },
                {
                    "id": "d5588f7b-19aa-45fe-959b-a05a1aa78146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ea8c98e-8a9e-48d7-a077-d731a08b9329",
                    "LayerId": "5bcb521e-b5ef-47a6-98fd-7996c8b3ced5"
                }
            ]
        },
        {
            "id": "1af45b31-750f-4e2c-9b84-3a1f9b5a1e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "compositeImage": {
                "id": "4d3dec76-99cb-43ff-9fd6-0ee13fbb0385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1af45b31-750f-4e2c-9b84-3a1f9b5a1e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d947f3-9ec3-427d-aae7-0be2b986e10a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1af45b31-750f-4e2c-9b84-3a1f9b5a1e56",
                    "LayerId": "2db1decf-0574-43ea-865c-23adde74fc04"
                },
                {
                    "id": "183dbb7e-6a69-4b1c-b9b4-0b2e7af315dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1af45b31-750f-4e2c-9b84-3a1f9b5a1e56",
                    "LayerId": "5bcb521e-b5ef-47a6-98fd-7996c8b3ced5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2db1decf-0574-43ea-865c-23adde74fc04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5bcb521e-b5ef-47a6-98fd-7996c8b3ced5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}