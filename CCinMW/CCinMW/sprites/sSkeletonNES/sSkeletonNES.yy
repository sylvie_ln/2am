{
    "id": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSkeletonNES",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "133062bb-60b7-4f7a-b250-67e7bcc3106d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "eb90db43-2ce0-4697-97e5-5b3ff837ae2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "133062bb-60b7-4f7a-b250-67e7bcc3106d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a383543-d07d-404c-a0e9-8d52cbdc2a81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "133062bb-60b7-4f7a-b250-67e7bcc3106d",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "67893cfb-b338-43ab-b063-e6d7f4956a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "98392abe-7b68-4913-9e02-3b810ab4137a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67893cfb-b338-43ab-b063-e6d7f4956a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff4c7c84-2724-4702-8410-6632a5aff084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67893cfb-b338-43ab-b063-e6d7f4956a86",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "07ba021e-76c9-4fe8-aaeb-3d0b04c169f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "51e60f72-6058-4008-923a-36ba59ac9ff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07ba021e-76c9-4fe8-aaeb-3d0b04c169f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbe1eb53-60c2-4cbe-b182-f71e91662b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07ba021e-76c9-4fe8-aaeb-3d0b04c169f1",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "3aff26a5-5179-474a-8687-8bb9231a3293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "8ba5120d-9465-47d5-876c-87d25330e3dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aff26a5-5179-474a-8687-8bb9231a3293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "746d6217-0ee9-4571-98e7-255aac44e904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aff26a5-5179-474a-8687-8bb9231a3293",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "4cd6191e-39e5-42be-b7fa-c92abce16859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "ad56d490-f7eb-4a10-a4e0-dbbca0350ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cd6191e-39e5-42be-b7fa-c92abce16859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c6977b6-40e1-4e77-a4c5-d359b11182d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cd6191e-39e5-42be-b7fa-c92abce16859",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "da40d8e3-cc40-4f5b-8367-797f714e5e3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "21fc22c7-8ad3-4d9e-865d-e9ef45944fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da40d8e3-cc40-4f5b-8367-797f714e5e3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96fbe4dd-60a4-48f9-b953-9e0c1138c7e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da40d8e3-cc40-4f5b-8367-797f714e5e3b",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "b4a9d19a-e628-4257-a6a2-652df4bf605c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "0f140d79-bd3d-4e75-8a2d-fb60d0f2c60f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a9d19a-e628-4257-a6a2-652df4bf605c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39db51f3-9aba-44a8-8bed-788112b5651a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a9d19a-e628-4257-a6a2-652df4bf605c",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "f2bc658b-11cf-444b-9ae3-c1056bbc46d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "4b5009ef-4f81-43be-9fba-c41f8aefba36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2bc658b-11cf-444b-9ae3-c1056bbc46d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ecaee1-c1fa-4a77-b5eb-9269ab571433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2bc658b-11cf-444b-9ae3-c1056bbc46d8",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "dc36bfee-e46d-4268-88dd-9346310e150c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "4410620c-9f56-401e-a808-0d054d9b865a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc36bfee-e46d-4268-88dd-9346310e150c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8a7d97-bd91-4889-837c-356c91bf7583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc36bfee-e46d-4268-88dd-9346310e150c",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "58ff2402-cb21-46e3-aaf8-316cabe32e9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "1368ae2d-635d-4c44-b4b2-686c8672e4f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58ff2402-cb21-46e3-aaf8-316cabe32e9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23eabb36-2dc3-4df5-a072-b887d01aad42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58ff2402-cb21-46e3-aaf8-316cabe32e9a",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "8e838b1d-8404-4dd1-92e9-839d0ea15feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "821d28ed-a92e-4c43-b6f1-b006116449be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e838b1d-8404-4dd1-92e9-839d0ea15feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bef7df0-1314-4397-b624-c4dcf66ddf8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e838b1d-8404-4dd1-92e9-839d0ea15feb",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        },
        {
            "id": "1d2a296b-4a84-44b4-8061-84fa2c990a60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "compositeImage": {
                "id": "ee03aff2-976c-43da-9ee8-d1a652b8bc63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d2a296b-4a84-44b4-8061-84fa2c990a60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6d943d-6a25-4cf1-807f-503f6e98c69c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d2a296b-4a84-44b4-8061-84fa2c990a60",
                    "LayerId": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0e64870a-3f1f-4bfe-a74a-bbf85ee5aef7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad5f1304-84be-4113-9c6a-48ad3006a7f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 8
}