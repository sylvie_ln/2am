{
    "id": "25d857e1-73a8-418f-8cfe-aaed586da54f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBallMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9c1aec7-777a-4e85-ab95-9fc791dfa915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25d857e1-73a8-418f-8cfe-aaed586da54f",
            "compositeImage": {
                "id": "2996880b-a5bb-4a61-9837-20b50fabb914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c1aec7-777a-4e85-ab95-9fc791dfa915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d900c24-b7e0-40f0-82a2-30c7ac822095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c1aec7-777a-4e85-ab95-9fc791dfa915",
                    "LayerId": "15a4b9b7-463c-416d-84bf-8a2ac6254e7b"
                },
                {
                    "id": "0d502906-3c27-4fb9-8ee6-a1ac397c6157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c1aec7-777a-4e85-ab95-9fc791dfa915",
                    "LayerId": "b7e19204-f91e-4a1d-b140-14768dac18ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "15a4b9b7-463c-416d-84bf-8a2ac6254e7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25d857e1-73a8-418f-8cfe-aaed586da54f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b7e19204-f91e-4a1d-b140-14768dac18ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25d857e1-73a8-418f-8cfe-aaed586da54f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}