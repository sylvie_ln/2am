{
    "id": "cd6f0257-d316-4ef8-8ed0-bc21b847af16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEivlysStill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 20,
    "bbox_right": 41,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70a64e12-4375-497e-b6b7-ab87b7e67b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd6f0257-d316-4ef8-8ed0-bc21b847af16",
            "compositeImage": {
                "id": "76ebe6aa-a7a8-4666-bf75-8437c0614c3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70a64e12-4375-497e-b6b7-ab87b7e67b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9883ff8-be8c-4181-9891-eb886d7722d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70a64e12-4375-497e-b6b7-ab87b7e67b1d",
                    "LayerId": "942d9956-0503-475e-969a-a82981bc0cb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "942d9956-0503-475e-969a-a82981bc0cb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd6f0257-d316-4ef8-8ed0-bc21b847af16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 19,
    "yorig": 27
}