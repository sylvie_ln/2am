{
    "id": "60026b4f-630e-4621-98a9-ecb9590aa304",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPuzzlePieceSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 100,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a315bed-0d51-4dbd-a8f7-b2dadb719c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60026b4f-630e-4621-98a9-ecb9590aa304",
            "compositeImage": {
                "id": "55b21166-34d1-4594-ad31-3c7826fb71d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a315bed-0d51-4dbd-a8f7-b2dadb719c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d492ff6b-84c2-4075-9123-453691504b4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a315bed-0d51-4dbd-a8f7-b2dadb719c18",
                    "LayerId": "c5418183-eecd-48f2-870e-fbd0fc1278e5"
                },
                {
                    "id": "921dec4d-4446-4612-82c1-806376eeff52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a315bed-0d51-4dbd-a8f7-b2dadb719c18",
                    "LayerId": "a6f327e0-1819-4f73-af7a-235f30e22fee"
                }
            ]
        },
        {
            "id": "1d33aaf9-f3c5-442e-a960-c7b7a0cfdfe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60026b4f-630e-4621-98a9-ecb9590aa304",
            "compositeImage": {
                "id": "4ddaf92a-cd41-4134-9b63-e747ad4a8e0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d33aaf9-f3c5-442e-a960-c7b7a0cfdfe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9459024c-a73d-453c-970b-34f9756e4c05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d33aaf9-f3c5-442e-a960-c7b7a0cfdfe1",
                    "LayerId": "c5418183-eecd-48f2-870e-fbd0fc1278e5"
                },
                {
                    "id": "6aa86af2-7a09-45ed-94ac-158b79382f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d33aaf9-f3c5-442e-a960-c7b7a0cfdfe1",
                    "LayerId": "a6f327e0-1819-4f73-af7a-235f30e22fee"
                }
            ]
        },
        {
            "id": "55ab5c9b-a59e-4ec5-898c-e3c15e0466d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60026b4f-630e-4621-98a9-ecb9590aa304",
            "compositeImage": {
                "id": "1e763677-5ab3-40ba-9d28-5ae02dcbdd2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ab5c9b-a59e-4ec5-898c-e3c15e0466d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4d310be-79c5-4ce0-9432-c48e37ca10d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ab5c9b-a59e-4ec5-898c-e3c15e0466d4",
                    "LayerId": "c5418183-eecd-48f2-870e-fbd0fc1278e5"
                },
                {
                    "id": "f83be187-bbb6-4cb8-bbf0-9613c48d4c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ab5c9b-a59e-4ec5-898c-e3c15e0466d4",
                    "LayerId": "a6f327e0-1819-4f73-af7a-235f30e22fee"
                }
            ]
        },
        {
            "id": "f2d62f35-a285-4120-bba4-8bbb9cc7126b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60026b4f-630e-4621-98a9-ecb9590aa304",
            "compositeImage": {
                "id": "4f9eb1f3-39d7-4f42-9b43-22a4fdbaf475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d62f35-a285-4120-bba4-8bbb9cc7126b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e23cc8b-28b5-43f1-9249-ec1c8ad578d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d62f35-a285-4120-bba4-8bbb9cc7126b",
                    "LayerId": "c5418183-eecd-48f2-870e-fbd0fc1278e5"
                },
                {
                    "id": "ba365eb8-a0a6-4477-88df-05f2018a58aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d62f35-a285-4120-bba4-8bbb9cc7126b",
                    "LayerId": "a6f327e0-1819-4f73-af7a-235f30e22fee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c5418183-eecd-48f2-870e-fbd0fc1278e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60026b4f-630e-4621-98a9-ecb9590aa304",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 25,
            "visible": false
        },
        {
            "id": "a6f327e0-1819-4f73-af7a-235f30e22fee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60026b4f-630e-4621-98a9-ecb9590aa304",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}