{
    "id": "e4476f6e-7c5f-4ab8-9f10-0ed3b932ede8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sThrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 3,
    "bbox_right": 77,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bb3d843-a1b1-4f55-b16d-bcd3898634e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4476f6e-7c5f-4ab8-9f10-0ed3b932ede8",
            "compositeImage": {
                "id": "e51a32e1-00b5-4312-8bcc-0ad925eb92b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb3d843-a1b1-4f55-b16d-bcd3898634e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3228bdf-49c2-4ad6-b0c6-4d494def06f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb3d843-a1b1-4f55-b16d-bcd3898634e1",
                    "LayerId": "c5f25e4e-fc34-4555-b334-fd58766a22b8"
                }
            ]
        },
        {
            "id": "bebec4a6-de9a-4841-85a6-a04a6f473256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4476f6e-7c5f-4ab8-9f10-0ed3b932ede8",
            "compositeImage": {
                "id": "dedc5e1e-01ab-432b-9bb0-f0c6c5336f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bebec4a6-de9a-4841-85a6-a04a6f473256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32000d6e-8606-4a9f-9272-8041ca87a9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bebec4a6-de9a-4841-85a6-a04a6f473256",
                    "LayerId": "c5f25e4e-fc34-4555-b334-fd58766a22b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c5f25e4e-fc34-4555-b334-fd58766a22b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4476f6e-7c5f-4ab8-9f10-0ed3b932ede8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 47
}