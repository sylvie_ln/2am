{
    "id": "ff544cc4-6e7b-4d7c-8347-7bbd5199c371",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyDieLand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0db62674-e3fc-4482-832e-70c07fb7530a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff544cc4-6e7b-4d7c-8347-7bbd5199c371",
            "compositeImage": {
                "id": "0ebf420b-f853-4181-9e7f-114349b6254b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0db62674-e3fc-4482-832e-70c07fb7530a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4e4b164-68df-4643-8783-f308fcad8ce8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0db62674-e3fc-4482-832e-70c07fb7530a",
                    "LayerId": "eb609f2f-df5a-4112-a234-aab7d47f4016"
                }
            ]
        },
        {
            "id": "7acea42a-f104-4f2f-adb5-418d47964cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff544cc4-6e7b-4d7c-8347-7bbd5199c371",
            "compositeImage": {
                "id": "0153d1b0-4f25-4de5-89b7-7d1c50c00ab1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7acea42a-f104-4f2f-adb5-418d47964cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27ecf6a4-68cc-426a-97af-fd78d8416bfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7acea42a-f104-4f2f-adb5-418d47964cca",
                    "LayerId": "eb609f2f-df5a-4112-a234-aab7d47f4016"
                }
            ]
        },
        {
            "id": "68a24faa-6580-4387-bea1-e42020edcd5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff544cc4-6e7b-4d7c-8347-7bbd5199c371",
            "compositeImage": {
                "id": "93ad5ad4-37e3-486d-9229-09047cccb76d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68a24faa-6580-4387-bea1-e42020edcd5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5908276-28ea-42f5-ab1a-e5fe761cd95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68a24faa-6580-4387-bea1-e42020edcd5d",
                    "LayerId": "eb609f2f-df5a-4112-a234-aab7d47f4016"
                }
            ]
        },
        {
            "id": "7f58060d-1f75-44df-8e13-23d5e84f95db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff544cc4-6e7b-4d7c-8347-7bbd5199c371",
            "compositeImage": {
                "id": "15987037-80d8-4403-81df-0cb119d621ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f58060d-1f75-44df-8e13-23d5e84f95db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5388481e-c629-4665-8cd7-230efdd1f641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f58060d-1f75-44df-8e13-23d5e84f95db",
                    "LayerId": "eb609f2f-df5a-4112-a234-aab7d47f4016"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "eb609f2f-df5a-4112-a234-aab7d47f4016",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff544cc4-6e7b-4d7c-8347-7bbd5199c371",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}