{
    "id": "c8aa596e-eaaa-4125-898c-c84e2c695eb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLaborie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40d433b3-55a1-4ab3-8ff7-f25bc57f6043",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8aa596e-eaaa-4125-898c-c84e2c695eb8",
            "compositeImage": {
                "id": "1148eb55-0f1c-44ce-82ea-f42f985c41f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40d433b3-55a1-4ab3-8ff7-f25bc57f6043",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f832f49a-86f2-4f24-803f-db7cbb195265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d433b3-55a1-4ab3-8ff7-f25bc57f6043",
                    "LayerId": "34eb77b0-e08d-4bfa-bf2d-a401a63618fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "34eb77b0-e08d-4bfa-bf2d-a401a63618fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8aa596e-eaaa-4125-898c-c84e2c695eb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 10,
    "yorig": 8
}