{
    "id": "c0d6f466-aa84-424e-ab19-65879d60bc78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecraftSky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19a9da91-309b-443c-acee-ee5fada55737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0d6f466-aa84-424e-ab19-65879d60bc78",
            "compositeImage": {
                "id": "52fde4f1-5552-4ae4-b964-a33ed4417d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a9da91-309b-443c-acee-ee5fada55737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f20256b4-6d2f-49f2-bdcd-c65aba6055f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a9da91-309b-443c-acee-ee5fada55737",
                    "LayerId": "f1f121bd-3e9f-4ad6-a22b-2c9274b98986"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f1f121bd-3e9f-4ad6-a22b-2c9274b98986",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0d6f466-aa84-424e-ab19-65879d60bc78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}