{
    "id": "fdc8880d-710b-4d93-991c-d3b3d56805de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWizaron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e648eab-90ff-4e43-a2f5-4fa0934a771e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc8880d-710b-4d93-991c-d3b3d56805de",
            "compositeImage": {
                "id": "3cdbceaa-c1eb-40ed-ac72-b6da29791a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e648eab-90ff-4e43-a2f5-4fa0934a771e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0316f497-2d8e-4fac-a92d-49c81a39ee52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e648eab-90ff-4e43-a2f5-4fa0934a771e",
                    "LayerId": "e9ba11ab-efe9-4227-9a69-fa82d7449ffd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e9ba11ab-efe9-4227-9a69-fa82d7449ffd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdc8880d-710b-4d93-991c-d3b3d56805de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}