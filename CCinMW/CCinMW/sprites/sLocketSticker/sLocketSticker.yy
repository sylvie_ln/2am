{
    "id": "adcf3c3b-1e38-4dc8-b3b2-25350233751b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLocketSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1814d6bd-53c9-455c-b960-00a3ca5d77aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adcf3c3b-1e38-4dc8-b3b2-25350233751b",
            "compositeImage": {
                "id": "eac1d352-a381-491c-ba8b-4d2c3aef88bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1814d6bd-53c9-455c-b960-00a3ca5d77aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6bc70bc-0f2a-4137-8535-ec359e547a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1814d6bd-53c9-455c-b960-00a3ca5d77aa",
                    "LayerId": "b94a37a9-bb18-47a4-9a74-67aba70820f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b94a37a9-bb18-47a4-9a74-67aba70820f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adcf3c3b-1e38-4dc8-b3b2-25350233751b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}