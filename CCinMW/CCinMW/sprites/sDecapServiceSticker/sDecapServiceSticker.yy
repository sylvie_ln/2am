{
    "id": "9340e4c9-13e3-4564-9f44-345424aa7a5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDecapServiceSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "212cadc3-2186-4a5d-9837-87c6f2da5592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9340e4c9-13e3-4564-9f44-345424aa7a5f",
            "compositeImage": {
                "id": "28e29973-e09d-4bda-ab69-068ca37f13cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "212cadc3-2186-4a5d-9837-87c6f2da5592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3013e5a5-5dd1-43d8-9a26-61016127f4e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "212cadc3-2186-4a5d-9837-87c6f2da5592",
                    "LayerId": "db505905-5287-4fdc-b158-5f45290f72b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "db505905-5287-4fdc-b158-5f45290f72b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9340e4c9-13e3-4564-9f44-345424aa7a5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}