{
    "id": "af96722a-2e32-43f4-a2a4-082e9f31e904",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCaptiveAutomaton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86243635-3698-427f-be99-37f4ba2f8981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af96722a-2e32-43f4-a2a4-082e9f31e904",
            "compositeImage": {
                "id": "6f46e199-eb08-4c9a-800f-882663d5eff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86243635-3698-427f-be99-37f4ba2f8981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "500d0b77-149d-4ec9-9301-35d36a3f3dfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86243635-3698-427f-be99-37f4ba2f8981",
                    "LayerId": "640d735e-4106-41e4-bc81-cc327bbae600"
                }
            ]
        },
        {
            "id": "273cffa6-7db9-4e38-ab5d-a5e296f2cfe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af96722a-2e32-43f4-a2a4-082e9f31e904",
            "compositeImage": {
                "id": "17fa0a0c-8796-4392-874e-59de5a882d40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273cffa6-7db9-4e38-ab5d-a5e296f2cfe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87080764-471a-44e8-b3fc-8b12882fc611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273cffa6-7db9-4e38-ab5d-a5e296f2cfe0",
                    "LayerId": "640d735e-4106-41e4-bc81-cc327bbae600"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "640d735e-4106-41e4-bc81-cc327bbae600",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af96722a-2e32-43f4-a2a4-082e9f31e904",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}