{
    "id": "35f60ce6-ff1e-4a84-bf64-9f6b27c83439",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7db9de09-21d7-497e-a313-8a192c84a9fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35f60ce6-ff1e-4a84-bf64-9f6b27c83439",
            "compositeImage": {
                "id": "4bf300db-0918-4c22-86c5-7bdb0630596d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db9de09-21d7-497e-a313-8a192c84a9fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2a9ace2-f0ae-4a4b-885e-26b22ba144bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db9de09-21d7-497e-a313-8a192c84a9fb",
                    "LayerId": "db834090-a961-4783-90b3-3b8fac232fae"
                }
            ]
        },
        {
            "id": "d3f58661-8f4e-4a45-bc53-be836ced4281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35f60ce6-ff1e-4a84-bf64-9f6b27c83439",
            "compositeImage": {
                "id": "72517e73-3a8f-4e19-bf40-1ee4ae7593a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3f58661-8f4e-4a45-bc53-be836ced4281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9b07b0-85a5-4e43-b9ce-7a89baf3011c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3f58661-8f4e-4a45-bc53-be836ced4281",
                    "LayerId": "db834090-a961-4783-90b3-3b8fac232fae"
                }
            ]
        },
        {
            "id": "aa00c503-bf37-4805-8b2d-a1ed83f080e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35f60ce6-ff1e-4a84-bf64-9f6b27c83439",
            "compositeImage": {
                "id": "19f37c5c-f479-484d-90a4-0f89b51bada2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa00c503-bf37-4805-8b2d-a1ed83f080e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4bddb85-5deb-4df8-b09c-e0da1a9c0558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa00c503-bf37-4805-8b2d-a1ed83f080e8",
                    "LayerId": "db834090-a961-4783-90b3-3b8fac232fae"
                }
            ]
        },
        {
            "id": "0fdb40b7-95c1-446f-9ec9-4af6beb39ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35f60ce6-ff1e-4a84-bf64-9f6b27c83439",
            "compositeImage": {
                "id": "1468ae81-c46d-468a-a201-04fd98217ecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fdb40b7-95c1-446f-9ec9-4af6beb39ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "727f5a0a-0c79-40e3-8682-c7871a0e0587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fdb40b7-95c1-446f-9ec9-4af6beb39ed4",
                    "LayerId": "db834090-a961-4783-90b3-3b8fac232fae"
                }
            ]
        },
        {
            "id": "d3cdf224-98ca-4e0f-93f7-351adefc4075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35f60ce6-ff1e-4a84-bf64-9f6b27c83439",
            "compositeImage": {
                "id": "d51378e2-66ca-4f85-b3c1-57e0e1e17457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3cdf224-98ca-4e0f-93f7-351adefc4075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de2e0fb7-c41c-480d-a9ec-7504bc808604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3cdf224-98ca-4e0f-93f7-351adefc4075",
                    "LayerId": "db834090-a961-4783-90b3-3b8fac232fae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "db834090-a961-4783-90b3-3b8fac232fae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35f60ce6-ff1e-4a84-bf64-9f6b27c83439",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}