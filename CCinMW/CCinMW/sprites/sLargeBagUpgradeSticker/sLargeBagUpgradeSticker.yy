{
    "id": "229fed73-e827-4963-ad2f-2fe709ad4a18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLargeBagUpgradeSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1e1a58c-8d46-416c-a5b9-f8f359f0869b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "229fed73-e827-4963-ad2f-2fe709ad4a18",
            "compositeImage": {
                "id": "7459574b-2110-46d8-96d8-af556e3f4486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e1a58c-8d46-416c-a5b9-f8f359f0869b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dc0235e-4093-4033-a684-1f53c93f37ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e1a58c-8d46-416c-a5b9-f8f359f0869b",
                    "LayerId": "a7565730-db48-44a7-9428-959451161154"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a7565730-db48-44a7-9428-959451161154",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "229fed73-e827-4963-ad2f-2fe709ad4a18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}