{
    "id": "f42151b7-2aff-499f-b1a1-726793273907",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTombstoner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "743c03f4-5561-4dfd-9884-4249d9a9e252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f42151b7-2aff-499f-b1a1-726793273907",
            "compositeImage": {
                "id": "4d8eb44e-259e-4a1e-a634-736a942a3aa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "743c03f4-5561-4dfd-9884-4249d9a9e252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "857a4571-9df6-4ff6-b7d0-1f3c431ba56f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "743c03f4-5561-4dfd-9884-4249d9a9e252",
                    "LayerId": "cdd4766b-44b9-4f34-b4e2-6fbedb0c51e4"
                },
                {
                    "id": "394d6817-713a-48d7-b7b8-9ebf8f5064f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "743c03f4-5561-4dfd-9884-4249d9a9e252",
                    "LayerId": "0fda7889-3434-40f5-b328-9b7921df21fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cdd4766b-44b9-4f34-b4e2-6fbedb0c51e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f42151b7-2aff-499f-b1a1-726793273907",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "0fda7889-3434-40f5-b328-9b7921df21fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f42151b7-2aff-499f-b1a1-726793273907",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4294901889,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}