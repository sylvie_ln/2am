{
    "id": "06d53fd4-29a7-4622-9130-16647d17ff49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecartLicenseSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fbad700-773c-4798-a6b2-c22353e7e221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06d53fd4-29a7-4622-9130-16647d17ff49",
            "compositeImage": {
                "id": "3a301a98-cc66-4315-b4ca-f9886f8599f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fbad700-773c-4798-a6b2-c22353e7e221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89cdf672-ae88-432e-af30-0f13b36c0446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fbad700-773c-4798-a6b2-c22353e7e221",
                    "LayerId": "17bbd07b-c5aa-4fce-b04e-5dfbabda8303"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "17bbd07b-c5aa-4fce-b04e-5dfbabda8303",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06d53fd4-29a7-4622-9130-16647d17ff49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 12
}