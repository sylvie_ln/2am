{
    "id": "5e00ff79-31ae-4d78-bda1-1c0d22d4a64f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHoverHeels",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18f6fa10-8795-44de-88c6-cafac7f56820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e00ff79-31ae-4d78-bda1-1c0d22d4a64f",
            "compositeImage": {
                "id": "12ec9a8c-39ab-4264-8779-59da50b1bb01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f6fa10-8795-44de-88c6-cafac7f56820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ecda3a3-07dc-4707-b26e-4fb63d4e298f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f6fa10-8795-44de-88c6-cafac7f56820",
                    "LayerId": "e8148b08-e43c-47dd-8193-d02e62f76954"
                },
                {
                    "id": "a90b4479-ff9b-4e1d-962e-245642c04bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f6fa10-8795-44de-88c6-cafac7f56820",
                    "LayerId": "52033e9f-8b58-4252-a90f-b1246e75a018"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e8148b08-e43c-47dd-8193-d02e62f76954",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e00ff79-31ae-4d78-bda1-1c0d22d4a64f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "52033e9f-8b58-4252-a90f-b1246e75a018",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e00ff79-31ae-4d78-bda1-1c0d22d4a64f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}