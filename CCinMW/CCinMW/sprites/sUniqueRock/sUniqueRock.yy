{
    "id": "1fd1a2de-75a7-4eb6-8017-f002c92248d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUniqueRock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b43ac9ed-b3bd-40e8-87e9-4ae94b113077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fd1a2de-75a7-4eb6-8017-f002c92248d0",
            "compositeImage": {
                "id": "6b66d12e-c3ce-4bd5-a7c5-e2477c0c0da1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b43ac9ed-b3bd-40e8-87e9-4ae94b113077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bcf3d3b-acfc-48b4-87d4-31ea8a03a7e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b43ac9ed-b3bd-40e8-87e9-4ae94b113077",
                    "LayerId": "b4d25113-d916-4d71-89cd-e7c90c340cab"
                },
                {
                    "id": "970a1069-b726-499e-b021-2304e24d143c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b43ac9ed-b3bd-40e8-87e9-4ae94b113077",
                    "LayerId": "a1b2c846-47d7-4c46-8656-747d3bf6504a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a1b2c846-47d7-4c46-8656-747d3bf6504a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fd1a2de-75a7-4eb6-8017-f002c92248d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b4d25113-d916-4d71-89cd-e7c90c340cab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fd1a2de-75a7-4eb6-8017-f002c92248d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}