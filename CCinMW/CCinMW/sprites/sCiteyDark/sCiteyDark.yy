{
    "id": "19b3a9f3-2a01-43c5-a223-eebc3eaa0b90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCiteyDark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f98f514-b5e4-4c6e-b6a7-5c42b0148658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19b3a9f3-2a01-43c5-a223-eebc3eaa0b90",
            "compositeImage": {
                "id": "d56dc052-5142-4afb-9abd-681fd84a2d20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f98f514-b5e4-4c6e-b6a7-5c42b0148658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b131786d-3541-43a1-8b47-4181abfe1de4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f98f514-b5e4-4c6e-b6a7-5c42b0148658",
                    "LayerId": "c76416e6-89bd-4d10-b319-c0a3aa0917c4"
                }
            ]
        },
        {
            "id": "c078285d-d139-476c-9f6b-61e0ebf3a356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19b3a9f3-2a01-43c5-a223-eebc3eaa0b90",
            "compositeImage": {
                "id": "210a9a52-9a0a-4cdc-8dd7-8fc870a40f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c078285d-d139-476c-9f6b-61e0ebf3a356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a554dfbf-686c-4a96-bef5-64babaf136bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c078285d-d139-476c-9f6b-61e0ebf3a356",
                    "LayerId": "c76416e6-89bd-4d10-b319-c0a3aa0917c4"
                }
            ]
        },
        {
            "id": "952ab90d-04a2-414d-bf69-d67f32223e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19b3a9f3-2a01-43c5-a223-eebc3eaa0b90",
            "compositeImage": {
                "id": "e0a9ec2e-f0b5-4d54-89c6-dc295000a88e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "952ab90d-04a2-414d-bf69-d67f32223e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3875ea25-cfce-41d2-bde3-b9325e255902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "952ab90d-04a2-414d-bf69-d67f32223e82",
                    "LayerId": "c76416e6-89bd-4d10-b319-c0a3aa0917c4"
                }
            ]
        },
        {
            "id": "39125aee-f401-4e92-9041-14bbfbcb4ec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19b3a9f3-2a01-43c5-a223-eebc3eaa0b90",
            "compositeImage": {
                "id": "4d632544-8ab9-4880-ab1a-894f874adb24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39125aee-f401-4e92-9041-14bbfbcb4ec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbb6bde5-cd24-489b-8f7a-62afc33fbc22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39125aee-f401-4e92-9041-14bbfbcb4ec8",
                    "LayerId": "c76416e6-89bd-4d10-b319-c0a3aa0917c4"
                }
            ]
        },
        {
            "id": "42a1afc8-c0f3-4f92-b85a-6871fd497126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19b3a9f3-2a01-43c5-a223-eebc3eaa0b90",
            "compositeImage": {
                "id": "183fc2fd-e2be-4689-af66-6c14be804163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a1afc8-c0f3-4f92-b85a-6871fd497126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecc6e538-7dee-4467-b522-dfed59c57db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a1afc8-c0f3-4f92-b85a-6871fd497126",
                    "LayerId": "c76416e6-89bd-4d10-b319-c0a3aa0917c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "c76416e6-89bd-4d10-b319-c0a3aa0917c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19b3a9f3-2a01-43c5-a223-eebc3eaa0b90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901929,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4284768238,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4284874819,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}