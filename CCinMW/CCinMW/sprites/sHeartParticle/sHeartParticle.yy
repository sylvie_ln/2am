{
    "id": "d125949d-d049-4d28-8e5f-25b9ae17ee23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeartParticle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3224b9c-437f-4de6-87ef-60cb8f6ebae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d125949d-d049-4d28-8e5f-25b9ae17ee23",
            "compositeImage": {
                "id": "dce32014-664e-4f2b-af00-bfdb11ece8f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3224b9c-437f-4de6-87ef-60cb8f6ebae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ef2c69c-0c76-445e-bd1c-de68f8b8e253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3224b9c-437f-4de6-87ef-60cb8f6ebae3",
                    "LayerId": "b1fc2873-a585-453c-998a-593f45e5ee5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b1fc2873-a585-453c-998a-593f45e5ee5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d125949d-d049-4d28-8e5f-25b9ae17ee23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}