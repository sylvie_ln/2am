{
    "id": "7b769393-b075-4f2f-ac61-307853b999e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHands",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98b9b16f-4d72-423c-9b10-fb599af9d490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "e4ee3a02-61c3-41e5-a4b9-38a2fb4bec62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b9b16f-4d72-423c-9b10-fb599af9d490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "775f97de-0662-43ee-9223-a7296a69549b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b9b16f-4d72-423c-9b10-fb599af9d490",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "2c19ea06-9264-410b-807a-6720aac1b925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "0f64f7b6-3250-49cc-9d84-26db496d454f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c19ea06-9264-410b-807a-6720aac1b925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9327eab4-9437-4608-b256-fdbb39be8912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c19ea06-9264-410b-807a-6720aac1b925",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "ccef36d1-797a-4d19-8b20-32c426b3ad24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "98b1644b-eb39-432b-8d4d-ac0a56f39499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccef36d1-797a-4d19-8b20-32c426b3ad24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "952eb830-c23b-4183-9c03-f32de6e3e2a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccef36d1-797a-4d19-8b20-32c426b3ad24",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "c3cd95d6-4bd4-44b2-8184-5833ceb80764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "01668ece-d095-4713-a521-28fae11466fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3cd95d6-4bd4-44b2-8184-5833ceb80764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961015d1-581f-424b-b2db-c9cc7e5c8ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3cd95d6-4bd4-44b2-8184-5833ceb80764",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "aba79001-2d4d-4b3e-ba3a-422b9b093a7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "92b80514-4bf5-45d0-b1d3-4e3c12510ea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aba79001-2d4d-4b3e-ba3a-422b9b093a7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0c9ff5-bf22-41d5-bb9b-8e018750f576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aba79001-2d4d-4b3e-ba3a-422b9b093a7f",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "9234e712-170e-49b0-bc5b-4994e2670683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "5afcfd05-6d30-4f0d-82a7-6dc6cef3f139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9234e712-170e-49b0-bc5b-4994e2670683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6afdd27e-5601-4022-92f8-b97486439a46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9234e712-170e-49b0-bc5b-4994e2670683",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "a40e91c2-a55c-460a-9a22-5289e76bcf03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "0369cdf1-df2b-43fe-bbbe-bdd02f379212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40e91c2-a55c-460a-9a22-5289e76bcf03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66b2cded-88c3-4ecb-8cc5-dcbc431a908e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40e91c2-a55c-460a-9a22-5289e76bcf03",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "185d2067-71c3-4e50-98ec-d91e32ea5e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "82e72604-8363-44ed-aa8b-4b5911d601b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "185d2067-71c3-4e50-98ec-d91e32ea5e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43837d08-b4d7-42e0-8e7a-570d364554b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "185d2067-71c3-4e50-98ec-d91e32ea5e0a",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "b630fd33-a325-4003-917c-83c0f51147e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "90a8cc2a-430c-4a32-8daf-8098acbc97dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b630fd33-a325-4003-917c-83c0f51147e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22c2904-3e4f-42f4-83de-813562ccbdf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b630fd33-a325-4003-917c-83c0f51147e8",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "ebdb9052-4d81-4ca6-a73c-7f1887453be5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "d0f1ed98-addc-4b9c-b31b-0008b424709d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebdb9052-4d81-4ca6-a73c-7f1887453be5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc8b2d11-5a23-42b7-9b5a-cd23edebb7a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebdb9052-4d81-4ca6-a73c-7f1887453be5",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "aded3f68-c0f5-41cd-a6de-01aecd49472f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "a832b579-d760-43e1-a80c-ff11c8317a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aded3f68-c0f5-41cd-a6de-01aecd49472f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fca0086d-d317-423b-8f6b-08498d724c04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aded3f68-c0f5-41cd-a6de-01aecd49472f",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        },
        {
            "id": "d1a26834-0e73-4fa6-975e-214d8e1b7542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "compositeImage": {
                "id": "df22f78d-a695-4248-a589-1afd77ec3dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1a26834-0e73-4fa6-975e-214d8e1b7542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b54b9517-dd8b-4898-9c7e-17253941d61a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1a26834-0e73-4fa6-975e-214d8e1b7542",
                    "LayerId": "473f582c-486f-4afc-9e6b-a06203a83919"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "473f582c-486f-4afc-9e6b-a06203a83919",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4294901889,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}