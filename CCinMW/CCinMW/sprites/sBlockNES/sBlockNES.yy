{
    "id": "1f7c779b-8dd3-42a5-9939-4c90cfc35727",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlockNES",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd746b28-5bd1-4b60-ae4f-b5c9cbf907e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f7c779b-8dd3-42a5-9939-4c90cfc35727",
            "compositeImage": {
                "id": "2a35d671-7413-4eae-a342-33e5abc193f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd746b28-5bd1-4b60-ae4f-b5c9cbf907e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e047289d-fc53-42f7-affd-f2749d3a0947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd746b28-5bd1-4b60-ae4f-b5c9cbf907e2",
                    "LayerId": "bef0566f-8c13-485d-9af7-ecde5d603554"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bef0566f-8c13-485d-9af7-ecde5d603554",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f7c779b-8dd3-42a5-9939-4c90cfc35727",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}