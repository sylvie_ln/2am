{
    "id": "b233f21a-6957-4f15-9fbe-d6507481afa4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMegaBagInterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 7,
    "bbox_right": 88,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "776e051c-6303-4c7e-94ee-707fb06119ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b233f21a-6957-4f15-9fbe-d6507481afa4",
            "compositeImage": {
                "id": "4926d5d4-9cda-42a5-ac53-7b749667b777",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "776e051c-6303-4c7e-94ee-707fb06119ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0684957-81f2-4dc9-8a4b-66a174e749e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "776e051c-6303-4c7e-94ee-707fb06119ed",
                    "LayerId": "ecd18d37-2a6d-4329-9d9b-3b9199015394"
                },
                {
                    "id": "2f5e35ca-c29b-4c2a-b836-67ba74d5c8d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "776e051c-6303-4c7e-94ee-707fb06119ed",
                    "LayerId": "1c050709-fd3c-427b-bbe0-17541a90a332"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ecd18d37-2a6d-4329-9d9b-3b9199015394",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b233f21a-6957-4f15-9fbe-d6507481afa4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "1c050709-fd3c-427b-bbe0-17541a90a332",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b233f21a-6957-4f15-9fbe-d6507481afa4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}