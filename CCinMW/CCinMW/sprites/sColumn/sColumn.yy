{
    "id": "7a6819b1-bec6-4693-986a-32b1bea26d04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sColumn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0dd4bbf-61a6-40d0-9ecc-4a24ec322799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a6819b1-bec6-4693-986a-32b1bea26d04",
            "compositeImage": {
                "id": "4104f13d-fb4f-4020-8a86-aab12f111ae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0dd4bbf-61a6-40d0-9ecc-4a24ec322799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a5e46fe-4059-4043-9291-ad3438a08622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0dd4bbf-61a6-40d0-9ecc-4a24ec322799",
                    "LayerId": "f4fe866c-8d9a-4013-a900-2e9ce74b90d7"
                }
            ]
        },
        {
            "id": "4f36ba19-59c9-4dc1-ba08-466dc138c6e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a6819b1-bec6-4693-986a-32b1bea26d04",
            "compositeImage": {
                "id": "d3782bfb-9919-41fe-bf69-552d449ec779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f36ba19-59c9-4dc1-ba08-466dc138c6e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "035d69c0-645b-4939-9af3-4bdddb66438e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f36ba19-59c9-4dc1-ba08-466dc138c6e6",
                    "LayerId": "f4fe866c-8d9a-4013-a900-2e9ce74b90d7"
                }
            ]
        },
        {
            "id": "f7961ce1-afdb-4f08-9fe1-7273d14ace0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a6819b1-bec6-4693-986a-32b1bea26d04",
            "compositeImage": {
                "id": "4b0e1755-9186-425b-9f79-4b00d7762964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7961ce1-afdb-4f08-9fe1-7273d14ace0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "851cd36a-6389-4ee6-a7d0-e359737bc62f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7961ce1-afdb-4f08-9fe1-7273d14ace0d",
                    "LayerId": "f4fe866c-8d9a-4013-a900-2e9ce74b90d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f4fe866c-8d9a-4013-a900-2e9ce74b90d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a6819b1-bec6-4693-986a-32b1bea26d04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}