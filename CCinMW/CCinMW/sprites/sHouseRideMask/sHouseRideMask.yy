{
    "id": "9d770ac6-4732-46ca-b9fb-88530496cfa9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouseRideMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "379081f0-e868-4e51-a605-46add5980d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d770ac6-4732-46ca-b9fb-88530496cfa9",
            "compositeImage": {
                "id": "0127b79b-9c9c-42bb-b1d3-0be17e324e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "379081f0-e868-4e51-a605-46add5980d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9ba4f5c-b8aa-4eb3-859c-94208e6a5e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379081f0-e868-4e51-a605-46add5980d6e",
                    "LayerId": "a1387a37-c25f-4929-b58d-7dd959854500"
                },
                {
                    "id": "18f5eb4b-9ed0-4714-9c05-07ad52d40637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379081f0-e868-4e51-a605-46add5980d6e",
                    "LayerId": "1c19abfc-e2e5-4307-b113-62300735cbfb"
                },
                {
                    "id": "55946847-8567-4d1f-b1ea-c4ffc6398d78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379081f0-e868-4e51-a605-46add5980d6e",
                    "LayerId": "b483e514-e562-447a-af8b-132f2c88bd1d"
                },
                {
                    "id": "93d22985-e388-44e8-8e7d-cff07a2e94fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379081f0-e868-4e51-a605-46add5980d6e",
                    "LayerId": "d325d043-209b-4187-84c6-ba5fff98ebef"
                },
                {
                    "id": "f6e8b4db-f12a-4c92-896b-3b3517f9992b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379081f0-e868-4e51-a605-46add5980d6e",
                    "LayerId": "82498f5f-9ff9-4dee-a91f-ad3ff13934e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "82498f5f-9ff9-4dee-a91f-ad3ff13934e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d770ac6-4732-46ca-b9fb-88530496cfa9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a1387a37-c25f-4929-b58d-7dd959854500",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d770ac6-4732-46ca-b9fb-88530496cfa9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "1c19abfc-e2e5-4307-b113-62300735cbfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d770ac6-4732-46ca-b9fb-88530496cfa9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "b483e514-e562-447a-af8b-132f2c88bd1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d770ac6-4732-46ca-b9fb-88530496cfa9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 25,
            "visible": false
        },
        {
            "id": "d325d043-209b-4187-84c6-ba5fff98ebef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d770ac6-4732-46ca-b9fb-88530496cfa9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}