{
    "id": "25404bfb-3698-4732-bd17-4d279ed921d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDrummerPlusheySticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4c28248-1c99-4d73-af29-fab7ac72b90b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25404bfb-3698-4732-bd17-4d279ed921d9",
            "compositeImage": {
                "id": "e2522985-344a-4986-9ff6-5122b9efb3e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4c28248-1c99-4d73-af29-fab7ac72b90b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f56fb3-c647-412c-a7e7-5908f989931b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4c28248-1c99-4d73-af29-fab7ac72b90b",
                    "LayerId": "bb4ba967-17f3-427f-809a-e6e09599f7ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bb4ba967-17f3-427f-809a-e6e09599f7ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25404bfb-3698-4732-bd17-4d279ed921d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}