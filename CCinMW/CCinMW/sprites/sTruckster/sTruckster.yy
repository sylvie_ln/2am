{
    "id": "8a5c4dfe-be9b-4bbf-9aba-98f15440a8e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTruckster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7cbb369-fc19-43c5-beed-4a2d66e4ae0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a5c4dfe-be9b-4bbf-9aba-98f15440a8e7",
            "compositeImage": {
                "id": "41e2d572-4518-417c-a7e3-90dc646defed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7cbb369-fc19-43c5-beed-4a2d66e4ae0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6d8389a-5940-4fa8-8a94-13aba811563f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7cbb369-fc19-43c5-beed-4a2d66e4ae0a",
                    "LayerId": "8f4faf89-0e2e-47fc-922e-b7afade96c96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8f4faf89-0e2e-47fc-922e-b7afade96c96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a5c4dfe-be9b-4bbf-9aba-98f15440a8e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}