{
    "id": "b1d32cc9-9c31-4c6b-a533-4060bbc57b0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2faf3994-99d2-4ae7-ae20-5a31b75b5c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1d32cc9-9c31-4c6b-a533-4060bbc57b0c",
            "compositeImage": {
                "id": "8f580bc0-b2c2-448a-afc9-d7f2306f5c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2faf3994-99d2-4ae7-ae20-5a31b75b5c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8043a502-88cf-41a0-91da-023816c8586c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2faf3994-99d2-4ae7-ae20-5a31b75b5c00",
                    "LayerId": "e330f8a6-2285-45f9-86c3-769facd67f86"
                }
            ]
        },
        {
            "id": "9bd53d93-3ba4-411e-9e30-fce246cd34a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1d32cc9-9c31-4c6b-a533-4060bbc57b0c",
            "compositeImage": {
                "id": "b689381c-6772-48a8-bb1b-3d376876edd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bd53d93-3ba4-411e-9e30-fce246cd34a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1026146-cf27-4104-9047-7fb8cf2768bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bd53d93-3ba4-411e-9e30-fce246cd34a3",
                    "LayerId": "e330f8a6-2285-45f9-86c3-769facd67f86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e330f8a6-2285-45f9-86c3-769facd67f86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1d32cc9-9c31-4c6b-a533-4060bbc57b0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}