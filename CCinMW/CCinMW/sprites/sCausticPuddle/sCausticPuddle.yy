{
    "id": "ae0c7305-dc14-4351-9f45-e1460430fcf5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCausticPuddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd595296-0ffb-4400-82a0-367aba4c27f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae0c7305-dc14-4351-9f45-e1460430fcf5",
            "compositeImage": {
                "id": "32d310a6-e335-479f-ae3a-16cefc4e4569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd595296-0ffb-4400-82a0-367aba4c27f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae1ac51-c5c5-402f-9bfc-44a284ca12ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd595296-0ffb-4400-82a0-367aba4c27f9",
                    "LayerId": "fc88fec5-bd0b-42c0-8b1e-5933fd01f546"
                }
            ]
        },
        {
            "id": "d3644fa6-92b9-4b69-8456-34d3bfbd93ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae0c7305-dc14-4351-9f45-e1460430fcf5",
            "compositeImage": {
                "id": "b12db4f6-8cc5-48a0-9ab8-648b8aae7099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3644fa6-92b9-4b69-8456-34d3bfbd93ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fbf91b2-af04-431e-a181-698dc36f43c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3644fa6-92b9-4b69-8456-34d3bfbd93ad",
                    "LayerId": "fc88fec5-bd0b-42c0-8b1e-5933fd01f546"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "fc88fec5-bd0b-42c0-8b1e-5933fd01f546",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae0c7305-dc14-4351-9f45-e1460430fcf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}