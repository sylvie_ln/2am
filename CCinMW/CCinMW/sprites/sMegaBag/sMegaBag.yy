{
    "id": "3edab299-c56b-479c-87ab-66e190933725",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMegaBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b28e83ae-f18c-443e-b14c-7412d88cf0d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3edab299-c56b-479c-87ab-66e190933725",
            "compositeImage": {
                "id": "e4dce249-c3f1-4eba-afc7-ca94fc2c94bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b28e83ae-f18c-443e-b14c-7412d88cf0d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b0dbe25-ee06-4217-a9f4-7490870cca35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b28e83ae-f18c-443e-b14c-7412d88cf0d8",
                    "LayerId": "45f947b8-610a-4a34-883e-c26e807a4596"
                },
                {
                    "id": "0794490f-cf1e-493a-ab62-7f7fdf189ead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b28e83ae-f18c-443e-b14c-7412d88cf0d8",
                    "LayerId": "fb977d8d-f336-48ba-adb9-7428c3184ad2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "45f947b8-610a-4a34-883e-c26e807a4596",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3edab299-c56b-479c-87ab-66e190933725",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fb977d8d-f336-48ba-adb9-7428c3184ad2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3edab299-c56b-479c-87ab-66e190933725",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}