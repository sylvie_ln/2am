{
    "id": "464214f0-6354-4984-963a-c4d8b5b06d00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTallBagUpgradeSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b054a838-281f-4c71-98be-98cd4b54868c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "464214f0-6354-4984-963a-c4d8b5b06d00",
            "compositeImage": {
                "id": "516584f8-0678-4824-a3b6-b0226d2b2adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b054a838-281f-4c71-98be-98cd4b54868c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c73012ad-2157-46e6-806b-e84a6fa0b661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b054a838-281f-4c71-98be-98cd4b54868c",
                    "LayerId": "d9687df1-f42d-4e1e-bafd-bfa7faa6fd78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d9687df1-f42d-4e1e-bafd-bfa7faa6fd78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "464214f0-6354-4984-963a-c4d8b5b06d00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}