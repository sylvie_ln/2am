{
    "id": "6af7263d-9877-45f6-a875-e2d69c9667c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleText",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 4,
    "bbox_right": 315,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89605182-97ff-44d7-9bb3-a953be584373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6af7263d-9877-45f6-a875-e2d69c9667c3",
            "compositeImage": {
                "id": "e963e210-2f7c-4102-aedf-7aa68f9c3499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89605182-97ff-44d7-9bb3-a953be584373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfd7392-3ae9-4353-9039-136caaec9f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89605182-97ff-44d7-9bb3-a953be584373",
                    "LayerId": "bf1c53b6-c109-4cdd-8314-12710e030059"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "bf1c53b6-c109-4cdd-8314-12710e030059",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6af7263d-9877-45f6-a875-e2d69c9667c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}