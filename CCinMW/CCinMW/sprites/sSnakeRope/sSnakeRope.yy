{
    "id": "f534d4cb-120b-4bbe-8460-51f496df1d01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnakeRope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8ad5837-14b9-496c-93d5-cb8679a06217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f534d4cb-120b-4bbe-8460-51f496df1d01",
            "compositeImage": {
                "id": "3b181cdf-ef64-4c3f-bb85-bdc0bb715514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ad5837-14b9-496c-93d5-cb8679a06217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35bf0ebf-33d2-45d4-ad35-2399683f71e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ad5837-14b9-496c-93d5-cb8679a06217",
                    "LayerId": "1705c270-2c45-459d-acc7-1ba225d478df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1705c270-2c45-459d-acc7-1ba225d478df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f534d4cb-120b-4bbe-8460-51f496df1d01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}