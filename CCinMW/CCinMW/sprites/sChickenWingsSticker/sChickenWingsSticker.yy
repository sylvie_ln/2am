{
    "id": "df67a01d-47c8-480c-9dc8-6708ef436667",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChickenWingsSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ddc8448-1cff-4e50-a679-ae294e1a2498",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df67a01d-47c8-480c-9dc8-6708ef436667",
            "compositeImage": {
                "id": "278ceb5c-849e-4e9c-8e72-eb1522d8d205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ddc8448-1cff-4e50-a679-ae294e1a2498",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "560374ff-cce5-4153-ad9d-f680acdf03f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ddc8448-1cff-4e50-a679-ae294e1a2498",
                    "LayerId": "59655368-9209-4e7d-ad80-1e68cb8641e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "59655368-9209-4e7d-ad80-1e68cb8641e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df67a01d-47c8-480c-9dc8-6708ef436667",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}