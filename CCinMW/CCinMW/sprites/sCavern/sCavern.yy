{
    "id": "64a57152-260c-4882-ae5c-e54a1e8cbdfc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCavern",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcc98a66-522b-4291-8628-940e0206a39e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a57152-260c-4882-ae5c-e54a1e8cbdfc",
            "compositeImage": {
                "id": "f89fa225-6312-4e75-be38-7a992c06916e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcc98a66-522b-4291-8628-940e0206a39e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17c776d-61f4-4b2b-ac4d-22478df1cf5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcc98a66-522b-4291-8628-940e0206a39e",
                    "LayerId": "8a934aa2-d7d3-42a5-81fe-7bbcbbd88a40"
                }
            ]
        },
        {
            "id": "79056ce5-a3a7-45f6-a479-abae1b878d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a57152-260c-4882-ae5c-e54a1e8cbdfc",
            "compositeImage": {
                "id": "f9504010-fad2-4fb3-b01b-e2fd895d044d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79056ce5-a3a7-45f6-a479-abae1b878d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0135a98d-c0b1-4b15-ae3a-f124bf7304bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79056ce5-a3a7-45f6-a479-abae1b878d5d",
                    "LayerId": "8a934aa2-d7d3-42a5-81fe-7bbcbbd88a40"
                }
            ]
        },
        {
            "id": "03052238-60e8-4dc6-93ff-764289189b04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a57152-260c-4882-ae5c-e54a1e8cbdfc",
            "compositeImage": {
                "id": "f5558a30-4269-4313-9f7c-8e54d703455c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03052238-60e8-4dc6-93ff-764289189b04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "384bfe92-6878-4ecf-9a17-63ebd6692eaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03052238-60e8-4dc6-93ff-764289189b04",
                    "LayerId": "8a934aa2-d7d3-42a5-81fe-7bbcbbd88a40"
                }
            ]
        },
        {
            "id": "cb566478-da91-45af-8a0c-829ece33fe7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a57152-260c-4882-ae5c-e54a1e8cbdfc",
            "compositeImage": {
                "id": "591d412b-32f3-4467-941e-9f1040afc64f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb566478-da91-45af-8a0c-829ece33fe7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12bf3188-267e-4a2e-95ae-b9ce8b12c363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb566478-da91-45af-8a0c-829ece33fe7d",
                    "LayerId": "8a934aa2-d7d3-42a5-81fe-7bbcbbd88a40"
                }
            ]
        },
        {
            "id": "aa61e8d9-9a2f-4699-b4f2-c58a27db72d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a57152-260c-4882-ae5c-e54a1e8cbdfc",
            "compositeImage": {
                "id": "4b424b75-5146-4ce3-9d72-038c27ec6420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa61e8d9-9a2f-4699-b4f2-c58a27db72d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ecf108-f7bb-49fe-b33c-96f143679014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa61e8d9-9a2f-4699-b4f2-c58a27db72d6",
                    "LayerId": "8a934aa2-d7d3-42a5-81fe-7bbcbbd88a40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8a934aa2-d7d3-42a5-81fe-7bbcbbd88a40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64a57152-260c-4882-ae5c-e54a1e8cbdfc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}