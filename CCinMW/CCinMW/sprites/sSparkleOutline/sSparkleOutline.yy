{
    "id": "6f77e010-9e13-4682-a26f-bccf4cdada69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSparkleOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4310d41b-f947-4577-9f39-76e8b0ff2bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f77e010-9e13-4682-a26f-bccf4cdada69",
            "compositeImage": {
                "id": "093e3189-aa3a-4b45-aef1-ead58e91e557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4310d41b-f947-4577-9f39-76e8b0ff2bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fa13884-7710-4e14-8e35-8337406b4d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4310d41b-f947-4577-9f39-76e8b0ff2bba",
                    "LayerId": "850c9f28-03ec-4716-982f-8bc12875a0b1"
                }
            ]
        },
        {
            "id": "35331bca-1ba9-4f70-ba8a-06e68e4e586b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f77e010-9e13-4682-a26f-bccf4cdada69",
            "compositeImage": {
                "id": "2442fcb5-92a0-453b-9c43-1199ad15f4c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35331bca-1ba9-4f70-ba8a-06e68e4e586b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f65bcb8d-4a04-4a51-87de-40f5d7dd7822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35331bca-1ba9-4f70-ba8a-06e68e4e586b",
                    "LayerId": "850c9f28-03ec-4716-982f-8bc12875a0b1"
                }
            ]
        },
        {
            "id": "5590a0ad-bec5-4946-a7c5-4c1049a94e08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f77e010-9e13-4682-a26f-bccf4cdada69",
            "compositeImage": {
                "id": "79e296a7-43a2-40b4-8738-12d9e7070f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5590a0ad-bec5-4946-a7c5-4c1049a94e08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "873f264a-9cd1-40a2-ab36-d0fa8c4f4094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5590a0ad-bec5-4946-a7c5-4c1049a94e08",
                    "LayerId": "850c9f28-03ec-4716-982f-8bc12875a0b1"
                }
            ]
        },
        {
            "id": "4f2b091d-b69f-4b99-b979-8f1d69194c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f77e010-9e13-4682-a26f-bccf4cdada69",
            "compositeImage": {
                "id": "844ca68f-0aff-4b58-8d72-c2869544ad36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2b091d-b69f-4b99-b979-8f1d69194c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82bf637d-e08f-4c8b-b298-d43da4568c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2b091d-b69f-4b99-b979-8f1d69194c4b",
                    "LayerId": "850c9f28-03ec-4716-982f-8bc12875a0b1"
                }
            ]
        },
        {
            "id": "06d018fc-ba3d-4a02-90fe-5f386332373f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f77e010-9e13-4682-a26f-bccf4cdada69",
            "compositeImage": {
                "id": "f3614ba5-cc08-408b-98e2-ea9839dcd42d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d018fc-ba3d-4a02-90fe-5f386332373f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7388a893-5af9-456f-8400-0db8ef1a2a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d018fc-ba3d-4a02-90fe-5f386332373f",
                    "LayerId": "850c9f28-03ec-4716-982f-8bc12875a0b1"
                }
            ]
        },
        {
            "id": "766011da-58e7-401f-afa3-1cdaa619ee6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f77e010-9e13-4682-a26f-bccf4cdada69",
            "compositeImage": {
                "id": "fb1730bb-b248-403a-9fcb-45491f72764b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766011da-58e7-401f-afa3-1cdaa619ee6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9743a38c-85c7-4833-bb45-d8efde0daf39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766011da-58e7-401f-afa3-1cdaa619ee6c",
                    "LayerId": "850c9f28-03ec-4716-982f-8bc12875a0b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "850c9f28-03ec-4716-982f-8bc12875a0b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f77e010-9e13-4682-a26f-bccf4cdada69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}