{
    "id": "de3695aa-fa57-4c3a-ac9a-95ad5f710a62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMemoryRevived",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f1b83db-0daf-4026-84e2-22cf19ff0695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de3695aa-fa57-4c3a-ac9a-95ad5f710a62",
            "compositeImage": {
                "id": "b0a78bfc-d125-46d3-8b20-3b62709cd15e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f1b83db-0daf-4026-84e2-22cf19ff0695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a94089-4e98-4972-bf2f-2a2b67eb4850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f1b83db-0daf-4026-84e2-22cf19ff0695",
                    "LayerId": "ca83e19b-8bab-4b6f-a56e-9a663ae7f512"
                },
                {
                    "id": "fe9e2605-95be-46a6-8762-5c5b549dd668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f1b83db-0daf-4026-84e2-22cf19ff0695",
                    "LayerId": "d7958852-1ce4-45f5-8e13-d502a592377a"
                },
                {
                    "id": "93bbb541-c7a2-4c67-bf50-9cf9b79dbe93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f1b83db-0daf-4026-84e2-22cf19ff0695",
                    "LayerId": "fc4fe192-92cf-4f8c-9ee6-1dc940b48fc8"
                },
                {
                    "id": "9f87b14c-9ac7-4e07-87d2-4ccac2276f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f1b83db-0daf-4026-84e2-22cf19ff0695",
                    "LayerId": "40f86c73-3b49-470b-b6b2-20dc76a30b8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ca83e19b-8bab-4b6f-a56e-9a663ae7f512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de3695aa-fa57-4c3a-ac9a-95ad5f710a62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d7958852-1ce4-45f5-8e13-d502a592377a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de3695aa-fa57-4c3a-ac9a-95ad5f710a62",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "fc4fe192-92cf-4f8c-9ee6-1dc940b48fc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de3695aa-fa57-4c3a-ac9a-95ad5f710a62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "40f86c73-3b49-470b-b6b2-20dc76a30b8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de3695aa-fa57-4c3a-ac9a-95ad5f710a62",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}