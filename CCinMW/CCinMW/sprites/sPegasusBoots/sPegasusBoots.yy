{
    "id": "e3eafdc5-47f3-415a-a7a5-b6b9a4c5427a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPegasusBoots",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15d3e2d9-c05b-4f7f-83ee-e5e0439efc13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3eafdc5-47f3-415a-a7a5-b6b9a4c5427a",
            "compositeImage": {
                "id": "eeb2c796-44d9-4f10-be7b-7f618b83b1fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15d3e2d9-c05b-4f7f-83ee-e5e0439efc13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6126e40c-d325-48b3-bc7e-d62aa0176962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15d3e2d9-c05b-4f7f-83ee-e5e0439efc13",
                    "LayerId": "5ea5d9cd-6bf3-4d60-b994-2e3159f5d77a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5ea5d9cd-6bf3-4d60-b994-2e3159f5d77a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3eafdc5-47f3-415a-a7a5-b6b9a4c5427a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}