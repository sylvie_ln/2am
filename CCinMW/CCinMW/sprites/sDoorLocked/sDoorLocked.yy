{
    "id": "17cf593e-0e40-4dde-a7ed-2991f1c15433",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoorLocked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fd477f7-e650-4603-8100-39bc6c0597d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17cf593e-0e40-4dde-a7ed-2991f1c15433",
            "compositeImage": {
                "id": "b1b57bb8-2115-4d3f-9ceb-10df9d17fc05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fd477f7-e650-4603-8100-39bc6c0597d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34838a2b-1e46-4518-b82e-b0b03836b2f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fd477f7-e650-4603-8100-39bc6c0597d8",
                    "LayerId": "8c171d2f-acc1-49c7-ab9c-000e47eb6a2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8c171d2f-acc1-49c7-ab9c-000e47eb6a2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17cf593e-0e40-4dde-a7ed-2991f1c15433",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}