{
    "id": "9ef4d58c-4368-45c1-875f-5bdbe0fbd634",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLabo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfce1411-f39a-4fd6-9ab9-eb06d1662df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ef4d58c-4368-45c1-875f-5bdbe0fbd634",
            "compositeImage": {
                "id": "a1fef094-acdf-498a-abf2-af01d9d669ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfce1411-f39a-4fd6-9ab9-eb06d1662df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b33feb5-92bb-4299-bc86-32adcfd1316d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfce1411-f39a-4fd6-9ab9-eb06d1662df4",
                    "LayerId": "23faad26-c94e-4836-9050-b59e3dcafc3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "23faad26-c94e-4836-9050-b59e3dcafc3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ef4d58c-4368-45c1-875f-5bdbe0fbd634",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 32
}