{
    "id": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecraftMultiplier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "245b730d-17b8-4204-b6fe-835e2e910176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "compositeImage": {
                "id": "a6d2f215-c144-4e02-b36a-668cbfca032b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "245b730d-17b8-4204-b6fe-835e2e910176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96816c7f-90ef-4696-8479-ebb8f147fa9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "245b730d-17b8-4204-b6fe-835e2e910176",
                    "LayerId": "06d49d19-7b20-4c7c-9893-1e7e5e933e08"
                }
            ]
        },
        {
            "id": "2b34bdc6-560c-4f2d-91fb-1c1a72d4503a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "compositeImage": {
                "id": "5ac79dce-96c3-4eed-b46f-fc4839fd5914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b34bdc6-560c-4f2d-91fb-1c1a72d4503a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9598b1ba-09f7-4be9-bfd6-7c252c9cfb2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b34bdc6-560c-4f2d-91fb-1c1a72d4503a",
                    "LayerId": "06d49d19-7b20-4c7c-9893-1e7e5e933e08"
                }
            ]
        },
        {
            "id": "e9cac9db-706f-4118-897a-2b89bb9e63dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "compositeImage": {
                "id": "a206dc9a-6381-4c27-89f1-065542f35053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9cac9db-706f-4118-897a-2b89bb9e63dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2754c9a7-8c26-4dc2-a05f-e1039df6c2ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9cac9db-706f-4118-897a-2b89bb9e63dd",
                    "LayerId": "06d49d19-7b20-4c7c-9893-1e7e5e933e08"
                }
            ]
        },
        {
            "id": "6e59ca12-1208-4d12-9f90-48ae056dc4b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "compositeImage": {
                "id": "ee9fb2fe-9316-4e05-85b3-277309be6261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e59ca12-1208-4d12-9f90-48ae056dc4b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16d29f4d-c5fe-49ee-a7f0-4daa720cbfb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e59ca12-1208-4d12-9f90-48ae056dc4b6",
                    "LayerId": "06d49d19-7b20-4c7c-9893-1e7e5e933e08"
                }
            ]
        },
        {
            "id": "ccf77517-eefb-4aba-b0cc-211a09c14aba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "compositeImage": {
                "id": "5bb76574-f389-4b43-a13e-7dc776eb0235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf77517-eefb-4aba-b0cc-211a09c14aba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb26479b-ccb0-4716-bade-6c223765726f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf77517-eefb-4aba-b0cc-211a09c14aba",
                    "LayerId": "06d49d19-7b20-4c7c-9893-1e7e5e933e08"
                }
            ]
        },
        {
            "id": "b0a7cc1d-1959-46a5-a69d-09ce3b6f2ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "compositeImage": {
                "id": "a7a09f9b-2b61-4de3-844b-d521ffd47c3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0a7cc1d-1959-46a5-a69d-09ce3b6f2ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ca6dd64-2e9d-4522-8379-33308dc2c3e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0a7cc1d-1959-46a5-a69d-09ce3b6f2ca4",
                    "LayerId": "06d49d19-7b20-4c7c-9893-1e7e5e933e08"
                }
            ]
        },
        {
            "id": "443e7619-ead1-410a-8e4d-8b18ef0c1c75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "compositeImage": {
                "id": "269d460d-3242-41cd-a1e0-ae83579426d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "443e7619-ead1-410a-8e4d-8b18ef0c1c75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92056e0a-762c-4bb5-9b37-2216ec1a5adf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "443e7619-ead1-410a-8e4d-8b18ef0c1c75",
                    "LayerId": "06d49d19-7b20-4c7c-9893-1e7e5e933e08"
                }
            ]
        },
        {
            "id": "39770922-9bc5-4521-ad1b-ed4e8a374fbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "compositeImage": {
                "id": "cbc17e07-2a5f-4731-a1f7-2a84a6586696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39770922-9bc5-4521-ad1b-ed4e8a374fbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f21690-6936-463e-ac18-3b52b312912f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39770922-9bc5-4521-ad1b-ed4e8a374fbb",
                    "LayerId": "06d49d19-7b20-4c7c-9893-1e7e5e933e08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "06d49d19-7b20-4c7c-9893-1e7e5e933e08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66ab3b6f-d274-481c-a17f-c885fdbb7a68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}