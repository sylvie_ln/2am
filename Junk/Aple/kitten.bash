#!/bin/bash
set -e
ditto -rsrc "/Users/sylvie/GameMakerStudio2/Mac/GMS2MAC/CCinMW/Clockwork Calamity in Mushroom World.app" ~/CCinMW.app
codesign --sign "Developer ID Application:" ~/CCinMW.app --force --deep --timestamp --options=runtime --entitlements game.entitlements
ditto -c -k --keepParent ~/CCinMW.app ~/CCinMW_Mac_STEAM.zip
xcrun altool --notarize-app --primary-bundle-id "net.love-game.ccinmw" --username "REDACTED" --password "REDACTED" --file ~/CCinMW_Mac_STEAM.zip
