#!/bin/bash
set -e
xcrun stapler staple ~/CCinMW.app
ditto -rsrc ~/CCinMW.app ~/CCinMW/CCinMW.app
ditto -c -k --keepParent ~/CCinMW.app ~/CCinMW_Mac_Steam.zip
hdiutil create -volname CCinMW -srcfolder ~/CCinMW -ov -format UDZO ~/CCinMW$1.dmg
