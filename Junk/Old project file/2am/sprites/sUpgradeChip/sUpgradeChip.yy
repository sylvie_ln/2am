{
    "id": "98c5b6a5-e9d6-4082-92d4-375210026cb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUpgradeChip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf45fde9-b478-42a9-a772-39b12ba36c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98c5b6a5-e9d6-4082-92d4-375210026cb0",
            "compositeImage": {
                "id": "9d13d8bc-d3d8-462c-bf80-ebc5dc7d70ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf45fde9-b478-42a9-a772-39b12ba36c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54101989-fd4a-4c58-9fe9-fa3b3867d0d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf45fde9-b478-42a9-a772-39b12ba36c82",
                    "LayerId": "a11a7805-388e-4efb-aeff-c30f17d4c89c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a11a7805-388e-4efb-aeff-c30f17d4c89c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98c5b6a5-e9d6-4082-92d4-375210026cb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}