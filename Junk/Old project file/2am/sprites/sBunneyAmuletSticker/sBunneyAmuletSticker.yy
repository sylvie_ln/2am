{
    "id": "4fd916e2-e3e9-4f76-86fa-8beef70eca3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyAmuletSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e09f9ba8-e1d3-4bff-b86a-34b584b6ee4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fd916e2-e3e9-4f76-86fa-8beef70eca3c",
            "compositeImage": {
                "id": "9416157b-1815-4b99-b04c-ca4c1e7b9a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e09f9ba8-e1d3-4bff-b86a-34b584b6ee4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a909e852-1cdf-4f0d-9358-02183ab2a46c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e09f9ba8-e1d3-4bff-b86a-34b584b6ee4b",
                    "LayerId": "632f0992-4ca1-48d2-a64e-7181abf334fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "632f0992-4ca1-48d2-a64e-7181abf334fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fd916e2-e3e9-4f76-86fa-8beef70eca3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}