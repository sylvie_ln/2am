{
    "id": "49092bbb-9f71-497d-9f42-36214db6dd45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMappingImplantSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c46ad12-cf54-49d0-a812-48de44aa2014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49092bbb-9f71-497d-9f42-36214db6dd45",
            "compositeImage": {
                "id": "74759409-de16-445e-9667-9b8833ae8390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c46ad12-cf54-49d0-a812-48de44aa2014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2723faa-6146-4fa9-92bd-5ab2c5999449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c46ad12-cf54-49d0-a812-48de44aa2014",
                    "LayerId": "9bf263b8-4629-4818-b48d-b8ae95e90a78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9bf263b8-4629-4818-b48d-b8ae95e90a78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49092bbb-9f71-497d-9f42-36214db6dd45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 6
}