{
    "id": "efcd4fa9-d48c-4d45-96e4-44776bc05b1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sVehicle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 6,
    "bbox_right": 18,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b392a61-915a-421b-a6dc-9a8a21723468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efcd4fa9-d48c-4d45-96e4-44776bc05b1b",
            "compositeImage": {
                "id": "416b9087-2995-4767-9170-899e2158a9bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b392a61-915a-421b-a6dc-9a8a21723468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3498a08d-4523-467a-bdbf-3f0bc7cb2394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b392a61-915a-421b-a6dc-9a8a21723468",
                    "LayerId": "c7bc4df3-de03-4c61-b77a-b6fb3c5ff51b"
                },
                {
                    "id": "c8ebdf72-4902-4a3f-bed3-3fd200564e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b392a61-915a-421b-a6dc-9a8a21723468",
                    "LayerId": "d890da89-da11-4e53-bf49-181f98d280cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c7bc4df3-de03-4c61-b77a-b6fb3c5ff51b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efcd4fa9-d48c-4d45-96e4-44776bc05b1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d890da89-da11-4e53-bf49-181f98d280cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efcd4fa9-d48c-4d45-96e4-44776bc05b1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}