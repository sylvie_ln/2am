{
    "id": "4ce20b49-b230-4abe-a2e5-0caf8501d717",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnowKeetyMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "804fc310-061f-48ee-971a-57a1f3533276",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ce20b49-b230-4abe-a2e5-0caf8501d717",
            "compositeImage": {
                "id": "65315012-b118-4467-9c05-cadbeaf3bfb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "804fc310-061f-48ee-971a-57a1f3533276",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5de8726-4be3-4d42-9219-7077d8e633c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "804fc310-061f-48ee-971a-57a1f3533276",
                    "LayerId": "19cfd8e2-2e72-4562-b9ed-4bd14a2ab05f"
                },
                {
                    "id": "e3ca2f1b-69bd-4b7e-be6e-7988c968c36e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "804fc310-061f-48ee-971a-57a1f3533276",
                    "LayerId": "06ec505a-f3a5-4a35-94c0-e812bc473e40"
                },
                {
                    "id": "440df4df-4f6c-4433-a710-257325bd95de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "804fc310-061f-48ee-971a-57a1f3533276",
                    "LayerId": "862c8741-1028-459d-b71f-1b2662f9e31c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "862c8741-1028-459d-b71f-1b2662f9e31c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ce20b49-b230-4abe-a2e5-0caf8501d717",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "19cfd8e2-2e72-4562-b9ed-4bd14a2ab05f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ce20b49-b230-4abe-a2e5-0caf8501d717",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "06ec505a-f3a5-4a35-94c0-e812bc473e40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ce20b49-b230-4abe-a2e5-0caf8501d717",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}