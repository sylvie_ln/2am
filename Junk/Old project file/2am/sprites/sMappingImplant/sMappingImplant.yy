{
    "id": "da3fd45f-e9bc-47f6-890a-8047ffa138de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMappingImplant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72292eaf-1fc4-4338-9571-85a397374e9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da3fd45f-e9bc-47f6-890a-8047ffa138de",
            "compositeImage": {
                "id": "741586a3-e324-4184-99da-cfdce300c253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72292eaf-1fc4-4338-9571-85a397374e9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbe36857-c541-4180-917f-efffbfa06c4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72292eaf-1fc4-4338-9571-85a397374e9f",
                    "LayerId": "7b85460f-6960-4bf1-bef8-4168d9054bf8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7b85460f-6960-4bf1-bef8-4168d9054bf8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da3fd45f-e9bc-47f6-890a-8047ffa138de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}