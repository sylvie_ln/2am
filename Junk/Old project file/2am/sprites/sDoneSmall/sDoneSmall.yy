{
    "id": "99bb91d6-f19f-41c1-9448-d4e01c7bafc1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoneSmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 38,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16525cfd-b54b-42b7-a95f-5c097cfffb46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99bb91d6-f19f-41c1-9448-d4e01c7bafc1",
            "compositeImage": {
                "id": "955d4203-af3f-4a1d-8493-d8b5b7094881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16525cfd-b54b-42b7-a95f-5c097cfffb46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eb4a881-25f8-4152-8059-f93871b869b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16525cfd-b54b-42b7-a95f-5c097cfffb46",
                    "LayerId": "3ae5e94d-8b2a-4bdf-99b1-45448a6195ea"
                }
            ]
        },
        {
            "id": "e9c4546f-7cc1-4ea5-804f-a43436e5438f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99bb91d6-f19f-41c1-9448-d4e01c7bafc1",
            "compositeImage": {
                "id": "81f1c75e-02ce-4c30-97c0-88d30eb605ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c4546f-7cc1-4ea5-804f-a43436e5438f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "544f80a9-ac20-4835-bea5-804b77c8bfef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c4546f-7cc1-4ea5-804f-a43436e5438f",
                    "LayerId": "3ae5e94d-8b2a-4bdf-99b1-45448a6195ea"
                }
            ]
        },
        {
            "id": "1de77b8b-0776-4db7-be24-69b7186f54e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99bb91d6-f19f-41c1-9448-d4e01c7bafc1",
            "compositeImage": {
                "id": "e3a67185-b964-4da4-beab-8fcb01565336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1de77b8b-0776-4db7-be24-69b7186f54e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aac0d4c5-71eb-4555-8629-1230e5e6e807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1de77b8b-0776-4db7-be24-69b7186f54e5",
                    "LayerId": "3ae5e94d-8b2a-4bdf-99b1-45448a6195ea"
                }
            ]
        },
        {
            "id": "593c7a6d-3261-4eb2-bbd6-7d05701445a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99bb91d6-f19f-41c1-9448-d4e01c7bafc1",
            "compositeImage": {
                "id": "5d91dc63-f439-4ee6-afd8-d6aabacb2fba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "593c7a6d-3261-4eb2-bbd6-7d05701445a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a87cb84-30b7-4a52-8b11-871172622c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "593c7a6d-3261-4eb2-bbd6-7d05701445a8",
                    "LayerId": "3ae5e94d-8b2a-4bdf-99b1-45448a6195ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3ae5e94d-8b2a-4bdf-99b1-45448a6195ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99bb91d6-f19f-41c1-9448-d4e01c7bafc1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}