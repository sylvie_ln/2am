{
    "id": "b1b3cc44-d247-4281-a5e3-94350bfc984b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyDie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dce14384-26bc-4dec-b988-25d64e714337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3cc44-d247-4281-a5e3-94350bfc984b",
            "compositeImage": {
                "id": "a4795f12-a734-4670-96f2-b9f87413554c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce14384-26bc-4dec-b988-25d64e714337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda65b76-76f6-4143-82f0-9d4f7178e34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce14384-26bc-4dec-b988-25d64e714337",
                    "LayerId": "918dead3-db7c-41fa-94bf-e35c1c356ffe"
                }
            ]
        },
        {
            "id": "3291487c-0290-4cb0-8a5f-658eeb23dbe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3cc44-d247-4281-a5e3-94350bfc984b",
            "compositeImage": {
                "id": "5066cf2d-353c-49db-8d10-98fd16ce95b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3291487c-0290-4cb0-8a5f-658eeb23dbe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ba9f5eb-03e2-4e71-a09c-e0cb6db02a49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3291487c-0290-4cb0-8a5f-658eeb23dbe4",
                    "LayerId": "918dead3-db7c-41fa-94bf-e35c1c356ffe"
                }
            ]
        },
        {
            "id": "ffd17438-22db-400e-b05c-733b3002ff27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3cc44-d247-4281-a5e3-94350bfc984b",
            "compositeImage": {
                "id": "0e5902c8-894c-4f38-876b-b509b1719f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffd17438-22db-400e-b05c-733b3002ff27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe4c8aa3-6790-407a-afe8-ed359ba7f738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffd17438-22db-400e-b05c-733b3002ff27",
                    "LayerId": "918dead3-db7c-41fa-94bf-e35c1c356ffe"
                }
            ]
        },
        {
            "id": "e32fe3ab-49e4-4637-9a31-f62fb120ffc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3cc44-d247-4281-a5e3-94350bfc984b",
            "compositeImage": {
                "id": "8959fc80-91e8-4749-a3a7-a00b36169b6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e32fe3ab-49e4-4637-9a31-f62fb120ffc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f3c5fc3-99bd-46ac-bf15-be651dbcad10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e32fe3ab-49e4-4637-9a31-f62fb120ffc0",
                    "LayerId": "918dead3-db7c-41fa-94bf-e35c1c356ffe"
                }
            ]
        },
        {
            "id": "430fbcfc-09fa-499b-b51b-14316aab945e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3cc44-d247-4281-a5e3-94350bfc984b",
            "compositeImage": {
                "id": "18a04480-8169-4b0f-97a0-6f5776432bf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430fbcfc-09fa-499b-b51b-14316aab945e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfac09e2-b87e-49bc-968f-2ddb3bff7faf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430fbcfc-09fa-499b-b51b-14316aab945e",
                    "LayerId": "918dead3-db7c-41fa-94bf-e35c1c356ffe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "918dead3-db7c-41fa-94bf-e35c1c356ffe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1b3cc44-d247-4281-a5e3-94350bfc984b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}