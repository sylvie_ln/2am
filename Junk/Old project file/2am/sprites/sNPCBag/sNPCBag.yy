{
    "id": "ff6b87ae-85b4-4ea4-aa68-84bce2fa78b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNPCBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 11,
    "bbox_right": 117,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97c5bf96-d33b-4b29-a61b-bd48207b56b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff6b87ae-85b4-4ea4-aa68-84bce2fa78b0",
            "compositeImage": {
                "id": "6a596f24-6855-48d9-b8ee-c908c66bdc6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97c5bf96-d33b-4b29-a61b-bd48207b56b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323749ad-3d74-4b08-afef-6316bb8eb11f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c5bf96-d33b-4b29-a61b-bd48207b56b0",
                    "LayerId": "c77f7ad9-0b24-49a8-b73c-1c4dd677d846"
                },
                {
                    "id": "afbe5411-699a-4200-884a-112cee30445a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c5bf96-d33b-4b29-a61b-bd48207b56b0",
                    "LayerId": "5f401269-0bdc-44a9-8ab4-626d14377ce4"
                },
                {
                    "id": "9c09b8f4-d48e-4082-8ecd-6f2e3a979f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c5bf96-d33b-4b29-a61b-bd48207b56b0",
                    "LayerId": "409f378a-bf3a-40a4-9108-8c94a4edf0bb"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 128,
    "layers": [
        {
            "id": "5f401269-0bdc-44a9-8ab4-626d14377ce4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff6b87ae-85b4-4ea4-aa68-84bce2fa78b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "409f378a-bf3a-40a4-9108-8c94a4edf0bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff6b87ae-85b4-4ea4-aa68-84bce2fa78b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "c77f7ad9-0b24-49a8-b73c-1c4dd677d846",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff6b87ae-85b4-4ea4-aa68-84bce2fa78b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}