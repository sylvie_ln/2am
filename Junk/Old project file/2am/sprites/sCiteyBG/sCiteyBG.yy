{
    "id": "b66606fc-536a-43c0-8da0-5a4960a41c92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCiteyBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d4d5f31-5eab-437f-a30d-924cfbc54451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b66606fc-536a-43c0-8da0-5a4960a41c92",
            "compositeImage": {
                "id": "546d2bad-5cee-4c8e-aabb-9a3c546407b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4d5f31-5eab-437f-a30d-924cfbc54451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3379e4d6-f5b6-42f8-a20b-d50356920159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4d5f31-5eab-437f-a30d-924cfbc54451",
                    "LayerId": "ee66e38f-fd81-4f35-a637-5d5360e2abfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ee66e38f-fd81-4f35-a637-5d5360e2abfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b66606fc-536a-43c0-8da0-5a4960a41c92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}