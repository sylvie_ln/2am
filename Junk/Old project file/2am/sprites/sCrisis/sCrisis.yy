{
    "id": "48c2fd31-5c62-4b6c-9094-6876b8eaa9e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCrisis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5acfbab6-f43c-42b7-8385-321f7c907d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48c2fd31-5c62-4b6c-9094-6876b8eaa9e5",
            "compositeImage": {
                "id": "a1a9ed39-e36f-4ef0-9713-b0f498943348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5acfbab6-f43c-42b7-8385-321f7c907d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d2edca2-5f77-40cb-b0dc-f3b24d9c8ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5acfbab6-f43c-42b7-8385-321f7c907d35",
                    "LayerId": "526f6fa0-ff69-4e08-90ec-08e0cb2ddec0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "526f6fa0-ff69-4e08-90ec-08e0cb2ddec0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48c2fd31-5c62-4b6c-9094-6876b8eaa9e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}