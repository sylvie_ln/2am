{
    "id": "7a116cdc-f11c-4228-8097-f214fbe86b58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadLBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 19,
    "bbox_right": 27,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f84114cc-83f0-4744-a34c-e73b9950f36f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a116cdc-f11c-4228-8097-f214fbe86b58",
            "compositeImage": {
                "id": "6b4f8d07-7318-4232-8620-fc8d9c842506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f84114cc-83f0-4744-a34c-e73b9950f36f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f2b4f1-7647-4b12-a5b1-3eb77f57ed06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f84114cc-83f0-4744-a34c-e73b9950f36f",
                    "LayerId": "d426465f-e314-4c19-961c-902bbf909d96"
                },
                {
                    "id": "326c88f9-2f67-429d-b1c7-77dba2f4c4a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f84114cc-83f0-4744-a34c-e73b9950f36f",
                    "LayerId": "9d064d86-cdd6-4806-a152-3332a0beca96"
                }
            ]
        },
        {
            "id": "ed683e5b-b27f-4b34-83a3-46ef7d8ae945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a116cdc-f11c-4228-8097-f214fbe86b58",
            "compositeImage": {
                "id": "cb3e0187-30a7-4737-8d67-c3843e79e07c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed683e5b-b27f-4b34-83a3-46ef7d8ae945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3fd50a0-c9d4-4851-9cf2-6ef8f12d783b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed683e5b-b27f-4b34-83a3-46ef7d8ae945",
                    "LayerId": "d426465f-e314-4c19-961c-902bbf909d96"
                },
                {
                    "id": "15b2a376-bc69-421e-8f3a-412b4ae77411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed683e5b-b27f-4b34-83a3-46ef7d8ae945",
                    "LayerId": "9d064d86-cdd6-4806-a152-3332a0beca96"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "d426465f-e314-4c19-961c-902bbf909d96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a116cdc-f11c-4228-8097-f214fbe86b58",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9d064d86-cdd6-4806-a152-3332a0beca96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a116cdc-f11c-4228-8097-f214fbe86b58",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}