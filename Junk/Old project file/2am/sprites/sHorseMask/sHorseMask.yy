{
    "id": "643a15b8-80b5-4b81-a89c-b672edfcf465",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHorseMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 21,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2e10379-12d0-409a-87a7-4c94accb1566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "643a15b8-80b5-4b81-a89c-b672edfcf465",
            "compositeImage": {
                "id": "c93be50f-d421-434b-9102-29549398eb5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e10379-12d0-409a-87a7-4c94accb1566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6f870a4-8709-4429-9c08-e333813e27a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e10379-12d0-409a-87a7-4c94accb1566",
                    "LayerId": "b661d1da-6f2b-4ade-88c0-92c369a71059"
                },
                {
                    "id": "c209ff44-58e7-43b7-ad6b-0dab103b3cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e10379-12d0-409a-87a7-4c94accb1566",
                    "LayerId": "224803c8-c3a2-420e-b0b9-b336740bfad7"
                },
                {
                    "id": "03c1308d-0e12-48ca-a3cf-ca3718d61791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e10379-12d0-409a-87a7-4c94accb1566",
                    "LayerId": "6e610553-dc9a-4270-b806-adc99d5119ab"
                },
                {
                    "id": "d1dc5bcc-aa26-431b-b6f4-80c0740824ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e10379-12d0-409a-87a7-4c94accb1566",
                    "LayerId": "bde6700d-97e2-4ca3-b477-893d4bc6c618"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bde6700d-97e2-4ca3-b477-893d4bc6c618",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "643a15b8-80b5-4b81-a89c-b672edfcf465",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b661d1da-6f2b-4ade-88c0-92c369a71059",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "643a15b8-80b5-4b81-a89c-b672edfcf465",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "224803c8-c3a2-420e-b0b9-b336740bfad7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "643a15b8-80b5-4b81-a89c-b672edfcf465",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "6e610553-dc9a-4270-b806-adc99d5119ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "643a15b8-80b5-4b81-a89c-b672edfcf465",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}