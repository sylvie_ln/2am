{
    "id": "9917b187-bd57-41a5-b280-ed02b94a7737",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCacklingBust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a737ea15-d1a1-47ab-b596-93abbec558fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9917b187-bd57-41a5-b280-ed02b94a7737",
            "compositeImage": {
                "id": "08804fd4-478e-469e-8c8d-5cb17976717d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a737ea15-d1a1-47ab-b596-93abbec558fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30184f9d-6523-41fa-b1c0-ed1a936ef75b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a737ea15-d1a1-47ab-b596-93abbec558fc",
                    "LayerId": "b237d751-b7cb-419e-9d31-e499d625b345"
                }
            ]
        },
        {
            "id": "d1692439-d991-4132-9051-d47b463f7098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9917b187-bd57-41a5-b280-ed02b94a7737",
            "compositeImage": {
                "id": "abdb4584-719f-47a4-acdc-947b132a69ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1692439-d991-4132-9051-d47b463f7098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34a84635-b26f-4da0-90c2-689273563b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1692439-d991-4132-9051-d47b463f7098",
                    "LayerId": "b237d751-b7cb-419e-9d31-e499d625b345"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "b237d751-b7cb-419e-9d31-e499d625b345",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9917b187-bd57-41a5-b280-ed02b94a7737",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}