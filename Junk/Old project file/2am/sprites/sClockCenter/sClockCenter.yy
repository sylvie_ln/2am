{
    "id": "06550172-f7f0-48d9-8c63-8e676ac18c52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClockCenter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54b97356-1447-4150-bcf5-a4230dafbc35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06550172-f7f0-48d9-8c63-8e676ac18c52",
            "compositeImage": {
                "id": "20a1141f-e99d-4c80-a863-32939d3132c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b97356-1447-4150-bcf5-a4230dafbc35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8ae047-6e55-4ca4-8ccc-2e0122351620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b97356-1447-4150-bcf5-a4230dafbc35",
                    "LayerId": "67d34bbd-88df-44ea-8ac0-1d80a6dc9622"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "67d34bbd-88df-44ea-8ac0-1d80a6dc9622",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06550172-f7f0-48d9-8c63-8e676ac18c52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}