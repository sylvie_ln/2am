{
    "id": "01835a3e-538f-44a7-a392-a3aca9e6046f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFloppyManual",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53dfb4f3-3a15-48cf-8d26-78c18d4bde89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01835a3e-538f-44a7-a392-a3aca9e6046f",
            "compositeImage": {
                "id": "cb1272a1-c505-4fb8-976a-f5bb34620dd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53dfb4f3-3a15-48cf-8d26-78c18d4bde89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10344890-78d1-4852-97f3-36f4310684e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53dfb4f3-3a15-48cf-8d26-78c18d4bde89",
                    "LayerId": "747ca1ce-b103-498f-931b-6996e475bd39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "747ca1ce-b103-498f-931b-6996e475bd39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01835a3e-538f-44a7-a392-a3aca9e6046f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}