{
    "id": "961abb46-f487-4bba-bc4a-11083b58d93a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 56,
    "bbox_right": 63,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afbbb0d5-0632-4e7f-8574-8dda31720dc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "961abb46-f487-4bba-bc4a-11083b58d93a",
            "compositeImage": {
                "id": "2509fbfb-61b4-4d13-9d3e-766ad058264b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afbbb0d5-0632-4e7f-8574-8dda31720dc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3511df53-b9dd-4800-99ac-36d39d6527da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afbbb0d5-0632-4e7f-8574-8dda31720dc5",
                    "LayerId": "163f924e-5052-4070-8054-f26e1facad96"
                },
                {
                    "id": "e7ce2f8d-76e2-4e0d-8c6c-a17bdc2b4bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afbbb0d5-0632-4e7f-8574-8dda31720dc5",
                    "LayerId": "16e30a8c-2f2b-4ebe-bbfc-40a20977994b"
                }
            ]
        },
        {
            "id": "619fa5d6-5f1e-4bb3-a1af-1e665e2fd199",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "961abb46-f487-4bba-bc4a-11083b58d93a",
            "compositeImage": {
                "id": "d0373be5-93ab-495f-b8c5-ad34fc4332e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "619fa5d6-5f1e-4bb3-a1af-1e665e2fd199",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41c69d96-6177-4ed8-988a-a54aa8827ea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "619fa5d6-5f1e-4bb3-a1af-1e665e2fd199",
                    "LayerId": "163f924e-5052-4070-8054-f26e1facad96"
                },
                {
                    "id": "a31b86bf-5f60-4ffe-b23a-d264857476d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "619fa5d6-5f1e-4bb3-a1af-1e665e2fd199",
                    "LayerId": "16e30a8c-2f2b-4ebe-bbfc-40a20977994b"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "163f924e-5052-4070-8054-f26e1facad96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "961abb46-f487-4bba-bc4a-11083b58d93a",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "16e30a8c-2f2b-4ebe-bbfc-40a20977994b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "961abb46-f487-4bba-bc4a-11083b58d93a",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}