{
    "id": "10b01346-6347-429f-859b-306b78372d62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSunny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0deee3ba-177e-4018-8d63-aa1ac22478b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10b01346-6347-429f-859b-306b78372d62",
            "compositeImage": {
                "id": "65d1b210-dc41-45a6-93cf-b67adb80f251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0deee3ba-177e-4018-8d63-aa1ac22478b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4543bc13-ddf0-4d45-9c2d-6b7774e57815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0deee3ba-177e-4018-8d63-aa1ac22478b8",
                    "LayerId": "f9b28c68-6a67-4185-a498-e6c8d7d06a94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f9b28c68-6a67-4185-a498-e6c8d7d06a94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10b01346-6347-429f-859b-306b78372d62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}