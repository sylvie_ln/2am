{
    "id": "b7d6d741-0cda-4484-a81e-093894410924",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyColors",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c08ea4ab-9027-4bde-9c45-7d45a03c6e6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7d6d741-0cda-4484-a81e-093894410924",
            "compositeImage": {
                "id": "4f39256d-e1d2-402f-bc65-76ccc543b405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08ea4ab-9027-4bde-9c45-7d45a03c6e6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce38adea-fade-4374-a6f8-2037e8cedef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08ea4ab-9027-4bde-9c45-7d45a03c6e6b",
                    "LayerId": "79604d37-62b5-4570-be2e-0842f3643534"
                },
                {
                    "id": "b90a2003-b708-4cdf-94c1-b2e64a0e767e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08ea4ab-9027-4bde-9c45-7d45a03c6e6b",
                    "LayerId": "c7305565-d75a-4006-818e-55e5f920fa6c"
                }
            ]
        },
        {
            "id": "1fbee1bf-b54e-4fb4-942a-3b2d0b309c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7d6d741-0cda-4484-a81e-093894410924",
            "compositeImage": {
                "id": "1b1816aa-8174-4549-aecb-0e0ebb8200d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fbee1bf-b54e-4fb4-942a-3b2d0b309c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec263d2-5d87-4cca-aea1-4b75af2940bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fbee1bf-b54e-4fb4-942a-3b2d0b309c17",
                    "LayerId": "79604d37-62b5-4570-be2e-0842f3643534"
                },
                {
                    "id": "649a61be-3932-4046-86e9-49c93051ef03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fbee1bf-b54e-4fb4-942a-3b2d0b309c17",
                    "LayerId": "c7305565-d75a-4006-818e-55e5f920fa6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "79604d37-62b5-4570-be2e-0842f3643534",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7d6d741-0cda-4484-a81e-093894410924",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c7305565-d75a-4006-818e-55e5f920fa6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7d6d741-0cda-4484-a81e-093894410924",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}