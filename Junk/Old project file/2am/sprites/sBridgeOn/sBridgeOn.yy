{
    "id": "5aba7566-b6b8-451a-82ea-72f95a85a8c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBridgeOn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0944101d-6e43-4ae9-9d69-d67666994a35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aba7566-b6b8-451a-82ea-72f95a85a8c9",
            "compositeImage": {
                "id": "a0c2a8bf-d96b-4e71-9837-474dec092c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0944101d-6e43-4ae9-9d69-d67666994a35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88717c3e-6ba3-45d3-a8ab-7424c2460b93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0944101d-6e43-4ae9-9d69-d67666994a35",
                    "LayerId": "d3f3a18a-476f-4b07-ba39-47b0d09c7c2a"
                }
            ]
        },
        {
            "id": "09aa3d26-3ecc-41da-9b8d-8074ef51f86b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aba7566-b6b8-451a-82ea-72f95a85a8c9",
            "compositeImage": {
                "id": "2bd5c247-1313-4997-9dc8-a8b4952546ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09aa3d26-3ecc-41da-9b8d-8074ef51f86b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e426168-79bb-454f-82a1-6273df38f80f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09aa3d26-3ecc-41da-9b8d-8074ef51f86b",
                    "LayerId": "d3f3a18a-476f-4b07-ba39-47b0d09c7c2a"
                }
            ]
        },
        {
            "id": "6bfb50c8-1e31-4542-b482-dde717aef5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aba7566-b6b8-451a-82ea-72f95a85a8c9",
            "compositeImage": {
                "id": "f6d1d18f-11d6-4145-a2ee-6d765b60af20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bfb50c8-1e31-4542-b482-dde717aef5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1839f355-e267-45b2-8b4a-49370f0521d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bfb50c8-1e31-4542-b482-dde717aef5eb",
                    "LayerId": "d3f3a18a-476f-4b07-ba39-47b0d09c7c2a"
                }
            ]
        },
        {
            "id": "6a99a05c-2147-477b-8d0b-e38a12506192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aba7566-b6b8-451a-82ea-72f95a85a8c9",
            "compositeImage": {
                "id": "7353ecad-d607-4c7d-9274-3ae49df74382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a99a05c-2147-477b-8d0b-e38a12506192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5b3efa1-4eca-4337-9702-1a7fa9e75978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a99a05c-2147-477b-8d0b-e38a12506192",
                    "LayerId": "d3f3a18a-476f-4b07-ba39-47b0d09c7c2a"
                }
            ]
        },
        {
            "id": "d71c922e-bbc0-470c-8ecc-968de66c6244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aba7566-b6b8-451a-82ea-72f95a85a8c9",
            "compositeImage": {
                "id": "c53dd6e6-4d8d-4c2a-bc3b-ee1dff7b5a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d71c922e-bbc0-470c-8ecc-968de66c6244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d60d7149-c01c-4ff1-8759-30fb6504ac32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d71c922e-bbc0-470c-8ecc-968de66c6244",
                    "LayerId": "d3f3a18a-476f-4b07-ba39-47b0d09c7c2a"
                }
            ]
        },
        {
            "id": "91c25f8c-0dfd-4c96-88ed-81a80702446f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aba7566-b6b8-451a-82ea-72f95a85a8c9",
            "compositeImage": {
                "id": "0fc04b06-24f0-4809-a4e2-ebc2bde57908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c25f8c-0dfd-4c96-88ed-81a80702446f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda1c8f2-364e-4b29-9b60-a59a0ad79912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c25f8c-0dfd-4c96-88ed-81a80702446f",
                    "LayerId": "d3f3a18a-476f-4b07-ba39-47b0d09c7c2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d3f3a18a-476f-4b07-ba39-47b0d09c7c2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aba7566-b6b8-451a-82ea-72f95a85a8c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4294948864,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}