{
    "id": "ba9993bb-be59-42e7-9531-f59b8f70f346",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittyShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d248f075-e666-4bea-8924-2692d5b503be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
            "compositeImage": {
                "id": "e587edd7-3392-42f1-9147-dd3da6113f68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d248f075-e666-4bea-8924-2692d5b503be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d282e66-ab67-4984-8668-e0a0720535dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d248f075-e666-4bea-8924-2692d5b503be",
                    "LayerId": "dd8d6b91-2de4-4090-89c6-5ef4aaba0d6c"
                }
            ]
        },
        {
            "id": "64a23f30-9808-49f6-b79c-ac0c7eefbea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
            "compositeImage": {
                "id": "e53354a6-2c27-4860-b0e3-60b37b1cf334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a23f30-9808-49f6-b79c-ac0c7eefbea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c0c95d-6e06-4ba2-8540-b94797ea7c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a23f30-9808-49f6-b79c-ac0c7eefbea4",
                    "LayerId": "dd8d6b91-2de4-4090-89c6-5ef4aaba0d6c"
                }
            ]
        },
        {
            "id": "6f213bcd-80b2-40b1-8a0d-bc1a270701a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
            "compositeImage": {
                "id": "445c7173-70cd-413a-8a5b-a11d252f3f18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f213bcd-80b2-40b1-8a0d-bc1a270701a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94e9db7f-3e6a-4b0c-a7cd-43b5c12b61c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f213bcd-80b2-40b1-8a0d-bc1a270701a7",
                    "LayerId": "dd8d6b91-2de4-4090-89c6-5ef4aaba0d6c"
                }
            ]
        },
        {
            "id": "19db72db-4a6b-460a-8752-7cf51c5abbfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
            "compositeImage": {
                "id": "14862a2c-3761-4230-a17f-09a2198b7cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19db72db-4a6b-460a-8752-7cf51c5abbfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b921f21-e531-484d-83c1-e3d9222e9887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19db72db-4a6b-460a-8752-7cf51c5abbfc",
                    "LayerId": "dd8d6b91-2de4-4090-89c6-5ef4aaba0d6c"
                }
            ]
        },
        {
            "id": "de5c2dfe-5d1a-4f39-94c2-b5e1789ddba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
            "compositeImage": {
                "id": "94822cd5-20d2-46aa-9946-da37413b9877",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de5c2dfe-5d1a-4f39-94c2-b5e1789ddba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890ef945-7598-405c-9032-341f0d2dfba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de5c2dfe-5d1a-4f39-94c2-b5e1789ddba0",
                    "LayerId": "dd8d6b91-2de4-4090-89c6-5ef4aaba0d6c"
                }
            ]
        },
        {
            "id": "487b54d2-ec45-461b-9b91-cdcb5e8dfbd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
            "compositeImage": {
                "id": "5f879f86-3708-444f-a878-4f889a7e9d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "487b54d2-ec45-461b-9b91-cdcb5e8dfbd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0503443d-9668-4c2f-a659-06334c0b3fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487b54d2-ec45-461b-9b91-cdcb5e8dfbd0",
                    "LayerId": "dd8d6b91-2de4-4090-89c6-5ef4aaba0d6c"
                }
            ]
        },
        {
            "id": "349512f7-2559-415d-b784-1c25cbd496e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
            "compositeImage": {
                "id": "8cb52b61-a322-4567-8276-b2cf5645083d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349512f7-2559-415d-b784-1c25cbd496e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d07f0d8-3429-434c-9235-3e56a1841a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349512f7-2559-415d-b784-1c25cbd496e4",
                    "LayerId": "dd8d6b91-2de4-4090-89c6-5ef4aaba0d6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dd8d6b91-2de4-4090-89c6-5ef4aaba0d6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}