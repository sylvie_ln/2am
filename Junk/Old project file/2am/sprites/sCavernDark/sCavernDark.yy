{
    "id": "6408b884-e7ba-4d38-aac6-1ba934dea498",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCavernDark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85a7e8a5-f21e-4ae9-88fe-15ea635dae65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6408b884-e7ba-4d38-aac6-1ba934dea498",
            "compositeImage": {
                "id": "fe9ea252-19a3-4496-bde5-8f0531ac687f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a7e8a5-f21e-4ae9-88fe-15ea635dae65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29d1ff94-444e-47bf-90f8-d4f253aba54e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a7e8a5-f21e-4ae9-88fe-15ea635dae65",
                    "LayerId": "56d65be3-b54e-4105-9339-a5d1bfec61e4"
                }
            ]
        },
        {
            "id": "b2660880-2de5-42c6-ad6c-1ccc750015eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6408b884-e7ba-4d38-aac6-1ba934dea498",
            "compositeImage": {
                "id": "adb34196-c053-4629-a603-fc92381598e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2660880-2de5-42c6-ad6c-1ccc750015eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb85d1ef-a31d-404d-a4ea-efc0f3178e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2660880-2de5-42c6-ad6c-1ccc750015eb",
                    "LayerId": "56d65be3-b54e-4105-9339-a5d1bfec61e4"
                }
            ]
        },
        {
            "id": "fd492b2b-0c52-4207-9746-ef52d855416c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6408b884-e7ba-4d38-aac6-1ba934dea498",
            "compositeImage": {
                "id": "784a81dc-6736-4c6c-9895-f72a2e53ca58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd492b2b-0c52-4207-9746-ef52d855416c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "792635f7-3e22-4f12-aecb-6016cf04ce3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd492b2b-0c52-4207-9746-ef52d855416c",
                    "LayerId": "56d65be3-b54e-4105-9339-a5d1bfec61e4"
                }
            ]
        },
        {
            "id": "b1102727-782b-4c35-8f44-197351e31a3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6408b884-e7ba-4d38-aac6-1ba934dea498",
            "compositeImage": {
                "id": "5d22c442-631d-4725-9778-b8a24df2426f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1102727-782b-4c35-8f44-197351e31a3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48db0f73-6819-483a-9e9d-bcfa570b13f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1102727-782b-4c35-8f44-197351e31a3e",
                    "LayerId": "56d65be3-b54e-4105-9339-a5d1bfec61e4"
                }
            ]
        },
        {
            "id": "d7c827be-db4b-4755-8b94-4bcf0f49213e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6408b884-e7ba-4d38-aac6-1ba934dea498",
            "compositeImage": {
                "id": "fa14f8c4-c2b6-403b-aaed-9b873937bea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c827be-db4b-4755-8b94-4bcf0f49213e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eeff89c-f657-4d3b-b601-fe861a895d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c827be-db4b-4755-8b94-4bcf0f49213e",
                    "LayerId": "56d65be3-b54e-4105-9339-a5d1bfec61e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "56d65be3-b54e-4105-9339-a5d1bfec61e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6408b884-e7ba-4d38-aac6-1ba934dea498",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}