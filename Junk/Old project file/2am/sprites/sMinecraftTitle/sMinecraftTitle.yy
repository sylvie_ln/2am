{
    "id": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecraftTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 165,
    "bbox_left": 0,
    "bbox_right": 270,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ff1b190-7453-4b54-8f4c-139c767d7c55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "compositeImage": {
                "id": "392f0d6c-b285-47f0-bfce-02f8adb25fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff1b190-7453-4b54-8f4c-139c767d7c55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2ea57f8-66b6-4e7d-a123-27894332670a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff1b190-7453-4b54-8f4c-139c767d7c55",
                    "LayerId": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3"
                }
            ]
        },
        {
            "id": "740f7d45-a6a7-49a5-b85b-866718fdcb4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "compositeImage": {
                "id": "65043451-0791-45cd-930d-5d8f27787cef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740f7d45-a6a7-49a5-b85b-866718fdcb4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25df7f8d-ee80-4087-bc66-fdc8521d33f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740f7d45-a6a7-49a5-b85b-866718fdcb4b",
                    "LayerId": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3"
                }
            ]
        },
        {
            "id": "a29a8bdf-322a-441d-bed6-a4fd241ee1e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "compositeImage": {
                "id": "980b214c-a71b-46c0-865e-75244916677e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a29a8bdf-322a-441d-bed6-a4fd241ee1e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a29eacd-b289-4ff7-b55f-9c06ffab5134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a29a8bdf-322a-441d-bed6-a4fd241ee1e2",
                    "LayerId": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3"
                }
            ]
        },
        {
            "id": "c118e1a8-4bd7-413e-9d90-1d657561d276",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "compositeImage": {
                "id": "d8eba96d-fac3-4f71-ad2a-fde623058af7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c118e1a8-4bd7-413e-9d90-1d657561d276",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd621007-a840-43d8-898e-2a8bb9208a48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c118e1a8-4bd7-413e-9d90-1d657561d276",
                    "LayerId": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3"
                }
            ]
        },
        {
            "id": "31d9d1fa-6e8c-4243-81b4-ba9eb4989016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "compositeImage": {
                "id": "9a5262fa-ef09-4853-b504-2dc59c28562b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d9d1fa-6e8c-4243-81b4-ba9eb4989016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9b078b7-08fc-46c1-92ca-9df1b7178e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d9d1fa-6e8c-4243-81b4-ba9eb4989016",
                    "LayerId": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3"
                }
            ]
        },
        {
            "id": "e05fe1e9-880c-4ac0-8d9c-4b4247db09e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "compositeImage": {
                "id": "885c8867-3048-4edd-af83-4f3df22aac2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05fe1e9-880c-4ac0-8d9c-4b4247db09e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ead320d-26f1-4c25-881e-93a557d2328d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05fe1e9-880c-4ac0-8d9c-4b4247db09e4",
                    "LayerId": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3"
                }
            ]
        },
        {
            "id": "571dfd64-380e-4464-ac0f-d3c4e7dbf7bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "compositeImage": {
                "id": "7b75fea9-4c0f-4be3-8847-8587f1c0d4d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "571dfd64-380e-4464-ac0f-d3c4e7dbf7bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658af451-1682-4b32-87f4-09195c767131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "571dfd64-380e-4464-ac0f-d3c4e7dbf7bb",
                    "LayerId": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3"
                }
            ]
        },
        {
            "id": "c2783cba-3941-4ffd-a677-a79dcece4cf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "compositeImage": {
                "id": "6da66ef0-9bed-4717-b490-e8515e736dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2783cba-3941-4ffd-a677-a79dcece4cf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c84fc257-16c4-4f11-ba46-1cb2d331b94c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2783cba-3941-4ffd-a677-a79dcece4cf7",
                    "LayerId": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "e6cb37b5-cfc2-4bcb-bcfe-8a98fe5ec9c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fed75f4f-36ce-48c5-bbcc-a7e727eb7ddf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}