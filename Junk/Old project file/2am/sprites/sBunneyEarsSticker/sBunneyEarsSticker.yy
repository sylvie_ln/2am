{
    "id": "14068078-3299-4859-9781-d855cc041556",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyEarsSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff301b5e-82ce-4e31-8c6e-56acb93818c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14068078-3299-4859-9781-d855cc041556",
            "compositeImage": {
                "id": "b845c783-65a8-47fe-b835-c9dc8efc08bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff301b5e-82ce-4e31-8c6e-56acb93818c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c326b874-f02c-44e3-8230-d48988e17463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff301b5e-82ce-4e31-8c6e-56acb93818c4",
                    "LayerId": "2137151a-ddd5-4f1f-bb41-96c7dcd951a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2137151a-ddd5-4f1f-bb41-96c7dcd951a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14068078-3299-4859-9781-d855cc041556",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}