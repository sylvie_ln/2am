{
    "id": "9afc8854-202b-4031-b8ef-8ffef7bf3e6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 33,
    "bbox_right": 78,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e124cfce-557f-47fc-8637-22c93152e20c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afc8854-202b-4031-b8ef-8ffef7bf3e6b",
            "compositeImage": {
                "id": "4f90ff26-051e-4da6-a0e1-1c4881396fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e124cfce-557f-47fc-8637-22c93152e20c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0d8da5-d224-4e61-b927-f3b164e12eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e124cfce-557f-47fc-8637-22c93152e20c",
                    "LayerId": "6ec80849-a24f-46b6-9fdc-06ced6e16c7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "6ec80849-a24f-46b6-9fdc-06ced6e16c7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9afc8854-202b-4031-b8ef-8ffef7bf3e6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}