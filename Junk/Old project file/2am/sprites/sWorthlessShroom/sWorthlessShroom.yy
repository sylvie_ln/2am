{
    "id": "c91a7e70-cc93-4d5a-b2f0-13b69fa3cb7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWorthlessShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4752843a-0293-4bc9-9d15-70c60ae55657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a7e70-cc93-4d5a-b2f0-13b69fa3cb7b",
            "compositeImage": {
                "id": "257e7f96-d3da-4ed5-a037-e0457f04d6b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4752843a-0293-4bc9-9d15-70c60ae55657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7a8f53e-525c-419d-ac73-301ff5147374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4752843a-0293-4bc9-9d15-70c60ae55657",
                    "LayerId": "0ab5aa10-ec05-4c35-b78f-71073c78a529"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0ab5aa10-ec05-4c35-b78f-71073c78a529",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c91a7e70-cc93-4d5a-b2f0-13b69fa3cb7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}