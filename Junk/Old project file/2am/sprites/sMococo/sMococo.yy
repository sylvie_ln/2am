{
    "id": "e3b2fb1d-32c7-4fbe-99f5-03ddf717d6e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMococo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0663343a-f54f-4687-813f-a6a568f7d0b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3b2fb1d-32c7-4fbe-99f5-03ddf717d6e6",
            "compositeImage": {
                "id": "2761ff15-be12-4a60-ba9f-c68adaa84c31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0663343a-f54f-4687-813f-a6a568f7d0b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e38670e1-c693-4ea3-80f0-9ddf2d842691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0663343a-f54f-4687-813f-a6a568f7d0b9",
                    "LayerId": "df64bc5b-1a24-4ae6-aa7c-56298a956cbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "df64bc5b-1a24-4ae6-aa7c-56298a956cbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3b2fb1d-32c7-4fbe-99f5-03ddf717d6e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}