{
    "id": "90724f99-f940-4cd7-8902-74907daf3020",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 3,
    "bbox_right": 77,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fdcaf89-a25c-4daa-a3cb-126a65ad243b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90724f99-f940-4cd7-8902-74907daf3020",
            "compositeImage": {
                "id": "347265fd-a81b-4bd7-872d-1c98a8d0b8fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fdcaf89-a25c-4daa-a3cb-126a65ad243b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "606e478a-38c8-4eb3-b4d5-2864c1c656e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fdcaf89-a25c-4daa-a3cb-126a65ad243b",
                    "LayerId": "a7c42e6d-3881-4144-83c3-07fea83e97b0"
                }
            ]
        },
        {
            "id": "e99b6bb2-b759-4051-af6b-c864405be209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90724f99-f940-4cd7-8902-74907daf3020",
            "compositeImage": {
                "id": "cdb2dab7-f843-49e7-9761-3ef9402c9eb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e99b6bb2-b759-4051-af6b-c864405be209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c937209-0d1e-46d6-9e0f-ba3982c1fe86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e99b6bb2-b759-4051-af6b-c864405be209",
                    "LayerId": "a7c42e6d-3881-4144-83c3-07fea83e97b0"
                }
            ]
        },
        {
            "id": "8b61fdcd-4f8f-4679-b1ef-914db382e5df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90724f99-f940-4cd7-8902-74907daf3020",
            "compositeImage": {
                "id": "a93f030f-6585-4746-bff4-2b86cdb24db0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b61fdcd-4f8f-4679-b1ef-914db382e5df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c5738a-2b4d-4a67-8193-3d1dec85160e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b61fdcd-4f8f-4679-b1ef-914db382e5df",
                    "LayerId": "a7c42e6d-3881-4144-83c3-07fea83e97b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a7c42e6d-3881-4144-83c3-07fea83e97b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90724f99-f940-4cd7-8902-74907daf3020",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 47
}