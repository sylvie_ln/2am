{
    "id": "8905efbf-f767-48d9-a6b4-567f21f81db9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMuseumBagInterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 7,
    "bbox_right": 88,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "240a3448-9a39-4111-893f-efc907be5da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8905efbf-f767-48d9-a6b4-567f21f81db9",
            "compositeImage": {
                "id": "17def6cd-4a58-416a-ae18-6e216116bb4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "240a3448-9a39-4111-893f-efc907be5da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d05bea02-4e60-4c83-a979-2c21fcf39dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240a3448-9a39-4111-893f-efc907be5da3",
                    "LayerId": "d3647edf-0986-4efa-874b-0943fda4f26d"
                },
                {
                    "id": "5318fda7-05f5-4e5f-a231-ef09cba8c93a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240a3448-9a39-4111-893f-efc907be5da3",
                    "LayerId": "eae7b02c-c1bd-4ea5-a965-3b8779437c61"
                },
                {
                    "id": "c070b503-1e72-4f5b-b4e8-b782963d7a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240a3448-9a39-4111-893f-efc907be5da3",
                    "LayerId": "fa58ccab-43a0-4b8a-843e-b4ddd7e37e1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d3647edf-0986-4efa-874b-0943fda4f26d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8905efbf-f767-48d9-a6b4-567f21f81db9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "eae7b02c-c1bd-4ea5-a965-3b8779437c61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8905efbf-f767-48d9-a6b4-567f21f81db9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "fa58ccab-43a0-4b8a-843e-b4ddd7e37e1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8905efbf-f767-48d9-a6b4-567f21f81db9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 60
}