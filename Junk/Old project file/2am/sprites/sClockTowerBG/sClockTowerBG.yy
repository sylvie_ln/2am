{
    "id": "c75363ee-e739-4f82-893c-063cc89956df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClockTowerBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04c94074-0c37-4c9b-a543-6a2b669fc6fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75363ee-e739-4f82-893c-063cc89956df",
            "compositeImage": {
                "id": "bbe2223a-e330-4de7-a172-ed4a1da81c95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04c94074-0c37-4c9b-a543-6a2b669fc6fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aad75bb-fc84-41bc-919c-5e852cd82ca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c94074-0c37-4c9b-a543-6a2b669fc6fd",
                    "LayerId": "d92c3d23-549c-4de6-a49b-b0bde8f5411e"
                },
                {
                    "id": "2863fbc5-cae5-4545-921a-665edcf9dd3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c94074-0c37-4c9b-a543-6a2b669fc6fd",
                    "LayerId": "edba52e1-0c8d-4382-94df-0ea4aff3f858"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d92c3d23-549c-4de6-a49b-b0bde8f5411e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c75363ee-e739-4f82-893c-063cc89956df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "edba52e1-0c8d-4382-94df-0ea4aff3f858",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c75363ee-e739-4f82-893c-063cc89956df",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 24
}