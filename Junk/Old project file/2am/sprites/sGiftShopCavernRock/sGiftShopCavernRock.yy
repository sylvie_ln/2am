{
    "id": "cda4ff8c-76d5-400a-a2fe-eb7453307099",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopCavernRock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb360f20-bab2-4f39-929d-6db3afa7f1bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cda4ff8c-76d5-400a-a2fe-eb7453307099",
            "compositeImage": {
                "id": "031b9e13-6c28-4217-9a0e-7f4e80c9c2ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb360f20-bab2-4f39-929d-6db3afa7f1bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95ff4152-9864-4d4d-9e22-61809c0f5d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb360f20-bab2-4f39-929d-6db3afa7f1bb",
                    "LayerId": "2d8abcb4-6357-49de-902b-20d61b42c0c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2d8abcb4-6357-49de-902b-20d61b42c0c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cda4ff8c-76d5-400a-a2fe-eb7453307099",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}