{
    "id": "faebf91e-0da4-4229-935a-15f0d3936ca1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPuzzlePiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01d4404e-332b-4864-bc4c-05e5498508f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faebf91e-0da4-4229-935a-15f0d3936ca1",
            "compositeImage": {
                "id": "a1fd1322-cc65-477a-9239-da7968db3fb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d4404e-332b-4864-bc4c-05e5498508f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9801c1ba-67aa-4962-8b84-45a05188142e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d4404e-332b-4864-bc4c-05e5498508f6",
                    "LayerId": "6c7aa458-10e5-4e44-bdea-fd8a4e204638"
                },
                {
                    "id": "0d80cc6c-23c3-4ef8-92fe-2cab8b81b468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d4404e-332b-4864-bc4c-05e5498508f6",
                    "LayerId": "9e766324-1ef4-4341-9177-0489724f4653"
                }
            ]
        },
        {
            "id": "1b6a1423-dd84-418d-b29e-b57076908dbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faebf91e-0da4-4229-935a-15f0d3936ca1",
            "compositeImage": {
                "id": "de6075ec-e22a-40e2-a4a9-64054be1d100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b6a1423-dd84-418d-b29e-b57076908dbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a829874d-48c9-4ee9-a6d0-5ed3539a1178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b6a1423-dd84-418d-b29e-b57076908dbf",
                    "LayerId": "6c7aa458-10e5-4e44-bdea-fd8a4e204638"
                },
                {
                    "id": "4b5cd448-88c0-43f2-840e-5f8da16e3f9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b6a1423-dd84-418d-b29e-b57076908dbf",
                    "LayerId": "9e766324-1ef4-4341-9177-0489724f4653"
                }
            ]
        },
        {
            "id": "cbf178d4-b0a4-4d5a-a791-07bd23c056ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faebf91e-0da4-4229-935a-15f0d3936ca1",
            "compositeImage": {
                "id": "85c79ee7-ccca-4c53-bd8e-7294c4629133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf178d4-b0a4-4d5a-a791-07bd23c056ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca262fbf-d9c0-4892-88cc-1297af587b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf178d4-b0a4-4d5a-a791-07bd23c056ea",
                    "LayerId": "6c7aa458-10e5-4e44-bdea-fd8a4e204638"
                },
                {
                    "id": "cd6da86c-1afb-4df6-a364-d180bf73e0dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf178d4-b0a4-4d5a-a791-07bd23c056ea",
                    "LayerId": "9e766324-1ef4-4341-9177-0489724f4653"
                }
            ]
        },
        {
            "id": "eb65f5f7-cdfa-4cf7-932e-323f7f0f4888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faebf91e-0da4-4229-935a-15f0d3936ca1",
            "compositeImage": {
                "id": "f47c3e53-3564-4910-b0c9-e9587018a1c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb65f5f7-cdfa-4cf7-932e-323f7f0f4888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab6feb6-11df-402f-9c1a-4294b8edb10d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb65f5f7-cdfa-4cf7-932e-323f7f0f4888",
                    "LayerId": "6c7aa458-10e5-4e44-bdea-fd8a4e204638"
                },
                {
                    "id": "bde11312-cf3b-4e9a-8054-eca1bb6720a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb65f5f7-cdfa-4cf7-932e-323f7f0f4888",
                    "LayerId": "9e766324-1ef4-4341-9177-0489724f4653"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9e766324-1ef4-4341-9177-0489724f4653",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "faebf91e-0da4-4229-935a-15f0d3936ca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 25,
            "visible": false
        },
        {
            "id": "6c7aa458-10e5-4e44-bdea-fd8a4e204638",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "faebf91e-0da4-4229-935a-15f0d3936ca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}