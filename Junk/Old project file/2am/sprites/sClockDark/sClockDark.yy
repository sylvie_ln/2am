{
    "id": "36991cee-afff-4803-bc96-f437c5228695",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClockDark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61a4ee95-ca62-42f1-a924-610fc0c67e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36991cee-afff-4803-bc96-f437c5228695",
            "compositeImage": {
                "id": "8710b8ef-88d3-4d02-a30c-e4f2332e0ec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61a4ee95-ca62-42f1-a924-610fc0c67e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44600cd1-5155-4a60-adf5-693acb663725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61a4ee95-ca62-42f1-a924-610fc0c67e39",
                    "LayerId": "55b6d8eb-bc85-4591-94f4-f6d8c6c73c8f"
                }
            ]
        },
        {
            "id": "e4de639d-c875-458b-9926-4cf3215c5735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36991cee-afff-4803-bc96-f437c5228695",
            "compositeImage": {
                "id": "f10afae7-9159-4f4c-9a98-e737c0e71329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4de639d-c875-458b-9926-4cf3215c5735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2297eb0e-b8db-4970-b23f-5241d2637d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4de639d-c875-458b-9926-4cf3215c5735",
                    "LayerId": "55b6d8eb-bc85-4591-94f4-f6d8c6c73c8f"
                }
            ]
        },
        {
            "id": "e2028d5d-8725-4d6c-8d8b-5d059f215468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36991cee-afff-4803-bc96-f437c5228695",
            "compositeImage": {
                "id": "8e3b3ff3-9ec0-421a-b31c-7b41a4fdcb1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2028d5d-8725-4d6c-8d8b-5d059f215468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d87b07-5ed4-44c2-bd33-25b394daca24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2028d5d-8725-4d6c-8d8b-5d059f215468",
                    "LayerId": "55b6d8eb-bc85-4591-94f4-f6d8c6c73c8f"
                }
            ]
        },
        {
            "id": "37bab699-6b35-4831-a05b-e0a347e48136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36991cee-afff-4803-bc96-f437c5228695",
            "compositeImage": {
                "id": "d9a7dac3-c50c-47d2-96bc-6662635f5846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37bab699-6b35-4831-a05b-e0a347e48136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3145fb2c-7b86-40df-9edd-5000b15f78c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37bab699-6b35-4831-a05b-e0a347e48136",
                    "LayerId": "55b6d8eb-bc85-4591-94f4-f6d8c6c73c8f"
                }
            ]
        },
        {
            "id": "59b9d681-f3f8-4121-8534-92bc51b92dc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36991cee-afff-4803-bc96-f437c5228695",
            "compositeImage": {
                "id": "a4ba3573-b8d3-4e5f-8217-3fa250df518d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b9d681-f3f8-4121-8534-92bc51b92dc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde681da-1ca4-4ed0-8aba-fd0cd1ca00ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b9d681-f3f8-4121-8534-92bc51b92dc5",
                    "LayerId": "55b6d8eb-bc85-4591-94f4-f6d8c6c73c8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "55b6d8eb-bc85-4591-94f4-f6d8c6c73c8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36991cee-afff-4803-bc96-f437c5228695",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}