{
    "id": "af271882-604b-4155-aca5-31753aae64f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFishCarRideMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 21,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43463024-af2f-4a86-a415-f7ae3d25abb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af271882-604b-4155-aca5-31753aae64f5",
            "compositeImage": {
                "id": "269b3521-45e6-4f43-98cc-2bb273079b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43463024-af2f-4a86-a415-f7ae3d25abb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ce7f211-dab3-4474-a8f3-4bc53549eca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43463024-af2f-4a86-a415-f7ae3d25abb3",
                    "LayerId": "65cfdcdf-c3c1-4fe3-aa07-8d021453b7bd"
                },
                {
                    "id": "504630f0-e5b5-4685-87bd-68ca8fd43fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43463024-af2f-4a86-a415-f7ae3d25abb3",
                    "LayerId": "acbe2f73-597e-44be-b0f8-23bd1ff17c20"
                },
                {
                    "id": "3058cb7c-8df2-4494-ae96-49f2ff101937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43463024-af2f-4a86-a415-f7ae3d25abb3",
                    "LayerId": "5f28562a-4178-47b6-adde-440d3ba221d5"
                }
            ]
        }
    ],
    "gridX": 12,
    "gridY": 12,
    "height": 24,
    "layers": [
        {
            "id": "5f28562a-4178-47b6-adde-440d3ba221d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af271882-604b-4155-aca5-31753aae64f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "65cfdcdf-c3c1-4fe3-aa07-8d021453b7bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af271882-604b-4155-aca5-31753aae64f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "acbe2f73-597e-44be-b0f8-23bd1ff17c20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af271882-604b-4155-aca5-31753aae64f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}