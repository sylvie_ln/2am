{
    "id": "a54d9b80-1b6f-437b-8848-b242e18b570c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWirte",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f57f096b-a20b-4917-9f9d-0eba89aa3eab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a54d9b80-1b6f-437b-8848-b242e18b570c",
            "compositeImage": {
                "id": "f6acc812-0006-4cba-a4eb-e8455018990b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f57f096b-a20b-4917-9f9d-0eba89aa3eab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b1ffe86-9200-4b0f-812c-65f37bec57c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f57f096b-a20b-4917-9f9d-0eba89aa3eab",
                    "LayerId": "778ae63b-6ef3-40ad-b397-408325290a36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "778ae63b-6ef3-40ad-b397-408325290a36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a54d9b80-1b6f-437b-8848-b242e18b570c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}