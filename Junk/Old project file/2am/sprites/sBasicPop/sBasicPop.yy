{
    "id": "bdd27172-12c7-499e-b69e-18fc936c83a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBasicPop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d6f2498-6e58-4c5c-a57e-311ecd1b7fae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
            "compositeImage": {
                "id": "20bc330d-fc8b-4a3a-a129-7391e083dd7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d6f2498-6e58-4c5c-a57e-311ecd1b7fae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bef78717-e1fa-41f4-9e86-b0d6d1829451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d6f2498-6e58-4c5c-a57e-311ecd1b7fae",
                    "LayerId": "c21671c5-3305-4200-bd15-795240bbb097"
                }
            ]
        },
        {
            "id": "6a1109eb-f061-435a-bb4e-fe933fab8832",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
            "compositeImage": {
                "id": "c35a968b-2c61-4af2-8f86-70df0f3c2f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a1109eb-f061-435a-bb4e-fe933fab8832",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe681dc-cb47-4758-b17a-4a757da04c84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a1109eb-f061-435a-bb4e-fe933fab8832",
                    "LayerId": "c21671c5-3305-4200-bd15-795240bbb097"
                }
            ]
        },
        {
            "id": "bef668db-e3f0-48f8-9c8a-1565837f22be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
            "compositeImage": {
                "id": "463d5831-df14-49e6-8d7e-ddd4c821461e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bef668db-e3f0-48f8-9c8a-1565837f22be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62504519-5fb2-49a7-bc47-698c03b4255b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bef668db-e3f0-48f8-9c8a-1565837f22be",
                    "LayerId": "c21671c5-3305-4200-bd15-795240bbb097"
                }
            ]
        },
        {
            "id": "2bd924df-08c7-4cd9-8566-715c19153540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
            "compositeImage": {
                "id": "55824dfd-6a00-40a6-be63-68d24ea976a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd924df-08c7-4cd9-8566-715c19153540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f37ad39-c11a-4b57-a24f-4e260699530a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd924df-08c7-4cd9-8566-715c19153540",
                    "LayerId": "c21671c5-3305-4200-bd15-795240bbb097"
                }
            ]
        },
        {
            "id": "b75ec093-12a0-4528-9860-f82a7f5417e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
            "compositeImage": {
                "id": "cee20b86-ca96-4cd7-9291-7b86225abef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b75ec093-12a0-4528-9860-f82a7f5417e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e719d72f-f4ba-486a-85e4-71c4cbc2f2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b75ec093-12a0-4528-9860-f82a7f5417e1",
                    "LayerId": "c21671c5-3305-4200-bd15-795240bbb097"
                }
            ]
        },
        {
            "id": "1924f5fe-a875-455d-82fa-48a3fe49f4a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
            "compositeImage": {
                "id": "106b26a6-7e01-4ed2-8eda-f95f1a2ef4dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1924f5fe-a875-455d-82fa-48a3fe49f4a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "216f6165-a9a7-4fa5-8d37-fb6593258630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1924f5fe-a875-455d-82fa-48a3fe49f4a0",
                    "LayerId": "c21671c5-3305-4200-bd15-795240bbb097"
                }
            ]
        },
        {
            "id": "bfc0bfc5-97d5-48ec-bbf0-fba2a7deec83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
            "compositeImage": {
                "id": "edca54f9-e67d-44fe-868a-f8e3c9dc9e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfc0bfc5-97d5-48ec-bbf0-fba2a7deec83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d105d5-89da-4314-aec0-a7e0a099695c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfc0bfc5-97d5-48ec-bbf0-fba2a7deec83",
                    "LayerId": "c21671c5-3305-4200-bd15-795240bbb097"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c21671c5-3305-4200-bd15-795240bbb097",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdd27172-12c7-499e-b69e-18fc936c83a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}