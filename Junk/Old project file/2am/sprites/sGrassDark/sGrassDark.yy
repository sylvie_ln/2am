{
    "id": "1ed19ba7-d9c2-4fc7-b05d-432091e2cb41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrassDark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba4f7ea7-1af7-4246-a448-069dd14adaab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed19ba7-d9c2-4fc7-b05d-432091e2cb41",
            "compositeImage": {
                "id": "6f1d827f-30b4-42b1-a77f-b3eead42b3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba4f7ea7-1af7-4246-a448-069dd14adaab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8eb2381-8b94-4839-9fc6-499e28c16195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4f7ea7-1af7-4246-a448-069dd14adaab",
                    "LayerId": "46487f19-d3a1-4ec0-a59e-33b8eebc3617"
                }
            ]
        },
        {
            "id": "7388acb0-cdb5-4c82-a1a0-b891964414b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed19ba7-d9c2-4fc7-b05d-432091e2cb41",
            "compositeImage": {
                "id": "e21fb88c-f240-4087-97cf-e81155436773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7388acb0-cdb5-4c82-a1a0-b891964414b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab87ce5-6d21-4059-91f6-92a8e4cd0b41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7388acb0-cdb5-4c82-a1a0-b891964414b2",
                    "LayerId": "46487f19-d3a1-4ec0-a59e-33b8eebc3617"
                }
            ]
        },
        {
            "id": "f8063744-9772-498a-8ed3-a00f61217c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed19ba7-d9c2-4fc7-b05d-432091e2cb41",
            "compositeImage": {
                "id": "bb6e9b8d-bb66-43bc-ae56-a4dcd52a0a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8063744-9772-498a-8ed3-a00f61217c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9413f2e9-9212-4e1f-940e-1f4fa775b544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8063744-9772-498a-8ed3-a00f61217c03",
                    "LayerId": "46487f19-d3a1-4ec0-a59e-33b8eebc3617"
                }
            ]
        },
        {
            "id": "d97d5d8c-d86b-45e5-80dc-2bdb67da5d57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed19ba7-d9c2-4fc7-b05d-432091e2cb41",
            "compositeImage": {
                "id": "9e284d2a-17ee-4524-840a-af969c965a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d97d5d8c-d86b-45e5-80dc-2bdb67da5d57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e8a58e-7fcb-4437-9f96-330006eef884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d97d5d8c-d86b-45e5-80dc-2bdb67da5d57",
                    "LayerId": "46487f19-d3a1-4ec0-a59e-33b8eebc3617"
                }
            ]
        },
        {
            "id": "320dc710-7332-45bc-a788-1cf88cb97820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed19ba7-d9c2-4fc7-b05d-432091e2cb41",
            "compositeImage": {
                "id": "5a3f21c0-b2e5-4d57-9369-f931ad8a9119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "320dc710-7332-45bc-a788-1cf88cb97820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb46c850-01d9-44c7-9918-1a4b29cb8968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "320dc710-7332-45bc-a788-1cf88cb97820",
                    "LayerId": "46487f19-d3a1-4ec0-a59e-33b8eebc3617"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "46487f19-d3a1-4ec0-a59e-33b8eebc3617",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ed19ba7-d9c2-4fc7-b05d-432091e2cb41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}