{
    "id": "c2f95f76-5786-4df2-aa71-d9287ea9c371",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBottledFlareSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "200a48f4-49be-4c84-ad8f-1a50776b563b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2f95f76-5786-4df2-aa71-d9287ea9c371",
            "compositeImage": {
                "id": "57fe0205-ddc5-43d1-aa96-9189ad40912a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "200a48f4-49be-4c84-ad8f-1a50776b563b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f43ca52-cecd-4026-ac43-b8dd1b46c7c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "200a48f4-49be-4c84-ad8f-1a50776b563b",
                    "LayerId": "864faa85-dee1-4ed2-a2c1-0d61f6aa9ea0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "864faa85-dee1-4ed2-a2c1-0d61f6aa9ea0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2f95f76-5786-4df2-aa71-d9287ea9c371",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}