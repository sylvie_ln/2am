{
    "id": "c1ecb0bb-e073-4a5a-bbaa-bd157a957a49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGalfio",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c00e7fc-5b0e-44e6-8ffa-dc1d460e22d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1ecb0bb-e073-4a5a-bbaa-bd157a957a49",
            "compositeImage": {
                "id": "1c1b1f90-4766-4367-91e5-129b14e49719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c00e7fc-5b0e-44e6-8ffa-dc1d460e22d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f66a913c-7a34-460c-91fb-b3193d840748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c00e7fc-5b0e-44e6-8ffa-dc1d460e22d0",
                    "LayerId": "77d3f229-86e8-46c2-adf9-d1a2cfeb8f0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "77d3f229-86e8-46c2-adf9-d1a2cfeb8f0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1ecb0bb-e073-4a5a-bbaa-bd157a957a49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}