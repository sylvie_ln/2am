{
    "id": "6f85da44-327c-4de9-93d5-005c6b9ddbd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloudKeySticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8783532-c596-4587-b018-9c03b3a8ea86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f85da44-327c-4de9-93d5-005c6b9ddbd1",
            "compositeImage": {
                "id": "dd0c9091-986c-4bb6-9943-4fbfcbe17593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8783532-c596-4587-b018-9c03b3a8ea86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0d31120-6a91-4bf1-a59a-21b61acfa6c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8783532-c596-4587-b018-9c03b3a8ea86",
                    "LayerId": "71eeee03-63ba-4564-901f-1963a14e512f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "71eeee03-63ba-4564-901f-1963a14e512f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f85da44-327c-4de9-93d5-005c6b9ddbd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}