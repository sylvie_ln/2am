{
    "id": "e0567e8f-0aeb-4242-b5f0-ee2d8b8dcbe4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMushroomQuesoGrande",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb0d5438-f785-4e07-afdd-87230faa763a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0567e8f-0aeb-4242-b5f0-ee2d8b8dcbe4",
            "compositeImage": {
                "id": "4154bc9b-5fbc-486d-a5d5-f67fc56ac041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb0d5438-f785-4e07-afdd-87230faa763a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92515a42-cd69-4fdc-ae90-af9938d7e279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb0d5438-f785-4e07-afdd-87230faa763a",
                    "LayerId": "3e4ca67f-c604-4cb6-bf95-e9e6ab127f11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3e4ca67f-c604-4cb6-bf95-e9e6ab127f11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0567e8f-0aeb-4242-b5f0-ee2d8b8dcbe4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}