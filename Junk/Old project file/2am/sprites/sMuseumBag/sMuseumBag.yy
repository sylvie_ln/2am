{
    "id": "51b6b49f-b3cc-4b72-ae37-0ab66f918032",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMuseumBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0381b13-5dce-430d-83f9-f41dbd5643fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51b6b49f-b3cc-4b72-ae37-0ab66f918032",
            "compositeImage": {
                "id": "5008be95-d7d5-442c-9daa-b7cfc4fcafcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0381b13-5dce-430d-83f9-f41dbd5643fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fce81d41-0ab7-4d62-a4cf-ec30948b1029",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0381b13-5dce-430d-83f9-f41dbd5643fd",
                    "LayerId": "f9bbcd44-633c-40fa-af64-8b349f4baec1"
                },
                {
                    "id": "fd5501c1-a702-4101-869a-fa9192fbd99f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0381b13-5dce-430d-83f9-f41dbd5643fd",
                    "LayerId": "d4b01b9a-6dad-43e9-a20f-f9d9c1a25e20"
                },
                {
                    "id": "1ae6a06b-7b88-4b90-8c52-ffe7300d935f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0381b13-5dce-430d-83f9-f41dbd5643fd",
                    "LayerId": "7f7a1acd-50cf-4c7d-b780-c05fbd22b30e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7f7a1acd-50cf-4c7d-b780-c05fbd22b30e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51b6b49f-b3cc-4b72-ae37-0ab66f918032",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f9bbcd44-633c-40fa-af64-8b349f4baec1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51b6b49f-b3cc-4b72-ae37-0ab66f918032",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d4b01b9a-6dad-43e9-a20f-f9d9c1a25e20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51b6b49f-b3cc-4b72-ae37-0ab66f918032",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 60
}