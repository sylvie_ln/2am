{
    "id": "79b0b3f4-38c5-42f7-886a-dcc968a6a1a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMagicCarrot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f6fa0f9-43ae-457f-a0bd-453e23625b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b0b3f4-38c5-42f7-886a-dcc968a6a1a6",
            "compositeImage": {
                "id": "3bb28222-2c3c-4927-a34f-a07046672936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f6fa0f9-43ae-457f-a0bd-453e23625b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef92b1d-19f1-42ee-9813-ea5e3f69b30f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f6fa0f9-43ae-457f-a0bd-453e23625b6d",
                    "LayerId": "2e265818-5646-469d-b5dc-5048f7c15c5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2e265818-5646-469d-b5dc-5048f7c15c5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79b0b3f4-38c5-42f7-886a-dcc968a6a1a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}