{
    "id": "390ab061-16f4-499d-8819-9ccb81763e68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecraftBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4018546-f734-45eb-a2d2-a7f004200598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "390ab061-16f4-499d-8819-9ccb81763e68",
            "compositeImage": {
                "id": "eb672b5e-c3cc-42c9-88c0-500d5f380486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4018546-f734-45eb-a2d2-a7f004200598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97f77036-a77d-4fe0-8815-f470ef88308c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4018546-f734-45eb-a2d2-a7f004200598",
                    "LayerId": "6d0e2d0f-4b1f-4330-be01-9e1cc31fa87b"
                }
            ]
        },
        {
            "id": "f2c48e7b-2a0f-4014-bca3-173e80a9f4d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "390ab061-16f4-499d-8819-9ccb81763e68",
            "compositeImage": {
                "id": "84f56bce-bef8-4d84-9e92-32679ff539ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c48e7b-2a0f-4014-bca3-173e80a9f4d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61c78a44-71f3-4285-a20e-7ded13fb64e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c48e7b-2a0f-4014-bca3-173e80a9f4d1",
                    "LayerId": "6d0e2d0f-4b1f-4330-be01-9e1cc31fa87b"
                }
            ]
        },
        {
            "id": "84b01d5a-dd21-4a08-af46-07bc3fb124b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "390ab061-16f4-499d-8819-9ccb81763e68",
            "compositeImage": {
                "id": "84343da1-bee8-49be-bc46-dc1d7bf74a08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b01d5a-dd21-4a08-af46-07bc3fb124b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f22bf02-81e3-4dcc-a1ea-7152592e0737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b01d5a-dd21-4a08-af46-07bc3fb124b4",
                    "LayerId": "6d0e2d0f-4b1f-4330-be01-9e1cc31fa87b"
                }
            ]
        },
        {
            "id": "96fc9052-3ee7-4ebf-9d72-0ee1a6c20eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "390ab061-16f4-499d-8819-9ccb81763e68",
            "compositeImage": {
                "id": "9f842188-d0f4-44ee-9d05-d36c0bfc4f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96fc9052-3ee7-4ebf-9d72-0ee1a6c20eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "863ee676-5456-4d4d-b2db-52c7bb796628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96fc9052-3ee7-4ebf-9d72-0ee1a6c20eb3",
                    "LayerId": "6d0e2d0f-4b1f-4330-be01-9e1cc31fa87b"
                }
            ]
        },
        {
            "id": "6362470e-fe6d-4dfa-b584-9aba46a55f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "390ab061-16f4-499d-8819-9ccb81763e68",
            "compositeImage": {
                "id": "0fb8d683-cec0-4e6e-81a0-3294834ce925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6362470e-fe6d-4dfa-b584-9aba46a55f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4566b47-04cc-4a7c-a495-505b1ab87261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6362470e-fe6d-4dfa-b584-9aba46a55f09",
                    "LayerId": "6d0e2d0f-4b1f-4330-be01-9e1cc31fa87b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6d0e2d0f-4b1f-4330-be01-9e1cc31fa87b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "390ab061-16f4-499d-8819-9ccb81763e68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}