{
    "id": "c091373f-6e78-4415-b814-dfb285abd5ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStarSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1bd094d-e7bb-489c-a6af-e69708abb130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c091373f-6e78-4415-b814-dfb285abd5ee",
            "compositeImage": {
                "id": "9995383d-db3d-4fd9-8348-0d1a9e4b1f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1bd094d-e7bb-489c-a6af-e69708abb130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649476eb-6230-4988-9aef-77bb438a2523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1bd094d-e7bb-489c-a6af-e69708abb130",
                    "LayerId": "8c510dd0-0a99-49de-b464-4c88f1fd9086"
                }
            ]
        },
        {
            "id": "25f37a94-d7ab-48db-b7ef-870a0c0b2b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c091373f-6e78-4415-b814-dfb285abd5ee",
            "compositeImage": {
                "id": "56f8ba7a-3b79-4e4e-ba64-fb991f907cad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f37a94-d7ab-48db-b7ef-870a0c0b2b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1b3938d-b26c-40b0-bed3-ff5a0338cdb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f37a94-d7ab-48db-b7ef-870a0c0b2b99",
                    "LayerId": "8c510dd0-0a99-49de-b464-4c88f1fd9086"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8c510dd0-0a99-49de-b464-4c88f1fd9086",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c091373f-6e78-4415-b814-dfb285abd5ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}