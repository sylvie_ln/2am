{
    "id": "66008360-991b-4f1c-a607-a7c8328d6fb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSmallBagInterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 7,
    "bbox_right": 57,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40bde847-db91-45dd-828a-bcfb2446f6be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66008360-991b-4f1c-a607-a7c8328d6fb0",
            "compositeImage": {
                "id": "e0d53df9-e952-43cf-9bb4-dcdabd80b4bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40bde847-db91-45dd-828a-bcfb2446f6be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7543d2c4-abed-40ef-bac8-a97b0127d4d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40bde847-db91-45dd-828a-bcfb2446f6be",
                    "LayerId": "a7e0f176-3f7c-4660-8177-256dde9fb64e"
                },
                {
                    "id": "f8caac57-1593-4246-afd8-84746a567013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40bde847-db91-45dd-828a-bcfb2446f6be",
                    "LayerId": "b5ec91eb-b667-4e11-a20b-a595f449fd55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a7e0f176-3f7c-4660-8177-256dde9fb64e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66008360-991b-4f1c-a607-a7c8328d6fb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "b5ec91eb-b667-4e11-a20b-a595f449fd55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66008360-991b-4f1c-a607-a7c8328d6fb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 40
}