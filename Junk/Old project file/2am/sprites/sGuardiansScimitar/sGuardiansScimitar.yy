{
    "id": "91641331-9363-49b5-bec3-8d6ac40de8f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGuardiansScimitar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 10,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2deff23-e26c-41fe-bc37-85cb091d1b02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91641331-9363-49b5-bec3-8d6ac40de8f8",
            "compositeImage": {
                "id": "5fb2f32b-1f8f-4198-8cad-ca05fd402561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2deff23-e26c-41fe-bc37-85cb091d1b02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d5ee14a-9b5e-49a8-9d5c-a21fd4417367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2deff23-e26c-41fe-bc37-85cb091d1b02",
                    "LayerId": "c8bb339b-b862-4846-9b25-584f1e67d3e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c8bb339b-b862-4846-9b25-584f1e67d3e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91641331-9363-49b5-bec3-8d6ac40de8f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}