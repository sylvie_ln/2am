{
    "id": "0238e1ed-c7bc-4e14-88b0-96f124743c9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCuterWillows",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d163f2d-283d-4c4b-bcb2-b591ca154034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0238e1ed-c7bc-4e14-88b0-96f124743c9e",
            "compositeImage": {
                "id": "82f523a2-80e3-4c35-8340-eb2038c6f547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d163f2d-283d-4c4b-bcb2-b591ca154034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "089f12cb-029b-4889-b68c-686f4302f7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d163f2d-283d-4c4b-bcb2-b591ca154034",
                    "LayerId": "4913b9d8-8bf4-42b3-be5c-c84ce5b83a2d"
                }
            ]
        },
        {
            "id": "b76d700e-42ad-4bad-9699-351c658fbecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0238e1ed-c7bc-4e14-88b0-96f124743c9e",
            "compositeImage": {
                "id": "db149f70-a6af-406c-ae68-b1bdf8959458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b76d700e-42ad-4bad-9699-351c658fbecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb03b089-c17f-452b-9231-669a9a688ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76d700e-42ad-4bad-9699-351c658fbecc",
                    "LayerId": "4913b9d8-8bf4-42b3-be5c-c84ce5b83a2d"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "4913b9d8-8bf4-42b3-be5c-c84ce5b83a2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0238e1ed-c7bc-4e14-88b0-96f124743c9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}