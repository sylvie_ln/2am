{
    "id": "a0e7b95a-eb84-43a7-8374-fb61fd4bf239",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopCavernRockSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86940f9d-05f7-41c0-8e77-2b4c2a4dd2e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e7b95a-eb84-43a7-8374-fb61fd4bf239",
            "compositeImage": {
                "id": "90930a8d-08ed-4ddb-8c1b-a93f7fe4f174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86940f9d-05f7-41c0-8e77-2b4c2a4dd2e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e512469-55bc-4bdb-8fa7-08a1fa3a8374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86940f9d-05f7-41c0-8e77-2b4c2a4dd2e3",
                    "LayerId": "4250fa33-a214-438e-9d8a-198abeed63f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4250fa33-a214-438e-9d8a-198abeed63f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0e7b95a-eb84-43a7-8374-fb61fd4bf239",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}