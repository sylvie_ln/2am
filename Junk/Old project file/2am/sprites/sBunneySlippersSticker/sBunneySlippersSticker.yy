{
    "id": "b93928f1-1ecc-40b5-ac50-1caa560da754",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneySlippersSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a3a1ad9-d14c-4b15-aa5d-7692addddfd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b93928f1-1ecc-40b5-ac50-1caa560da754",
            "compositeImage": {
                "id": "fe4f6f61-1da3-48c5-b99f-11f36837c385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a3a1ad9-d14c-4b15-aa5d-7692addddfd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a39bc5-4d05-4603-8c43-e57eb09ca040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a3a1ad9-d14c-4b15-aa5d-7692addddfd5",
                    "LayerId": "b5d49860-95ca-4044-9bba-ed85403ccbd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b5d49860-95ca-4044-9bba-ed85403ccbd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b93928f1-1ecc-40b5-ac50-1caa560da754",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}