{
    "id": "74fbb19a-8ca9-4cec-ba1f-41aee8d10a42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpringSneakersSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be711240-c966-4631-8962-7584f59b27d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74fbb19a-8ca9-4cec-ba1f-41aee8d10a42",
            "compositeImage": {
                "id": "0700fe0a-a5bb-4270-9737-2555043a890d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be711240-c966-4631-8962-7584f59b27d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a625fc6b-a8d6-429b-aeb3-3c47325d5fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be711240-c966-4631-8962-7584f59b27d3",
                    "LayerId": "e993182e-fb6d-4dd9-8f40-3e6cdb757b96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e993182e-fb6d-4dd9-8f40-3e6cdb757b96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74fbb19a-8ca9-4cec-ba1f-41aee8d10a42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}