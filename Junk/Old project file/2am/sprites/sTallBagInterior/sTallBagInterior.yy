{
    "id": "01734c75-8126-463d-a5d3-4ce293dff989",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTallBagInterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8988f1b-810c-42ff-ac76-d6bb7437d784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01734c75-8126-463d-a5d3-4ce293dff989",
            "compositeImage": {
                "id": "a6a77008-1fd8-40cc-8d9e-64aea5c96a21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8988f1b-810c-42ff-ac76-d6bb7437d784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6679f3-57d9-4d33-a1d1-da937b61c126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8988f1b-810c-42ff-ac76-d6bb7437d784",
                    "LayerId": "de84c2b6-e348-4ee2-9493-debefaf6e109"
                },
                {
                    "id": "c98631b8-ad00-4c17-9e1c-105fe81998c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8988f1b-810c-42ff-ac76-d6bb7437d784",
                    "LayerId": "395aa003-2a7a-4aa1-94b3-4201f90563c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "de84c2b6-e348-4ee2-9493-debefaf6e109",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01734c75-8126-463d-a5d3-4ce293dff989",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "395aa003-2a7a-4aa1-94b3-4201f90563c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01734c75-8126-463d-a5d3-4ce293dff989",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 60
}