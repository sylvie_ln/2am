{
    "id": "3b2d633a-d2b6-443c-ad95-53e2b2a1db83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sZupzel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ff8616d-0cbf-4e60-8bd7-e19e99e5a2a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b2d633a-d2b6-443c-ad95-53e2b2a1db83",
            "compositeImage": {
                "id": "9db06cc5-3de0-41fe-a2c0-b3935539a9bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ff8616d-0cbf-4e60-8bd7-e19e99e5a2a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4e1db43-5fe1-4e9d-978a-d5aa06066a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ff8616d-0cbf-4e60-8bd7-e19e99e5a2a5",
                    "LayerId": "5a64d2cd-b5f2-4922-8c8b-13a9cc470721"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5a64d2cd-b5f2-4922-8c8b-13a9cc470721",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b2d633a-d2b6-443c-ad95-53e2b2a1db83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}