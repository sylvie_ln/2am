{
    "id": "e6656779-9f07-4c1e-9456-b6d2dadf4b54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMineria",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de8964e4-f14a-4543-b222-ccb8b71e7a08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6656779-9f07-4c1e-9456-b6d2dadf4b54",
            "compositeImage": {
                "id": "658daf1e-11a1-4a48-8289-a0f70c848134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de8964e4-f14a-4543-b222-ccb8b71e7a08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d12e335-a05d-4316-9a9b-eb83d63838f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de8964e4-f14a-4543-b222-ccb8b71e7a08",
                    "LayerId": "aa15b053-cdd1-4c10-85ce-4a491d58d01b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "aa15b053-cdd1-4c10-85ce-4a491d58d01b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6656779-9f07-4c1e-9456-b6d2dadf4b54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}