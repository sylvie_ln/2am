{
    "id": "6852f011-fe26-4de5-b606-bc49c1d2bf30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSparkleShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14afd1bc-0328-4a97-82ac-8332887763cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6852f011-fe26-4de5-b606-bc49c1d2bf30",
            "compositeImage": {
                "id": "6e1ea80e-2d85-43a6-b909-63b5c06ee517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14afd1bc-0328-4a97-82ac-8332887763cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f56f5f15-cd2d-4bca-a3d3-9ebc25949ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14afd1bc-0328-4a97-82ac-8332887763cb",
                    "LayerId": "2486c16c-5972-4340-b1e8-de7363c88c15"
                },
                {
                    "id": "7eeb64e1-8429-4b3b-a67e-512db69c1481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14afd1bc-0328-4a97-82ac-8332887763cb",
                    "LayerId": "f55b36b9-0875-4950-9268-4e77da5d9f67"
                },
                {
                    "id": "52a3c033-6b1a-4b7e-847a-267476ca7bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14afd1bc-0328-4a97-82ac-8332887763cb",
                    "LayerId": "ff64bf0e-b716-4c60-b03e-d9d5d7300ba4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2486c16c-5972-4340-b1e8-de7363c88c15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6852f011-fe26-4de5-b606-bc49c1d2bf30",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f55b36b9-0875-4950-9268-4e77da5d9f67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6852f011-fe26-4de5-b606-bc49c1d2bf30",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ff64bf0e-b716-4c60-b03e-d9d5d7300ba4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6852f011-fe26-4de5-b606-bc49c1d2bf30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}