{
    "id": "7f8408b8-4412-481f-af91-7cd8b13ea154",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIllegalUpgradeChipSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d23b04c-627f-4816-818e-cc0f9a8c2d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f8408b8-4412-481f-af91-7cd8b13ea154",
            "compositeImage": {
                "id": "8eb95eed-5658-4206-84b4-12f29e137999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d23b04c-627f-4816-818e-cc0f9a8c2d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ebbddaf-b18f-4ec2-885c-b6e90a56b177",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d23b04c-627f-4816-818e-cc0f9a8c2d78",
                    "LayerId": "d9a455b4-c5e9-454e-9ab6-ee3e37df2670"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d9a455b4-c5e9-454e-9ab6-ee3e37df2670",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f8408b8-4412-481f-af91-7cd8b13ea154",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}