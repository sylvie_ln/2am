{
    "id": "6940b614-12bf-42b8-ab07-15513d525710",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpringShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e011fe0a-61d1-44ab-9f82-3e80908c5bbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6940b614-12bf-42b8-ab07-15513d525710",
            "compositeImage": {
                "id": "ce36b436-cd48-4ee7-9f6a-2f525e83cfb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e011fe0a-61d1-44ab-9f82-3e80908c5bbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb21e093-f726-4952-b000-d808ef7e63e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e011fe0a-61d1-44ab-9f82-3e80908c5bbe",
                    "LayerId": "257057cd-bbdb-47d5-8895-1512d11120bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "257057cd-bbdb-47d5-8895-1512d11120bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6940b614-12bf-42b8-ab07-15513d525710",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}