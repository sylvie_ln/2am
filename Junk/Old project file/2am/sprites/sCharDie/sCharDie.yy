{
    "id": "78130f18-f211-4016-9b14-8936a9924a18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharDie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "641f958c-22cf-4f75-990c-f1b7390d7974",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78130f18-f211-4016-9b14-8936a9924a18",
            "compositeImage": {
                "id": "105827b5-2ec8-49e4-8f32-470ad8f5a966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "641f958c-22cf-4f75-990c-f1b7390d7974",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94228196-936f-4d78-b22d-dc28d2a0a99c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "641f958c-22cf-4f75-990c-f1b7390d7974",
                    "LayerId": "ee3a8733-eec9-4a9d-88d8-d2618d4b8c2b"
                }
            ]
        },
        {
            "id": "bc7ae806-f40f-4def-b527-77d92770583d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78130f18-f211-4016-9b14-8936a9924a18",
            "compositeImage": {
                "id": "ccb49839-7024-4cc7-9379-0915528f7082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc7ae806-f40f-4def-b527-77d92770583d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220ed744-9a06-49da-ab5b-213b16d31efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc7ae806-f40f-4def-b527-77d92770583d",
                    "LayerId": "ee3a8733-eec9-4a9d-88d8-d2618d4b8c2b"
                }
            ]
        },
        {
            "id": "c7dadfbd-b200-45e7-9522-134f7da9c537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78130f18-f211-4016-9b14-8936a9924a18",
            "compositeImage": {
                "id": "13a29256-d5c9-49c9-bb3b-4ea39e64ba0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7dadfbd-b200-45e7-9522-134f7da9c537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7409d93-0816-465d-ab0a-6179fa2ee18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7dadfbd-b200-45e7-9522-134f7da9c537",
                    "LayerId": "ee3a8733-eec9-4a9d-88d8-d2618d4b8c2b"
                }
            ]
        },
        {
            "id": "662b3aee-bfda-443a-b01a-3a4e373338a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78130f18-f211-4016-9b14-8936a9924a18",
            "compositeImage": {
                "id": "7a78b24f-dcbd-452d-97d6-96eadb835a8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "662b3aee-bfda-443a-b01a-3a4e373338a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f34267f4-ae12-4e96-935b-d9c7c53030d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "662b3aee-bfda-443a-b01a-3a4e373338a2",
                    "LayerId": "ee3a8733-eec9-4a9d-88d8-d2618d4b8c2b"
                }
            ]
        },
        {
            "id": "7ac01999-34b1-4786-af55-c774bc4f0adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78130f18-f211-4016-9b14-8936a9924a18",
            "compositeImage": {
                "id": "090d3797-e992-4261-a9a6-9711fa11fb9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ac01999-34b1-4786-af55-c774bc4f0adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56226c4e-f925-4656-b2b0-1260e3a16c1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ac01999-34b1-4786-af55-c774bc4f0adf",
                    "LayerId": "ee3a8733-eec9-4a9d-88d8-d2618d4b8c2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ee3a8733-eec9-4a9d-88d8-d2618d4b8c2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78130f18-f211-4016-9b14-8936a9924a18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}