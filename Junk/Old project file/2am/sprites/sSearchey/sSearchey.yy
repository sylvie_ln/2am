{
    "id": "b0297ccf-110a-4626-ba09-2ddb1a699d08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSearchey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87822af1-b881-45a8-81df-3be36a9f3536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0297ccf-110a-4626-ba09-2ddb1a699d08",
            "compositeImage": {
                "id": "1c6940dd-d1ec-4c4d-b8cc-ab4d9c04ac78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87822af1-b881-45a8-81df-3be36a9f3536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f59e754-e1ec-472d-89a3-ceb9dc07c041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87822af1-b881-45a8-81df-3be36a9f3536",
                    "LayerId": "37a43c93-f67e-4d24-ab35-c9ccc3cb51ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "37a43c93-f67e-4d24-ab35-c9ccc3cb51ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0297ccf-110a-4626-ba09-2ddb1a699d08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}