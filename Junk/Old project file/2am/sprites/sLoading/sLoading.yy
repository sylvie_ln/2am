{
    "id": "f9ef120c-223b-463e-9120-6f239fe8a90b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLoading",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 0,
    "bbox_right": 331,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "583757d1-2874-4b67-92b6-47c4510705c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9ef120c-223b-463e-9120-6f239fe8a90b",
            "compositeImage": {
                "id": "bae989fa-a830-4d79-bbbc-fa1956b9a2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "583757d1-2874-4b67-92b6-47c4510705c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a52cf60f-a94c-4d3d-af1a-b1702b63aecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "583757d1-2874-4b67-92b6-47c4510705c9",
                    "LayerId": "71d71967-75cd-40ba-9987-9118a96767bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "71d71967-75cd-40ba-9987-9118a96767bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9ef120c-223b-463e-9120-6f239fe8a90b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 332,
    "xorig": 24,
    "yorig": 72
}