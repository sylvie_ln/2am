{
    "id": "bab204b6-b0cc-4772-b84c-343751027c24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloudyMixture",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a9221c2-71ef-4729-a2a4-780433939715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bab204b6-b0cc-4772-b84c-343751027c24",
            "compositeImage": {
                "id": "027d103d-fdc2-4d30-8788-86834fa8a376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a9221c2-71ef-4729-a2a4-780433939715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4226804c-6d28-445a-a821-ee6a4219564b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a9221c2-71ef-4729-a2a4-780433939715",
                    "LayerId": "1800f28d-e012-467c-ab26-739aff2e90aa"
                }
            ]
        },
        {
            "id": "7dcf2b6b-8915-4efd-b710-9cfb43047bdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bab204b6-b0cc-4772-b84c-343751027c24",
            "compositeImage": {
                "id": "0d6f3255-b671-4042-b89b-7dc48851cbfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dcf2b6b-8915-4efd-b710-9cfb43047bdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ec5467-cad1-4d5a-a76b-2a56a22ffe08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dcf2b6b-8915-4efd-b710-9cfb43047bdd",
                    "LayerId": "1800f28d-e012-467c-ab26-739aff2e90aa"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "1800f28d-e012-467c-ab26-739aff2e90aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bab204b6-b0cc-4772-b84c-343751027c24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}