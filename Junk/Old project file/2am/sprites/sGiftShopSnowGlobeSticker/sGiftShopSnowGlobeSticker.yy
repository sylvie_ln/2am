{
    "id": "3356ad48-9dc9-42a9-a19a-4460e8a3627f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopSnowGlobeSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a03f5e14-b92f-465d-9e9e-84ff9d356c6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3356ad48-9dc9-42a9-a19a-4460e8a3627f",
            "compositeImage": {
                "id": "5d63e183-958b-4a41-961e-2f711685e053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a03f5e14-b92f-465d-9e9e-84ff9d356c6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad6757f-1400-4e08-bde6-3e9332f17c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a03f5e14-b92f-465d-9e9e-84ff9d356c6d",
                    "LayerId": "6b486a48-bfaf-45d1-8839-ec07f35711c9"
                },
                {
                    "id": "837873b8-c0b9-4434-9425-b9db302797e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a03f5e14-b92f-465d-9e9e-84ff9d356c6d",
                    "LayerId": "3a2060a3-d60e-4a57-b1ba-2ff5befc2783"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6b486a48-bfaf-45d1-8839-ec07f35711c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3356ad48-9dc9-42a9-a19a-4460e8a3627f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3a2060a3-d60e-4a57-b1ba-2ff5befc2783",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3356ad48-9dc9-42a9-a19a-4460e8a3627f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}