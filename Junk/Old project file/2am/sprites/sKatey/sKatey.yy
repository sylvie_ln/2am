{
    "id": "0f94fa59-62f2-4a3f-b94d-10c6fc90daf9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKatey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3f418b2-3266-4cde-a65d-56eefacb6545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f94fa59-62f2-4a3f-b94d-10c6fc90daf9",
            "compositeImage": {
                "id": "b41e8ceb-664f-444a-8e4c-ac0e16e79644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f418b2-3266-4cde-a65d-56eefacb6545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ad1c513-617a-4e64-b780-b099b438bada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f418b2-3266-4cde-a65d-56eefacb6545",
                    "LayerId": "400039ac-e4ea-482b-a40e-692238ea8d11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "400039ac-e4ea-482b-a40e-692238ea8d11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f94fa59-62f2-4a3f-b94d-10c6fc90daf9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}