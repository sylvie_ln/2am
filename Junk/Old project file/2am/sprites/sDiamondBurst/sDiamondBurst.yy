{
    "id": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDiamondBurst",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5f39c27-fb6f-43f5-b496-4ad6643ddf73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
            "compositeImage": {
                "id": "d5c558c1-6670-4bd3-8e59-81d7a5562b90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f39c27-fb6f-43f5-b496-4ad6643ddf73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d50c92-486c-48bc-b086-311c68c4940f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f39c27-fb6f-43f5-b496-4ad6643ddf73",
                    "LayerId": "7c88ac42-3008-463b-9db9-84ef7230fb7b"
                }
            ]
        },
        {
            "id": "c409b1ce-e91e-4315-87ac-cc620e91632c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
            "compositeImage": {
                "id": "1764f635-a025-4a6a-a6f0-5e2cd5d46638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c409b1ce-e91e-4315-87ac-cc620e91632c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51ea3b21-236e-40f7-8ab3-c1d00b0546a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c409b1ce-e91e-4315-87ac-cc620e91632c",
                    "LayerId": "7c88ac42-3008-463b-9db9-84ef7230fb7b"
                }
            ]
        },
        {
            "id": "e9f7cfcb-854d-4cdc-948e-95183e7d973b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
            "compositeImage": {
                "id": "f8768c5e-f7aa-4985-b467-ce76d67dde44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f7cfcb-854d-4cdc-948e-95183e7d973b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dae1c11f-eb95-4e3f-a074-cf400e647051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f7cfcb-854d-4cdc-948e-95183e7d973b",
                    "LayerId": "7c88ac42-3008-463b-9db9-84ef7230fb7b"
                }
            ]
        },
        {
            "id": "d8d5f748-d1c2-49d2-ae6f-d4185e4d7107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
            "compositeImage": {
                "id": "0ac7c0c8-f03a-4e29-b8b3-fde6bedf5460",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8d5f748-d1c2-49d2-ae6f-d4185e4d7107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5bcf9d4-9456-4861-bce8-e7b7664ab412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8d5f748-d1c2-49d2-ae6f-d4185e4d7107",
                    "LayerId": "7c88ac42-3008-463b-9db9-84ef7230fb7b"
                }
            ]
        },
        {
            "id": "7c12835b-8a18-4967-b971-00dec52e76ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
            "compositeImage": {
                "id": "a2e671b8-a1b9-4e2e-999d-cb3b9ca45aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c12835b-8a18-4967-b971-00dec52e76ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d488aa-6a46-42de-bbbf-900bf9914a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c12835b-8a18-4967-b971-00dec52e76ea",
                    "LayerId": "7c88ac42-3008-463b-9db9-84ef7230fb7b"
                }
            ]
        },
        {
            "id": "09ba376c-5f26-4d97-9bba-7a7a92b0ac7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
            "compositeImage": {
                "id": "d1290683-2d37-49e7-8619-a18bc166af48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ba376c-5f26-4d97-9bba-7a7a92b0ac7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "029a2798-36dc-4157-882e-6294f8dfd3f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ba376c-5f26-4d97-9bba-7a7a92b0ac7a",
                    "LayerId": "7c88ac42-3008-463b-9db9-84ef7230fb7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7c88ac42-3008-463b-9db9-84ef7230fb7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}