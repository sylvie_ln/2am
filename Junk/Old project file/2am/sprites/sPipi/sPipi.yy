{
    "id": "b21c7446-b26f-4438-aee3-e5334a9af70e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPipi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03113ed1-8d49-40e5-aabd-60c487f1b6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b21c7446-b26f-4438-aee3-e5334a9af70e",
            "compositeImage": {
                "id": "27ef4a90-f89b-4a30-b4fd-f1d0197cd812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03113ed1-8d49-40e5-aabd-60c487f1b6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70916cb6-6907-4a30-8584-671d5acdb2fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03113ed1-8d49-40e5-aabd-60c487f1b6b5",
                    "LayerId": "df3e2142-ee26-4afe-b016-fb838ddc3a74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "df3e2142-ee26-4afe-b016-fb838ddc3a74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b21c7446-b26f-4438-aee3-e5334a9af70e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}