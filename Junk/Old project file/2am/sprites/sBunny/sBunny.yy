{
    "id": "69446cd2-15df-4a15-afb3-2892b4625b56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "194ffc58-5873-424b-aa2d-59888d582af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69446cd2-15df-4a15-afb3-2892b4625b56",
            "compositeImage": {
                "id": "ee835d0e-a354-4b05-8b68-0ef148e719f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "194ffc58-5873-424b-aa2d-59888d582af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb274bd7-d0aa-4b49-bc1d-00ef19ed3ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "194ffc58-5873-424b-aa2d-59888d582af7",
                    "LayerId": "5c5a1598-caaa-4be4-be00-e47f122f5e83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5c5a1598-caaa-4be4-be00-e47f122f5e83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69446cd2-15df-4a15-afb3-2892b4625b56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}