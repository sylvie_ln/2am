{
    "id": "504eab81-0a48-4143-b497-0de3f12ea76a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTradingTime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 84,
    "bbox_right": 237,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d3d540b-8de2-4552-b065-35b7b38b1b43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "504eab81-0a48-4143-b497-0de3f12ea76a",
            "compositeImage": {
                "id": "bc125d6e-1a8a-4d5a-96f0-35846060ac7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d3d540b-8de2-4552-b065-35b7b38b1b43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb28e745-dc5f-4250-ab5a-6acc9909cbc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d3d540b-8de2-4552-b065-35b7b38b1b43",
                    "LayerId": "665a5ee0-0c9a-434f-bbc7-38a064a40c40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "665a5ee0-0c9a-434f-bbc7-38a064a40c40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "504eab81-0a48-4143-b497-0de3f12ea76a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}