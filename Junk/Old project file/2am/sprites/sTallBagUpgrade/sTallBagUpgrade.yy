{
    "id": "fab58744-dcc6-4c3b-b0b6-8a3e5008aa51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTallBagUpgrade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f210d3b9-2426-4682-bd61-5efffdf978ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fab58744-dcc6-4c3b-b0b6-8a3e5008aa51",
            "compositeImage": {
                "id": "2b836c62-93a8-4d57-9eda-ae5bfff798eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f210d3b9-2426-4682-bd61-5efffdf978ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5ec779-ab3b-44f8-8ab0-7f82775ab626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f210d3b9-2426-4682-bd61-5efffdf978ba",
                    "LayerId": "f76a2948-c898-4741-8c9c-05c8667649ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f76a2948-c898-4741-8c9c-05c8667649ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fab58744-dcc6-4c3b-b0b6-8a3e5008aa51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}