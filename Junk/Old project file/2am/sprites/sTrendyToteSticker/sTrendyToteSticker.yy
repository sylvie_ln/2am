{
    "id": "96f2a0ba-200b-4b18-9379-70b02e7922b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrendyToteSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c3f3138-f187-46e3-86d6-3188e51f533d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f2a0ba-200b-4b18-9379-70b02e7922b3",
            "compositeImage": {
                "id": "84ff8ae5-f450-4266-800d-212f37275a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3f3138-f187-46e3-86d6-3188e51f533d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f114d50-25d8-4db7-bbd9-32efda2cc0bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3f3138-f187-46e3-86d6-3188e51f533d",
                    "LayerId": "875e72d3-03c9-4fbc-969d-9ad6b151c06b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "875e72d3-03c9-4fbc-969d-9ad6b151c06b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96f2a0ba-200b-4b18-9379-70b02e7922b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}