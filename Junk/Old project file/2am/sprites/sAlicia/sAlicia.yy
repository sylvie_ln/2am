{
    "id": "0ced771f-554a-4700-90e8-b3bf4f6d6b83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAlicia",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb210ab7-0604-4346-8ffa-60d827ade62a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ced771f-554a-4700-90e8-b3bf4f6d6b83",
            "compositeImage": {
                "id": "a8ba8a58-b2a2-4d5b-8e4f-9138eb54f48b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb210ab7-0604-4346-8ffa-60d827ade62a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "accc69af-b39e-4319-a2ae-0f4275ec0890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb210ab7-0604-4346-8ffa-60d827ade62a",
                    "LayerId": "f0d960aa-4f33-407a-a44d-ad4e21712b0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f0d960aa-4f33-407a-a44d-ad4e21712b0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ced771f-554a-4700-90e8-b3bf4f6d6b83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}