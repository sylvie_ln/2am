{
    "id": "b0d0345e-2f18-467b-b5c9-f90c4bd4c541",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWorthlessShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d57f4761-4b8e-4442-b1ff-0e3060231065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d0345e-2f18-467b-b5c9-f90c4bd4c541",
            "compositeImage": {
                "id": "8202e708-d729-4e91-8d88-8dbf9f8d7e8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d57f4761-4b8e-4442-b1ff-0e3060231065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de1ef7c8-0fc4-4e2b-8059-24e3eb07d21b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d57f4761-4b8e-4442-b1ff-0e3060231065",
                    "LayerId": "af9072a5-2bf3-4f3f-a0d6-d518789564b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "af9072a5-2bf3-4f3f-a0d6-d518789564b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0d0345e-2f18-467b-b5c9-f90c4bd4c541",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}