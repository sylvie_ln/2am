{
    "id": "d52b2e0b-1748-4752-9379-56be6f9c6036",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnowKeetySticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "648faf82-785b-4c96-a190-7ef8af82327e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d52b2e0b-1748-4752-9379-56be6f9c6036",
            "compositeImage": {
                "id": "351a83a1-7801-4748-827c-efd46ade87ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "648faf82-785b-4c96-a190-7ef8af82327e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "921f817d-0212-4ca7-9ff6-66614e02ed05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648faf82-785b-4c96-a190-7ef8af82327e",
                    "LayerId": "eb38aec7-00b6-4bfc-aed1-880682ea8293"
                },
                {
                    "id": "2538b2d2-915c-4bdc-8088-15080f2e4e4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648faf82-785b-4c96-a190-7ef8af82327e",
                    "LayerId": "fd722447-eb09-4d31-ab1d-0de74aca0b6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "eb38aec7-00b6-4bfc-aed1-880682ea8293",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d52b2e0b-1748-4752-9379-56be6f9c6036",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fd722447-eb09-4d31-ab1d-0de74aca0b6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d52b2e0b-1748-4752-9379-56be6f9c6036",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 18
}