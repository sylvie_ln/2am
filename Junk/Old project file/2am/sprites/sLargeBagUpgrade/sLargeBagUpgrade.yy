{
    "id": "96fa3672-e210-4dd2-b4c2-15114a4d23f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLargeBagUpgrade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85752e7e-105c-4bb8-a074-25e9ba9bb540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96fa3672-e210-4dd2-b4c2-15114a4d23f6",
            "compositeImage": {
                "id": "0ed99199-f156-44c1-b09c-f7e7366f235e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85752e7e-105c-4bb8-a074-25e9ba9bb540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c26af4-bf8b-4f21-915e-b602667109b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85752e7e-105c-4bb8-a074-25e9ba9bb540",
                    "LayerId": "369e94aa-cd87-414b-8468-81e09854df64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "369e94aa-cd87-414b-8468-81e09854df64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96fa3672-e210-4dd2-b4c2-15114a4d23f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}