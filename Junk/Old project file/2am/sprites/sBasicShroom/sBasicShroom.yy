{
    "id": "f4274a99-4689-478d-8874-ee294cf3edaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBasicShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccc92ecf-d272-4f9e-b8ab-899425f9712a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
            "compositeImage": {
                "id": "11c832e0-9c39-4c40-b414-edfbb9876836",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc92ecf-d272-4f9e-b8ab-899425f9712a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db48975-2bda-4133-8f1a-68cfd8f634f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc92ecf-d272-4f9e-b8ab-899425f9712a",
                    "LayerId": "f0d8ad6a-499e-44b0-bee6-6d87f9efc609"
                }
            ]
        },
        {
            "id": "caabe3eb-252f-4e3d-bc14-927dfe190ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
            "compositeImage": {
                "id": "6711b62f-3c59-445e-ab77-8ebbb584cdcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caabe3eb-252f-4e3d-bc14-927dfe190ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3fd4617-cd22-444c-a5df-30338f0798be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caabe3eb-252f-4e3d-bc14-927dfe190ef0",
                    "LayerId": "f0d8ad6a-499e-44b0-bee6-6d87f9efc609"
                }
            ]
        },
        {
            "id": "6345b817-af90-41b7-a981-80d7f92625e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
            "compositeImage": {
                "id": "6df70f49-1de4-4521-a058-f56f816f4f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6345b817-af90-41b7-a981-80d7f92625e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "324da02b-3fc7-41e4-a0d3-c6fcf4850331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6345b817-af90-41b7-a981-80d7f92625e7",
                    "LayerId": "f0d8ad6a-499e-44b0-bee6-6d87f9efc609"
                }
            ]
        },
        {
            "id": "642e11bb-1f97-4ce8-bf90-9fc8a11b5ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
            "compositeImage": {
                "id": "5c4137ea-3d40-4fbd-bc68-d8c3da3c4d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "642e11bb-1f97-4ce8-bf90-9fc8a11b5ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2686f15f-2a87-402b-8c92-c06b714ea954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "642e11bb-1f97-4ce8-bf90-9fc8a11b5ce3",
                    "LayerId": "f0d8ad6a-499e-44b0-bee6-6d87f9efc609"
                }
            ]
        },
        {
            "id": "ce7b0d47-6079-43ec-ae2f-a98d4b7c3c12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
            "compositeImage": {
                "id": "4133ad21-9b79-4377-a67f-d6016041f151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce7b0d47-6079-43ec-ae2f-a98d4b7c3c12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6accd90a-a912-4cce-9e08-5ae7d938cf5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce7b0d47-6079-43ec-ae2f-a98d4b7c3c12",
                    "LayerId": "f0d8ad6a-499e-44b0-bee6-6d87f9efc609"
                }
            ]
        },
        {
            "id": "93b8271c-e914-465e-a799-e760c58087c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
            "compositeImage": {
                "id": "bd49678f-f368-4657-84e2-218005f20499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93b8271c-e914-465e-a799-e760c58087c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59e586a7-e081-45ff-91a2-b972dd821c69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93b8271c-e914-465e-a799-e760c58087c2",
                    "LayerId": "f0d8ad6a-499e-44b0-bee6-6d87f9efc609"
                }
            ]
        },
        {
            "id": "be400ec0-a835-4118-b2a2-caf4c0ab12c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
            "compositeImage": {
                "id": "4e22e1f5-0aa2-48ce-b883-be4394dddf98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be400ec0-a835-4118-b2a2-caf4c0ab12c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2f7e966-ad05-4adc-b790-cfd9e4eb50e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be400ec0-a835-4118-b2a2-caf4c0ab12c8",
                    "LayerId": "f0d8ad6a-499e-44b0-bee6-6d87f9efc609"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 16,
    "layers": [
        {
            "id": "f0d8ad6a-499e-44b0-bee6-6d87f9efc609",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4274a99-4689-478d-8874-ee294cf3edaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901972,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}