{
    "id": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSylvieFontB",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d537b72-0329-497d-b41b-94144c6e5e43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "798ce270-93f2-4098-9867-754264bfd17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d537b72-0329-497d-b41b-94144c6e5e43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9600f32-444f-4564-93d4-e4195485509d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d537b72-0329-497d-b41b-94144c6e5e43",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "db3e3009-2653-4084-87cb-f59ae33064c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "0d78b5b0-b159-449c-afdb-5fbf73105ccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db3e3009-2653-4084-87cb-f59ae33064c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a3701d2-d84c-431a-93ad-46b9883b2fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db3e3009-2653-4084-87cb-f59ae33064c7",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "9dc1a2b2-c61c-40cc-8a1a-3b7789843eb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "1ca31b7c-4620-403e-af48-4e3dadd8803b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc1a2b2-c61c-40cc-8a1a-3b7789843eb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b747edbb-da6e-4fcc-b0f2-75a315dc4e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc1a2b2-c61c-40cc-8a1a-3b7789843eb5",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "90b0201d-92d5-4c70-91e4-9367b5c63f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "c255149b-a0b9-474c-90a6-7e1152ca831a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90b0201d-92d5-4c70-91e4-9367b5c63f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b1a6d2f-abb0-452f-81d0-81ec62307dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90b0201d-92d5-4c70-91e4-9367b5c63f3a",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "c3be863a-e518-4480-9269-5767a7b82109",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "f1220bf0-a5bc-4cb7-9d59-a6291197af70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3be863a-e518-4480-9269-5767a7b82109",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "946d5177-1128-46fe-bc1c-8687a5140fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3be863a-e518-4480-9269-5767a7b82109",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "e8067afd-f42c-41ee-ba2e-dfc8d7fcc002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "5b7917e2-0513-4e56-b083-02a098853b3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8067afd-f42c-41ee-ba2e-dfc8d7fcc002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad1b951-de4c-47a1-8bf1-ca7a7fed0422",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8067afd-f42c-41ee-ba2e-dfc8d7fcc002",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "2f080d8e-4c34-409c-bb0d-853e5b2fa28a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "6764b3a3-21c5-4b9b-b556-83377386006e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f080d8e-4c34-409c-bb0d-853e5b2fa28a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f6e119a-6bac-4976-868e-fd19f687de42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f080d8e-4c34-409c-bb0d-853e5b2fa28a",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "d48574b7-ace1-4f83-b363-c9928e7a4622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "fb487105-d2fd-4b3a-86de-5d4c706bda63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d48574b7-ace1-4f83-b363-c9928e7a4622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4317bfa-6df3-46cb-8391-9ddfc2adf77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d48574b7-ace1-4f83-b363-c9928e7a4622",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "5af57d5e-1cb4-4093-a914-09d9f7a87da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "008ed552-f458-48ad-8b6e-79f702cd6d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5af57d5e-1cb4-4093-a914-09d9f7a87da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fb60880-20e9-42cd-8b1c-88f500395811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5af57d5e-1cb4-4093-a914-09d9f7a87da7",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "6e48501a-3885-402c-8178-360160af929a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "9ddb862f-ef54-480f-a947-ae58f0f27661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e48501a-3885-402c-8178-360160af929a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76d5a8c6-b23d-44f6-a663-94415a6e7a2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e48501a-3885-402c-8178-360160af929a",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "31fd6b30-71a5-4704-b052-c1cce1eafb66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "2ecf818a-eee5-4c68-a154-58218ae921d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31fd6b30-71a5-4704-b052-c1cce1eafb66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96554399-b134-4f75-a277-fe82e08d7b26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31fd6b30-71a5-4704-b052-c1cce1eafb66",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "224fdce6-ab3c-4795-b13d-dd562804e0a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "d9c84ad8-a52b-4018-beb3-522d1de1e6d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "224fdce6-ab3c-4795-b13d-dd562804e0a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37527bb8-ca80-4278-a142-5ca363b52723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "224fdce6-ab3c-4795-b13d-dd562804e0a9",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "31278d21-8783-43a8-b3a8-f9926c042321",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "0f1ec6ae-b389-4834-9e83-507a80b1dd95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31278d21-8783-43a8-b3a8-f9926c042321",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f33d7bf-8749-4528-9f16-c2cce82b3868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31278d21-8783-43a8-b3a8-f9926c042321",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "4c3aa5de-db0c-44c3-a811-412d0f6b769e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "cb5d5649-f3ca-42ba-9b18-afa79beec538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c3aa5de-db0c-44c3-a811-412d0f6b769e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fd90acf-c252-4a1a-8073-6cbe1c089a77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c3aa5de-db0c-44c3-a811-412d0f6b769e",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "d514ffcc-45d4-475d-8866-514f402033b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "6e71ad85-0232-4404-96c5-d63906a3921f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d514ffcc-45d4-475d-8866-514f402033b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e6c798-c96b-4837-9f50-5e22fc436261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d514ffcc-45d4-475d-8866-514f402033b1",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "db758bb1-8103-4d19-8047-2bde87cd5e4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "67d60547-69d3-4a7f-80d8-93345fcd20af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db758bb1-8103-4d19-8047-2bde87cd5e4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4442479e-efe6-48ab-ba6d-eb3281bc821a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db758bb1-8103-4d19-8047-2bde87cd5e4b",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "47b1701a-c8b6-418b-964b-b2cf58c4f2bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "e2f65323-a7a3-42bd-83e8-bdb2eb50cbb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b1701a-c8b6-418b-964b-b2cf58c4f2bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5064fca9-f24d-464e-ac19-82bdef718429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b1701a-c8b6-418b-964b-b2cf58c4f2bd",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "ea7dafa6-a665-4339-8f90-eff0e3bf981f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "0f69fe26-184a-4afd-8d5d-79ee7e8341bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea7dafa6-a665-4339-8f90-eff0e3bf981f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2605490-e936-4ce4-b792-2b696b4cf938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7dafa6-a665-4339-8f90-eff0e3bf981f",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "c2fa4514-1671-4704-ad30-9b7b8f3c7014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "5577c769-46b8-4e32-ad25-2fe4e505044a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2fa4514-1671-4704-ad30-9b7b8f3c7014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b847bf2-4b59-4075-b146-8c1e4982ca1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2fa4514-1671-4704-ad30-9b7b8f3c7014",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "a2bb8366-a005-433b-a74f-a79cf5615ee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "5568581e-6926-425b-826f-e09ace6e80b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2bb8366-a005-433b-a74f-a79cf5615ee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acafbe0a-9369-4385-b854-4c0b50a3d8c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2bb8366-a005-433b-a74f-a79cf5615ee5",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "e7385f44-2d30-43af-a126-8234bd473997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "a20ac8c8-dff3-4d90-896f-dc7a6f6cfb69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7385f44-2d30-43af-a126-8234bd473997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc51da6a-c860-44b7-a8f5-24450f545573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7385f44-2d30-43af-a126-8234bd473997",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "934e04b1-79a7-4a27-bf07-1ffda8824591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "97583481-866c-4417-b44a-b5c3fe0efaf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "934e04b1-79a7-4a27-bf07-1ffda8824591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c61bf7-cf88-423e-b454-fbdaa216ccc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "934e04b1-79a7-4a27-bf07-1ffda8824591",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "c635d3e7-573a-4759-b52e-138d35ad73ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "4aa954e2-9aa8-4ba1-b1db-73025f91741d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c635d3e7-573a-4759-b52e-138d35ad73ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93bb1ad2-2ff7-4421-a899-8e344c517a52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c635d3e7-573a-4759-b52e-138d35ad73ad",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "811f1aae-7aba-4ccc-9aeb-cfbe50d39d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "28defb45-a907-4c73-9593-77a051f96267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811f1aae-7aba-4ccc-9aeb-cfbe50d39d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdb695c7-589b-48e3-b40c-0e92740aedfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811f1aae-7aba-4ccc-9aeb-cfbe50d39d2e",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "79a426fd-622c-42f0-93ee-5119233d14c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "ad43d6f4-134a-4cb7-9a72-b19f56af1423",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79a426fd-622c-42f0-93ee-5119233d14c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e54a45f-96de-4619-8d51-c8b7420f61ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79a426fd-622c-42f0-93ee-5119233d14c2",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "f98a8ab4-a037-4ebc-97db-7c9e7c2897c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "924ea725-785c-455e-8492-c894b910e7e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98a8ab4-a037-4ebc-97db-7c9e7c2897c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5e66ee0-6674-48ee-86c4-c31ea7e55ac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98a8ab4-a037-4ebc-97db-7c9e7c2897c0",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "c9eeaa59-1b91-480c-b6d7-4c05faae7fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "8a5e3df3-aaa8-46f1-a94a-b26edd24f2c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9eeaa59-1b91-480c-b6d7-4c05faae7fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c626229-1851-4ac7-ba02-1575805938df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9eeaa59-1b91-480c-b6d7-4c05faae7fe1",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "4094c695-042f-4636-827d-646d22b82e32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "dd4d1035-dc42-45c2-924d-f9f352929e6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4094c695-042f-4636-827d-646d22b82e32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbfaec57-1f92-4d94-a752-5edd15e2d5a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4094c695-042f-4636-827d-646d22b82e32",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "53a9bba9-1c4d-45db-9460-5dbaed2b8152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "bcb3f4c2-c954-4697-847a-4df3d08c370e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53a9bba9-1c4d-45db-9460-5dbaed2b8152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2350b63-1554-4989-a7ed-d3b1a0e4b68f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53a9bba9-1c4d-45db-9460-5dbaed2b8152",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "60cbebf5-9fcb-409a-897c-8677c1d5fca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "0fc451ed-08c2-4690-9e1d-e15e44605bb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60cbebf5-9fcb-409a-897c-8677c1d5fca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5e473c-b113-49ce-b555-0817d43ebd3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60cbebf5-9fcb-409a-897c-8677c1d5fca6",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "a2fea53a-e6cb-448c-9953-992eb18e02c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "9324aa93-9d76-493b-9f0c-b07de3d1fea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2fea53a-e6cb-448c-9953-992eb18e02c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db1d9c3e-93be-4bd6-907e-c5a003b2deaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2fea53a-e6cb-448c-9953-992eb18e02c4",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "df3cbeb9-f295-47ab-8170-72fd284d25f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "fb83c041-839b-4427-86f3-59bd539e3ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df3cbeb9-f295-47ab-8170-72fd284d25f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58657857-7ded-4bcf-8242-c24e768930cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df3cbeb9-f295-47ab-8170-72fd284d25f7",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "f80ad168-4df9-479c-9422-e863ab5f9c62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "15c1226f-944c-4d64-a590-e783acd398f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f80ad168-4df9-479c-9422-e863ab5f9c62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b0a2762-7f35-478b-8778-d58fecbce3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f80ad168-4df9-479c-9422-e863ab5f9c62",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "d4f65901-d14a-4cca-9dce-1da1b69a25f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "1a75f271-ca61-4ee4-84fc-7081a00c245f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4f65901-d14a-4cca-9dce-1da1b69a25f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0eb18e6-7ef6-46b9-991b-15ce8cb775cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4f65901-d14a-4cca-9dce-1da1b69a25f6",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "7cc617b1-cc38-45cc-a714-b6e07d03eb76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "7e1e4f6d-beba-4344-93f6-5c1ce891544d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc617b1-cc38-45cc-a714-b6e07d03eb76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b9537ef-a830-4689-a50a-f5f84d02c5ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc617b1-cc38-45cc-a714-b6e07d03eb76",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "0021c6a2-9de4-45cd-b986-f9290077c90d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "ae6c0591-0b48-427b-8630-3a2cf2157e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0021c6a2-9de4-45cd-b986-f9290077c90d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fe15413-cfc0-4991-aad3-a0cb255c4e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0021c6a2-9de4-45cd-b986-f9290077c90d",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "dba04bbe-d868-42e6-8dc2-2be3beed60aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "1abe6bde-1809-4598-a89d-6a6590bc1b32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dba04bbe-d868-42e6-8dc2-2be3beed60aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d8411d-f3b1-4de7-989a-482cfb607afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dba04bbe-d868-42e6-8dc2-2be3beed60aa",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "705f435c-d31f-403a-b44f-8d81adc2fb4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "93308576-f9ed-44d3-b3c5-f4b5bf8a1c9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "705f435c-d31f-403a-b44f-8d81adc2fb4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd31bcd3-d4de-4858-8d23-ec68322345da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "705f435c-d31f-403a-b44f-8d81adc2fb4d",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "5f3658f7-2c2a-4969-8136-5dbb0ddf1026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "60db2d28-2fcd-44a6-939d-d9a37864bf18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f3658f7-2c2a-4969-8136-5dbb0ddf1026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4f11598-cdf4-4367-949b-1de1be158228",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f3658f7-2c2a-4969-8136-5dbb0ddf1026",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "7f4987f5-151c-42e2-a19b-b47d5d17cd8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "bc04d90b-f607-418d-918c-1007a98826a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4987f5-151c-42e2-a19b-b47d5d17cd8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b2bcf3-9930-4bc9-89a4-3723fdd7f878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4987f5-151c-42e2-a19b-b47d5d17cd8f",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "b57fad74-9624-4f9b-b03d-211746706a74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "a4162a51-2668-4e20-aad6-c696cac248cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b57fad74-9624-4f9b-b03d-211746706a74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92bb701f-c033-471c-b3a1-7025c96225b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b57fad74-9624-4f9b-b03d-211746706a74",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "117f9858-83a4-4438-a0d5-999e2226b17f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "2a870e2b-950c-49f6-a3a0-a93ae907086c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "117f9858-83a4-4438-a0d5-999e2226b17f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3211618a-7ba6-4494-9511-23dc9982245a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "117f9858-83a4-4438-a0d5-999e2226b17f",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "9dcc9656-bd5c-4da4-9d2e-edaa76a01815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "d22b424b-0a78-454f-8be4-2c7e45069cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dcc9656-bd5c-4da4-9d2e-edaa76a01815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5486376-3735-4b26-8d51-92485008de13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dcc9656-bd5c-4da4-9d2e-edaa76a01815",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "8c5650a9-e4e5-43a8-bbb5-8a1236433c2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "28fe3734-241f-4b38-835b-ee76f5e40eb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c5650a9-e4e5-43a8-bbb5-8a1236433c2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f2dc53c-3e7f-465c-8e58-1886233d00ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c5650a9-e4e5-43a8-bbb5-8a1236433c2b",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "18f91194-5abc-4eee-b4ec-eb0bb26e3a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "735d945a-f2af-4645-9cae-105068670548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f91194-5abc-4eee-b4ec-eb0bb26e3a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b93fe6a-e8df-43ed-b721-6287a9666bb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f91194-5abc-4eee-b4ec-eb0bb26e3a80",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "3d645e95-030a-458b-a30c-7fe315257170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "af119473-25d9-4243-b10f-ed5c36335361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d645e95-030a-458b-a30c-7fe315257170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d3a4731-0b72-46dc-a94b-ea2a63c4e257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d645e95-030a-458b-a30c-7fe315257170",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "05542c9b-5ac9-4156-a5db-42b39afe42d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "584a4939-3019-4640-901b-33c4a18dcd7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05542c9b-5ac9-4156-a5db-42b39afe42d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb5d622-8aee-404d-870a-f0907a723177",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05542c9b-5ac9-4156-a5db-42b39afe42d4",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "8237c162-7033-480a-a194-08d218aa27bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "b8176099-264c-46db-aee3-a4e8558d2b4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8237c162-7033-480a-a194-08d218aa27bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c48b59c-07d9-4608-b762-f5068f2888a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8237c162-7033-480a-a194-08d218aa27bd",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "667f9509-7c6b-47ff-a383-7489ccafe4bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "806c2239-0c0a-4461-a193-ca05f93661be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "667f9509-7c6b-47ff-a383-7489ccafe4bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48aca119-34df-4f58-8e05-d2d75a111d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "667f9509-7c6b-47ff-a383-7489ccafe4bb",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "3b871aba-d600-4b06-9e87-26c568ab1699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "743a8053-8d21-48d1-ba0b-928327c1bcde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b871aba-d600-4b06-9e87-26c568ab1699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c09bf82-4737-4a40-bf0c-8a3aeeb1d889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b871aba-d600-4b06-9e87-26c568ab1699",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "173edeaf-2962-44a4-be3b-209b1c846646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "be856ab9-e1af-4b8f-a8b9-ff75669e0980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "173edeaf-2962-44a4-be3b-209b1c846646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09c92cd0-77bf-443b-ad5e-2fe0754260fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "173edeaf-2962-44a4-be3b-209b1c846646",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "b672a1e6-71d6-4bee-8876-6f8a6e8d21a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "e1cc8fa6-2158-4f7f-b790-d87134477bc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b672a1e6-71d6-4bee-8876-6f8a6e8d21a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9bc94a3-3f4d-4b1f-bd6c-2c9a651a23ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b672a1e6-71d6-4bee-8876-6f8a6e8d21a6",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "cd8e4021-7e6e-44ea-b1fe-4134cfdc2cd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "7eed2d85-ee76-4318-a48e-2a499aee3c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd8e4021-7e6e-44ea-b1fe-4134cfdc2cd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc314755-0cdd-4213-b66e-d0f34919a18d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd8e4021-7e6e-44ea-b1fe-4134cfdc2cd7",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "d253fd58-b186-4fe7-af4c-8b56556b5de0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "bf4cbce4-e593-43ac-977b-5aff555fcf83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d253fd58-b186-4fe7-af4c-8b56556b5de0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0db9ef-b7bb-43f0-8155-70db9b943ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d253fd58-b186-4fe7-af4c-8b56556b5de0",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "2b07b839-be88-4c2d-a9ef-8ddc300635ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "328645a3-8fb4-41f7-93dd-fa3683d02888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b07b839-be88-4c2d-a9ef-8ddc300635ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8362093f-1f6c-4915-bb45-81981ef2c9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b07b839-be88-4c2d-a9ef-8ddc300635ec",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "76be6686-891b-4d48-a47f-95bffae3a158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "4f80062c-538d-4a9c-9d3e-dfedc6238445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76be6686-891b-4d48-a47f-95bffae3a158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "859aba00-e623-4e29-b240-cdf869b0af8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76be6686-891b-4d48-a47f-95bffae3a158",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "c716d373-9ac9-4e35-8d3c-d2faa02d358a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "97f4e91b-0d6a-42a0-aec8-8b635e31aadb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c716d373-9ac9-4e35-8d3c-d2faa02d358a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3edd78f1-2dd8-4511-94c4-ca7a8176afa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c716d373-9ac9-4e35-8d3c-d2faa02d358a",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "672ce1da-ed5e-4606-93eb-a7f511fd74f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "b9746af5-0890-46f2-9c89-ef2edb70bd1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "672ce1da-ed5e-4606-93eb-a7f511fd74f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ccdea6-4dad-4edf-a10e-82bbf2282dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "672ce1da-ed5e-4606-93eb-a7f511fd74f0",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "941b1752-db36-4cb1-a603-b2284efa9c57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "ec386291-0cb5-443f-885e-e44efd32deb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "941b1752-db36-4cb1-a603-b2284efa9c57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e692234a-b723-46d9-a64b-0ee21334a325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "941b1752-db36-4cb1-a603-b2284efa9c57",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "818d5444-fe75-47b1-9cd6-a6fbb18b3309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "2459d511-dea9-4838-b856-c01a8ab798e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "818d5444-fe75-47b1-9cd6-a6fbb18b3309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "903a2e35-fb01-4963-b581-b4bb59d36ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "818d5444-fe75-47b1-9cd6-a6fbb18b3309",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "729b8fa5-9ea4-40db-8338-e0d9a5b62228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "a3c6d037-83e1-4526-a5fa-c3aa1a7417bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "729b8fa5-9ea4-40db-8338-e0d9a5b62228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "582eecd6-2f4b-44c5-8ab2-045b4f21f88a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "729b8fa5-9ea4-40db-8338-e0d9a5b62228",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "518ef1ad-fd38-49ac-8e7b-8bd77ff146be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "ee26f151-0888-4d91-b672-cab16dda34fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "518ef1ad-fd38-49ac-8e7b-8bd77ff146be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90377b86-3694-40f5-b6f3-3820df290064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "518ef1ad-fd38-49ac-8e7b-8bd77ff146be",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "c6e2ae5e-b38d-48e8-b2fe-4ff2649672ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "d92d6da3-1640-42a3-b5f3-c6cf2d838f94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e2ae5e-b38d-48e8-b2fe-4ff2649672ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc36740f-5049-452d-b6ce-040d04f9709e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e2ae5e-b38d-48e8-b2fe-4ff2649672ab",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "ac8f940a-1320-4eb9-bfd7-c3dd749903b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "61a1ff8c-e42d-4fa7-b2e7-ac7ed1e692d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8f940a-1320-4eb9-bfd7-c3dd749903b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fdcbf30-1c92-477d-8897-9e8b57575186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8f940a-1320-4eb9-bfd7-c3dd749903b4",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "39cdd8b3-c908-4bac-a8eb-3c58f7c12271",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "70b4c8bf-c2aa-4c42-9ff9-f306cbb73565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39cdd8b3-c908-4bac-a8eb-3c58f7c12271",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e41b64-560f-4ba4-a569-bb1a99cffaca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39cdd8b3-c908-4bac-a8eb-3c58f7c12271",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "6089d738-d64a-4f9f-a63b-a883e67dd5e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "637cda89-23b6-459f-9b82-f58b1b542a05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6089d738-d64a-4f9f-a63b-a883e67dd5e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7dbb6e-95d3-4003-a5d2-06d47f18de39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6089d738-d64a-4f9f-a63b-a883e67dd5e8",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "8f617d71-cc66-4426-8298-03317f5dbe5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "38c6b651-ffa9-403f-ac65-c7c90e592ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f617d71-cc66-4426-8298-03317f5dbe5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8d7721-a93b-4b13-8ce5-f7543cedc495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f617d71-cc66-4426-8298-03317f5dbe5f",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "bdb3035b-7385-44d2-b7b2-03e1ace06e64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "8082518c-3e2c-42c0-8ed0-5b9336f3a7d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdb3035b-7385-44d2-b7b2-03e1ace06e64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ccf5e8d-e236-4491-b681-53daab461764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdb3035b-7385-44d2-b7b2-03e1ace06e64",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "8c6eaff1-4902-4e1a-b1fd-296822916e2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "05980bea-53ab-4d21-9a1e-5e28f2cddda8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c6eaff1-4902-4e1a-b1fd-296822916e2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0305cb9-cd07-41d2-b043-68fb2bffef18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c6eaff1-4902-4e1a-b1fd-296822916e2a",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "fa9aba7f-1fb0-421c-b0c8-8cceec7c8c0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "da2652aa-bae2-4560-ae17-3b6f329d58ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa9aba7f-1fb0-421c-b0c8-8cceec7c8c0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05ece187-a4a6-4144-9856-3c7473a0f3ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa9aba7f-1fb0-421c-b0c8-8cceec7c8c0e",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "aa74aefd-2863-4773-bef4-8020fefa2eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "17d34fd5-1610-471e-bb15-de67c6b391fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa74aefd-2863-4773-bef4-8020fefa2eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354644a4-cd65-4b25-920b-8668e758fbc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa74aefd-2863-4773-bef4-8020fefa2eaf",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "7651a44d-55d7-45f2-b574-8abeac35cd6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "030f673a-2fe1-4e7a-94a8-75b4439f87e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7651a44d-55d7-45f2-b574-8abeac35cd6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "819f9237-50c6-4f53-8c48-074a21f75254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7651a44d-55d7-45f2-b574-8abeac35cd6f",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "1a4ecc6d-3a52-4c26-aeae-a47e99bda027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "5a16d050-d68b-45eb-8954-075dda882534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4ecc6d-3a52-4c26-aeae-a47e99bda027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9bf0767-e3e6-4f7a-aaf2-a62c523117a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4ecc6d-3a52-4c26-aeae-a47e99bda027",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "203a44b2-fcc9-4163-a593-0994bb285883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "f28058e8-f69c-49d0-a71f-2def7d2fa252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "203a44b2-fcc9-4163-a593-0994bb285883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee95f089-3445-4cfb-87d7-b598bd8edff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "203a44b2-fcc9-4163-a593-0994bb285883",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "8debda89-448f-428e-8567-7e9e7ad605a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "c85fb17c-7f14-47fe-acd9-e0d3fdec61bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8debda89-448f-428e-8567-7e9e7ad605a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a31fa20-5607-4fec-bb9d-f4659aded6d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8debda89-448f-428e-8567-7e9e7ad605a4",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "b53dabeb-d1d5-44e0-a6bd-905a596aa332",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "c8febebc-c99c-4416-bea5-f2e7ca38f6a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b53dabeb-d1d5-44e0-a6bd-905a596aa332",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1972c0c0-7136-47c6-9958-8bb06d750e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b53dabeb-d1d5-44e0-a6bd-905a596aa332",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "ed1e3073-1be4-49d9-bf61-851f24e2e0db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "73dda7c7-e59e-46da-9f49-c5d9b9ac3033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed1e3073-1be4-49d9-bf61-851f24e2e0db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "172b976b-6af5-4834-a312-7f89a0d74527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed1e3073-1be4-49d9-bf61-851f24e2e0db",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "132e66c6-4dde-496a-bcf4-5d32dfd69f69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "9f119479-08ef-480b-a968-e37238f17309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132e66c6-4dde-496a-bcf4-5d32dfd69f69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "578db4d7-0f7d-4d51-9cce-f01dd83ed7a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132e66c6-4dde-496a-bcf4-5d32dfd69f69",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "cc49d2de-c321-4a27-b5d9-a318c60d3860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "a2e539cb-b71b-4b16-9222-33ed25f76d04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc49d2de-c321-4a27-b5d9-a318c60d3860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59ad6a3-e704-4ebc-8e02-e2f733ace375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc49d2de-c321-4a27-b5d9-a318c60d3860",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "1b5cbd1c-bc48-4eff-9686-e869b9cf0fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "8cfba219-ddba-4440-a8cd-a651ec385fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b5cbd1c-bc48-4eff-9686-e869b9cf0fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45d91634-2950-4e72-a1cc-6d3e57ef9efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b5cbd1c-bc48-4eff-9686-e869b9cf0fcf",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "f770bc93-01dd-41b8-aee2-4976bd9e334e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "9ef50dba-b190-431c-a64b-26b5c21c0dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f770bc93-01dd-41b8-aee2-4976bd9e334e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd60d5d-e3ba-4848-b821-1002e4fb7034",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f770bc93-01dd-41b8-aee2-4976bd9e334e",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "98c10865-12c0-453a-bad4-e43dcde5dabb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "8e20fb57-6b25-4089-921a-00d7812d9fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c10865-12c0-453a-bad4-e43dcde5dabb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc5a7e86-a53f-41e7-885e-f46a8cef496f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c10865-12c0-453a-bad4-e43dcde5dabb",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "f46a6f9a-eb4b-4e57-9731-31986c397e3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "be1882fc-4b04-4b38-af81-8297a345abb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f46a6f9a-eb4b-4e57-9731-31986c397e3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de5da81-a936-4258-a256-1a0053b983c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f46a6f9a-eb4b-4e57-9731-31986c397e3a",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "244f1560-94c4-48b3-8393-6646ba52baa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "07ff2dd8-767b-4502-8f73-032a7cd58414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244f1560-94c4-48b3-8393-6646ba52baa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "716429a3-f94e-4e21-b913-fe2966e74f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244f1560-94c4-48b3-8393-6646ba52baa5",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "1d2d743e-180a-4c3c-9052-01f78b1527d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "e3add114-080f-43e4-ab3e-e1ce29f77236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d2d743e-180a-4c3c-9052-01f78b1527d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81d01734-1be5-4f2d-9ade-d22c7151ab86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d2d743e-180a-4c3c-9052-01f78b1527d7",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "ceb08f97-abb6-4fb4-8e2f-7bc5f1e99881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "c9a3f116-5f96-4753-b19b-2d739b2b1113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceb08f97-abb6-4fb4-8e2f-7bc5f1e99881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b1a6ba6-5819-44ef-a0c4-ef7392524698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceb08f97-abb6-4fb4-8e2f-7bc5f1e99881",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "e71541a7-9b95-4e92-9996-ef5f0815400a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "30089144-2e68-499b-83e9-42bee424da8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e71541a7-9b95-4e92-9996-ef5f0815400a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493acf6a-1f84-4978-b4f4-d302e31fc879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e71541a7-9b95-4e92-9996-ef5f0815400a",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "905cd2c3-4401-421a-8e20-c72a84252767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "df36eade-b9c0-4e4d-89d6-268f807ffd65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "905cd2c3-4401-421a-8e20-c72a84252767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6e7721-dcc7-464e-ab8f-d37c25c7f5ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "905cd2c3-4401-421a-8e20-c72a84252767",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "e90ffec4-8d9d-4e04-b8de-9791943f18ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "665599eb-8680-4746-ab0e-094ab3f11480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e90ffec4-8d9d-4e04-b8de-9791943f18ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5daddb31-e5d3-45c5-941c-9cc9081b77d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e90ffec4-8d9d-4e04-b8de-9791943f18ef",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "4d82d2c7-1352-4ff6-bf82-fe8613b8f5d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "12f96372-f657-48d8-98d0-78c096d46e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d82d2c7-1352-4ff6-bf82-fe8613b8f5d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6bcf5e3-f238-4dfe-9c97-a0daebd300e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d82d2c7-1352-4ff6-bf82-fe8613b8f5d8",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "b1448645-e74a-4180-a36c-d4cbe9e4e742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "e7b0e9a1-24a2-4085-a3e8-2bedb9ecf73e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1448645-e74a-4180-a36c-d4cbe9e4e742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f602bb-adde-4a00-8afa-78af43c8ae4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1448645-e74a-4180-a36c-d4cbe9e4e742",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "7e10a92a-805f-455f-9380-b93ec33ef9fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "c1bba911-35b8-4663-9427-b02358c80589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e10a92a-805f-455f-9380-b93ec33ef9fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8759e1c3-8fdc-4a2d-b23f-b27726fcf704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e10a92a-805f-455f-9380-b93ec33ef9fd",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "6388390f-da9f-4c5d-957d-3d1ae52024d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "4a76bdde-2c1d-4f98-9969-4c7e86211121",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6388390f-da9f-4c5d-957d-3d1ae52024d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b93fd60f-ebf0-43e0-a0b8-b94372c9d3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6388390f-da9f-4c5d-957d-3d1ae52024d9",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "55861728-1fb9-41a4-be07-82f5fb1ef508",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "57b3ff24-e1e1-4d42-9b2e-de09b497137a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55861728-1fb9-41a4-be07-82f5fb1ef508",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a526baec-4848-419c-969c-82f07c87de5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55861728-1fb9-41a4-be07-82f5fb1ef508",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "e336c6de-ed36-4914-bf80-4844ce018fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "f1e4623f-9500-49de-85ad-b1cc165df328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e336c6de-ed36-4914-bf80-4844ce018fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c8ca80-2d48-4d34-b220-26ff876db0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e336c6de-ed36-4914-bf80-4844ce018fa6",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "5bc9795e-2e14-4544-8639-64d275b95e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "b4b0984b-dfff-4050-a426-5d78fc0cbb3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bc9795e-2e14-4544-8639-64d275b95e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd783b6-1fbf-4168-9508-f14a210670be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bc9795e-2e14-4544-8639-64d275b95e0d",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "8f42b996-4d4c-49db-b5d6-97f74e25d417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "5aa2c65c-f44b-4f68-b5ac-9ad83be631db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f42b996-4d4c-49db-b5d6-97f74e25d417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450d7de5-67e5-47b2-a536-2baa61c28168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f42b996-4d4c-49db-b5d6-97f74e25d417",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "2b285c7a-6493-4efd-8ca6-6af01ca63bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "5710b82e-8f34-4667-a4b3-1f72af9c6a01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b285c7a-6493-4efd-8ca6-6af01ca63bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "791fa58e-1c70-4479-b051-e624f665ef6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b285c7a-6493-4efd-8ca6-6af01ca63bcf",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        },
        {
            "id": "5464c48e-0299-49c7-b30d-f496e477fa06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "compositeImage": {
                "id": "b1fd1856-7b16-4192-9981-89876e0008c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5464c48e-0299-49c7-b30d-f496e477fa06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2f73ec-f9d6-4e7d-aa98-32716ef0ce6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5464c48e-0299-49c7-b30d-f496e477fa06",
                    "LayerId": "baccfc8d-5f14-48cc-8081-d5497b2acb38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "baccfc8d-5f14-48cc-8081-d5497b2acb38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "778c7dcb-3e7c-45ea-ba4a-6f99e3cd827f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}