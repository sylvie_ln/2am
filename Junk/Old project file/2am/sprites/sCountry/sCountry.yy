{
    "id": "2af5d893-f8e4-4bcd-a675-d5711d68318a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCountry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56bf4058-a76a-4e9b-b1b9-d51d66b6700d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2af5d893-f8e4-4bcd-a675-d5711d68318a",
            "compositeImage": {
                "id": "898ff81c-4c6c-42b4-87af-d10dc388396b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56bf4058-a76a-4e9b-b1b9-d51d66b6700d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae46ade8-2129-4c21-8ba3-b036076a441b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56bf4058-a76a-4e9b-b1b9-d51d66b6700d",
                    "LayerId": "db996d01-7f99-4027-9c62-2f3540b73998"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "db996d01-7f99-4027-9c62-2f3540b73998",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2af5d893-f8e4-4bcd-a675-d5711d68318a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}