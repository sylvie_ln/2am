{
    "id": "66ea71ef-6745-4649-aa26-bef5a6a0e078",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlackRoseMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a415ee61-3d4d-482b-b36e-e980ac159b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ea71ef-6745-4649-aa26-bef5a6a0e078",
            "compositeImage": {
                "id": "e942b67a-6876-48ca-866e-fc5d25eda038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a415ee61-3d4d-482b-b36e-e980ac159b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c5b2b6f-7a49-4ee7-9fad-342f61ec62b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a415ee61-3d4d-482b-b36e-e980ac159b78",
                    "LayerId": "74e94e12-4699-4530-a900-1d85e2dd5ede"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "74e94e12-4699-4530-a900-1d85e2dd5ede",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66ea71ef-6745-4649-aa26-bef5a6a0e078",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}