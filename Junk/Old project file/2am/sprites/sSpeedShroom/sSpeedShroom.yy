{
    "id": "6faa11b5-b733-40ca-94f8-3118341da6f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpeedShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2837f8c3-9a47-4f55-90ea-178c505fc166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa11b5-b733-40ca-94f8-3118341da6f8",
            "compositeImage": {
                "id": "6e153137-2c3e-4ef5-a7c9-f45ec85d7f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2837f8c3-9a47-4f55-90ea-178c505fc166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f165f534-b54b-4e3e-a62e-2ed90047e54c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2837f8c3-9a47-4f55-90ea-178c505fc166",
                    "LayerId": "5d975b24-9a2e-4a52-8734-e806a6e89b62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5d975b24-9a2e-4a52-8734-e806a6e89b62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6faa11b5-b733-40ca-94f8-3118341da6f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}