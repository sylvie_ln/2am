{
    "id": "b6b176ab-cc61-4c5b-98fc-86b02ba92c1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMinecraft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4d94ee2-e94c-4221-a73c-cd720dceecbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6b176ab-cc61-4c5b-98fc-86b02ba92c1a",
            "compositeImage": {
                "id": "c3380b57-a288-4b16-a3e9-358affef6e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4d94ee2-e94c-4221-a73c-cd720dceecbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c020f2-7ce7-4a4f-9d99-fe4ec1d41def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4d94ee2-e94c-4221-a73c-cd720dceecbb",
                    "LayerId": "3183d449-bdc9-4606-b292-e57378955c5c"
                }
            ]
        },
        {
            "id": "47b7b9a4-3dda-42fb-9f63-99cb81c60967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6b176ab-cc61-4c5b-98fc-86b02ba92c1a",
            "compositeImage": {
                "id": "3705cdf6-1053-44a9-9bd8-4ba6526cb8b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b7b9a4-3dda-42fb-9f63-99cb81c60967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93a65e62-72ae-4521-81cf-a1a31cba43bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b7b9a4-3dda-42fb-9f63-99cb81c60967",
                    "LayerId": "3183d449-bdc9-4606-b292-e57378955c5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "3183d449-bdc9-4606-b292-e57378955c5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6b176ab-cc61-4c5b-98fc-86b02ba92c1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 4
}