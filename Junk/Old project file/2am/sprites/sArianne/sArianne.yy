{
    "id": "877e5d87-347e-4e29-b93e-dfee1f52bf88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sArianne",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "630ea447-8a65-453e-bea0-1e636da249ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "877e5d87-347e-4e29-b93e-dfee1f52bf88",
            "compositeImage": {
                "id": "d04da220-c478-4c96-8a65-a7f5074aae10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "630ea447-8a65-453e-bea0-1e636da249ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93d995d-875b-404f-b710-fc12b287befc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "630ea447-8a65-453e-bea0-1e636da249ee",
                    "LayerId": "9b4966ad-4b11-4d66-a795-7982c9d94882"
                }
            ]
        },
        {
            "id": "1a81aa5a-b310-4c87-b8f8-7b10ef228761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "877e5d87-347e-4e29-b93e-dfee1f52bf88",
            "compositeImage": {
                "id": "2f20378f-d173-4c31-a2e4-28ea414200bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a81aa5a-b310-4c87-b8f8-7b10ef228761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33df2a7e-bc12-472d-8250-b18e691bde95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a81aa5a-b310-4c87-b8f8-7b10ef228761",
                    "LayerId": "9b4966ad-4b11-4d66-a795-7982c9d94882"
                }
            ]
        },
        {
            "id": "6d7e781d-be72-4e76-a02f-4596692e603e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "877e5d87-347e-4e29-b93e-dfee1f52bf88",
            "compositeImage": {
                "id": "c2f7b697-7c31-4200-90ab-78aa79d69d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d7e781d-be72-4e76-a02f-4596692e603e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2980e67a-8e72-490b-a7cd-a1200c6aecb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d7e781d-be72-4e76-a02f-4596692e603e",
                    "LayerId": "9b4966ad-4b11-4d66-a795-7982c9d94882"
                }
            ]
        },
        {
            "id": "00db4af8-4a08-4667-8473-a72aa36a3e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "877e5d87-347e-4e29-b93e-dfee1f52bf88",
            "compositeImage": {
                "id": "f819896a-e027-4000-82c8-a36f8c28371b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00db4af8-4a08-4667-8473-a72aa36a3e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7908db8c-5b43-4a05-a587-7d7edfd75d75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00db4af8-4a08-4667-8473-a72aa36a3e9e",
                    "LayerId": "9b4966ad-4b11-4d66-a795-7982c9d94882"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9b4966ad-4b11-4d66-a795-7982c9d94882",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "877e5d87-347e-4e29-b93e-dfee1f52bf88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}