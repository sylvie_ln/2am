{
    "id": "a3e84858-34ae-42bf-8aef-dee5acc2fb72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRingingTheBell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a62059c6-0ca5-436a-8eed-c8defe83fcba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3e84858-34ae-42bf-8aef-dee5acc2fb72",
            "compositeImage": {
                "id": "b0da05eb-05b0-4023-8225-1633fa73e78b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a62059c6-0ca5-436a-8eed-c8defe83fcba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce8e1db3-9b8f-471b-9b67-e9a0cd6d3cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a62059c6-0ca5-436a-8eed-c8defe83fcba",
                    "LayerId": "a259395f-8705-4b04-8612-041860c269cc"
                }
            ]
        },
        {
            "id": "b9908c2c-5153-4fc4-a326-550f9f37dcfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3e84858-34ae-42bf-8aef-dee5acc2fb72",
            "compositeImage": {
                "id": "ee09718c-fb82-44cc-97c9-7a02a54b4b65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9908c2c-5153-4fc4-a326-550f9f37dcfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "022e721c-851a-4fa7-b2fb-ff7c975e7ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9908c2c-5153-4fc4-a326-550f9f37dcfa",
                    "LayerId": "a259395f-8705-4b04-8612-041860c269cc"
                }
            ]
        },
        {
            "id": "c0f47082-f8e0-4fbb-8e2d-e16a99f3a3e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3e84858-34ae-42bf-8aef-dee5acc2fb72",
            "compositeImage": {
                "id": "6b3c1343-a5f4-44d6-9e28-df8788e9bf8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f47082-f8e0-4fbb-8e2d-e16a99f3a3e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d7f289-afd7-48eb-9d92-554828df38b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f47082-f8e0-4fbb-8e2d-e16a99f3a3e3",
                    "LayerId": "a259395f-8705-4b04-8612-041860c269cc"
                }
            ]
        },
        {
            "id": "33786ac6-2aac-43d3-a8bf-f033b15aefbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3e84858-34ae-42bf-8aef-dee5acc2fb72",
            "compositeImage": {
                "id": "2d004a77-9d05-4a60-b769-d0b90abc82f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33786ac6-2aac-43d3-a8bf-f033b15aefbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a526f45-cec7-448e-964b-843c4ed74422",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33786ac6-2aac-43d3-a8bf-f033b15aefbc",
                    "LayerId": "a259395f-8705-4b04-8612-041860c269cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a259395f-8705-4b04-8612-041860c269cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3e84858-34ae-42bf-8aef-dee5acc2fb72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 12
}