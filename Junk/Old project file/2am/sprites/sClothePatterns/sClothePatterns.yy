{
    "id": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClothePatterns",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11277206-2cfe-43da-a67e-39f46317ce42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "compositeImage": {
                "id": "9a947224-36ef-4086-b701-a0e249691b59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11277206-2cfe-43da-a67e-39f46317ce42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85d8a189-902a-45f0-a50c-4558e8d46e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11277206-2cfe-43da-a67e-39f46317ce42",
                    "LayerId": "325c7e35-e9c5-4672-b7c9-34c34897e38a"
                }
            ]
        },
        {
            "id": "d7630623-5850-4b17-bfed-63ec93dd6ea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "compositeImage": {
                "id": "04c1a140-8406-4b1e-ac3d-9630431e52f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7630623-5850-4b17-bfed-63ec93dd6ea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcafd921-6d6a-4104-91e7-e8b1e9ae041a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7630623-5850-4b17-bfed-63ec93dd6ea1",
                    "LayerId": "325c7e35-e9c5-4672-b7c9-34c34897e38a"
                }
            ]
        },
        {
            "id": "6908383f-7e00-4950-83ae-982f0ffdda1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "compositeImage": {
                "id": "f52d94ad-c3db-42ff-adcb-8b07a81c1df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6908383f-7e00-4950-83ae-982f0ffdda1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca1c5ca-4308-4fef-901b-3066c3388843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6908383f-7e00-4950-83ae-982f0ffdda1a",
                    "LayerId": "325c7e35-e9c5-4672-b7c9-34c34897e38a"
                }
            ]
        },
        {
            "id": "414486bf-ef81-4cb0-a15e-498daf59f04f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "compositeImage": {
                "id": "33954c01-dd60-4572-8601-1f803540c6f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "414486bf-ef81-4cb0-a15e-498daf59f04f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc5c171-4642-4d2d-b02f-f4419a39d162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "414486bf-ef81-4cb0-a15e-498daf59f04f",
                    "LayerId": "325c7e35-e9c5-4672-b7c9-34c34897e38a"
                }
            ]
        },
        {
            "id": "9e28788e-6edf-4461-bd5e-5814e45c21d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "compositeImage": {
                "id": "b47cf819-15e3-4929-9194-239f26938401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e28788e-6edf-4461-bd5e-5814e45c21d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0fd76d9-114b-4891-82cc-7f8c47014c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e28788e-6edf-4461-bd5e-5814e45c21d9",
                    "LayerId": "325c7e35-e9c5-4672-b7c9-34c34897e38a"
                }
            ]
        },
        {
            "id": "d19338db-2a99-49c5-8c4c-faf1e3c603d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "compositeImage": {
                "id": "66cf1609-424d-48e5-911a-84ba2553266b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d19338db-2a99-49c5-8c4c-faf1e3c603d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24ced712-063c-43d4-8b33-d517c383cd47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d19338db-2a99-49c5-8c4c-faf1e3c603d8",
                    "LayerId": "325c7e35-e9c5-4672-b7c9-34c34897e38a"
                }
            ]
        },
        {
            "id": "043a5c1e-e56c-4e06-9b58-6dbf514b85bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "compositeImage": {
                "id": "6d5a819d-9866-4409-9675-4fdf735ec324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "043a5c1e-e56c-4e06-9b58-6dbf514b85bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66e975c9-5304-463a-a60a-9967f6d7ee47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "043a5c1e-e56c-4e06-9b58-6dbf514b85bb",
                    "LayerId": "325c7e35-e9c5-4672-b7c9-34c34897e38a"
                }
            ]
        },
        {
            "id": "54a3569e-05bb-4c04-8851-2f15d16be9d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "compositeImage": {
                "id": "e219da00-cfa5-43d7-9355-c03ebd28e92b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a3569e-05bb-4c04-8851-2f15d16be9d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75cbf072-e8a6-4dbc-8e2b-ad8e40182956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a3569e-05bb-4c04-8851-2f15d16be9d0",
                    "LayerId": "325c7e35-e9c5-4672-b7c9-34c34897e38a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "325c7e35-e9c5-4672-b7c9-34c34897e38a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bb64414-6be6-4b8c-9875-c8bbc0008fbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}