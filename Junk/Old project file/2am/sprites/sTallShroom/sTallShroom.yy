{
    "id": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTallShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ea29836-9a69-4a4d-b077-417acbd4802b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
            "compositeImage": {
                "id": "322bc462-1ca7-4756-aa03-a01e52f7d150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ea29836-9a69-4a4d-b077-417acbd4802b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a89bf6a-6485-4eca-a826-cd7e2a11bc01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ea29836-9a69-4a4d-b077-417acbd4802b",
                    "LayerId": "3d56dfa3-3b5a-4754-9d83-22e88b723037"
                }
            ]
        },
        {
            "id": "b41fdb2f-6c2b-494c-b076-264abf167c51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
            "compositeImage": {
                "id": "276365e9-07f8-4c75-a567-f85c79ab1d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b41fdb2f-6c2b-494c-b076-264abf167c51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5666058-64d6-470e-a67b-78f84cb11a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41fdb2f-6c2b-494c-b076-264abf167c51",
                    "LayerId": "3d56dfa3-3b5a-4754-9d83-22e88b723037"
                }
            ]
        },
        {
            "id": "c5a46824-7ce1-4023-a78a-fdde47e1793d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
            "compositeImage": {
                "id": "064aca7c-f61b-498a-b986-e7d4a91c072e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a46824-7ce1-4023-a78a-fdde47e1793d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23427a72-db6f-499f-93cf-45e3b8742ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a46824-7ce1-4023-a78a-fdde47e1793d",
                    "LayerId": "3d56dfa3-3b5a-4754-9d83-22e88b723037"
                }
            ]
        },
        {
            "id": "16b2be8e-06ea-4258-a114-c6d8284b11e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
            "compositeImage": {
                "id": "4d93ba81-22ab-4822-93b0-e7c5e183c0c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16b2be8e-06ea-4258-a114-c6d8284b11e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d5e7c8b-f2da-463f-96a8-63ddf9b4675e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16b2be8e-06ea-4258-a114-c6d8284b11e4",
                    "LayerId": "3d56dfa3-3b5a-4754-9d83-22e88b723037"
                }
            ]
        },
        {
            "id": "d8697ad0-a341-4859-8fe9-50053520242a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
            "compositeImage": {
                "id": "ac691f1e-a145-49ce-959d-2814fbcc2320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8697ad0-a341-4859-8fe9-50053520242a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98b3e23a-bf1f-403a-8216-b58bd606f627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8697ad0-a341-4859-8fe9-50053520242a",
                    "LayerId": "3d56dfa3-3b5a-4754-9d83-22e88b723037"
                }
            ]
        },
        {
            "id": "73345d86-d177-42d6-9219-828915b286fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
            "compositeImage": {
                "id": "0fd3821b-7c12-4f47-9767-0a10d483027a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73345d86-d177-42d6-9219-828915b286fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0472f4-aeda-42ec-b339-9a9c2aa453a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73345d86-d177-42d6-9219-828915b286fa",
                    "LayerId": "3d56dfa3-3b5a-4754-9d83-22e88b723037"
                }
            ]
        },
        {
            "id": "7e8281fe-2920-410e-87f8-14ad51f40e26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
            "compositeImage": {
                "id": "ab48e98c-8486-4f87-91d6-e1eda8093a9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e8281fe-2920-410e-87f8-14ad51f40e26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80432678-9fb0-4c80-8700-e9264395da8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e8281fe-2920-410e-87f8-14ad51f40e26",
                    "LayerId": "3d56dfa3-3b5a-4754-9d83-22e88b723037"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3d56dfa3-3b5a-4754-9d83-22e88b723037",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10da749b-f0ba-4850-ab41-7d9687abc7c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 24
}