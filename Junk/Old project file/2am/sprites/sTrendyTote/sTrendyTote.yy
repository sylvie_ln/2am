{
    "id": "def3cb12-caa8-4357-839e-e84bb8c0557c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrendyTote",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b25c4ff-0b02-498b-804c-5b09c8831635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def3cb12-caa8-4357-839e-e84bb8c0557c",
            "compositeImage": {
                "id": "147de797-d8a4-4084-9f35-17ffe3ffcdd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b25c4ff-0b02-498b-804c-5b09c8831635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fa438c8-a5e0-48f7-814d-d4919757ac18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b25c4ff-0b02-498b-804c-5b09c8831635",
                    "LayerId": "22f635d3-377e-4b10-9453-7ee83a6ba8fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "22f635d3-377e-4b10-9453-7ee83a6ba8fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "def3cb12-caa8-4357-839e-e84bb8c0557c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}