{
    "id": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTinyShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2774da7e-01e7-49f9-afeb-bacd6ca9e9c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
            "compositeImage": {
                "id": "06fe7739-3099-4dc3-81bf-1bdc0ac89070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2774da7e-01e7-49f9-afeb-bacd6ca9e9c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e347d473-d538-4e16-9c91-c61026248251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2774da7e-01e7-49f9-afeb-bacd6ca9e9c4",
                    "LayerId": "9b1c3ed3-f5b2-4a75-9022-c674c344d979"
                }
            ]
        },
        {
            "id": "92314530-1657-4e87-b3e9-f28098f7178d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
            "compositeImage": {
                "id": "9ae6ea40-5617-417f-bd76-7f45113c0f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92314530-1657-4e87-b3e9-f28098f7178d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726448bf-a037-4c3c-9177-ec6f0cd9f7ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92314530-1657-4e87-b3e9-f28098f7178d",
                    "LayerId": "9b1c3ed3-f5b2-4a75-9022-c674c344d979"
                }
            ]
        },
        {
            "id": "235e3869-6742-4b27-9cc6-92bb6c073ff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
            "compositeImage": {
                "id": "c26d6947-fbb1-4544-80fc-2cd667734c79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235e3869-6742-4b27-9cc6-92bb6c073ff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ff06ffb-f907-47bd-b317-ce1aa0431f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235e3869-6742-4b27-9cc6-92bb6c073ff2",
                    "LayerId": "9b1c3ed3-f5b2-4a75-9022-c674c344d979"
                }
            ]
        },
        {
            "id": "6bb0d85f-36ab-43d4-97d4-3f129279b3bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
            "compositeImage": {
                "id": "86b12cbb-76b6-4759-b945-661a27f985b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bb0d85f-36ab-43d4-97d4-3f129279b3bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba55ff1-22a8-4b53-a4d5-8475e0f46c5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bb0d85f-36ab-43d4-97d4-3f129279b3bf",
                    "LayerId": "9b1c3ed3-f5b2-4a75-9022-c674c344d979"
                }
            ]
        },
        {
            "id": "352128ee-5fab-4f81-92b0-fa63f82d65df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
            "compositeImage": {
                "id": "29926ca5-47d9-414e-878f-834ed827925d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "352128ee-5fab-4f81-92b0-fa63f82d65df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d7e13ba-e8c5-4900-b6a3-e2044a740bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "352128ee-5fab-4f81-92b0-fa63f82d65df",
                    "LayerId": "9b1c3ed3-f5b2-4a75-9022-c674c344d979"
                }
            ]
        },
        {
            "id": "c55e83fa-5d6d-4bb4-a7e3-0ca9c978bc50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
            "compositeImage": {
                "id": "c730beee-b477-403f-8a41-ff3a79c5d6d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c55e83fa-5d6d-4bb4-a7e3-0ca9c978bc50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "647a4f80-c1e0-4433-b514-be9d7a39314e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c55e83fa-5d6d-4bb4-a7e3-0ca9c978bc50",
                    "LayerId": "9b1c3ed3-f5b2-4a75-9022-c674c344d979"
                }
            ]
        },
        {
            "id": "3f83715e-6ca5-4c6a-875f-7c006925bbb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
            "compositeImage": {
                "id": "54051df7-daed-4ffb-ba0a-d94b4837f718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f83715e-6ca5-4c6a-875f-7c006925bbb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "555e2f23-1ed5-4b51-9cd8-f1193eb8fc27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f83715e-6ca5-4c6a-875f-7c006925bbb7",
                    "LayerId": "9b1c3ed3-f5b2-4a75-9022-c674c344d979"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9b1c3ed3-f5b2-4a75-9022-c674c344d979",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a102e6d0-e4c6-47a1-a241-4d0bd3686dba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}