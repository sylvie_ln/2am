{
    "id": "7a7a8d89-db95-44e9-8183-a5e97af6ebbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "019de9e9-ba25-4333-a462-8c906cd00078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7a8d89-db95-44e9-8183-a5e97af6ebbd",
            "compositeImage": {
                "id": "6284efc8-1a72-4a48-8bfd-21be44ecba56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "019de9e9-ba25-4333-a462-8c906cd00078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7a22e7-d0b2-42f2-a396-2682620d45d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "019de9e9-ba25-4333-a462-8c906cd00078",
                    "LayerId": "dfeecbc8-211d-43c4-bf41-7c425a9d29a9"
                }
            ]
        },
        {
            "id": "196fbe21-15e8-47a4-a0d1-8bb444f7b3f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7a8d89-db95-44e9-8183-a5e97af6ebbd",
            "compositeImage": {
                "id": "0e930f63-82ad-4126-9fec-d9e0b1b160d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "196fbe21-15e8-47a4-a0d1-8bb444f7b3f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7896129-d126-49c4-b021-524380e4d6d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "196fbe21-15e8-47a4-a0d1-8bb444f7b3f9",
                    "LayerId": "dfeecbc8-211d-43c4-bf41-7c425a9d29a9"
                }
            ]
        },
        {
            "id": "c6934dba-2540-45e5-b237-8214ad22aa52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7a8d89-db95-44e9-8183-a5e97af6ebbd",
            "compositeImage": {
                "id": "b044ed8a-2384-4d48-87f2-ebad5a55064a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6934dba-2540-45e5-b237-8214ad22aa52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c42e07a-3b76-449c-8be7-9cfa3f94fba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6934dba-2540-45e5-b237-8214ad22aa52",
                    "LayerId": "dfeecbc8-211d-43c4-bf41-7c425a9d29a9"
                }
            ]
        },
        {
            "id": "85db015b-4904-4f26-b32a-ee544a24ac4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7a8d89-db95-44e9-8183-a5e97af6ebbd",
            "compositeImage": {
                "id": "16ec05c3-d3d4-4947-a56a-291557a810cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85db015b-4904-4f26-b32a-ee544a24ac4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d3457e-94f9-40cf-a297-28febedcaad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85db015b-4904-4f26-b32a-ee544a24ac4f",
                    "LayerId": "dfeecbc8-211d-43c4-bf41-7c425a9d29a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dfeecbc8-211d-43c4-bf41-7c425a9d29a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a7a8d89-db95-44e9-8183-a5e97af6ebbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}