{
    "id": "ccc0e7b1-452f-4461-ab35-7574524dcb5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87121c06-5ae0-4dcc-9f8d-b7f20ec880f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccc0e7b1-452f-4461-ab35-7574524dcb5b",
            "compositeImage": {
                "id": "27e4c8b7-027b-434e-9eef-8d41533ccb0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87121c06-5ae0-4dcc-9f8d-b7f20ec880f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0680a671-0a12-4f69-a503-a6bd36e774f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87121c06-5ae0-4dcc-9f8d-b7f20ec880f1",
                    "LayerId": "3ac6c399-5014-4389-a0d4-b603047e7a65"
                }
            ]
        },
        {
            "id": "f35be3df-62bb-468d-b9c4-22e30bed5315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccc0e7b1-452f-4461-ab35-7574524dcb5b",
            "compositeImage": {
                "id": "265a71c1-d6ec-458a-8797-74b2b7098124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f35be3df-62bb-468d-b9c4-22e30bed5315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7107b83c-6a1f-441b-a867-85ba02b49880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f35be3df-62bb-468d-b9c4-22e30bed5315",
                    "LayerId": "3ac6c399-5014-4389-a0d4-b603047e7a65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3ac6c399-5014-4389-a0d4-b603047e7a65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccc0e7b1-452f-4461-ab35-7574524dcb5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}