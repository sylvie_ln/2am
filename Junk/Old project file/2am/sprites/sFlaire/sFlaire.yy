{
    "id": "a81cbee7-d66a-432d-b723-9dc17da48cbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlaire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0a5eec6-2232-4a55-aec1-600b00c7b916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81cbee7-d66a-432d-b723-9dc17da48cbf",
            "compositeImage": {
                "id": "e716b030-db7a-4eb8-953f-22966faa5456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a5eec6-2232-4a55-aec1-600b00c7b916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ddf07e7-2acf-4cff-a30c-c80707508a7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a5eec6-2232-4a55-aec1-600b00c7b916",
                    "LayerId": "f8b1b662-05d8-4c10-a64f-7f496bda640c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f8b1b662-05d8-4c10-a64f-7f496bda640c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a81cbee7-d66a-432d-b723-9dc17da48cbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}