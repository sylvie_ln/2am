{
    "id": "d5fe8d84-2e5d-4954-a024-840e9397a393",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFishCarSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4965a9e-fdb1-4824-b76e-cd30a2a92cb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5fe8d84-2e5d-4954-a024-840e9397a393",
            "compositeImage": {
                "id": "f8b973d4-f358-4aa2-a4e5-9299d35cf457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4965a9e-fdb1-4824-b76e-cd30a2a92cb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b23bce1b-ee3d-4a79-87b0-cfe22b613966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4965a9e-fdb1-4824-b76e-cd30a2a92cb8",
                    "LayerId": "d494d26b-6ab3-4205-822d-e62e7040d007"
                },
                {
                    "id": "a16d166e-d15d-4278-ac8d-1e1da1098d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4965a9e-fdb1-4824-b76e-cd30a2a92cb8",
                    "LayerId": "f0d7cdae-2025-4e3b-bade-1abcc54cec72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d494d26b-6ab3-4205-822d-e62e7040d007",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5fe8d84-2e5d-4954-a024-840e9397a393",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f0d7cdae-2025-4e3b-bade-1abcc54cec72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5fe8d84-2e5d-4954-a024-840e9397a393",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 18
}