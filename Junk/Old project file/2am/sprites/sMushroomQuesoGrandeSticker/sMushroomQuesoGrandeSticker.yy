{
    "id": "773f1b95-9361-4503-8a0a-6e8044cb5c11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMushroomQuesoGrandeSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20b0c21a-a6ed-4174-a104-2ccab7d8de8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773f1b95-9361-4503-8a0a-6e8044cb5c11",
            "compositeImage": {
                "id": "7541828e-9552-43a3-9311-3b54108c63bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20b0c21a-a6ed-4174-a104-2ccab7d8de8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e695080c-15a1-4c0a-959f-14c8ca9df249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20b0c21a-a6ed-4174-a104-2ccab7d8de8e",
                    "LayerId": "0b2cb021-3f3e-41c6-ae7e-5f32f4b7b4c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0b2cb021-3f3e-41c6-ae7e-5f32f4b7b4c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "773f1b95-9361-4503-8a0a-6e8044cb5c11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}