{
    "id": "dc560cfc-c4ca-4e03-98b9-91ac6417625a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 3,
    "bbox_right": 77,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf38e83b-b407-4fe5-82fd-7bec3014924c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc560cfc-c4ca-4e03-98b9-91ac6417625a",
            "compositeImage": {
                "id": "4f3031b6-ab2d-4d17-bfba-775484d7c4e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf38e83b-b407-4fe5-82fd-7bec3014924c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca289dd-2542-487e-93df-4d5249acefae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf38e83b-b407-4fe5-82fd-7bec3014924c",
                    "LayerId": "38916d02-4d68-4529-bd3e-be947f41c999"
                }
            ]
        },
        {
            "id": "cd8fdfde-b627-4210-9bd6-65f6d70ae0c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc560cfc-c4ca-4e03-98b9-91ac6417625a",
            "compositeImage": {
                "id": "0ae90be4-475c-4487-83b2-5134ea951182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd8fdfde-b627-4210-9bd6-65f6d70ae0c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e04b4773-ba2d-43bc-8863-238d2abb5fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd8fdfde-b627-4210-9bd6-65f6d70ae0c4",
                    "LayerId": "38916d02-4d68-4529-bd3e-be947f41c999"
                }
            ]
        },
        {
            "id": "b8f5996c-4086-493e-85cc-e32af21e771e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc560cfc-c4ca-4e03-98b9-91ac6417625a",
            "compositeImage": {
                "id": "f4a031bb-a7ab-4202-9451-99583b404bf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8f5996c-4086-493e-85cc-e32af21e771e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c097d9a1-85df-4459-9223-509bd566ea8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8f5996c-4086-493e-85cc-e32af21e771e",
                    "LayerId": "38916d02-4d68-4529-bd3e-be947f41c999"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "38916d02-4d68-4529-bd3e-be947f41c999",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc560cfc-c4ca-4e03-98b9-91ac6417625a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 47
}