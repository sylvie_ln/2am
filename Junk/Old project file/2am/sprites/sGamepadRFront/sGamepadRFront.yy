{
    "id": "d1f23f0c-709e-42ba-a4d5-6701ea652df1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadRFront",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 61,
    "bbox_right": 82,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12446159-4a6a-4ad2-9090-f8c092caf85c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f23f0c-709e-42ba-a4d5-6701ea652df1",
            "compositeImage": {
                "id": "e43eabb8-ee59-4134-9aca-bba8f7ac710b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12446159-4a6a-4ad2-9090-f8c092caf85c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74e1807b-3996-4cd0-b355-935a1c6d700f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12446159-4a6a-4ad2-9090-f8c092caf85c",
                    "LayerId": "0d5ed181-5e8c-4539-b8c8-74b3202acbaf"
                },
                {
                    "id": "98d5d63f-e519-4680-b7d8-554282bc386c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12446159-4a6a-4ad2-9090-f8c092caf85c",
                    "LayerId": "2649dd66-8740-48a0-b6bb-a5b7b73a3632"
                }
            ]
        },
        {
            "id": "10a50788-fd9c-4914-94b1-cc18af91aeac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f23f0c-709e-42ba-a4d5-6701ea652df1",
            "compositeImage": {
                "id": "9e3e8dac-67c6-4d5c-984d-564b0e2c2058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10a50788-fd9c-4914-94b1-cc18af91aeac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc72f69-290a-4a61-8ae7-b24ceca5830b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10a50788-fd9c-4914-94b1-cc18af91aeac",
                    "LayerId": "0d5ed181-5e8c-4539-b8c8-74b3202acbaf"
                },
                {
                    "id": "9815579e-3119-4ed7-809c-93c77e8c3a8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10a50788-fd9c-4914-94b1-cc18af91aeac",
                    "LayerId": "2649dd66-8740-48a0-b6bb-a5b7b73a3632"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "0d5ed181-5e8c-4539-b8c8-74b3202acbaf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1f23f0c-709e-42ba-a4d5-6701ea652df1",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2649dd66-8740-48a0-b6bb-a5b7b73a3632",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1f23f0c-709e-42ba-a4d5-6701ea652df1",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}