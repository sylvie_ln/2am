{
    "id": "af263178-8187-446d-9ea6-d91c70501203",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLocket",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3c54cd1-ac54-4fd7-a97a-76bae2179878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af263178-8187-446d-9ea6-d91c70501203",
            "compositeImage": {
                "id": "f7f020ef-4161-4ea3-bc99-6f7515a25551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3c54cd1-ac54-4fd7-a97a-76bae2179878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "583e6dff-6a21-42da-ab44-290c8ef7f8fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3c54cd1-ac54-4fd7-a97a-76bae2179878",
                    "LayerId": "c26c5041-dafd-4f7b-ac2f-369a845943ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c26c5041-dafd-4f7b-ac2f-369a845943ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af263178-8187-446d-9ea6-d91c70501203",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}