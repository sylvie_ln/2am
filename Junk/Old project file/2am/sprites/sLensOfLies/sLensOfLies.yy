{
    "id": "68df445d-b94b-49d7-905e-be0d7f82660d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLensOfLies",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6660ce1-ddfd-4c54-9a61-3b884be2c785",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68df445d-b94b-49d7-905e-be0d7f82660d",
            "compositeImage": {
                "id": "d6ffe363-63b7-454e-a733-50ba9b507b98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6660ce1-ddfd-4c54-9a61-3b884be2c785",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5fcb1cb-00c6-4f5b-a7ef-2c3e7807fd94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6660ce1-ddfd-4c54-9a61-3b884be2c785",
                    "LayerId": "b0d7bbc1-4463-4f03-a982-0165849822be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b0d7bbc1-4463-4f03-a982-0165849822be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68df445d-b94b-49d7-905e-be0d7f82660d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}