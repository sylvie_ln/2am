{
    "id": "1b0b96e4-3d80-4918-a8cb-65153d97c715",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadLFront",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 13,
    "bbox_right": 34,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee550956-0a01-4808-808b-d272b7880a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b0b96e4-3d80-4918-a8cb-65153d97c715",
            "compositeImage": {
                "id": "f9e41b1a-5cf0-449c-bba1-8aa4e6a23a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee550956-0a01-4808-808b-d272b7880a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f24c953-b973-49f4-b896-a412046fa990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee550956-0a01-4808-808b-d272b7880a80",
                    "LayerId": "8844a862-72c7-4499-acce-81c88175df0d"
                },
                {
                    "id": "d66091b5-fb4a-47ca-ab94-6f28ea702c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee550956-0a01-4808-808b-d272b7880a80",
                    "LayerId": "3f9a5018-7354-44c1-b018-1814580ecc14"
                }
            ]
        },
        {
            "id": "d3858381-0d9b-4665-86c6-0a0186c3d27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b0b96e4-3d80-4918-a8cb-65153d97c715",
            "compositeImage": {
                "id": "9e0538fd-138d-4b48-bfff-bc3655554b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3858381-0d9b-4665-86c6-0a0186c3d27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d89f45-4038-4805-bf08-921661dfefbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3858381-0d9b-4665-86c6-0a0186c3d27f",
                    "LayerId": "8844a862-72c7-4499-acce-81c88175df0d"
                },
                {
                    "id": "061b47dd-7fd9-4f64-9391-e148dd848bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3858381-0d9b-4665-86c6-0a0186c3d27f",
                    "LayerId": "3f9a5018-7354-44c1-b018-1814580ecc14"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "8844a862-72c7-4499-acce-81c88175df0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b0b96e4-3d80-4918-a8cb-65153d97c715",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3f9a5018-7354-44c1-b018-1814580ecc14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b0b96e4-3d80-4918-a8cb-65153d97c715",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}