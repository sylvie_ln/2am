{
    "id": "7fda49f7-70ea-4db8-9d48-c3b91d5f5d46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoomBoxSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd9bf22e-88d3-469a-9603-8f441f7319e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fda49f7-70ea-4db8-9d48-c3b91d5f5d46",
            "compositeImage": {
                "id": "4098ff20-7b24-4356-9b7f-b6cdf004248e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd9bf22e-88d3-469a-9603-8f441f7319e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b676315e-7d4c-46a0-8c0f-7d0959d45149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd9bf22e-88d3-469a-9603-8f441f7319e0",
                    "LayerId": "b5999327-2f42-40b0-a26b-386bbd12ddc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b5999327-2f42-40b0-a26b-386bbd12ddc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fda49f7-70ea-4db8-9d48-c3b91d5f5d46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}