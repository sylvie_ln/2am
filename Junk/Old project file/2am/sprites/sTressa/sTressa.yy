{
    "id": "d613faeb-3ded-4dc6-9e45-5413bb55d71d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTressa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d84f7889-9a22-45cf-97c0-00324ad4087a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d613faeb-3ded-4dc6-9e45-5413bb55d71d",
            "compositeImage": {
                "id": "92234e36-1b85-439c-837b-75dd94080931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84f7889-9a22-45cf-97c0-00324ad4087a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "512d0c4d-45a4-4a7c-a293-3794354cdff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84f7889-9a22-45cf-97c0-00324ad4087a",
                    "LayerId": "e32f3739-8106-41e0-a9d3-26279eae4517"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e32f3739-8106-41e0-a9d3-26279eae4517",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d613faeb-3ded-4dc6-9e45-5413bb55d71d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}