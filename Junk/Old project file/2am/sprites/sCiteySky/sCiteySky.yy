{
    "id": "12d42557-b0b8-43d7-8fe7-ff8b7a2ef520",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCiteySky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b8e5f63-66b7-4cd5-81f3-196fe1da9d1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12d42557-b0b8-43d7-8fe7-ff8b7a2ef520",
            "compositeImage": {
                "id": "4af6a717-f52d-4852-9b7b-7d45f1a7f150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b8e5f63-66b7-4cd5-81f3-196fe1da9d1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8c1e09-9b17-4772-9811-d7ee3f71c1ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b8e5f63-66b7-4cd5-81f3-196fe1da9d1d",
                    "LayerId": "7159476a-11bb-4ea4-83ba-51250eb64929"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7159476a-11bb-4ea4-83ba-51250eb64929",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12d42557-b0b8-43d7-8fe7-ff8b7a2ef520",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}