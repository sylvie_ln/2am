{
    "id": "243416ca-24be-4d71-ad3d-b60a8b7f5af9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCarelessDesire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b503721a-8279-4813-b823-9fa8b7b6f46b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243416ca-24be-4d71-ad3d-b60a8b7f5af9",
            "compositeImage": {
                "id": "3ab98d64-9d04-4b0e-916e-e680cf1d8a83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b503721a-8279-4813-b823-9fa8b7b6f46b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e7951f1-b1d7-4843-bb2e-e54636a9d7b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b503721a-8279-4813-b823-9fa8b7b6f46b",
                    "LayerId": "4b1fa03b-75d3-4f4c-b0be-adb38b62f7b9"
                }
            ]
        },
        {
            "id": "affee3ef-daed-4163-8ed4-6c6a09aa2bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243416ca-24be-4d71-ad3d-b60a8b7f5af9",
            "compositeImage": {
                "id": "5f2e1e48-0144-46ab-baa3-7c3cf13ed4b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "affee3ef-daed-4163-8ed4-6c6a09aa2bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa9e0228-fa1b-4451-943b-6af581201d87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "affee3ef-daed-4163-8ed4-6c6a09aa2bc2",
                    "LayerId": "4b1fa03b-75d3-4f4c-b0be-adb38b62f7b9"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "4b1fa03b-75d3-4f4c-b0be-adb38b62f7b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "243416ca-24be-4d71-ad3d-b60a8b7f5af9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}