{
    "id": "676bab5f-6124-4be0-9a8a-c528f18dbe7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCherie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9540bd7e-5a6e-406e-835f-b6bffbbab342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "676bab5f-6124-4be0-9a8a-c528f18dbe7d",
            "compositeImage": {
                "id": "b6e2c382-358f-4aa8-b460-b1be88f265f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9540bd7e-5a6e-406e-835f-b6bffbbab342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "058a0e57-bd06-4da8-b496-a182858398ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9540bd7e-5a6e-406e-835f-b6bffbbab342",
                    "LayerId": "0339434b-bf4e-4b46-8c7e-a0c4cc46d3dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0339434b-bf4e-4b46-8c7e-a0c4cc46d3dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "676bab5f-6124-4be0-9a8a-c528f18dbe7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}