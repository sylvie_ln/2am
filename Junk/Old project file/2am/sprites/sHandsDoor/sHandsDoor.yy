{
    "id": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHandsDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb75ca00-d21e-4165-a510-79cd2715ae7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "367bfc76-d773-45d7-aec6-6a281d575cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb75ca00-d21e-4165-a510-79cd2715ae7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ebdd300-8c59-4906-a22c-bb85e314ae6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb75ca00-d21e-4165-a510-79cd2715ae7e",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "dc4e713b-7f88-4a33-9359-4c488e84e26b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb75ca00-d21e-4165-a510-79cd2715ae7e",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "f9f8ed45-740a-4aec-b3f3-7bb9c722488f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "2b88cf9d-bf17-4287-a6b8-3d49c0746a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9f8ed45-740a-4aec-b3f3-7bb9c722488f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58c67642-81c3-4161-8c9b-3c659a5b9f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f8ed45-740a-4aec-b3f3-7bb9c722488f",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "9b811496-586a-4718-a772-70bbfef62ad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f8ed45-740a-4aec-b3f3-7bb9c722488f",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "d635f057-c209-49e1-bfb2-6f843ab9294d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "34592ab2-3cff-4907-aad4-4d61ca68cc8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d635f057-c209-49e1-bfb2-6f843ab9294d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b8ed3b-454e-410d-919a-b3cbd2ec3217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d635f057-c209-49e1-bfb2-6f843ab9294d",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "ae72ae88-6ca0-4ddb-8534-abe441232315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d635f057-c209-49e1-bfb2-6f843ab9294d",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "c9c4082e-7adc-49b8-8936-713eda2d9570",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "643ca4bc-35e5-46ff-92b9-eac56ca8d0e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c4082e-7adc-49b8-8936-713eda2d9570",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e270785-2ac3-42b5-ab65-9ee0226d2b5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c4082e-7adc-49b8-8936-713eda2d9570",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "e9412171-7930-48a9-b985-d2114bbceb75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c4082e-7adc-49b8-8936-713eda2d9570",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "73f3b94a-fb57-478d-817e-0d0b2ef9b5ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "d3f02f41-40a3-4e74-a54e-eda71f3a6be8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f3b94a-fb57-478d-817e-0d0b2ef9b5ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea919fa8-7db1-4dc6-ad67-ff926bb39935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f3b94a-fb57-478d-817e-0d0b2ef9b5ff",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "9bd61f08-64d5-4f49-81b0-aa460e220e4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f3b94a-fb57-478d-817e-0d0b2ef9b5ff",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "a87d95f3-3568-4e31-a192-829988ebedab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "3f01b1e7-13db-429d-a7f8-681d1ffb8fd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a87d95f3-3568-4e31-a192-829988ebedab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e51f4ed5-2d6b-4688-a0ef-559d813d9ceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87d95f3-3568-4e31-a192-829988ebedab",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "2636d079-33c7-4b01-bf2a-80f7985f6fb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87d95f3-3568-4e31-a192-829988ebedab",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "22473930-3e6c-4e89-995b-1b4c8018d47c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "323240e8-152a-4f64-9387-cb198b03ee8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22473930-3e6c-4e89-995b-1b4c8018d47c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1451bf1-9398-416a-9998-a88d22b32dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22473930-3e6c-4e89-995b-1b4c8018d47c",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "58b492a9-01f0-4eb0-bd93-3eb919bfff4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22473930-3e6c-4e89-995b-1b4c8018d47c",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "4164ef05-fb02-4462-a601-bbac0594f66d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "1ee5cdf3-c245-4ce5-bfaa-ed7990280e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4164ef05-fb02-4462-a601-bbac0594f66d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "587d6ff8-9d65-472c-9d8e-ce092c71b3da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4164ef05-fb02-4462-a601-bbac0594f66d",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "61de7410-96ab-4ea0-ad99-cbc0afb7e0cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4164ef05-fb02-4462-a601-bbac0594f66d",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "6a985c2a-66c0-4d13-9206-1f2e07752abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "184facaa-a7e8-4382-a2b3-ede5dfee8b07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a985c2a-66c0-4d13-9206-1f2e07752abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ad52c0-b476-4409-939f-6f47fb481dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a985c2a-66c0-4d13-9206-1f2e07752abc",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "0846aa19-4930-456f-b914-6e8e90be441a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a985c2a-66c0-4d13-9206-1f2e07752abc",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "7d60122e-7d03-428a-af28-5906b8ff151b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "0dc0190b-af5a-4c7e-af80-0296ce6b1041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d60122e-7d03-428a-af28-5906b8ff151b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14117bff-6489-4877-ae87-ccbcdd1662ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d60122e-7d03-428a-af28-5906b8ff151b",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "26e741ed-e0f8-4990-a22b-9b2043e57e82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d60122e-7d03-428a-af28-5906b8ff151b",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "3fb69a37-3d75-4f57-94af-72383a048f8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "c4db7d65-e814-4ecc-90a1-c39de1aa406e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fb69a37-3d75-4f57-94af-72383a048f8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1e55d1a-496a-4830-bb52-03638957712d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb69a37-3d75-4f57-94af-72383a048f8a",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "c967065d-7fd1-4aa0-ba00-e7956f0c220b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb69a37-3d75-4f57-94af-72383a048f8a",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        },
        {
            "id": "ad53e877-1ea8-441b-82c3-9fcc60201e55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "compositeImage": {
                "id": "71c6d04d-fd8a-40ec-97b9-08e323e133ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad53e877-1ea8-441b-82c3-9fcc60201e55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbe6a837-5e20-4497-b6f2-b07b1647700f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad53e877-1ea8-441b-82c3-9fcc60201e55",
                    "LayerId": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5"
                },
                {
                    "id": "2fb8c1f4-25de-408e-90bf-5af38fbca438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad53e877-1ea8-441b-82c3-9fcc60201e55",
                    "LayerId": "394dafb6-dcca-470c-b236-24555e1726f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c9d2ed8d-401d-442c-adf3-a5f18f3526b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "394dafb6-dcca-470c-b236-24555e1726f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "143bb8ee-af8c-4853-b25d-e5eb10e148ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4294901889,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}