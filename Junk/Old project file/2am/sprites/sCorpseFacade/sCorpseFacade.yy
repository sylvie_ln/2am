{
    "id": "abf0c357-e189-4801-a4f2-3398d64f8750",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCorpseFacade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "647cea38-749c-41ad-9426-d3870bf21c43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abf0c357-e189-4801-a4f2-3398d64f8750",
            "compositeImage": {
                "id": "731b0cfc-3e4e-4371-b8e3-461548959732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647cea38-749c-41ad-9426-d3870bf21c43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a8b8ea-112e-40ea-ba8f-9da16087ea94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647cea38-749c-41ad-9426-d3870bf21c43",
                    "LayerId": "7ef2e740-e6a2-46b2-bdb1-a8f76761bd9f"
                }
            ]
        },
        {
            "id": "c016de49-3b2c-49f9-8c35-9aa8fcdc36a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abf0c357-e189-4801-a4f2-3398d64f8750",
            "compositeImage": {
                "id": "02113b01-0953-4c29-af79-dc4267ae234a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c016de49-3b2c-49f9-8c35-9aa8fcdc36a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c47ba095-c533-47e1-b13e-ac0b28985af4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c016de49-3b2c-49f9-8c35-9aa8fcdc36a7",
                    "LayerId": "7ef2e740-e6a2-46b2-bdb1-a8f76761bd9f"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 20,
    "layers": [
        {
            "id": "7ef2e740-e6a2-46b2-bdb1-a8f76761bd9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abf0c357-e189-4801-a4f2-3398d64f8750",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}