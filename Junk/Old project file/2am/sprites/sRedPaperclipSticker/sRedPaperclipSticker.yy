{
    "id": "b5880b2a-f211-4e77-8de9-da817a44d175",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRedPaperclipSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21f8f006-a726-434f-8580-6bda9ffdb7bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5880b2a-f211-4e77-8de9-da817a44d175",
            "compositeImage": {
                "id": "05988b6f-d381-41b1-af45-b6628627b56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f8f006-a726-434f-8580-6bda9ffdb7bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddd64d6d-7618-4597-9132-f7937f7b9f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f8f006-a726-434f-8580-6bda9ffdb7bb",
                    "LayerId": "9da01a78-bb64-4404-a38f-8b093c652a88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9da01a78-bb64-4404-a38f-8b093c652a88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5880b2a-f211-4e77-8de9-da817a44d175",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}