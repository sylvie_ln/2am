{
    "id": "36dee931-4a19-4f61-97f0-ffea365460c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWideBagInterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 7,
    "bbox_right": 89,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c131563-f596-498e-80d4-6b86b46fe0f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dee931-4a19-4f61-97f0-ffea365460c9",
            "compositeImage": {
                "id": "cc6f070a-c69c-4bbc-8318-847d1c41c6ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c131563-f596-498e-80d4-6b86b46fe0f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ffc9ad6-bf34-4da9-9692-b488f51f2ef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c131563-f596-498e-80d4-6b86b46fe0f5",
                    "LayerId": "b401a2b7-fce4-4ad0-9d71-a2d8adc1fde6"
                },
                {
                    "id": "a182b0fc-bcb8-45ed-9106-244c3a1aab12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c131563-f596-498e-80d4-6b86b46fe0f5",
                    "LayerId": "274d14b4-a0bb-4894-9f06-e864d341dab7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b401a2b7-fce4-4ad0-9d71-a2d8adc1fde6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36dee931-4a19-4f61-97f0-ffea365460c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "274d14b4-a0bb-4894-9f06-e864d341dab7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36dee931-4a19-4f61-97f0-ffea365460c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 40
}