{
    "id": "db17f898-0e9d-42e4-a0db-acce725d4e40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWideBagUpgradeSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27cc45fa-dfa9-4f39-a38f-62eb64831f83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db17f898-0e9d-42e4-a0db-acce725d4e40",
            "compositeImage": {
                "id": "83c6b652-6d12-4747-a05c-ad081adf3aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27cc45fa-dfa9-4f39-a38f-62eb64831f83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b9a3924-e540-4874-bf88-fde60d7d6107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27cc45fa-dfa9-4f39-a38f-62eb64831f83",
                    "LayerId": "3f58842d-20cd-489b-8acb-edba9a000c2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3f58842d-20cd-489b-8acb-edba9a000c2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db17f898-0e9d-42e4-a0db-acce725d4e40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}