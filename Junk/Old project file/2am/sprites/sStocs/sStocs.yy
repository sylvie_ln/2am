{
    "id": "5448e1b9-9c4e-4f9d-a6e7-76b45d4f10e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStocs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fb5d662-f22a-4cdc-a720-1f1688242e6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5448e1b9-9c4e-4f9d-a6e7-76b45d4f10e6",
            "compositeImage": {
                "id": "a77b936e-4b11-4223-9076-1322c2a5e56a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb5d662-f22a-4cdc-a720-1f1688242e6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289081e8-a957-4225-b583-5df105b88ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb5d662-f22a-4cdc-a720-1f1688242e6a",
                    "LayerId": "b2ed25a5-5bed-4291-843c-3c66eab850a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b2ed25a5-5bed-4291-843c-3c66eab850a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5448e1b9-9c4e-4f9d-a6e7-76b45d4f10e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}