{
    "id": "0cb71d18-276b-42ab-b2c5-6e7aaab05eea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopShirtSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e70f08ec-3d5b-45a1-b473-3573e72beea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cb71d18-276b-42ab-b2c5-6e7aaab05eea",
            "compositeImage": {
                "id": "447b1a7e-dd06-4a26-ba6c-07073e8846f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e70f08ec-3d5b-45a1-b473-3573e72beea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7500b8f9-379e-40d4-8d06-6d67f38823e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e70f08ec-3d5b-45a1-b473-3573e72beea9",
                    "LayerId": "82829dcd-6bea-4d80-820b-51a98883a636"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "82829dcd-6bea-4d80-820b-51a98883a636",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cb71d18-276b-42ab-b2c5-6e7aaab05eea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}