{
    "id": "59092ac7-8b1b-4e26-b256-146e1a70bafa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFishCar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2377e68-82ab-4102-9a6b-d573dc9f10cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59092ac7-8b1b-4e26-b256-146e1a70bafa",
            "compositeImage": {
                "id": "cba6076f-2e0a-4c83-9995-9988fb6d8dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2377e68-82ab-4102-9a6b-d573dc9f10cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05408126-472d-4a51-bc4b-095f0bb8e88b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2377e68-82ab-4102-9a6b-d573dc9f10cf",
                    "LayerId": "5347d470-a672-42a0-a275-64c6637a5fd5"
                },
                {
                    "id": "52ae0c05-e8d9-40fa-99d5-ea1891c4d6b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2377e68-82ab-4102-9a6b-d573dc9f10cf",
                    "LayerId": "c60344af-2973-4882-9418-504b19bb08fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c60344af-2973-4882-9418-504b19bb08fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59092ac7-8b1b-4e26-b256-146e1a70bafa",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5347d470-a672-42a0-a275-64c6637a5fd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59092ac7-8b1b-4e26-b256-146e1a70bafa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}