{
    "id": "8d365a3b-67de-4086-ab2f-a87ab531439c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHandsSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "172c9aca-5d92-4b0d-8dda-73173d85db40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "273d4711-bffe-4586-99e6-95bf28beab63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "172c9aca-5d92-4b0d-8dda-73173d85db40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b07afbfd-ab94-4ea0-ae0f-7692f8712c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "172c9aca-5d92-4b0d-8dda-73173d85db40",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "a459519d-7515-4302-8390-f9d3da7fb827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "b14960e1-b361-4a3e-a597-572b5b9c0dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a459519d-7515-4302-8390-f9d3da7fb827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6da37548-8cb0-4efe-94a8-a2ce1a8ae887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a459519d-7515-4302-8390-f9d3da7fb827",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "d7fc26ed-71d4-4588-a01d-85e1febe333d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "3d87df61-3569-4534-a380-9d98b6b91cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7fc26ed-71d4-4588-a01d-85e1febe333d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad6e62f9-9279-4bf3-8b09-c5c59f9b3b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7fc26ed-71d4-4588-a01d-85e1febe333d",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "74a452de-b1fd-4ccf-b713-46ba45783201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "5b5da398-041d-4b8b-80d6-47eda449920e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74a452de-b1fd-4ccf-b713-46ba45783201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ca3e01c-176d-430e-9fbb-d13594d81434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74a452de-b1fd-4ccf-b713-46ba45783201",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "15619971-74ab-4d0b-b879-b12458da1e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "49ebd6ca-644b-444a-a240-8ca247ab7de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15619971-74ab-4d0b-b879-b12458da1e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8193220a-b83f-45c2-b0de-6eff15fd3d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15619971-74ab-4d0b-b879-b12458da1e7f",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "7b060750-28fb-41eb-9901-0fc1a6bbe2fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "1f90260b-daac-42cc-a7fa-fae300068095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b060750-28fb-41eb-9901-0fc1a6bbe2fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9314c3e-5a4c-4405-9b1a-e44afd4f8cd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b060750-28fb-41eb-9901-0fc1a6bbe2fd",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "3427fe3d-8b38-4afd-97d8-fab9540234a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "ec242872-b02f-4b2c-bc76-0405c88a02e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3427fe3d-8b38-4afd-97d8-fab9540234a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bf89b1b-3981-4291-b7fe-79abed567f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3427fe3d-8b38-4afd-97d8-fab9540234a4",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "43a3c8f4-edb2-443d-898f-dcc31932e6e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "dc19b4e2-e8c4-4a81-ac56-effc672a2aeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a3c8f4-edb2-443d-898f-dcc31932e6e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b3e4635-83db-46b0-8422-d8c03658f8a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a3c8f4-edb2-443d-898f-dcc31932e6e5",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "1fee10a0-0188-4c4e-87a3-5afb5ba59c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "e447efbb-2992-496d-8c56-f1ff2c6674b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fee10a0-0188-4c4e-87a3-5afb5ba59c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a1b9502-68a7-44a8-9d6c-acb176e2a2e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fee10a0-0188-4c4e-87a3-5afb5ba59c14",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "f6d74d03-02ef-4643-a009-5d2cc283b1bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "0af5a765-3435-4dd2-8e6b-c38d0dc68cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d74d03-02ef-4643-a009-5d2cc283b1bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25be8f49-3e43-47c4-8b2f-163333127cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d74d03-02ef-4643-a009-5d2cc283b1bf",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "92070a6d-c27a-4936-b38f-21a92bec0908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "34307321-10d4-4527-8ec1-2b8421a88107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92070a6d-c27a-4936-b38f-21a92bec0908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44302e7-c035-4be9-bdef-eb0937f6d6c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92070a6d-c27a-4936-b38f-21a92bec0908",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        },
        {
            "id": "f1503437-4071-49b4-abc2-b46d8b81032e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "compositeImage": {
                "id": "1b3970dc-bcb9-40a9-a39f-18cb9ddecf0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1503437-4071-49b4-abc2-b46d8b81032e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca584e55-1eef-40c5-adbd-f2ffbeb66a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1503437-4071-49b4-abc2-b46d8b81032e",
                    "LayerId": "28ad2492-6d74-42c9-9245-ade60fa2a5ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "28ad2492-6d74-42c9-9245-ade60fa2a5ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d365a3b-67de-4086-ab2f-a87ab531439c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4294901889,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}