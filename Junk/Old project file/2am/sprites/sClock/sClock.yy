{
    "id": "467e9a28-61f7-4651-8610-3906f79210da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8d6ac0a-cd5f-4dcb-b10a-1ce0df370f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "467e9a28-61f7-4651-8610-3906f79210da",
            "compositeImage": {
                "id": "b8abe3c3-047a-4a91-8c9e-301a86bfb5f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d6ac0a-cd5f-4dcb-b10a-1ce0df370f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "563f1b57-b703-4c1b-a22d-bd7d51e96c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d6ac0a-cd5f-4dcb-b10a-1ce0df370f87",
                    "LayerId": "a796d78b-a599-4d28-ba8f-1fc528147f5f"
                },
                {
                    "id": "830de161-aa1b-4d18-9297-9d46f0081f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d6ac0a-cd5f-4dcb-b10a-1ce0df370f87",
                    "LayerId": "2f802c48-924b-4fb9-b947-3effd6d761a0"
                }
            ]
        },
        {
            "id": "b216d1e1-a81d-49b4-9764-01368eef8a22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "467e9a28-61f7-4651-8610-3906f79210da",
            "compositeImage": {
                "id": "6b53c3a5-9a50-4695-a5ba-b8828cb3789b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b216d1e1-a81d-49b4-9764-01368eef8a22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd073cc8-51eb-4a13-91cc-ed6561ec67b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b216d1e1-a81d-49b4-9764-01368eef8a22",
                    "LayerId": "a796d78b-a599-4d28-ba8f-1fc528147f5f"
                },
                {
                    "id": "3f5e8a7c-9814-4ab0-99a6-6823402b1f00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b216d1e1-a81d-49b4-9764-01368eef8a22",
                    "LayerId": "2f802c48-924b-4fb9-b947-3effd6d761a0"
                }
            ]
        },
        {
            "id": "58e78144-b8d4-473c-ac88-763c74c3071b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "467e9a28-61f7-4651-8610-3906f79210da",
            "compositeImage": {
                "id": "67285ae8-e299-4bd6-85bf-f3aec68911d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e78144-b8d4-473c-ac88-763c74c3071b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffb50cc9-42ff-4bc8-b3e8-37ce44af1da7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e78144-b8d4-473c-ac88-763c74c3071b",
                    "LayerId": "a796d78b-a599-4d28-ba8f-1fc528147f5f"
                },
                {
                    "id": "41cdea44-3934-4edf-a821-dfe692470109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e78144-b8d4-473c-ac88-763c74c3071b",
                    "LayerId": "2f802c48-924b-4fb9-b947-3effd6d761a0"
                }
            ]
        },
        {
            "id": "97c3b542-074e-4386-a6e1-a79f7acb6ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "467e9a28-61f7-4651-8610-3906f79210da",
            "compositeImage": {
                "id": "84f57453-21c1-405c-8db0-1888b7f1557f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97c3b542-074e-4386-a6e1-a79f7acb6ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5446e8-d69b-49ba-9ab3-0ba1c6915708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c3b542-074e-4386-a6e1-a79f7acb6ab3",
                    "LayerId": "a796d78b-a599-4d28-ba8f-1fc528147f5f"
                },
                {
                    "id": "60ec84aa-882c-4a90-bd1e-4ecdfe2993f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c3b542-074e-4386-a6e1-a79f7acb6ab3",
                    "LayerId": "2f802c48-924b-4fb9-b947-3effd6d761a0"
                }
            ]
        },
        {
            "id": "4d4faed2-33e7-4f80-8909-609dcbbc90c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "467e9a28-61f7-4651-8610-3906f79210da",
            "compositeImage": {
                "id": "4b3fa60c-1739-40f1-991d-acde58cc2d3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d4faed2-33e7-4f80-8909-609dcbbc90c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd6bea5a-d8c9-4654-bc36-eaa33935487d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d4faed2-33e7-4f80-8909-609dcbbc90c3",
                    "LayerId": "a796d78b-a599-4d28-ba8f-1fc528147f5f"
                },
                {
                    "id": "4c93f754-912b-4408-aad6-eeb8325bd684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d4faed2-33e7-4f80-8909-609dcbbc90c3",
                    "LayerId": "2f802c48-924b-4fb9-b947-3effd6d761a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2f802c48-924b-4fb9-b947-3effd6d761a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "467e9a28-61f7-4651-8610-3906f79210da",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 25,
            "visible": true
        },
        {
            "id": "a796d78b-a599-4d28-ba8f-1fc528147f5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "467e9a28-61f7-4651-8610-3906f79210da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        1090519039,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}