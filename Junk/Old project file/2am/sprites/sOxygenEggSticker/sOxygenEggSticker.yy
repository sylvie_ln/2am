{
    "id": "a361b142-51e9-4b74-9734-baf1dd471cb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOxygenEggSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c085c061-9e9f-4a79-bef2-91db4e20b8e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a361b142-51e9-4b74-9734-baf1dd471cb0",
            "compositeImage": {
                "id": "cde694a5-7e7d-4f85-ab52-5a8f67cbcde6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c085c061-9e9f-4a79-bef2-91db4e20b8e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5459abce-b399-4459-8fb2-9e90fb662cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c085c061-9e9f-4a79-bef2-91db4e20b8e7",
                    "LayerId": "9860acca-2caf-466a-bfa3-309d950bc196"
                }
            ]
        },
        {
            "id": "1fa41755-6e12-446d-88fd-d745326daa37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a361b142-51e9-4b74-9734-baf1dd471cb0",
            "compositeImage": {
                "id": "d3a8a0ce-a606-4c82-b79b-0399c2f2f7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fa41755-6e12-446d-88fd-d745326daa37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d334ca8-0bf1-4d74-a7e6-6190d00f04ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fa41755-6e12-446d-88fd-d745326daa37",
                    "LayerId": "9860acca-2caf-466a-bfa3-309d950bc196"
                }
            ]
        },
        {
            "id": "92a3bbf5-9c06-4dce-9240-28e8f9aa517d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a361b142-51e9-4b74-9734-baf1dd471cb0",
            "compositeImage": {
                "id": "37cba578-e21f-484d-bd71-ed9157902abf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92a3bbf5-9c06-4dce-9240-28e8f9aa517d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8049cb6b-d88c-4ff0-aee9-00327f037dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92a3bbf5-9c06-4dce-9240-28e8f9aa517d",
                    "LayerId": "9860acca-2caf-466a-bfa3-309d950bc196"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9860acca-2caf-466a-bfa3-309d950bc196",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a361b142-51e9-4b74-9734-baf1dd471cb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}