{
    "id": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStatue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14a5acf7-2529-40e1-af6d-d3bfeb9ccb07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "compositeImage": {
                "id": "b77d751b-b076-4bc4-bdc9-c149b35684ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14a5acf7-2529-40e1-af6d-d3bfeb9ccb07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f063d789-0e77-486c-a632-b537cdb5746c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14a5acf7-2529-40e1-af6d-d3bfeb9ccb07",
                    "LayerId": "51961895-71c6-4a30-aa92-625dd0e1153f"
                }
            ]
        },
        {
            "id": "ad036d42-6223-47a5-9ec3-fa52e73f0fea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "compositeImage": {
                "id": "79e5f3e6-ebca-42ad-b43a-4b6db7e72860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad036d42-6223-47a5-9ec3-fa52e73f0fea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc24f34f-fe41-4bb9-b86b-2f87f066a655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad036d42-6223-47a5-9ec3-fa52e73f0fea",
                    "LayerId": "51961895-71c6-4a30-aa92-625dd0e1153f"
                }
            ]
        },
        {
            "id": "ed7f4ba6-b503-4df4-a502-aeeb48147004",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "compositeImage": {
                "id": "6fa86aa2-cc6c-485c-b1b0-61ec1a361fb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed7f4ba6-b503-4df4-a502-aeeb48147004",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ac463a3-3f4b-45a3-ac76-fd0ba64765b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed7f4ba6-b503-4df4-a502-aeeb48147004",
                    "LayerId": "51961895-71c6-4a30-aa92-625dd0e1153f"
                }
            ]
        },
        {
            "id": "39e703ab-1782-47eb-bdc1-1ed569036e9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "compositeImage": {
                "id": "195d266a-71c1-4ad4-a0a5-394213588554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e703ab-1782-47eb-bdc1-1ed569036e9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7b7093-6568-4632-9f6c-5fa8b6d1841b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e703ab-1782-47eb-bdc1-1ed569036e9d",
                    "LayerId": "51961895-71c6-4a30-aa92-625dd0e1153f"
                }
            ]
        },
        {
            "id": "96f8dadf-3d1a-42a6-9ea2-4e85f2a5e3c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "compositeImage": {
                "id": "70d986ed-818c-42da-a339-c326583d7322",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96f8dadf-3d1a-42a6-9ea2-4e85f2a5e3c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "627ba985-55f6-4deb-ac51-253c33e09df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96f8dadf-3d1a-42a6-9ea2-4e85f2a5e3c9",
                    "LayerId": "51961895-71c6-4a30-aa92-625dd0e1153f"
                }
            ]
        },
        {
            "id": "4abeab4c-4256-4812-8ee7-d1ce90b1abbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "compositeImage": {
                "id": "d8d480f3-0e57-4a6e-a735-69ef5644aac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4abeab4c-4256-4812-8ee7-d1ce90b1abbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "882aff35-9c1e-44a6-b211-17c777c301c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4abeab4c-4256-4812-8ee7-d1ce90b1abbb",
                    "LayerId": "51961895-71c6-4a30-aa92-625dd0e1153f"
                }
            ]
        },
        {
            "id": "c0c1e218-d81f-4046-b443-3217e973f5b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "compositeImage": {
                "id": "d3b3a7b9-4956-4617-920f-c18ef00ff858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c1e218-d81f-4046-b443-3217e973f5b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ae1994-0a93-4910-a272-aa4f7413ec21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c1e218-d81f-4046-b443-3217e973f5b3",
                    "LayerId": "51961895-71c6-4a30-aa92-625dd0e1153f"
                }
            ]
        },
        {
            "id": "b228aea3-f972-4a8f-814b-83fb6d50435f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "compositeImage": {
                "id": "83d73f6f-28a1-476e-b77c-822e00d79bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b228aea3-f972-4a8f-814b-83fb6d50435f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab720f4-00dd-474c-a523-4b2490e928d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b228aea3-f972-4a8f-814b-83fb6d50435f",
                    "LayerId": "51961895-71c6-4a30-aa92-625dd0e1153f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "51961895-71c6-4a30-aa92-625dd0e1153f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "316c5428-8c2e-4cf8-a6b5-a1b0637be46a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}