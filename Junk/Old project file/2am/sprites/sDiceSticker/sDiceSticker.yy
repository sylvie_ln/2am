{
    "id": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDiceSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77308945-3a5e-4839-87c1-d4affd391f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "728667da-f6b0-44e3-956c-f6e5cb4e9c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77308945-3a5e-4839-87c1-d4affd391f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e77c1f6-2713-4c45-ac72-9694a7d20665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77308945-3a5e-4839-87c1-d4affd391f36",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "907b36c2-4474-482f-8ca9-6b64e5f39897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "c835e405-a876-41c9-a54c-bee072127338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "907b36c2-4474-482f-8ca9-6b64e5f39897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48dd8ba1-d01f-4529-9c0c-ede3156a5e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "907b36c2-4474-482f-8ca9-6b64e5f39897",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "3038a9d2-a1b6-471f-b669-0911bf0b575a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "16606a9e-eb07-4ae8-af9d-d5f2198f982c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3038a9d2-a1b6-471f-b669-0911bf0b575a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eaa1879-425c-4419-bec9-77144de5a4fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3038a9d2-a1b6-471f-b669-0911bf0b575a",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "c62d6d59-77d3-4e7c-9681-cf4426f318d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "6de134da-cd70-475f-bce6-7835d51475a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c62d6d59-77d3-4e7c-9681-cf4426f318d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf932555-469b-41da-97c8-7780c67d3ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c62d6d59-77d3-4e7c-9681-cf4426f318d4",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "0ada7144-2cda-4e03-9005-5997936bb93c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "df28a46d-3c48-4b8c-91b2-870c985e213a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ada7144-2cda-4e03-9005-5997936bb93c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c333b26f-4218-412c-9a69-1f7a6a83a7c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ada7144-2cda-4e03-9005-5997936bb93c",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "d2996aeb-751d-436f-a767-40e2c14e5b92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "01fb94a2-b869-47a6-8350-7bcad4a2e06b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2996aeb-751d-436f-a767-40e2c14e5b92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49aa388-ed20-47d6-b026-073f28f5a04b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2996aeb-751d-436f-a767-40e2c14e5b92",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "fb7ee375-c422-4e54-a3ff-a27657844ab1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "8eeb7f2b-fceb-4132-add5-a910043ecd4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb7ee375-c422-4e54-a3ff-a27657844ab1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d482beb0-42bd-4185-8920-4cd92d186c30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb7ee375-c422-4e54-a3ff-a27657844ab1",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "3478a8f2-bf92-4b20-95d0-fd8fa02c4af8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "139d2d43-5082-4684-bcb9-61f5bc1fd15e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3478a8f2-bf92-4b20-95d0-fd8fa02c4af8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf79489-5388-40ae-b638-5f44238396f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3478a8f2-bf92-4b20-95d0-fd8fa02c4af8",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "eb8f9e64-6b66-42e4-a96d-7133054bb60b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "9f28d126-6ced-4eed-b3d2-a0ecf7bb39fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb8f9e64-6b66-42e4-a96d-7133054bb60b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6fb2dce-054b-4cc7-9390-fc9a6da1cf37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb8f9e64-6b66-42e4-a96d-7133054bb60b",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "1f67c637-418a-45d1-a7c7-41f4e7cf44e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "06ea5a0b-ff98-4c43-b52e-4a850fdc3c02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f67c637-418a-45d1-a7c7-41f4e7cf44e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38d67576-3f60-4c1e-8c1a-000767a1dbac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f67c637-418a-45d1-a7c7-41f4e7cf44e6",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "85a36ff9-4d8a-4399-9d38-2d129c8708fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "d48da4b8-aec3-439e-b491-adca2f679910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a36ff9-4d8a-4399-9d38-2d129c8708fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f5d5c46-0b3f-4e5c-bd57-c91acadfacb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a36ff9-4d8a-4399-9d38-2d129c8708fe",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        },
        {
            "id": "c08b526e-1cf5-4300-bed1-5192d6c2d3a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "compositeImage": {
                "id": "bc11ec50-9d6d-4418-80fb-4d82aa95f976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08b526e-1cf5-4300-bed1-5192d6c2d3a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a2e4f48-df72-4693-a1d2-8ccb27e6d522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08b526e-1cf5-4300-bed1-5192d6c2d3a6",
                    "LayerId": "696b7f64-5e3a-448c-814f-bc542259dd0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "696b7f64-5e3a-448c-814f-bc542259dd0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41c0dc1a-ef60-4a35-9066-362a4bf4cda8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}