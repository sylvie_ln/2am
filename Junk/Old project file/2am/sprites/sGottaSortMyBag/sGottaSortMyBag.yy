{
    "id": "8e41dcd4-7ce9-4548-8cb7-0812d1370a90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGottaSortMyBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 7,
    "bbox_right": 313,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cd944c3-3baa-4e83-a3bd-ed8d8e34fe7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e41dcd4-7ce9-4548-8cb7-0812d1370a90",
            "compositeImage": {
                "id": "3dd4210e-c637-4a2e-9981-77d0de203b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cd944c3-3baa-4e83-a3bd-ed8d8e34fe7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88748298-b137-45ad-99dd-1e3e0069d77c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cd944c3-3baa-4e83-a3bd-ed8d8e34fe7a",
                    "LayerId": "74fa5533-cb1f-48c7-82e1-8b2636d0525f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "74fa5533-cb1f-48c7-82e1-8b2636d0525f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e41dcd4-7ce9-4548-8cb7-0812d1370a90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}