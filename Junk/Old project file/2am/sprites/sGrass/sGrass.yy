{
    "id": "a05bd222-33c7-4f6e-a8d6-86169d28e329",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "752d4f92-7a8a-487e-93b7-4d0ffca02734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05bd222-33c7-4f6e-a8d6-86169d28e329",
            "compositeImage": {
                "id": "4ae96874-d6ed-4206-9812-e455badfe7c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "752d4f92-7a8a-487e-93b7-4d0ffca02734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fdb43c4-18df-4190-a528-5da3254f8f12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "752d4f92-7a8a-487e-93b7-4d0ffca02734",
                    "LayerId": "419e4105-8194-4495-a500-ec21e87bbc3f"
                }
            ]
        },
        {
            "id": "f4740fa9-57a3-45e9-81ff-40355e3fdde5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05bd222-33c7-4f6e-a8d6-86169d28e329",
            "compositeImage": {
                "id": "c13223c1-3f46-49af-8791-a29162718e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4740fa9-57a3-45e9-81ff-40355e3fdde5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6c7128-3fd6-4f25-b349-4ea64a211f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4740fa9-57a3-45e9-81ff-40355e3fdde5",
                    "LayerId": "419e4105-8194-4495-a500-ec21e87bbc3f"
                }
            ]
        },
        {
            "id": "f391e82c-350a-4fb1-ae15-2b8a7237cb16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05bd222-33c7-4f6e-a8d6-86169d28e329",
            "compositeImage": {
                "id": "7b1622c0-7310-4314-9136-92afc75ba66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f391e82c-350a-4fb1-ae15-2b8a7237cb16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7f8936-d092-4252-99c4-5dfc1d520d35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f391e82c-350a-4fb1-ae15-2b8a7237cb16",
                    "LayerId": "419e4105-8194-4495-a500-ec21e87bbc3f"
                }
            ]
        },
        {
            "id": "1fe45ed4-59f1-4a1d-a8c8-5d82c55fbdee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05bd222-33c7-4f6e-a8d6-86169d28e329",
            "compositeImage": {
                "id": "12c49927-893b-4164-9a44-e7c97f4a869a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fe45ed4-59f1-4a1d-a8c8-5d82c55fbdee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e75efbdb-6ec8-4ec8-8a4f-9b493f99d1cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fe45ed4-59f1-4a1d-a8c8-5d82c55fbdee",
                    "LayerId": "419e4105-8194-4495-a500-ec21e87bbc3f"
                }
            ]
        },
        {
            "id": "e96a1951-23a4-4611-a766-f8717fce3eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05bd222-33c7-4f6e-a8d6-86169d28e329",
            "compositeImage": {
                "id": "07861170-0fe5-4671-8e04-8e0abcd3395f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96a1951-23a4-4611-a766-f8717fce3eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c7cdd4-c866-4c9e-a4ed-69cf6df21cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96a1951-23a4-4611-a766-f8717fce3eef",
                    "LayerId": "419e4105-8194-4495-a500-ec21e87bbc3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "419e4105-8194-4495-a500-ec21e87bbc3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a05bd222-33c7-4f6e-a8d6-86169d28e329",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}