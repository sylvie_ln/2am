{
    "id": "98bcd1a7-d2c4-4f23-bdbc-86830e2c3a19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoomBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "898d6cab-752f-42b2-be66-10d95d0e6577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98bcd1a7-d2c4-4f23-bdbc-86830e2c3a19",
            "compositeImage": {
                "id": "2a92e287-5cbe-4a99-aedd-e239a74d2cb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "898d6cab-752f-42b2-be66-10d95d0e6577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfef955e-fb18-4d4e-920e-af8c9fe27218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "898d6cab-752f-42b2-be66-10d95d0e6577",
                    "LayerId": "e55de914-c73b-4d46-aa00-62424c4168f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e55de914-c73b-4d46-aa00-62424c4168f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98bcd1a7-d2c4-4f23-bdbc-86830e2c3a19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}