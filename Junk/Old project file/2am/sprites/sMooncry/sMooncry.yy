{
    "id": "643b5b30-1b66-48b1-9198-1b7919934c0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMooncry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93605f0a-c766-40f2-8fb5-84bce5f9d1fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "643b5b30-1b66-48b1-9198-1b7919934c0b",
            "compositeImage": {
                "id": "a7304c92-de93-4491-8ea8-0228cfb99765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93605f0a-c766-40f2-8fb5-84bce5f9d1fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30dfc6f5-4b5a-4116-9bdc-e42b4c363747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93605f0a-c766-40f2-8fb5-84bce5f9d1fa",
                    "LayerId": "f811b05d-fec6-47d7-99fe-3d142b6a132a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f811b05d-fec6-47d7-99fe-3d142b6a132a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "643b5b30-1b66-48b1-9198-1b7919934c0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}