{
    "id": "3efd235b-f4c5-4753-abac-6c0a44a7da39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRedCat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc651a37-1607-4570-b8ed-c6a09ee2dc22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3efd235b-f4c5-4753-abac-6c0a44a7da39",
            "compositeImage": {
                "id": "461be642-dca2-4728-b303-77e6556cfb9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc651a37-1607-4570-b8ed-c6a09ee2dc22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77821eb8-2757-4900-8760-7b43d9311f8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc651a37-1607-4570-b8ed-c6a09ee2dc22",
                    "LayerId": "7e120135-7474-4e59-a0bc-d98342b28fc1"
                }
            ]
        },
        {
            "id": "d2490ed2-7897-4410-bdac-0121405e2268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3efd235b-f4c5-4753-abac-6c0a44a7da39",
            "compositeImage": {
                "id": "49f6cad0-4edf-4ed4-aae6-6226465dd4b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2490ed2-7897-4410-bdac-0121405e2268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a93d87d0-d037-4168-9278-57a231873169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2490ed2-7897-4410-bdac-0121405e2268",
                    "LayerId": "7e120135-7474-4e59-a0bc-d98342b28fc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7e120135-7474-4e59-a0bc-d98342b28fc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3efd235b-f4c5-4753-abac-6c0a44a7da39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}