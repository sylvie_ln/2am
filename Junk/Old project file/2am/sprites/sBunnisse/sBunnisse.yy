{
    "id": "dc12aa57-27a3-43e2-8e18-4e2eaad6a558",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunnisse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebc94aaf-bfff-4ed0-9448-65a0efc31a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc12aa57-27a3-43e2-8e18-4e2eaad6a558",
            "compositeImage": {
                "id": "6537a481-4840-4b77-841b-689d944b4a74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc94aaf-bfff-4ed0-9448-65a0efc31a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b41e8698-9ed6-4927-b554-527f6fd9f5f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc94aaf-bfff-4ed0-9448-65a0efc31a38",
                    "LayerId": "924d5a5f-b0b3-4486-964e-74cc7a435ba5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "924d5a5f-b0b3-4486-964e-74cc7a435ba5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc12aa57-27a3-43e2-8e18-4e2eaad6a558",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}