{
    "id": "54f4a4c2-17d4-4597-9665-039d2da222da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCavey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "947cf724-252d-4868-9459-1689296dbc53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54f4a4c2-17d4-4597-9665-039d2da222da",
            "compositeImage": {
                "id": "87935e7c-7283-43d5-ba0c-30420921bc27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "947cf724-252d-4868-9459-1689296dbc53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e18ebd49-d082-4dfe-9c99-b60f94be7de6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "947cf724-252d-4868-9459-1689296dbc53",
                    "LayerId": "3acc38a9-574c-4f81-960c-0272298f841b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3acc38a9-574c-4f81-960c-0272298f841b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54f4a4c2-17d4-4597-9665-039d2da222da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}