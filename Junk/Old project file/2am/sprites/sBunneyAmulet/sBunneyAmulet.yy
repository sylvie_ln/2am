{
    "id": "e8c6f6dd-9217-4324-9ad0-3379e2b0ef33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunneyAmulet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "486918e9-4571-42ee-a03a-1cbf5f8fe8fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c6f6dd-9217-4324-9ad0-3379e2b0ef33",
            "compositeImage": {
                "id": "8f152827-71e7-4627-a4b6-af059294667a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "486918e9-4571-42ee-a03a-1cbf5f8fe8fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1523b21-fb75-4dc9-91b1-c6cdbcdd1e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "486918e9-4571-42ee-a03a-1cbf5f8fe8fb",
                    "LayerId": "40d795d1-58ef-4ede-b4bd-8dfcb36ca053"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "40d795d1-58ef-4ede-b4bd-8dfcb36ca053",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8c6f6dd-9217-4324-9ad0-3379e2b0ef33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}