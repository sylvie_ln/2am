{
    "id": "6611633b-bee8-4678-b878-05536c6e0d5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCherrySky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05187416-ffd5-412b-8e97-288d0aaac85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6611633b-bee8-4678-b878-05536c6e0d5a",
            "compositeImage": {
                "id": "37dbafaf-ea81-4de7-b5a4-7bbc6c845821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05187416-ffd5-412b-8e97-288d0aaac85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9492b6-a443-48d6-a93a-c466b24ec635",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05187416-ffd5-412b-8e97-288d0aaac85b",
                    "LayerId": "0531d4d4-b862-4fe6-8d70-39a88e17c0df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0531d4d4-b862-4fe6-8d70-39a88e17c0df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6611633b-bee8-4678-b878-05536c6e0d5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}