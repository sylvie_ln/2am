{
    "id": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEivlysBow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 8,
    "bbox_right": 42,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23a09267-da80-4c14-9406-3314e2e6812a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "5ca0d30b-a87d-4802-99dc-3ff6b430a07d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a09267-da80-4c14-9406-3314e2e6812a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "019ef148-1b26-4360-80ec-6866f819ae60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a09267-da80-4c14-9406-3314e2e6812a",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "ce5d76fa-087e-44a2-ad57-8c096e80c8f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "1bf955bc-f4dd-483b-8bbe-9f32c4fd1f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce5d76fa-087e-44a2-ad57-8c096e80c8f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4848e4-ed53-480b-86e8-6bc96e5615ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce5d76fa-087e-44a2-ad57-8c096e80c8f9",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "c80d74ee-2bf6-4e07-a817-ba1edec11f7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "0c09a94d-adb3-47ac-a37b-f7f94e196664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c80d74ee-2bf6-4e07-a817-ba1edec11f7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "229139c2-6a41-436b-998e-471720daa8f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c80d74ee-2bf6-4e07-a817-ba1edec11f7f",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "33df5b41-6c86-4eca-a18a-8cd9f4019231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "7bb6a09d-eee7-48a5-98f3-c9678d1bbe70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33df5b41-6c86-4eca-a18a-8cd9f4019231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c654d2-a0c9-497f-97ad-44cd1de951ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33df5b41-6c86-4eca-a18a-8cd9f4019231",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "2f888a69-7872-44e4-ae56-e803849f82a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "e007dafb-d526-4130-8011-5c6016c0d2c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f888a69-7872-44e4-ae56-e803849f82a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8064569e-de08-434e-8286-ada1072fd27f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f888a69-7872-44e4-ae56-e803849f82a4",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "8b9266c6-2ab0-4b94-8478-be63e6b313d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "bf8b94ff-922c-4324-a21b-8f19eadbff30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b9266c6-2ab0-4b94-8478-be63e6b313d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af9bc03c-3b32-459e-ad3a-bea145225fa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b9266c6-2ab0-4b94-8478-be63e6b313d6",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "08a139c0-2fe2-43ba-9da4-c92878c9c04e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "2a721c47-3f29-4235-93e8-3a3cca1bf8e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08a139c0-2fe2-43ba-9da4-c92878c9c04e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6eb17f4-c52d-48a9-952b-6d606049642f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08a139c0-2fe2-43ba-9da4-c92878c9c04e",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "b99105f0-4f92-4e45-b530-d2c6a36fc231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "dff976fb-7db1-4f49-a695-99bb2630cc55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b99105f0-4f92-4e45-b530-d2c6a36fc231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb88070-11bd-417a-8927-02b10dde7ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b99105f0-4f92-4e45-b530-d2c6a36fc231",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "1f061818-f691-4ac9-9722-ea9415121e88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "35db5a4e-a646-4b0f-b01f-7648ac7958f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f061818-f691-4ac9-9722-ea9415121e88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2339e7c6-31b7-44c0-8eae-5fc10f2c6706",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f061818-f691-4ac9-9722-ea9415121e88",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        },
        {
            "id": "9f32b48e-9d5a-4ab9-9a67-c031e083a103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "compositeImage": {
                "id": "a6dd0fef-2fbe-4c3d-9614-3f56aa8fe3bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f32b48e-9d5a-4ab9-9a67-c031e083a103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfeedf54-7f97-4cb8-b885-48d511c319e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f32b48e-9d5a-4ab9-9a67-c031e083a103",
                    "LayerId": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "610dc3f8-b84c-48a6-85e7-4860bd6c68ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d13a2bb4-845e-4c0b-8ba3-28ad0b2b84b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 19,
    "yorig": 27
}