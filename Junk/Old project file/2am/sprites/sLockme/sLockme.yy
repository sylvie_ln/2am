{
    "id": "e7a2fabb-d51a-47e3-8f5c-5423b7455d39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLockme",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa1dc43c-07a4-450c-9db2-ad5401c5a00c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7a2fabb-d51a-47e3-8f5c-5423b7455d39",
            "compositeImage": {
                "id": "6e2a534b-3fbf-43fa-9b03-55c0748c606f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa1dc43c-07a4-450c-9db2-ad5401c5a00c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f835be86-c8d6-4968-b58a-6ce328744d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa1dc43c-07a4-450c-9db2-ad5401c5a00c",
                    "LayerId": "52486f93-1c01-436d-b905-3ca36a9fd70f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "52486f93-1c01-436d-b905-3ca36a9fd70f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7a2fabb-d51a-47e3-8f5c-5423b7455d39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 9,
    "yorig": 8
}