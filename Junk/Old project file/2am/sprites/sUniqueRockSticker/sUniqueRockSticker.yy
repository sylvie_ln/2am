{
    "id": "88b793ac-3a3e-4168-8c5f-b8ba390b7793",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUniqueRockSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3832b7ce-51bc-46e8-baee-a7c6feac460f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b793ac-3a3e-4168-8c5f-b8ba390b7793",
            "compositeImage": {
                "id": "5a8966b5-7c98-4aee-95bb-8f46e6b3d940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3832b7ce-51bc-46e8-baee-a7c6feac460f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9cc9b28-62ee-44d3-92e2-0171ef78fc58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3832b7ce-51bc-46e8-baee-a7c6feac460f",
                    "LayerId": "a4b32343-51b0-4834-be47-6ffca277663e"
                },
                {
                    "id": "7ea89440-9a36-4613-9d9b-436069b51c92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3832b7ce-51bc-46e8-baee-a7c6feac460f",
                    "LayerId": "3c6f317a-7540-4a72-8e0b-f1e6a7b66621"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a4b32343-51b0-4834-be47-6ffca277663e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88b793ac-3a3e-4168-8c5f-b8ba390b7793",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3c6f317a-7540-4a72-8e0b-f1e6a7b66621",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88b793ac-3a3e-4168-8c5f-b8ba390b7793",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}