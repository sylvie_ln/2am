{
    "id": "acb5aaa4-8b12-4919-8d50-75c5b6348243",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMagicCarrotSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "651e0783-63f7-467f-92b3-789661540d42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acb5aaa4-8b12-4919-8d50-75c5b6348243",
            "compositeImage": {
                "id": "2ca302d7-5b85-48e9-9e9b-36031e818706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "651e0783-63f7-467f-92b3-789661540d42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767e06aa-5ce6-479f-be38-88d67df6355e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "651e0783-63f7-467f-92b3-789661540d42",
                    "LayerId": "4fb57828-1393-4030-99ab-7a7d286a0acd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4fb57828-1393-4030-99ab-7a7d286a0acd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acb5aaa4-8b12-4919-8d50-75c5b6348243",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}