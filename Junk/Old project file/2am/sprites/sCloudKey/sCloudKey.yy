{
    "id": "e7e67751-7eed-4db3-9fcc-6c895d325cc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloudKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05611880-f995-444c-9d3b-f8e695c676d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7e67751-7eed-4db3-9fcc-6c895d325cc2",
            "compositeImage": {
                "id": "afe24e5e-1c19-4b41-8569-664fef7f273b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05611880-f995-444c-9d3b-f8e695c676d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d9f38a1-f6ce-4c58-8e85-836af264b9e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05611880-f995-444c-9d3b-f8e695c676d8",
                    "LayerId": "21ee64b4-e762-4ff9-b01e-c3760938115c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "21ee64b4-e762-4ff9-b01e-c3760938115c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7e67751-7eed-4db3-9fcc-6c895d325cc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}