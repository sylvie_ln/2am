{
    "id": "6b6bd58c-1ef7-4794-ace2-fa205d5d74d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLargeTruckRideMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 1,
    "bbox_right": 46,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8598c6a9-712a-431b-b795-be3b629c9f0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b6bd58c-1ef7-4794-ace2-fa205d5d74d6",
            "compositeImage": {
                "id": "8dd6fb74-8374-43d9-ba96-f497d30edc25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8598c6a9-712a-431b-b795-be3b629c9f0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38a67c39-73b6-4089-b0b6-8c7aba0a4c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8598c6a9-712a-431b-b795-be3b629c9f0f",
                    "LayerId": "6f3c7fd8-9637-4044-8c43-ebfd39ebca4a"
                },
                {
                    "id": "c5128f49-d4c1-4852-84c4-fab65b7d9065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8598c6a9-712a-431b-b795-be3b629c9f0f",
                    "LayerId": "ebcf0b5b-bc90-4297-a09e-7473723a86bb"
                },
                {
                    "id": "d6e3a5fa-2e9d-4eda-9b7d-9b375763617e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8598c6a9-712a-431b-b795-be3b629c9f0f",
                    "LayerId": "fe7a9225-3ee4-49cf-a7b2-b10cc316fd70"
                },
                {
                    "id": "6e33c4f7-192c-4860-a65c-ef87e1d1b2fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8598c6a9-712a-431b-b795-be3b629c9f0f",
                    "LayerId": "2e1f1a98-c71c-4a2d-8d9f-f7c6afc01538"
                },
                {
                    "id": "12226434-8e94-4b9d-a55a-acc5df8451b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8598c6a9-712a-431b-b795-be3b629c9f0f",
                    "LayerId": "f38302b4-84ee-4c79-a78e-0586c2b0741c"
                },
                {
                    "id": "8965fddc-3579-49fd-814a-c9e291e69eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8598c6a9-712a-431b-b795-be3b629c9f0f",
                    "LayerId": "42f8ee24-f812-4a96-a4ee-011106a66efc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "42f8ee24-f812-4a96-a4ee-011106a66efc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b6bd58c-1ef7-4794-ace2-fa205d5d74d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6f3c7fd8-9637-4044-8c43-ebfd39ebca4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b6bd58c-1ef7-4794-ace2-fa205d5d74d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 25,
            "visible": false
        },
        {
            "id": "ebcf0b5b-bc90-4297-a09e-7473723a86bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b6bd58c-1ef7-4794-ace2-fa205d5d74d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 50,
            "visible": false
        },
        {
            "id": "fe7a9225-3ee4-49cf-a7b2-b10cc316fd70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b6bd58c-1ef7-4794-ace2-fa205d5d74d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 50,
            "visible": false
        },
        {
            "id": "2e1f1a98-c71c-4a2d-8d9f-f7c6afc01538",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b6bd58c-1ef7-4794-ace2-fa205d5d74d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "f38302b4-84ee-4c79-a78e-0586c2b0741c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b6bd58c-1ef7-4794-ace2-fa205d5d74d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4283190348,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 20
}