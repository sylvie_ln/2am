{
    "id": "cc21ceba-c7a5-438b-bd21-f1b9096c2359",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHorseRideMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d8232a9-fa6d-44c9-8942-9f9609706791",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc21ceba-c7a5-438b-bd21-f1b9096c2359",
            "compositeImage": {
                "id": "493d7698-72f3-437a-b863-4d4536faf11c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d8232a9-fa6d-44c9-8942-9f9609706791",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17aaa0ba-3525-4ded-9381-34e1de74c909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8232a9-fa6d-44c9-8942-9f9609706791",
                    "LayerId": "8e98cd2b-37e8-46d5-8d7f-02628a6fc36b"
                },
                {
                    "id": "fcc289c7-9c8b-464c-9392-cc77754a496b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8232a9-fa6d-44c9-8942-9f9609706791",
                    "LayerId": "16976ecf-2382-4cf1-9539-6ad27da69468"
                },
                {
                    "id": "3799ffd9-f9e1-497d-838a-b88896479ab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8232a9-fa6d-44c9-8942-9f9609706791",
                    "LayerId": "dc917ac3-8120-48b9-96e0-94758a184281"
                },
                {
                    "id": "a13200e4-d6f7-4c52-b037-8faf08cdd980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8232a9-fa6d-44c9-8942-9f9609706791",
                    "LayerId": "5a6ac85d-712c-4132-885e-54c77e13b3c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5a6ac85d-712c-4132-885e-54c77e13b3c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc21ceba-c7a5-438b-bd21-f1b9096c2359",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8e98cd2b-37e8-46d5-8d7f-02628a6fc36b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc21ceba-c7a5-438b-bd21-f1b9096c2359",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "16976ecf-2382-4cf1-9539-6ad27da69468",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc21ceba-c7a5-438b-bd21-f1b9096c2359",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "dc917ac3-8120-48b9-96e0-94758a184281",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc21ceba-c7a5-438b-bd21-f1b9096c2359",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}