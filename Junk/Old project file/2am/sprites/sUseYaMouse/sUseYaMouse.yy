{
    "id": "9ce77003-5802-4243-aea4-5ca9c1c9f5f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUseYaMouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 85,
    "bbox_right": 235,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37f3dd0d-90e8-4fa0-a077-928cc144f581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ce77003-5802-4243-aea4-5ca9c1c9f5f3",
            "compositeImage": {
                "id": "9cbfa523-cf06-4f4c-80c8-d4f103103c4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37f3dd0d-90e8-4fa0-a077-928cc144f581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "219ab69a-90b3-4bc0-834f-e912ae1e953d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f3dd0d-90e8-4fa0-a077-928cc144f581",
                    "LayerId": "091f8d3f-10eb-4265-9e98-97ef009d4027"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "091f8d3f-10eb-4265-9e98-97ef009d4027",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ce77003-5802-4243-aea4-5ca9c1c9f5f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 31
}