{
    "id": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConfigHatOLD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57de5ccf-b6d6-42f4-b6fc-b1205bf194f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "94251d16-584f-4e44-b5d3-f36ae7567503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57de5ccf-b6d6-42f4-b6fc-b1205bf194f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ca14426-396d-4b2c-b606-63dc7110307e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57de5ccf-b6d6-42f4-b6fc-b1205bf194f9",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "c96abe91-c749-4a59-a2cd-fdf08e0d9bf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "5ca1fe6c-17b2-460b-ad92-7e13c35d960f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c96abe91-c749-4a59-a2cd-fdf08e0d9bf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b376ea3a-9e5f-49ce-9d24-3a01d2954bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c96abe91-c749-4a59-a2cd-fdf08e0d9bf0",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "ff1a64b4-98d5-4c34-953b-027a563e144b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "011e17c0-ec39-468a-b9b5-193df4b08601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff1a64b4-98d5-4c34-953b-027a563e144b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "441d70a2-3b52-4bea-9af3-b17a9965c0fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff1a64b4-98d5-4c34-953b-027a563e144b",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "6d0269b6-b052-41ce-bec5-f60f4bf0789f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "e4cf3cce-a7c1-43af-9b14-038d493917fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d0269b6-b052-41ce-bec5-f60f4bf0789f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ab62cb4-4f9f-4cf8-b5f4-9344eeb82ff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d0269b6-b052-41ce-bec5-f60f4bf0789f",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "521f8725-9fa6-4f23-b49f-5fdf7cb803ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "a02e7519-b5a0-47f4-bd95-823c52c541b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "521f8725-9fa6-4f23-b49f-5fdf7cb803ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6628bc08-328c-4d86-91a9-375ed9e5c151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "521f8725-9fa6-4f23-b49f-5fdf7cb803ae",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "26edba24-8f57-4d81-98df-8aeebec84769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "23432b68-4a95-4996-a97d-4ed879f89b6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26edba24-8f57-4d81-98df-8aeebec84769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d929823-4519-4e6e-997c-de2271dccf62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26edba24-8f57-4d81-98df-8aeebec84769",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "5ebfa1ba-2538-45d9-99ec-d489e4dc2bde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "3e73f660-e1f6-4090-b75e-33a27c9dc839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ebfa1ba-2538-45d9-99ec-d489e4dc2bde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c73e29a2-6e91-4a5b-a397-bb52c990b8f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ebfa1ba-2538-45d9-99ec-d489e4dc2bde",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "1fe7b519-8e99-4bb9-9171-52a00857881f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "d0b54f35-82be-4820-879e-0d48f444f0c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fe7b519-8e99-4bb9-9171-52a00857881f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4283b86d-46d2-4350-9f5b-0accbe98c8fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fe7b519-8e99-4bb9-9171-52a00857881f",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "46200f50-9d52-40a8-a996-33af016242c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "68946210-b36d-4e2e-a128-25e0f54f3367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46200f50-9d52-40a8-a996-33af016242c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e047ea7a-4bb1-4c11-af3c-1558b57e1728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46200f50-9d52-40a8-a996-33af016242c1",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "e01cdb82-8018-4e94-9a4e-54ef660681ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "a5390711-842c-47d5-99c1-a0c290eff3ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e01cdb82-8018-4e94-9a4e-54ef660681ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72824202-8fe6-489b-bafb-7deb6f1cd653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e01cdb82-8018-4e94-9a4e-54ef660681ed",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "728dba6b-0eaa-4f9b-adea-163aa24b5649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "75f243f8-99db-4f85-ae47-671bc20d6f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "728dba6b-0eaa-4f9b-adea-163aa24b5649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d8d855c-b4ba-4e75-a768-3154667b69b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "728dba6b-0eaa-4f9b-adea-163aa24b5649",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "ef818567-63db-4a08-920d-dbd925c345f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "c7311195-522b-48a1-8033-5394d745dc3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef818567-63db-4a08-920d-dbd925c345f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbb86b56-a7d0-4ae1-9007-a2d49293a28c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef818567-63db-4a08-920d-dbd925c345f4",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "1396cb6e-8842-4d43-9e75-139f74f30fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "3b5baad0-ef1f-4d2f-9c38-715d4d4cadc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1396cb6e-8842-4d43-9e75-139f74f30fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f67beefe-0b44-4aee-a9b2-7a2e650af8d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1396cb6e-8842-4d43-9e75-139f74f30fde",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "e251ed08-c60c-43a7-a741-f7f4e06d921d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "6b68aa09-390c-4c98-a5b7-486dc8f238b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e251ed08-c60c-43a7-a741-f7f4e06d921d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34ad8cd1-47fd-47a6-84e6-f41c06cf9afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e251ed08-c60c-43a7-a741-f7f4e06d921d",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "6478f765-c07c-4cc8-b384-783ba085ad29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "e71963aa-40fa-4888-9a69-49b391fb1598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6478f765-c07c-4cc8-b384-783ba085ad29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a889ade4-63a0-421c-a2bc-7a2ff6897e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6478f765-c07c-4cc8-b384-783ba085ad29",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        },
        {
            "id": "2e1f4a98-6473-4a76-9a4d-cf8b9ddcc2cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "compositeImage": {
                "id": "0a9ec7da-4fc9-4f88-838e-46d228a37f33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e1f4a98-6473-4a76-9a4d-cf8b9ddcc2cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08a2955d-e724-4be1-b3d3-bf0fc763126c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e1f4a98-6473-4a76-9a4d-cf8b9ddcc2cf",
                    "LayerId": "8e3bbd27-d4f9-4423-8b33-088bf460daf8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8e3bbd27-d4f9-4423-8b33-088bf460daf8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81c685f6-95c9-4855-9d65-6eae4b811eeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}