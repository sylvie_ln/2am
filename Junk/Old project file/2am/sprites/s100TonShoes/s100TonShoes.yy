{
    "id": "28935559-b33b-4dd4-a71f-cf6a084b3871",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s100TonShoes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "267bc28a-e624-41a9-aa07-711eeeab5ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28935559-b33b-4dd4-a71f-cf6a084b3871",
            "compositeImage": {
                "id": "b329f722-92d9-47d0-8f2e-02e5284c2ce8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "267bc28a-e624-41a9-aa07-711eeeab5ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691d0910-89f9-4f71-af58-309b888d2b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "267bc28a-e624-41a9-aa07-711eeeab5ca8",
                    "LayerId": "19ece403-1b52-4a68-9409-bf1dcb931117"
                },
                {
                    "id": "f78fcba5-ed81-48c3-b43a-ad66cf5dcc2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "267bc28a-e624-41a9-aa07-711eeeab5ca8",
                    "LayerId": "f93b86c3-3cc5-4974-b4ac-8ff8b522a195"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "19ece403-1b52-4a68-9409-bf1dcb931117",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28935559-b33b-4dd4-a71f-cf6a084b3871",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f93b86c3-3cc5-4974-b4ac-8ff8b522a195",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28935559-b33b-4dd4-a71f-cf6a084b3871",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}