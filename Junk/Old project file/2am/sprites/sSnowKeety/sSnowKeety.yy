{
    "id": "99de34d4-4dd7-41a4-b67d-e6c88b0744d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnowKeety",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0def30d8-1959-435a-be75-93f8518e798f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99de34d4-4dd7-41a4-b67d-e6c88b0744d3",
            "compositeImage": {
                "id": "1c56806f-ff1f-44b9-89cd-5ca0803f6e58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0def30d8-1959-435a-be75-93f8518e798f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be31eff5-b286-4f90-a423-6c41765648ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0def30d8-1959-435a-be75-93f8518e798f",
                    "LayerId": "b1f4cad8-f981-497c-9ca6-93deca39f0f5"
                },
                {
                    "id": "046f96a8-af26-4a84-bb81-c07e9bf5e09f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0def30d8-1959-435a-be75-93f8518e798f",
                    "LayerId": "6c99dfdd-4ebc-41ea-a759-461ce53e3d55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b1f4cad8-f981-497c-9ca6-93deca39f0f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99de34d4-4dd7-41a4-b67d-e6c88b0744d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6c99dfdd-4ebc-41ea-a759-461ce53e3d55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99de34d4-4dd7-41a4-b67d-e6c88b0744d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}