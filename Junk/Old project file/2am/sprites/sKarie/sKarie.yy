{
    "id": "c3bc92e7-4363-4695-a692-49ab0e3f6915",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKarie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d65e3af7-4280-4036-98f5-ba35a74f5cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3bc92e7-4363-4695-a692-49ab0e3f6915",
            "compositeImage": {
                "id": "acfcf3e6-ac56-4c6c-83d2-c028d465fb1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d65e3af7-4280-4036-98f5-ba35a74f5cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9e269e7-600e-470f-8843-feac2d88390d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d65e3af7-4280-4036-98f5-ba35a74f5cca",
                    "LayerId": "9f13abad-ad08-475d-ac70-2cb5685067f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9f13abad-ad08-475d-ac70-2cb5685067f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3bc92e7-4363-4695-a692-49ab0e3f6915",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}