{
    "id": "1050906e-27a8-46ad-aefd-8c6a3316b411",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBridgeOrb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c1f63e2-176d-4316-a217-2bde1cfc63c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1050906e-27a8-46ad-aefd-8c6a3316b411",
            "compositeImage": {
                "id": "6e0db6c3-40dc-4643-bce3-065f148d3df9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c1f63e2-176d-4316-a217-2bde1cfc63c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c40ab34-b5e9-44fd-b4ae-9e2a69ecffb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c1f63e2-176d-4316-a217-2bde1cfc63c5",
                    "LayerId": "00730431-d866-4e8e-876b-8387aa1620f7"
                },
                {
                    "id": "f551aeeb-94c6-4ddb-becb-8ea1f63482af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c1f63e2-176d-4316-a217-2bde1cfc63c5",
                    "LayerId": "222be45f-b22e-4555-a738-79d93fd9ed73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "222be45f-b22e-4555-a738-79d93fd9ed73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1050906e-27a8-46ad-aefd-8c6a3316b411",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "00730431-d866-4e8e-876b-8387aa1620f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1050906e-27a8-46ad-aefd-8c6a3316b411",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4294948864,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}