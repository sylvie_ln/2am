{
    "id": "a63ab1e2-a2b2-4b97-b1aa-fca0381d107a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoringRockSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f889919c-c8ae-4b05-b245-bd84eb23e229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a63ab1e2-a2b2-4b97-b1aa-fca0381d107a",
            "compositeImage": {
                "id": "fb1bed1c-c890-4f58-b5fb-16061539216a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f889919c-c8ae-4b05-b245-bd84eb23e229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ed9f88-d7e8-475f-9b94-707950a1a940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f889919c-c8ae-4b05-b245-bd84eb23e229",
                    "LayerId": "15dcc9ea-82cd-436a-ba42-508eee647633"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "15dcc9ea-82cd-436a-ba42-508eee647633",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a63ab1e2-a2b2-4b97-b1aa-fca0381d107a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}