{
    "id": "a8c8b81c-63f6-4f01-81cd-a4ac72c0b7e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLetterSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db0f12e6-6729-4b1d-91f1-2e4121f380dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8c8b81c-63f6-4f01-81cd-a4ac72c0b7e9",
            "compositeImage": {
                "id": "0e178e7f-97c0-4526-a899-3bbb4ab63d3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db0f12e6-6729-4b1d-91f1-2e4121f380dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07aba749-0702-42df-a01c-935f607de0a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db0f12e6-6729-4b1d-91f1-2e4121f380dd",
                    "LayerId": "2ee717dd-6d3e-4ddc-8027-58dbd9ac9302"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2ee717dd-6d3e-4ddc-8027-58dbd9ac9302",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8c8b81c-63f6-4f01-81cd-a4ac72c0b7e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}