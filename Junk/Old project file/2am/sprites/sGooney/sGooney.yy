{
    "id": "77576633-6105-4736-9ad8-a68d96f916eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGooney",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d13fa7b8-d404-4384-b343-a72603d8d778",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77576633-6105-4736-9ad8-a68d96f916eb",
            "compositeImage": {
                "id": "faa783dc-2dd5-46d3-b152-ce6eb8331288",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d13fa7b8-d404-4384-b343-a72603d8d778",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f69cdd4-41ac-4bc6-84ba-7623eec3a24b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13fa7b8-d404-4384-b343-a72603d8d778",
                    "LayerId": "30081093-6907-4925-9616-695fd44a306d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "30081093-6907-4925-9616-695fd44a306d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77576633-6105-4736-9ad8-a68d96f916eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}