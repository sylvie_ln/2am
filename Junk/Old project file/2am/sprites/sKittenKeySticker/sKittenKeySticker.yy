{
    "id": "48b86f4b-6a24-4f76-bf81-cf5a06f0fbb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittenKeySticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7abc865d-a291-465a-895a-a985075ea257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48b86f4b-6a24-4f76-bf81-cf5a06f0fbb0",
            "compositeImage": {
                "id": "4f38a15f-1667-4c5f-ba18-26ee16efdd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7abc865d-a291-465a-895a-a985075ea257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3712473-4ef3-4c74-97e4-f0f719afd935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7abc865d-a291-465a-895a-a985075ea257",
                    "LayerId": "e3970cd4-4a95-4d34-90d5-2e518ac2ed8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e3970cd4-4a95-4d34-90d5-2e518ac2ed8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48b86f4b-6a24-4f76-bf81-cf5a06f0fbb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 4,
    "yorig": 6
}