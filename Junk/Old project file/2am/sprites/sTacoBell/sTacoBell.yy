{
    "id": "c1df27d5-c208-4be0-8363-8bd855a0bea7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTacoBell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab607e7d-e0de-401d-8878-c651ec0bb40d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1df27d5-c208-4be0-8363-8bd855a0bea7",
            "compositeImage": {
                "id": "f9ffba0d-891b-4209-b3bb-e53fe294b7b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab607e7d-e0de-401d-8878-c651ec0bb40d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f61dfa-47e9-4b06-b54c-481ca47e4e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab607e7d-e0de-401d-8878-c651ec0bb40d",
                    "LayerId": "98bdd610-7fd2-4c95-a88a-f97b2361cb12"
                }
            ]
        },
        {
            "id": "ae708b1f-3023-403b-876f-50456936de0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1df27d5-c208-4be0-8363-8bd855a0bea7",
            "compositeImage": {
                "id": "c27b44d0-7b57-4450-8bbc-fcd8b3fa7f12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae708b1f-3023-403b-876f-50456936de0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "438dacf9-8c0f-4bbd-a9bc-2201ce12e881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae708b1f-3023-403b-876f-50456936de0e",
                    "LayerId": "98bdd610-7fd2-4c95-a88a-f97b2361cb12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "98bdd610-7fd2-4c95-a88a-f97b2361cb12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1df27d5-c208-4be0-8363-8bd855a0bea7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}