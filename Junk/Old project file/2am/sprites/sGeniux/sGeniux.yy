{
    "id": "babe6a3f-6d8e-4a2a-9b55-0302684bcd15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGeniux",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64ba6c0c-465b-468e-bb8d-5e9ca62c034e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "babe6a3f-6d8e-4a2a-9b55-0302684bcd15",
            "compositeImage": {
                "id": "d9b66ccb-55d9-4218-9442-df021aea2149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ba6c0c-465b-468e-bb8d-5e9ca62c034e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77c0f37c-7ba7-40e6-8fc4-a56c44192fb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ba6c0c-465b-468e-bb8d-5e9ca62c034e",
                    "LayerId": "02ff7642-392b-4a79-9c9c-73b523d1a311"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "02ff7642-392b-4a79-9c9c-73b523d1a311",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "babe6a3f-6d8e-4a2a-9b55-0302684bcd15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}