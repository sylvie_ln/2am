{
    "id": "a7f7d6fe-a10f-4e28-9a25-5c366d88d4fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGiftShopShirt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3b2bd66-9418-412f-9c49-0dd9a8485870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7f7d6fe-a10f-4e28-9a25-5c366d88d4fc",
            "compositeImage": {
                "id": "2f5d78a2-8dc2-443b-bf4a-1ea2ef93600e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3b2bd66-9418-412f-9c49-0dd9a8485870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73a2248-b8b8-4a3a-8c48-92cfe02fdda0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3b2bd66-9418-412f-9c49-0dd9a8485870",
                    "LayerId": "00d77f59-2277-4f88-85c3-bcf15fbc6e9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "00d77f59-2277-4f88-85c3-bcf15fbc6e9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7f7d6fe-a10f-4e28-9a25-5c366d88d4fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}