{
    "id": "f67d8360-c6ef-4745-8105-96fc664c6fbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnakeSegments",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f032b840-3950-4030-83d9-3decb4c7afcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f67d8360-c6ef-4745-8105-96fc664c6fbf",
            "compositeImage": {
                "id": "2b76867c-fc77-4304-b38b-70e7630bed67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f032b840-3950-4030-83d9-3decb4c7afcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94ebf940-7b98-41bc-8af5-f3e072899e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f032b840-3950-4030-83d9-3decb4c7afcc",
                    "LayerId": "f5475ac4-7126-4050-8cd0-b52edb556b12"
                }
            ]
        },
        {
            "id": "e4c75937-310e-4b04-99a1-15931f829e4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f67d8360-c6ef-4745-8105-96fc664c6fbf",
            "compositeImage": {
                "id": "72bd15b7-0c01-44f9-b5fd-6a7b92aafa61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4c75937-310e-4b04-99a1-15931f829e4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2142549c-9be0-408a-a390-d375c2409d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4c75937-310e-4b04-99a1-15931f829e4c",
                    "LayerId": "f5475ac4-7126-4050-8cd0-b52edb556b12"
                }
            ]
        },
        {
            "id": "19734ac3-4b05-4936-b4fa-7cf86d256581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f67d8360-c6ef-4745-8105-96fc664c6fbf",
            "compositeImage": {
                "id": "33f28375-bc58-461a-82a9-6b30c9a410fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19734ac3-4b05-4936-b4fa-7cf86d256581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00ee55ce-ce58-46b2-8653-e9690c770b99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19734ac3-4b05-4936-b4fa-7cf86d256581",
                    "LayerId": "f5475ac4-7126-4050-8cd0-b52edb556b12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f5475ac4-7126-4050-8cd0-b52edb556b12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f67d8360-c6ef-4745-8105-96fc664c6fbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}