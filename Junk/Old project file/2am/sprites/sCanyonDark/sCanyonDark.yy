{
    "id": "da43a3f2-eaae-4b39-b783-4b39b0b9e8f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanyonDark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78b0af09-d621-4452-93a5-f4ef56f9bc56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da43a3f2-eaae-4b39-b783-4b39b0b9e8f4",
            "compositeImage": {
                "id": "43732792-580a-4313-bf70-5506a9ebd28a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b0af09-d621-4452-93a5-f4ef56f9bc56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289f9f19-72bc-4e74-9e2d-1f896fb83e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b0af09-d621-4452-93a5-f4ef56f9bc56",
                    "LayerId": "9b3d24e2-14d0-467a-99e6-6f0fd1181cc7"
                }
            ]
        },
        {
            "id": "f6969b7b-1ed7-422b-86bc-e86a00ce5836",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da43a3f2-eaae-4b39-b783-4b39b0b9e8f4",
            "compositeImage": {
                "id": "f863ae3c-b234-4fca-9b11-1f06af72e134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6969b7b-1ed7-422b-86bc-e86a00ce5836",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf98a7b-56c9-4ead-8990-e617457fa604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6969b7b-1ed7-422b-86bc-e86a00ce5836",
                    "LayerId": "9b3d24e2-14d0-467a-99e6-6f0fd1181cc7"
                }
            ]
        },
        {
            "id": "af728c91-9411-4277-9e96-9d85a8305857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da43a3f2-eaae-4b39-b783-4b39b0b9e8f4",
            "compositeImage": {
                "id": "1b912f84-83f8-4647-b610-d6ae79ffa806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af728c91-9411-4277-9e96-9d85a8305857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98929fc4-176e-4c24-a4de-9716fdbdd2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af728c91-9411-4277-9e96-9d85a8305857",
                    "LayerId": "9b3d24e2-14d0-467a-99e6-6f0fd1181cc7"
                }
            ]
        },
        {
            "id": "0a68c67f-a9ca-4e7f-8a85-396fab725caa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da43a3f2-eaae-4b39-b783-4b39b0b9e8f4",
            "compositeImage": {
                "id": "12628094-1251-4978-b978-e58e8c966714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a68c67f-a9ca-4e7f-8a85-396fab725caa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8a1a8fe-01aa-4d56-ac7e-94436b55f84a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a68c67f-a9ca-4e7f-8a85-396fab725caa",
                    "LayerId": "9b3d24e2-14d0-467a-99e6-6f0fd1181cc7"
                }
            ]
        },
        {
            "id": "3bf9ca0c-0df5-4765-a275-b71a56e8fc92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da43a3f2-eaae-4b39-b783-4b39b0b9e8f4",
            "compositeImage": {
                "id": "8d995f65-5781-4e98-85c4-9771e03781ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bf9ca0c-0df5-4765-a275-b71a56e8fc92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa92decc-d285-4176-9b13-ee135850c1ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bf9ca0c-0df5-4765-a275-b71a56e8fc92",
                    "LayerId": "9b3d24e2-14d0-467a-99e6-6f0fd1181cc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "9b3d24e2-14d0-467a-99e6-6f0fd1181cc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da43a3f2-eaae-4b39-b783-4b39b0b9e8f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}