{
    "id": "a685c930-761c-4427-a140-fc50d9d0def1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s100TonShoesSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8dfc95d4-9293-4e59-8183-a72ebce32cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a685c930-761c-4427-a140-fc50d9d0def1",
            "compositeImage": {
                "id": "2f3c4fe5-3cfc-44f4-b048-ba87f4c66421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dfc95d4-9293-4e59-8183-a72ebce32cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81078bb6-e622-4fb8-ae73-e9163a03e4ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dfc95d4-9293-4e59-8183-a72ebce32cd0",
                    "LayerId": "8fd0cc42-4341-4e6d-b33e-d15f8eb2cbfd"
                },
                {
                    "id": "51ba12ef-f863-47e7-acb6-39c64d5da8e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dfc95d4-9293-4e59-8183-a72ebce32cd0",
                    "LayerId": "9943b504-912b-4891-9064-14d16864c21a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8fd0cc42-4341-4e6d-b33e-d15f8eb2cbfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a685c930-761c-4427-a140-fc50d9d0def1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9943b504-912b-4891-9064-14d16864c21a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a685c930-761c-4427-a140-fc50d9d0def1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}