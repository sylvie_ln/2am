{
    "id": "5f7d1a4b-abbf-45e9-89fb-7df4f5fb5702",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBeleve",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f702566-5fa3-4240-8052-856fc439bf97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f7d1a4b-abbf-45e9-89fb-7df4f5fb5702",
            "compositeImage": {
                "id": "409d2d39-5ad1-4e13-964b-c2dea14254dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f702566-5fa3-4240-8052-856fc439bf97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5359a5b-8d7a-4c1a-b893-df345e36b0b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f702566-5fa3-4240-8052-856fc439bf97",
                    "LayerId": "d1564a95-9cd0-4da7-89a3-e85dcab582ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d1564a95-9cd0-4da7-89a3-e85dcab582ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f7d1a4b-abbf-45e9-89fb-7df4f5fb5702",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}