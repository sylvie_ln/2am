{
    "id": "517d862b-c010-4cea-b1b0-0a1dbf612bea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChestor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40ec61ea-6876-4945-b099-c2c731b7d24a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "517d862b-c010-4cea-b1b0-0a1dbf612bea",
            "compositeImage": {
                "id": "78e5c10e-b70e-4709-bef8-b7856df43ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ec61ea-6876-4945-b099-c2c731b7d24a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7044ad2-0f86-4b10-ba2c-e0ef3b55088a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ec61ea-6876-4945-b099-c2c731b7d24a",
                    "LayerId": "d6b1e25d-45b7-42cd-a69d-a2bf94cae86b"
                }
            ]
        },
        {
            "id": "c8c04a59-c6bd-410f-8b64-ce1641dd4a5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "517d862b-c010-4cea-b1b0-0a1dbf612bea",
            "compositeImage": {
                "id": "3405411a-55e1-4c3b-b330-31c8758e99b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8c04a59-c6bd-410f-8b64-ce1641dd4a5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1f31b91-698b-44e8-8178-92b9ed655479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8c04a59-c6bd-410f-8b64-ce1641dd4a5b",
                    "LayerId": "d6b1e25d-45b7-42cd-a69d-a2bf94cae86b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d6b1e25d-45b7-42cd-a69d-a2bf94cae86b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "517d862b-c010-4cea-b1b0-0a1dbf612bea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}