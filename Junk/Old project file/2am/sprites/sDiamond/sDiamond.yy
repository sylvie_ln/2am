{
    "id": "10882d05-d6f3-485d-8a54-13778af26fa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDiamond",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cce20850-ed82-408d-968b-aa8794a5f7ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10882d05-d6f3-485d-8a54-13778af26fa8",
            "compositeImage": {
                "id": "c5af07eb-177d-4ae1-849b-1f3d35d2be79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cce20850-ed82-408d-968b-aa8794a5f7ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba48463-ba75-46e0-8f95-034787bea6b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cce20850-ed82-408d-968b-aa8794a5f7ca",
                    "LayerId": "17c65a3e-0b59-4401-a662-d0ba09aa4a24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "17c65a3e-0b59-4401-a662-d0ba09aa4a24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10882d05-d6f3-485d-8a54-13778af26fa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}