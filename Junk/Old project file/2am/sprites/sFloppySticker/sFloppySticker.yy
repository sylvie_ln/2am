{
    "id": "f6054c34-a756-4727-a158-75411edce08f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFloppySticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b48d82d-dcda-4219-b060-b4f6f2056e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "4debc90a-fb52-4279-b165-0bd33f8d6111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b48d82d-dcda-4219-b060-b4f6f2056e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef382102-d4d3-466c-bfe1-4cf766ad6882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b48d82d-dcda-4219-b060-b4f6f2056e16",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "1dcaaec7-3e7b-43f7-8399-b592ed619172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "8453d3c1-3195-4d06-9679-f19ff3461915",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dcaaec7-3e7b-43f7-8399-b592ed619172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33bca235-152d-4bf1-9a52-8c382f382478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dcaaec7-3e7b-43f7-8399-b592ed619172",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "2a3e5695-c86e-4d65-ac56-74d08594fdf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "cf63a1a3-fac2-4afc-b71c-61328e4c235a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a3e5695-c86e-4d65-ac56-74d08594fdf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6da80b8-d663-4e2b-b900-aac4a286108f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a3e5695-c86e-4d65-ac56-74d08594fdf1",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "47334bda-5e60-4090-be74-8be517dc019e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "9cbdae73-f1cd-4160-9c41-eaf10fc2fd31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47334bda-5e60-4090-be74-8be517dc019e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b236f1be-aae8-44dc-9ff3-056a84924084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47334bda-5e60-4090-be74-8be517dc019e",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "7e819273-6f69-49a9-a617-672c3a066b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "0e72f866-eb05-4425-90a6-4eddbf9f79a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e819273-6f69-49a9-a617-672c3a066b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6e21e30-33a1-461d-b49f-bfaee3fbd36c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e819273-6f69-49a9-a617-672c3a066b13",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "e3cffe0d-0c08-4f9c-aaa2-dad2d8687cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "5fad1be9-1813-4a1c-991e-b1836fe39fb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3cffe0d-0c08-4f9c-aaa2-dad2d8687cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6f6999c-f4e0-4815-bdf8-b5252ade2b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3cffe0d-0c08-4f9c-aaa2-dad2d8687cf2",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "ad190490-e80d-4668-8091-449f53b19df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "2f7cc98f-105a-4797-897d-14a6a9fd0424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad190490-e80d-4668-8091-449f53b19df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e813cb09-f2e5-4cc8-9e9c-cb8d2724f2e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad190490-e80d-4668-8091-449f53b19df7",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "bca57023-8317-4bcf-9de1-f3bb1583a2bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "06607db2-c6e5-4034-bfc3-d095093e8f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca57023-8317-4bcf-9de1-f3bb1583a2bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef4baa22-80fe-4e25-9ff2-1fbab537c3ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca57023-8317-4bcf-9de1-f3bb1583a2bd",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "acdcd15d-a739-4018-b43f-915975c4c7d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "ce6180f8-9372-4bee-bd65-67be27ca6b29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acdcd15d-a739-4018-b43f-915975c4c7d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f0c2159-eec1-4c70-935c-09870f044e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acdcd15d-a739-4018-b43f-915975c4c7d2",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        },
        {
            "id": "732df78f-4572-411f-822c-16319b63a47e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "compositeImage": {
                "id": "74621aa7-d752-4099-bc81-6379c8b7726b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "732df78f-4572-411f-822c-16319b63a47e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a080e072-9e19-4dff-bd0b-e4de314ea5b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "732df78f-4572-411f-822c-16319b63a47e",
                    "LayerId": "d925d8bb-21db-42cd-8bcb-041563e0210c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d925d8bb-21db-42cd-8bcb-041563e0210c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6054c34-a756-4727-a158-75411edce08f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}