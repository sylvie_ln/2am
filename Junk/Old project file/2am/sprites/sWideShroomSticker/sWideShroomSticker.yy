{
    "id": "9b576d07-323d-4113-a22d-aa3c4de9259e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWideShroomSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bad1e9db-db5b-4603-9035-8b174353b1da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "compositeImage": {
                "id": "ecf130ab-fc93-4a6d-b96d-86fadd40f722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bad1e9db-db5b-4603-9035-8b174353b1da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3edc577a-1304-4144-a415-4c054250e5fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bad1e9db-db5b-4603-9035-8b174353b1da",
                    "LayerId": "d425fc41-f0fa-41fc-b3df-b001e38dc748"
                },
                {
                    "id": "a7dfce58-350e-4e2e-b701-d2b03f7ae877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bad1e9db-db5b-4603-9035-8b174353b1da",
                    "LayerId": "77efab93-a711-4960-9335-d4ee30830413"
                }
            ]
        },
        {
            "id": "a934a382-3d06-4a90-b20d-11d1148afa8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "compositeImage": {
                "id": "e2d3bb43-0db3-4d1f-b14a-e0f9a34d2d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a934a382-3d06-4a90-b20d-11d1148afa8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b287d44a-66f3-4005-be1b-16ea9b521618",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a934a382-3d06-4a90-b20d-11d1148afa8b",
                    "LayerId": "d425fc41-f0fa-41fc-b3df-b001e38dc748"
                },
                {
                    "id": "e6fb91d2-380e-4a68-ab2b-399513b11de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a934a382-3d06-4a90-b20d-11d1148afa8b",
                    "LayerId": "77efab93-a711-4960-9335-d4ee30830413"
                }
            ]
        },
        {
            "id": "0d260de6-3f17-432f-aa33-c1a86e3adf76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "compositeImage": {
                "id": "d78e9e95-ae44-4847-b017-710dda1e1e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d260de6-3f17-432f-aa33-c1a86e3adf76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e5fdaa3-8fea-4793-8a25-445b335ea853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d260de6-3f17-432f-aa33-c1a86e3adf76",
                    "LayerId": "d425fc41-f0fa-41fc-b3df-b001e38dc748"
                },
                {
                    "id": "77cc5f4f-3ada-40a6-b24e-d9767f84370f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d260de6-3f17-432f-aa33-c1a86e3adf76",
                    "LayerId": "77efab93-a711-4960-9335-d4ee30830413"
                }
            ]
        },
        {
            "id": "65c38ed3-482e-4ea9-a684-bc4c152ed46e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "compositeImage": {
                "id": "a23be20d-a520-41ab-9386-05b4d0accc7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65c38ed3-482e-4ea9-a684-bc4c152ed46e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a083675-dc43-4b97-b31d-73920d3b551d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65c38ed3-482e-4ea9-a684-bc4c152ed46e",
                    "LayerId": "d425fc41-f0fa-41fc-b3df-b001e38dc748"
                },
                {
                    "id": "9b86f527-aeb8-4823-87ef-41e9905652dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65c38ed3-482e-4ea9-a684-bc4c152ed46e",
                    "LayerId": "77efab93-a711-4960-9335-d4ee30830413"
                }
            ]
        },
        {
            "id": "e4a5dcc8-9c54-4e90-aecd-9784f49f22ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "compositeImage": {
                "id": "1f5b17a8-ce09-4b8d-aee1-99ff5c499b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a5dcc8-9c54-4e90-aecd-9784f49f22ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207f038e-c20a-43d4-a45b-f64a839197d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a5dcc8-9c54-4e90-aecd-9784f49f22ca",
                    "LayerId": "d425fc41-f0fa-41fc-b3df-b001e38dc748"
                },
                {
                    "id": "2a74c47a-8329-4e62-8ef0-3a5069b16542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a5dcc8-9c54-4e90-aecd-9784f49f22ca",
                    "LayerId": "77efab93-a711-4960-9335-d4ee30830413"
                }
            ]
        },
        {
            "id": "a8d1b990-1c22-4578-90ec-c333d9897005",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "compositeImage": {
                "id": "4938260e-e95a-4ef6-9db6-957141493a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d1b990-1c22-4578-90ec-c333d9897005",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4adb274-b1b0-4cbd-b68e-b73cd1683613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d1b990-1c22-4578-90ec-c333d9897005",
                    "LayerId": "d425fc41-f0fa-41fc-b3df-b001e38dc748"
                },
                {
                    "id": "6c63e771-6bf6-4cf9-b705-5269ab0b6886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d1b990-1c22-4578-90ec-c333d9897005",
                    "LayerId": "77efab93-a711-4960-9335-d4ee30830413"
                }
            ]
        },
        {
            "id": "3b875001-a0fb-43ef-b460-6e48b9fb5c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "compositeImage": {
                "id": "ef825e10-5417-4d35-9719-e95f24953562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b875001-a0fb-43ef-b460-6e48b9fb5c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b6b3f6-554d-4239-9d50-8eba107b945c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b875001-a0fb-43ef-b460-6e48b9fb5c44",
                    "LayerId": "d425fc41-f0fa-41fc-b3df-b001e38dc748"
                },
                {
                    "id": "a91a5392-3fc9-4041-9560-459a7e24ff53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b875001-a0fb-43ef-b460-6e48b9fb5c44",
                    "LayerId": "77efab93-a711-4960-9335-d4ee30830413"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d425fc41-f0fa-41fc-b3df-b001e38dc748",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "77efab93-a711-4960-9335-d4ee30830413",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b576d07-323d-4113-a22d-aa3c4de9259e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}