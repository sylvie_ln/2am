{
    "id": "db9c9c1b-8a64-4d4e-9ab2-4240f09716a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnowKeetyRideMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 21,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dac3a4f-09e9-4dd4-a501-987208a65beb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db9c9c1b-8a64-4d4e-9ab2-4240f09716a3",
            "compositeImage": {
                "id": "5bb0bb26-89b9-4a9e-95d2-31ced8a37f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dac3a4f-09e9-4dd4-a501-987208a65beb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14588838-1095-4e7c-afe6-d31168cc8744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dac3a4f-09e9-4dd4-a501-987208a65beb",
                    "LayerId": "a873fa8d-7b77-4773-8c85-e406b32ba10d"
                },
                {
                    "id": "99876f1c-a6ef-49cd-ae13-1572b85f5842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dac3a4f-09e9-4dd4-a501-987208a65beb",
                    "LayerId": "b4d2823b-5e65-4fad-8fd8-b22c93115b35"
                },
                {
                    "id": "8b651ab9-f08e-4cbd-830c-05b320142dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dac3a4f-09e9-4dd4-a501-987208a65beb",
                    "LayerId": "6d4be27e-4b8e-44ae-94ed-1938dd159b9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6d4be27e-4b8e-44ae-94ed-1938dd159b9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db9c9c1b-8a64-4d4e-9ab2-4240f09716a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a873fa8d-7b77-4773-8c85-e406b32ba10d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db9c9c1b-8a64-4d4e-9ab2-4240f09716a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "b4d2823b-5e65-4fad-8fd8-b22c93115b35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db9c9c1b-8a64-4d4e-9ab2-4240f09716a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 16
}