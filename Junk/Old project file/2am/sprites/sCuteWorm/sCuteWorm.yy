{
    "id": "510145bf-13b6-4e76-8c1e-f4c57cdf0207",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCuteWorm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 412,
    "bbox_left": 0,
    "bbox_right": 603,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71b12f25-e86c-44e2-b412-72f7a36d234c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "510145bf-13b6-4e76-8c1e-f4c57cdf0207",
            "compositeImage": {
                "id": "1f497b38-b0a0-4115-b43a-8bb1c9c56f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b12f25-e86c-44e2-b412-72f7a36d234c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72934bc2-d41c-4b69-99ee-b71e5c6d2433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b12f25-e86c-44e2-b412-72f7a36d234c",
                    "LayerId": "4680d377-0cc6-4cf5-b760-46e1c41e1972"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 413,
    "layers": [
        {
            "id": "4680d377-0cc6-4cf5-b760-46e1c41e1972",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "510145bf-13b6-4e76-8c1e-f4c57cdf0207",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 604,
    "xorig": 302,
    "yorig": 206
}