{
    "id": "4debb5b9-a6df-4415-ac12-2b337855c0e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLegendaryTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7222db3c-16fb-4fc4-a7db-549d0a57ea9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4debb5b9-a6df-4415-ac12-2b337855c0e3",
            "compositeImage": {
                "id": "508c8b07-af14-49f6-a458-cbc3e564260b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7222db3c-16fb-4fc4-a7db-549d0a57ea9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9dffa53-7ed4-46d4-9a83-b8f7a682a956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7222db3c-16fb-4fc4-a7db-549d0a57ea9c",
                    "LayerId": "15637da0-d41f-4f52-b311-00ea2f98771b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "15637da0-d41f-4f52-b311-00ea2f98771b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4debb5b9-a6df-4415-ac12-2b337855c0e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}