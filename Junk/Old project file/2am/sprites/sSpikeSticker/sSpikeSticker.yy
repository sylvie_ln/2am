{
    "id": "dda6bd8e-f96e-4b87-98a9-a833d35227c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpikeSticker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "057240a0-1bec-4f45-bbad-eb997adb05b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dda6bd8e-f96e-4b87-98a9-a833d35227c0",
            "compositeImage": {
                "id": "343ac2bf-0675-4762-a248-3abb67c76f25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057240a0-1bec-4f45-bbad-eb997adb05b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6690dda-8279-4ecd-b360-ed3160ef5119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057240a0-1bec-4f45-bbad-eb997adb05b0",
                    "LayerId": "f1f42348-d9dc-43b0-8413-0244bf857fe9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f1f42348-d9dc-43b0-8413-0244bf857fe9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dda6bd8e-f96e-4b87-98a9-a833d35227c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}