{
    "id": "f74d6975-15f4-4675-b442-a0dc6ae7b538",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPutItInTheBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 6,
    "bbox_right": 317,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e888e2db-30f1-4a50-9426-b87591a7bd25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74d6975-15f4-4675-b442-a0dc6ae7b538",
            "compositeImage": {
                "id": "3d25aee7-8f9f-4f98-8a9f-714c5b38cf06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e888e2db-30f1-4a50-9426-b87591a7bd25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e42dcbca-8fef-4cd8-9c88-14fd9880e52b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e888e2db-30f1-4a50-9426-b87591a7bd25",
                    "LayerId": "218c03e8-8af4-4ec2-a2cc-ce03ea6322eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "218c03e8-8af4-4ec2-a2cc-ce03ea6322eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f74d6975-15f4-4675-b442-a0dc6ae7b538",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}