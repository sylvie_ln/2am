{
    "id": "9d826020-097b-4086-b2d7-737de754dda8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sQuitSmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 38,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d3db63c-4440-42ae-aa67-a7ddef94f708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d826020-097b-4086-b2d7-737de754dda8",
            "compositeImage": {
                "id": "b1cc0474-a700-42d5-8798-96602d77a493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d3db63c-4440-42ae-aa67-a7ddef94f708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07b945d-f8ee-4fe2-be63-9a4eb2507d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3db63c-4440-42ae-aa67-a7ddef94f708",
                    "LayerId": "0e4e335e-bf63-4e8f-9dfd-93334ca98c05"
                }
            ]
        },
        {
            "id": "b1402fdd-22b4-4a11-a8bd-b27194da3cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d826020-097b-4086-b2d7-737de754dda8",
            "compositeImage": {
                "id": "bac7ad35-4024-4895-abb0-e4995077bc0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1402fdd-22b4-4a11-a8bd-b27194da3cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08a3a92c-a142-4e71-93f3-89a6c1f69826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1402fdd-22b4-4a11-a8bd-b27194da3cf6",
                    "LayerId": "0e4e335e-bf63-4e8f-9dfd-93334ca98c05"
                }
            ]
        },
        {
            "id": "2389e2a0-607c-4c88-b245-d9aba68cae00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d826020-097b-4086-b2d7-737de754dda8",
            "compositeImage": {
                "id": "42835113-0ee6-497c-a718-233905c375d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2389e2a0-607c-4c88-b245-d9aba68cae00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "283010e5-12a9-4401-9276-d228538f2054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2389e2a0-607c-4c88-b245-d9aba68cae00",
                    "LayerId": "0e4e335e-bf63-4e8f-9dfd-93334ca98c05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0e4e335e-bf63-4e8f-9dfd-93334ca98c05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d826020-097b-4086-b2d7-737de754dda8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 39,
    "yorig": 0
}