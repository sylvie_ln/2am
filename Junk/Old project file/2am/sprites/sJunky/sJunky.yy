{
    "id": "8c597447-b1c9-4d9b-9f20-0abfdf332eca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJunky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ca33f25-5e09-44da-a673-5664d63044dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c597447-b1c9-4d9b-9f20-0abfdf332eca",
            "compositeImage": {
                "id": "9658760e-a216-491b-9d77-62769a9fa5d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca33f25-5e09-44da-a673-5664d63044dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b2fa6a-0738-4b96-961a-2bd1f87a8091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca33f25-5e09-44da-a673-5664d63044dc",
                    "LayerId": "0e524c5a-c869-4b04-a1cd-c73c23404858"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0e524c5a-c869-4b04-a1cd-c73c23404858",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c597447-b1c9-4d9b-9f20-0abfdf332eca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}