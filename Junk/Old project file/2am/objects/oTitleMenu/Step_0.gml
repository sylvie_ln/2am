///@description
if menu == quit and subsub {
	exit;	
}
if !subsub {
	if input_pressed_repeat("Up") {
		if submenu == 0 {
			menu--;	
		} else {
			submenu--;	
			if submenu <= 0 {
				submenu = submenus[menu]-1;	
			}
		}
		play_menu_cursor_move();
	}
	if input_pressed_repeat("Down") {
		if submenu == 0 {
			menu++;	
		} else {
			submenu++;
		}
		play_menu_cursor_move();
	}
	if input_pressed("Jump") {
		if submenu == 0 {
			submenu = 1;	
		} else {
			subsub = true;	
		}
		play_menu_forward();
	}
}
if input_pressed("Escape") {
	if subsub and submenu != control_idx[1] {
		subsub = false;
	} else if submenu >= 1 and !pause_mode {
		submenu = 0;	
	} else if submenu == 0 {
		if menu == quit {
			submenu = 1;
		} else {
			menu = quit;	
		}
	}
	if !pause_mode {
		play_menu_backward();
	}
}

if menu < 0 { menu = menus-1; }
if menu >= menus { menu = 0; }
if submenu != 0 {
	if submenu >= submenus[menu] { submenu = 1; }
}

if menu == quit and submenu == 1 and !subsub {
	audio_sound_gain(global.bgm,0,fade_time/global.default_room_speed*1000);
	alarm[0] = fade_time;
	subsub = true;
	exit;
}

if subsub {
	if menu == audio_idx[0] and submenu == audio_idx[1]
	and !oAudioMenu.active {
		with oAudioMenu {
			event_user(0);
		}
	}
	if menu == graphic_idx[0] and submenu == graphic_idx[1]
	and !oGraphicsMenu.active {
		with oGraphicsMenu {
			event_user(0);
		}
	}
	if menu == control_idx[0] and submenu == control_idx[1]
	and !oControlsMenu.active {
		with oControlsMenu {
			event_user(0);
		}
	}
	if menu == playful_idx[0] and submenu == playful_idx[1]
	and !oAssistMenu.active {
		with oAssistMenu {
			event_user(0);
		}
	}
	
	if (menu == reset_control_idx[0] and submenu == reset_control_idx[1])
	and !oResetter.active {
		with oResetter {
			desc="Controls will be reset.\nOther options are left alone."
			word="CONTROL"
			event_user(0);	
		}
	}
	
	if (menu == reset_option_idx[0] and submenu == reset_option_idx[1])
	and !oResetter.active {
		with oResetter {
			desc="Controls are left alone.\nOther options shall be reset."
			word="DEFAULT"
			event_user(0);	
		}
	}
	
	if (menu == reset_world_idx[0] and submenu == reset_world_idx[1]) 
	and !oResetter.active {
		with oResetter {
			desc="The world shall be reset.\nOptions are left alone."
			word="REBIRTH"
			event_user(0);	
		}
	}
	
	if (menu == reset_contract_idx[0] and submenu == reset_contract_idx[1]) 
	and !oResetter.active {
		with oResetter {
			desc="With this, everything will end."
			word="GOODBYE"
			event_user(0);	
		}
	}
	if (menu == options_back_idx[0] and submenu == options_back_idx[1]) 
	or (menu == enter_back_idx[0] and submenu == enter_back_idx[1]) 
	or (menu == reset_back_idx[0] and submenu == reset_back_idx[1]) 
	{
		if pause_mode {
			with oPauseMenu { active = true; }
			instance_destroy();
			exit;
		}
		submenu = 0;
		event_user(0);
	}
	
	if (menu == enter_go_idx[0] and submenu == enter_go_idx[1]) {
		load_game();
		room_goto(asset_get_index(global.current_room));
	}
}