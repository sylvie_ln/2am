if pause_mode and subsub { 
	if oAudioMenu.active or oGraphicsMenu.active or oControlsMenu.active or oAssistMenu.active {
		exit; 
	}
}
draw_set_font(global.neco_font);
draw_set_color(c_white);
if !pause_mode {
	var txt = "Another world by sylvie and hubol."
	draw_text(room_width-4-string_width(txt),room_height-4-string_height(txt),txt);
}
var hpos = x;
var vpos = y;
//show_debug_message([menu,submenu]);

if menu == quit and subsub {
	vpos += 20;
}
var txt = header[submenu == 0 ? 0 : menu, min(1,submenu)];
draw_text(hpos,vpos,txt);
hpos += 10;
vpos += string_height(txt);
if submenu == 0 {
	var stop = menus;
	for(var i=0; i<stop; i++) {
		if i == menu {
			draw_text(hpos-10,vpos,">");
		}
		txt = option[i,0];
		draw_text(hpos,vpos,txt);
		vpos += string_height(txt);
	}
} else {
	var stop = submenus[menu];
	for(var i=1; i<stop; i++) {
		if i == submenu {
			draw_text(hpos-10,vpos,">");
		}
		txt = option[menu,i];
		draw_text(hpos,vpos,txt);
		vpos += string_height(txt);
	}
}
if menu == quit and subsub {
	draw_set_alpha(lerp(0,1,(fade_time-alarm[0])/fade_time));
	draw_set_color(c_black);
	draw_rectangle(0,0,room_width,room_height,false);	
	draw_set_alpha(1);
	draw_set_color(c_white);
}