event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A very small and cutey little shroom!"

damp = 0.85;
bounce = damp;
fric = 0.99;
spinny = false;

depth -= 2;
if room != rmCoolMuseum {
	image_index = sylvie_irandom(sprite_get_number(sprite_index)-1);
}
image_speed = 0;