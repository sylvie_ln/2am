{
    "id": "3ebf2473-19e8-4f1a-a39f-958d45bed045",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEmergencyBubble",
    "eventList": [
        {
            "id": "c92e6d66-a7cd-4923-b0d1-3d97e22a5dc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3ebf2473-19e8-4f1a-a39f-958d45bed045"
        },
        {
            "id": "f6f20702-912a-4fee-a75a-c1306bfbaa1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "3ebf2473-19e8-4f1a-a39f-958d45bed045"
        },
        {
            "id": "71b4c069-2ee1-4e5d-8de9-99e1a0fad03a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "3ebf2473-19e8-4f1a-a39f-958d45bed045"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0cbf5b0e-b25e-4a0a-adfe-d022891eaa62",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7835eae5-abc1-419f-8905-9a4bb94552ac",
    "visible": true
}