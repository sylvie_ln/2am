{
    "id": "ca06b711-5d9e-4762-b6f7-2a6bae4fd11a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oOxygenEgg",
    "eventList": [
        {
            "id": "9fa962e0-dbf0-4cbe-9c25-3a02274ec2d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca06b711-5d9e-4762-b6f7-2a6bae4fd11a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "80b931ed-23f0-4dfb-9ffd-ade5efc708b3",
    "visible": true
}