{
    "id": "63e3e96e-c531-40e8-bac1-6a2259c037af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSurpriseBox",
    "eventList": [
        {
            "id": "588651c9-1ed0-44a9-b825-9074c1e28422",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63e3e96e-c531-40e8-bac1-6a2259c037af"
        },
        {
            "id": "c624f06c-43d2-43f0-8e33-e210bc66ec32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "63e3e96e-c531-40e8-bac1-6a2259c037af"
        },
        {
            "id": "59e0db98-957a-4a3b-aa50-c9262dd7d286",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "63e3e96e-c531-40e8-bac1-6a2259c037af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "34217d94-4afa-4ceb-bbba-0fa309d4b1d4",
    "visible": true
}