///@description

// Inherit the parent event

script = @"An amazing box filled with SM surprises. The number on the box is the amount of SM it contains.
Will you open this great box?! \ask{Yes!}{No}
!if answer==1
"
var sus=surprise[floor(image_index)];
for(var i=0; i<array_length_1d(sus); i++) {
	script += @" !throw "+object_get_name(sus[i])+@"
	"	
}
script += @" !play sndMinecartLetsGo
 !disappear
!else
 How sad!!
!fi"

event_inherited();

