if fake and bag_has_item(oLensOfLies) {
	exit;	
}
if outer { exit; }
for(var i=0; i<image_xscale; i++) {
	for(var j=0; j<image_yscale; j++) {
		draw_sprite_ext(sprite_index,ii[i,j],x+i*sprite_get_width(sprite_index),y+j*sprite_get_height(sprite_index),1,1,0,c_white,image_alpha);
		if layer_exists("Overlay") {
			draw_sprite_ext(overlay,1,x+i*sprite_get_width(sprite_index),y+j*sprite_get_height(sprite_index),1,1,0,c_white,image_alpha);
		}
	}
}
