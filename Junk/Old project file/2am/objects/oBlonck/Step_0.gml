if !fake { exit; }
if instance_exists(oFlarekitten) {
	dofade = true;
	//faded = false;
}
var f = 0.25;
var t = 0.05;
var dirs = [[0,-1],[0,1],[-1,0],[1,0]];
var touch = place_meeting(x,y,oChar);
if (touch or dofade) and !faded and !oShifter.shifting {
	//if touch { ff = 1; }
	faded = true;
	if image_alpha > t {
		image_alpha -= image_alpha*f;
	} else {
		image_alpha = 0;
	}
	for(var i=0; i<4; i++) {
		var d = dirs[i];
		with instance_place(x+d[0],y+d[1],oBlonck) {
			if !fake { continue; }
			if !faded {
				dofade = true;	
				//ff = other.ff*0.9;
				event_perform(ev_step,ev_step_normal);
				dofade = false;
			}
		}
	}
}