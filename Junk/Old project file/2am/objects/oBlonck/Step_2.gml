if !fake { exit; }
if instance_exists(oShifter) and oShifter.shifting { exit; }
var f = 0.1;
var t = 0.05;
if !faded {
	if image_alpha < 1-t {
		image_alpha += max(t,image_alpha*f);	
	} else {
		image_alpha = 1;	
	}
}