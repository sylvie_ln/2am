if !solid { exit; }
if !just_created { exit; }
just_created = false;
var xscale = image_xscale;
var yscale = image_yscale;
image_xscale = 1;
image_yscale = 1;
for(var i=0; i<xscale; i++) {
	for(var j=0; j<yscale; j++) {
		if j == 0 and (y mod global.rheight != 0) and !solid_at(x+i*sprite_get_width(sprite_index),y-sprite_get_width(sprite_index)) {
			ii[i,j] = sylvie_random(1,num-1);
			if !ii_only and instance_exists(oRoomGen) and image_alpha != 0.5 {
				if (x mod global.rwidth != 0) and ((x+oRoomGen.cell_size) mod global.rwidth != 0) {
					ds_list_add(oRoomGen.valid_blocks,id);	
				}
			}
		} else {
			ii[i,j] = 0;	
		}
	}
}
image_xscale = xscale;
image_yscale = yscale;