{
    "id": "010b61a2-11ce-4ddd-a4d6-cd7f11b55741",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlackRose",
    "eventList": [
        {
            "id": "1e04b3bd-3dd8-4b21-8b54-905e599754cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "010b61a2-11ce-4ddd-a4d6-cd7f11b55741"
        },
        {
            "id": "05de5074-c6e7-45de-9f6e-e085022e7c26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "010b61a2-11ce-4ddd-a4d6-cd7f11b55741"
        },
        {
            "id": "612e8a4f-e9b1-4e08-b568-f9afa97d3e38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "010b61a2-11ce-4ddd-a4d6-cd7f11b55741"
        },
        {
            "id": "6ab863d0-68bb-4702-b60a-bc7e9b7abd00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "010b61a2-11ce-4ddd-a4d6-cd7f11b55741"
        }
    ],
    "maskSpriteId": "66ea71ef-6745-4649-aa26-bef5a6a0e078",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e34aba32-2cea-494d-a909-a9a04b10600f",
    "visible": true
}