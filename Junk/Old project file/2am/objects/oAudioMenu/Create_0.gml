active = false;
parent = noone;

var i = 0;
option[i++] = "Volume"
choices[i-1] = [0.01,0.02,0.05,0.1,0.25,0.5,0.75,1];
option_choice[i-1] = find_choice(global.volume,choices[i-1]);
option_var[i-1] = "volume"
option[i++] = "Mute Music"
choices[i-1] = [1,0];
option_choice[i-1] = find_choice(global.mute_music,choices[i-1]);
option_var[i-1] = "mute_music"
option[i++] = "Mute SoundFX" 
choices[i-1] = [1,0];
option_choice[i-1] = find_choice(global.mute_sfx,choices[i-1]);
option_var[i-1] = "mute_sfx"
option[i++] = "Done"
choices[i-1] = -1;
option_choice[i-1] = -1;
option_var[i-1] = ""
total = i;
selected = 0;

enum audio_options {
	volume,
	mute_music,
	mute_sfx,
	done
}