{
    "id": "3b7f8d68-c654-4713-ae16-f25ec2401174",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWideBagUpgrade",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4a2d3ff5-ea0b-4185-b5c4-2b30a28bfe90",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4e23fa8-d10b-4279-af83-9f4692fa03f8",
    "visible": true
}