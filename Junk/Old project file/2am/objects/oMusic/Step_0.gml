if no_radio == 0 and instance_exists(oChar) and (instance_exists(oBoomBox) or bag_has_item(oBoomBox)) {
	if global.radio_position == -1 or !audio_is_playing(radio_playlist[global.radio_position]) {
		global.radio_position = (global.radio_position+1) mod radio_playist_length;
		play_radio_bgm(radio_playlist[global.radio_position]);
		start = true;
	}
	var gain = 1;
	if instance_exists(oBoomBox) {
		var dist = 72;
		gain = max(0,dist-point_distance(oChar.x,oChar.y,oBoomBox.x,oBoomBox.y))/dist;
	}
	if audio_exists(global.bgm_instance) {
		audio_sound_gain(global.bgm_instance,sqr(1-gain),start? 0 :1000);
	}
	if audio_exists(global.radio_bgm_instance) {
		audio_sound_gain(global.radio_bgm_instance,sqrt(gain),start? 0: 1000);
	}
} else {
	if no_radio == 1 {
		if audio_exists(global.bgm_instance) {
			audio_sound_gain(global.bgm_instance,1,0);
		}
		no_radio = 2;	
	} else if no_radio == 0 {
		stop_radio_bgm();	
	}
}

start = false;