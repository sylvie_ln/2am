if (round(true_mouse(0)) != px or round(true_mouse(1)) != py or mouse_check_button_pressed(mb_left)) and !start and !instance_exists(oInTheBag) and ps == global.scale and pf == global.fullscreen {
	if !(instance_exists(oCursor) and oCursor.active) 
	or room == rmContract
	or room == rmMinecraft {
		window_set_cursor(cr_default);
	}
	alarm[0] = room_speed;
}
px = round(true_mouse(0));
py = round(true_mouse(1));
ps = global.scale;
pf = global.fullscreen;
start = false;