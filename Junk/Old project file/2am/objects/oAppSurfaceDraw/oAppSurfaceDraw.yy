{
    "id": "334b73e7-0971-4f42-a534-0047bc74d452",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAppSurfaceDraw",
    "eventList": [
        {
            "id": "0ba121d4-890c-4b85-ac4d-39c1b9f79630",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "334b73e7-0971-4f42-a534-0047bc74d452"
        },
        {
            "id": "31b107f3-696e-46f8-899d-42b49665cd98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "334b73e7-0971-4f42-a534-0047bc74d452"
        },
        {
            "id": "15642330-5723-492b-aced-467a8b4e009b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "334b73e7-0971-4f42-a534-0047bc74d452"
        },
        {
            "id": "0b07cbbd-5a4e-4c58-ad61-042473c7f1bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "334b73e7-0971-4f42-a534-0047bc74d452"
        },
        {
            "id": "e603e62c-1d15-4496-bccf-446b4543f93e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "334b73e7-0971-4f42-a534-0047bc74d452"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}