{
    "id": "43e4fdf8-8a6a-4823-af2e-c6d33e79d7af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDollars",
    "eventList": [
        {
            "id": "e4e8e246-d9f3-4089-89f4-97d19b0f9c1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43e4fdf8-8a6a-4823-af2e-c6d33e79d7af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d0afbdc5-1aec-4efd-9dea-b5e141dd9e8e",
    "visible": true
}