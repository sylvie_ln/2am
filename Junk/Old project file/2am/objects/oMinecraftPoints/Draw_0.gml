if multi > 9 {
	if !instance_exists(oMinecraftError) {
		instance_create_depth(room_width div 2,room_height div 2,depth-10,oMinecraftError);	
	}
	exit;
}
draw_sprite_ext(sMinecraftPoints,0,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
if multi > 1 {
	draw_sprite_ext(sMinecraftMultiplier,multi-2,x+16,y-6,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
/*
multi++;
if multi > 9 {
	multi = 2;
}
/*
if stepc >= sprite_get_number(spr) {
	stepc = 0;
	spr++;
}
if !sprite_exists(spr) {
	spr = sCharS;
	stepc = 0;
}
draw_sprite(spr,stepc++,x,y);