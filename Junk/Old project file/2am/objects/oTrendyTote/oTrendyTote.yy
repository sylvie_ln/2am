{
    "id": "fd4f2891-8ac2-4924-b3ec-ed1d844a79a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTrendyTote",
    "eventList": [
        {
            "id": "8d86cff4-339a-4733-9e44-9aedbc306a7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd4f2891-8ac2-4924-b3ec-ed1d844a79a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "def3cb12-caa8-4357-839e-e84bb8c0557c",
    "visible": true
}