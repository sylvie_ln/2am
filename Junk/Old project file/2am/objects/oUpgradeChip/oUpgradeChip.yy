{
    "id": "3eb77c79-a2e8-46fe-a706-afa95167cff2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oUpgradeChip",
    "eventList": [
        {
            "id": "293131dd-fa41-4d7f-b036-a307e21b4a3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3eb77c79-a2e8-46fe-a706-afa95167cff2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c5b6a5-e9d6-4082-92d4-375210026cb0",
    "visible": true
}