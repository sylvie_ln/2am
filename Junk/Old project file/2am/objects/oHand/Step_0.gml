if paused {exit;}
if waiting { 
	exit;
}

x += hv;
y += vv;

var dir = point_direction(x,y,target.x,target.y);
if !place_meeting(x,y,target) {
	acc += grav;	
	if acc > maxspd { acc = maxspd; }
} else {
	acc = 0;
}
hv += lengthdir_x(acc,dir);
vv += lengthdir_y(acc,dir);
var spd = point_distance(0,0,hv,vv);
if spd > maxspd {
	hv = (hv/spd)*maxspd;
	vv = (vv/spd)*maxspd;
}