{
    "id": "3cf2a5e5-8deb-4ca2-bdc0-e75f47cf3912",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHand",
    "eventList": [
        {
            "id": "2f9efa06-f90e-411f-9248-ddcfb95ac769",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3cf2a5e5-8deb-4ca2-bdc0-e75f47cf3912"
        },
        {
            "id": "8ac9b504-a4b1-4a45-a99a-fe15588e8495",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3cf2a5e5-8deb-4ca2-bdc0-e75f47cf3912"
        }
    ],
    "maskSpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
    "overriddenProperties": null,
    "parentObjectId": "c8ff8d82-948f-4875-b52e-6b0807ea0545",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
    "visible": true
}