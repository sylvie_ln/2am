///@description
if global.contract_signed { room_goto(rmTitle); exit; }
cx = room_width div 2;
page = 0;
contract_box = array_create(11,undefined);
contract_hover = array_create(11,false);
contract_signed = array_create(11,0);
init = false;

back_hover = false;
next_hover = false;
name_hover = false;

naming = false;
name = ""

signed = false;
can_accept = false;

keyboard_string_prev = "";