{
    "id": "5791a94a-a6a4-41d7-8e95-4900aa17c53e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTreePortal",
    "eventList": [
        {
            "id": "fb5b9f93-8a97-48b8-b6f4-f48936065748",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5791a94a-a6a4-41d7-8e95-4900aa17c53e"
        },
        {
            "id": "29a98549-d850-46d1-be95-304cab9bb2de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5791a94a-a6a4-41d7-8e95-4900aa17c53e"
        },
        {
            "id": "5ce50fee-2e8c-447a-a444-a32ec38b7b00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "5791a94a-a6a4-41d7-8e95-4900aa17c53e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}