if global.flag_legendary_tree != 0 { exit; }
cx = camera_get_view_x(view_camera[0])-xo
cy = camera_get_view_y(view_camera[0])-yo;
if bag_has_item(oSparkleShroom) and 
collision_rectangle(x-56-16,y-32,x-56+16,y-32+32,oChar,false,true) != noone {
	tick++;
} else {
	tick = 0;	
	xo = 0;
	yo = 0;
}
if tick == global.default_room_speed*2 {	
	event_user(15);
}
if tick >= global.default_room_speed*2 {	
	xo = sylvie_random(-2,2);
	yo = sylvie_random(-2,2);
}
if tick == global.default_room_speed*4 {
	event_user(15);
}
if tick == global.default_room_speed*5 {
	event_user(15);
}
if tick == global.default_room_speed*6 {
	event_user(15);
}
if tick == global.default_room_speed*7 {
	event_user(15);
}
if tick == global.default_room_speed*7 + (global.default_room_speed div 3) {
	event_user(15);
}
if tick == global.default_room_speed*7 + 2*(global.default_room_speed div 3) {
	event_user(15);
}
if tick >= global.default_room_speed*8 {
	event_user(15);
	stop_bgm()
	event_user(2);
}
camera_set_view_pos(view_camera[0],cx+xo,cy+yo);