{
    "id": "cd47f69d-1784-4169-b2a9-ad121823f8ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBunneyPin",
    "eventList": [
        {
            "id": "481fdaaa-2972-40fc-a06e-8280dea3894e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd47f69d-1784-4169-b2a9-ad121823f8ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "52e91e8e-945d-4730-8481-b7290bb6d1bd",
    "visible": true
}