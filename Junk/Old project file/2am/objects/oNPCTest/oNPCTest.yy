{
    "id": "07e3f979-6a52-472f-b2a6-3878a0f3a30e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPCTest",
    "eventList": [
        {
            "id": "9706c79e-f0e7-408b-9e80-17cd476670ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "07e3f979-6a52-472f-b2a6-3878a0f3a30e"
        },
        {
            "id": "d09bd5b9-2aa3-40ee-a14d-c819e5743237",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "07e3f979-6a52-472f-b2a6-3878a0f3a30e"
        },
        {
            "id": "17255abf-7b7f-4e3b-952d-494f5804eace",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "07e3f979-6a52-472f-b2a6-3878a0f3a30e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}