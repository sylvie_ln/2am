draw_sprite_ext(array[count],0,room_width div 2,room_height div 2,4,4,0,c_white,1);
var name = sprite_get_name(array[count]);
name = string_extract(name,2,string_length(name));
for(var i=ord("A"); i<ord("Z"); i++) {
	name = string_char_at(name,1)+string_replace_all(string_extract(name,2,string_length(name)),chr(i)," "+chr(i));	
}
draw_set_font(global.sylvie_font_bold_2x);
draw_set_color(c_white);
draw_text_centered(room_width div 2,room_height-32,name);

