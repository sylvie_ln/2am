event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;
depth += 4;

script = "An authentic Minecart License, those who possess it do have the skill to drive a minecart."
image_speed = 0;