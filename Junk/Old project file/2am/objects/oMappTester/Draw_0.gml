draw_set_color(c_white);
draw_set_color(c_dkgray);
for(var i=0; i<room_width; i+=scale) {
	draw_line(i,0,i,room_height);
	for(var j=0; j<room_height; j+=scale) {
		draw_line(0,j,room_width,j);
	}
}
if !surface_exists(surfy) {
	surfy = surface_create(512,512);	
}
if surface_exists(surfy) {
	surface_set_target(surfy);
	draw_clear_alpha(c_black,0);
	var cx = 0;
	var cy = 0;
	var cw = 16*scale;
	var ch = 8*scale;
	var c = 1*scale;
	var co = 2;
	var ng = node[ninfo.data];
	var nw = ds_grid_width(ng);
	var nh = ds_grid_height(ng);
	for(var i=0; i<nw; i++) {
		for(var j=0; j<nh; j++) {
			var cell = [x+co+1+cx*cw,y+co+1+cy*ch];
			draw_set_color(c_white);	
			draw_rectangle(cell[0]+c,cell[1]+c,cell[0]+cw-c,cell[1]+ch-c,false);
			var list = ng[# nmod(i,nw),nmod(j,nh)];
			var degree = (list & 1 == 1) + (list & 2 == 2) + (list & 4 == 4) + (list & 8 == 8);
			draw_set_color(c_gray);	
			draw_set_font(global.sylvie_font_bold);
			draw_text_centered(cell[0]+(cw div 2),cell[1]+scale,string(degree));
			draw_set_color(c_white);	
			var type = list & 15;
			if type & edir.up == edir.up {
				var amount = floor(log2(edir.up))*8;
				var pos =  (((list >> 4) >> amount) & 15)*scale;
				var open = (((list >> 8) >> amount) & 15)*scale;
				draw_rectangle(cell[0]+pos,cell[1],cell[0]+pos+open,cell[1]+c,false);
			}
			if type & edir.down == edir.down {
				var amount = floor(log2(edir.down))*8;
				var pos =  (((list >> 4) >> amount) & 15)*scale;
				var open = (((list >> 8) >> amount) & 15)*scale;
				draw_rectangle(cell[0]+pos,cell[1]+ch-c,cell[0]+pos+open,cell[1]+ch,false);
			}
			if type & edir.left == edir.left {
				var amount = floor(log2(edir.left))*8;
				var pos =  (((list >> 4) >> amount) & 15)*scale;
				var open = (((list >> 8) >> amount) & 15)*scale;
				draw_rectangle(cell[0],cell[1]+pos,cell[0]+c,cell[1]+pos+open,false);
			}
			if type & edir.right == edir.right {
				var amount = floor(log2(edir.right))*8;
				var pos =  (((list >> 4) >> amount) & 15)*scale;
				var open = (((list >> 8) >> amount) & 15)*scale;
				draw_rectangle(cell[0]+cw,cell[1]+pos,cell[0]+cw-c,cell[1]+pos+open,false);
			}
			cy++;
		}
		cx++;
		cy = 0;
	}
	surface_reset_target();
	draw_surface_ext(surfy,0,0,1,1,0,c_white,0.75);
}