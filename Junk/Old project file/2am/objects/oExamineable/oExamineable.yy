{
    "id": "27f4be64-31ee-4711-9f61-3aa0161ee194",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oExamineable",
    "eventList": [
        {
            "id": "215f2075-5ca3-4700-a569-f8d2249162c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27f4be64-31ee-4711-9f61-3aa0161ee194"
        },
        {
            "id": "fe39af44-7c6b-463f-9f8e-bce30fb246fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "27f4be64-31ee-4711-9f61-3aa0161ee194"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "65ef3ec7-0bb9-430d-b919-9b85a40a68c9",
    "visible": false
}