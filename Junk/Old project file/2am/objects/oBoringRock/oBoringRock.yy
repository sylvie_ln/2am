{
    "id": "1164541c-a72f-45b1-93d0-414388b1768a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBoringRock",
    "eventList": [
        {
            "id": "a318e81e-09b1-45d8-b85b-0f6ecd585ea6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1164541c-a72f-45b1-93d0-414388b1768a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "13db29d6-39fd-4350-b598-8d1391d6d6bc",
    "visible": true
}