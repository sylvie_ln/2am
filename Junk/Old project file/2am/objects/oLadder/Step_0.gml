if on {
	sprite_index = sLadderOn;
	upblocker=true;
	image_alpha = 1;	
} else {
	sprite_index = sLadderOff;
	upblocker=false;
	if bag_has_item(oLadderOrb) or instance_exists(oLadderOrb) {
		image_alpha = 0.5;	
	} else {
		image_alpha = 0;	
	}
}