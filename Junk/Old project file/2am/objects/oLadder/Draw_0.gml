if !surface_exists(surfy) {
	surfy = surface_create(npow2(16*image_yscale),npow2(16*image_yscale));	
	surface_set_target(surfy);
	draw_clear_alpha(c_black,0);
	surface_reset_target();
}
	
if surface_exists(surfy) {
	surface_set_target(surfy);
	for(var i=0; i<image_yscale; i++) {
		draw_sprite(sprite_index,image_index,0,i*16);
	}
	draw_set_color(c_white);
	if top {
		draw_line(0,0,15,0);
	}
	surface_reset_target();
	draw_surface_ext(surfy,x,y,1,1,0,c_white,image_alpha);
}