{
    "id": "fcf94ef4-1091-40ac-ab48-031bf0cc21ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLadder",
    "eventList": [
        {
            "id": "fa00d7b0-0048-4245-a747-84a87acc71a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fcf94ef4-1091-40ac-ab48-031bf0cc21ba"
        },
        {
            "id": "8431bcdb-017a-4d3e-aeaf-a3542303a8ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fcf94ef4-1091-40ac-ab48-031bf0cc21ba"
        },
        {
            "id": "6d1852ac-dd29-41c7-9968-4e95b097f116",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "fcf94ef4-1091-40ac-ab48-031bf0cc21ba"
        },
        {
            "id": "42cbe6c8-8ff5-4c7a-b0f5-080a923d9df4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fcf94ef4-1091-40ac-ab48-031bf0cc21ba"
        },
        {
            "id": "0f8c0585-4c0b-4a50-87d9-1c835e8c003b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "16d0232f-308c-4d78-ab35-6c0f35b4d410",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fcf94ef4-1091-40ac-ab48-031bf0cc21ba"
        },
        {
            "id": "06137796-539c-45e4-85b3-b2cada29ecd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "fcf94ef4-1091-40ac-ab48-031bf0cc21ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c2c37f2f-efeb-49ff-ac2c-48feb39779e3",
    "visible": true
}