// Inherit the parent event
event_inherited();

if bag_has_item(asset_get_index(required_hand)) {
	draw_sprite_ext(sHands, image_index, x, y, 1, 1, 0, -1, (sin(current_time / 1000 + x) + 1) / 2);
}