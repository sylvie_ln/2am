event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "An extremely tall shroom, looks like lamp a bit."

damp = 0.5;
bounce = damp;
fric = 0.95;
depth += 3;

if room != rmCoolMuseum {
	image_index = sylvie_irandom(sprite_get_number(sprite_index)-1);
}
image_speed = 0;