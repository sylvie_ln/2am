{
    "id": "5e0d7429-5e74-415a-adc8-5778601e9064",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCursor",
    "eventList": [
        {
            "id": "9c1e52a8-3087-4817-9cea-0e4b21880269",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5e0d7429-5e74-415a-adc8-5778601e9064"
        },
        {
            "id": "a5404219-e14f-481a-a921-fb1424cba605",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5e0d7429-5e74-415a-adc8-5778601e9064"
        },
        {
            "id": "dbc45a19-cc71-4712-b293-e56568c17027",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5e0d7429-5e74-415a-adc8-5778601e9064"
        },
        {
            "id": "6dc452ed-e262-437d-88e2-0235d0edb7be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5e0d7429-5e74-415a-adc8-5778601e9064"
        },
        {
            "id": "42aea616-dbf7-4080-8479-548e34cf43eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5e0d7429-5e74-415a-adc8-5778601e9064"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}