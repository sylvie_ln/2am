var moved = input_pressed("Left") or input_pressed("Right") or input_pressed("Up") or input_pressed("Down");
var pressed = input_pressed("Jump");
if (moved or (pressed and !wait_until_moved)) and !active {
	if alarm[0] >= 0 {
		alarm[1] = alarm[0]; 
	} else {
		event_user(1);
	}
}
if wait_until_moved and pressed { wait_until_moved = false; }
if mouse_check_button_pressed(mb_left) { active = false; wait_until_moved = true; }
if !active { exit; }
var spd = 4 * global.scale;
if input_held("Jump") {
	spd = global.scale;
}	
if input_held("Left") {
	display_mouse_set(display_mouse_get_x()-spd,display_mouse_get_y());
}
if input_held("Right") {
	display_mouse_set(display_mouse_get_x()+spd,display_mouse_get_y());
}
if input_held("Up") {
	display_mouse_set(display_mouse_get_x(),display_mouse_get_y()-spd);
}
if input_held("Down") {
	display_mouse_set(display_mouse_get_x(),display_mouse_get_y()+spd);
}