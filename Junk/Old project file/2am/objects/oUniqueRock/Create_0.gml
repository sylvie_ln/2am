event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A lovely, unique rock, unlike any other in the world, that has the shape of a sweet Kitty and glows with a sparkle."

damp = 0.5;
bounce = damp;
fric = 0.95;
//grav *= 2;


image_speed = 0;