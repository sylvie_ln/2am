{
    "id": "f8da4d68-59bb-4cba-8dd2-2f3fe69542ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitle",
    "eventList": [
        {
            "id": "08c87d1f-03ee-492e-b55e-d374ed458764",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f8da4d68-59bb-4cba-8dd2-2f3fe69542ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e084c276-ace0-48d3-8739-39428b8e6169",
    "visible": false
}