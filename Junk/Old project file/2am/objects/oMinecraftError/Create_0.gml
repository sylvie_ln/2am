stop_bgm();
layer_hspeed("Background",0);
with oMinecraftBlock {
	spd = 0;
	alarm[0] = -1;
}	
with oDiamond {
	spd = 0;
	
}
with oMinecraft {
	paused = true;	
	image_speed = 0;
}
with oDiamondBurst {
	image_speed = 0;	
	spd = 0;
}
with oMinecraftPoints {
	gravity = 0;
	vspeed = 0;
}
with oMinecraftController {
	instance_destroy();
}	
image_xscale = 0;
image_yscale = 0;
time = global.default_room_speed*2;
sc = 0;
alarm[0] = global.default_room_speed;
image_speed = 0;