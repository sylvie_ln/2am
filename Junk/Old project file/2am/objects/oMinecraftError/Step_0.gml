if image_xscale < 1 {
	image_xscale += sc;
	if image_xscale > 1 {
		image_xscale = 1;	
	}
}
if image_yscale < 1 {
	image_yscale += sc;
	if image_yscale > 1 {
		image_yscale = 1;	
	}
}
with oCursor { event_user(0); }
if collision_point(mouse_x,mouse_y,object_index,false,false) {
	if input_released("Jump") { event_perform(ev_mouse,ev_left_release); }
	else if input_held("Jump") { event_perform(ev_mouse,ev_left_button); }
}