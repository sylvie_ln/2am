{
    "id": "675b1636-77e2-4dc4-828d-5e96071859db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinecraftError",
    "eventList": [
        {
            "id": "43a130ee-6474-4477-9b1f-d819e0f606d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "675b1636-77e2-4dc4-828d-5e96071859db"
        },
        {
            "id": "e4918d32-5a2f-436d-ae24-964ba5717d16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "675b1636-77e2-4dc4-828d-5e96071859db"
        },
        {
            "id": "6c352dda-02b9-4eaa-88b7-999b3f8ea096",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "675b1636-77e2-4dc4-828d-5e96071859db"
        },
        {
            "id": "8d0e8752-1c82-4cf4-bc3d-b842a85cf349",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "675b1636-77e2-4dc4-828d-5e96071859db"
        },
        {
            "id": "3fe6f787-36da-4ab7-8c5d-93ae0fde2b8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "675b1636-77e2-4dc4-828d-5e96071859db"
        },
        {
            "id": "1a4a4442-b55b-4da1-a20e-632f07bd38af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "675b1636-77e2-4dc4-828d-5e96071859db"
        },
        {
            "id": "48698a05-2825-435b-a9af-c3bd51e58ad4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "675b1636-77e2-4dc4-828d-5e96071859db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8c756fde-75c6-49a3-9c8c-fe1ecd59bf20",
    "visible": true
}