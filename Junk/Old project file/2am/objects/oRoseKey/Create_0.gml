event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A special kind of key with a flower motif. Who knows what amazing things it will unlock?"

damp = 0.75;
bounce = damp;
fric = 0.95;
grav /= 2

image_speed = 0;

/// In oInit, define a trade entry for this item!