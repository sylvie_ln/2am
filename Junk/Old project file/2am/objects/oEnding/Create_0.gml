phase = 0;
factor = 0;
spd = 0.1;
acc = 0.1;
u = 1000000;
time = 0;

all_hands = 
global.flag_door_darkness
and global.flag_door_dreams
and global.flag_door_earth
and global.flag_door_flames
and global.flag_door_leafs
and global.flag_door_lies
and global.flag_door_light
and global.flag_door_moon
and global.flag_door_sky
and global.flag_door_storms
and global.flag_door_tears
and global.flag_door_thunder;
//all_hands = true;
var i = 0;
t[i++] = 10750000;
t[i++] = 12575000;
t[i++] = 16000000;
t[i++] = 24000000;
t[i++] = 32000000;
t[i++] = 40000000;
t[i++] = 48000000;
t[i++] = 56000000;
t[i++] = 64000000;
t[i++] = 72000000;
t[i++] = 80000000;
t[i++] = 88000000;
t[i++] = 96000000;
t[i++] = 104000000;
t[i++] = 112000000;
t[i++] = 120000000;
t[i++] = 128000000;
t_total = i;
time_total = t[t_total-1];
i = 0;
death = 128000000 + (u*3);
text[i++] = [
"Credits",
""
];
text[i++] = [
"Gamedesign Princess",
"Sylvie"
];
text[i++] = [
"Gamedesign Consultant",
"Hubol"
]
text[i++] = [
"Programming Lead",
"Sylvie"
]
text[i++] = [
"Programming Assistant",
"Hubol"
]
text[i++] = [
"Characters and Story",
"by Sylvie"
]
text[i++] = [
"Great Ideas",
"by Hubol"
]
text[i++] = [
"Music Arrange & Vocals",
"by Hubol"
]
text[i++] = [
"Two Secret Songs",
"by Sylvie"
]
text[i++] = [
"Lovely Lyrics",
"by Sylvie"
]
text[i++] = [
"Lyrics Helper",
"Hubol"
]
text[i++] = [
"All the Sound Effects",
"by Hubol"
]
text[i++] = [
"All the Graphics",
"by Sylvie"
]
if all_hands {
	text[i++] = [
	"",
	"She spent a lot of time creating this world.\nThank you for your gentle exploration."
	]
	text[i++] = [
	"",
	"Maybe you'll meet again in some far away time.\nUntil then, please keep her in your memory."
	]
} else {
	text[i++] = [
	"",
	"Though the flow of time has been restored,\nit is a little stilted and uneven."
	]
	text[i++] = [
	"",
	"Perhaps there's more to discover in this world.\nBut for now, have a good rest."
	]
}
text_total = i;
text_pos = 0;

stop = 0;
char_pos = -16;
char_start = 32;
char_end = 91; //2*(room_height div 3)-8;

eivlys = false;
eivlys_offset = 4;
eivlys_offset2 = 4;
eivlys_start_y = room_height + 32;
eivlys_y = eivlys_start_y;
dancing = false;