if phase == 1 {
	draw_set_color(make_color_rgb(0xEE,0x1C,0x24));
	var vpos = 99;	
	draw_line(-1,vpos,factor,vpos);
	draw_line(room_width-1-factor,vpos,room_width-1,vpos);
} if phase > 1 {
	if !dancing { sprite_index = sCharR; }
	draw_set_color(c_white);
	if text_pos < text_total {
		var a = text[text_pos];
		var hpos = 8
		var vpos = 2*(room_height div 3)+8;
		var dist = t[6] - t[5];
		var range = (0.25 * dist);
		var lerpo = lerp(room_height-vpos-4,-20,power(min((dist),2*(time-t[phase-1]))/(dist),1));
		if phase == 2 { lerpo = 0; }
		else if t[phase]-time < range {
			draw_set_alpha(sqr((t[phase]-time)/range));
			if time > time_total {
				draw_set_alpha(0);	
			}
		}
		draw_set_font(global.sylvie_font_bold);
		if lerpo == 0 {
			draw_text_centered(room_width div 4,vpos+lerpo,a[0]);
		} else{
			draw_text(hpos,vpos+lerpo,a[0]);
		}
		draw_set_font(global.sylvie_font);
		if text_pos >= text_total-2 {
			draw_text(8,room_height-string_height(a[1])-8,a[1]);
		} else {
			draw_text(hpos,vpos+16+lerpo,a[1]);
		}
		draw_set_alpha(1);	
		//draw_set_valign(fa_top);
	}
}
var cx = room_width div 5;
if !dancing {
	if abs(char_pos-char_start)<0.4 {
		if stop == 0 {
			stop = time;	
		}
		draw_sprite(sprite_index,image_index,
				cx,
				lerp(char_start,char_end,min(time-stop,t[1]-stop)/(t[1]-stop)));
	} else {
		char_pos += (char_start - char_pos) * 0.1;
		draw_sprite(sprite_index,image_index,
				cx,
				char_pos);
	}
}
if !all_hands { exit; }
if eivlys {
	var eivlys_final_y = char_end+eivlys_offset2-1;
	if abs(eivlys_y-eivlys_final_y)>0.4 {
		eivlys_y += (eivlys_final_y-eivlys_y) * 0.05;
		draw_sprite(sEivlysStill,image_index,cx+eivlys_offset,eivlys_y);
	} else {
		if !dancing {
			dancing = true;
			sprite_index = sEivlysDance;
		}
		draw_sprite(sprite_index,image_index,cx+eivlys_offset,eivlys_final_y);
	}
}