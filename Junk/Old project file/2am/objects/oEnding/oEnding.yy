{
    "id": "396708fb-9d79-496e-8555-e49ce72b3db0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnding",
    "eventList": [
        {
            "id": "05b573be-2a64-4105-8178-6c2a2d421f18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "396708fb-9d79-496e-8555-e49ce72b3db0"
        },
        {
            "id": "389bf0b7-fb37-4a5a-a0e6-aaebe84b2ac6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "396708fb-9d79-496e-8555-e49ce72b3db0"
        },
        {
            "id": "fe5763cb-0c86-411b-bc0b-edceadecd843",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "396708fb-9d79-496e-8555-e49ce72b3db0"
        },
        {
            "id": "6d30d42b-b490-4cc0-b88d-8158124eea9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "396708fb-9d79-496e-8555-e49ce72b3db0"
        },
        {
            "id": "b854dd26-3229-4a4d-975e-bfc03a73ea48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 22,
            "eventtype": 7,
            "m_owner": "396708fb-9d79-496e-8555-e49ce72b3db0"
        },
        {
            "id": "9f27c26e-f521-48c3-ad90-b412f4097803",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "396708fb-9d79-496e-8555-e49ce72b3db0"
        },
        {
            "id": "d046700f-3214-4748-9c04-68389c33b61e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "396708fb-9d79-496e-8555-e49ce72b3db0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4b8cfc90-25d6-49a2-9dfd-32553d3152b2",
    "visible": true
}