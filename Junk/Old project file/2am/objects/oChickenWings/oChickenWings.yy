{
    "id": "2d5cad4f-275f-4111-a73a-eeedb25965dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChickenWings",
    "eventList": [
        {
            "id": "c3ee7839-f302-4c8a-a1d5-9478ec75bd2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d5cad4f-275f-4111-a73a-eeedb25965dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94d8666a-88f2-4422-8ee6-9b7315fed53f",
    "visible": true
}