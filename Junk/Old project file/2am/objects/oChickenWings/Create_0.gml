event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "The legendary Chicken Wings, they allow you to soar through the sky like a majestic chicken."

damp = 0.9;
bounce = damp;
fric = 0.95;
grav /= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!