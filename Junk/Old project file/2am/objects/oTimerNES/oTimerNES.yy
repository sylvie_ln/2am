{
    "id": "32c792c2-2f8f-4de9-9f2c-5d11360cd265",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTimerNES",
    "eventList": [
        {
            "id": "382547fb-0860-4240-b7e2-f2d27a6998c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32c792c2-2f8f-4de9-9f2c-5d11360cd265"
        },
        {
            "id": "14eb466a-f098-4463-a8d9-d0c071606130",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32c792c2-2f8f-4de9-9f2c-5d11360cd265"
        },
        {
            "id": "e00af055-8b23-4e3a-9c81-016c0acc7f89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "32c792c2-2f8f-4de9-9f2c-5d11360cd265"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}