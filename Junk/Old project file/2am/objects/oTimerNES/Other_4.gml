if room == rmSecretTomb and !global.mute_music {
	audio_stop_sound(bgmSecretTomb);
	audio_play_sound(bgmSecretTomb,1000,false);
	audio_sound_gain(bgmSecretTomb,1,0);
}