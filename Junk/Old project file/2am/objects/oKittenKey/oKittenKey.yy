{
    "id": "bfbdd606-3771-45d8-a428-c7b3e0d9500c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKittenKey",
    "eventList": [
        {
            "id": "54e63a4b-4249-4861-a0d6-f00fc776026b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bfbdd606-3771-45d8-a428-c7b3e0d9500c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "52174c26-4512-4f8d-9247-b8313f91f5e8",
    "visible": true
}