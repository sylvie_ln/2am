event_inherited();

upblocker = false;
pushable = false;
carriable = false;
pusher = false;
carrier = false;

script = "One of the legendary four Puzzle Pieces hidden by the Puzzler! I wonder what happens when you bring all four together."

damp = 0.75;
bounce = damp;
fric = 0.95;

if room == rmCoolMuseum {
	visible = true;	
	upblocker = true;
	pushable = true;
	carriable = true;
	pusher = true;
	carrier = true;
}

switch(room) {
	case rmCylveyCitey:
		image_index = 0;
		break;
	case rmCherryBlossomHills:
		image_index = 1;
		break;
	case rmCanyonCliffs:
		image_index = 2;
		break;
	case rmCavernOfAncients:
		image_index = 3;
		break;
}
image_speed = 0;

/// In oInit, define a trade entry for this item!