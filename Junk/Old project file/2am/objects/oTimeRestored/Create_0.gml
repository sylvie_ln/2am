if !global.mute_sfx {
	rumble_sound = audio_play_sound(sndTowerPeakRumble, -1, true);
} else {
	rumble_sound = noone;	
}
play_sound(sndTimeRestored);

tick = 0;

with instance_create_depth(0,0,0,oFlash) {
	event_perform(ev_other,ev_room_start);
	with oFogg {
		instance_destroy();	
	}
}	
with oTheBell {
	ring = true;	
}

with oBiggHanddss {
	rot = true;	
}

with oClockSpike {
	shaking = true;	
	alarm[0] = time;
}

bg = layer_background_get_id("Background");
starz = layer_background_get_id("Starz");
layer_background_visible(starz,false);

colorspd = 2;

with oDoor {
	instance_change(oFinaleDoor,true)
	script = "Thank you from the deepest, most rotten depths of my heart."
	old_script = script;
	sad = "The passage undulates wildly! There's no going back.\nLooks like you're going to have to jump."
	image_speed *= 4;
	event_user(0);
}