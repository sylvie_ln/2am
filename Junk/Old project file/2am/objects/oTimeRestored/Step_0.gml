layer_background_blend(bg,
make_color_hsv(
color_get_hue(layer_background_get_blend(bg))+colorspd,
color_get_saturation(layer_background_get_blend(bg)),
color_get_value(layer_background_get_blend(bg)))
)

tick++;
if (tick % (global.default_room_speed * 2) == 0)
	play_sound(sndTowerPeakTick2);
else if (tick % global.default_room_speed == 0)
	play_sound(sndTowerPeakTick1);
	
if !global.mute_sfx {
	if (!instance_exists(oClockSpike) && rumble_sound != noone) {
		audio_sound_gain(rumble_sound, 0, 4000);
		rumble_sound = noone;
	}
}