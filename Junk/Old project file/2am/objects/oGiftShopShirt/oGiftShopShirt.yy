{
    "id": "831e2a31-62df-4110-a1c9-cef4d58a5d58",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGiftShopShirt",
    "eventList": [
        {
            "id": "5fb8acd0-11fd-4245-863a-22adc9123eb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "831e2a31-62df-4110-a1c9-cef4d58a5d58"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7f7d6fe-a10f-4e28-9a25-5c366d88d4fc",
    "visible": true
}