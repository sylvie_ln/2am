///@description Memoris!

mem = [
// Creative Laughter (Cherry low)
["Cherie", "Sweets", "Uuella", "Lamia", "Geniux"],
// Cackling Bust (Canyon near entrance)
["Tressa","Canyetta","Crisis","Mineria","Wirte"],
// Cornered Wilderness (Cavern upper left)
["Gooney","Cavey","Bunnisse","Archaea","Graal"],
// Corpse Facade (Clock)
["Byclock","Uncomus","Beleve","Mooncry"],
// Cordless Funeral (Cavern upper right)
["Slash","Wizaron","Alicia","Zupzel",],
// Crumbled Hearth (Canyon lower left)
["Junky","Laborie","Searchey","Truckster"],
// Cuter Willows (Cherry lower right)
["Creamy","Arianne","Plunch","Katey"],
// Captive Automaton (Cavern lower right)
["Flaire","Bella","Country","Lady"],
// Careless Desire (Cherry left)
["Sunny","Karie","TroubleAndStrife","Princess"],
// Caustic Puddle (Canyon upper)
["Rainbis","Rockout","Jealousy","Chestor"],
// Cataclysmic Fountain (Citey lower)
["Stocs","Mococo","Pipi","Galfio"],
// Cloudy Mixture (Citey upper)
["Lockme","Stormy","Chicen","Queen"],
]


item = [
// Creative Laughter (Cherry low)
[oMiniBunney,oSnakeRope,oJIGGLER,oBunneySlippers,oBunneyEars,],
// Cackling Bust (Canyon near entrance)
[oBoringRock,oSylviePlushey,oBottledFlare,oGiftShopSnowGlobe,[-1,oTinyShroom]],
// Cornered Wilderness (Cavern upper left)
[oGiftShopPostcard,oGiftShopCoffeeMug,oDrummerPlushey,oGiftShopShirt,oGiftShopCavernRock],
// Corpse Facade (Clock)
[oJunkParts,oSpringSneakers,oClimbingCleats,[-1,oFloatShroom],oFishCar],
// Cordless Funeral (Cavern upper right)
[oBottledFlare,oTrendyTote,[-1,oSpeedShroom],oWizardmobile,oPegasusBoots],
// Crumbled Hearth (Canyon lower left)
[oJunkParts,oBoringRock,oJunkParts,oBottledFlare,[-1,oSpringShroom]],
// Cuter Willows (Cherry lower right)
[oBottledFlare,oGiftShopKitteyPlush,oKittenKey,oGuardianShield,[-1,oKittyShroom]],
// Captive Automaton (Cavern lower right)
[oDice,oJunkParts,oSnakeRope,[-1,oPoisonShroom],oMushroomQuesoGrande],
// Careless Desire (Cherry left)
[o100TonShoes,oJunkParts,oRoseKey,[-1,oSparkleShroom],oMagicCarrot],
// Caustic Puddle (Canyon upper)
[oTinyShroom,oSnakeRope,oBridgeOrb,oLadderOrb,[-1,oRainbowShroom]],
// Cataclysmic Fountain (Citey lower)
[oDollars,oRedPaperclip,oUniqueRock,oGuardianScimitar,oGenericItemTemplate],
// Cloudy Mixture (Citey upper)
[oJunkParts,oRegularKey,oJunkParts,oHoverHeels,oJunkParts,],
]