///@description Easygoing
var time = get_timer();

var nw = 2;
var nh = 2;

if fill == 0 {
	if i+j mod 7 == 0 {
		progress = clamp((j*w+i)/(2*w*h),0,1);
	}
} else {
	if c mod 7 == 0 {
		progress = clamp(((w*h)+c)/(2*w*h),0,1);
	}
}

switch(step) {
	case 0: // Creative Laughter (Cherry)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 4;
		//            x    y   x   y   y       x
		var arrs = [[50],[50],[5],[5],[2,3],[2,3]];
		var links = [25,16,2,1,1,1];
		var ow = [4,6];
		var oh = [3,5];
	break;
	case 1: // Cackling Bust (Canyon)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 4;
		var arrs = [[50],[50],[5],[5],[2,3],[2,3]];
		var links = [25,16,2,1,1,1];
		var ow = [5,8];
		var oh = [4,6];
	break;
	case 2: // Cornered Wilderness (Cavern)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 4;
		var arrs = [[50],[50],[5],[5],[2,3],[2,3]];
		var links = [25,16,2,1,1,1];
		var ow = [3,5];
		var oh = [2,4];
	break;
	case 3: // Corpse Facade (Clock)
		door_freq = 1;
		unco_freq = 3;
		rare_freq = 6;
		var arrs = [[50],[50],[5],[5],[2,3],[3,2]];
		var links = [25,16,1,1,0,0];
		var ow = 0;
		var oh = 0;
	break;
	case 4: // Cordless Funeral (Speedshroom)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 6;
		var arrs = [[50],[50],[5],[5],[1],[5]];
		var links = [25,16,1,1,0,0];
		var ow = [3,5];
		var oh = [2,4];
	break;
	case 5: // Crumbled Hearth (Springshroom)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 4;
		var arrs = [[50],[50],[5],[5],[1],[5]];
		var links = [25,16,1,1,0,0];
		var ow = [5,8];
		var oh = [4,6];
	break;
	case 6: // Cuter Willows (KitteyShroom)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 6;
		var arrs = [[50],[50],[5],[5],[1],[5]];
		var links = [25,16,1,1,0,0];
		var ow = [4,6];
		var oh = [3,5];
	break;
	case 7: // Captive Automaton (Poisonshroom)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 6;
		var arrs = [[50],[50],[25],[5],[5],[1]];
		var links = [25,16,9,1,0,0];
		var ow = [4,6];
		var oh = [3,5];
	break;
	case 8: // Careless Desire (Sparkleshroom)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 6;
		var arrs = [[50],[50],[25],[5],[5],[1]];
		var links = [25,16,9,1,0,0];
		var ow = [5,8];
		var oh = [4,6];
	break;
	case 9: // Caustic Puddle (Rainbowshroom)
		door_freq = 1;
		unco_freq = 2;
		rare_freq = 6;
		var arrs = [[50],[50],[25],[5],[5],[1]];
		var links = [25,16,9,2,0,0];
		var ow = [3,5];
		var oh = [2,4];
	break;
	case 10: // Cataclysmic Fountain
		door_freq = 17;
		unco_freq = 3;
		rare_freq = 5;
		var arrs = [[50],[50],[5],[5],[4,1],[1,4]];
		var links = [25,16,1,1,0,0];
		var ow = 0;
		var oh = 0;
	break;
	case 11: // Cloudy Mixture
		door_freq = 13;
		unco_freq = 3;
		rare_freq = 7;
		var arrs = [[50],[50],[5],[5],[4,1],[4,1]];
		var links = [25,16,1,1,0,0];
		var ow = 0;
		var oh = 0;
	break;
}

while get_timer()-time < ((1/room_speed)/3)*1000000 {
	if fill == 0 {
		if !mapp_done {
			var hixlist = ds_list_create();
			var hixa = arrs[0];
			var hixl = array_length_1d(hixa);
			var hicx = 0;
			for(var hix=0; hix<w; hix+=hixa[(hicx++) mod hixl]) {
				//disabled_show_debug_message("hix: "+string(hix));
				var hiylist = ds_list_create();
				var hiya = arrs[1];
				var hiyl = array_length_1d(hiya);
				var hicy = 0;
				for(var hiy=0; hiy<h; hiy+=hiya[(hicy++) mod hiyl]) {
					//disabled_show_debug_message(" hiy: "+string(hiy));
					var mexlist = ds_list_create();
					var mexa = arrs[2]
					var mexl = array_length_1d(mexa);
					var mecx = 0;
					for(var	mex=hix; mex<hix+hixa[hicx mod hixl]; mex+=mexa[(mecx++) mod mexl]) {
						//disabled_show_debug_message("  mex: "+string(mex));
						var meylist = ds_list_create();
						var meya = arrs[3];
						var meyl = array_length_1d(meya);
						var mecy = 0;
						for(var	mey=hiy; mey<hiy+hiya[hicy mod hiyl]; mey+=meya[(mecy++) mod meyl]) {
							//disabled_show_debug_message("   mey: "+string(mey));
							var loylist = ds_list_create();
							var loya = arrs[4];
							var loyl = array_length_1d(loya);
							var locy = 0;
							for(var loy=mey; loy<mey+meya[mecy mod meyl]; loy+=loya[(locy++) mod loyl]) {
								//disabled_show_debug_message("    loy: "+string(loy));
								var loxlist = ds_list_create();
								var loxa = arrs[5];
								var loxl = array_length_1d(loxa);
								var locx = 0;
								for(var lox=mex; lox<mex+mexa[mecx mod mexl]; lox+=loxa[(locx++) mod loxl]) {
									//disabled_show_debug_message("     lox: "+string(lox));
									var nw = loxa[locx mod loxl];
									var nh = loya[locy mod loyl];
									var node = mapp_mxn(nw,nh,ow,oh,stepseed+sylvie_random(0,1024));
									node[ninfo.xp] = lox;
									node[ninfo.yp] = loy;
									//disabled_show_debug_message("     loxnode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
									ds_list_add(loxlist,node);
								}
								node = mapp_link(loxlist,stepseed+sylvie_random(0,1024),links[5]);
								node[ninfo.yp] = loy;
								//disabled_show_debug_message("    loynode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
								ds_list_add(loylist,node);
							}
							node = mapp_link(loylist,stepseed+sylvie_random(0,1024),links[4]);
							node[ninfo.xp] = mex;
							node[ninfo.yp] = mey;
							//disabled_show_debug_message("   meynode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
							ds_list_add(meylist,node);
						}
						node = mapp_link(meylist,stepseed+sylvie_random(0,1024),links[3]);
						node[ninfo.xp] = mex;
						//disabled_show_debug_message("  mexnode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
						ds_list_add(mexlist,node);
					}
					node = mapp_link(mexlist,stepseed+sylvie_random(0,1024),links[2]);
					node[ninfo.xp] = hix;
					node[ninfo.yp] = hiy;
					//disabled_show_debug_message(" hiynode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
					ds_list_add(hiylist,node);
				}
				node = mapp_link(hiylist,stepseed+sylvie_random(0,1024),links[1]);
				node[ninfo.xp] = hix;
				//disabled_show_debug_message("hixnode: "+string(node)+" "+pos_key(ds_grid_width(node[ninfo.data]),ds_grid_height(node[ninfo.data])));
				ds_list_add(hixlist,node);
			}
			mapp = mapp_link(hixlist,stepseed+sylvie_random(0,1024),links[0]);
		}
		switch(step) {
		    case 0: global.sylvie_seed = -5627848; break;
		    case 1: global.sylvie_seed = 270755750; break;
		    case 2: global.sylvie_seed = -1601121997; break;
		    case 3: global.sylvie_seed = 1085197338; break;
		    case 4: global.sylvie_seed = -1935107271; break;
		    case 5: global.sylvie_seed = 1884342148; break;
		    case 6: global.sylvie_seed = 204562096; break;
		    case 7: global.sylvie_seed = -449520369; break;
		    case 8: global.sylvie_seed = -2030662219; break;
		    case 9: global.sylvie_seed = 115897508; break;
		    case 10: global.sylvie_seed = -642726069; break;
		    case 11: global.sylvie_seed = -1416288349; break;
		}
		var oo = offsets[? stepname];
		fill_setup((w div 2)+oo[0], (h div 2)+oo[1]);
	}  else if fill == 1 {
		if all_done { fill = 2; } else {
			fill_loop(door_freq,unco_freq,rare_freq);
		}
	} else if fill > 1 {
		done = true;
		fill_cleanup();
		break;
	}
}

	/*
	if fill == 0 {
		if j == 0 and i == 0 {
			big = ds_list_create();
			list = ds_list_create();
			done = false;
		}
		if j < h and i < w {
			switch(step) {
				case 0: // Creative Laughter (Cherry)
					var node = mapp_mxn(nw,nh,[4,8],[4,6],stepseed+j*w+i);
				break;
				case 1: // Cackling Bust (Canyon)
					var node = mapp_mxn(nw,nh,[5,7],[4,6],stepseed+j*w+i);
				break;
				case 2: // Cornered Wilderness (Cavern)
					var node = mapp_mxn(nw,nh,[4,6],[3,4],stepseed+j*w+i);
				break;
				case 3: // Corpse Facade (Clock)
					var node = mapp_mxn(nw,nh,[8,12],[2,4],stepseed+j*w+i);
				break;
			}
			node[ninfo.xp] = i;
			node[ninfo.yp] = j;
			ds_list_add(list,node);	
			i += 2;
		} else if j < h and i >= w {
			var node = mapp_link(list,stepseed+j*w+i);
			node[ninfo.yp] = j;
			ds_list_add(big,node);
			j += 4;
			i = 0;
			if j < h {
				list = ds_list_create();
			}
		} else if j >= h {
			mapp = mapp_link(big,stepseed+j*w+i);
			fill_setup();
		}
	} else if fill == 1 {
		switch(step) {
			case 0: // Creative Laughter (Cherry)
				door_freq = 3;
				unco_freq = 2;
				rare_freq = 5;
			break;
			case 1: // Cackling Bust (Canyon)
				door_freq = 3;
				unco_freq = 2;
				rare_freq = 5;
			break;
			case 2: // Cornered Wilderness (Cavern)
				door_freq = 3;
				unco_freq = 2;
				rare_freq = 5;
			break;
			case 3: // Corpse Facade (Clock)
				door_freq = 4;
				unco_freq = 3;
				rare_freq = 7;
			break;
		}
		fill_loop(door_freq,unco_freq,rare_freq);
	}
	*/