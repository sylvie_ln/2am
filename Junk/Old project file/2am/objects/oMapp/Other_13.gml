/// @description Tower
var time = get_timer();
var big = ds_list_create();
for(var i=0; i<w; i+=1) {
	var list = ds_list_create();
	for(var j=0; j<h; j+=5) {
		var node = mapp_mxn(1,5,3,-4,stepseed+j*w+i);
		node[ninfo.xp] = i;
		node[ninfo.yp] = j;
		ds_list_add(list,node);
	}
	var node = mapp_link(list,stepseed+j*w+i);
	node[ninfo.xp] = i;
	ds_list_add(big,node);
}
mapp = mapp_link(big,stepseed+j*w+i);
fill_setup();
switch(step) {
	case 9: // Caustic Puddle (Rainbowshroom)
		door_freq = 5;
		unco_freq = 2;
		rare_freq = 9;
	break;
}
while(fill == 1) {
	fill_loop(door_freq,unco_freq,rare_freq);
}
fill_cleanup();
done = true;



/*
var bignode = mapp_link(big);
/*
var bnd = bignode[?"data"];
var bnw = ds_grid_width(bnd);
var bnh = ds_grid_height(bnd);
for(var i=0; i<bnw; i++) {
	for(var j=0; j<bnh; j++) {
		var bb = bnd[# i,j];
		if bb[|0] & edir.left == edir.left
		and bb[|0]& edir.right == edir.right {
			if j+1 < bnh {
				var bd = bnd[# i,j+1];
				if bb[|0] & edir.down == edir.down 
				and (bd[|0] & edir.left == edir.left
				  or bd[|0] & edir.right == edir.right) {
					bb[|0] -= edir.down;
					bd[|0] -= edir.up;
				}
			}
		}
	}
}
*/
/*
for(var i=0; i<bnw; i++) {
	for(var j=0; j<bnh; j++) {
		if bnd[# i,j] & edir.up != edir.up {
			var fluff = sylvie_choose(0,1,0,1,2);
			if i-1 >= 0 and (fluff == 0 or fluff == 2) {
				bnd[# i,j] |= edir.left;
				bnd[# i-1,j] |= edir.right;	
			}
			if i+1 < bnw and (fluff == 1 or fluff == 2) {
				bnd[# i,j] |= edir.right;
				bnd[# i+1,j] |= edir.left;	
			}
		}
	}
}
*/
//mapp_place(0,0,bignode);