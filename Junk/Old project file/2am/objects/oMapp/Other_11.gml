/// @description Corridor
var time = get_timer();

if fill == 0 {
	if i+j mod 7 == 0 {
		progress = clamp((j*w+i)/(2*w*h),0,1);
	}
} else {
	if c mod 7 == 0 {
		progress = clamp(((w*h)+c)/(2*w*h),0,1);
	}
}

while get_timer()-time < ((1/room_speed)/3)*1000000 {
	if fill == 0 {
		if i == 0 and k == 0 and j == 0 {
			big = ds_list_create();
		}
		if k < 10 {
			if i == 0 and j == (h/10)*k {
				klist = ds_list_create();
			}
			if i < w {
				if j == (h/10)*k {
					ilist = ds_list_create();
				}
				if j < (h/10)*(k+1) {
					switch(step) {
						case 4: // Cordless Funeral (Speedshroom)
							var node = mapp_mxn(5,1,3,-8,stepseed+j*w+i);
						break;
						case 5: // Crumbled Hearth (Springshroom)
							var node = mapp_mxn(5,1,4,-4,stepseed+j*w+i);
						break;
						case 6: // Cuter Willows (KitteyShroom)
							var node = mapp_mxn(5,1,6,-6,stepseed+j*w+i);
						break;
					}
					node[ninfo.xp] = i;
					node[ninfo.yp] = j;
					ds_list_add(ilist,node);
					j++;
				} else {
					var node = mapp_link(ilist,stepseed+j*w+i);
					node[ninfo.xp] = i;
					node[ninfo.yp] = (h/10)*k;
					ds_list_add(klist,node);
					i+=5;
					j=(h/10)*k;
				}
			} else {
				var node = mapp_link(klist,stepseed+j*w+i);
				node[ninfo.xp] = i;
				ds_list_add(big,node);
				k++;
				i = 0;
				j=(h/10)*k;
			}
		} else {
			mapp = mapp_link(big,stepseed+j*w+i);
			fill_setup();
		}
	} else if fill == 1 {
		switch(step) {
			case 4: // Cordless Funeral (Speedshroom)
				door_freq = 8;
				unco_freq = 2;
				rare_freq = 9;
			break;
			case 5: // Crumbled Hearth (Springshroom)
				door_freq = 6;
				unco_freq = 2;
				rare_freq = 11;
			break;
			case 6: // Cuter Willows (KitteyShroom)
				door_freq = 5;
				unco_freq = 2;
				rare_freq = 9;
			break;
		}
		fill_loop(door_freq,unco_freq,rare_freq);
	} else if fill > 1 {
		done = true;
		fill_cleanup();
		break;
	}
}