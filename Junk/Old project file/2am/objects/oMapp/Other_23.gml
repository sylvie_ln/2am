var u = 1000000;

if step == 0 {
	sylvie_set_seed(666);
	var name = "Creative Laughter"
	style = mapstyle.easygoing;
} else 
if step == 1 {
	sylvie_set_seed(12345);
	var name = "Cackling Bust"	
	style = mapstyle.easygoing;
} else
if step == 2 {
	sylvie_set_seed(101010);
	var name = "Cornered Wilderness"
	style = mapstyle.easygoing;
} else
if step == 3 {
	sylvie_set_seed(9);
	var name = "Corpse Facade"
	style = mapstyle.easygoing;
} else
if step == 4 {
	sylvie_set_seed(1066610);
	var name = "Cordless Funeral"
	style = mapstyle.easygoing;
} else
if step == 5 {
	sylvie_set_seed(123456);
	var name = "Crumbled Hearth"
	style = mapstyle.easygoing;
} else
if step == 6 {
	sylvie_set_seed(666666);
	var name = "Cuter Willows"
	style = mapstyle.easygoing;
} else
if step == 7 {
	sylvie_set_seed(1033310);
	var name = "Captive Automaton"
	style = mapstyle.easygoing;
} else
if step == 8 {
	sylvie_set_seed(363636);
	var name = "Careless Desire"
	style = mapstyle.easygoing;
} else
if step == 9 {
	sylvie_set_seed(1234567);
	var name = "Caustic Puddle"
	style = mapstyle.easygoing;
} else
if step == 10 {
	sylvie_set_seed(4444);
	var name = "Cataclysmic Fountain"
	style = mapstyle.easygoing;
} else
if step == 11 {
	sylvie_set_seed(2222);
	var name = "Cloudy Mixture"
	style = mapstyle.easygoing;
} 
else
if step == 12 {
	if dumpme {
		file_text_close(dump);	
	}
}

if state == "idle" {
	i = 0;
	j = 0;
	k = 0;
	c = 0;
	fill = false;
	done = false;
	state = "waiting";
	show_debug_message(name+" start "+string(get_timer()/u));
} else if state == "waiting" {;
	stepseed = random_get_seed();
	stepname = name;
	event_user(style);
	if done { 
		state = "done";
	}
} else if state == "done" {
	if !mapp_done {
		mapmapp[? name] = mapp[ninfo.data];
		cutey_write_grid(mapp[ninfo.data],"mapmapp[? \""+name+"\"]","0");
	}
	if !door_done {
		mapmapp[? name+"_door"] = doormapp;
		cutey_write_grid(doormapp,"mapmapp[? \""+name+"_door\"]","0");
	}
	if !item_done {
		mapmapp[? name+"_item"] = itemmapp;
		cutey_write_grid(itemmapp,"mapmapp[? \""+name+"_item\"]","0");
	}
	if !luck_done {
		mapmapp[? name+"_luck"] = luckmapp;
		cutey_write_grid(luckmapp,"mapmapp[? \""+name+"_luck\"]","0");
	}
	if !lore_done {
		mapmapp[? name+"_lore"] = loremapp;
		cutey_write_grid(loremapp,"mapmapp[? \""+name+"_lore\"]","\"\"");
	}
	if !cute_done {
		mapmapp[? name+"_cute"] = cutemapp;
		cutey_write_grid(cutemapp,"mapmapp[? \""+name+"_cute\"]","\"\"");
	}
	show_debug_message(name+" end "+string(get_timer()/u));
	if dumpme {
		file_text_close(dump);	
		dump = file_text_open_append("Sivy.txt");	
	}
	if !global.contract_signed {
		var startt = get_timer();
		var tt = 0;
		while tt < (5/12)*u {
			tt += get_timer()-startt;
			startt = get_timer();
		}
	}
	progress = 0;
	fluffy = sylvie_random(0,288/24);
	step++;
	state = "idle";
}