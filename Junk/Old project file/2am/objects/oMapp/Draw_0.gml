if !instance_exists(oRoomGen) { exit; }
if !bag_has_item(oMappingImplant) { exit; }
draw_set_color(c_white);
if !surface_exists(surfy) {
	surfy = surface_create(128,128);	
}
if surface_exists(surfy) {
	surface_set_target(surfy);
	draw_clear_alpha(c_black,0);
	var cx = 0;
	var cy = 0;
	var cw = 16*scale;
	var ch = 8*scale;
	var c = 1*scale;
	var co = 2;
	draw_set_color(c_black);
	draw_rectangle(x+co,y+co,x+co+(2*view+1)*(cw)+2,y+co+(2*view+1)*(ch)+2,false);
	draw_set_color(c_gray);
	draw_rectangle(x+co+1,y+co+1,x+co+(2*view+1)*(cw)+1,y+co+(2*view+1)*(ch)+1,false);
	for(var i=oRoomGen.xp-view; i<oRoomGen.xp+view+1; i++) {
		for(var j=oRoomGen.yp-view; j<oRoomGen.yp+view+1; j++) {
			var cell = [x+co+1+cx*cw,y+co+1+cy*ch];
			draw_set_color(c_ltgray);	
			if global.explored[? mush_key(i,j)] {
				draw_set_color(c_white);
			}
			draw_rectangle(cell[0]+c,cell[1]+c,cell[0]+cw-c,cell[1]+ch-c,false);
			var list = mapp[# nmod(i,w),nmod(j,h)];
			var type = list & 15;
			if type & edir.up == edir.up {
				var amount = floor(log2(edir.up))*8;
				var pos =  (((list >> 4) >> amount) & 15)*scale;
				var open = (((list >> 8) >> amount) & 15)*scale;
				draw_rectangle(cell[0]+pos,cell[1],cell[0]+pos+open,cell[1]+c,false);
			}
			if type & edir.down == edir.down {
				var amount = floor(log2(edir.down))*8;
				var pos =  (((list >> 4) >> amount) & 15)*scale;
				var open = (((list >> 8) >> amount) & 15)*scale;
				draw_rectangle(cell[0]+pos,cell[1]+ch-c,cell[0]+pos+open,cell[1]+ch,false);
			}
			if type & edir.left == edir.left {
				var amount = floor(log2(edir.left))*8;
				var pos =  (((list >> 4) >> amount) & 15)*scale;
				var open = (((list >> 8) >> amount) & 15)*scale;
				draw_rectangle(cell[0],cell[1]+pos,cell[0]+c,cell[1]+pos+open,false);
			}
			if type & edir.right == edir.right {
				var amount = floor(log2(edir.right))*8;
				var pos =  (((list >> 4) >> amount) & 15)*scale;
				var open = (((list >> 8) >> amount) & 15)*scale;
				draw_rectangle(cell[0]+cw,cell[1]+pos,cell[0]+cw-c,cell[1]+pos+open,false);
			}
			var draw_lore = false;
			var draw_item = false;
			var draw_rare = false;
			var draw_door = doormapp[# nmod(i,w),nmod(j,h)];
			var draw_char = (i == oRoomGen.xp and j == oRoomGen.yp);
			if (bag_has_item(oIllegalUpgradeChip)) {
				if loremapp[# nmod(i,w),nmod(j,h)] != "" {
					draw_lore = true;	
				}
				if cutemapp[# nmod(i,w),nmod(j,h)] != "" {
					draw_item = true;	
				}
			}
			var lucky = false;
			if global.flag_dice != 0 { lucky = true; } 
			if (bag_has_item(oUpgradeChip))  and itemmapp[# nmod(i,w),nmod(j,h)] == 1 
			and (luckmapp[# nmod(i,w),nmod(j,h)] == 1 or lucky) {
				draw_rare = true;
			}
			
			var draw_count = draw_char + draw_lore + draw_item + draw_rare + draw_door;
			
			var rare_removed =
				ds_map_exists(global.rare_dead,oRoomGen.area+"_"+room_key(i,j));
			var item_removed =
				ds_map_exists(global.cute_dead,oRoomGen.area+"_"+room_key(i,j));
			var lore_removed =
				ds_map_exists(global.memory_dead,loremapp[# nmod(i,w),nmod(j,h)]);
			
			if draw_rare {
				draw_sprite_ext(sMappRare,0,max(0,draw_count-2)+cell[0]+(((max(1,draw_count-1)+(draw_char and !(draw_item or draw_lore)))*cw)/(draw_count+1)),cell[1]+ch/2,
				1,1,0,rare_removed ? c_dkgray : c_white, 1);
				/*var previous_color = draw_get_color();
				var illegal_color = merge_color(c_fuchsia,merge_color(c_aqua,c_blue,0.5),0.5);
				draw_set_color(merge_color(previous_color,illegal_color,0.5));	
				draw_rectangle(cell[0]+c,cell[1]+c,cell[0]+cw-c,cell[1]+ch-c,false);
				draw_set_color(merge_color(previous_color,merge_color(illegal_color,c_black,0.5),0.5));	
				draw_rectangle(cell[0]+c,cell[1]+c,cell[0]+cw-c,cell[1]+ch-c,true);
				draw_set_color(previous_color);	*/
			}
			 
			if draw_lore {
				draw_sprite_ext(sMappLore,0,max(0,draw_count-1)+cell[0]+((draw_count*cw)/(draw_count+1)),cell[1]+ch/2,
				1,1,0,lore_removed ? c_dkgray : c_white, 1);
			}
			if draw_item {
				draw_sprite_ext(sMappItem,0,max(0,draw_count-1)+cell[0]+((draw_count*cw)/(draw_count+1)),cell[1]+ch/2,
				1,1,0,item_removed ? c_dkgray : c_white, 1);
			}
			
			if draw_char {
				draw_sprite(sMappMe,0,max(0,draw_count-3)+cell[0]+((max(1,draw_count-2)*cw)/(draw_count+1)),cell[1]+ch/2);
			}
			
			//if bag_has_item(oUpgradeChip) or draw_me {
			if draw_door {
				draw_sprite(sMappDoor,0,max(0,draw_count-1)+cell[0]+((draw_count*cw)/(draw_count+1)),cell[1]+ch/2);
			}
		//}
			cy++;
		}
		cx++;
		cy = 0;
	}
	surface_reset_target();
	draw_surface_ext(surfy,0,0,1,1,0,c_white,0.75);
}