///@description Untamed
mapp = mapp_mxn_wild(w,h,stepseed);
fill_setup();
switch(step) {
	case 10: // Cloudy Mixture
		door_freq = 13;
		unco_freq = 3;
		rare_freq = 4;
	break;
	case 11: // Cataclysmic Fountain
		door_freq = 17;
		unco_freq = 3;
		rare_freq = 4;
	break;
}
while(fill == 1) {
	fill_loop(door_freq,unco_freq,rare_freq);
}
fill_cleanup();
done = true;