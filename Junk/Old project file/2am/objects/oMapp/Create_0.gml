event_user(12);
w = 100;
h = 100;
scale = 1;
view = 2;
surfy = -1;
step = 0;
state = "idle";
mapmapp = ds_map_create();
progress = 0;
fluffy = 0;
/*
if !variable_global_exists("style") {
	global.style = 0;	
} else {
	global.style++;
	global.style = global.style mod mapstyle.last;
}

event_user(global.style);
*/
startx = 0;
starty = 0;

done = false;
depth = -996;

enum mapstyle {
	easygoing = 0,
	corridor = 1,
	pitfall = 2,
	tower = 3,
	untamed = 4,
	last = 5,
}

dumpme = false;
if dumpme {
	dump = file_text_open_write("Sivy.txt");
}
fluffesque();
mapp_done = true and !dumpme;
door_done = true and !dumpme;
item_done = true and !dumpme;
luck_done = true and !dumpme;
lore_done = true and !dumpme;
cute_done = true and !dumpme;
all_done = true;

offsets = ds_map_create();
offsets[?"Creative Laughter"] = [8,8];
offsets[?"Cackling Bust"] = [7,7];
offsets[?"Cornered Wilderness"] = [7,7];
offsets[?"Corpse Facade"] = [7,7];
offsets[?"Cordless Funeral"] = [5,8];
offsets[?"Crumbled Hearth"] = [7,7];
offsets[?"Cuter Willows"] = [5,7];
offsets[?"Captive Automaton"] = [7,5];
offsets[?"Careless Desire"] = [7,5];
offsets[?"Caustic Puddle"] = [7,9];
offsets[?"Cataclysmic Fountain"] = [5,5];
offsets[?"Cloudy Mixture"] = [7,7];