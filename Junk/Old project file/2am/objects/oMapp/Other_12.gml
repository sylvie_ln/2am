/// @description Pitfall
var time = get_timer();
var big = ds_list_create();
for(var i=0; i<w; i+=2) {
	var list = ds_list_create();
	for(var j=0; j<h; j+=5) {
		switch(step) {
			case 7: // Captive Automaton (Poisonshroom)
				var node = mapp_mxn(2,5,-10,0,stepseed+j*w+i);
			break;
			case 8: // Careless Desire (Sparkleshroom)
				var node = mapp_mxn(2,5,-8,4,stepseed+j*w+i);
			break;
		}
		node[ninfo.xp] = i;
		node[ninfo.yp] = j;
		ds_list_add(list,node);
	}
	var node = mapp_link(list,stepseed+j*w+i);
	node[ninfo.xp] = i;
	ds_list_add(big,node);
}
mapp = mapp_link(big,stepseed+j*w+i);
fill_setup();
switch(step) {
	case 7: // Captive Automaton (Poisonshroom)
		door_freq = 7;
		unco_freq = 3;
		rare_freq = 13;
	break;
	case 8: // Careless Desire (Sparkleshroom)
		door_freq = 9;
		unco_freq = 4;
		rare_freq = 11;
	break;
}
while(fill == 1) {
	fill_loop(door_freq,unco_freq,rare_freq);
}
fill_cleanup();
done = true;