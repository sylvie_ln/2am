if paused { exit; }
if mask_index == sSnakeRope {
	if !(placed or generated) {
		mask_index = sSnakeHitbox;	
		exit;
	}
}
//var phv = hv;
image_yscale = 1+(floor(hv)/2);
if placed or generated { 
	mask_index = sSnakeRope;
	exit; 
}

var hv_floor = floor(hv);
if hv_floor > hv_floor_prev {
	if unpaused_ever {
		play_sound(sndSnakeGrow);
	}
	hv_floor_prev = hv_floor;
}

unpaused_ever = true;

if room != rmMushZone and hv >= 1 { exit; }
if hv >= 8 { exit; }
if !stopme {
	hv+=0.25;
}
image_yscale = 1+(floor(hv)/2);
var blocked = collision_at(x,y-8);
while collision_at(x,y-8) {
	hv -= 0.25;
	image_yscale = 1+(floor(hv)/2);
	if hv <= 1 {
		hv = 1;
		image_yscale = 1+(floor(hv)/2);
		repeat(8) {
			y++;
			if !collision_at(x,y) { break; }
		}
		break;
	}
}
if blocked { stopme = true; exit; }