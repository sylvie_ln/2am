event_inherited();

upblocker = false;
pushable = false;
carriable = false;
pusher = false;
carrier = false;


if room == rmMushZone {
	act1 = "Climb"
	script="I'm just a rope made out of a snake, why don't you climb me usefully?"
} else {
	act1 = "Talk"
	script = "Hisss, I simply can't thrive in this environment. Can't you use me in a Mushroom Zone?"
}

stopme = false;
damp = 0.75;
bounce = damp;
fric = 0.95;

image_index = 0; 
image_speed = 0;
hv = 0;
hv_floor_prev = 0;
unpaused_ever = false;


/// In oInit, define a trade entry for this item!