{
    "id": "8654b836-dac2-47ca-8902-a24b6b957179",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMagicCarrot",
    "eventList": [
        {
            "id": "9d183e0c-6d81-4ea8-aba4-19e3d4b95eba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8654b836-dac2-47ca-8902-a24b6b957179"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "79b0b3f4-38c5-42f7-886a-dcc968a6a1a6",
    "visible": true
}