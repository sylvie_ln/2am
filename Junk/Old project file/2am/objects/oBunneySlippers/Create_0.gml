event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "Fluffy soft bunney slippers! They are so cute and make you feel fluffy when you walk."

damp = 0.75;
bounce = damp;
fric = 0.95;

grav /= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!