{
    "id": "91ac8e94-bb99-4d56-aadc-566d419297a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBunneySlippers",
    "eventList": [
        {
            "id": "80e79f42-8c5c-4e39-879b-a46c224c32d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "91ac8e94-bb99-4d56-aadc-566d419297a3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8afb35c-062f-4fb6-97dd-df3c4dfabd3d",
    "visible": true
}