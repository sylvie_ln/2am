{
    "id": "62516309-c94f-4c04-a0da-74fed8281311",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSnowKeety",
    "eventList": [
        {
            "id": "3a34aaf4-7d71-4289-8dad-0364d9d63dfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62516309-c94f-4c04-a0da-74fed8281311"
        },
        {
            "id": "a3d29e8d-888f-4f00-ab9b-b5cf3fa99b3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "62516309-c94f-4c04-a0da-74fed8281311"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99de34d4-4dd7-41a4-b67d-e6c88b0744d3",
    "visible": true
}