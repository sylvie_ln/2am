active = false;
parent = noone;

//yes_no = [ false, true ];
power_scale = [ 0, 0.5, 1, 1.5, 2 ]
option_choice_names = [ "None", "Low", "Default", "High", "Extreme" ];

air_drain = ["Air Drain", "assist_air_drain", power_scale];
jump_power = ["Jump Power", "assist_jump_power", power_scale];
speed_power = ["Speed Power", "assist_speed_power", power_scale];
gravity_power = ["My Gravity", "assist_gravity", power_scale];
fps_power = ["Game Speed", "assist_gamespeed", power_scale];
//pop_mushrooms = ["Explodin' Shrooms", "assist_pop_mushrooms", yes_no];
done = ["Done", noone, noone];

options = [ speed_power, jump_power, air_drain, gravity_power, fps_power, done ];
options_length = array_length_1d(options);
options_index = 0;

option_choice_indices = [];
for (var i = 0; i < options_length; i++) {
	var option = options[i];
	option_choice_indices[i] = 0;
	if option[1] == noone {
		continue;
	}
	var choice_value = variable_global_get(option[1]);
	
	var choices = option[2];
	var choices_length = array_length_1d(choices);
	
	for (var j = 0; j < choices_length; j++) {
		if choices[j] == choice_value {
			option_choice_indices[i] = j;
			break;
		}
	}
}