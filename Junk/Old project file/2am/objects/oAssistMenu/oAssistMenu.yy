{
    "id": "0e11c6e4-ef55-40d3-a81c-48fd5f00fe44",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAssistMenu",
    "eventList": [
        {
            "id": "11440446-64be-49c1-a365-8c99ad985094",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e11c6e4-ef55-40d3-a81c-48fd5f00fe44"
        },
        {
            "id": "24c0044e-95bd-4919-a4e5-388a609c5e65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0e11c6e4-ef55-40d3-a81c-48fd5f00fe44"
        },
        {
            "id": "5e1a183b-4b18-461a-808b-1fcd595d4ddf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "0e11c6e4-ef55-40d3-a81c-48fd5f00fe44"
        },
        {
            "id": "64fa708c-97ee-4e8e-971c-1baf33c5ac59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e11c6e4-ef55-40d3-a81c-48fd5f00fe44"
        },
        {
            "id": "f70ea6ac-dca6-4f89-9b05-cb192b8ba6bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0e11c6e4-ef55-40d3-a81c-48fd5f00fe44"
        },
        {
            "id": "54406935-56d6-4608-9607-c8b062a0e9cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0e11c6e4-ef55-40d3-a81c-48fd5f00fe44"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}