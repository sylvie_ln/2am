if !active { exit; }
draw_set_font(global.neco_font);
var hpos = x;
var vpos = y;
var mlen = 0;
for(var i=0; i<options_length; i++) {
	var option = options[i];
	var txt = option[0];
	draw_text(hpos,vpos,txt);	
	vpos += string_height(txt);
	mlen = max(mlen,string_width(txt));
}
hpos += mlen+10;
vpos = y;
for(var i=0; i<options_length; i++) {
	var option = options[i];
	var ww = 36;
	if i == options_index {
		draw_text(x-7,vpos,"*");
	}
	if option[1] == noone
		continue;
	
	var txt = option_choice_names[option_choice_indices[i]];
	
	if i == options_index {
		draw_text(hpos-6,vpos,"<");
		draw_text(hpos+ww,vpos,">");
	}
	draw_text_centered(hpos+(ww div 2),vpos,txt);	
	vpos += string_height(option[0]);
}