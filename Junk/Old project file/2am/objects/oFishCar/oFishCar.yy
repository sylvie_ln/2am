{
    "id": "f5a82472-bb1d-40bf-a293-1e7d04d77594",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFishCar",
    "eventList": [
        {
            "id": "e6f00ce8-ac4c-48d8-ae19-a31e5bd57647",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f5a82472-bb1d-40bf-a293-1e7d04d77594"
        },
        {
            "id": "bd3ff280-a07a-4b0c-ba6e-fc0db49984c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "f5a82472-bb1d-40bf-a293-1e7d04d77594"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59092ac7-8b1b-4e26-b256-146e1a70bafa",
    "visible": true
}