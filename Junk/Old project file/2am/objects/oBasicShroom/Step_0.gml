if paused { exit; }

var gdir = (grav < 0 ? -1 : 1);
if thrown_timer > 0 {
	thrown_timer--;	
}
var ong = collision_at(x,y+gdir);
if !ong {
	if thrown_timer <= 0 or throwdown {
		vv += grav;
	}
}

if object_index == oDice and !place_meeting(x,y,oDice) {
	with oDice { blocker = true; }	
}


if !move(hv,0)  and object_index != oSylvieTruck  {
	hv = -hv;	
	if object_is_ancestor(object_index,oVehicle) and bounce != 0 {
		if hv == 0 {
			image_xscale = -image_xscale;
		} else {
			image_xscale = sign(hv);
		}
	}
}

bounced = false;
if !move(vv,1) {
	if vv*gdir > 0 {
		if !throwdown {
			if solid_at(x,y+gdir) or blocker_at(x,y+gdir)
			or (object_index == oJetpack and place_meeting(x,y+gdir,oChar)) {
				vv = -vv*damp;	
				if hdamp {
					hv = hv*damp;
				}
				dampboost += dampdamp;
				bounced = true;
			} else {
				if throwspin {
					throwspin -= 1/5;
					if !throwspin {
						throwspin = 0;
						draw_angle = 0;
						spin_dir = 0;
					}
				}
				vv = -vv*bounce;
				if abs(vv) < sqrt(2*abs(grav)*(54*bounce)) {
					vv = gdir*(-sqrt(2*abs(grav)*(54*bounce)));	
				}
			}
		} else {
			throwdown = false;
			thrown_timer = 0;
			vv = -vv;	
		}
		if abs(vv) < damp+dampboost {
			vv = 0;
			dampboost = 0;
		}
	} else {
		vv = -vv;	
	}
}

if object_index == oDice {
	with oDice { blocker = false; }	
}

if (vv == 0 and ong) or (object_is_ancestor(object_index,oVehicle) and riding and do_friction) {
	hv = hv*fric;
	if abs(hv) < 0.5 {
		hv = 0;	
	}
}

if throwspin {
	draw_angle += 30*max(1,point_distance(0,0,hv,vv))*spin_dir;
	if hv == 0 and vv == 0 {
		throwspin = false;
		draw_angle = 0;
		spin_dir = 0;
	}
}