if ring == true {
	if !rung {
		image_speed = 1;
		alarm[0] = bigtime - time;
		rung = true;
	} else {
		if slow {
			image_speed -= 1/time;
			if image_speed <= 0 {
				image_speed = 0;
				image_index = 0;
				ring = false;
			}
		}
	}
}

if (prev != image_index && (image_index == 1 || image_index == 3))
	play_sound(sndTowerPeakBellToll);

prev = image_index;