{
    "id": "8f5d1a03-fd90-4ff4-94c5-e3b36f67f3d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTheBell",
    "eventList": [
        {
            "id": "7a126c24-35e0-4755-b700-cef8e228ae33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f5d1a03-fd90-4ff4-94c5-e3b36f67f3d0"
        },
        {
            "id": "4a4a73fd-1999-4b81-8e0f-8d86c72cc2b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f5d1a03-fd90-4ff4-94c5-e3b36f67f3d0"
        },
        {
            "id": "b86f345a-ddc4-4da8-9cfb-3371cff17bbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8f5d1a03-fd90-4ff4-94c5-e3b36f67f3d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a3e84858-34ae-42bf-8aef-dee5acc2fb72",
    "visible": true
}