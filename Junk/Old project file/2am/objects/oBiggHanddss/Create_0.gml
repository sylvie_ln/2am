depth = 101;
var i=0;
var count=12;
while(count>0) {
	handspd[i] = 12-count;
	handpos[i] = 0;
	if i == hand.darkness
		handshow[i] = global.flag_door_darkness;
	else if i == hand.dreams
		handshow[i] = global.flag_door_dreams;
	else if i == hand.earth
		handshow[i] = global.flag_door_earth;
	else if i == hand.flames
		handshow[i] = global.flag_door_flames;
	else if i == hand.leafs
		handshow[i] = global.flag_door_leafs;
	else if i == hand.lies
		handshow[i] = global.flag_door_lies;
	else if i == hand.light
		handshow[i] = global.flag_door_light;
	else if i == hand.moon
		handshow[i] = global.flag_door_moon;
	else if i == hand.sky
		handshow[i] = global.flag_door_sky;
	else if i == hand.storms
		handshow[i] = global.flag_door_storms;
	else if i == hand.tears
		handshow[i] = global.flag_door_tears;
	else if i == hand.thunder
		handshow[i] = global.flag_door_thunder;
	i = nmod(i-1,12);
	count--;
}
handpos[12] = 0;
handspd[12] = 4;
rot = false;