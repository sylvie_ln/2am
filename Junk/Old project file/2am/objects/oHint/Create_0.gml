event_inherited();

upblocker = false;
pushable = false;
carriable = false;
pusher = false;
carrier = false;

damp = 0;
bounce = 0;
fric = 0.95;

enum hints {
	BiggerBag,
	CheeseyTreat,
	RainbowShroom,
	ClimbingTower,
	TowerPrisoner,
	OverpricedKitteys,
	FloatShroom,
	LegendaryTree,
	StormyDemeanor,
	ShadyIndividual,
	CrossingCanyon,
	MinecartLicense,
	SpringShroom,
	SomethingSparkly,
	CanyonSky,
	PoisonShroom,
	WinningRace,
	SecretTomb,
	RareShop,
	GamblersHeart,
	last
}

hint_script[hints.BiggerBag]
=@"You might find that your Bag can't hold a lot of items. In particular, your basic Bag can't carry more than one WideShroom or TallShroom at a time, which is very inconvenient!
There is a Bagsmith in the lower left area of the Citey who can help you. Bring her WideShrooms and TallShrooms and she can use them to upgrade your Bag.
WideShrooms are native to the Mushroom Zones of the Cavern of Ancients, and TallShrooms are native to the Mushroom Zones of the Cherry Blossom Hills. You won't find them many other places.
You can also purchase WideShrooms and TallShrooms from UnCoMus, the Uncommon Mushroom Shop in the Citey Center."
hint_script[hints.RainbowShroom]
=@"Someone in the upper left area of the Citey wants a rare mushroom known as the RainbowShroom.
This is quite a hard item to get. It's natively found in a Mushroom Zone high in the sky of the Canyon Cliffs. This is a hard area to reach, and you'll also need a key to enter the zone.
There's another way to get a RainbowShroom, but it's a secret!"
hint_script[hints.CheeseyTreat]
=@"In the upper right area of the Citey, someone wants a cheesey treat.
This treat can be found at a restaurant in the lowest area of the Cavern of Ancients.
You'll have to gather ingredients for the owner though, which might be a bit of a hassle."
hint_script[hints.ClimbingTower]
=@"The Cylvey Clock Tower can be climbed without any items that aid mobility, but it's rather difficult.
You might want to wait until you have something that lets you jump higher or bounce higher to tackle it. Note that you can't throw items inside the tower, so that won't help!
You'll need certain Hands of the Clock to make progress through the tower. I won't tell you which ones exactly. Explore and figure it out!
There's also a Mushroom Zone inside, which again needs certain Hands to reach, as well as a key to open. 
Finally, I'll note that two of the 12 Hands of the Clock are hidden in the tower itself. If you've found ten Hands in the outer world, you're ready to climb the tower.
You can reach the top without all 12 Hands, but will you really be able to restore the flow of time if some of them are missing? A Clock needs 12 Hands to work."
hint_script[hints.TowerPrisoner]
=@"To reach the very top of the tower, you'll need to solve the riddle posed by the prisoner in the upper right to get the Hand of Lies.
Don't pay attention to meaning of the story she tells you. It's really just a word game. Or maybe you could call it a letter game.
Hidden in the text somewhere is what you need to trade for the Hand. Look carefully."
hint_script[hints.CrossingCanyon]
=@"There's a wide gap in the Canyon that you can't simply jump across. So how do you get past it?
One way is to use an item to give yourself a boost. Throw an item and ride on it, and see if you can get across by jumping off the item at the right time.
Alternatively, there's this trick. If you throw an item downwards in midair, and hold the jump button as you close your bag, you'll do a midair jump off the item!
If you want a more permanent method, you'll need to buy a Bridget's Ball from the Canyon's local treasure hunter. You can use this to create a bridge across the gap.
However, Bridget's Ball costs two WideShrooms to trade for. If you haven't found a way to upgrade the size of your Bag, you won't be able to make the trade."
hint_script[hints.SpringShroom]
=@"You might have met a few people who want a SpringShroom. But where do you find such a rare item?
Well, have you met the Junk Shop owner? They frequently need springs as parts for repairs. So, they set up shop near an area where SpringShrooms can be found.
However, you'll have to find a secret passage to get there. And you'll need a key to enter the zone as well."
hint_script[hints.MinecartLicense]
=@"There's a fun Minecart Adventure you can go on in the Canyon. However, you need a license to drive a minecart.
In the Citey, right near the Canyon entrance, there's someone who has a license you can trade for.
If you aren't sure what to trade them, try to figure out what kind of items they like."
hint_script[hints.SomethingSparkly]
=@"There are two people in the Canyon Cliffs who are big fans of Rocks. One of them has a very Unique Rock that seems valuable.
They say that they'll trade the Unique Rock for a shroom that sparkles. But what's such a shroom??
You'll need to find the SparkleShroom, which is native to one of the locked Mushroom Zones in the Cherry Blossom Hills. It's to the left of the entrance to the Hills."
hint_script[hints.CanyonSky]
=@"Even if you can cross the Canyon's big gap, there are a lot of interesting things in the sky above.
It'll be very tedious to climb into the sky just by bouncing on items. You'll want to get a Ladderia's Ball from the Treasure Hunter, which will give you a way to climb upwards.
Even after you climb up though, there's another large gap to cross leading to a cliff's edge. You can cross this gap by riding on an item, but....
It's also possible to get up there without items by simply doing a well-timed jump off the ladder. It takes some experimenting and practice to find the right timing though."
hint_script[hints.PoisonShroom]
=@"The owner of Taco Sacc wants PoisonShrooms to make a lovely dish. Where can you find this rare shroom?
Naturally, the restaurant was built near a Mushroom Zone where PoisonShrooms grow natively. It's just over to the right. But you'll need a key to enter it."
hint_script[hints.WinningRace]
=@"The RaceRunner has never been defeated in a race. Maybe you will be the first to win. But how?
Well, there are actually lots of methods that will work. The idea is to use items to go faster.
In the Canyon Cliffs, near the entrance, there's a shoe shop. One of their items, the Pegasus Shoes, lets you dash quickly, though it's hard to control. 
You'll need SpeedShrooms to trade for the shoes. These are found in the Mushroom Zone in the upper right area of the Cavern. It's a little tricky to get up there though, and it requires a key to enter.
If you're clever and set it up properly, you might be able to even find a way to win the race using a SpeedShroom alone, saving yourself a trip to the Canyon.
One other option is to use a Vehicle to win the race. But it can be hard to find such Vehicles, and some of them aren't even fast enough to win."
hint_script[hints.SecretTomb]
=@"In the upper right area of the Cavern of Ancients, it's rumored there is a Secret Tomb. What could be inside?
Just getting to the upper right is a bit tricky. If you have a Ladderia's Ball, you can create a ladder up there. You can get these from the Treasure Hunter in the Canyon Cliffs.
If you don't have a ladder, there's a secret passage you can take.
Once you're there, you'll find a riddle inscribed on a statue that you must solve. The riddle refers to the four Kitteys in the bottom right of the Cavern.
If you're having trouble solving the riddle, read onwards. But I encourage you to try to solve it yourself first. \ask{Read onwards}{Stop reading}
!if answer==1
 The upper Kittey statue represents the moon, hence the crescent moon button on it.
 The four Kittey statues at the bottom describe the movements of a kitten throughout the day. The starting point is the early morning.
 First, make note of which statue refers to which time of the day. Then, flip the switches on the morning, afternoon, and evening statues so that each switch points to the statue with the next part of the day.
 The statue that represents the late night talks about the ocean under the moon. The switch here should point upwards - directly at the upper statue that represents the moon.
 When the switches are arranged correctly, pressing the crescent moon button will open the tomb entrance! Isn't that a great puzzle?!
!fi"
hint_script[hints.RareShop]
=@"There's apparently a shop that sells rare mushrooms in the world. It turns out it's actually in this Cavern of Ancients.
Look for a person who seems to be impossible to reach. Then search the room underneath for a secret passage leading upwards.
The rare mushrooms are expensive though, and a different mushroom is sold each day of the week, so if the shop doesn't have the right mushroom, you might have to wait a while."
hint_script[hints.GamblersHeart]
=@"A gambler who threw away everything except the dice they bet their life on.
If you throw all three dice at once, and they each land on a 6, something amazing might happen.
But like the gambler says, there's only a 1 in 216 chance of this happening. Is it worth the trouble?
If you can find an item somewhere that increases your luck, maybe it will be easier. Such an item exists in the Cherry Blossom Hills."
hint_script[hints.StormyDemeanor]
=@"In the upper left sky of the Cherry Blossom Hills, there's an angry little thundercloud. Unfortunately, this cloud happens to have one of the legendary Hands of the Clock. How can you convince her to trade it?
Well, she's very vain, and happens to love items that look like her. Something like a statue of herself would be perfect.
You probably can't find such a statue, but in the Canyon Cliffs, you can find an item with a similar shape and colour."
hint_script[hints.LegendaryTree]
=@"The Legendary Tree is a golden tree that grows at the peak of the Cherry Blossom Hills. It's said that if you confess your love to someone under this tree, you'll both be blessed with a wonderful life.
It is also said that there is a mystical connection between this tree and the SparkleShroom, a rare golden mushroom native to the Hills.
SparkleShrooms can be found in the Mushroom Zone to the left of the entrance to the Hills. It's a little awkward to get up there, but you can hop on an item, or use a Bridget's Ball to create a bridge. You'll also need a key to enter.
Once you have the SparkleShroom, stand under the Legendary Tree, and maybe a heart-pounding event will happen."
hint_script[hints.FloatShroom]
=@"The Laboratorey scientist requires a FloatShroom for some of their inventions. You might have seen other people who want this rare floating mushroom.
This is found in a rather strange location. The Cylvey Clock Tower actually contains a Mushroom Zone within it, and the FloatShroom is found there.
You'll probably need one of these three Hands of the Clock to reach it: the Hand of Storms, the Hand of Sky, or the Hand of Thunder. You might be able to find a way to get to it without a Hand though. You'll also need a key to enter the zone.
Could there perhaps be another way to get these floating shrooms? I wonder."
hint_script[hints.ShadyIndividual]
=@"There's a strange person in the bottom left area of the Cherry Blossom Hills. They won't talk to you unless you have a password.
If you simply keep talking to them, you might be able activate the Forgot your Password feature and see the security questions.
The password is a little strange though. Instead of an actual word, the password consists of three specific items, which you must have in your bag.
If you can't figure it out, don't worry about it. This person doesn't have anything that's needed to complete your quest to restore the flow of time."
hint_script[hints.OverpricedKitteys]
=@"There's someone in the bottom right areas of the Cherry Blossom Hills who sells KitteyShrooms. However, they're absurdly expensive, so I wouldn't recommend buying them.
She claims her shop is the only place in the world where you can find KitteyShrooms, but that can't be true. Where does she source her KitteyShrooms from then?
It turns out that there's actually a secret passage right by her shop. It leads to a Mushroom Zone where KitteyShrooms live, so you can find some for yourself. You'll need a key to enter the zone though."


image_speed = 0;