sylvie_list_shuffle(global.memory_particles[0])
sylvie_list_shuffle(global.memory_particles[1])
sylvie_list_shuffle(global.memory_particles[2])
alarm[1] = 16;
if bag_has_item(oMemoryDisk) {
	var mem_ii = bag_item_ii(oMemoryDisk);
	var mem_map = global.memory_disks[|mem_ii];
	var mem_pair = mem_map[?oRoomGen.area];
	mem_pair[|0] = oRoomGen.xp;
	mem_pair[|1] = oRoomGen.yp;
}