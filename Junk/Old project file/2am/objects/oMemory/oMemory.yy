{
    "id": "7feae8ef-2709-4606-be1f-d218e8c60d12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMemory",
    "eventList": [
        {
            "id": "47e688d7-4276-4c8c-be58-cacdd6e47539",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        },
        {
            "id": "7122e83f-592b-446e-9f2e-6237465fa706",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        },
        {
            "id": "02a72101-10b8-4e86-8e2a-bc8cc5e0435a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        },
        {
            "id": "389c0002-8751-424e-b389-847853ebead8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        },
        {
            "id": "ffde60fc-f7af-48ba-8290-7d1240f937c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        },
        {
            "id": "fb998010-9249-4d8f-8ee2-f3208755626c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        },
        {
            "id": "70a18276-6dcc-416f-8e03-692d90831c9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        },
        {
            "id": "2362819c-c326-4ee4-ad2d-6321b12e907d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        },
        {
            "id": "1f1c53cc-44a0-4504-9312-84423d665d0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "7feae8ef-2709-4606-be1f-d218e8c60d12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87dbcdf8-c550-4a42-b5c2-1382ef07f51b",
    "visible": true
}