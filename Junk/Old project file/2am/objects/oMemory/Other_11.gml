if !instance_exists(oRoomGen) { exit; }
if cutscene { exit; }
if talking or script == gaze {
	event_inherited();	
} else if ds_map_exists(global.memory_dead,memory_id) {
	//show_debug_message(memory_id);
	script = base_script+@"
	"+global.memorys[? memory_id]
	if bag_has_item(oMemoryDisk) {
		var mem_ii = bag_item_ii(oMemoryDisk);
		var mem_map = global.memory_disks[|mem_ii];
		var mem_pair = mem_map[?oRoomGen.area];
		if mem_pair[|0] == oRoomGen.startx and mem_pair[|1] == oRoomGen.starty {
			script += @"
				Will you record this spot on your Memory Disk?\nYou can only record one spot per zone, and it cannot be overwritten or reset. \ask{Yes}{No}
				!if answer==1
				 !user 3
				!fi"
		}
	}
	old_script = script;
	event_user(0);
	event_inherited();
} else {
	play_sound(sndMemory);
	talking = true;
	cutscene = true;
	global.memory_dead[? memory_id] = true;
	dead = true;
	instance_create_depth(0,0,0,oFlash);
	sylvie_list_shuffle(global.memory_particles[0])
	sylvie_list_shuffle(global.memory_particles[1])
	sylvie_list_shuffle(global.memory_particles[2])
	alarm[0] = global.default_room_speed*2;
}
