{
    "id": "2bcd5d68-f156-4f2a-92ef-19cacd0d11a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBridgeOrb",
    "eventList": [
        {
            "id": "5b162a79-9f93-41e1-9049-62fcfc2a1366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2bcd5d68-f156-4f2a-92ef-19cacd0d11a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1050906e-27a8-46ad-aefd-8c6a3316b411",
    "visible": true
}