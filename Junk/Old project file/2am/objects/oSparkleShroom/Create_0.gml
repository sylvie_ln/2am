event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A sparkling shroom made of golds, it sparkles and shines so bright."

damp = 0.5;
bounce = damp;
fric = 0.95;


image_speed = 0;