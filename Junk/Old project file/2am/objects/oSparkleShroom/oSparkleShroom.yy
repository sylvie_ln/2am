{
    "id": "00393d76-ea2f-476c-b2e0-ad011ac25169",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSparkleShroom",
    "eventList": [
        {
            "id": "30c0d40b-1bab-4c5c-9366-7cf1f4bf2bae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "00393d76-ea2f-476c-b2e0-ad011ac25169"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de6ced25-9c1f-462f-87d8-1dd53d5c9773",
    "visible": true
}