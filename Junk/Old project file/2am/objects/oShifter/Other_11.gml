//disabled_show_debug_message("END "+room_get_name(room));
if room == rmCoolMuseum { exit; }
var rm = get_that_room();
if room == rmMushZone {
	rm += "_"+oRoomGen.area;
}
if ds_map_exists(global.shroom_insertion_map,rm) {
	var list = global.shroom_insertion_map[?rm];
	ds_list_clear(list);
	with oShroom {
		if object_index == oSpike {continue; }
		if !generated and (instance_exists(oRoomGen) or !placed) {
			//show_debug_message("Saving "+object_get_name(object_index));
			if instance_exists(oRoomGen) {
				var xo = other.xoff;
				var yo = other.yoff;
				if !is_undefined(global.warp_x) {
					//xo = global.warp_x*global.rwidth;
					//yo = global.warp_y*global.rheight;
				}
				//show_debug_message(string(nmod(x+other.xoff,global.rwidth*oRoomGen.wwidth))+","+string(nmod(y+other.yoff,global.rheight*oRoomGen.wheight)));
				var info = list_create(nmod(x+xo,global.rwidth*oRoomGen.wwidth),nmod(y+yo,global.rheight*oRoomGen.wheight),hv,vv,object_get_name(object_index),image_index,image_xscale);
			} else {
				var info = list_create(x+other.xoff,y+other.yoff,hv,vv,object_get_name(object_index),image_index,image_xscale);
			}
			ds_list_add(list,info);
			ds_list_mark_as_list(list,ds_list_size(list)-1);
		} else {
			//show_debug_message("Not saving "+object_get_name(object_index));
		}
	}
}