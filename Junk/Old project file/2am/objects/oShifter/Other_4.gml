if room != rmMushZone {
	if ds_exists(global.mushzone_params,ds_type_map) {
		ds_map_destroy(global.mushzone_params);
	}
	global.mushzone_params = -1;	
} else {
	if is_undefined(global.warp_x) {
		global.warp_x = oRoomGen.startx;
		global.warp_y = oRoomGen.starty;	
	}
	if !is_undefined(global.warp_x) {
		xoff = global.warp_x*global.rwidth;
		yoff = global.warp_y*global.rheight;
		xoff = nmod(xoff,global.rwidth*oRoomGen.wwidth);
		yoff = nmod(yoff,global.rheight*oRoomGen.wheight);
	}
}


if layer_exists("Scenery") {
	ds_list_add(layers,layer_get_id("Scenery"));
}
if layer_exists("Background") {
	ds_list_add(layers,layer_get_id("Background"));
}
if layer_exists("Overlay") {
	ds_list_add(layers,layer_get_id("Overlay"));
}
for(var i=0; i<16; i++) {
	if layer_exists("Scenery_"+string(i)) {
		ds_list_add(layers,layer_get_id("Scenery_"+string(i)));
	}	
	if layer_exists("Background_"+string(i)) {
		ds_list_add(layers,layer_get_id("Background_"+string(i)));
	}	
}

var rm = get_that_room();
if room = rmMushZone {
	rm += "_"+oRoomGen.area;
}
//disabled_show_debug_message("START "+room_get_name(room));
if room != rmCoolMuseum { 
	if ds_map_exists(global.shroom_insertion_map,rm) {
		var list = global.shroom_insertion_map[?rm];
	} else {
		var list = ds_list_sylvie_create();
		ds_map_add_list(global.shroom_insertion_map,rm,list);
	}
	enum shroom_insertion_info {
		xp,
		yp,
		hv,
		vv,
		type,
		ii,
		xs,
		last
	}
	for(var i=0; i<ds_list_size(list); i++) {
		var sh_info = list[|i];
		var sh_x = sh_info[|shroom_insertion_info.xp]-xoff;
		var sh_y = sh_info[|shroom_insertion_info.yp]-yoff;
		var sh_hv = sh_info[|shroom_insertion_info.hv];
		var sh_vv = sh_info[|shroom_insertion_info.vv];
		var sh_type = asset_get_index(sh_info[|shroom_insertion_info.type]);
		var sh_ii = sh_info[|shroom_insertion_info.ii];
		var sh_xs = sh_info[|shroom_insertion_info.xs];
		global.do_deletion = false;
		var shroom = instance_create_depth(sh_x,sh_y,0,sh_type);
		global.do_deletion = true;
		if instance_exists(shroom) {
			shroom.placed = false;
			shroom.hv = sh_hv;
			shroom.vv = sh_vv;
			shroom.image_index = sh_ii;
			shroom.image_xscale = sh_xs;
			with shroom {
				event_user(0);	
			}
		}
	}
	ds_list_clear(list);

}

global.room_started = true;
global.do_deletion = true;