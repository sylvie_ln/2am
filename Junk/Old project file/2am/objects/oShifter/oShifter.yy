{
    "id": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShifter",
    "eventList": [
        {
            "id": "13d13742-5757-4f97-bda6-883cde6fabaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742"
        },
        {
            "id": "804fd29f-04a3-400f-b286-2930d86d00bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742"
        },
        {
            "id": "3fd9c1e5-5a59-453d-9950-2eaacdce9b92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742"
        },
        {
            "id": "074bc648-fbd0-4db6-9899-f9d828c7186c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742"
        },
        {
            "id": "b04ed4d4-4aba-43ca-8e0d-4aff5b1aafae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742"
        },
        {
            "id": "79d5ca38-046a-4d47-8608-b55881639d60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742"
        },
        {
            "id": "ec6fbba6-8658-4670-8077-e45d07b5f1bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742"
        },
        {
            "id": "ee9d80b2-784a-49eb-b53c-53facddde929",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "dd2fadc7-70c1-4c3e-a5c1-89aa62f6f742"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}