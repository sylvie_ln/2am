with oChar {
	if alarm[3] >= 0 {
		draw_set_alpha((ending_timer-alarm[3])/ending_timer);
		draw_set_color(c_black);
		draw_rectangle(
		camera_get_view_x(view_camera),
		camera_get_view_y(view_camera),
		camera_get_view_x(view_camera)+global.view_width,
		camera_get_view_y(view_camera)+global.view_height,
		false);
		draw_set_alpha(1);
	}
}