{
    "id": "6bc854b5-796d-46b7-a865-392d63fa9ef5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCalledHand",
    "eventList": [
        {
            "id": "eec88c5f-b557-4a8d-9aa4-574caf8b33bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6bc854b5-796d-46b7-a865-392d63fa9ef5"
        },
        {
            "id": "03ccf012-4565-4c98-9897-30c8e0a1266f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6bc854b5-796d-46b7-a865-392d63fa9ef5"
        }
    ],
    "maskSpriteId": "298b4dca-b5ef-4b5a-a398-7085be3bdc8d",
    "overriddenProperties": null,
    "parentObjectId": "c8ff8d82-948f-4875-b52e-6b0807ea0545",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7b769393-b075-4f2f-ac61-307853b999e1",
    "visible": true
}