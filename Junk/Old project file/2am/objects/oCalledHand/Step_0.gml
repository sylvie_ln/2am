x += hv;
y += vv;

var dist = point_distance(x,y,target.x,target.y);
var dir = point_direction(x,y,target.x,target.y);
if !place_meeting(x,y,target) {
	//acc += grav;	
	//if acc > maxspd { acc = maxspd; }
} else {
	with target.object_index {
		if image_index == other.image_index {
			instance_change(oDoor,false);
			image_speed = 1;
		}
	}
	play_sound(sndUseCalledWand);
	instance_create_depth(0,0,0,oFlash);
	bag_remove_item(delete);
	instance_destroy();
	exit;
	acc = 0;
}
	
	hv += lengthdir_x(acc,dir);
	vv += lengthdir_y(acc,dir);
	var spd = point_distance(0,0,hv,vv);
	if spd > maxspd {
		hv = (hv/spd)*maxspd;
		vv = (vv/spd)*maxspd;
	}