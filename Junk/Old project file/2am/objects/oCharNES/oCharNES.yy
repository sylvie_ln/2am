{
    "id": "fade8298-cc08-4a98-90a7-b8d2bf021804",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCharNES",
    "eventList": [
        {
            "id": "45ef2432-377d-4751-965f-3d85f6fe0157",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fade8298-cc08-4a98-90a7-b8d2bf021804"
        },
        {
            "id": "b3c78e06-0e4a-4b41-b900-e8e77c79f007",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fade8298-cc08-4a98-90a7-b8d2bf021804"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c2eebbd-0f50-45d8-9e0f-c3be8d482903",
    "visible": true
}