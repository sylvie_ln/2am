if !talking and script != old_script {
	script = old_script;
	event_user(0);	
}

locked = !is_undefined(global.lock_flag[? tag]);
if locked_prev != noone and locked_prev != locked and !locked {
	play_sound(sndDoorUnlock);
}
locked_prev = locked;