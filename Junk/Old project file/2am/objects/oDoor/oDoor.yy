{
    "id": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoor",
    "eventList": [
        {
            "id": "893268ea-8085-4b60-b2ce-62ce9f65b82e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159"
        },
        {
            "id": "6bbbff76-cb10-484d-8a61-39b03e880163",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159"
        },
        {
            "id": "2f16d885-8ce8-4c5a-b4a6-d69c15f36b88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159"
        },
        {
            "id": "aa6a7a84-a783-4dbc-9093-8b8c478a9728",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159"
        },
        {
            "id": "2c162db2-b690-48c8-b8a6-db581f1b1a41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159"
        },
        {
            "id": "e653a2be-426d-4f2c-bb56-c076cb37251d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "5e27b158-c3fb-4f8d-8ed4-41d1c582b159"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a7a8d89-db95-44e9-8183-a5e97af6ebbd",
    "visible": true
}