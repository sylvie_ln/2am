///@description Enter
if	!is_undefined(global.lock_flag[? tag]) {
	old_script = script;
	if bag_has_item(asset_get_index(global.lock_item[? tag])) {
		script = @"Use a "+global.lock_name[? tag]+@" to unlock it?\ask{Yes}{No}
		!if answer==1
		 !unlock
		!fi";
	} else {
		script = "Seems locked, you will need a "+global.lock_name[? tag]+" to open it."
	}
	event_user(0);
	event_user(1);
} else if bag_item_count(oMemoryDisk) > 1 and rm == rmMushZone {
	old_script = script;
	script = "ERROR! To prevent fatal memory corruption, you cannot enter a Mushroom Zone while carrying multiple Memory Disks."
	event_user(0);
	event_user(1);
} else if rm != room { 
	if door_script != -1 {
		script_execute_ext(door_script);	
	}
	if rm == rmMushZone and bag_has_item(oMemoryDisk) and bag_item_ii(oMemoryDisk) == 0  
	and global.mushzone_params[? "name"] != "Careless Desire" {
		old_script = script;
		script = @"ERROR! Memory Disk data load failed. Scanning disk for corruption.
		"
		for(var i=0; i<12; i++) {
			if global.area_names[i] == "Careless Desire" {
				script+="Sector "+string(i)+" ["+global.area_names[i]+"] is OK."
			} else {
				script+="ERROR! Sector "+string(i)+" ["+global.area_names[i]+"] is corrupted."
			}
			script += global.nl;
		}
		script += "Scan complete, have a nice day."
		event_user(0);
		event_user(1);
		ds_map_destroy(global.mushzone_params);
		global.mushzone_params = -1;
	} else {
		global.door_tag = tag;
		global.previous_room = room_get_name(room);
		global.current_room = room_get_name(rm);
		play_sound(sndOpenAndCloseDoor);
		global.entering_door = true;
		room_go(rm);
	}
} else {
	with oDoor {
		if id != other.id and tag == other.tag {
			play_sound(sndOpenAndCloseDoor);
			oChar.x	= x;
			oChar.y = y;
			with oChar {
				bx = (floor(x/global.view_width))*global.view_width;
				by = (floor(y/(global.view_height-20)))*(global.view_height-20);
				camera_set_view_pos(view_camera[0],bx,by);	
			}
		}
	}
}