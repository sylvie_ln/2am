{
    "id": "a6f4614c-88da-49a8-95a9-a1fbfd238ede",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinecraftBlock",
    "eventList": [
        {
            "id": "2319656d-9982-4919-97a1-1dafe6811899",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a6f4614c-88da-49a8-95a9-a1fbfd238ede"
        },
        {
            "id": "ac7f09fb-82a2-4daa-b5b3-cddff1b3b9f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a6f4614c-88da-49a8-95a9-a1fbfd238ede"
        },
        {
            "id": "28d8f09b-4920-4563-aeda-a1fde4db9ea1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "a6f4614c-88da-49a8-95a9-a1fbfd238ede"
        },
        {
            "id": "d898b875-167c-4285-abef-6f760b0f967b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a6f4614c-88da-49a8-95a9-a1fbfd238ede"
        },
        {
            "id": "7fe70681-8140-4500-90af-875cb434c8cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a6f4614c-88da-49a8-95a9-a1fbfd238ede"
        },
        {
            "id": "530423c6-79f2-4010-8ebd-743be635935d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a6f4614c-88da-49a8-95a9-a1fbfd238ede"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "390ab061-16f4-499d-8819-9ccb81763e68",
    "visible": true
}