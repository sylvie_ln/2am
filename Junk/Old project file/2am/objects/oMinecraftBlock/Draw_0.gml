if num == 0 { exit; }
for(var i=0; i<image_xscale; i++) {
	for(var j=0; j<image_yscale; j++) {
		if ii[i,j] != 0 {
			draw_sprite_ext(sprite_index,ii[i,j],x+i*sprite_get_width(sprite_index),y+j*sprite_get_height(sprite_index),1,1,0,c_white,image_alpha);	
		} else {
			draw_sprite_ext(sprite_index,0,x+i*sprite_get_width(sprite_index),y+j*sprite_get_height(sprite_index),1,1,0,c_white,image_alpha);
		}
	}
}