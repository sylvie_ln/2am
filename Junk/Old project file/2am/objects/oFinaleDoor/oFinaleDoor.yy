{
    "id": "72d1855d-5224-4f18-944f-d9d5bf5b2e30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFinaleDoor",
    "eventList": [
        {
            "id": "086354fa-6780-448b-8b0a-22bc35dde88e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "72d1855d-5224-4f18-944f-d9d5bf5b2e30"
        },
        {
            "id": "e22df297-a65f-4039-b68c-678300fb2c21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "72d1855d-5224-4f18-944f-d9d5bf5b2e30"
        },
        {
            "id": "7237b145-d386-479d-a56d-b77f67f83f36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "72d1855d-5224-4f18-944f-d9d5bf5b2e30"
        },
        {
            "id": "38d31d2a-292b-43dd-99ec-0380c8d59c6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "72d1855d-5224-4f18-944f-d9d5bf5b2e30"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a7a8d89-db95-44e9-8183-a5e97af6ebbd",
    "visible": true
}