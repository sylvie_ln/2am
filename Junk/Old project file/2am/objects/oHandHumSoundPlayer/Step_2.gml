if global.mute_sfx { exit; }
if !variable_instance_exists(self, "hum_sound")
{
	hum_sound = noone;
	
	gain = 0;
	freq = 0;
}

var traveled = 0;
var x_diff = 0;
var y_diff = 0;

var hand_objects = get_hand_objects();
var hand_objects_length = array_length_1d(hand_objects);

for (var i = 0; i < hand_objects_length; i += 1)
{
	var hand_object = hand_objects[i];
	var dx = hand_object.x - hand_object.xprevious;
	var dy = hand_object.y - hand_object.yprevious;
	traveled += sqrt(sqr(dx) + sqr(dy)) / hand_objects_length;
	x_diff += dx / hand_objects_length;
	y_diff += dy / hand_objects_length;
	
	free_ifpogo(hand_object);
}

var gain_target = min(traveled / 24, 1);
var freq_target = abs(sin(x_diff - y_diff)) * 2;

gain = lerp(gain, gain_target, 0.2);
freq = lerp(freq, freq_target, 0.2);

if gain > 0.02
{
	if hum_sound == noone or !audio_is_playing(hum_sound)
	{
		hum_sound = audio_play_sound(sndHandHum, -1, true);
		audio_sound_gain(hum_sound, 0, 0);
	}
		
	audio_sound_gain(hum_sound, gain, 33);
	audio_sound_pitch(hum_sound, freq);
}
else if hum_sound != noone
{
	audio_stop_sound(hum_sound);
	hum_sound = noone;
}