{
    "id": "4a2d3ff5-ea0b-4185-b5c4-2b30a28bfe90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBagUpgrade",
    "eventList": [
        {
            "id": "218b64e0-b46a-4bef-8eac-dd42a24c027c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a2d3ff5-ea0b-4185-b5c4-2b30a28bfe90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c8ff8d82-948f-4875-b52e-6b0807ea0545",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}