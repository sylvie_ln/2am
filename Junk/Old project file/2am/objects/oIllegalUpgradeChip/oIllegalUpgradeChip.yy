{
    "id": "8938cd96-cb2d-4945-bbc6-0a9c0b655436",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oIllegalUpgradeChip",
    "eventList": [
        {
            "id": "ed51e66a-dda5-4a31-b141-e58ce3f26c0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8938cd96-cb2d-4945-bbc6-0a9c0b655436"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3e960114-f332-49a5-a401-7cb55db57b2a",
    "visible": true
}