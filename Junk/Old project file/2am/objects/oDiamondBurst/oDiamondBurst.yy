{
    "id": "73b1c946-9bda-47fd-a601-4004c5ed00fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDiamondBurst",
    "eventList": [
        {
            "id": "18df25a1-bfaf-4b36-819e-1f3075522755",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "73b1c946-9bda-47fd-a601-4004c5ed00fb"
        },
        {
            "id": "c964a9c6-3c30-474e-946e-573a7962fbfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "73b1c946-9bda-47fd-a601-4004c5ed00fb"
        },
        {
            "id": "4ca4a79c-4e32-47fb-8ff6-68c4992704e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73b1c946-9bda-47fd-a601-4004c5ed00fb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a985f6ba-1689-49c5-8c48-2bb13b8e29e5",
    "visible": true
}