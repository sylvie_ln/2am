{
    "id": "fba14f56-e5ec-448b-bb0a-cee032e17902",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHouse",
    "eventList": [
        {
            "id": "f5345aa1-16bb-4099-893a-caafb2f5d1e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fba14f56-e5ec-448b-bb0a-cee032e17902"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "36da6698-a114-4ce3-93f1-e431de663638",
    "visible": true
}