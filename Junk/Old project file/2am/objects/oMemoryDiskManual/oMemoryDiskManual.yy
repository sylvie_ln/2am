{
    "id": "a1871553-068b-486b-8a3b-4833c55346b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMemoryDiskManual",
    "eventList": [
        {
            "id": "9198e57e-0ab1-4604-84c7-0f57e7178e23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1871553-068b-486b-8a3b-4833c55346b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "01835a3e-538f-44a7-a392-a3aca9e6046f",
    "visible": true
}