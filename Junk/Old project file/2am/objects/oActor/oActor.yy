{
    "id": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oActor",
    "eventList": [
        {
            "id": "ceeae8d4-c8df-46ce-841c-107d6eda5f31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "fb140bbd-5067-4db2-8557-ac0e931bd3f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "3ca27e8f-5e00-4cf1-8396-8ff04ff54177",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "949b4613-9b54-466d-a16f-39037c34c956",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "6cdd01c7-5b19-423d-9106-27c38c43c3f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d575649-441d-43d2-86d9-f646b788338d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}