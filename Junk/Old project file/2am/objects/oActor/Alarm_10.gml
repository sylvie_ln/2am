move_timer--;
var factor = 1-(move_timer/move_time);
//disabled_show_debug_message(string(factor)+" "+string(move_timer)+"/"+string(move_time));
x = lerp(move_start_x,move_target_x,factor);
y = lerp(move_start_y,move_target_y,factor);
if move_timer <= 0 {
	x = move_target_x;
	y = move_target_y;
	with move_mover {
		stop = false;
		event_user(1);
	}
} else {
	alarm[10] = 1;
}