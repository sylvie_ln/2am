timer--;
if timer <= 0 {
	timer = time;
	var block = instance_create_depth(room_width,h,depth-1,oMinecraftBlock);
	block.image_yscale = ceil((room_height-h)/2);
	with block { event_user(0); }
	block.spd = spd;
	last = minecart_choose(-last,-abs(last),clamp(-round(last/2),-1,1),last,last*2,last*4,last*8,clamp(round(last/2),-1,1))
	if last == 0 { last = minecart_choose(-1,1); }
	last = clamp(last,-4,128);
	h += last;
	h = clamp(h,64,room_height-20)
}

if !instance_exists(oMinecraftIntro) {
	dimer--;
	if dimer <= 0 {
		dimer = dime;
		var dh = round(minecart_random(24,h-24)/16)*16;
		var di = instance_create_depth(room_width+8,dh,depth-1,oDiamond);
		di.spd = spd;	
	}
}