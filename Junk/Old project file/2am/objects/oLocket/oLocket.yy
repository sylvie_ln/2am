{
    "id": "0969d4b7-5b1a-4ad9-8197-79d8f75af269",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLocket",
    "eventList": [
        {
            "id": "0eb7d9e2-5688-411c-9b2a-1da9d32b33ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0969d4b7-5b1a-4ad9-8197-79d8f75af269"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "af263178-8187-446d-9ea6-d91c70501203",
    "visible": true
}