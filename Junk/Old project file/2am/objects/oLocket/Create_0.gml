event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = @"Sunny's precious locket. 
You aren't even supposed to be able to obtain this item, so it would be rude to look at what's inside it. You think you have a guess, though...."

damp = 0.75;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!