event_inherited();
/*
var temp = [ds_list_create(),ds_map_create(),ds_grid_sylvie_create(1,1),ds_queue_create(),ds_priority_create(),ds_stack_create(),];
global.trcount++;
//disabled_show_debug_message(string(global.trcount)+" "+room_get_name(room)+" "+string(temp)+" "+string(get_timer()/1000000));
ds_list_destroy(temp[0]);
ds_map_destroy(temp[1]);
ds_grid_sylvie_destroy(temp[2]);
ds_queue_destroy(temp[3]);
ds_priority_destroy(temp[4]);
ds_stack_destroy(temp[5]);
*/
base_grav = 0.15;

max_air = 30*global.default_room_speed;
air_base = max_air;
air_reserves = 0;
air = air_base;
air_multiplier = 27;

pushable = true;
carriable = true;
event_perform(ev_step,ev_step_begin);
event_user(12);
consecutive_bounce_count = 0;

dead = false;
dead_stage = 0;
respawn_time = global.default_room_speed;
max_respawn_time = global.default_room_speed*5;

ong = true;
ong_prev = ong;
vv_prev = vv;
inf_jump = false;
jumped = true;

enum eChar {
	apply_friction = 10,
	do_movement = 11,
	bounce_off = 12
}

enum aChar {
	respawn = 0,
	force_respawn = 1,
}

bx = (x div global.view_width)*global.view_width;
by = (y div (global.view_height-20))*(global.view_height-20);

npc = noone;
npcs = ds_list_sylvie_create();
conversing = false;

paused = false;

respawn = false;

jump_buffer_time = (global.default_room_speed div 6);
jump_buffer_timer = 0;

// Bunney
bunney = false;

// Boot's
pegasus = false;
pegasus_timer = 0;
pegasus_time = global.default_room_speed;
pegasus_boost = spd/2;
pegasus_hdir = 0;

hover = false;
hover_multiplier = 10;
walkoff_time = global.default_room_speed div 10;
walkoff_timer = walkoff_time;

climber = false;

springey = false;
springey_damp = 0.75;
springey_springed  = false;
springey_max_height = 320;

heavy = false;

quiet = false;

riding = false;

// Chicken Wings
wings = false;

// Ening
ending_timer = global.default_room_speed*3;

global.current_room = room_get_name(room);

npc_buffer = 0;
npc_buffer_max = global.default_room_speed div 4;

window_set_cursor(cr_none);