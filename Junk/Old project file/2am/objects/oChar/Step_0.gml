event_user(12);
var walkoff_time = self.walkoff_time;
if hover { walkoff_time *= hover_multiplier; }
var acc = self.acc;
if hover { acc /= lerp(2,1,min(spd,abs(hv))/spd); }
vv_prev = vv;
var grav = self.grav;
var jmp = self.jmp;

paused =
(instance_exists(oShifter) and oShifter.shifting) or
(instance_exists(oInTheBag)) or 
is_pause_menu_open();

if paused or (global.closed_bag and alarm[5] >= 0){
	exit;
}

var at_door = place_meeting(x,y,oDoor);
if !at_door {
	var memory = instance_place(x,y,oMemory);
	if memory != noone {
		if memory.sprite_index == sMemoryRevived {
			at_door = true;	
		}
	}
}
if room == rmMushZone and at_door {
	last_door_x = oRoomGen.xp;
	last_door_y = oRoomGen.yp;
}

conversing = instance_exists(npc) and (npc.talking or npc.alarm[10] > 0 or npc.cutscene);

if !dead {
	if input_pressed("Shroom") {
		if !conversing and !riding and instance_exists(oEivlysCurse) {
			npc = oEivlysCurse.id;
			if !conversing {
				with npc {
					event_user(1);	
				}
			}
		} else if !conversing and !riding and !global.closed_bag and room != rmLegendaryTree and !global.flag_finale {
			var bag = instance_create_depth(0,0,depth,oInTheBag);
			bag.mode = 1;
			bag.holding = false;
			bag.original = false;
			with bag { event_user(0); }
			exit;
		}
	}


	if fall_through > 0 {
		fall_through--;	
	}
	if npc_buffer > 0 {
		npc_buffer--;	
	}
	if input_pressed("Down") and !instance_exists(npc) {
		fall_through = 3;
		npc_buffer = npc_buffer_max;
		if ong {
			walkoff_timer = walkoff_time;
		}
	}
}


ong = collision_at(x,y+1) and (springey_springed or vv >= 0);
conversing = instance_exists(npc) and (npc.talking or npc.alarm[10] > 0 or npc.cutscene);

if ong {
	consecutive_bounce_count = 0;
	if !springey_springed {
		walkoff_timer = 0;
	}
	jumped = false;
}
if !ong and vv > 0 {
	springey_springed = false;
}

if !conversing {
	var ladder = instance_place(x,y,oLadder);
	var snakey = false;
	if ladder == noone {
		ladder = instance_place(x,y,oSnakeRope);	
		if instance_exists(ladder) {
			if ladder.mask_index == sSnakeHitbox {
				snakey = true and room == rmMushZone;
			} else {
				ladder = noone;	
			}
		}
	}
	if dead or ladder == noone or (!snakey and !ladder.upblocker) {
		if !ong {
			if walkoff_timer >= walkoff_time {
				vv += grav;
			}
			walkoff_timer++;
		}
	} else if snakey or ladder.upblocker {
		if input_held("Up") {
			vv -= grav * (snakey ? 2 : 1);
		} 
		if input_held("Down") and !ong {
			vv += grav * (snakey ? 2 : 1);	
		}
	}	
}

/*
if keyboard_check_pressed(ord("R")) {
	event_perform(ev_alarm,aChar.respawn);
	exit;
}
*/

if dead {
	var prefix = bunney ? "sBunney" : "sChar";
	if dead_stage == -1 {
		dead_stage++;	
	}
	if dead_stage == 0 {
		sprite_index = asset_get_index(prefix+"Die");
	} 
	if dead_stage == 1 {
		mask_index = asset_get_index(prefix+"DieMask");
		if ong {
			dead_stage++;	
		} else {
			sprite_index = asset_get_index(prefix+"DieFall");
		}
	} 
	if dead_stage == 2 {
		sprite_index = asset_get_index(prefix+"DieLand");
		if !ong {
			dead_stage = 1;	
			image_speed = 1;
			alarm[aChar.respawn] = -1;
		}
	}
	event_user(eChar.apply_friction);
	event_user(eChar.do_movement);
	exit;
}

ds_list_clear(npcs);
var num = instance_place_list(x,y+(npc_buffer > 0 and npc_buffer != npc_buffer_max ? 2 : 0),oNPC,npcs,true);
if !conversing and !riding {
	if num == 0 and npc_buffer <= 0 { npc = noone; }
	else if num == 1 { 
		if !npc_ignore(npcs[|0]) {
			npc = npcs[|0]; 
		} else if npc_buffer <= 0 {
			npc = noone;	
		}
	}
	else {
		if npc != noone and instance_exists(npc) {
			var dist = point_distance(x,y,npc.x,npc.y);
			if !npc.visible { dist = 1024; }
		} else {
			var dist = 1024;	
		}
		var nothing_found = true;
		for(var i=num-1; i>=0; i--) {
			if npc_ignore(npcs[|i]) { 
				continue;
			}
			nothing_found = false;
			var nn = npcs[|i];
			var pdist = point_distance(x,y,nn.x,nn.y);
			if !nn.visible { pdist = 1024; }
			if pdist < dist {
				npc = nn;
			}
		}
		if nothing_found and npc_buffer <= 0  { npc = noone; }
	}
}
var done = false;
if alarm[4] < 0 {
	with npc {
		if cutscene and !talking { continue; }
		if alarm[10] > 0 { continue; }
		if (!talking and input_pressed("Up"))
		or (talking and input_pressed(oInput.act1)) {
			event_user(1);	
			if !talking { 
				done = true;	
			}
		} 
		if !talking and input_pressed("Down") {
			event_user(2);	
		}
	}
}
if conversing or done {
	exit;
}

if !riding {
	if bag_has_item(oJetpack) {
		if input_held("Jump") {
			if heavy {
				vv -= grav-(grav/4);
			} else {
				if vv > grav { vv -= grav; }
				vv -= grav*2;
			}
			if room == rmMushZone {
				air_base -= global.default_room_speed/6;
			}
		} 
	}
}

var refill_air = at_door or place_meeting(x,y,oEmergencyBubble);

if room == rmMushZone {
	if !refill_air {
		if !riding {
			air_base -= (1/(power(2,bag_item_count(oOxygenEgg)/3))) * global.assist_air_drain;
		}
	} else {
		air_base = max_air;	
	}
	air = air_base + air_reserves;
	if air <= 0 {
		air = 0;
		die(self);
		exit;
	}
}

play_character_air_sound(refill_air);


if input_pressed("Bounce") {
	//global.bouncey = !global.bouncey;	
	//pegasus = !pegasus;
}

var left = input_held("Left");
var right = input_held("Right");
var hdir = right-left;
if left and right {
	hdir = (input_held_time("Left") <= input_held_time("Right")) ? -1 : 1;
}

if !riding or (riding and place_free(x,y)) {
	if !pegasus {
		if abs(hv+acc*hdir) <= spd or hdir != sign(hv) {
			if hover {
				if hdir != sign(hv) and abs(hv) <= spd {
					hv += (acc*(1-clamp(abs(hv)/spd,0,1)))*hdir;
				} else {
					hv += acc*hdir;	
				}
			} else {
				hv += acc*hdir;
			}
		} else {
			if abs(hv) < spd {
				if hdir > 0 {
					hv = spd;	
				} else if hdir < 0 {
					hv = -spd;	
				}
			}
		}
	} else {
		image_speed = 1;
		if pegasus_timer < pegasus_time and (pegasus_timer > 0 or hdir != 0) {
			pegasus_timer++;
			if hdir != 0 and (input_pressed("Left") or input_pressed("Right") or pegasus_timer == global.default_room_speed div 3) {
				pegasus_hdir = hdir;
			}
			if pegasus_timer > global.default_room_speed div 3 and pegasus_hdir == 0 {
				pegasus_timer = 0;	
			}
			image_speed = 2;
			if pegasus_timer == pegasus_time {
				pegasus_hdir = image_xscale;	
			}
		} else {	
			image_speed = 2;
			if hv == 0 or (hv != 0 and (hdir != -pegasus_hdir)) {
				/*if pegasus_hdir == 1 {
					hv = max(hv,(spd+pegasus_boost)*pegasus_hdir);
				} else if pegasus_hdir == -1 {
					hv = min(hv,(spd+pegasus_boost)*pegasus_hdir);
				}*/
				if pegasus_hdir != 0 {
					hv = (spd+pegasus_boost)*pegasus_hdir;
				} else {
					pegasus_timer = 0;	
				}
			} else {
				pegasus_timer = 0;
				pegasus_hdir = 0;
			}
		}
	}

	event_user(eChar.apply_friction);

	if jump_buffer_timer > 0 {
		jump_buffer_timer--;	
	}
	var pressed_jump = input_pressed("Jump");
	if pressed_jump or input_held("Jump") {
		if pressed_jump {
			jump_buffer_timer = jump_buffer_time;
		}
		if ((ong and jump_buffer_timer > 0) 
		    or (pressed_jump and inf_jump) 
			or (!ong and pressed_jump and walkoff_timer < walkoff_time)
			or (wings))  {
			if !(wings and !ong) {
				play_sound(sndCharJump);
			}
			vv = -jmp;	
			jumped = true;
			walkoff_timer = walkoff_time;
			jump_buffer_timer = 0;
		}
	}

	if global.closed_bag and input_held("Jump") and (ong or inf_jump or walkoff_timer < walkoff_time) {
		vv = -jmp;
		jumped = true;	
		walkoff_timer = walkoff_time;
		jump_buffer_timer = 0;
		global.closed_bag = false;
	}
	
	/*
	if jumped and walkoff_timer == walkoff_time and global.assist_pop_mushrooms {
		with collision_rectangle(bbox_left-2,bbox_bottom,bbox_right+2,bbox_bottom+2,oBasicShroom,false,true) {
			if object_index == oBasicShroom and beside(other,1,-1) {
				instance_destroy();	
				if room != rmCoolMuseum {
					if generated and instance_exists(oRoomGen) {
						oRoomGen.local_shroom_deletion_map[? room_key(oRoomGen.xp,oRoomGen.yp)+pos_key(xt,yt)] = object_index;
					}
					if placed and !instance_exists(oRoomGen) and !always_respawn {
						global.shroom_deletion_map[? get_that_room()+pos_key(xstart,ystart)] = true;
					}
				}
				with instance_create_depth(x,y,oChar.depth-1,oBasicPop) {
					image_index = other.image_index;	
				}
			}
		}	
	}
	*/

	if jumped and input_released("Jump") and vv < 0 {
		vv /= 2;
		jumped = false;
	}
}

event_user(eChar.do_movement);

if !quiet {
	play_character_step_sound();
}

if hdir != 0 {
	image_xscale = hdir;	
}

var spr = "";
var prefix = "sChar";
if bunney {
	prefix = "sBunney";
}
if !ong and walkoff_timer >= walkoff_time and pegasus_timer == 0 {
	if vv <= 0 {
		spr = "J";
	} else {
		spr = "F";	
	}
} else {
	if hdir != 0 
	or pegasus_timer != 0
	or (hover and abs(hv) >= 0.5)
	or (hover and !ong) {
		spr = "R";	
	} else {
		spr = "S";
	}
}
var spr = asset_get_index(prefix+spr);
if sprite_exists(spr) { sprite_index = spr; }

ong_prev = ong;


if is_pause_key_pressed() and !global.closed_bag and !global.flag_finale {
	instance_create_depth(0, 0, -1000, oPauseMenu);
	exit;
}

if !input_held("Jump") {
	global.closed_bag = false;	
}