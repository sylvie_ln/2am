///@description Update jump & speed values

var gspeed = round(30 * (global.assist_gamespeed == 0 ? 1/10 : global.assist_gamespeed));
if game_get_speed(gamespeed_fps) != gspeed {
	game_set_speed(gspeed, gamespeed_fps);
}
var jump_power = (global.assist_jump_power > 1 ? power(global.assist_jump_power,global.assist_jump_power) : global.assist_jump_power);
var grav_power = (global.assist_gravity > 1 ? sqr(global.assist_gravity) : global.assist_gravity);
grav = (base_grav * grav_power);
var gravy = grav == 0 ? base_grav : grav;
bounce = sqrt(2*gravy*4);
climber_bounce = sqrt(2*gravy*6);
jmp = sqrt(2*gravy*(33*jump_power));
if heavy { gravy *= 2; grav = (grav_power == 0 ? 0 : gravy); jmp = sqrt(2*gravy*(17*jump_power)); }

spd = (3.33*global.assist_speed_power);
acc = spd/8;
fric = acc;
spd += fric;
acc += fric;
