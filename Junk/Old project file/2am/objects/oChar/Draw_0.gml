if bag_has_item(oBunneyEars) {
	draw_sprite_ext(sBunneyEars,0,x,bbox_top-4,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
if bag_has_item(oMiniBunney) {
	draw_sprite_ext(sMiniBunney,0,x,bbox_top-8,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
if bag_has_item(oChickenWings) {
	draw_sprite_ext(sCharWings,image_index,x,y,
	image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}


draw_self();

var yp = y;
if room == rmMushZone and (global.assist_air_drain != 0) {
	var air_text = "AIR "+string(round(air/max_air*100));
	var xp = x-((string_width(air_text)-string_width(" ")) div 2);
	yp += 9
	//yp -= 9+8
	draw_set_font(global.neco_font);
	draw_set_color(c_black);
	draw_text(xp+1,yp,air_text);	
	draw_text(xp-1,yp,air_text);	
	draw_text(xp,yp-1,air_text);	
	draw_text(xp,yp+1,air_text);	
	draw_text_color(xp,yp,air_text,c_white,c_white,make_color_rgb(0xcb,0xdb,0xfc),make_color_rgb(0xcb,0xdb,0xfc),1);	
}

if false and global.bouncey {
	var b_text = "You are Bouncey";
	var xp = x-((string_width(b_text)-string_width(" ")) div 2);
	yp += 9;
	draw_set_font(global.neco_font);
	draw_set_color(c_black);
	draw_text(xp+1,yp,b_text);	
	draw_text(xp-1,yp,b_text);	
	draw_text(xp,yp-1,b_text);	
	draw_text(xp,yp+1,b_text);	
	draw_text_color(xp,yp,b_text,c_white,c_white,make_color_hsl_240(210,239,200),make_color_hsl_240(210,239,200),1);	
}

if instance_exists(npc) and !conversing and !riding
and !npc_ignore(npc)
and !(npc.object_index == oPuzzlePiece and !npc.visible) {
	
	play_npc_option_threshold_sound();
	
	var b = 4;
	var cl = camera_get_view_x(view_camera);
	var ct = camera_get_view_y(view_camera);
	var cr = cl+camera_get_view_width(view_camera);
	var cb = ct+camera_get_view_height(view_camera)-20;
	
	draw_set_font(global.sylvie_font_bold);
	var xp = npc.x;
	var yp = npc.y-16-(npc.sprite_height/2)
	if room == rmMushZone {
		yp -= 9;	
	}
	if yp-24 < ct {
		yp = npc.bbox_bottom+16+b;
	}
	var t1 = chr(ord("~")+edir.up)+" "+npc.act1;
	var t2 = chr(ord("~")+edir.down)+" "+npc.act2;
	var w = max(string_width(t1),string_width(t2));
	xp = clamp(xp,cl+b+(w div 2),cr-b-(w div 2));
	draw_set_color(c_black);
	draw_text_centered(xp+1,yp-16,t1);
	draw_text_centered(xp+1,yp,t2);
	draw_text_centered(xp-1,yp-16,t1);
	draw_text_centered(xp-1,yp,t2);
	draw_text_centered(xp,yp+1-16,t1);
	draw_text_centered(xp,yp+1,t2);
	draw_text_centered(xp,yp-1-16,t1);
	draw_text_centered(xp,yp-1,t2);
	draw_set_color(c_white);
	draw_text_centered(xp,yp-16,t1);
	draw_text_centered(xp,yp,t2);
}