/// @description Do movement

	var fric = self.fric;
	if hover { fric /= 4; }
	var walkoff_time = self.walkoff_time;
	if hover { walkoff_time *= hover_multiplier; }

	if vv != 0 {
		//disabled_show_debug_message(vv);	
	}

	var px = x;
	var py = y;
	
if !riding or (riding and place_free(x,y)){ 
	if !move(hv,0) { 
		if !(dead and dead_stage == -1) {
			if hv != 0 and global.bouncey {
				var poff = pegasus ? pegasus_boost : 0;
				if !climber {
					if abs(hv) < ((spd+poff)*2-fric*2) {
						vv -= bounce; 
						walkoff_timer = walkoff_time;
					}
					hv = -sign(hv)*spd*2; 
				} else {
					if abs(hv) < ((spd+poff)*1.5-fric*2) {
						vv -= climber_bounce; 
						walkoff_timer = walkoff_time;
					}
					hv = -sign(hv)*spd*1.5; 
				}
			
				play_character_bounce_sound(consecutive_bounce_count++);
			
				if pegasus {
					pegasus_hdir = -pegasus_hdir;
					if pegasus_hdir != 0 {
						image_xscale = pegasus_hdir;
					}
				}
			} else {
				hv = 0;	
			}
		}
	}

	if !move(vv,1) {
		if !(dead and dead_stage == -1) {
			if vv > 0 {
				play_character_land_sound(vv);
			}
			//disabled_show_debug_message("prev: "+string(vv));
			if springey and vv > 0 {
				var height = sqr(vv)/(2*grav);
				if input_held("Jump") {
					var new_height = max(height+2,height/springey_damp);
					if height <= 8 {
						new_height = max(height+2,height*2);	
					}
					//disabled_show_debug_message("newh: "+string(new_height));
					new_height = min(new_height,springey_max_height);
					vv = -sqrt(2*grav*new_height);
					springey_springed = true;
					walkoff_timer = walkoff_time;
					//jump_buffer_timer = 0;
				} else {
				//disabled_show_debug_message("high: "+string(height));
					if height <= 4 { 
						vv = 0;
					} else {
						var new_height = min(height-2,height*springey_damp);
						if height <= 8 {
							new_height = min(height-2,height/2);	
						}
						new_height = min(new_height,springey_max_height);
						//disabled_show_debug_message("newh: "+string(new_height));
						vv = -sqrt(2*grav*new_height);
						springey_springed = true;
						walkoff_timer = walkoff_time;
						//jump_buffer_timer = 0;
					}
				}
			} else {
				if springey and vv < 0 {
					vv = -vv;	
				} else {
					vv = 0; 
				}
			}
			//disabled_show_debug_message("post: "+string(vv));
		}
	}
}


if bbox_bottom < by {
	if room = rmTowerTop {
			
	} else if room == rmLegendaryTree {
		y += global.view_height;	
	} else {
		x = px;
		if instance_exists(oRoomGen) {
			with oRoomGen {
				yp -= 1;
				shift_dir = edir.up;
				event_user(eRoomGen.shift);
			}
		} else {
			with oShifter {
				shift_dir = edir.up;
				event_user(0);
			}
		}
	}
}
if bbox_top >= by+global.rheight {
	if room = rmTowerTop {
		if alarm[3] < 0 {
			alarm[3] = ending_timer;	
		}
	} else if room == rmLegendaryTree {
		if bbox_top >= by+global.view_height {
			y -= global.view_height;
		}
	} else {
		x = px;
		if instance_exists(oRoomGen) {
			with oRoomGen {
				yp += 1;
				shift_dir = edir.down;
				event_user(eRoomGen.shift);
			}
		} else {
			with oShifter {
				shift_dir = edir.down;
				event_user(0);
			}
		}
	}
}
if bbox_right < bx {
	if room = rmTowerTop {
			
	} else if room == rmLegendaryTree {
		x = max(bx-32,x);
	} else {
		y = py;
		if instance_exists(oRoomGen) {
			with oRoomGen {
				xp -= 1;
				shift_dir = edir.left;
				event_user(eRoomGen.shift);
			}
		} else {
			with oShifter {
				shift_dir = edir.left;
				event_user(0);
			}
		}
	}
}
if bbox_left >= bx+global.rwidth {
	if room = rmTowerTop {
			
	} else if room == rmLegendaryTree {
		x = min(bx+global.rwidth+32,x);
	} else {
		y = py;
		if instance_exists(oRoomGen) {
			with oRoomGen {
				xp += 1;
				shift_dir = edir.right;
				event_user(eRoomGen.shift);
			}
		} else {
			with oShifter {
				shift_dir = edir.right;
				event_user(0);
			}
		}
	}
}