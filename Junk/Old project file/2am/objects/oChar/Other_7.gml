if dead {
	var prefix = bunney ? "sBunney" : "sChar";
	if dead_stage == 0 {
		dead_stage++;
		image_index = 0;
		if ong {
			sprite_index = asset_get_index(prefix+"DieLand");
			image_index = sprite_get_number(sprite_index)-1;	
		}
	} else if dead_stage == 2 {
		alarm[aChar.respawn] = respawn_time;
		image_speed = 0;
		dead_stage++;
	}
}