{
    "id": "f759267d-7113-4d07-b79a-dca54556d58d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGiftShopSnowGlobe",
    "eventList": [
        {
            "id": "81862df9-701c-470e-a585-aacc8e0eae2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f759267d-7113-4d07-b79a-dca54556d58d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5c17df20-ef08-42c4-a128-16c0c7c0ce60",
    "visible": true
}