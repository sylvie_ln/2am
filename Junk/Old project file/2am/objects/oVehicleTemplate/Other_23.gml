var left = input_held("Left");
var right = input_held("Right");
var lp = input_pressed("Left");
var rp = input_pressed("Right");
var hdir = right-left;
if left and right {
	hdir = (input_held_time("Left") <= input_held_time("Right")) ? -1 : 1;
}

if hdir != 0 {
	hv = -hdir*spd;
}

var ong = collision_at(x,y+1) and (bounced or vv >= 0);
do_friction = ong;
if input_held("Jump") and ong {
	vv = -jmp;
}