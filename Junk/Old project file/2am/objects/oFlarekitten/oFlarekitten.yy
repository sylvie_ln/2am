{
    "id": "f5514deb-9efc-4530-a09a-fe76b699a366",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlarekitten",
    "eventList": [
        {
            "id": "8d308bd2-5b63-4000-b515-d4a31c4e3f97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f5514deb-9efc-4530-a09a-fe76b699a366"
        },
        {
            "id": "f8bcc92b-8417-43bf-b8db-e59c545e1be0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f5514deb-9efc-4530-a09a-fe76b699a366"
        },
        {
            "id": "6dd3180d-486b-4a31-b257-99ffca88950e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "f5514deb-9efc-4530-a09a-fe76b699a366"
        },
        {
            "id": "59ec22ef-21df-4f8f-9e93-dfbf90758618",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "f5514deb-9efc-4530-a09a-fe76b699a366"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c8ff8d82-948f-4875-b52e-6b0807ea0545",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "210ded78-a5ba-4da9-acba-66a6839c2fc6",
    "visible": true
}