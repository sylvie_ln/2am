event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A squishey soft plushey of a Kittey Statue from the Cavern of Ancients, it is so Cute!"

damp = 0.95;
bounce = damp;
fric = 0.95;
grav /= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!