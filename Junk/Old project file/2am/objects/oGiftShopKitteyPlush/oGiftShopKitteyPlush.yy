{
    "id": "28c8b45e-e8d7-45ce-9b61-d338f136131c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGiftShopKitteyPlush",
    "eventList": [
        {
            "id": "9393e4c9-8f23-4f02-a7f8-beaa9167aa57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "28c8b45e-e8d7-45ce-9b61-d338f136131c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0c3fc0c0-dcbf-4ad6-8ac4-855112e15d02",
    "visible": true
}