{
    "id": "e924812f-4202-407b-8b57-05a090f9710d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpike",
    "eventList": [
        {
            "id": "506e7022-c91d-47ce-9b40-b5ba768248ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e924812f-4202-407b-8b57-05a090f9710d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0cbf5b0e-b25e-4a0a-adfe-d022891eaa62",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "02fb89e3-1ca2-4aed-9e2c-5be9d97e4b9b",
    "visible": true
}