event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "The Legendary Golden Bunney Amulet of the Legends. It is rumored it will allow you to speak to bunneys. But at what cost?"

damp = 0.75;
bounce = damp;
fric = 0.95;

//grav *= 2;

image_speed = 0;

/// In oInit, define a trade entry for this item!