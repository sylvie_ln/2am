event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "An extremely wide shroom, perfect for things like standing on."

damp = 0.5;
bounce = damp;
fric = 0.95;
depth += 4;

image_speed = 0;
if room != rmCoolMuseum {
	image_index = sylvie_irandom(sprite_get_number(sprite_index)-1);
}