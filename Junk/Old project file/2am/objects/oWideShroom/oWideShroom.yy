{
    "id": "2169f124-ea3e-4f65-b833-cfd4d34c8b34",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWideShroom",
    "eventList": [
        {
            "id": "4c264a41-db7d-4198-8d69-414fd0104568",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2169f124-ea3e-4f65-b833-cfd4d34c8b34"
        },
        {
            "id": "79257cdd-10ea-42fa-a9e6-c945f8018b2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2169f124-ea3e-4f65-b833-cfd4d34c8b34"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83a62301-4765-460d-8f78-21e1ea647f89",
    "visible": true
}