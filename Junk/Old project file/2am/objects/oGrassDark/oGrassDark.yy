{
    "id": "99462db7-1a4a-4e18-9c10-5ff3528f17b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGrassDark",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "48c12c2f-c326-4494-b6f7-dae1ed760e9d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1ed19ba7-d9c2-4fc7-b05d-432091e2cb41",
    "visible": true
}