{
    "id": "e5752219-871c-4df2-bbbf-dd83bb02a47f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinecraft",
    "eventList": [
        {
            "id": "330231b0-bb53-4cb1-9dd7-38f78df8923d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e5752219-871c-4df2-bbbf-dd83bb02a47f"
        },
        {
            "id": "2be9a4a3-1dd1-4351-873d-d231b8015f8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e5752219-871c-4df2-bbbf-dd83bb02a47f"
        },
        {
            "id": "53fef3c4-490c-4d55-a049-6a242ff001a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5752219-871c-4df2-bbbf-dd83bb02a47f"
        },
        {
            "id": "b331fdca-5b02-49e7-94ad-1c5208b4011e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e5752219-871c-4df2-bbbf-dd83bb02a47f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b6b176ab-cc61-4c5b-98fc-86b02ba92c1a",
    "visible": true
}