// Inherit the parent event
event_inherited();

if !global.mute_sfx {
	audio_stop_sound(move_sound);
}