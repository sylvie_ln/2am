var should_soften_move_sound = paused or !place_meeting(x,y+1,oMinecraftBlock);
if !global.mute_sfx {
 audio_sound_gain(move_sound, should_soften_move_sound ? 0 : 1, should_soften_move_sound ? 100 : 25);
}
if paused { exit; }

xo = 0;
if input_held("Left") {
	xo = -3;
} else if input_held("Right") {
	xo = 3;
}

while place_meeting(x,y,oMinecraftBlock) {
	y -= 1;
	vv -= 0.5;
}
if !place_meeting(x,y+1,oMinecraftBlock) {
	vv += grav;	
}

if !move(vv,1) {
	vv = 0;	
}
with instance_place(x,y,oDiamond) {
	var b = instance_create_depth(x,y,depth+2,oDiamondBurst);
	b.spd = spd;
	var m = instance_create_depth(other.x,other.y,depth-1,oMinecraftPoints);
	other.count++;
	m.multi = other.count;
	m.vspeed = min(0,other.vspeed)-4;
	instance_destroy();	
}