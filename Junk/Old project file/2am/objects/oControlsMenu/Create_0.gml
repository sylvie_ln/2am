enum control_options {
	gamepad_on,
	gamepad_deadzone,
	key_config,
	up_key,
	down_key,
	left_key,
	right_key,
	jump_key,
	bag_key,
	cancel_key,
	back,
	last
}

words[control_options.gamepad_on] = "Gamepad Enabled"
words[control_options.gamepad_deadzone] = "Gamepad Deadzone"
words[control_options.key_config] = "* Buttons *"
words[control_options.up_key] = "Up"
words[control_options.down_key] = "Down"
words[control_options.left_key] = "Left"
words[control_options.right_key] = "Right"
words[control_options.jump_key] = "Jump/Confirm"
words[control_options.bag_key] = "Open/Close"
words[control_options.cancel_key] = "Pause/Cancel"
words[control_options.back] = "I'm done!"
selected = 0;


for(var i=0; i<control_options.last; i++) {
	act[i] = "";	
}
act[control_options.up_key] = "Up"
act[control_options.down_key] = "Down"
act[control_options.left_key] = "Left"
act[control_options.right_key] = "Right"
act[control_options.jump_key] = "Jump"
act[control_options.bag_key] = "Shroom"
act[control_options.cancel_key] = "Escape"

defkey[control_options.up_key] = vk_up
defkey[control_options.down_key] = vk_down
defkey[control_options.left_key] = vk_left
defkey[control_options.right_key] = vk_right
defkey[control_options.jump_key] = vk_space
defkey[control_options.bag_key] = vk_control
defkey[control_options.cancel_key] = vk_escape

for(var i=0; i<control_options.last; i++) {
	selected_key[i] = 0;	
}
waiting = false;

active = false;
previous = ds_list_create();