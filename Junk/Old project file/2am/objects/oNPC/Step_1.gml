paused = 
	(instance_exists(oShifter) and oShifter.shifting)
or  (instance_exists(oInTheBag))
or	(outside_view() 
    and (very_solid_boundary or !(instance_exists(oChar) and beside(oChar,1,-1) and place_meeting(x,y-2,oChar)))
    and !object_is_ancestor(object_index,oHand)
	and object_index != oFlarekitten
    and object_index != oCalledHand
	and object_index != oRaceRunner
	)
or  (instance_exists(oChar) and oChar.conversing and !throw_ns) 
or  (talking)
or (object_is_ancestor(object_index,oShroom) and !object_is_ancestor(object_index,oVehicle) and room == rmCoolMuseum and placed)
or is_pause_menu_open();
if instance_exists(oChar) and !oChar.conversing {
	throw_ns = false;	
}