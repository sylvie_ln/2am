var sprr = asset_get_index("s"+name);
if sprite_exists(sprr) and object_index != oIntangibleNPC {
	cutey = false;
	image_blend = c_white;
	sprite_index = sprr;
}
if cutey {
	sylvie_set_seed((x*30389)+(47951*y)+(room*9999));
	deco = sylvie_irandom(sprite_get_number(sDecos)-1);
	deco_color = sylvie_choose(base_colors[sylvie_irandom(total_colors-1)],base_colors_dark[sylvie_irandom(total_colors-1)]);
	hair = sylvie_irandom(sprite_get_number(sHairs)-1);
	hair_color = pattern_colors[sylvie_irandom(total_colors-1)];
	clothe = sylvie_irandom(sprite_get_number(sClotheBases)-1);
	clothe_color = sylvie_choose(base_colors[sylvie_irandom(total_colors-1)],base_colors_dark[sylvie_irandom(total_colors-1)]);
	pattern = sylvie_irandom(sprite_get_number(sClothePatterns)-1);
	pattern_color = hair_color;
	while pattern_color == hair_color {
		pattern_color = pattern_colors[sylvie_irandom(total_colors-1)];
	}
	body_color = 
	sylvie_choose(
	make_color_hsl_240(sylvie_random(10,20),120,sylvie_choose(sylvie_random(60,90),sylvie_random(180,200))),
	make_color_hsl_240(sylvie_random(0,239),120,sylvie_choose(sylvie_random(60,90),sylvie_random(160,200))),
	);
}
event_user(0);
if global.flag_minecart == 1 and name == "Mineria" {
	event_user(1);	
}
if room == rmCherryBlossomHills and global.flag_legendary_tree == 0 and name == "Sunny" {
	instance_destroy();
}
if global.flag_legendary_tree == 1 and name == "Karie" {
	event_user(1);	
}