{
    "id": "99f130cf-2289-49f3-aab5-8ecdf99519f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpeedShroom",
    "eventList": [
        {
            "id": "a2a95a33-544c-40a4-88ac-a4e38fb0636e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99f130cf-2289-49f3-aab5-8ecdf99519f9"
        },
        {
            "id": "04a6dd3f-5760-4c82-a60f-17e50cdda8e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99f130cf-2289-49f3-aab5-8ecdf99519f9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6faa11b5-b733-40ca-94f8-3118341da6f8",
    "visible": true
}