if paused { exit; }

if thrown_timer > 0 {
	thrown_timer--;	
}

var ong = collision_at(x,y+1);
if ong and hv != spd and thrown_timer <= 0 {
	if hv == 0 { hv = sylvie_choose(-1,1); }
	hv = sign(hv)*spd;	
}
if !ong {
	if thrown_timer <= 0 {
		vv += grav;
	}
}

if !move(hv,0) {
	hv = -hv;	
}

if !move(vv,1) {
	if vv > 0 {
		if solid_at(x,y+1) or blocker_at(x,y+1) {
			if thrown_timer <= 0 {
				vv = 0;
			} else {
				vv = -vv*bounce;
			}
		} else {
			vv = -vv*bounce;	
			if abs(vv) < sqrt(2*grav*(54*bounce)) {
				vv = -sqrt(2*grav*(54*bounce));	
			}
		}
	} else {
		vv = 0;	
	}
}

if sign(hv) != 0 {
	image_xscale = sign(hv);	
}

if (ong and !collision_at(x,y+1))
or (ong and collision_at(x+96*sign(hv),y)) {
	vv = -jmp;
}