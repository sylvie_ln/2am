{
    "id": "743effd6-fcc1-4faa-8396-a3e8e46511f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRedShard",
    "eventList": [
        {
            "id": "2f9e101c-7f48-4420-a149-b4eb9e15310f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "743effd6-fcc1-4faa-8396-a3e8e46511f9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90859649-9100-4873-a63c-4b88b9127018",
    "visible": true
}