{
    "id": "97d49540-83f3-450a-bc90-abc32135ff1f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSylvieTruck",
    "eventList": [
        {
            "id": "b51dbc22-328c-43ac-ac51-ac0485020687",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97d49540-83f3-450a-bc90-abc32135ff1f"
        },
        {
            "id": "78f114b6-3683-4552-a38f-0b40c345c200",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "97d49540-83f3-450a-bc90-abc32135ff1f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "298f726f-789d-4e6b-810d-175c0fd82a15",
    "visible": true
}