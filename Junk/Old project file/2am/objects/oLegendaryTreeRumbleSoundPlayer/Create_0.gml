if !global.mute_sfx {
	sound = audio_play_sound(sndLegendaryTreeRumble, -1, true);
	audio_sound_gain(sound, 0, 0);
} else {
	sound = noone;	
}