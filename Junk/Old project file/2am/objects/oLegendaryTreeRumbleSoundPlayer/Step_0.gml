var target_gain = 0;

with oTreePortal
{
	if xo != 0 or yo != 0
		target_gain = 1;
}

if !global.mute_sfx {
	audio_sound_gain(sound, target_gain, 500);
}