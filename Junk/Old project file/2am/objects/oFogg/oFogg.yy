{
    "id": "fec9c3e4-4f0e-4fb1-8e7a-3f993215c051",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFogg",
    "eventList": [
        {
            "id": "0d4615fd-6bc5-4f6b-bbfe-70f198c478ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fec9c3e4-4f0e-4fb1-8e7a-3f993215c051"
        },
        {
            "id": "ef351a67-adce-4e7a-8e8a-ce99fe1765a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "fec9c3e4-4f0e-4fb1-8e7a-3f993215c051"
        },
        {
            "id": "ae5d5179-bff5-4f2d-8d9a-a09e91500c52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "fec9c3e4-4f0e-4fb1-8e7a-3f993215c051"
        },
        {
            "id": "73b20e39-0e24-403f-b7c9-1405e07deb74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fec9c3e4-4f0e-4fb1-8e7a-3f993215c051"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}