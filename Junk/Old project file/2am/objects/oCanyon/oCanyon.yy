{
    "id": "65bd0823-90ca-4e4d-85ca-b1ab557a9067",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCanyon",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "48c12c2f-c326-4494-b6f7-dae1ed760e9d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "0d36dd09-9896-42ee-b8fd-13d784827278",
    "visible": true
}