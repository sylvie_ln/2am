if !surface_exists(surfy) {
	surfy = surface_create(npow2(16*image_xscale),npow2(16*image_xscale));	
	surface_set_target(surfy);
	draw_clear_alpha(c_black,0);
	surface_reset_target();
}
	
if surface_exists(surfy) {
	surface_set_target(surfy);
	for(var i=0; i<image_xscale; i++) {
		draw_sprite(sprite_index,image_index,i*16,0);
	}
	draw_set_color(c_white);
	var h = 6;
	draw_line(0,0,0,h);
	draw_line(image_xscale*16-1,0,image_xscale*16-1,h);
	surface_reset_target();
	draw_surface_ext(surfy,x,y,1,1,0,c_white,image_alpha);
}