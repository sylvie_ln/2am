if !on {
	with other { instance_destroy(); }
	on = true;
	variable_global_set("flag_bridge_"+name,true);
	instance_create_depth(0,0,0,oFlash);
	play_sound(sndBallBridgeLadderUse);
	if name == "canyon" {
		with oNPC {
			if name == "Tressa"	{
				x -= 392;
				if global.flag_ladder_canyon {
					x -= 88;
					y -= (424-88);
				}
			}
		}
	}
}