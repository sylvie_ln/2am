{
    "id": "43618bbb-d08c-442b-9712-c42e99d31b0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBridge",
    "eventList": [
        {
            "id": "51e27fa2-54c3-496b-9ab7-1cdee8ddc722",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43618bbb-d08c-442b-9712-c42e99d31b0b"
        },
        {
            "id": "296c75f3-f4be-4c78-a2ad-4f6c6961fc0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "43618bbb-d08c-442b-9712-c42e99d31b0b"
        },
        {
            "id": "1713cd91-5541-457d-9fcf-4a333607b299",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "43618bbb-d08c-442b-9712-c42e99d31b0b"
        },
        {
            "id": "effe7ca4-865b-499a-a2ca-cee3572312f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43618bbb-d08c-442b-9712-c42e99d31b0b"
        },
        {
            "id": "8cd380cf-cbc0-407f-9884-4c62305148c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2bcd5d68-f156-4f2a-92ef-19cacd0d11a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "43618bbb-d08c-442b-9712-c42e99d31b0b"
        },
        {
            "id": "a3b4207f-bc3a-4674-9d5d-d9122c001b01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "43618bbb-d08c-442b-9712-c42e99d31b0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6420e9eb-23cb-4651-a18f-4778e63a4bb7",
    "visible": true
}