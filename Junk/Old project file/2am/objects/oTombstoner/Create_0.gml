event_inherited();
name = "EivlysTombstone"
cutey=false;
act1="Look"
act2="Touch"
sad = @"!if global.flag_finale==1
 Violently cold.
!else
 A strange warmth fills your body. It is a little painful and sad.
!fi";