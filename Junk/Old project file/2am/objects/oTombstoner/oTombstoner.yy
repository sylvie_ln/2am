{
    "id": "a31284fa-f5ed-4000-8225-c782ff982d86",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTombstoner",
    "eventList": [
        {
            "id": "6fed4f4a-9387-44d8-bdf0-b130c3a37686",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a31284fa-f5ed-4000-8225-c782ff982d86"
        },
        {
            "id": "7c7a023b-49c5-41f1-9b62-c78c181f9bbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "a31284fa-f5ed-4000-8225-c782ff982d86"
        },
        {
            "id": "4bd64f50-ea68-4768-a8ec-0788149a3755",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a31284fa-f5ed-4000-8225-c782ff982d86"
        },
        {
            "id": "7ba50775-e58b-4921-b01e-52de0eb42b36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "a31284fa-f5ed-4000-8225-c782ff982d86"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "085ce69f-b8e8-4404-b8c5-40d1d52c2fa0",
    "visible": true
}