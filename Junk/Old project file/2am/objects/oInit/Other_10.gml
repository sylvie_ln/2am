///@description Trade definitions
// trade_cost(noone,0) means the item cannot be traded
// trade_cost(oSomeItem,0) means oSomeItem is "exactly" what the trader wants
// cost 1 is "okay item"
// cost 2 is "good item"
// cost 3 is "great item"
// items not in the cost list are "unwanted items"

trade_define_item(oBasicShroom,"Basic Shroom","A lovely and simple shroom.",
trade_standard_costs(),2);
trade_define_item(oWorthlessShroom,"Worthless Shroom","A completely worthless, and bad shroom.",
trade_standard_costs(),5);
trade_define_item(oTinyShroom,"Tiny Shroom","A very cute little small shroom!",
trade_standard_costs(),5);
trade_define_item(oTallShroom,"Tall Shroom","A tall and lanky shroom.",
trade_standard_costs(),5);
trade_define_item(oWideShroom,"Wide Shroom","A quite wide and lengthy shroom.",
trade_standard_costs(),5);
trade_define_item(oSpringShroom,"Spring Shroom","21 SM. A springy shroom that'll let you zoom.",
trade_standard_costs(),21);
trade_define_item(oFloatShroom,"Float Shroom","19 SM. A floating shroom that flies like a broom.",
trade_standard_costs(),19);
trade_define_item(oRainbowShroom,"Rainbow Shroom","25 SM. A colourful shroom like flowers that bloom.",
trade_standard_costs(),25);
trade_define_item(oSparkleShroom,"Sparkle Shroom","24 SM. A golden shroom that's fit for a tomb.",
trade_standard_costs(),24);
trade_define_item(oPoisonShroom,"Poison Shroom","23 SM. A deadly shroom that spells your doom.",
trade_standard_costs(),23);
trade_define_item(oSpeedShroom,"Speed Shroom","22 SM. A speedy shroom that's a sonic boom.",
trade_standard_costs(),22);
trade_define_item(oKittyShroom,"Kittey Shroom","20 SM. A cutey shroom that's great for your room.",
trade_standard_costs(),20);

trade_define_item(oMushroomQuesoGrande,"Mushroom Queso Grande","The famous cheesey dish that will warm your insides with flavor.",
trade_cost(oPoisonShroom,2),4);

trade_define_item(oMinecartLicense,"Minecart License","It proves that the holder can drive a minecart with skill.",
trade_cost(oWorthlessShroom,3),3);


trade_define_item(oHandFake,"Fake Hand","A fake version of one of the legendary Twelve Hands of the Clock.",
trade_cost(noone,0),99);

trade_define_item(oHandOfFlames,"Hand of Flames","One of the legendary Twelve Hands of the Clock. I'll trade it for a delicious cheesey.",
trade_cost(oMushroomQuesoGrande,0),99);

trade_define_item(oHandOfEarth,"Hand of Earth","One of the legendary Twelve Hands of the Clock.",
trade_cost(noone,0),99);

trade_define_item(oHandOfTears,"Hand of Tears","One of the legendary Twelve Hands of the Clock.",
trade_cost(oKittyShroom,0),99);

trade_define_item(oHandOfSky,"Hand of Sky","One of the legendary Twelve Hands of the Clock. I'll trade it for a Rock that is Unique.",
trade_cost(oUniqueRock,0,oBoringRock,1),99);

trade_define_item(oHandOfStorms,"Hand of Storms","One of the legendary Twelve Hands of the Clock. I control the Weather with its power, hahaha.",
trade_cost(oBoringRock,0,oGiftShopCavernRock,0),99);

trade_define_item(oHandOfDreams,"Hand of Dreams","One of the legendary Twelve Hands of the Clock. It is so rainbow and cute, I love it.",
trade_cost(oRainbowShroom,0),99);

trade_define_item(oHandOfLies,"Hand of Lies","One of the legendary Twelve Hands of the Clock. Solve the riddle of Eivlys to grasp it.",
trade_cost(oTinyShroom,1),7);

trade_define_item(oHandOfDarkness,"Hand of Darkness","One of the legendary Twelve Hands of the Clock.",
trade_cost(noone,0),99);

trade_define_item(oHandOfLeafs,"Hand of Leafs","One of the legendary Twelve Hands of the Clock.",
trade_cost(noone,0),99);

trade_define_item(oHandOfLight,"Hand of Light","One of the legendary Twelve Hands of the Clock.",
trade_cost(noone,0),99);

trade_define_item(oHandOfMoon,"Hand of Moon","One of the legendary Twelve Hands of the Clock.",
trade_cost(noone,0),99);

trade_define_item(oHandOfThunder,"Hand of Thunder","One of the legendary Twelve Hands of the Clock.",
trade_cost(noone,0),99);

trade_define_item(oWideBagUpgrade,"Wide Power","This Power will increase Bag's width. Requires a wide thing.",
trade_cost(oWideShroom,0),99);

trade_define_item(oTallBagUpgrade,"Tall Power","This Power will increase Bag's height. Requires a tall thing.",
trade_cost(oTallShroom,0),99);

trade_define_item(oLargeBagUpgrade,"Final Power","This Power will maximize a Bag. Requires 5+ tall or wide things.",
trade_cost(oTallShroom,1,oWideShroom,1),5);

trade_define_item(oBridgeOrb,"Bridget's Ball","A ball that reveals bridges. I need 2 WideShrooms to summon such a ball.",
trade_cost(oWideShroom,2),4);

trade_define_item(oLadderOrb,"Ladderia's Ball","A ball that reveals ladders. I need 2 TallShrooms to summon such a ball.",
trade_cost(oTallShroom,2),4);

trade_define_item(oBoringRock,"Rock","Just a boring, old rock. Sucks!",
trade_cost(oBasicShroom,1),2);
trade_define_item(oUniqueRock,"Unique Rock","This rock is so unique that it looks like a Kitty. I'll trade it for a shroom that Sparkles.",
trade_cost(oSparkleShroom,0),99);

trade_define_item(oGiftShopShirt,"Funny Shirt","It's the classic \"I'm stupid\" shirt, great way to trick your friends.",
trade_standard_costs(),3);
trade_define_item(oGiftShopSnowGlobe,"Cavern Snowglobe","A mystical snowglobe of the Cavern, never forget forever with this one.",
trade_standard_costs(),3);
trade_define_item(oGiftShopKitteyPlush,"Kittey Plushey","A cutey plushey of the famous Kittey Statues. It is said Eivlys herself sewed this.",
trade_standard_costs(),3);
trade_define_item(oGiftShopPostcard,"Cavern Postcard","You will not forget your memory with this great postcard of the Cavern.",
trade_standard_costs(),3);
trade_define_item(oGiftShopCavernRock,"Cavern Rock","A rock straight from the walls of this Cavern, although, some prankster carved a face into it.",
trade_standard_costs(),3);
trade_define_item(oGiftShopCoffeeMug,"Cavern Mug","A coffee mug, always the most useful gift, and it even has the Cavern of Ancients logo on it.",
trade_standard_costs(),3);

trade_define_item(oPegasusBoots,"Pegasus Shoes","You will be forced to run fast when wearing them. Needs SpeedShroom x2.",
trade_cost(oSpeedShroom,1),2);
trade_define_item(o100TonShoes,"100Ton Boots","They are far too heavy for your pitiful body. Needs TallShroom x1.",
trade_cost(oTallShroom,1),1);
trade_define_item(oClimbingCleats,"Climbing Cleats","Well, they will let you climb better. Needs WideShroom x3.",
trade_cost(oWideShroom,1),3);
trade_define_item(oHoverHeels,"Hover Heels","Hover for a short time, however they are so slippery to walk in. Needs FloatShroom x2.",
trade_cost(oFloatShroom,1),2);
trade_define_item(oSpringSneakers,"Spring Sneakers","Bounce when you encounter the ground. Needs SpringShroom x2.",
trade_cost(oSpringShroom,1),2);

trade_define_item(oBlueShard,"Shard of Eivlys","A crystal shard of her body. Perhaps I would trade it for an artefact of this Cavern.",
trade_cost(oGiftShopShirt,1,oGiftShopSnowGlobe,1,oGiftShopKitteyPlush,1,oGiftShopPostcard,1,oGiftShopCavernRock,1,oGiftShopCoffeeMug,1),1);
trade_define_item(oGreenShard,"Shard of Eivlys","A crystal shard of her body. Perhaps I would trade it for a symbol of Eivlys's magic.",
trade_cost(oMagicCarrot,1,oBunneySlippers,1,oBunneyEars,1,oMiniBunney,1,oBunneyPin,1,oBunneyAmulet,1),1); 
trade_define_item(oRedShard,"Shard of Eivlys","A crystal shard of her body. Perhaps I would trade it.... for your legs??! Cackle cackle!!",
trade_cost(oPegasusBoots,1,o100TonShoes,1,oClimbingCleats,1,oHoverHeels,1,oSpringSneakers,1,oBunneySlippers,1),1);

trade_define_item(oGapKey,"Key of the Gap","The key that opens gaps in reality.",
trade_cost(oRedShard,1,oBlueShard,1,oGreenShard,1),3);

trade_define_item(oGuardianScimitar,"Scimitar of Sevily","Legendary sword of which only one exists. The rarest sword ever. Extremely valuable.",
trade_cost(oSylviePlushey,1,oDrummerPlushey,2,oJIGGLER,3),3);
trade_define_item(oGuardianShield,"Shield","A normal shield",
trade_cost(noone,0),99);

// Key shop
trade_define_item(oRegularKey,"Key","Simpley a normal key, great for unlocking regular doors. Costs 5 SM.",
trade_standard_costs(),5);
trade_define_item(oRoseKey,"Rose Key","An artisanal key handcrafted from a Rose, its probably extremely useful. Costs 16 SM.",
trade_standard_costs(),16);
trade_define_item(oCloudKey,"Cloud Key","A key that really seems pointless and not useful, however it's rare. Costs 12 SM.",
trade_standard_costs(),12);
trade_define_item(oKittenKey,"Kitten Key","A very cutey key. Not usable as a key due to its shape. Good souvenir. Costs 8 SM.",
trade_standard_costs(),8);

trade_define_item(oMagicCarrot,"Magic Carrot","Grants the sensations of eating a carrot. I need the power of a rainbow to summon it.",
trade_cost(oRainbowShroom,3),3);
trade_define_item(oBunneySlippers,"Bunney Slippers","Cutey slippers that quiet the sound of your stepping. I need two wide things to make it.",
trade_cost(oWideShroom,2),4);
trade_define_item(oBunneyEars,"Bunney Ears","The ears make you look just like a bunney. I need a tall thing to make it.",
trade_cost(oTallShroom,2),2);
trade_define_item(oMiniBunney,"MiniBunney","A cute friend of a bunney just for you. I need a tiny thing to make it.",
trade_cost(oTinyShroom,2),2);
trade_define_item(oBunneyPin,"Bunney Pin","It looks like an \"anime-girl\". Drastically increases luck. I need SparkleShroom to make it.",
trade_cost(oSparkleShroom,3),3);
trade_define_item(oBunneyAmulet,"Bunney Amulet","A power that lets you speak to bunneys. I would trade it for some premium bunney food.",
trade_cost(oGuardianScimitar,0),99);

trade_define_item(oRedPaperclip,"Red Paperclip","This item is really bad, I hate this.",
trade_cost(oWorthlessShroom,0),99);
trade_define_item(oFishCar,"Fish Car","An unbelievably fishy vehicle that's fun to drive/swim, but it can't hold papers well.",
trade_cost(oRedPaperclip,0,oPinkPaperclip,0),99);
trade_define_item(oWizardmobile,"Wizardmobile","This sports car has the extreme magic of a wizard, but it's useless underwater.",
trade_cost(oFishCar,0),99);
trade_define_item(oSnowKeety,"Snow Keety","This snow-mobile of a kitten is good on snowey terrain, but it's not very wizard.",
trade_cost(oWizardmobile,0),99);
trade_define_item(oSylvieTruck,"Big Truck","This sylvie truck is so big and powerful, but it's bad at going through snow.",
trade_cost(oSnowKeety,0),99);
trade_define_item(oHouse,"Cutey House","This is such a wonderful house everywhere, but it can't drive like a truck.",
trade_cost(oSylvieTruck,0),99);
trade_define_item(oHorse,"Lil' Horsey","This is a horse with lovely hair, but you can't live inside of it.",
trade_cost(oHouse,0),99);
trade_define_item(oPinkPaperclip,"Pink Paperclip","This paperclip has a cute flower motif, but I can't ride off into the sunset on it.",
trade_cost(oHorse,0),99);
trade_define_item(oChickenWings,"Chicken Wings","These are the wings that a chicken uses to fly, they are known as a legendary item.",
trade_cost(oPinkPaperclip,0),99);

trade_define_item(oPuzzleScroll,"Puzzle Hint","Sylvie Kitten.",
trade_cost(noone,0),99);
trade_define_item(oPuzzlePiece,"Puzzle Piece","One of the best puzzle pieces ever.",
trade_cost(noone,0),99);
trade_define_item(oLensOfLies,"Lens of Lies","Allows you to see through falsehoods. It's the reward for completing my puzzle.",
trade_cost(oPuzzlePiece,2),8);

trade_define_item(oSylviePlushey,"Girls Sweet Doll","A very cute Sylvieish doll. Low popularity. Costs 3 SM.",
trade_standard_costs(),3);
trade_define_item(oDrummerPlushey,"Lovely Drummer","A delightful Hubolesque plushey. Medium popularity. Costs 4 SM.",
trade_standard_costs(),4);
trade_define_item(oJIGGLER,"JIGGLER","An original Sylbolian character. Extreme popularity. Costs 6 SM.",
trade_standard_costs(),6);

// Labo
trade_define_item(oMemoryDiskManual,"Memory Disk Manual","Explains how the Memory Disk works. Costs 2 SM.",
trade_standard_costs(),2);
trade_define_item(oMemoryDisk,"Memory Disk","Please read the manual to learn about it. Needs Junk Parts 2x or SpringShroom 1x.",
trade_cost(oJunkParts,1,oSpringShroom,2),2);
trade_define_item(oMappingImplant,"Mapping Implant","Displays map of nearby rooms in Mushroom Zones. Needs Junk Parts 3x.",
trade_cost(oJunkParts,1),3);
trade_define_item(oUpgradeChip,"Upgrade Chip","Increases the power of the Mapping Implant. Needs Junk Parts 2x.",
trade_cost(oJunkParts,1),2);
trade_define_item(oJetpack,"Jetpack","Allows propulsive flight. Needs Junk Parts 5x or FloatShroom 3x.",
trade_cost(oJunkParts,2,oFloatShroom,3),9);

// Junk Shop
trade_define_item(oJunkParts,"Junky","Melancholic",
trade_cost(noone,0),99);

// Utility Shop
trade_define_item(oSnakeRope,"Snake Rope","A snake you can use in Mushroom Zones to climb so upwards. Costs 5 SM.",
trade_standard_costs(),5);
trade_define_item(oEmergencyBubble,"Emergency Bubble","A bubble to refill your air. One-time use in Mushroom Zones. Costs 5 SM.",
trade_standard_costs(),5);
trade_define_item(oBottledFlare,"Bottled Flarekitten","A curious kitten that sniffs out rare items and reveals secret passages. Costs 5 SM.",
trade_standard_costs(),5);
trade_define_item(oOxygenEgg,"Oxygen Egg","Oxygen-rich egg that reduces the rate of air depletion in Mushroom Zones. Costs 40 SM.",
trade_standard_costs(),40);

trade_define_item(oDollars,"Dollars","Oh this? It's just my foreign currency, much more expensive and high class than SM.",
trade_cost(noone,0),99);

trade_define_item(oLetter,"Lovely Letter","My best friend Sunny sent me this. It said to meet under the Legendary Tree....",
trade_cost(noone,0),99);
trade_define_item(oLocket,"Heart Locket","Karie gave me this. It's my most precious treasure.",
trade_cost(noone,0),99);

trade_define_item(oTrendyTote,"Trendy Tote","An extremely fashionesque tote. NOT FOR SALE, FOR USE INSIDE MUSEUM ONLY.",
trade_cost(noone,0),99);

trade_define_item(oSurpriseBox,"Surprise Box","A box of loot that's so surprising. It contains much SM currency.",
trade_cost(noone,0),99);

trade_define_item(oGenericItemTemplate,"Generic Item Template","Found this in the game's data, seems to be a template used to make items.",
trade_cost(noone,0),99);
trade_define_item(oVehicleTemplate,"Vehicle Template","Found this in the game's data, seems to be a template used to make vehicles.",
trade_cost(noone,0),99);
trade_define_item(oIllegalUpgradeChip,"Illegally Modded Chip","I hacked one of the Labo's chips to show more info. Needs the mapping implant to work.",
trade_cost(noone,0),99);
trade_define_item(oSpike,"Spike","Found this in the game's data, seems to be a hazardous spike from an early version.",
trade_cost(noone,0),99);
trade_define_item(oStar,"Vehicle Template","Found this in the game's data, seems to be a collectable star from an early version.",
trade_cost(noone,0),99);

