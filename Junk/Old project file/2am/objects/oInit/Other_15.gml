///@description Init data structures

// DATA STRUCTURES (mutable)
show_debug_message("Initializing data structures.");
global.initial_bag_grid_map = ds_map_create();
global.sticker_grid_cache_map = ds_map_create();
global.bag_grid_map = ds_map_create();
global.npc_bag_map = ds_map_create();
global.trade_map = ds_map_create();
global.item_map = ds_map_create();
global.explored = ds_map_create();
global.collected = ds_map_create();
global.memory_dead = ds_map_create();
global.rare_dead = ds_map_create();
global.cute_dead = ds_map_create();
global.shroom_deletion_map = ds_map_create();
global.shroom_insertion_map = ds_map_create();

global.memorys = ds_map_create();

global.lock_item = ds_map_create();
global.lock_name = ds_map_create();
global.lock_flag = ds_map_create();

global.bag_contents = ds_list_create();
global.bag_contents_backup = -1; // only set inside the museum

global.memory_particles = -1; // initialized by oMemory

var skip_zero = false;
if !(variable_global_exists("memory_disks") and ds_list_size(global.memory_disks) > 0) {
	global.memory_disks = ds_list_create();
} else {
	skip_zero = true;	
}
for(var i=0; i<sprite_get_number(sFloppy); i++) {
	if i == 0 and skip_zero { continue; }
	var mem_map = ds_map_create();
	for(var j=0; j<array_length_1d(global.area_names); j++) {
		var wwidth=100;
		var wheight=100;
		var oo = oMapp.offsets[? global.area_names[j]];
		ds_map_add_list(mem_map,global.area_names[j],list_create_nosylvie(
		(wwidth div 2)+oo[0],
		(wheight div 2)+oo[1]
		));
	}
	global.memory_disks[|i] = mem_map;
	ds_list_mark_as_map(global.memory_disks,i);
}