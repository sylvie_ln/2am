///@description Reset options

global.scale = global.max_scale;
global.fullscreen = false;
global.volume = 1;
global.mute = false;
global.mute_music = false;
global.mute_sfx = false;
global.assist_jump_power = 1;
global.assist_speed_power = 1;
global.assist_air_drain = 1;
global.assist_gravity = 1;
global.assist_gamespeed = 1;
