global.sylvie_font = font_add_sprite(sSylvieFont,ord(" "),true,1);
global.sylvie_font_bold = font_add_sprite(sSylvieFontB,ord(" "),true,1);
global.sylvie_font_bold_2x = font_add_sprite(sSylvieFontB2x,ord(" "),true,2);
global.neco_font = font_add_sprite(sLittleNecoVariable,ord(" "),true,1);

global.default_room_speed = 30;

global.game_save_map = ds_map_create();
global.options_save_map = ds_map_create();
global.keys_save_map = ds_map_create();
global.game_save_file = "save.fluffy"
global.options_save_file = "options.fluffy"
global.keys_save_file = "keys.fluffy"

init_outline();

global.view_width = room_width;
global.view_height = room_height;

global.max_scale = compute_max_scale();

sylvie_ds_tracker_init();

event_user(2);
event_user(3);

load_options();

instance_create_depth(0,0,0,oMusic);
audio_set_master_gain(0,global.volume)

instance_create_depth(0,0,0,oInput);
load_keyconf();

instance_create_depth(0,0,0,oCursor);
instance_create_depth(0,0,0,oDiceGoddess);

global.rwidth = room_width;
global.rheight = (room_height-20);

global.bag_grid_cell_size = 2;

global.area_names = [
"Creative Laughter",
"Cackling Bust",
"Cornered Wilderness",
"Cuter Willows",
"Careless Desire",
"Crumbled Hearth",
"Caustic Puddle",
"Captive Automaton",
"Cordless Funeral",
"Corpse Facade",
"Cloudy Mixture",
"Cataclysmic Fountain",
];

globalvar base_colors, base_colors_dark, pattern_colors, total_colors;
var jump = 10;
total_colors = 240 div jump;
for(i=0;i<total_colors; i++) {
    base_colors[i] = make_color_hsl_240(i*jump,240,180);
    base_colors_dark[i] = make_color_hsl_240(i*jump,240,100);
    var val = 180;
    switch(i) {
        case 0: val = 140; break;
        case 1: val = 130; break;
        case 2: val = 120; break;
        case 3: val = 110; break;
        case 4: val = 100; break;
        case 5: val = 100; break;
        case 6: val = 100; break;
        case 7: val = 100; break;
        case 8: val = 100; break;
        case 9: val = 100; break;
        case 10: val = 100; break;
        case 11: val = 100; break;
        case 12: val = 100; break;
        case 13: val = 120; break;
        case 14: val = 150; break;
        case 15: val = 170; break;
        case 16: val = 180; break;
        case 17: val = 180; break;
        case 18: val = 170; break;
        case 19: val = 170; break;
        case 20: val = 160; break;
        case 21: val = 160; break;
        case 22: val = 150; break;
        case 23: val = 150; break;
    }
    pattern_colors[i] = make_color_hsl_240(i*jump,240,val);
}

global.sylvie_seed = 0xc001ca75;

global.nl = @"
";

// Contract information
global.contract_signed = false;
global.contract_name = "";
global.contract_soul_bond = false;

// Clear flags
global.game_clear = false;
global.eivlys_clear = false;

instance_create_depth(0,0,0,oMapp);

event_user(4); // init world variables

event_user(5); // init data structures

event_user(6); // memorie's

event_user(7) // lock the doors

with oMapp {
	if load_mush_zones {
		event_user(13);	
	}
}

event_user(0);

load_game();

if classic_load_bar {
	application_surface_draw_enable(false);
	factor = 0;
	alarm[1] = 1;
} else {
	event_user(1);
}
