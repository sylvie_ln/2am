///@desc Done loading
global.scale = min(global.scale,global.max_scale);
window_set_scale(global.scale);
surface_resize(application_surface,global.view_width,global.view_height);
instance_create_depth(0,0,0,oAppSurfaceDraw);
alarm[0] = 1;