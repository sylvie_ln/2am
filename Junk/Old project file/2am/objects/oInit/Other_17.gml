///@description Lock doors
show_debug_message("Locking doors.");
lock_door("CiteyMushZone3",oGapKey,"key that slices through dimensions");
lock_door("CiteyMushZone2",oCloudKey,"key as fluffy as a cloud");
lock_door("ClockMushZone",oRegularKey,"key");
lock_door("CherryMushZone2A",oRegularKey,"key");
lock_door("CherryMushZone2B",oRegularKey,"key");
lock_door("CanyonMushZone2A",oRegularKey,"key");
lock_door("CanyonMushZone2B",oRegularKey,"key");
lock_door("CavernMushZone2A",oRegularKey,"key");
lock_door("CavernMushZone2B",oRegularKey,"key");
