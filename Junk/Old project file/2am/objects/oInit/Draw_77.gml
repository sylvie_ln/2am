if alarm[0] > 0 { exit; }
var w = 288;
if oMapp.step != 7 {
	if oMapp.step > 4 {
		factor = w/12*(clamp(min(10,oMapp.step),0,12))+((w/12)*oMapp.progress)+oMapp.fluffy;
	} else {
		factor = w/12*(clamp(min(10,oMapp.step),0,12))+((w/12)*oMapp.progress);
	}
}
draw_sprite(sLoading,0,sprite_get_xoffset(sLoading),sprite_get_yoffset(sLoading));
draw_sprite_ext(sLoadBar,0,sprite_get_xoffset(sLoading),sprite_get_yoffset(sLoading),
factor,
1,0,c_white,1);
if oMapp.step >= 12 {
	global.memory_disks_init = true;
	event_user(1);
}