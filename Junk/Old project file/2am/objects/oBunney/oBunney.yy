{
    "id": "3d48e6b9-06fe-4d4b-936b-d0e34d824a7a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBunney",
    "eventList": [
        {
            "id": "27542ad7-fce0-4c4c-aff6-6f73b9849484",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3d48e6b9-06fe-4d4b-936b-d0e34d824a7a"
        },
        {
            "id": "c7a02a03-6aa7-4d47-950d-a1b6a7f456f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d48e6b9-06fe-4d4b-936b-d0e34d824a7a"
        },
        {
            "id": "751f70a4-8570-43a9-b621-de817896262b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d48e6b9-06fe-4d4b-936b-d0e34d824a7a"
        },
        {
            "id": "2c8e670b-a348-4e0c-8984-959963d6913c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "3d48e6b9-06fe-4d4b-936b-d0e34d824a7a"
        },
        {
            "id": "7a6f6e05-c0fd-4a6a-a2b0-d5891588f01e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "3d48e6b9-06fe-4d4b-936b-d0e34d824a7a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75d5e985-a4a8-4475-a0f1-303207124ffa",
    "visible": true
}