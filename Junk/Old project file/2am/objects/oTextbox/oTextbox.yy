{
    "id": "9be658b2-ff93-4b59-b9cb-6db50737d6ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTextbox",
    "eventList": [
        {
            "id": "02f765ef-56fc-4b63-bbe1-3db5b60d20d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9be658b2-ff93-4b59-b9cb-6db50737d6ba"
        },
        {
            "id": "a01fbcb6-14ce-4048-9e91-bbe97747f958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9be658b2-ff93-4b59-b9cb-6db50737d6ba"
        },
        {
            "id": "0bab797d-d083-47b3-a43f-43e2b94d9527",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "9be658b2-ff93-4b59-b9cb-6db50737d6ba"
        },
        {
            "id": "c05aa503-53b2-47ac-9974-962223b2d57c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9be658b2-ff93-4b59-b9cb-6db50737d6ba"
        },
        {
            "id": "ee66886d-59b9-4c90-997b-101bff3f6355",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9be658b2-ff93-4b59-b9cb-6db50737d6ba"
        },
        {
            "id": "66da1549-0da4-4e98-a28f-64eef1687296",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "9be658b2-ff93-4b59-b9cb-6db50737d6ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}