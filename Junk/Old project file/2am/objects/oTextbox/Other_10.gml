/// @description Finish printing text
can_play_type_sound = false;

do {
    event_perform(ev_step,ev_step_normal);
} until finished;

can_play_type_sound = true;

play_sound(sndDialogForce);