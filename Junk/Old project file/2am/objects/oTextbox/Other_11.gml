/// @description Initialize
// This should be called when the textbox is created, after the instance_create call

// Handle all the text commands
for(var i=1; i<=string_length(text); i++) {
    if string_char_at(text,i) == "\\" {
       var result = execute_text_command(text,i);
	   //disabled_show_debug_message(string_copy(text,i,6)+"...");
       text = process_command(text,i,process_command_actions.remove);
       text = string_insert(result,text,i);
	   //disabled_show_debug_message(string_copy(text,i,6)+"...");
	   //disabled_show_debug_message(string(i)+" "+text);
       if result != "\\" {
            i--;
       }
    }
}

// Now do linebreaking and calculate width and height of the text
width = 0;
height = 0;
text = string_trim(text);
draw_set_font(global.neco_font);
var adjusted_max_width = max_width - border*3; // we want text to be comfy within the borders
var line = "";
var chunk_start = 1;
var chunk_length = 0;
var chunk_type = 1; // 0 = whitespace, 1 = word
// we read the text in chunks, alternating between whitespace and non-whitespace
for(var i=1; i<=string_length(text); i++) {
    var c = string_char_at(text,i);
    if (ord(c) <= ord(" ") and chunk_type == 1) 
    or (ord(c) >  ord(" ") and chunk_type == 0) 
    or i == string_length(text) { // end of chunk reached
        var chunk = string_copy(text,chunk_start,chunk_length);
        if string_width(line+chunk) > adjusted_max_width { // adding chunk would exceed the max
            // flag this position for linebreaking
            ds_queue_enqueue(linebreaks,chunk_start);
            // update width and height
            width = max(width,string_width(line));
            height += string_height(line);
            // if we just read a whitespace chunk
            // don't add extra whitespace to the start of the next line
            // but for non-whitespace chunks
            // put the chunk at the start of the next line
            line = "";
            if chunk_type == 1 {
                line = chunk;
            }
        } else { // we wouldn't exceed the max, so add the chunk to the line
            line += chunk;
        }
        // reset chunk
        chunk_start = i;
        chunk_length = 0;
        chunk_type = 1-chunk_type;
    } 
    chunk_length++;
}
// if we reach the end of the string and there's an unfinished line
// update the width accordingly
if line != "" {
    width = max(width,string_width(line));
    height += string_height(line);
}