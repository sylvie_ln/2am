event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "A lovely and cute pink paperclip, with a lovely flower on it. I bet it could be Traded for something wonderful"

damp = 0.75;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!