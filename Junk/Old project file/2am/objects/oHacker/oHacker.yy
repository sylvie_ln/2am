{
    "id": "18488e88-7894-43a8-a14b-a708dcf72082",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHacker",
    "eventList": [
        {
            "id": "0a26233b-7c52-49d9-bbf2-1f7c300a911d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "18488e88-7894-43a8-a14b-a708dcf72082"
        },
        {
            "id": "4a7e818d-5dff-4b71-98b1-ba2ebf458401",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "18488e88-7894-43a8-a14b-a708dcf72082"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f688b8-dd4d-4d42-96ce-776174c8176b",
    "visible": true
}