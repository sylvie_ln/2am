{
    "id": "899729d1-0c14-4c06-ac0e-9314f961ae38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRainbowShroomCutescene",
    "eventList": [
        {
            "id": "2bb464ef-d964-45e7-995a-52bb02df204e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "899729d1-0c14-4c06-ac0e-9314f961ae38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c8ff8d82-948f-4875-b52e-6b0807ea0545",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d620b6c-8cb0-4c10-8f85-3337483872c6",
    "visible": true
}