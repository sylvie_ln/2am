{
    "id": "284a0f2d-8353-4869-b770-b497d948381d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oClockSpike",
    "eventList": [
        {
            "id": "4c978a58-c53e-45eb-95a0-d684cd1f8f3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "284a0f2d-8353-4869-b770-b497d948381d"
        },
        {
            "id": "39293938-6489-40e8-8a2e-532037cbc9f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "284a0f2d-8353-4869-b770-b497d948381d"
        },
        {
            "id": "a1709b45-4492-433b-843d-3579b83a0efa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "284a0f2d-8353-4869-b770-b497d948381d"
        },
        {
            "id": "b98f37c7-7f19-48f5-8d90-49d434b51eca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "284a0f2d-8353-4869-b770-b497d948381d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ffba4ac-beef-4771-8b67-400423e44bd4",
    "visible": true
}