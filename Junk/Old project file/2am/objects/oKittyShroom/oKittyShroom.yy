{
    "id": "cd6ef03f-8dc2-4efd-8449-77ac375065d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKittyShroom",
    "eventList": [
        {
            "id": "87c9b6f4-fdb5-482f-94f0-4f183791eb2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd6ef03f-8dc2-4efd-8449-77ac375065d1"
        },
        {
            "id": "ba0c5dfa-ffd4-4d77-a3a6-d6bf03d77df3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd6ef03f-8dc2-4efd-8449-77ac375065d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba9993bb-be59-42e7-9531-f59b8f70f346",
    "visible": true
}