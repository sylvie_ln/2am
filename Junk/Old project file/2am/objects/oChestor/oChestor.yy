{
    "id": "fbfe8635-c0f2-4dda-8da0-c17f87dff904",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChestor",
    "eventList": [
        {
            "id": "4da8653e-5a4e-45e3-a205-cd2e5912919a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "fbfe8635-c0f2-4dda-8da0-c17f87dff904"
        },
        {
            "id": "fb861ef8-1509-4bea-9b75-2427b12fc8e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fbfe8635-c0f2-4dda-8da0-c17f87dff904"
        },
        {
            "id": "118d8846-bed7-4ef0-bb5d-a250d8dda16e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fbfe8635-c0f2-4dda-8da0-c17f87dff904"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "517d862b-c010-4cea-b1b0-0a1dbf612bea",
    "visible": true
}