if !active { exit; }

if input_pressed("Escape")
or (selected == graphic_options.done and input_pressed("Jump")) {
	active = false;
	play_menu_backward();
	with parent { event_user(0); }
	exit;
}

if input_pressed_repeat("Up") {
	selected--;
	if selected < 0 {
		selected = total-1;	
	}
	play_menu_cursor_move();
}
if input_pressed_repeat("Down") {
	selected++;
	if selected >= total {
		selected = 0;
	}
	play_menu_cursor_move();
}
var change = false;
if input_pressed_repeat("Left") {
	option_choice[selected]--;
	if option_choice[selected] < 0 {
		if array_length_1d(choices[selected]) == 2 {
			option_choice[selected] = 1;	
			change = true;	
			play_menu_adjust();
		} else {
			option_choice[selected] = 0;
			play_menu_error();
		}
	} else {
		change = true;	
		play_menu_adjust();
	}
}
if input_pressed_repeat("Right") {
	option_choice[selected]++;
	if option_choice[selected] >= array_length_1d(choices[selected]) {
		if array_length_1d(choices[selected]) == 2 {
			option_choice[selected] = 0;	
			change = true;
			play_menu_adjust();
		} else {
			option_choice[selected] = array_length_1d(choices[selected])-1;	
			play_menu_error();
		}
	} else {
		change = true;
		play_menu_adjust();
	}
}

if change {
	var ca = choices[selected];
	if option_var[selected] != "" {
		variable_global_set(option_var[selected],ca[option_choice[selected]]);
	}
	switch(selected) {
		case graphic_options.fullscreen:
			if window_get_fullscreen() != global.fullscreen {
				window_set_fullscreen(global.fullscreen);	
			}
		case graphic_options.scale:
			window_set_scale(global.scale);
			alarm[1] = 1;
		break;
	}
}