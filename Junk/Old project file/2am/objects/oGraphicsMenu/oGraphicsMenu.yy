{
    "id": "c386ad22-32f0-410c-bcc5-9f9d2598da8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGraphicsMenu",
    "eventList": [
        {
            "id": "9894a30a-5299-4e3a-a010-04b2d353834a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c386ad22-32f0-410c-bcc5-9f9d2598da8e"
        },
        {
            "id": "229f7aef-fcba-418e-943d-2f56dc1942c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c386ad22-32f0-410c-bcc5-9f9d2598da8e"
        },
        {
            "id": "756fbf8d-0b5b-40fd-bed8-164643be84ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c386ad22-32f0-410c-bcc5-9f9d2598da8e"
        },
        {
            "id": "c9e974dd-ce7b-4c4a-8e95-00ff88b68b29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c386ad22-32f0-410c-bcc5-9f9d2598da8e"
        },
        {
            "id": "977ef03c-b6a9-4313-9e5c-fd01fe291ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c386ad22-32f0-410c-bcc5-9f9d2598da8e"
        },
        {
            "id": "c3f1fb04-e113-48bf-89bc-17e384de0b34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c386ad22-32f0-410c-bcc5-9f9d2598da8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}