active = false;
parent = noone;

var i = 0;
option[i++] = "Scale"
choices[i-1] = array_create(global.max_scale);
var scale_choices = choices[i-1];
var idx = 0;
for(var sc=1; sc<=global.max_scale; sc++) {
	scale_choices[@idx] = sc;
	idx++;
} 
option_choice[i-1] = find_choice(global.scale,choices[i-1]);
option_var[i-1] = "scale"
option[i++] = "Fullscreen"
choices[i-1] = [1,0];
option_choice[i-1] = find_choice(global.fullscreen,choices[i-1]);
option_var[i-1] = "fullscreen"
/*
option[i++] = "Soft Colors" 
choices[i-1] = [1,0];
option_choice[i-1] = find_choice(global.mute_sfx,choices[i-1]);
option_var[i-1] = "mute_sfx"
*/
option[i++] = "Done"
choices[i-1] = -1;
option_choice[i-1] = -1;
option_var[i-1] = ""
total = i;
selected = 0;

enum graphic_options {
	scale,
	fullscreen,
	done
}