{
    "id": "8d7dc4f1-7d21-48dd-9fca-bd42ab4195b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGiftShopPostcard",
    "eventList": [
        {
            "id": "c1266066-9221-482a-a511-26a180a29bb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d7dc4f1-7d21-48dd-9fca-bd42ab4195b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d345f38-c955-4189-a7ec-3fb71effbc31",
    "visible": true
}