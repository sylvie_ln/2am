var bag_yoff = sprite_get_yoffset(bag_spr)-(sprite_get_height(bag_spr)/2)
if mode == 2 {
	bag_yoff-=8;
	with instance_create_depth(xp+96,yp+bag_yoff,depth-1,oBag) {
		sprite_index = other.bag_spr;	
		if room == rmCoolMuseum { sprite_index = sMuseumBag; }
	}
	with instance_create_depth(xp+96,yp+bag_yoff,depth-1,oBagInterior) {
		sprite_index = other.bag_int_spr;	
		if room == rmCoolMuseum { sprite_index = sMuseumBagInterior; }
	}
	with instance_create_depth(xp-96,yp,depth-1,oBag) {
		type = 1;
		sprite_index = sNPCBag;
	}
	with instance_create_depth(xp-96,yp,depth-1,oBagInterior) {
		type = 1;
		sprite_index = sNPCBagInterior;
	}
	npc.text = npc.orig_text;
} else {
	bag_yoff += 8;
	with instance_create_depth(xp,yp+bag_yoff,depth-1,oBag) {
		sprite_index = other.bag_spr;	
		if room == rmCoolMuseum { sprite_index = sMuseumBag; }
	}
	with instance_create_depth(xp,yp+bag_yoff,depth-1,oBagInterior) {
		sprite_index = other.bag_int_spr;	
		if room == rmCoolMuseum { sprite_index = sMuseumBagInterior; }
	}
}

// load npc bag
if mode == 2 {
	var npc_bag;
	with oBag {
		if type == 1 { npc_bag = self; }	
	}
	var populate = ds_list_sylvie_create();
	if !ds_map_exists(global.npc_bag_map,npc.name) {
		var bag_sticker_list = ds_list_sylvie_create();
		bag_grid_init(npc.name);
		if ds_map_exists(global.trade_map,npc.name) {
			var items = global.trade_map[? npc.name];
			for(var i=0; i<ds_list_size(items); i++) {
				// Each item is:
				// Object index
				// Name
				// Description
				// Cost
				var item = ds_list_sylvie_create();
				trade_copy_item(item,items[|i]);
				if npc.name == "Laborie" and asset_get_index(item[|item_data.obj]) == oMemoryDisk { 
					global.disk_num++;
				}
				ds_list_add(populate,item);
			}
		}
	} else if ds_map_exists(global.npc_bag_map,npc.name) {
		var bag_sticker_list = global.npc_bag_map[? npc.name];
		var stuff_on_head = false;
		//disabled_show_debug_message(ds_list_size(bag_sticker_list));
		var dont_restock = ds_map_create();
		for(var i=0; i<ds_list_size(bag_sticker_list); i++) {
			var st_info = bag_sticker_list[|i];
			var st_x = st_info[|npc_bag_sticker_info.xp];
			var st_y = st_info[|npc_bag_sticker_info.yp];
			var st_type = asset_get_index(st_info[|npc_bag_sticker_info.type]);
			var st_ii = st_info[|npc_bag_sticker_info.ii];
			var st_item = st_info[|npc_bag_sticker_info.item];
			dont_restock[?st_item[|item_data.restock]] = true;
			var st = instance_create_depth(cl+st_x,ct+st_y,depth-2,oSticker);
			st.sprite_index = asset_get_index(sprite_get_name(object_get_sprite(st_type))+"Sticker");
			st.image_index = st_ii;
			st.shroom_type = st_type;
			var stuff_on_head = false;
			if abs((cl+st_x)-hex) < 1 {
				var stuff_on_head = true;
			}
			st.in_bag = -1;
			st.owner = -1;
			st.item = st_item;
			if stuff_on_head {
				st.depth -= 1;
				hey -= abs(st.bbox_bottom-st.bbox_top)
			}
		}
		if npc.must_restock {
			if ds_map_exists(global.trade_map,npc.name) {
				var items = global.trade_map[? npc.name];
				for(var i=0; i<ds_list_size(items); i++) {
					// Each item is:
					// Object index
					// Name
					// Description
					// Cost
					var itcheck = items[|i];
					if itcheck[|item_data.restock] == 0 { continue; }
					if ds_map_exists(dont_restock,itcheck[|item_data.restock]) { continue; }
					if npc.name == "Laborie" and asset_get_index(itcheck[|item_data.obj]) == oMemoryDisk {
						global.disk_num++;
						if global.disk_num >= sprite_get_number(sFloppy) {
							continue;	
						}
					}
					var item = ds_list_sylvie_create();
					trade_copy_item(item,items[|i]);
					ds_list_add(populate,item);
				}
			}
		}
		ds_map_destroy(dont_restock);
	}
	var head_x = hex;
	var head_y = hey; 
	for(var i=0; i<ds_list_size(populate); i++) {
		var item = populate[|i];
		var item_obj = asset_get_index(item[|item_data.obj]);
		var st = instance_create_depth(npc_bag.x,npc_bag.y,depth-2,oSticker);
		st.sprite_index = asset_get_index(sprite_get_name(object_get_sprite(item_obj))+"Sticker");
		st.image_index = sylvie_irandom(sprite_get_number(st.sprite_index)-1);
		if st.sprite_index == sHandsSticker {
			switch(item_obj) {
				case oHandOfTears: st.image_index = hand.tears; break;
				case oHandOfDarkness: st.image_index = hand.darkness; break;
				case oHandOfLight: st.image_index = hand.light; break;
				case oHandOfFlames: st.image_index = hand.flames; break;
				case oHandOfLeafs: st.image_index = hand.leafs; break;
				case oHandOfThunder: st.image_index = hand.thunder; break;
				case oHandOfLies: st.image_index = hand.lies; break;
				case oHandOfStorms: st.image_index = hand.storms; break;
				case oHandOfSky: st.image_index = hand.sky; break;
				case oHandOfEarth: st.image_index = hand.earth; break;
				case oHandOfMoon: st.image_index = hand.moon; break;	
				case oHandOfDreams: st.image_index = hand.dreams; break;
			}
		}
		if item_obj == oSurpriseBox {
			switch(item[|item_data.threshold]) {
				case 1: st.image_index = 0; break;
				case 2: st.image_index = 1; break;
				case 3: st.image_index = 2; break;
				case 4: st.image_index = 3; break;
				case 5: st.image_index = 4; break;
				case 6: st.image_index = 5; break;
				case 7: st.image_index = 6; break;
			}
		}
		if item_obj == oHint {
			var offset = 0;
			if room == rmCherryBlossomHills { offset = 5; }
			if room == rmCanyonCliffs { offset = 10; }
			if room == rmCavernOfAncients { offset = 15; }
			st.image_index = i+offset;	
		}
		if item_obj == oOxygenEgg {
			st.image_index = i mod sprite_get_number(sOxygenEgg);	
		}
		if npc.name == "Katey" {
			switch(item[|item_data.name]) {
				case "Persian": st.image_index = 5;
				break;
				case "Russian Blue": st.image_index = 4;
				break;
				case "Savannah": st.image_index = 1;
				break;
				case "Scottish Fold": st.image_index = 3;
				break;
				case "Sphynx": st.image_index = 2;
				break;
			}
		}
		if npc.name == "Junky" {
			switch(item[|item_data.name]) {
				case "Junk Part: Screw": st.image_index = 0;
				break;
				case "Junk Part: Gear": st.image_index = 1;
				break;
				case "Junk Part: Spring": st.image_index = 2;
				break;
				case "Junk Part: Nail": st.image_index = 3;
				break;
				case "Junk Part: Nut": st.image_index = 4;
				break;
				case "Junk Part: Gemstone": st.image_index = 5;
				break;
			}
		}
		if item_obj == oMemoryDisk {
			if npc.name == "Junky" {
				st.image_index = 0;
			} else {
				st.image_index = global.disk_num;
			}
		}
		st.shroom_type = item_obj;
		st.in_bag = -1;
		st.owner = -1;
		with st { event_user(1); }
		var pos = bag_grid_find_free(npc.name,st);
		if pos[0] == -1 {
			st.x = head_x;
			st.y = head_y - abs(st.y - st.bbox_bottom);
			head_y -= abs(st.bbox_bottom-st.bbox_top);
			hey = head_y;
		} else {
			bag_grid_insert(npc.name,pos[0],pos[1],st);
			var npc_bag_left = npc_bag.x - npc_bag.sprite_xoffset;
			var npc_bag_top = npc_bag.y - npc_bag.sprite_yoffset;
			st.x = npc_bag_left + pos[0]*global.bag_grid_cell_size + st.sprite_xoffset;	
			st.y = npc_bag_top + pos[1]*global.bag_grid_cell_size + st.sprite_yoffset;
		}
		st.item = item;
		// Now we copy the sticker position over to the npc bag map
		enum npc_bag_sticker_info {
			xp,
			yp,
			type,
			ii,
			item,
			last
		}
		var st_info = list_create(st.x-cl,st.y-ct,object_get_name(st.shroom_type),st.image_index,item);
		ds_list_mark_as_list(st_info,npc_bag_sticker_info.item);
		ds_list_add(bag_sticker_list,st_info);
		ds_list_mark_as_list(bag_sticker_list,ds_list_size(bag_sticker_list)-1);
		ds_map_add_list(global.npc_bag_map,npc.name,bag_sticker_list);
	}
	ds_list_sylvie_destroy(populate);
	npc.must_restock = false;
}
for(var i=0; i<ds_list_size(global.bag_contents); i++) {
	var st_info = global.bag_contents[|i];
	var st_x = st_info[|bag_contents_info.xp];
	var st_y = st_info[|bag_contents_info.yp];
	var st_type = asset_get_index(st_info[|bag_contents_info.type]);
	var st_ii = st_info[|bag_contents_info.ii];
	var st = instance_create_depth(oBag.x+st_x,oBag.y+st_y,depth-2,oSticker);
	st.sprite_index = asset_get_index(sprite_get_name(object_get_sprite(st_type))+"Sticker");
	st.image_index = st_ii;
	st.shroom_type = st_type;
	st.in_bag = 1;
	st.owner = 1;
}



if mode == 2 and instance_exists(npc) {
	if first_item != noone {
		first_item = asset_get_index(npc.first_item[|item_data.obj]);
	}
	heist = (room == rmCoolMuseum and npc.name == "Alicia");
}