with oSticker {
	xprev = x;
	yprev = y;
}

var bbw = sprite_get_bbox_right(sQuit)-sprite_get_bbox_left(sQuit);
var bbh = sprite_get_bbox_bottom(sQuit)-sprite_get_bbox_top(sQuit);

var click_press = mouse_check_button_pressed(mb_left) or input_pressed("Jump");

if mode != 2 {
	hover_quit = point_in_rectangle(mouse_x,mouse_y,cr-bbw,cb-bbh,cr,cb);
	draw_sprite(sQuit,hover_quit,cr,cb);
	if hover_quit and click_press {
		instance_destroy();	
		exit;
	}
}

hover_done = false;
if mode == 1 {
	hover_done = point_in_rectangle(mouse_x,mouse_y,cl,cb-bbh,cl+bbw,cb);
	draw_sprite(sDone,(holding? 2 : hover_done),cl,cb);
	if hover_done and !holding and click_press {
		done = true;
		instance_destroy();	
		exit;
	}
}

if mode == 2 {
	var bbw = sprite_get_bbox_right(sQuitSmall)-sprite_get_bbox_left(sQuitSmall);
	var bbh = sprite_get_bbox_bottom(sQuitSmall)-sprite_get_bbox_top(sQuitSmall);
	hover_quit = point_in_rectangle(mouse_x,mouse_y,cr-bbw,ct,cr,ct+bbh);
	draw_sprite(sQuitSmall,(trade_lock? 2 : hover_quit),cr,ct);
	if hover_quit and !trade_lock and click_press {
		instance_destroy();	
		exit;
	}
	/*
	hover_done = false;
	hover_done = point_in_rectangle(mouse_x,mouse_y,cl,ct,cl+bbw,ct+bbh);
	draw_sprite(sDoneSmall,(holding? 2 : hover_done),cl,ct);
	if hover_done and !holding and mouse_check_button_pressed(mb_left) {
		done = true;
		instance_destroy();	
		exit;
	}
	*/
	/*
	var bbw = sprite_get_bbox_right(sTrade)-sprite_get_bbox_left(sTrade);
	var bbh = sprite_get_bbox_bottom(sTrade)-sprite_get_bbox_top(sTrade);
	var can_done = !trade_lock;
	var gray = can_done and trade_ok;
	hover_trade = false;
	var shift = round(bbw*(1.05));
	if can_done {
		var spr = sDone;	
		shift -= (sprite_get_width(sDone) div 2)-2;
	} else {
		var spr = sQuit;	
		shift += sprite_get_width(sDone) div 2;
	}
	hover_trade = point_in_rectangle(mouse_x,mouse_y,xp+shift-(bbw div 2),cb-bbh,xp+shift+(bbw div 2),cb);
	draw_sprite(spr,(gray? 2 : hover_trade),xp+shift,cb);
	if hover_trade and !can_done and mouse_check_button_pressed(mb_left) {
		if can_done {
			done = true;
		}
		instance_destroy();	
		exit;		
	}
	*/
}


var dmx = display_mouse_get_x();
var dmy = display_mouse_get_y();
var wx = window_get_x();
var wy = window_get_y();

if holding and cursor_in_window {
	x = mouse_x + holding_offset_x;
	y = mouse_y + holding_offset_y;
	sprite_index = sticker;
	image_index = sii;
	draw_outline_ext(c_white, 0.5);
	draw_self();
	var npc_bag_int = noone;
	with oBagInterior {
		if type == 1 { npc_bag_int = self; }
	}
	if place_meeting(x,y,npc_bag_int) or place_meeting(x,y,oBag) or place_meeting(x,y,oSticker) 
		or (mode != 2 and room == rmCylveyClockTower and !place_meeting(x,y,oBagInterior)) {
		var sz = (max(bbox_right-bbox_left,bbox_bottom-bbox_top) div 2)+4;
		draw_set_alpha(0.5);	
		var xox = 0;
		var yoy = 0;
		draw_line_width_color(x-sz+xox,y-sz+yoy,x+sz+xox,y+sz+yoy,4,c_red,c_maroon);	
		draw_line_width_color(x-sz+xox,y+sz+yoy,x+sz+xox,y-sz+yoy,4,c_red,c_maroon);	
		draw_set_alpha(1);
	}
	sprite_index = -1;
} 



if selected == noone and instance_exists(backup) and !trade_ok {
	if backup.in_bag != 1 {
		selected = backup;
		selected.visible = false;
		selected.in_bag = -1;
		backup = noone;
	}
}
if selected != noone and selected.owner == -1 {
	if trade_ok {
		if orig_pos[0] != -1 and move_timer < move_time {
			move_timer++;
			selected.x = round(lerp(orig_pos[0],target_pos[0],move_timer/move_time));
			selected.y = round(lerp(orig_pos[1],target_pos[1],move_timer/move_time));
		}
	} else {
		if offer_evaluated {
			if orig_pos[0] != -1 and move_timer > 0 {
				move_timer--;
				selected.x = round(lerp(orig_pos[0],target_pos[0],move_timer/move_time));
				selected.y = round(lerp(orig_pos[1],target_pos[1],move_timer/move_time));
			}
		}
	}
	selected.visible = false;
	x = selected.x;
	y = selected.y;
	sprite_index = selected.sprite_index;
	image_index = selected.image_index;
	draw_outline_ext(c_white,1);
	draw_self();
	sprite_index = -1;
	if selected.x == orig_pos[0] and selected.y == orig_pos[1] { 
		if selected_wait != noone {
			selected.visible = true;
			selected.in_bag = -1;
			selected = selected_wait;
			selected_wait = noone;
			backup = noone;
			sticker_x = selected.x;
			sticker_y = selected.y;
			sticker = selected.sprite_index;
			sii = selected.image_index;
			event_perform(ev_alarm,2);
		} else {
			trade_ok = false;
			selected.in_bag = -1;
		}
		//alarm[1] = 3;
	} else if selected.x == target_pos[0] and selected.y == target_pos[1] { 
		selected.visible = true;
		selected.in_bag = 0;
		backup = selected;
		selected = noone;	
	}
}

if instance_exists(target_item) {
	target_item.visible = false;
	x = target_item.x;
	y = target_item.y;
	sprite_index = target_item.sprite_index;
	image_index = target_item.image_index;
	draw_outline_ext(c_white,1);
	draw_self();
	sprite_index = -1;
}

if trade_debug {
	draw_set_color(c_white)
	var i = 1;
	var ss = 12;
	draw_text(hex-16,hey-(++i)*ss,trade_ok ? "Trade OK" : "Trade NOT OK");
	draw_text(hex-16,hey-(++i)*ss,trade_was_ok ? "Trade Was OK" : "Trade was NOT OK");
	draw_text(hex-16,hey-(++i)*ss,was_in_bag ? "Was in Bag" : "Was NOT in Bag");
	draw_text(hex-16,hey-(++i)*ss,offer_evaluated ? "Offer evaluated" : "Offer NOT evaluated");
	draw_text(hex-16,hey-(++i)*ss, "A2: "+string(alarm[2]));
	draw_text(hex-16,hey-(++i)*ss, "A1: "+string(alarm[1]));
	draw_text(hex-16,hey-(++i)*ss, "A0: "+string(alarm[0]));
}
if cursor_in_window {
	draw_sprite_ext(sCursor,0,mouse_x,mouse_y,1,1,0,c_white,0.75);
}

with oCursor {
	event_user(0);	
}

cursor_in_window =
	dmx > wx-global.scale and dmx <= wx+window_get_width()+global.scale
and dmy > wy-global.scale and dmy <= wy+window_get_height()+global.scale;