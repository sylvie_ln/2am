bag_spr = asset_get_index("s"+global.bag);
bag_int_spr = asset_get_index("s"+global.bag+"Interior");
npc = noone;
shroom = noone;
shroom_type = noone;
sticker = -1;
sii = 0;
done = false;
holding = true;
original = true;
holding_offset_x = 0;
holding_offset_y = 0;

selected = noone;
backup = noone;
backup_item = noone;
sticker_x = -1;
sticker_y = -1;

owner = 0;
wanted = false;

orig_pos = [-1,-1];
target_pos = [-1,-1];
cell_pos = [-1,-1];
move_timer = 0;
move_time = global.default_room_speed;


depth = -1000;

cx = camera_get_view_x(view_camera);
cy = camera_get_view_y(view_camera);
cw = camera_get_view_width(view_camera);
ch = camera_get_view_height(view_camera);
cl = cx;
ct = cy;
cr = cl+cw;
cb = ct+ch;
xp = cl+(cw div 2);
yp = ct+(ch div 2);

// 0 = put
// 1 = rearrange
// 2 = trade
mode = 0;

can_trade = false;
trade_ok = false;
trade_lock = false;
trade_was_ok = false;
trade_selected = noone;
offer_evaluated = false;
selected_wait = noone;

hover_quit = false;
hover_trade = false;
hover_done = false;

was_in_bag = false;
was_on_head = false;
head_item_height = 0;
item_restock = 0;


target_item = noone;
hex = cl+4+16;
hey = cb-32;

steal = 0;
stole = false;

heist = false;

what_you_got = noone;

play_sound(sndBagOpen);

press_holding = false;
press_wait = 0;

first_item = noone;

with oCursor { 
	active = false;
	wait_until_moved = false;
	alarm[0] = (room_speed div 5);
}
cursor_in_window = false;