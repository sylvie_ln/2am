draw_set_alpha(0.9);
draw_circle_color(xp,yp,point_distance(xp,yp,cr,cb)+1,merge_color(c_white,c_black,0.75),c_black,false);
draw_set_alpha(1);


if mode == 0 {
	draw_sprite(sPutItInTheBag,0,cl,ct);
} else if mode == 1 {
	draw_sprite(sGottaSortMyBag,0,cl,ct);
} else if mode == 2 {
	draw_sprite(sTradingTime,0,cl,ct);
}
//draw_text(cl,ct + (ch div 2),"original: "+string(original));
//draw_text(cl,ct + (ch div 2)+16,"shroom type: "+object_get_name(shroom_type));
if global.first_bag and mode != 2 {
	draw_sprite(sUseYaMouse,0,cl,cb);
}

if mode == 2 and instance_exists(npc) {
	with npc {
		var px = x;
		var py = y;
		image_xscale = 2;
		image_yscale = 2;
		x = other.cl+4+8*image_xscale;
		y = other.cb-4-8*image_yscale;
		
		event_perform(ev_draw,0);
		
		//text = "Juicy JIGGLER\n10x BasicShroom\nA juicey little friendley JIGGLER.\nNo warranty."
		event_perform_object(oNPCArrow,ev_other,ev_user0);
		image_xscale = 1;
		image_yscale = 1;
		x = px;
		y = py;
	}
	play_trade_npc_sound();
	draw_sprite_ext(sCharS,0,cr-4-16,cb-4-16,-2,2,0,c_white,1);
}

