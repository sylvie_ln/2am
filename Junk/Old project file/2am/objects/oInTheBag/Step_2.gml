if trade_lock {
	var all_in = true;
	if holding { all_in = false; }
	/*
	if steal > 0 { 
		all_in = false; 
	}
	*/
	with oSticker {
		if !wanted { continue; }
		if in_bag != -1 {
			all_in = false;	
		}
	}
	if all_in and !done {
		with oSticker {
			if in_bag != 0 {
				owner = in_bag;
			}	
		}
		if stole {
			npc.text = npc.complete_steal_text;
			stole = false;
		} else {
			npc.text = npc.complete_text;
		}
		if alarm[0] < 0 {
			alarm[0] = global.default_room_speed;
		}
	}
	
	if target_item == noone {
		var npc_bag;
		with oBag {
			if type == 1 { npc_bag = self; }	
		}
		with oSticker {
			if owner == 1 and in_bag == 0 and wanted {
				other.target_item = self;
			}
		}
		if target_item != noone {
			var free_pos = bag_grid_find_free(npc.name,target_item);
			cell_pos = free_pos;
			if free_pos[0] == -1 or item_restock != 0 {
				if was_on_head {
					hey += head_item_height;
					was_on_head = false;
				}
				target_pos = [hex,hey-abs(target_item.y-target_item.bbox_bottom)];
				hey -= abs(target_item.bbox_bottom-target_item.bbox_top);
			} else {
				var npc_bag_left = npc_bag.x - npc_bag.sprite_xoffset;
				var npc_bag_top = npc_bag.y - npc_bag.sprite_yoffset;
				target_pos[0] = npc_bag_left + free_pos[0]*global.bag_grid_cell_size + target_item.sprite_xoffset;
				target_pos[1] = npc_bag_top + free_pos[1]*global.bag_grid_cell_size + target_item.sprite_yoffset;
			}
			
			/*
			//disabled_show_debug_message(sprite_get_name(npc_bag.sprite_index));
			var bag_grid = global.bag_grid_map[? npc.name]
			var bw = ds_grid_width(bag_grid);
			var bh = ds_grid_height(bag_grid);
			for(var j=0; j<bh; j++) {
				var str = "";
				for(var i=0; i<bw; i++) {
					str += string(bag_grid[# i,j]);
				}
				//disabled_show_debug_message(str);
			}*/
			orig_pos = [target_item.x,target_item.y];
			move_timer = 0;
			target_item.visible = false;
		}
	} else {
		if move_timer <= move_time {
			target_item.x = round(lerp(orig_pos[0],target_pos[0],move_timer/move_time));
			target_item.y = round(lerp(orig_pos[1],target_pos[1],move_timer/move_time));
			move_timer++;
		}
		if move_timer > move_time {
			target_item.visible = true;
			if cell_pos[0] != -1 and item_restock == 0 {
				bag_grid_insert(npc.name,cell_pos[0],cell_pos[1],target_item);
			} else {
				target_item.depth -= 1;	
			}
			target_item.owner = -1;
			target_item.in_bag = -1;
			if !ds_exists(target_item.item,ds_type_list) {
				target_item.item = trade_add_item(target_item.shroom_type);
				trade_modify_item(target_item.item,item_data.cost,trade_cost(noone,0));
				if npc.name == "Wirte" and target_item.shroom_type == oWorthlessShroom {
					trade_modify_item(target_item.item,item_data.name,"Wonderful Shroom");
				}
				trade_modify_item(target_item.item,item_data.desc,npc.no_tradebacks_text);
			}
			if !ds_map_exists(global.npc_bag_map,npc.name) {
				var bag_sticker_list = ds_list_sylvie_create();
				bag_grid_init(npc.name);
			} else {
				var bag_sticker_list = global.npc_bag_map[? npc.name];
			}
			var titem = ds_list_sylvie_create();
			trade_copy_item(titem,target_item.item);
			var st_info = list_create(target_item.x-cl,target_item.y-ct,object_get_name(target_item.shroom_type),target_item.image_index,titem);
			ds_list_mark_as_list(st_info,npc_bag_sticker_info.item);
			ds_list_add(bag_sticker_list,st_info);
			ds_list_mark_as_list(bag_sticker_list,ds_list_size(bag_sticker_list)-1);
			ds_map_replace_list(global.npc_bag_map,npc.name,bag_sticker_list);
			target_item = noone;
		}	
	}
}

if done { exit; }

var was_holding = holding;
var click_press = mouse_check_button_pressed(mb_left) 
or (!oCursor.wait_until_moved and !holding and input_pressed("Jump"))
or (holding and input_pressed("Shroom"));
if press_holding == 0 and (!oCursor.wait_until_moved and !holding and input_pressed("Jump")) {
	press_holding = 1;
	press_wait = room_speed div 2;
} 
if holding and input_released("Jump") and (press_holding <= 0 or press_wait <= 0) {
	click_press = true;
	press_holding = 0;
}
if !holding and input_released("Jump") {
	press_holding = 0;
	press_wait = 0;
}
var moved = input_pressed("Left") or input_pressed("Right") or input_pressed("Up") or input_pressed("Down");
if press_holding == 1 and moved {
	press_holding = -1;
}
if press_wait > 0 {
	press_wait--;
}

if click_press or (mouse_spam and keyboard_check(vk_shift)) 
and !hover_quit and !hover_done and (mode != 2 or !hover_trade) and alarm[0] < 0 {
	if holding { // put down a held item
		var st = instance_create_depth(mouse_x + holding_offset_x,mouse_y + holding_offset_y,depth-2,oSticker);
		st.sprite_index = sticker;
		st.image_index = sii;
		if original {
			st.shroom = shroom;
		} else {
			st.shroom_type = shroom_type;	
		}
		st.owner = owner;	
		st.wanted = wanted;
		with st { event_user(0); }
		
		play_sound(holding ? sndCannotPlaceItemInBag : sndPlaceItemInBag);
		
		if !holding {
			if original {
				original = false;
			}
			if mode == 0 and st.in_bag {
				done = true;
				with oCursor { active = false; alarm[0] = global.default_room_speed; }
				alarm[0] = global.default_room_speed div 2;
			} else if mode == 0 {
				mode = 1;
			}
			if mode == 2 {
				if trade_lock and steal <= 0 { exit; }
				if trade_lock {
					if st.owner == -1 {
						if st.in_bag == 0 {
							if steal {
								//st.owner = 1;	
							}
						} 
					} else if st.owner == 1 {
						if st.in_bag == 0 {
							if steal > 0 {
								if st.wanted {
									steal--;
									if steal == 0 {
										npc.text = npc.steal_abort_text;
									} else {
										npc.text = npc.give_back_text;
									}
								} else {
									npc.text = npc.give_back_text;
								}
							} 
						} else if st.in_bag == 1 {
							if st.wanted {
								npc.text = npc.steal_over_text;
								stole = true;
							}
						}
					}
				} else if trade_ok or trade_was_ok { // if trade is ready
					if st.in_bag == 1  { // put it in your bag
						if st.owner == 1 {
							if was_in_bag {
								if trade_was_ok {
									trade_ok = true;	
									offer_evaluated = true;
									trade_was_ok = false;
								}
								was_in_bag = false;
							} else {
								//re-evaluate the offer
								trade_was_ok = false;
								trade_ok = false;	
								event_perform(ev_alarm,2);
							}
						} else if st.owner == -1 {
							trade_lock = true;
							what_you_got = st.shroom_type;
							if object_is_ancestor(what_you_got,oBagUpgrade) {
								bag_upgrade(what_you_got);
								st.away = true;
							}
							with oSticker {
								if in_bag == 0 and owner == 1 {
									wanted = true;	
								}
							}
							var npc_bag;
							with oBag {
								if type == 1 { npc_bag = self; }	
							}
							var npc_bag_left = npc_bag.x - npc_bag.sprite_xoffset;
							var npc_bag_top = npc_bag.y - npc_bag.sprite_yoffset;
							if orig_pos[0] != hex {
								bag_grid_delete(npc.name,
								(orig_pos[0]-st.sprite_xoffset-npc_bag_left) div global.bag_grid_cell_size,
								(orig_pos[1]-st.sprite_yoffset-npc_bag_top) div global.bag_grid_cell_size,
								st);
							}
							var bag_sticker_list = global.npc_bag_map[? npc.name];
							for(var i=0; i<ds_list_size(bag_sticker_list); i++) {
								var info = bag_sticker_list[|i];
								if orig_pos[0]-cl == info[|npc_bag_sticker_info.xp] and orig_pos[1]-ct == info[|npc_bag_sticker_info.yp] {
									break;	
								}
							}
							ds_list_sylvie_destroy(info);
							ds_list_delete(bag_sticker_list,i);
							ds_map_replace_list(global.npc_bag_map,npc.name,bag_sticker_list);
							npc.text = npc.thank_text;
							trade_ok = true;
							trade_was_ok = false;
						}	
					} else { // put it in the field
						if (was_in_bag or selected == noone) and st.owner == 1 {
							//re-evaluate the offer
							was_in_bag = false
							trade_ok = false;
							event_perform(ev_alarm,2);
						}  else if selected != noone {
							if trade_was_ok {
								npc.text = npc.lets_text;
								trade_ok = true;
								trade_was_ok = false;
								offer_evaluated = true;
							}
						}
					}
				} else if st.in_bag == 1 { // trade not ready, put it back in your bag
					var middle_empty = true;
					with oSticker {
						if in_bag == 0 {
							middle_empty = false;	
						}
					}
					if middle_empty {
						if selected != noone {
							// revert to the selected item's text
							var item = selected.item;
							npc.text = 
							npc.thatsmy_text+item[|item_data.name]+((item[|item_data.obj]=="oHint")?"":".")+"\n"+
							item[|item_data.desc]; 
							alarm[1] = -1;
							alarm[2] = global.default_room_speed;
						} else {
							// revert to the original text
							npc.text = npc.orig_text;	
						}
					} else {
						if !was_in_bag {
							alarm[1] = -1;
							alarm[2] = global.default_room_speed;
						} else {
							was_in_bag = false;	
							if !offer_evaluated {
								alarm[1] = -1;
								alarm[2] = 1;
							}
						}
						//event_perform(ev_alarm,2);
					}
				} else { // trade not ready, put it in the field
					if heist {
						event_perform(ev_alarm,1);
					} else
					if true { //or selected != noone {
						// evaluate the offer if the sticker is newly added to field
						if was_in_bag {
							if was_in_bag { was_in_bag = false; }
							alarm[1] = -1;
							alarm[2] = global.default_room_speed;
						} else if selected != noone {
							if trade_was_ok {
								trade_ok = true;
								npc.text = npc.lets_text;
								trade_was_ok = false;	
							}
							if !offer_evaluated {
								alarm[1] = -1;
								alarm[2] = 1;
							}
						} else {
							alarm[1] = -1;
							alarm[2] = 1;
						}
					}
					// otherwise don't change any text
				}
				if st.in_bag != 0 and !trade_lock {
					st.owner = st.in_bag;	
				} 
				if backup == -12345 {
					backup = st;	
					st.item = backup_item;
					target_pos[0] = st.x;
					target_pos[1] = st.y;
				}
			}
		}
	} else { // click a sticker
		var st = collision_point(mouse_x,mouse_y,oSticker,true,true);
		var st_offset_x = st != noone ? st.x - mouse_x : 0;
		var st_offset_y = st != noone ? st.y - mouse_y : 0;
		
		if st != noone and (mode != 2 or st != target_item) {
			if mode == 2 {
				if !trade_lock and move_timer > 0 and move_timer < move_time {
					exit;	
				}
				if trade_lock and heist { exit; }
				/*
				if alarm[2] >= 0 or alarm[1] >= 0 {
					if st.owner == -1 { exit; }
					if selected != noone and selected.in_bag == -1 {
						//exit;	
					}
				}*/
				if st.in_bag == -1 { // if you click an npc's bag sticker
					var item = st.item;
					var traded_by_me = npc.object_index != oHacker and st.item[|item_data.desc] == npc.no_tradebacks_text;
					var in_field = noone;
					with oSticker {
						if owner == -1 and in_bag == 0 {
							in_field = id; 
							break;
						}
					}
					if !trade_ok and selected_wait == noone {
						if in_field != noone {
							npc.text = npc.finish_text;
							selected_wait = st;
							if in_field != noone {
								offer_evaluated = true; 
							}
							alarm[1] = -1;
							alarm[2] = global.default_room_speed;
							if selected != noone and selected.x == orig_pos[0] and selected.y == orig_pos[1] {
								selected.visible = true;
								selected.in_bag = -1;
								selected = selected_wait;
								selected_wait = noone;
								backup = noone;
								sticker_x = st.x;
								sticker_y = st.y;
								sticker = st.sprite_index;
								sii = st.image_index;
								exit;
							}
						} else {
							npc.text = 
							npc.thatsmy_text+item[|item_data.name]+((item[|item_data.obj]=="oHint")?"":".")+"\n"+
								item[|item_data.desc]; 
						}
					}
					if selected == st {
						// deselecting
						if trade_ok { // after trade is approved
							/*
							npc.text = npc.back_text; 
							trade_ok = false;
							*/
							// do nothing! can't deselect at this point!
						} else { // deselecting outside a trade
							st.visible = true;
							selected = noone;
							alarm[2] = -1;
							alarm[1] = -1;
							npc.text = npc.dont_text; 
						}
					} else { // selecting
						if trade_ok {
							if !trade_lock {
								if traded_by_me {
									npc.text = npc.no_tradebacks_text;
								} else if selected_wait == noone {
									npc.text = npc.finish_text;
									trade_was_ok = false;
									trade_ok = false;
									selected_wait = st;
								}
							}
							// cannot select something else here!!
						} else {
							if selected_wait == noone {
								if selected != noone {
									selected.visible = true;
								}
								selected = st;
								alarm[1] = -1;
								alarm[2] = global.default_room_speed;
							} else if !traded_by_me {
								play_sound(sndDialogSelect);
								npc.text = npc.finish_text;
								trade_was_ok = false;
								selected_wait = st;	
								if in_field != noone {
									offer_evaluated = true; 
								}
								alarm[1] = -1;
								alarm[2] = global.default_room_speed;
							} 
						}
					}
					if selected == st {
						sticker_x = st.x;
						sticker_y = st.y;
						sticker = st.sprite_index;
						sii = st.image_index;
					}
				} else { // if you click a sticker not in npc's bag
					if !trade_ok and st.in_bag == 0 {
						alarm[1] = -1;
						alarm[2] = -1;
					} else if st.in_bag == 1 { 
						//select from your bag while npc sticker is highlighted
						// update text based on what the npc thinks of the item
						//alarm[2] = -1;
						//alarm[1] = -1;
						if trade_lock {
							if st.owner == -1 {
								if steal > 0 {
									npc.text = npc.promise_text;
								} else {
									npc.text = npc.out_of_bag_text;	
								}
							} else if st.owner == 1 {
								if st.wanted {	
									npc.text = npc.yes_give_text
								} else if steal > 0 {
									npc.text = npc.no_way_text	
								}
							}
						} else if trade_ok {
							was_in_bag = true;
							//if npc.text != npc.more_text {
								offer_evaluated = false;
							//}
							npc.text = npc.more_text;
							trade_was_ok = true;
							trade_ok = false;
						} else {
							was_in_bag = true;
							if selected != noone {
								var item = selected.item;
							} else {
								var item = noone;
								with oSticker {
									if in_bag != -1 { continue; }
									item = self.item;
									var costmap = item[|item_data.cost];
									if ds_map_exists(costmap,object_get_name(st.shroom_type)) {
										break;	
									}
									item = noone;
								}
							}
							if item != noone {
								var costmap = item[|item_data.cost];
								if ds_map_exists(costmap,object_get_name(noone)) {
									npc.text = npc.refuse_text;
								} else if !ds_map_exists(costmap,object_get_name(st.shroom_type)) {
									npc.text = npc.no_text;
									var cost = costmap[? ds_map_find_first(costmap)];
									if cost == 0 {
										npc.text = npc.no_exact_text;	
									}
								} else {
									var cost = costmap[?object_get_name(st.shroom_type)];
									switch(cost) {
										case 0: npc.text = npc.exact_text;  break;
										case 1: npc.text = npc.low_text;    break;
										case 2: npc.text = npc.medium_text; break;
										case 3: npc.text = npc.high_text;	break;
									}
								}
							} else {
								npc.text = npc.no_text;
							}
						}
					}
					if trade_ok { // selecting stuff during a trade
						// selecting from your own bag is fine
						// grabbing from the field is a problem
						if st.in_bag == 0 {
							// selecting the npc's item is okay but set the backup!
							if st.owner == -1 and st != selected {
								backup = -12345;
								backup_item = st.item;
							}
							// selecting your stuff is a problem
							if st.owner == 1 {
								// if you haven't put the npc's item in your bag, you're attempting to backout	
								// if you have, you're stealing!
								if trade_lock {
									if st.wanted {
										npc.text = npc.steal_text;
										steal++;
									}
								} else {
									//if npc.text != npc.back_text {
										offer_evaluated = false;
									//}
									npc.text = npc.back_text;
									trade_was_ok = true;
									trade_ok = false;
								}
							}
						}
					}
					if st.in_bag == 0 and st.owner == -1 
					and (st == selected or !offer_evaluated or !trade_ok) { exit; }
					holding = true;
					holding_offset_x = st_offset_x;
					holding_offset_y = st_offset_y;
					sticker = st.sprite_index;
					sii = st.image_index;
					owner = st.owner;
					wanted = st.wanted;
					was_in_bag = (st.in_bag == 1);
					if shroom != noone and st.shroom == shroom {
						original = true;	
					} else {
						shroom_type = st.shroom_type;	
					}
					with st { instance_destroy(); }
				}
			} else {
				holding = true;
				holding_offset_x = st_offset_x;
				holding_offset_y = st_offset_y;
				sticker = st.sprite_index;
				sii = st.image_index;
				if shroom != noone and st.shroom == shroom {
					original = true;	
				} else {
					shroom_type = st.shroom_type;	
				}
				with st { instance_destroy(); }
			}
		}
	}
}
with oSticker {
	if away { instance_destroy(); }	
}

if mode == 1 and was_holding and !holding and input_pressed("Shroom") {
	done = true;
	instance_destroy();
	exit;
}