trade_was_ok = false;
offer_evaluated = false;
if !holding {
	var items_in_middle = false;
	with oSticker {
		if in_bag == 0 {
			items_in_middle = true;
			break;
		}
	}
	if items_in_middle {
		var traded_by_me = npc.object_index != oHacker 
			and instance_exists(selected)
			and selected.item[|item_data.desc] == npc.no_tradebacks_text;
		if !traded_by_me {
			npc.text = npc.hmm_text;
			alarm[1] = global.default_room_speed;
		}
	} else {
		//offer_evaluated = true;
		if selected != noone {
			//var item = selected.item;
			//npc.text = 
			//npc.thatsmy_text+item[|item_data.name]+((item[|item_data.obj]=="oHint")?"":".")+"\n"+
			//				 item[|item_data.desc]; 
		} else {
			//npc.text = npc.orig_text;	
		}
		alarm[1] = -1;
	}
} else {
	alarm[2] = global.default_room_speed;	
}