if mode == 1 and !holding and input_pressed("Shroom") {
	done = true;
	instance_destroy();
	exit;
}

if input_pressed("Escape") and !trade_lock {
	instance_destroy();
	exit;
}