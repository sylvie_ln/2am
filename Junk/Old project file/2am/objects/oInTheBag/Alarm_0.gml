var wait = false;
if mode == 2 {
	with oSticker {
		if in_bag == 0 { 
			wait = true; 
			break; 
		}
	}
}
if !wait and !holding {
	done = true;
	instance_destroy();
	exit;
}
alarm[0] = global.default_room_speed; 