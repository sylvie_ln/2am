{
    "id": "c8ff8d82-948f-4875-b52e-6b0807ea0545",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShroom",
    "eventList": [
        {
            "id": "54d3181c-ce9b-46ff-bd3c-1f93b15b724e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "c8ff8d82-948f-4875-b52e-6b0807ea0545"
        },
        {
            "id": "4b614cb2-cf35-4555-8b32-b8793fa282a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8ff8d82-948f-4875-b52e-6b0807ea0545"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c9b106e-eadc-4e4d-a2fc-32ee1b51c52a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}