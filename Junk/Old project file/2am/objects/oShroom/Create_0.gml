event_inherited();

upblocker = true;
solid_boundary = true;
paused = false;

cutey = false;

act1 = "Examine"
act2 = "Collect"

depth = 5;

rare = false;
cute = false;
always_respawn = false;

thrown_timer = 0;
thrown_time = global.default_room_speed div 3;
throwdown = false;

spinny = false;
throwspin = false;
draw_angle = 0;

no_carry = false;

name = object_get_name(object_index)

if room != rmMushZone and global.do_deletion {
	if ds_map_exists(global.shroom_deletion_map,get_that_room()+pos_key(other.xstart,other.ystart)) {
		event_user(0);
		instance_destroy();
	}
}