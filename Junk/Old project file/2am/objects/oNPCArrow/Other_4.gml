deco = sylvie_irandom(sprite_get_number(sDecos)-1);
deco_color = sylvie_choose(base_colors[sylvie_irandom(total_colors-1)],base_colors_dark[sylvie_irandom(total_colors-1)]);
hair = sylvie_irandom(sprite_get_number(sHairs)-1);
hair_color = pattern_colors[sylvie_irandom(total_colors-1)];
clothe = sylvie_irandom(sprite_get_number(sClotheBases)-1);
clothe_color = sylvie_choose(base_colors[sylvie_irandom(total_colors-1)],base_colors_dark[sylvie_irandom(total_colors-1)]);
pattern = sylvie_irandom(sprite_get_number(sClothePatterns)-1);
pattern_color = hair_color;
while pattern_color == hair_color {
	pattern_color = pattern_colors[sylvie_irandom(total_colors-1)];
}
body_color = 
sylvie_choose(
make_color_hsl_240(sylvie_random(10,20),120,sylvie_random(60,200)),
make_color_hsl_240(sylvie_random(0,239),120,sylvie_random(90,150)),
);