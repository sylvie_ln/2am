/// @description Insert description here
// You can write your code in this editor
draw_set_font(global.neco_font);
var w = string_width(text);
var h = string_height(text);
var b = 4;
var yo = 20;
draw_set_color(c_white);
var cl = camera_get_view_x(view_camera);
var ct = camera_get_view_y(view_camera);
var cr = cl+camera_get_view_width(view_camera);
var cb = ct+camera_get_view_height(view_camera);
var left = cl+4+16*image_xscale+8;
var top = cb-32-2*b; //cb-h-2*b;
var right = ((cl+cr) div 2)+32; //max(left+w+2*b,(cl+cr) div 2); //left+w+2*b;
var bottom = cb-b;
if w >= abs(right-left)-2*b {
	var split = string_split(text," ");
	var new_text = "";
	for(var i=0; i<ds_list_size(split); i++) {
		var word = split[|i];
		if string_width(new_text+word+" ") >= abs(right-left)-2*b {
			new_text += "\n"
		}
		new_text += word;
		if i < ds_list_size(split)-1 {
			new_text += " ";	
		}
	}
	text = new_text;
	var w = string_width(text);
	var h = string_height(text);
	ds_list_sylvie_destroy(split);
}
/*
var left = x-w/2-b;
var top = y-yo-h-b;
var right = x+w/2+b;
var bottom = y-yo+b;

if left < cl {
	right += abs(cl-left)+b;	
	left += abs(cl-left)+b;
}
if right > cr {
	left -= abs(cr-right)+b;
	right -= abs(cr-right)+b;	
}
if top < ct {
	bottom += abs(bbox_bottom+4+b-top);
	top = bbox_bottom+4+b;
	draw_triangle(x,bbox_bottom+b,x-b,top,x+b,top,false);
} else {
	draw_triangle(x,y-yo+b*2,x-b,y-yo+b-1,x+b,y-yo+b-1,false);	
}
*/
draw_roundrect(left,top,right,bottom,false);
draw_triangle(left,y-b,left,y+b,x+4+8*image_xscale,y,false);	
draw_set_color(c_black);
draw_text_centered((left+right) div 2,top+((bottom-top) div 2)-(h div 2),text);