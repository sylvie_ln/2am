if !surface_exists(stencil) {
	stencil = surface_create(16,16);
}
if !surface_exists(surfy) {
	surfy = surface_create(16,16);
}

if surface_exists(stencil) {
	surface_set_target(stencil);
	draw_clear_alpha(c_black,0);
	draw_sprite_ext(sClotheBases,clothe,8,8,1,1,0,c_white,1);
	surface_reset_target();
}
if surface_exists(surfy) {
	/*
	var ii = image_index;
	sprite_index = sBody;
	draw_outline();
	sprite_index = sClotheBases;
	image_index = clothe;
	draw_outline();
	sprite_index = sDecos;
	image_index = deco;
	draw_outline();
	sprite_index = sHairs;
	image_index = hair;
	draw_outline();
	sprite_index = sBody;
	image_index = ii;
	draw_outline();
	*/
	draw_sprite_ext(sBody,0,x,y,1,1,0,body_color,1);
	surface_set_target(surfy);
	draw_clear_alpha(c_black,0);
	draw_sprite_ext(sClothePatterns,pattern,8,8,1,1,0,pattern_color,1);
	gpu_set_blendmode_ext(bm_dest_color,bm_src_alpha);
	draw_surface(stencil,0,0);
	gpu_set_blendmode(bm_normal);
	surface_reset_target();
	draw_sprite_ext(sClotheBases,clothe,x,y,1,1,0,clothe_color,1);
	draw_surface(surfy,x-8,y-8);
	draw_sprite_ext(sDecos,deco,x,y,1,1,0,deco_color,1);
	draw_sprite_ext(sHairs,hair,x,y,1,1,0,hair_color,1);
}
if active {
	draw_set_font(global.sylvie_font);
	var w = string_width(text);
	var h = string_height(text);
	var b = 4;
	var yo = 20;
	draw_set_color(c_white);
	var left = x-w/2-b;
	var top = y-yo-h-b;
	var right = x+w/2+b;
	var bottom = y-yo+b;
	var cl = camera_get_view_x(view_camera)+b;
	var ct = camera_get_view_y(view_camera)+b;
	var cr = cl+camera_get_view_width(view_camera)-b;
	var cb = ct+camera_get_view_height(view_camera)-20-b;
	
	if left < cl {
		right += abs(cl-left)+b;	
		left += abs(cl-left)+b;
	}
	if right > cr {
		left -= abs(cr-right)+b;
		right -= abs(cr-right)+b;	
	}
	if top < ct {
		bottom += abs(bbox_bottom+4+b-top);
		top = bbox_bottom+4+b;
		draw_triangle(x,bbox_bottom+b,x-b,top,x+b,top,false);
	} else {
		draw_triangle(x,y-yo+b*2,x-b,y-yo+b-1,x+b,y-yo+b-1,false);	
	}
	draw_roundrect(left,top,right,bottom,false);
	draw_set_color(c_black);
	draw_text_centered((left+right) div 2,top+b,text);
}