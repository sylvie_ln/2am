{
    "id": "621be3db-392b-40e7-bed1-eb534a59639b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPCArrow",
    "eventList": [
        {
            "id": "8b5a09d2-870c-42d4-af1a-e83c1cf9e552",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "621be3db-392b-40e7-bed1-eb534a59639b"
        },
        {
            "id": "0fa74a8f-a8ec-4f77-9e2d-75b9667d3ce8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "621be3db-392b-40e7-bed1-eb534a59639b"
        },
        {
            "id": "19fa9e8d-6ee0-4e20-b4fc-478d191fee96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "621be3db-392b-40e7-bed1-eb534a59639b"
        },
        {
            "id": "30e82526-efa1-45ee-961d-db2d41d7370d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "621be3db-392b-40e7-bed1-eb534a59639b"
        },
        {
            "id": "f081420a-acbd-4787-82ea-db8b6c6f3db0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "621be3db-392b-40e7-bed1-eb534a59639b"
        },
        {
            "id": "476c41e7-99eb-410e-a8d5-a6bb3ea819b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "621be3db-392b-40e7-bed1-eb534a59639b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d575649-441d-43d2-86d9-f646b788338d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f688b8-dd4d-4d42-96ce-776174c8176b",
    "visible": true
}