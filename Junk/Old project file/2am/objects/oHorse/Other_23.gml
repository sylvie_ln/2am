var left = input_held("Left");
var right = input_held("Right");
var lp = input_pressed("Left");
var rp = input_pressed("Right");
var hdir = right-left;
if left and right {
	hdir = (input_held_time("Left") <= input_held_time("Right")) ? -1 : 1;
}

var ong = collision_at(x,y+1) and (bounced or vv >= 0);
if hdir != 0 {
	image_xscale = hdir;
}
hv += image_xscale*acc;
if abs(hv) < 0.5+acc {
	hv 	= image_xscale*(0.5+acc);
}
if abs(hv) > spd {
	hv = sign(hv)*spd;	
}

var ong = collision_at(x,y+1) and (bounced or vv >= 0);
do_friction = true;
if (ong and !collision_at(x+hv,y+1))
or (ong and collision_at(x+64*sign(hv),y)) {
	vv = -jmp;
}