{
    "id": "9b32344b-b7b3-4b95-ad1d-03cbcf610ac0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHorse",
    "eventList": [
        {
            "id": "381d1265-7f4c-47a8-8d1f-ebacba7f7e82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9b32344b-b7b3-4b95-ad1d-03cbcf610ac0"
        },
        {
            "id": "e1a67677-8560-4db6-851e-49a6465a6264",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "9b32344b-b7b3-4b95-ad1d-03cbcf610ac0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5ba81ee9-a3a4-4882-bfa1-41485451d3ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "41cae0b4-a6f2-4256-9fe8-bc1761cf6ca2",
    "visible": true
}