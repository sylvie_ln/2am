{
    "id": "e5fde156-fdda-4a51-84c1-04dd94df4fbe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oClimbingCleats",
    "eventList": [
        {
            "id": "6dc6920a-770f-4b10-8ede-58186f682604",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5fde156-fdda-4a51-84c1-04dd94df4fbe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "deba9440-0682-4ce5-a242-0d7c081ae785",
    "visible": true
}