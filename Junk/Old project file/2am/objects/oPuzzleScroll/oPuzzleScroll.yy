{
    "id": "29c2de60-b658-4aab-ac0b-e20103a8bc23",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPuzzleScroll",
    "eventList": [
        {
            "id": "34ae78e8-0ba5-437d-9b10-dc12dc57fd67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29c2de60-b658-4aab-ac0b-e20103a8bc23"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a7aef17-a057-475a-abef-702ed8097adc",
    "visible": true
}