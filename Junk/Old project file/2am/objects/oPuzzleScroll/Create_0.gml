event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = "An ancient scroll, it seems to be containing a puzzle-hint. How are you reading this, anyways?"

damp = 0.85;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!