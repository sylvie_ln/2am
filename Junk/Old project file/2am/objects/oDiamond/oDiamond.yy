{
    "id": "53674b9e-e74e-4546-95f1-2460e8a6ab55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDiamond",
    "eventList": [
        {
            "id": "60a3f64b-aca3-44f5-aa29-8389cb3b864b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "53674b9e-e74e-4546-95f1-2460e8a6ab55"
        },
        {
            "id": "979832d4-97be-4b6f-a657-be2ba9224d44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53674b9e-e74e-4546-95f1-2460e8a6ab55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "10882d05-d6f3-485d-8a54-13778af26fa8",
    "visible": true
}