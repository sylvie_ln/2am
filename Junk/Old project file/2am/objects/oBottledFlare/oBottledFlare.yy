{
    "id": "b2acea85-43fc-4cfb-8422-081d463305f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBottledFlare",
    "eventList": [
        {
            "id": "ac8416c0-b3e4-43e6-87af-a5c3e6ea1406",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2acea85-43fc-4cfb-8422-081d463305f1"
        },
        {
            "id": "5f48cbf6-aa00-4ac1-b5fc-1ad7715910be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2acea85-43fc-4cfb-8422-081d463305f1"
        },
        {
            "id": "a9697468-8663-4a16-bad9-19a042a0e2d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "b2acea85-43fc-4cfb-8422-081d463305f1"
        },
        {
            "id": "0d396701-c67c-4cf5-b795-d94a98659191",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "b2acea85-43fc-4cfb-8422-081d463305f1"
        },
        {
            "id": "2e995915-1fb0-4c52-91c1-dcee1f9639b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b2acea85-43fc-4cfb-8422-081d463305f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b180942-75c0-43f8-b03f-f6c18264ffeb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "42e54525-350d-4620-8d52-eabac4ad086b",
    "visible": true
}