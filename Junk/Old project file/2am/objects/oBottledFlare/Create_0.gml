event_inherited();

upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = @"A firey kitten trapped in a bottle. When used in a Mushroom Zone, the unique atmospheric pressure will cause the bottle to shatter, and the kitten will help find nearby rare items. Secret paths will also be revealed by the light of the kitten."

damp = 0.5;
bounce = damp;
fric = 0.95;

image_speed = 0;

/// In oInit, define a trade entry for this item!