if standard_gen {
	var oo = oMapp.offsets[?area];
	startx = (wwidth div 2)+oo[0];
	starty = (wheight div 2)+oo[1];
	
	ds_grid_clear(door_map,noone);
	ds_grid_clear(common_map,noone);
	ds_grid_clear(uncommon_map,noone);
	ds_grid_clear(luck_map,0);

	tag = get_that_room()+"_"+oRoomGen.area;
	if !ds_map_exists(global.shroom_deletion_map,tag) {
		local_shroom_deletion_map = ds_map_create();
		ds_map_add_map(global.shroom_deletion_map,tag,local_shroom_deletion_map);
	} else {
		local_shroom_deletion_map = global.shroom_deletion_map[?tag];	
	}
	
	sylvie_set_seed(room_seed(startx,starty));
	var commons = ds_list_sylvie_create();
	if level == 2 {
		repeat(40) {
			ds_list_add(commons,0);
		}
		repeat(50) {
			ds_list_add(commons,1);
		}
		repeat(8) {
			ds_list_add(commons,2);
		}
		repeat(2) {
			ds_list_add(commons,3);
		}
	}
	else {
		repeat(10) {
			ds_list_add(commons,0);
		}
		repeat(30) {
			ds_list_add(commons,1);
		}
		repeat(39) {
			ds_list_add(commons,2);
		}
		repeat(20) {
			ds_list_add(commons,3);
		}
		repeat(1) {
			ds_list_add(commons,100);
		}
	}
	
	for(var i=0; i<oRoomGen.wwidth; i++) {
		sylvie_list_shuffle(commons);
		for(var j=0; j<oRoomGen.wheight; j++) {
			sylvie_set_seed(room_seed(i,j));
			common_map[# i,j] = commons[|j];
			
		}
	}
	ds_list_sylvie_destroy(commons);
	
	mapper = oMapp;
	with mapper {
		while step < 12 {
			event_user(13);	
		}
		mapp = mapmapp[? other.area];	
		if ds_map_exists(mapmapp,other.area+"_door") {
			doormapp = mapmapp[? other.area+"_door"];
			ds_grid_copy(other.door_map,doormapp);
		}
		if ds_map_exists(mapmapp,other.area+"_item") {
			itemmapp = mapmapp[? other.area+"_item"];
			ds_grid_copy(other.uncommon_map,itemmapp);
		}
		if ds_map_exists(mapmapp,other.area+"_luck") {
			luckmapp = mapmapp[? other.area+"_luck"];
			ds_grid_copy(other.luck_map,luckmapp);
		}
		if ds_map_exists(mapmapp,other.area+"_lore") {
			loremapp = mapmapp[? other.area+"_lore"];
			ds_grid_copy(other.lore_map,loremapp);
		}
		if ds_map_exists(mapmapp,other.area+"_cute") {
			cutemapp = mapmapp[? other.area+"_cute"];
			ds_grid_copy(other.cute_map,cutemapp);
		}
	}
	
	xp = startx;
	yp = starty;
	if !is_undefined(global.warp_x) {
		xp = global.warp_x;
		yp = global.warp_y;
	}
	event_user(eRoomGen.shift);

}