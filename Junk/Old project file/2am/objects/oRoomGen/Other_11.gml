/// @description Room add
sylvie_set_seed(room_seed(xp,yp));
var lucky = luck_map[# xp,yp];
var very_lucky = false;
if global.flag_dice != 0 { 
	very_lucky = lucky;
	lucky = true; 
} 
var xx = 0; 
var yy = 0;
var w = global.rwidth div cell_size;
var h = global.rheight div cell_size;
var decay = 1;
switch(shift_dir) {
	case edir.up: yy -= h; break;
	case edir.down: yy += h; break;
	case edir.left: xx -= w; break;
	case edir.right: xx += w; break;
	case 0: 
		if !is_undefined(global.warp_x) {
			xx = (xp-global.warp_x)*w;
			yy = (yp-global.warp_y)*h;
		} else {
			xx = (xp-startx)*w;
			yy = (yp-starty)*h;
		}
	break;
}
var burger = [[],[]];
array_copy(burger[0],0,border_position,0,array_length_1d(border_position));
array_copy(burger[1],0,border_opening,0,array_length_1d(border_opening));

var eivlys_room = border_type == 0;

catfood(border_type,xx,yy,w,h,decay,burger);
//room_map[? room_key(xp,yp)] = border;
//gen_map[? room_key(xp,yp)] = [border_position,border_opening,border_type];

//gen_door = (xp == 0 and yp == 0) or sylvie_random(0,1) == 0;


ds_list_clear(valid_blocks);
ds_list_clear(all_blocks);

with oThingy {
	event_user(14);	
}

ds_list_sort(all_blocks,true);
sylvie_list_shuffle(all_blocks);

for(var i=0; i<ds_list_size(all_blocks); i++) {
	with (all_blocks[|i] & 0x3fffff) {
		event_user(13);
	}	
}

sylvie_list_shuffle(valid_blocks);

if door_map[# xp,yp] {
	var door = place_thingy(oDoor);
	if door == noone {
		var door = place_thingy_force(oDoor);
	}
	if door != noone {
		with door {
			tag = global.door_tag;
			rm = asset_get_index(global.previous_room);
			script = "This door leads to a safe place."
			old_script = script;
			event_user(0);
		}
		if !is_undefined(global.warp_x) {
			if xp == global.warp_x and yp == global.warp_y {
				if !instance_exists(oChar) {
					instance_create_depth(door.x,door.y,0,oChar);	
				}
			}
		} else if xp == startx and yp == starty {
			if !instance_exists(oChar) {
				instance_create_depth(door.x,door.y,0,oChar);	
			}
		}
		if instance_exists(door) {
			if !eivlys_room {
				kitten3(door,border_type);	
			}
			with door {
				while !collision_at(x,y+1) {
					y += 1;	
				}
			}
		}
	}
}

if lore_map[# xp,yp] != "" {
	var mem = place_thingy(oMemory);
	if mem == noone {
		var mem = place_thingy_force(oMemory);
	}
	if mem != noone {
		with mem { 
			tag = global.door_tag;
			rm = asset_get_index(global.previous_room);
			memory_id = other.lore_map[# other.xp,other.yp];
			event_perform(ev_other,ev_room_start)
			event_user(0); 
		}
		
		if !is_undefined(global.warp_x) {
			if xp == global.warp_x and yp == global.warp_y {
				if !instance_exists(oChar) {
					instance_create_depth(mem.x,mem.y,0,oChar);	
				}
			}
		} 
		
		if instance_exists(mem) {
			if !eivlys_room {
				kitten3(mem,border_type);	
			} 
			with mem {
				while !collision_at(x,y+1) {
					y += 1;	
				}
			}
		}
	}
}

if cute_map[# xp,yp] != "" {
	var tem = place_thingy(asset_get_index(cute_map[# xp,yp]));
	if tem != noone {
		with tem { 
			event_user(0); 
		}
		if instance_exists(tem) {
			if !eivlys_room {
				kitten3(tem,border_type);	
			}
			with tem {
				cute = true;
				while !collision_at(x,y+1) {
					y += 1;	
				}
			}
		}
	}
}
/*
if ds_map_exists(rare_map,k) {
	var shroom = place_thingy(rare_map[?k]);
	with shroom { event_user(0); }
}
*/
if uncommon_map[# xp,yp] != 0 and lucky {
	/*
	global.area_names = [
	0  "Cloudy Mixture", 
	1  "Cataclysmic Fountain",
	2  "Cornered Wilderness",
	3  "Captive Automaton",
	4  "Cackling Bust",
	5  "Caustic Puddle",
	6  "Crumbled Hearth",
	7  "Creative Laughter",
	8  "Cuter Willows",
	9  "Careless Desire",
	10 "Corpse Facade",
	11 "Cordless Funeral",
	];
	*/
	var cherry = [noone,rare_shroom,oTallShroom,oTallShroom,oTallShroom,oWorthlessShroom];
	var cavern = [noone,rare_shroom,oWideShroom,oWideShroom,oWideShroom,oWorthlessShroom];
	var canyon = [noone,rare_shroom,oTinyShroom,oTinyShroom,oTinyShroom,oWorthlessShroom];
	var unco = [noone,rare_shroom,
	oWorthlessShroom,oTinyShroom,oTallShroom,oWideShroom
	];
	switch(oRoomGen.area) {
		case "Cloudy Mixture":
			unco = [noone,sylvie_choose(oSpringShroom,oFloatShroom),oSpringShroom,oFloatShroom,sylvie_choose(oTallShroom,oTallShroom,oWideShroom,oWideShroom,oTinyShroom),sylvie_choose(oTinyShroom,oTinyShroom,oTallShroom,oWideShroom)]	
		break;
		case "Cataclysmic Fountain":
			unco = [noone,sylvie_choose(oKittyShroom,oSparkleShroom,oFloatShroom),sylvie_choose(oSpringShroom,oFloatShroom),sylvie_choose(oSparkleShroom,oRainbowShroom),sylvie_choose(oKittyShroom,oSpeedShroom),sylvie_choose(oPoisonShroom,oFloatShroom)]
		break;
		case "Cornered Wilderness":
		case "Captive Automaton":
		case "Cordless Funeral":
			unco = cavern;
		break;
		case "Creative Laughter":
		case "Cuter Willows":
		case "Careless Desire":
			unco = cherry;
		break;
		case "Cackling Bust":
		case "Caustic Puddle":
		case "Crumbled Hearth":
			unco = canyon;
		break;
		case "Corpse Facade":
		break;
	}
	if level == 1 {
		unco = [noone,rare_shroom,oWorthlessShroom,oWorthlessShroom,oWorthlessShroom,oWorthlessShroom]	
	}
	if unco != noone {
		var shroom = place_thingy(unco[uncommon_map[# xp,yp]]);
		//show_debug_message([xp,yp,uncommon_map[# xp,yp],shroom]);
		if uncommon_map[# xp,yp] == 1 and shroom == noone {
			place_thingy_force(unco[uncommon_map[# xp,yp]]);
		}
		with shroom { event_user(0); }
		if instance_exists(shroom) {
			if !eivlys_room {
				kitten3(shroom,border_type);	
			}
			if shroom.object_index == rare_shroom {
				shroom.rare = true;
			}
		}
	}
}

var num = common_map[# xp, yp];
if very_lucky { num++; }
if eivlys_room {
	place_thingy(oGiftShopKitteyPlush);
	place_thingy(oSylviePlushey);
	place_thingy(oSylviePlushey);
	var iii =0;
	with place_thingy(oKittyShroom) { image_index = iii++; }
	with place_thingy(oKittyShroom) { image_index = iii++; }
	with place_thingy(oKittyShroom) { image_index = iii++; }
	with place_thingy(oKittyShroom) { image_index = iii++; }
	with place_thingy(oKittyShroom) { image_index = iii++; }
	with place_thingy(oKittyShroom) { image_index = iii++; }
	with place_thingy(oKittyShroom) { image_index = iii++; }
	place_thingy(oSylviePlushey);
	place_thingy(oSylviePlushey);
	place_thingy(oKittenKey);
} else {
	repeat(num) {
		var shroom = place_thingy(oBasicShroom);
		if shroom == noone { break; }
		with shroom { event_user(0); }
	}
}

with oShroom {
	if just_created	{
		var xt = x;
		var yt = y;
		switch(other.shift_dir) {
			case edir.up: yt += global.rheight; break;
			case edir.down: yt -= global.rheight; break;
			case edir.left: xt += global.rwidth; break;
			case edir.right: xt -= global.rwidth; break;
		}
		//disabled_show_debug_message("checking "+string(other.local_shroom_deletion_map)+" " + room_key(other.xp,other.yp)+pos_key(xt,yt))
		if other.local_shroom_deletion_map[?room_key(other.xp,other.yp)+pos_key(xt,yt)] == object_index {
			//disabled_show_debug_message("deleting" + room_key(other.xp,other.yp)+pos_key(xt,yt))
			instance_destroy();	
		}
	}
}

with oShroom {
	just_created = false;	
	var xt = x;
	var yt = y;
	switch(other.shift_dir) {
		case edir.up: yt += global.rheight; break;
		case edir.down: yt -= global.rheight; break;
		case edir.left: xt += global.rwidth; break;
		case edir.right: xt -= global.rwidth; break;
	}
	if object_index == oKittyShroom or object_index == oSpeedShroom {
		generated = false;	
		other.local_shroom_deletion_map[? room_key(other.xp,other.yp)+pos_key(xt,yt)] = object_index;
	}
}

global.explored[?mush_key(xp,yp)] = true;

if !oShifter.shifting {
	event_user(15);
}