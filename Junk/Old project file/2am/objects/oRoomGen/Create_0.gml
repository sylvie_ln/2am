
 sylvie_ds_tracker_print();	
//sylvie_shuffle();
fluffey = 0;

cell_size = sprite_get_width(sBlonck);

tag = "ChangeThisSylvie"
area = "Sylvie's World"

tile = oBlonck;

standard_gen = true;
fixed_border = -1;

level = 0;

shifting = false;
shift_dir = 0;
shift_spd = 16;
shift_time = 0;

global.rwidth = room_width;
global.rheight = (room_height-20);
wwidth = 100;
wheight = 100;

border_type = 15;
border_position = array_create(9,-1);
border_opening = array_create(9,-1);

door_map = ds_grid_sylvie_create(wwidth,wheight);
common_map = ds_grid_sylvie_create(wwidth,wheight);
uncommon_map = ds_grid_sylvie_create(wwidth,wheight);
luck_map = ds_grid_sylvie_create(wwidth,wheight);
lore_map = ds_grid_sylvie_create(wwidth,wheight);
cute_map = ds_grid_sylvie_create(wwidth,wheight);
ds_grid_clear(door_map,false);
ds_grid_clear(common_map,noone);
ds_grid_clear(uncommon_map,noone);
ds_grid_clear(luck_map,false);
ds_grid_clear(lore_map,"");
ds_grid_clear(cute_map,"");

valid_blocks = ds_list_sylvie_create();
all_blocks = ds_list_sylvie_create();

enum eRoomGen {
	shift = 0,
	add = 1,
}

scatter_count = 0;
rare_shroom = noone;

mapper = noone;
