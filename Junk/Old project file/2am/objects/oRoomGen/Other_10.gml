///@description Room shift
with oBlonck {
	if outer { 
		instance_destroy(); 
	}	
}

with oShifter {
	shift_dir = other.shift_dir;	
	event_user(0);
}

xp = nmod(xp,oRoomGen.wwidth);
yp = nmod(yp,oRoomGen.wheight);

var data = mapper.mapp[# xp,yp];
border_type = data & 15;
//show_message(border_type);
border_position = array_create(9,-1);
border_opening = array_create(9,-1);
for(var i=1; i<=8; i*=2) {
	if border_type & i == i {
		var amount = floor(log2(i))*8;
		border_position[i] =  ((data >> 4) >> amount) & 15;
		border_opening[i]  =  ((data >> 8) >> amount) & 15;
		//show_message(border_position[i]);
		//show_message(border_opening[i]);
	}
}
event_user(eRoomGen.add);
/*
var k = room_key(xp,yp);
//disabled_show_debug_message(k);
var k_right = room_key(xp+1,yp);
var k_left = room_key(xp-1,yp);
var k_up = room_key(xp,yp-1);
var k_down = room_key(xp,yp+1);


if ds_map_exists(gen_map,k) {
	var boar = gen_map[? k];
	border_position = boar[0];
	border_opening = boar[1];
	border_type = boar[2];
} else {
	sylvie_set_seed(room_seed(xp,yp));
	border_type = sylvie_random(0,15);
	if xp == startx and yp == starty {
		border_type = 13;	
	}
	if fixed_border != -1 {
		border_type = fixed_border;
		fixed_border = -1;
	}
	
	border_position = array_create(9,-1);
	border_opening = array_create(9,-1);
	
	width = global.rwidth div cell_size;
	height = global.rheight div cell_size;
	for(var i=1; i<=8; i*=2) {
		if i & border_type == 0 {
			construct_wall(i);	
		} else {
			construct_entry(i);	
		}
	}

	// Match borders of adjacent rooms
	if ds_map_exists(room_map,k_right) { 
		var boar = room_map[? k_right];
		var bp = boar[0];
		var bo = boar[1];
		var bt = boar[2];
		border_position[edir.right] = bp[edir.left];
		border_opening[edir.right] = bo[edir.left];
		if bt & edir.left == edir.left {
			border_type = border_type | edir.right;
		} else {
			border_type = border_type & (15-edir.right);
		}
		//disabled_show_debug_message("Matching right "+k_right);
	}

	if ds_map_exists(room_map,k_left) { 
		var boar = room_map[? k_left];
		var bp = boar[0];
		var bo = boar[1];
		var bt = boar[2];
		border_position[edir.left] = bp[edir.right];
		border_opening[edir.left] = bo[edir.right];
		if bt & edir.right == edir.right {
			border_type = border_type | edir.left;
		} else {
			border_type = border_type & (15-edir.left);
		}
		//disabled_show_debug_message("Matching left "+k_left);
	}

	if ds_map_exists(room_map,k_up) { 
		var boar = room_map[? k_up];
		var bp = boar[0];
		var bo = boar[1];
		var bt = boar[2];
		border_position[edir.up] = bp[edir.down];
		border_opening[edir.up] = bo[edir.down];
		if bt & edir.down == edir.down {
			border_type = border_type | edir.up;
		} else {
			border_type = border_type & (15-edir.up);
		}
		//disabled_show_debug_message("Matching up "+k_up);
	}

	if ds_map_exists(room_map,k_down) { 
		var boar = room_map[? k_down];
		var bp = boar[0];
		var bo = boar[1];
		var bt = boar[2];
		border_position[edir.down] = bp[edir.up];
		border_opening[edir.down] = bo[edir.up];
		if bt & edir.up == edir.up {
			border_type = border_type | edir.down;
		} else {
			border_type = border_type & (15-edir.down);
		}
		//disabled_show_debug_message("Matching down "+k_down);
	}
}

if shift_dir != 0 {
	event_user(eRoomGen.add);
} else if !ds_map_exists(gen_map,k) {
	/*
	//disabled_show_debug_message(k +" "+string(border_type));
	for(var i=1; i<=8; i*=2) {
		//disabled_show_debug_message(string(i)+" p"+string(border_position[i])+" o"+string(border_opening[i]));
	}
	*/
	/*
	room_map[? k] = [border_position,border_opening,border_type];
	gen_map[? k] = [border_position,border_opening,border_type];
	if (xp == startx and yp == starty) or (!is_undefined(global.warp_x) and xp == global.warp_x and yp == global.warp_y) {
		event_user(eRoomGen.add);	
	}
}