///@description
event_inherited();
script = @"!set cutscene,1
A voice suddenly echoes...
I am the Goddess of Dice. I saw your amazing feat just now, my child.
Nobody in all of history has ever rolled three sixes in a single toss.
I grant you my blessing. May your travels be filled with good fortune.
The voice goes silent. You have a sudden premonition that it'll be easier to find rare items now....
!set cutscene,0"
cutey = false;

