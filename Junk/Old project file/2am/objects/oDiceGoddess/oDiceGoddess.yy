{
    "id": "b29cfc0c-998c-4fbf-a76b-db487fdb8c3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDiceGoddess",
    "eventList": [
        {
            "id": "dafa216b-d2bf-4a73-8d2a-f63ae6f5998e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b29cfc0c-998c-4fbf-a76b-db487fdb8c3c"
        },
        {
            "id": "89e4661b-f039-4040-8322-6fe6ea7204cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b29cfc0c-998c-4fbf-a76b-db487fdb8c3c"
        },
        {
            "id": "d777e801-7ec7-402e-81a9-0c922aa7edd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b29cfc0c-998c-4fbf-a76b-db487fdb8c3c"
        },
        {
            "id": "1cae377b-5856-43f7-912e-2acaf31ba388",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "b29cfc0c-998c-4fbf-a76b-db487fdb8c3c"
        },
        {
            "id": "2a5ba0da-d25b-487c-a0f5-69b092c44a8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b29cfc0c-998c-4fbf-a76b-db487fdb8c3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "933dc18f-fdb5-4385-a087-666ea74a308b",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}