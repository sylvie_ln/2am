event_inherited();

upblocker = false;
precise_upblocker = true;
pushable = true;
carriable = true;
pusher = true;
carrier = true;

script = ""

odepth = 0;
depth = 0;
if object_index == oHorse {
	depth += 1;	
}
if object_index == oFishCar {
	depth += 3;	
}
if object_index == oWizardmobile {
	depth += 5;	
}
if object_index == oSnowKeety {
	depth += 7;
}
if object_index == oSylvieTruck {
	depth += 9;	
}
if object_index == oHouse {
	depth += 11;	
}
if object_index == oVehicleTemplate {
	depth += 1;
}

riding = false;
do_friction = false;

act1 = "Ride";

damp = 0.5;
bounce = damp;
fric = 0.95;
//grav *= 2;

vehicle_pos = [0,0];

if sprite_exists(sprite_index) {
	item_mask = asset_get_index(sprite_get_name(sprite_index)+"Mask");
	ride_mask = asset_get_index(sprite_get_name(sprite_index)+"RideMask");
} else {
	item_mask = mask_index;
	ride_mask = mask_index;	
}

image_speed = 0;

/// In oInit, define a trade entry for this item!