///@description

// Inherit the parent event
event_inherited();

if paused { image_index = round(image_index/2)*2; exit; }

if collision_at(x,y+1) or bounced {
	if abs(vv) < 0.5 {
		image_index = round(image_index/2)*2;	
		if bag_has_item(oBunneyPin) and thrown_dice {
			image_index = 10;
		}
		image_speed = 0;
	}
	lhv = hv;
	lvv = vv;
} else {
	if thrown_timer > 0 {
		lhv = hv;
		lvv = vv;
	}
	image_speed = lerp(1/4,1,point_distance(0,0,lhv,lvv)/6);	
}
