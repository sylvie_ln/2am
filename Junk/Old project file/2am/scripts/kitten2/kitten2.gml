var enter = argument[0];
var leave = argument[1];
var initial, final;
var boi = border_position[enter]+(border_opening[enter] div 2);
var bof = border_position[leave]+(border_opening[leave] div 2);
switch(enter) {
	case edir.up:
		initial = [left+boi,top];
	break;
	case edir.down:
		initial = [left+boi,top+height-1];
	break;
	case edir.left:
		initial = [left,top+boi];
	break;
	case edir.right:
		initial = [left+width-1,top+boi];
	break;
}
switch(leave) {
	case edir.up:
		final = [left+bof,top];
	break;
	case edir.down:
		final = [left+bof,top+height-1];
	break;
	case edir.left:
		final = [left,top+bof];
	break;
	case edir.right:
		final = [left+width-1,top+bof];
	break;
}
var found = false;
var kittens = ds_priority_create();
var came_from = ds_map_create();
var cost_so_far = ds_map_create();
var cost_map = ds_map_create();
came_from[?pos_key(initial[0],initial[1])] = [undefined,undefined];
cost_so_far[?pos_key(initial[0],initial[1])] = 0;

for(var i=left; i<left+width; i++) {
	for(var j=top; j<top+height; j++) {
		var block = collision_point(i*cell_size,j*cell_size,oBlonck,false,true);
		var is_block = block != noone;
		var cost = (is_block ? choose(10,20,30) : choose(0,1,2));	
		with block {
			if (x div other.cell_size) <= other.left
			or (x div other.cell_size) >= other.left+other.width-1
			or (y div other.cell_size) <= other.top
			or (y div other.cell_size) >= other.top+other.height-1 {
				cost += 10000;
			} else
			if !place_meeting(x,y-1,oBlonck) 
			or !place_meeting(x,y+1,oBlonck) 
			or !place_meeting(x-1,y,oBlonck) 
			or !place_meeting(x+1,y,oBlonck) {
				cost += choose(100,200,300);	
			}
			if image_alpha == 0.5 { cost = choose(0,1,2); }
		}
		cost_map[? pos_key(i,j)] = cost;
	}
}

var shifts = list_create([0,-1],[0,1],[-1,0],[1,0]);
ds_priority_add(kittens,initial,0);
while !ds_priority_empty(kittens) {
	var current = ds_priority_delete_min(kittens);
	if current[0] == final[0] and current[1] == final[1] {
		found = true;
		break;	
	}
	sylvie_list_shuffle(shifts);
	for(var i=0; i<ds_list_size(shifts); i++) {
		var shift = shifts[|i];
		var next = [current[0]+shift[0],current[1]+shift[1]];
		if next[0] < min(final[0],left+1) 
		or next[0] > max(final[0],left+width-2) 
		or next[1] < min(final[1],top+1)
		or next[1] > max(final[1],top+height-2) {
			continue;	
		}
		var nk = pos_key(next[0],next[1]);
		var new_cost = cost_so_far[? pos_key(current[0],current[1])] + cost_map[? nk];
		//disabled_show_debug_message(pos_key(current[0],current[1])+" -> "+nk+" : "+string(new_cost));
		if !ds_map_exists(cost_so_far,nk) or new_cost < cost_so_far[?nk] {
			cost_so_far[?nk] = new_cost;	
			ds_priority_add(kittens,next,new_cost);
			came_from[?nk] = current;
		}
	}
}
// If you change this, copy it into kitten3 as well
var accumulator = ds_list_create();
if found {
	var current = final;
	while !(is_undefined(current[0]) and is_undefined(current[1])) {
		var ck = pos_key(current[0],current[1]);
		var cf = came_from[? ck];
		with collision_point(current[0]*cell_size,current[1]*cell_size,oBlonck,false,true) {
			ds_list_add(accumulator,id);
			image_alpha = 0.5;
		}
		current = cf;
	}
}
if ds_list_size(accumulator) < 5 {
	for(var i=0; i<ds_list_size(accumulator); i++) {
		with accumulator[|i] {
			solid = false;
			with instance_place(x,y+1,oBlonck) {
				ii_only = true;
				just_created = true;
				event_user(13);	
				ii_only = false;
			}
			instance_destroy();	
		}
	}
}
ds_list_sylvie_destroy(accumulator);

ds_priority_destroy(kittens);
ds_map_destroy(came_from);
ds_map_destroy(cost_so_far);
ds_map_destroy(cost_map);
ds_list_sylvie_destroy(shifts);