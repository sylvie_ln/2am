var array = argument0;
var item = argument1;

if !is_undefined(item)
	array[array_length_1d(array)] = item;

return array;