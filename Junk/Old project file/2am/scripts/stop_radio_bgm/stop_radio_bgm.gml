if variable_global_exists("radio_bgm") and audio_exists(global.radio_bgm) and audio_is_playing(global.radio_bgm) {
	audio_stop_sound(global.radio_bgm);
	global.radio_bgm = -1;
}