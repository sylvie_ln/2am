var m = argument[0];
var n = argument[1];
opening_width = 0;
opening_height = 0;
if argument_count > 2 {
	opening_width = argument[2];
	opening_height = argument[3];
}
var topleft = noone;
var topright = noone;
var bottomleft = noone;
var bottomright = noone;
var i = 1;
var j = 1;

if m >= 2 and n >= 2 {
	var i = m div 2;
	var j = n div 2;
} else if m >= 2 and n == 1 {
	var i = m div 2;
} else if m == 1 and n >= 2 {
	var j = n div 2;
}

if m == 1 and n == 1 {
	return mapp_node(0,0,mapp_data(0));
}

if m != 1 {
	var topright = mapp_mxn(m-i,j,opening_width,opening_height,argument[4]+sylvie_random(0,1024));
	topright[ninfo.xp] += i;
}
if n != 1 {
	var bottomleft = mapp_mxn(i,n-j,opening_width,opening_height,argument[4]+sylvie_random(0,1024));
	bottomleft[ninfo.yp] += j;
}
if m != 1 and n != 1 {
	var bottomright = mapp_mxn(m-i,n-j,opening_width,opening_height,argument[4]+sylvie_random(0,1024));
	bottomright[ninfo.xp] += i;
	bottomright[ninfo.yp] += j;		
}
var topleft = mapp_mxn(i,j,opening_width,opening_height,argument[4]+sylvie_random(0,1024));

if m == 1 and n != 1 {
	return mapp_link(list_create_nosylvie(topleft,bottomleft),argument[4]+sylvie_random(0,1024));
} 
if m != 1 and n == 1 {
	return mapp_link(list_create_nosylvie(topleft,topright),argument[4]+sylvie_random(0,1024));
} 

var style = sylvie_choose(0,1,2); //sylvie_choose(0,1);
switch(style) {
	case 0:	
		var n1 = mapp_link(list_create_nosylvie(topleft,topright),argument[4]+sylvie_random(0,1024));
		var n2 = mapp_link(list_create_nosylvie(bottomleft,bottomright),argument[4]+sylvie_random(0,1024));
		return mapp_link(list_create_nosylvie(n1,n2),argument[4]+sylvie_random(0,1024));
	case 1:	
		var n1 = mapp_link(list_create_nosylvie(topleft,bottomleft),argument[4]+sylvie_random(0,1024));
		var n2 = mapp_link(list_create_nosylvie(topright,bottomright),argument[4]+sylvie_random(0,1024));
		return mapp_link(list_create_nosylvie(n1,n2),argument[4]+sylvie_random(0,1024));
	case 2:
		return mapp_link(list_create_nosylvie(topleft,topright,bottomleft,bottomright),argument[4]+sylvie_random(0,1024));	
}
/*

var list = ds_list_create();
for(var i=0; i<argument[0]; i++) {
	for(var j=0; j<argument[1]; j++) {
		ds_list_add(list,mapp_node(i,j,mapp_data(0)));	
	}
}
return mapp_link(list,argument[4]+sylvie_random(0,1024));