///@param door_freq
///@param unco_freq
///@param rare_freq
var door_freq = argument[0];
var unco_freq = argument[1];
var rare_freq = argument[2];
if ds_queue_empty(kittens) { fill = 2; exit; }
var kitten = ds_queue_dequeue(kittens);
if !mapp_done {
	var mappdata = mapp[ninfo.data];
} else {
	var mappdata = mapmapp[? stepname];	
}
var border = mappdata[# kitten[0],kitten[1]] & 0xf;

if kitten[2] mod door_freq == 0 {
	if !her_voice or (kitten[0] == start_xp and kitten[1] == start_yp) {
		if !door_done {
			//show_debug_message(["DOOR", kitten]);
			doormapp[# kitten[0],kitten[1]] = true;
		}
	}
	if kitten[2] > max_dist {
		max_dist = kitten[2];
		var mem_map = global.memory_disks[|0];
		var mem_pair = mem_map[?stepname];
		if wrap_distance(start_xp,start_yp,kitten[0],kitten[1],w,h) > wrap_distance(start_xp,start_yp,mem_pair[|0],mem_pair[|1],w,h) {
			mem_pair[|0] = kitten[0];
			mem_pair[|1] = kitten[1];
		}
	}
}
if kitten[2] mod unco_freq == 0 {
	if !item_done {
		itemmapp[# kitten[0],kitten[1]] = sylvie_choose(2,3,4,5);
	} else {
		sylvie_choose(2,3,4,5);
	}
	if !luck_done {
		luckmapp[# kitten[0],kitten[1]]	= kitten[7];
	}
	kitten[7] = 1-kitten[7];
}
if kitten[3] mod rare_freq == 0 {
	if !item_done {
		itemmapp[# kitten[0],kitten[1]] = 1;
	}
	kitten[3] = 0;
	if !luck_done {
		luckmapp[# kitten[0],kitten[1]]	= kitten[4];
	}
	if kitten[4] and kitten[2] <= 16 {
		//show_debug_message(["RARE",kitten]);
		if kitten[2] < 10 rarity[0]++;
		if kitten[2] < 13 rarity[1]++;
		if kitten[2] < 16 rarity[2]++;
	}
	kitten[4] = 1-kitten[4];
	kitten[5] = 0;
	kitten[6] = 1;
}
		
var visits = [[0,-1,edir.up],[0,1,edir.down],[-1,0,edir.left],[1,0,edir.right]];
sylvie_set_seed(stepseed+c);
sylvie_list_shuffle(four);
var border_count = 0;
var hbor_count = 0;
var vbor_count = 0;
var k8 = false;
for(var v=0; v<4; v++) {
	var visit = visits[four[|v]];
	if(visit[2] & border == 0) { 
		border_count++; 
		if visit[2] <= edir.down {
			vbor_count++;
		} else {
			hbor_count++;	
		}
		continue; 
	}
}
var too_far = (kitten[0] <= 54 or kitten[0] >= 60 or kitten[1] <= 54 or kitten[1] >= 60);
var dead = border_count == 3 and (kitten[0] != start_xp or kitten[1] != start_yp);
var meow = (dead or too_far) and kitten[8] >= 0;// and (kitten[8] >= 0 and (border & kitten[8] == kitten[8])));
for(var v=0; v<4; v++) {
	var visit = visits[four[|v]];
	var nkx = nmod(kitten[0]+visit[0],w);
	var nky = nmod(kitten[1]+visit[1],h);
	if kittens_visited[# nkx,nky] { continue; }
	if(visit[2] & border == 0) { 
		continue;	
	}
	if kitten[8] >= -1 and (kitten[8] == -1 or k8) {
		kitten[8] = visit[2];
	}
	k8 = true;
	var split_rare = 1;
	if visit[2] == edir.up { kitten[6]++; split_rare = min(kitten[6],rare_freq-kitten[3]); }
	if visit[2] == edir.down { split_rare = kitten[5]; 	kitten[5] = 1-kitten[5]; }
	var newkitten = [nkx,nky,kitten[2]+1,kitten[3]+split_rare,kitten[4],kitten[5],kitten[6],kitten[7],meow ? -2 : kitten[8]];
	ds_queue_enqueue(kittens,newkitten);
	kittens_visited[# newkitten[0],newkitten[1]] = true;
}

var lore_array = mem[step];
var cute_array = item[step];
if dead or too_far {
	if lore_counter < array_length_1d(lore_array) {
		if !lore_done and meow {
			//show_debug_message([dead,kitten,lore_array[lore_counter mod array_length_1d(lore_array)]]);
			loremapp[# kitten[0],kitten[1]] = lore_array[lore_counter mod array_length_1d(lore_array)];
			lore_counter++;
		}
	} else if dead {
		var place = cute_array[item_counter mod array_length_1d(cute_array)];
		if is_array(place) {
			if !item_done {
				itemmapp[# kitten[0],kitten[1]] = 1;
				item_counter++;
			}
		} else {
			if !cute_done {
				cutemapp[# kitten[0],kitten[1]] = object_get_name(place);
				item_counter++;
			}
		}
	}
}

c++;