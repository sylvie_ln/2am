///@param obj
///@param name
///@param desc
///@param cost
///@param threshold
var new = argument[0];
var item = argument[1];
ds_list_copy(new,item);
new[|item_data.cost] = ds_map_create();
ds_map_copy(new[|item_data.cost],item[|item_data.cost]);
ds_list_mark_as_map(new,item_data.cost);