if !play_music
	return 0;
if global.mute { return 0; }
if global.mute_music { return 0; }

if global.radio_bgm != argument[0] and audio_exists(global.radio_bgm) and audio_is_playing(global.radio_bgm) {
	audio_stop_sound(global.bgm);
}
global.radio_bgm = argument[0];
if audio_exists(global.radio_bgm) and !audio_is_playing(global.radio_bgm) {
	global.radio_bgm_instance = audio_play_sound(global.radio_bgm,1000,false);
}