///@param action_name The action name to read input for
///@param *repeat_speed The speed at which the key should repeat
///@param *repeat_delay The delay before the key repeats
if argument_count == 1 {
	return input_read(argument[0],input_read_type.pressed_repeat);
} else {
	return input_read(argument[0],input_read_type.pressed_repeat,argument[1],argument[2]);
}