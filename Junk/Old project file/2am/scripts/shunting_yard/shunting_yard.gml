// Implementation of the "shunting yard algorithm" for  evaluating expressions.
// Input is a tokenized version of the expression, given as a list of pairs 
// of the form [token_string,token_type].
// Token types are defined in the expression_process script.
var precedence;
precedence[cond_tokens.left_paren] = 1;
precedence[cond_tokens.right_paren] = 1;
precedence[cond_tokens.or_op] = 2;
precedence[cond_tokens.xor_op] = 3;
precedence[cond_tokens.and_op] = 4;
precedence[cond_tokens.eq_op] = 5;
precedence[cond_tokens.ne_op] = 5;
precedence[cond_tokens.lt_op] = 6;
precedence[cond_tokens.le_op] = 6;
precedence[cond_tokens.gt_op] = 6;
precedence[cond_tokens.ge_op] = 6;
precedence[cond_tokens.plus_op] = 7;
precedence[cond_tokens.minus_op] = 7;
precedence[cond_tokens.times_op] = 8;
precedence[cond_tokens.divide_op] = 8;
precedence[cond_tokens.minus_unary_op] = 9;
precedence[cond_tokens.not_op] = 9;

var input = argument[0];
var operand_stack = ds_stack_create();
var operator_stack = ds_stack_create();
var previous_token;
previous_token[0] = "";
previous_token[1] = -1;
var unary = false;
for(var i=0; i<ds_list_size(input); i++) {
    var token = input[|i];
    var str = token[0];
    var type = token[1];
    
    var unary = false;
    var previous_type = previous_token[1];
    if previous_type != cond_tokens.variable and previous_type != cond_tokens.constant and previous_type != cond_tokens.string_constant and previous_type != cond_tokens.right_paren {
        unary = true;
    }
    
    //disabled_show_debug_message("Reading token ["+str+","+string(type)+"]");
    switch(type) {
        case cond_tokens.unknown:
            show_error("Unknown token "+str+" encountered in conditional.",true);
        break;
        case cond_tokens.variable:
            var value = variable_get(str);
            //disabled_show_debug_message("Pushing variable "+str+":"+string(value));
            ds_stack_push(operand_stack,value);
        break;
        case cond_tokens.string_constant:
            //disabled_show_debug_message("Pushing constant "+str);
            ds_stack_push(operand_stack,str);
        break;
        case cond_tokens.constant:
            //disabled_show_debug_message("Pushing constant "+str);
            ds_stack_push(operand_stack,real(str));
        break;
        default:
            if type == cond_tokens.left_paren {
                //disabled_show_debug_message("Pushing (");
                ds_stack_push(operator_stack,type);
            } else if type == cond_tokens.not_op or (unary and type == cond_tokens.minus_op) {
                if unary and type == cond_tokens.minus_op {
                    ds_stack_push(operator_stack,cond_tokens.minus_unary_op);
                } else {
                    ds_stack_push(operator_stack,type);
                }
            } else {
                while (type == cond_tokens.right_paren and ds_stack_top(operator_stack) != cond_tokens.left_paren)
                or (type != cond_tokens.right_paren and !ds_stack_empty(operator_stack) and precedence[ds_stack_top(operator_stack)] >= precedence[type]) {
                    var op_type = ds_stack_pop(operator_stack);
                    if op_type == cond_tokens.not_op or op_type == cond_tokens.minus_unary_op {
                        var v2 = 0;
                        var v1 = ds_stack_pop(operand_stack);
                    } else {
                        var v2 = ds_stack_pop(operand_stack);
                        var v1 = ds_stack_pop(operand_stack);
                    }
                    apply_operator(operand_stack,op_type,v1,v2);
                }
                if type == cond_tokens.right_paren {
                    if ds_stack_top(operator_stack) != cond_tokens.left_paren {
                        show_error("Unmatched parentheses in conditional.",true);
                    }
                    ds_stack_pop(operator_stack);
                } else if unary and type == cond_tokens.minus_op {
                    ds_stack_push(operator_stack,cond_tokens.minus_unary_op);
                } else {
                    ds_stack_push(operator_stack,type);
                }
            }
        break;
    }
    previous_token = token;
}
while !ds_stack_empty(operator_stack) {
    var op_type = ds_stack_pop(operator_stack);
    if op_type == cond_tokens.not_op or op_type == cond_tokens.minus_unary_op {
        var v2 = 0;
        var v1 = ds_stack_pop(operand_stack);
    } else {
        var v2 = ds_stack_pop(operand_stack);
        var v1 = ds_stack_pop(operand_stack);
    }
    apply_operator(operand_stack,op_type,v1,v2);
}
var result = ds_stack_top(operand_stack);
ds_stack_destroy(operand_stack);
ds_stack_destroy(operator_stack);
return result;
