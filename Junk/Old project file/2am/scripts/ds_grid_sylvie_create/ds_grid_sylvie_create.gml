var grid = ds_grid_create(argument[0],argument[1]);
if memory_debug {
	global.sylvie_grid_max = max(global.sylvie_grid_max,grid);
	var str = "ALOC "+object_get_name(object_index)+":"+string(id)+" created grid #"+string(grid)+"\nCallstack: "+string(debug_get_callstack());
	if grid_debug and memory_detailed_debug {
		show_debug_message(str);
	}
	global.sylvie_map_of_grids[?grid] = str;
	return grid;
}
return grid;