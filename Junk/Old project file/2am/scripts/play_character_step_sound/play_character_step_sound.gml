var interval = 16;

if (!variable_instance_exists(self, "trip"))
{
	self.trip = 0;
	self.current_trip = 0;
	self.first_step_sound = true;
}
	
if ((self.hv == 0 && self.pegasus_timer == 0) || !self.ong)
{
	self.trip = 0;
	self.current_trip = 0;
	self.first_step_sound = true;
}
else {
	self.trip += abs(self.hv);
	if self.hv == 0 && self.pegasus_timer > 0 {
		self.trip += abs(self.spd+self.pegasus_boost);
	}
}

if (self.trip >= self.current_trip + interval)
{
	play_sound(self.first_step_sound ? sndCharStep1 : sndCharStep2);
	self.current_trip += interval;
	self.first_step_sound = !self.first_step_sound;
}