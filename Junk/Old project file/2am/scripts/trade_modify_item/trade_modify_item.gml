var item = argument[0];
for(var i=1; i<argument_count; i+=2) {
	if argument[i] == item_data.obj {
		item[|argument[i]] = object_get_name(argument[i+1]);	
	} else {
		item[|argument[i]] = argument[i+1];	
	}
	if argument[i] == item_data.cost {
		ds_list_mark_as_map(item,argument[i]);
	}
}