if file_exists(global.keys_save_file) {
	var file = file_text_open_read(global.keys_save_file);
	global.keys_save_map = json_decode(file_text_read_string(file));
	file_text_close(file);
	save_load_impl("load",global.keys_save_map,"keys");
}
