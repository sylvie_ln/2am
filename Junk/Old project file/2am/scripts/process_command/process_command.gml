enum process_command_actions {
    get_name,
    get_args,
    remove
}
var str = argument[0];
var i = argument[1];
var action = argument[2];
var args = ds_list_sylvie_create();

// if i points to the last character, we just have a backslash, not a command
if i == string_length(str) {
    if action == process_command_actions.remove {
        ds_list_sylvie_destroy(args);
        return str;
    } else if action == process_command_actions.get_name {
        ds_list_sylvie_destroy(args);
        return "";
    } else if action == process_command_actions.get_args {
        return args;
    }
    
}
// if the next character is a backslash, we have an escaped backslash
if string_char_at(str,i+1) == "\\" {
    if action == process_command_actions.remove {
        ds_list_sylvie_destroy(args);
        return string_delete(str,i,2);
    } else if action == process_command_actions.get_name {
        ds_list_sylvie_destroy(args);
        return "\\";
    } else if action == process_command_actions.get_args {
        return args;
    }
}
// if the next character is n, we have a newline
if string_char_at(str,i+1) == "n" {
    if action == process_command_actions.remove {
        ds_list_sylvie_destroy(args);
        return string_delete(str,i,2);
    } else if action == process_command_actions.get_name {
        ds_list_sylvie_destroy(args);
        return "n";
    } else if action == process_command_actions.get_args {
        return args;
    }
}


// otherwise, extract the command name
var j = i+1;
while true {
    var c = string_char_at(str,j);
    if ord(c) <= ord(" ") or c == "{" or j == string_length(str)+1 {
        break;
    }
    j++;
}
// i points to the slash at the start of the command
// j now points to the character after the command name
if action == process_command_actions.get_name {
    ds_list_sylvie_destroy(args);
    return string_extract(str,i+1,j-1);
}

// extract the arguments
var k = j-1;
while true {
    var j = k+1;
    var c = string_char_at(str,j);
    // if there are no (more) arguments...
    if c != "{" {
        // j now points to the character after the command+arguments end
        // so k should point to the character before j, the last character of the command
        var k = j-1;
        break;
    }
    
    var match = 1;
    for(var k=j+1; k<=string_length(str); k++) {
        var c = string_char_at(str,k);
        if c == "{" { match++; }
        if c == "}" { match--; }
        if match == 0 { break; }
    }
    if match != 0 {
        show_error("Text command syntax error in "+str,true);
    }
    // now j represents the position of the { and k represents the position of the }
    ds_list_add(args,string_extract(str,j+1,k-1));
}
if action == process_command_actions.get_args {
    return args;
} else if action == process_command_actions.remove {
    ds_list_sylvie_destroy(args);
    return string_delete(text,i,k-i+1);
}

