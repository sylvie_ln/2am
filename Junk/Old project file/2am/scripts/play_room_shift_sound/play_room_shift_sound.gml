// @param shifting

if global.mute_sfx { exit; }
var shifting = argument0;

if (!variable_instance_exists(self, "shifting_prev"))
{
	self.shifting_prev = shifting;
	self.shifting_sound = noone;
}

if (self.shifting_prev != shifting)
{
	if (shifting)
		self.shifting_sound = play_sound(sndShiftRoom);
	else if (self.shifting_sound != noone)
		audio_sound_gain(self.shifting_sound, 0, 100);
	self.shifting_prev = shifting;	
}