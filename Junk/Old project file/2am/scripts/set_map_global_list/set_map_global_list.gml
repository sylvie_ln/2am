var name = argument[0];
var map = argument[1];
var value = variable_global_get(name);
if ds_exists(value,ds_type_list) {
	if ds_map_exists(map,name) {
		ds_map_replace_list(map,name,value);
	} else {
		ds_map_add_list(map,name,value);
	}
} else if value == -1 {
	ds_map_delete(map,name);
}