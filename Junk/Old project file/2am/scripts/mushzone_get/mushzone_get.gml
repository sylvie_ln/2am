var p = global.mushzone_params;
oRoomGen.area = p[? "name"];
oRoomGen.fluffey = p[? "fluffey"];
oRoomGen.tile = asset_get_index(p[? "tile"]);
layer_background_blend(layer_background_get_id("Background"),p[? "bg_color"]);
if sprite_exists(asset_get_index(p[? "bg_overlay"])) {
	layer_background_sprite(layer_background_get_id("Overlay"),asset_get_index(p[? "bg_overlay"]));
}
oRoomGen.rare_shroom = asset_get_index(p[? "rare"]);
oRoomGen.level = p[? "level"];