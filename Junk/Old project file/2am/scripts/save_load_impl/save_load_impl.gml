var map = argument[1];
var type = argument[2]

if argument[0] == "save" {
	global_script = set_map_global;
	list_script = set_map_global_list;
	map_script = set_map_global_map;
	map_grid_script = set_map_global_map_grids;
	key_script = save_keys;
}
if argument[0] == "load" {
	global_script = get_map_global;
	list_script = get_map_global_list;
	map_script = get_map_global_map;
	map_grid_script = get_map_global_map_grids;
	key_script = load_keys;
}

if type == "game" {
	script_execute(map_grid_script,"bag_grid_map",map);
	script_execute(map_script,"mushzone_params",map);;
	script_execute(map_script,"npc_bag_map",map);
	script_execute(map_script,"explored",map);
	script_execute(map_script,"collected",map);
	script_execute(map_script,"shroom_deletion_map",map);
	script_execute(map_script,"shroom_insertion_map",map);
	script_execute(map_script,"memory_dead",map);
	script_execute(map_script,"rare_dead",map);
	script_execute(map_script,"cute_dead",map);
	script_execute(map_script,"lock_flag",map);
	script_execute(list_script,"bag_contents",map);
	script_execute(list_script,"bag_contents_backup",map);
	script_execute(list_script,"memory_disks",map);
	script_execute(global_script,"game_clear",map);
	script_execute(global_script,"eivlys_clear",map);
	script_execute(global_script,"memory_disks_init",map);
	script_execute(global_script,"contract_signed",map);
	script_execute(global_script,"contract_name",map);
	script_execute(global_script,"contract_soul_bond",map)
	script_execute(global_script,"radio_position",map);
	script_execute(global_script,"previous_room",map);
	script_execute(global_script,"current_room",map);
	script_execute(global_script,"warp_x",map);
	script_execute(global_script,"warp_y",map);
	script_execute(global_script,"door_tag",map);
	script_execute(global_script,"bag",map);
	script_execute(global_script,"first_bag",map);
	script_execute(global_script,"disk_num",map);
	script_execute(global_script,"flag_minecart",map); // Safe
	script_execute(global_script,"flag_rainbow",map);  // Safe
	script_execute(global_script,"flag_racerunner",map); // Safe
	script_execute(global_script,"flag_legendary_tree",map); // Safe
	script_execute(global_script,"flag_kittey",map); 
	script_execute(global_script,"flag_cheesey",map); 
	script_execute(global_script,"flag_gap",map); 
	script_execute(global_script,"flag_fishcar",map); 
	script_execute(global_script,"flag_wizardmobile",map); 
	script_execute(global_script,"flag_snowkeety",map); 
	script_execute(global_script,"flag_sylvietruck",map); 
	script_execute(global_script,"flag_house",map); 
	script_execute(global_script,"flag_horse",map); 
	script_execute(global_script,"flag_pinkpaperclip",map); 
	script_execute(global_script,"flag_chicen",map); 
	script_execute(global_script,"flag_puzzler",map); 
	script_execute(global_script,"flag_lies",map); 
	script_execute(global_script,"flag_door_darkness",map);
	script_execute(global_script,"flag_door_light",map);
	script_execute(global_script,"flag_door_tears",map);
	script_execute(global_script,"flag_door_flames",map);
	script_execute(global_script,"flag_door_leafs",map);
	script_execute(global_script,"flag_door_thunder",map);
	script_execute(global_script,"flag_door_lies",map);
	script_execute(global_script,"flag_door_sky",map);
	script_execute(global_script,"flag_door_earth",map);
	script_execute(global_script,"flag_door_storms",map);
	script_execute(global_script,"flag_door_moon",map);
	script_execute(global_script,"flag_door_dreams",map);
	script_execute(global_script,"flag_bridge_canyon",map);
	script_execute(global_script,"flag_bridge_cherry",map);
	script_execute(global_script,"flag_ladder_canyon",map);
	script_execute(global_script,"flag_ladder_cavern",map);
	script_execute(global_script,"flag_tomb",map); // Safe
	script_execute(global_script,"flag_switch_1",map);
	script_execute(global_script,"flag_switch_2",map);
	script_execute(global_script,"flag_switch_3",map);
	script_execute(global_script,"flag_switch_4",map);
	script_execute(global_script,"flag_paperclips",map);
	script_execute(global_script,"flag_dice",map);
	script_execute(global_script,"flag_hacked",map);
	script_execute(global_script,"heist_count",map);
	script_execute(global_script,"heist_stage",map);
	script_execute(global_script,"heist_date",map);
	script_execute(global_script,"heist_date_string",map);
	script_execute(global_script,"heist_item",map);
	script_execute(global_script,"heist_ii",map);
	//script_execute(global_script,"flag_finale",map); // DELIBERATELY NOT SAVED
	script_execute(global_script,"kicks",map);
} else if type == "options" {
	script_execute(global_script,"mute_music",map);
	script_execute(global_script,"mute_sfx",map);
	script_execute(global_script,"scale",map);
	script_execute(global_script,"fullscreen",map);
	script_execute(global_script,"volume",map);
	script_execute(global_script,"gamepad_on",map);
	script_execute(global_script,"gamepad_deadzone",map);
	script_execute(global_script,"assist_air_drain",map);
	script_execute(global_script,"assist_jump_power",map);
	script_execute(global_script,"assist_speed_power",map);
	script_execute(global_script,"assist_gravity",map);
	script_execute(global_script,"assist_gamespeed",map);
} else if type == "keys" {
	script_execute(key_script,map);
}

return map;