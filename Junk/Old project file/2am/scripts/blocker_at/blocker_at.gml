var xp = argument[0];
var yp = argument[1];
ds_list_clear(actors);
var num = instance_place_list(xp,yp,oActor,actors,false);
for(var i=0; i<num; i++) {
	var actor = actors[|i];
	if actor.blocker { return true; }
	if fall_through <= 0 and ((actor.upblocker and bbox_bottom < actor.bbox_top) 
	or (xp-x == 0 and yp-y > 0 and actor.precise_upblocker and position_meeting(x,bbox_bottom+1,actor) and !position_meeting(x,bbox_bottom,actor)))
	{ return true; }
}
return false;