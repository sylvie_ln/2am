var str = argument[0]
var value = argument[1];
var pos = string_pos(".",str);
if pos != 0 {
    var prefix = string_extract(str,1,pos-1);
    var postfix = string_extract(str,pos+1,string_length(str));
    if prefix == "global" {
        variable_global_set(postfix,value);
    } else {
        if ord(string_char_at(prefix,1)) >= ord("0") and ord(string_char_at(prefix,1)) <= ord("9") {
            // first symbol is a digit, assume an instance id was given
            variable_instance_set(real(prefix),postfix,value);
        } else {
            // assume an object name was given
            with asset_get_index(prefix) {
                variable_instance_set(id,postfix,value);
            }
        }
    }
    if is_undefined(value) {
        show_error("Undefined variable "+str+" encountered in expression.",true);
    }
} else {
    value = variable_instance_set(id,str,value);
    if is_undefined(value) {
        show_error("Undefined variable "+str+" encountered in expression.",true);
    }
}
