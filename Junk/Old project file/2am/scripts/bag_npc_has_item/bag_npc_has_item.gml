///@param item
///@param npc
var npc = argument[1];
if !ds_map_exists(global.npc_bag_map,npc.name) {	
	if ds_map_exists(global.trade_map,npc.name) {
		var items = global.trade_map[? npc.name];
		for(var i=0; i<ds_list_size(items); i++) {
			var item = items[|i];
			var item_obj = asset_get_index(item[|item_data.obj]);
			if argument[0] == item_obj {
				return true;	
			}
		}
	}
} else {
	var bag_sticker_list = global.npc_bag_map[? npc.name];
	for(var i=0; i<ds_list_size(bag_sticker_list); i++) {
		var st_info = bag_sticker_list[|i];
		var st_type = asset_get_index(st_info[|npc_bag_sticker_info.type]);
		if st_type == argument[0] { return true; }
	}
}
return false;