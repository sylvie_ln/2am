var cost_map = ds_map_create();
for(var i=0; i<argument_count; i += 2) {
	ds_map_add(cost_map,object_get_name(argument[i]),argument[i+1]);
}
return cost_map;