var map = argument[0];
with oInput {
	for(var a=0; a<ds_list_size(action_list); a++) {
		var act = action_list[|a];
		var list = actions[? act];
		for(var i=0; i<ds_list_size(list); i++) {
			var inp = list[|i];
			var kind = inp[|0];
			var key = inp[|1];
			var axis = 0;
			if kind == input_kind.gamepad_axis {
				axis = inp[|2];	
			}
			if kind == input_kind.gamepad_hat {
				axis = inp[|2];	
			}
			var kind_string = "";
			switch(kind) {
				case input_kind.key: kind_string = "Key"; break;
				case input_kind.gamepad_button: kind_string = "GamepadButton"; break;
				case input_kind.gamepad_axis: kind_string = "GamepadAxis"; break;
				case input_kind.gamepad_hat: kind_string = "GamepadHat"; break;
			}
			variable_global_set("Action_"+act+"_Kind_"+string(i),kind_string);
			variable_global_set("Action_"+act+"_Input_"+string(i),key);
			variable_global_set("Action_"+act+"_Axis_"+string(i),axis);
			set_map_global("Action_"+act+"_Kind_"+string(i),map);
			set_map_global("Action_"+act+"_Input_"+string(i),map);
			set_map_global("Action_"+act+"_Axis_"+string(i),map);
		}
	}
}