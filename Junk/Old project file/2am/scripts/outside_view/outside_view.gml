var inst = id;
if argument_count > 0 { inst = argument[0]; }
var vc = view_camera[0];
if argument_count > 1 { vc = argument[1]; }

return !intersect_rect(inst.bbox_left-1,inst.bbox_top-1,inst.bbox_right+1,inst.bbox_bottom+1,
camera_get_view_x(vc),camera_get_view_y(vc),
camera_get_view_x(vc)+camera_get_view_width(vc)+1,camera_get_view_y(vc)+camera_get_view_height(vc)-20+1)