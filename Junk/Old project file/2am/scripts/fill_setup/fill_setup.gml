fill = 1;
start_xp = 0;
start_yp = 0;
if argument_count > 0 {
	start_xp = argument[0];
	start_yp = argument[1];
}
// make fill grids
if !door_done {
	doormapp = ds_grid_sylvie_create(w,h);
	ds_grid_clear(doormapp,false);
}
if !item_done {
	itemmapp = ds_grid_sylvie_create(w,h);
	ds_grid_clear(itemmapp,0);
}
if !luck_done {
	luckmapp = ds_grid_sylvie_create(w,h);
	ds_grid_clear(luckmapp,0);
}
if !lore_done {
	loremapp = ds_grid_sylvie_create(w,h);
	ds_grid_clear(loremapp,"");
}
if !cute_done {
	cutemapp = ds_grid_sylvie_create(w,h);
	ds_grid_clear(cutemapp,"");
}
lore_counter = 0;
item_counter = 0;
rarity = [0,0,0];
// kittener
kittens = ds_queue_create();
ds_queue_enqueue(kittens,[start_xp,start_yp,0,1,0,0,0,0,-1]);
kittens_visited = ds_grid_sylvie_create(w,h);
ds_grid_clear(kittens_visited,false);
kittens_visited[# start_xp,start_yp] = true;
four = list_create_nosylvie(0,1,2,3);

max_dist = 0;