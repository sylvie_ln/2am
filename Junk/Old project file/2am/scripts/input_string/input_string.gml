var input = argument[0];
var kind = input[|0];
switch(kind) {
	case input_kind.key:
		var key = input[|1];
		return input_key_string(key);
	case input_kind.gamepad_button:
		var button = input[|1];    
		switch(button) {
			case gp_face1:
		        return "Bottom Face Button";
		    case gp_face2:
		        return "Right Face Button";
		    case gp_face3:
		        return "Left Face Button";
		    case gp_face4:
		        return "Top Face Button";
		    case gp_padd:
		        return "DPad Down";
		    case gp_padl:
		        return "DPad Left";
		    case gp_padr:
		        return "DPad Right";
		    case gp_padu:
		        return "DPad Up";
		    case gp_shoulderl:
		        return "L Front";
		    case gp_shoulderlb:
		        return "L Back";
		    case gp_shoulderr:
		        return "R Front";
		    case gp_shoulderrb:
		        return "R Back";
		    case gp_start:
		        return "Start";
		    case gp_select:
		        return "Select";
		    case gp_stickl:
				return "LStick Press";
		    case gp_stickr:
				return "RStick Press";
		    default:
		        return "Unknown";
		}
	case input_kind.gamepad_axis:
		var axis = input[|1];
		var dir = input[|2];
		switch(axis) {
			case gp_axislh:	
				return (dir == 1) ? "LStick Right" : "LStick Left";
			case gp_axisrh:	
				return (dir == 1) ? "RStick Right" : "RStick Left";
			case gp_axislv:	
				return (dir == 1) ? "LStick Down" : "LStick Up";
			case gp_axisrv:	
				return (dir == 1) ? "RStick Down" : "RStick Up";
		    default:
		        return "Unknown";
		}
	break;
	case input_kind.gamepad_hat:
		var dir = input[|2];
		switch(dir) {
			case 1: return "Hat Up"; break;
			case 2: return "Hat Right"; break;
			case 4: return "Hat Left"; break;
			case 8: return "Hat Down"; break;
		}
	break;
	case input_kind.mouse:	
		var button = input[|1];
		switch(button) {
			case mb_left: 
				return "Left Click";
			case mb_right: 
				return "Right Click";
			case mb_middle: 
				return "Middle Click";
		}
	break;
	case input_kind.waiting:
		return "(Press Button)";
	break;
}