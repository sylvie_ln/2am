var size = argument[0];
var slot = ceil(log2(size));
if !surface_exists(global.scratch_surface[slot]) {
    global.scratch_surface[slot] = surface_create(size,size);
}
return global.scratch_surface[slot];