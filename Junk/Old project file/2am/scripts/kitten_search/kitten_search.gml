var initial = argument[0];
var kitten = [initial[0],initial[1]];
var target = argument[1];
//disabled_show_debug_message("target: "+string(target));
if target == 0 { return true; }
var hungry = argument[2];
var align_min = argument[3];
var align_max = argument[4];
var visited = ds_map_create();
var count = 512;
var inner_count = 4;
var found = false;
repeat(count) {
	repeat(inner_count) {
		var dir = sylvie_choose(
			edir.up,
			edir.down,
			edir.left,
			edir.right,
			target,
			target
		);
		if initial[0] == left or initial[0] == left+width-1 {
			if kitten[1] < align_min and dir == edir.up	{ dir = edir.down; }
			if kitten[1] >= align_max and dir == edir.down { dir = edir.up; }
		} else {
			if kitten[0] < align_min and dir == edir.left { dir = edir.right; }
			if kitten[0] >= align_max and dir == edir.right { dir = edir.left; }
		}
		switch(dir) {
			case edir.up:   
				if hungry != -1 or (hungry == -1 and collision_point(kitten[0]*cell_size,(kitten[1]-1)*cell_size,oBlonck,false,true) == noone) {
					kitten[1] -= 1; 
				}
			break;
			case edir.down:  
				if hungry != -1 or (hungry == -1 and collision_point(kitten[0]*cell_size,(kitten[1]+1)*cell_size,oBlonck,false,true) == noone) {
					kitten[1] += 1; 
				}
			break;
			case edir.left:	 
				if hungry != -1 or (hungry == -1 and collision_point((kitten[0]-1)*cell_size,kitten[1]*cell_size,oBlonck,false,true) == noone) {
					kitten[0] -= 1; 
				}
			break;
			case edir.right: 
				if hungry != -1 or (hungry == -1 and collision_point((kitten[0]+1)*cell_size,kitten[1]*cell_size,oBlonck,false,true) == noone) {
					kitten[0] += 1; 
				}
			break;
		}
		if initial[0] == left or initial[0] == left+width-1 {
			kitten[0] = clamp(kitten[0],left,left+width-1);
			kitten[1] = clamp(kitten[1],top+1,top+height-2);
			if initial[0] == left and kitten[0] == left+width-1 and kitten[1] != clamp(kitten[1],align_min,align_max-1) {
				kitten[0] = left+width-2;
			}
			if initial[0] == left+width-1 and kitten[0] == left and kitten[1] != clamp(kitten[1],align_min,align_max-1) {
				kitten[0] = left+1;
			}
		} else {
			kitten[0] = clamp(kitten[0],left+1,left+width-2);
			kitten[1] = clamp(kitten[1],top,top+height-1);
			if initial[1] == top and kitten[1] == top+height-1 and kitten[0] != clamp(kitten[0],align_min,align_max-1) {
				kitten[0] = top+height-2;
			}
			if initial[1] == top+height-1 and kitten[1] == top and kitten[0] != clamp(kitten[0],align_min,align_max-1) {
				kitten[0] = top+1;
			}
		}
		if !ds_map_exists(visited,pos_key(kitten[0],kitten[1])) {
			visited[?pos_key(kitten[0],kitten[1])] = [kitten[0],kitten[1]];
			break;
		}
	}
	//disabled_show_debug_message("kitten: "+string(kitten[0])+" "+string(kitten[1]));
	switch(target) {
		case edir.up:	 if kitten[1] <= top and kitten[0] >= align_min and kitten[0] < align_max { found = true; } break;	
		case edir.down:  if kitten[1] >= top+height-1 and kitten[0] >= align_min and kitten[0] < align_max { found = true; } break;	
		case edir.left:  if kitten[0] <= left and kitten[1] >= align_min and kitten[1] < align_max { found = true; } break;	
		case edir.right: if kitten[0] >= left+width-1 and kitten[1] >= align_min and kitten[1] < align_max { found = true; } break;	
	}
	if found { break; }
}
if found and hungry == 1 {
	for(var k=ds_map_find_first(visited); !is_undefined(k); k=ds_map_find_next(visited,k)) {
		var pos = visited[? k];
		//disabled_show_debug_message("eating: "+string(pos[0])+" "+string(pos[1]));
		with collision_point(pos[0]*cell_size,pos[1]*cell_size,oBlonck,false,true) {
			instance_destroy();	
		}
	}
}
ds_map_destroy(visited);
return found;