if file_exists(global.game_save_file) {
	//var file = file_text_open_read(global.game_save_file);
	//global.game_save_map = json_decode(file_text_read_string(file));
	//file_text_close(file);
	var buffer = buffer_load(global.game_save_file);
	var decompressed = buffer_decompress(buffer);
	global.game_save_map = json_decode(buffer_read(decompressed,buffer_string));
	buffer_delete(buffer);
	buffer_delete(decompressed);
	save_load_impl("load",global.game_save_map,"game");
}
