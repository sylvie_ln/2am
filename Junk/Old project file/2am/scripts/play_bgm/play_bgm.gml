if !play_music
	return 0;
if global.mute { return 0; }
if global.mute_music { return 0; }

if global.bgm != argument[0] and audio_exists(global.bgm) and audio_is_playing(global.bgm) {
	audio_stop_sound(global.bgm);
}
global.bgm = argument[0];
if audio_exists(global.bgm) and !audio_is_playing(global.bgm) {
	global.bgm_instance = audio_play_sound(global.bgm,1000,room == rmEnding ? false : true);
}