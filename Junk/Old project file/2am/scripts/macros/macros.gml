// Enables the Voice of Eivlys (level editor) 
// Or not, because I cut that. Just leave this on, okay?
#macro her_voice 1

#macro play_music 1

// Show that classic load bar. Slows down loading time.
// However, if you turn it off, then you'll get lag spikes 
// shortly after starting the game because not all zone types
// have background loading implemented properly.
#macro classic_load_bar 1

// Disable and Mushroom Zones won't be generated at all.
// Fast startup time but the game will crash (?) if you enter a zone.
#macro load_mush_zones 1

// Hold shift to spam mouse clicks during trades. 
// Maybe useful for looking for or replicating bugs that require frame perfect clicks.
#macro mouse_spam 0

// Displays some variables on the screen during trading.
#macro trade_debug 0

// Displays a bunch of logging information about memory allocation, 
// probably not useful unless your Sylvie.
#macro memory_debug 0
#macro list_debug 0
#macro grid_debug 0
#macro memory_detailed_debug 0
