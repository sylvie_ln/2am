///@param id
///@param item
var set = argument[0];
var items = set[|0];
var contains = set[|1];
var item = argument[1];
var in_set = false;
for(var i=0; i<ds_list_size(items); i++) {
	if item == items[|i] {
		in_set = true;
		ds_list_delete(items,i);
		break;
	}
}
if in_set {
	ds_map_delete(contains,item);
	return true;
}
return false;