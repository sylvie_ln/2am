with oChar
{
	if room == rmMushZone {
		if bag_has_item(oMemoryDisk) {
			global.warp_x = last_door_x;
			global.warp_y = last_door_y;
		} else {
			global.warp_x = oRoomGen.startx;
			global.warp_y = oRoomGen.starty;
		}
	}
}
room_restart();