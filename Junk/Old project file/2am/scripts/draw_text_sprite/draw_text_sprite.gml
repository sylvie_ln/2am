///@param x The x position.
///@param y The y position.
///@param text The text.
// Draws text with mid-text sprites. 
// Limitations: only works with left halign, doesn't do linebreaks properly, no animation.
// Type \sprite:i to draw subimage i of sprite.
sprite_text_impl(argument[0],argument[1],argument[2],sprite_text_modes.draw);