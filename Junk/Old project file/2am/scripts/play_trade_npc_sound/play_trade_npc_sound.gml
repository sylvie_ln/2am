if !variable_instance_exists(self, "npc") or !instance_exists(npc) or !variable_instance_exists(npc, "text")
	return;
	
if variable_instance_exists(self, "_npc_text_prev")
{
	if _npc_text_prev != npc.text
		play_sound(sndDialogSelect);
}

_npc_text_prev = npc.text;