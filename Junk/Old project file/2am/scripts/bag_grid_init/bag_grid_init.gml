///@param name
var name = argument[0];
if ds_map_exists(global.bag_grid_map,name) { exit; }
var cell = global.bag_grid_cell_size;
var w = sprite_get_width(sNPCBag) div cell;
var h = sprite_get_height(sNPCBag) div cell;
var grid = ds_grid_sylvie_create(w,h);
ds_map_add(global.bag_grid_map,name,grid);
var npc_bag, npc_bag_int;
with oBag {
	if type == 1 { npc_bag = self; }	
}
with oBagInterior {
	if type == 1 { npc_bag_int = self; }	
}
var left = npc_bag.x - npc_bag.sprite_xoffset;
var top = npc_bag.y - npc_bag.sprite_yoffset;
for(var i=0; i<w*cell; i+=cell) {
	for(var j=0; j<h*cell; j+=cell) {
		if  collision_rectangle(left+i,top+j,left+i+cell,top+j+cell,npc_bag,true,false) == noone
		and 
		collision_rectangle(left+i,top+j,left+i+cell,top+j+cell,npc_bag_int,true,false) == npc_bag_int {
			grid[# i div cell, j div cell] = true;
		} else {
			grid[# i div cell, j div cell] = false;	
		}
	}
}

/*var grid_copy = ds_grid_sylvie_create(w,h);
ds_grid_copy(grid_copy,grid);
ds_map_add(global.initial_bag_grid_map,name,grid_copy);


/*
//disabled_show_debug_message(sprite_get_name(npc_bag.sprite_index));
for(var j=0; j<h; j++) {
	var str = "";
	for(var i=0; i<w; i++) {
		str += string(grid[# i,j]);
	}
	//disabled_show_debug_message(str);
}