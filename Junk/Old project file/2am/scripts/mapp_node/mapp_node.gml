///@param x
///@param y
///@param data
return [argument[0],argument[1],argument[2]];

enum ninfo {
	xp,yp,data	
}
/*
node[?"x"] = argument[0];
node[?"y"] = argument[1];
node[?"data"] = argument[2];
/*
//disabled_show_debug_message("NODE("+string(argument[0])+","+string(argument[1])+")");
var grid_str = "";
for(var j=0; j<ds_grid_height(argument[2]); j++) {
	for(var i=0; i<ds_grid_width(argument[2]); i++) {
		var g = argument[2];
		grid_str += ((g[# i,j] < 10) ? "0" : "") + string(g[# i,j]);
	}
	grid_str += "\n";
}
//disabled_show_debug_message(grid_str);
*/
//return node;