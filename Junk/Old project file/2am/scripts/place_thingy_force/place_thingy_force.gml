if argument[0] == noone { return noone; }
var hpos = 10+20;
var yoff = -8;
var spr = object_get_sprite(argument[0]);
if sprite_exists(spr) {
	yoff = sprite_get_yoffset(spr)-sprite_get_bbox_bottom(spr)-1;
}
var vpos = 160-10-20;
var vplace = 160-20+yoff;
while(true) {
	var blonck = collision_rectangle(hpos-8,vpos+8,hpos+7,vpos+9,oBlonck,false,true);
	if blonck != noone {
		var blonck = collision_rectangle(hpos-4,vpos-4,hpos+3,vpos+3,oBlonck,false,true);
		with blonck { 
			for(var i=0; i<ds_list_size(other.valid_blocks); i++) {
				if other.valid_blocks[|i] == id {
					ds_list_delete(other.valid_blocks,i);
					break;
				}
			}
			instance_destroy(); 
		}	
		var inst = instance_create_depth(hpos,vplace,depth,argument[0]);
		inst.generated = true;
		return inst;
	}
	hpos += 20;
	if hpos > global.view_width {
		if oRoomGen.xp == 0 and oRoomGen.yp == 0 {
			show_error("Horrible error.",true);
		}
		return noone;
	}
}