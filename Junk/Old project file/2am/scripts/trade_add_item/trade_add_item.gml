///@param obj
///@param name
///@param desc
///@param cost
///@param threshold
var obj = object_get_name(argument[0]);
if object_index == oInTheBag {
	var name = npc.name;
} else {
	var name = self.name;
}
if !ds_map_exists(global.trade_map,name) {
	trade_init();	
}
var item_list = global.trade_map[? name];
var item = -1;
if argument_count == 1 {
	var item = ds_list_sylvie_create();
	//disabled_show_debug_message(name+" "+object_get_name(object_index)+" "+obj+":"+string(id));
	trade_copy_item(item,global.item_map[? obj]);	
	ds_list_add(item_list,item);
} else if argument_count == 2 {
	var item = ds_list_sylvie_create();
	trade_copy_item(item,global.item_map[? obj]);	
	item[|item_data.desc] = argument[1];
	ds_list_add(item_list,item);
} else {
	var item = ds_list_sylvie_create();
	for(var i=0; i<item_data.last; i++) {
		if i == item_data.obj {
			ds_list_add(item,obj);
		} else {
			ds_list_add(item,argument[i]);
		}
		if i == item_data.cost {
			ds_list_mark_as_map(item,ds_list_size(item)-1);
		}
	}	
	item[|item_data.restock] = 0;
	ds_list_add(item_list,item);
	
}
if item != -1 {
	ds_list_mark_as_list(item_list,ds_list_size(item_list)-1);
}
if ds_list_size(item_list) == 1 {
	if object_index == oInTheBag {
		npc.first_item = item;	
	} else {
		first_item = item;
	}
}
return item;