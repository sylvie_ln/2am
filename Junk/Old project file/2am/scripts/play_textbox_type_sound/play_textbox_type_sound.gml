// @param npc

var npc = argument0;

if (!variable_instance_exists(self, "tick"))
	self.tick = 0;
	
self.tick = (self.tick + 1) % 2;

var sound = sndNormalNpcSpeak;
if (npc.object_index == oDoor)
	sound = sndDoorSpeak;
if (npc.object_index == oChicen)
	sound = sndSpeakChicken;
if (npc.object_index == oBunney)
	sound = sndSpeakRabbit;
if (npc.object_index == oCloudy)
	sound = sndSpeakThunderGod;

if (self.tick == 0)
	play_sound(sound);