// @param vspeed

if global.mute_sfx { exit; }
var vspeed_factor = abs(argument0);

var s = play_sound(sndCharLand);
audio_sound_gain(s, min(1, 0.2 + max(vspeed_factor - 2, 0) * 0.15), 0);
audio_sound_pitch(s, 1 + 2 / max(1, vspeed_factor));