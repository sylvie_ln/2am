if !dead {
	if argument[0] != self {
		var spike = argument[0];
		var dir = point_direction(spike.x,spike.y,x,y);
		hv = lengthdir_x(spd*2,dir);
		vv = lengthdir_y(spd,dir);
	}
	dead = true;
	dead_stage = -1;
	image_index = 0;
	alarm[aChar.force_respawn] = max_respawn_time;
	play_sound(sndCharDie);
}