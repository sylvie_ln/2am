if memory_debug {
	if list_debug {
		var map = global.sylvie_map_of_lists;
		for(var i=0; i<=global.sylvie_max; i++) {
			if ds_map_exists(map,i) {
				if !ds_exists(i,ds_type_list) {
					// This funny message "DESTROYED AT A TIME!" is a reference to Raiden Fighters or something.
					// Its a message that appears when you destroy a lot at a time and I thought it was funny
					show_debug_message("DESTROYED AT A TIME! "+map[?i]);
					ds_map_delete(map,i);
				} else {
					show_debug_message("STILL "+map[?i]);
				}
			}
		}
	}
	if grid_debug {
		var map = global.sylvie_map_of_grids;
		for(var i=0; i<=global.sylvie_grid_max; i++) {
			if ds_map_exists(map,i) {
				if !ds_exists(i,ds_type_grid) {
					// This funny message "DESTROYED AT A TIME!" is a reference to Raiden Fighters or something.
					// Its a message that appears when you destroy a lot at a time and I thought it was funny
					show_debug_message("DESTROYED AT A TIME! "+map[?i]);
					ds_map_delete(map,i);
				} else {
					show_debug_message("STILL "+map[?i]);
				}
			}
		}
	}
}