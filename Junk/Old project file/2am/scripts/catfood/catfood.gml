enum edir {
	up = 1,
	down = 2,
	left = 4,
	right = 8,
}

enum catfood_style {
	screen,
	platform
}

//cell_size = 20;
left = argument[1];
top = argument[2];
width = argument[3];
height = argument[4];
decay = argument[5];
current_decay = decay;
block_count = 0;

// borders
border_type = argument[0];
border_position = array_create(9,-1);
border_opening = array_create(9,-1);

for(var i=1; i<=8; i*=2) {
	if i & border_type == 0 {
		construct_wall(i);	
	} else {
		construct_entry(i);	
	}
}

if argument_count > 6 {
	var boar = argument[6];
	for(var i=1; i<=8; i*=2) {
		var bp = boar[@0];
		var bo = boar[@1];
		if bp[@i] != -1 {
			border_position[i] = bp[@i];	
		}
		if bo[@i] != -1 {
			border_opening[i] = bo[@i];	
		}
	}
}

//disabled_show_debug_message(border_position);
//disabled_show_debug_message(border_opening);

construct_border(border_type);	

//if border_type == 0 { exit; }

// corners
var corners = [edir.up + edir.left, 
			   edir.up + edir.right, 
			   edir.down + edir.left, 
			   edir.down + edir.right];
for(var i=0; i<array_length_1d(corners); i++) {
	var corner = corners[i];
	construct_corner(corner);
}


var dirs = [edir.up,edir.down,edir.left,edir.right];
for(var i=0; i<array_length_1d(dirs); i++) {
	for(var j=i+1; j<array_length_1d(dirs); j++) {
		var di = dirs[i];
		var dj = dirs[j];
		if border_type & di+dj == di+dj {
			kitten2(di,dj);
		}
	}	
}


/*
//disabled_show_debug_message(border_type);
var dirs = [edir.up,edir.down,edir.left,edir.right];
var initials = [
-1,
[left+border_position[edir.up],top],
[left+border_position[edir.down],top+height-1],
-1,
[left,top+border_position[edir.left]],
-1,
-1,
-1,
[left+width-1,top+border_position[edir.right]],
];
for(var ii=0; ii<array_length_1d(dirs); ii++) {
	for(var jj=0; jj<array_length_1d(dirs); jj++) {
		if ii == jj { continue; }
		var i = dirs[ii];
		var j = dirs[jj];
		if border_type & i {
			//disabled_show_debug_message("kitty on the run");
			if !kitten_search(initials[i],border_type & j,-1,border_position[j],border_position[j]+border_opening[j]) {	
				var seed = random_get_seed();
				//disabled_show_debug_message("kitty couldn't find a way!");
				if kitten_search(initials[i],border_type & j,0,border_position[j],border_position[j]+border_opening[j]) {
					sylvie_set_seed(seed);	
					//disabled_show_debug_message("kitty is hungrey");
					kitten_search(initials[i],border_type & j,1,border_position[j],border_position[j]+border_opening[j]);
				}
			}
		}
	}
}

// platforms?

// tunnels?

/*
var spikey = sylvie_random(0,16);
for(i=1; i<=spikey; i++) {
	var xs = (left*16)+sylvie_random(3,width-4)*16+8;
	var ys = (top*16)+sylvie_random(3,height-4)*16+8;
	if collision_point(xs,ys,oThingy,false,true) == noone {
		instance_create_depth(xs,ys,depth,oSpike);
	}
}
*/

/*
var star_counter = 0;
while(true) {
	var xs = (left*cell_size)+sylvie_random(1,width-2)*cell_size+(cell_size div 2);
	var ys = (top*cell_size)+sylvie_random(1,height-2)*cell_size+(cell_size div 2);
	if collision_point(xs,ys,oThingy,false,true) == noone {
		instance_create_depth(xs,ys,depth,oStar);
		break;
	}
	star_counter++;
	if star_counter > 128 {
		break;	
	}
}

if ds_map_exists(global.block_map,room_key(xp,yp)) {
	var block_list = global.block_map[? room_key(xp,yp)];
	for(var i=0; i<ds_list_size(block_list); i++) {
		var pos = block_list[|i];
		with collision_point(left*cell_size+pos[0],top*cell_size+pos[1],oBlonck,false,true) {
			instance_destroy();	
		}
	}
}
*/

//return recompute_borders(left,top,width,height);