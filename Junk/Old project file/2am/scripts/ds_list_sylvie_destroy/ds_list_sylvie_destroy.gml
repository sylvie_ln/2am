var list = argument[0];
if memory_debug {
	var str = "FREE "+object_get_name(object_index)+":"+string(id)+" destroyed list #"+string(list)+"\nCallstack: "+string(debug_get_callstack());
	if list_debug and memory_detailed_debug {
		show_debug_message(str);
	}
	ds_map_delete(global.sylvie_map_of_lists,list);
}
ds_list_destroy(list);