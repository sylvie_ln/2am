script=@"I'm the Guardian of this door. But I've never been in the door, don't ask me about it.
It's tough guarding all the time. I want to go the Citey and buy popular items, but I have to stand here...."

name="Arianne"

trade_add_item(oGuardianScimitar);
trade_add_item(oGuardianShield);
trade_add_item(oGuardianShield,"Shield 2","This is my backup shield",
trade_cost(noone,0),99);


// PRE TRADE
// When you first open the trade screen.
orig_text = "I am a Guardian, one who guards the area. I like popular items, but I can't move from here....";
// When you select an item other than the selected one.
finish_text = "Oh, there's something else you want?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back...."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't want that item...."
// When you offer a low-rank item.
low_text = "Oh, that looks a little popular."
// When you offer a mid-rank item.
medium_text = "Now that's somewhat popular."
// When you offer a high-rank item.
high_text = "Oh yes, it's the extremely popular JIGGLER!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I can't handle that much popular items!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Hmm, it's not popular enough...."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Maybe a little more popular...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I don't want some of those things...."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I don't know about this offer...."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Oh no, I won't trade it...."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "I'm not so interested...."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Yes, I will finally have popular items!"
// When you take your item back after the NPC accepts your offer.
back_text = "Backing out? :("
// When you offer more stuff after the NPC accepts your offer.
more_text = "Will you give me even more items?!"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Um.... it's too late to back out...."
// When the NPC begins taking you items.
thank_text = "Thanks so much for getting these for me!"
// When a trade ends and you didn't attempt to steal.
complete_text = "Hooray!"
// When a trade ends and you attempted to steal.
complete_steal_text = "I'm a Guardian, but I allowed a thief to trick me. How shameful!"

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Please don't steal..."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Wow."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you...."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, please give back it."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You keep that, and give back the items you promised...."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "That isn't one of the items you promised."