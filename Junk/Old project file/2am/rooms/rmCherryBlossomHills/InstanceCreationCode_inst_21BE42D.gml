script=@"So I guess you're wondering about me and my small buisness.
I run a store called the Kittenarium, it's the only place in the world to get Kittens.
!if global.flag_snowkeety==0
 And I am a big fan of Wizards. If you had a wizard like item, maybe I would trade my vehicle for it.
!fi"
name="Katey"

trade_add_item(oSnowKeety);
trade_add_item(oKittyShroom,"Persian","A prestigious longhair breed, rather high maintenance. 55 SM.",
trade_standard_costs(),55);
trade_add_item(oKittyShroom,"Sphynx","The famous hairless cat, needs frequent bathing. 48 SM.",
trade_standard_costs(),48);
trade_add_item(oKittyShroom,"Russian Blue","This silvery blue coat is quite striking. 47 SM.",
trade_standard_costs(),47);
trade_add_item(oKittyShroom,"Scottish Fold","Known for the unique folded ears and gentleness. 46 SM.",
trade_standard_costs(),46);
trade_add_item(oKittyShroom,"Savannah","A rare and exotic crossbreed, very active. 60 SM.",
trade_standard_costs(),60);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Welcome to the Kittenarium, don't you want a lovely Kittey?";
// When you select an item other than the selected one.
finish_text = "You want a different item?"
// When you deselect an item.
dont_text = "Oh, don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I shall not trade this back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "No thanks."
// When you offer a low-rank item.
low_text = "I'll take it, but I'd really prefer you pay with TinyShrooms."
// When you offer a mid-rank item.
medium_text = "Ugh, these take up so much space. I'll accept it, but don't you have TinyShrooms?"
// When you offer a high-rank item.
high_text = "Yes, excellent."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Your offer is a little too high. Calm down."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "That's not quite enough."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Just a little more."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Please just pay with normal SM currency."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Please just pay with normal SM currency."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Holy kitten, that's such a wizard item."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "That doesn't look wizard at all."
// When you offer more than one of the exact item.
multiple_exact_text = "A single wizard's all I need in life."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "I want that wizard. The other stuff, not so much."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I won't trade this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Not interested in that."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "Not wizard, not worth it."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Great, now this will be a good trade."
// When you take your item back after the NPC accepts your offer.
back_text = "Huh, getting cold feet?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Wow, expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late for a refund...."
// When the NPC begins taking you items.
thank_text = "Pleasure doing buisness with you."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks, come by again someday."
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks, come by again someday, if you stop being a theif."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Don't you steal."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "You've committed the hated act of theivery."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thief and loser!"
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, please right now give back that item you promised."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep it. Give back what was promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "That's another item from the items you promised."