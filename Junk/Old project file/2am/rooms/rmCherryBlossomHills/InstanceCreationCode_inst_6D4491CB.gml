script=@"I'm a character with hints.
Would you like to learn some hints? \ask{Yes}{No}
!if answer==1
 Welcome to the Cherry Blossom Hills. It's a lovely place to start your adventure.
 Down in the cave below you'll find a Mushroom Zone. It's a dangerous place, but you can find TallShrooms in there.
 If you explore some more, you'll meet lots of interesting characters, and you might see a few places you can't seem to reach. Say, the ledge to the left.
 Often you can reach these areas by throwing items from your bag. You can ride on top of thrown items to reach new places!
 There's usually a more convenient way to reach these areas than riding items like this, but it might be obscure to discover.
 That's all for now. I have some more specific hints, but they cost mony.
!fi"
name="Cherie"

trade_add_item(oHint,"Overpriced Kitteys?","This hint costs 1 SM.",
trade_standard_costs(),1);
trade_add_item(oHint,"Where's the FloatShroom?","This hint costs 3 SM.",
trade_standard_costs(),3);
trade_add_item(oHint,"The Legendary Tree?","This hint costs 5 SM.",
trade_standard_costs(),5);
trade_add_item(oHint,"A Stormy Demeanor?","This hint costs 7 SM.",
trade_standard_costs(),7);
trade_add_item(oHint,"That Shady Individual?","This hint costs 9 SM.",
trade_standard_costs(),9);

// PRE TRADE
// When you first open the trade screen.
orig_text = "These hints might be a little helpful.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different hint?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Please use standard SM currency to purchase hints."
// When you offer a low-rank item.
low_text = "Thanks, a basic item."
// When you offer a mid-rank item.
medium_text = "Thanks, a slightly large item."
// When you offer a high-rank item.
high_text = "Thanks, a tiny little item."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "You're overpaying a little, take some of the stuff out."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "That's not enough mony."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "You need a little more mony."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Please use standard SM currency to purchase hints."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Please only use standard SM currency to purchase hints."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I am not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Sorry, not trading for that."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "Please use standard SM currency to purchase hints."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Cool! I hope you find the hint helpful on your quest."
// When you take your item back after the NPC accepts your offer.
back_text = "Backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Offering more stuff?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Can't stop the trade now...."
// When the NPC begins taking you items.
thank_text = "I'll take your items."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks, see you later!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Bye."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Trying to steal? Interesting."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Hmph."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "What a waste of time."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of my stuff."
// When you pick up an item that you stole.
yes_give_text = "Yeah, give it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep it. Just give back my stuff."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No, that's not one of the items you promised...."