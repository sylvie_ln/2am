script=@"The Cherry Blossom Hills. A famous place home to things like animals and the Laboratorey.
Once time is flowing again, I was thinking of doing some developments here, maybe building a suburb. 
But nobody I talked to seems to know what a House is...."
name = "Lamia"

trade_add_item(oHouse);

// PRE TRADE
// When you first open the trade screen.


// Alt text version!
orig_alt_text = "I'm known as the House builder. Thanks for the Truck, it's so wonderful.";
if global.flag_house == 0 {
	orig_text = "I'm known as the House builder. I'm looking for a Truck, I need it for my Small Business.";
} else {
	orig_text = orig_alt_text;
}
// For alt text, add a hook in oInTheBag as well.

// When you select an item other than the selected one.
finish_text = "You want a different item?"
// When you deselect an item.
dont_text = "You don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I will not trade it back."
// Prefix before item name
thatsmy_text = "This is a "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Ah, that Sylvie-Truck looks perfect for my entrepeneurial schemes."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "Nothing but a truck will satisfy me."
// When you offer more than one of the exact item.
multiple_exact_text = "I don't want multiple trucks, just exactly one truck."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "I'm not interested in anything that isn't a truck."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "This isn't for trade."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "You need to learn the ways of business negotiations. This is not a good offer."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "Don't have a truck? You're out of luck."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Pleasure doing buisness with you."
// When you take your item back after the NPC accepts your offer.
back_text = "Hm? Are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "The trade is set, no going back now."
// When the NPC begins taking you items.
thank_text = "I'll be taking this."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks for the business!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Get out of my sight."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "What are you doing?"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "This is not business-like conduct. Put it back immediately."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "How rude."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, please give back this item you promised."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep it, but give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that. Give me the items you promised."