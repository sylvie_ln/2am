script=@"Hello, this is the lovely Cherry Blossom Hills. Let me give you a information. \ask{Yes}{No}
!if answer==1
 There are many Cherry Blossom Trees in this area, including the Legendary Tree.
 And also, I am the Bunney Keeper who takes care of all the Bunneys. 
 Do you want more information about Bunneys? \ask{Yes}{No}
 !if answer==1
  It's said that Bunneys should eat Carrots, but it's actually bad for their health to eat too much.
  It's better for them to eat Swords, which are long and pointy like a Carrot, but have so much more Vitamins and Minerals.
  I am looking for more Swords to feed my Bunneys. Do you have one?
 !else
  See you later, have a good time in the Cherry Blossom Hills.
 !fi
!else
 There are many things to see in the Cherry Blossom Hills, please stay around longingly.
!fi"

name="Sweets"

trade_add_item(oBunneyAmulet);
trade_add_item(oMagicCarrot);
trade_add_item(oBunneyEars);
trade_add_item(oBunneyPin);
trade_add_item(oBunneySlippers);
trade_add_item(oMiniBunney);
// PRE TRADE
// When you first open the trade screen.
orig_text = "I have many items related to bunneys, won't you like them?";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different kind?"
// When you deselect an item.
dont_text = "Oh, you don't want this kind?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back...."
// Prefix before item name
thatsmy_text = "It's "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Oh, I'm looking for a different kind of item here...."
// When you offer a low-rank item.
low_text = "Basic shrooms, they are known for their basic qualities."
// When you offer a mid-rank item.
medium_text = "Thanks, this is a good materials."
// When you offer a high-rank item.
high_text = "Wow, that looks quite amazing!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't need so many items...."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need a little more materials...."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I need a little more materials...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Some of this stuff isn't really what I want.... can you remove them?"
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "This deal isn't ideal.... hehe."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Woow!! That sword looks so nutritious!!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "That does not look like a sword."
// When you offer more than one of the exact item.
multiple_exact_text = "I'll just take one sword, I don't deserve so much valuable treasures..."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "It's okay, I only want the sword and not other things...."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I can't trade it...."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "That items isn't really what I want for this...."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "I will only trade this for a sword, preferably one that's legendary and rare."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Great, let's trade!"
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, what's wrong?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Whoa, offering more?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Um.... it's a little too late to change your mind...."
// When the NPC begins taking you items.
thank_text = "I will take these now!"
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks so much, have a great day."
// When a trade ends and you attempted to steal.
complete_steal_text = "Bye, have a bad day."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, that isn't wonderful of you."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "I don't like what you have just done."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you for returning."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some promised items."
// When you pick up an item that you stole.
yes_give_text = "You should give that back...."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "It's too late to back out, please just give the promised items!"
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "That was not one of the promised items, okay?"