script=@"
!if global.flag_chicen==1
 Congratulations, you can fly freely like a bird now. Specifically a chicen bird.
!elif 1
 Yeah, you could call me one of the animals of the hills.
 I'm a chicen with lovely wings. They let me soar through the sunny sky.
 If you have a paperclip, I'll grant you this power of flight yourself. But only if it's not a red paperclip. It has to be another kind of paperclip.
!fi"
name="Chicen"

trade_add_item(oChickenWings);
trade_add_lootbox(oFloatShroom,3,5); // 2:16
trade_add_lootbox(oFloatShroom,3,5); // 2:16

// PRE TRADE
// When you first open the trade screen.
orig_text = "Cluck cluck, I'm a little lovely chicken.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I shan't trade it back."
// Prefix before item name
thatsmy_text = "The "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "No, I'm looking for a floating shroom item here."
// When you offer a high-rank item.
high_text = "Yeah, that's the right item that I need so much."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't want so many of them."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need more than that."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Maybe a little more...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "No, only the floating shrooms, not other stuff."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "No, only the floating shrooms, not other stuff."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Now that's a stylish paperclip that I can approve of."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "There is one, and only one thing I want, and it's a paperclip that isn't red."
// When you offer more than one of the exact item.
multiple_exact_text = "Wow, you got two of those? I only need one."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Please remove the unnecessary items. I just want a non-red paperclip."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Cluck cluck, I'm not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Such an offer, any chicen would decline."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "That's something that isn't what I desire."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Excellent, you're receiving an item from a chicen."
// When you take your item back after the NPC accepts your offer.
back_text = "Are you chickening out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late to chicken out."
// When the NPC begins taking you items.
thank_text = "I'll take these, cluck."
// When a trade ends and you didn't attempt to steal.
complete_text = "Congratulations!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Congratulations."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Cluck off, I'm just a chicken."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "I'm just a chicken and you steal from me."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "I'm only a chicken."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of those great items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, return this."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You must keep this, just return the promised items to me."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No, not that, the original promised items."