script=@"Welcome to my evil Laboratorey, this is where I create my inventions.
Right now I have various inventions to create, can you help me?"
name="Laborie"

trade_recet_fluffy(name);
var disk = trade_add_item(oMemoryDisk);
if disk[|item_data.restock] == 0 {
	disk[|item_data.restock] = 1;
}
trade_add_item(oMemoryDiskManual);
trade_add_item(oMappingImplant);
trade_add_item(oUpgradeChip);
trade_add_item(oJetpack);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Wahaha, these are my evil and brilliant inventions.";
// When you select an item other than the selected one.
finish_text = "Ah, another invention has caught your eye?!"
// When you deselect an item.
dont_text = "You don't want it?!"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade it back, gaha."
// Prefix before item name
thatsmy_text = "That's my "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "That is not what I seek!"
// When you offer a low-rank item.
low_text = "Yes, hehe."
// When you offer a mid-rank item.
medium_text = "Yes, wahaha."
// When you offer a high-rank item.
high_text = "Yes! Ha ha ha! Yes!!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Gahu! This is far more than necessary!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Not enough, guheheh."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Just a bit more.... wuhuhu."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Good, but you have some unnecessary items, hehe."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "That's not enough.... and remove unneeded items too."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "This is not for trade, waha."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "I won't accept such an offer!!"
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Kyahaha! My evil inventions will take over the world."
// When you take your item back after the NPC accepts your offer.
back_text = "Wehehe, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your evil offer??"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Guhaha, you can't back out of the trade now...."
// When the NPC begins taking you items.
thank_text = "Waheha!!!"
// When a trade ends and you didn't attempt to steal.
complete_text = "Excellent, my laboratorey grows ever stronger."
// When a trade ends and you attempted to steal.
complete_steal_text = "Excellent, my laboratorey grows ever stronger."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Screech, you are stealing! Sound the alarm!"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "I will destroy you."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Your destruction is averted for now."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "Fool, you must give back all items."
// When you pick up an item that you stole.
yes_give_text = "Yes, put that back or face certain death."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "I do not want it back, I want the promised-items."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "You are nothing but pathetic, give back the promised-items only."