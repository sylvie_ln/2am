script=@"I'm a researcher at this Laboratorey, I've invented things like a car that's also a fish, and more.
!if global.flag_fishcar==1
 Thank you for the amazing invention idea. My papers are so organized now!
!elif 1
 Recently, I can't seem to keep my important research-papers together. I wish I had something like a clip for papers, but I don't think it's been invented.
!fi"
name="Searchey"

trade_modify_item(trade_add_item(oFishCar),item_data.name,"Invention: Fish Car");
trade_add_lootbox(oSpringShroom,2,5); // 3:16

// PRE TRADE
// When you first open the trade screen.
orig_text = "Welcome, will you peruse my invention?";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different item...?"
// When you deselect an item.
dont_text = "Oh, you don't want it...?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade back :("
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "That doesn't seem to have springy potential...."
// When you offer a low-rank item.
low_text = "I guess that item is okay...."
// When you offer a mid-rank item.
medium_text = "That seems like a good springy-parts."
// When you offer a high-rank item.
high_text = "Yes! I love that item!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't want that much of it."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I would like more items than that...."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text =  "I would like more items than that...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I only need springy shrooms...."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I only need springy shrooms...."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Wow, now that looks like an interesting device."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "That doesn't seem useful for my paper clipping desire."
// When you offer more than one of the exact item.
multiple_exact_text = "I would just like exactly one of those, for my research."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "I only want that paper-clipping device."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "This cannot be traded.."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "That's not such a good offer to me...."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text =  "I've researched all these items already."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Excellent, give this to me, I'd like to research it."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh sorry, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Oh cool, are you expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You cannot back out of the trade now...."
// When the NPC begins taking you items.
thank_text = "Thanks, this will benefit my researches."
// When a trade ends and you didn't attempt to steal.
complete_text = "Have an amazing day."
// When a trade ends and you attempted to steal.
complete_steal_text = "Have a disgusting day."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Oh, your a thief?"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Thief."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Goodbye."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "Seems you still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, please give back this."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You keep it. Give back the items for my research"
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want to research that, give the promised items."