script = @"!if global.flag_lies==0
I have been \color{c_purple}left\color{c_white} eternally imprisoned in this hole by Eivlys.
I await the one who can solve the riddle of Eivlys, and be granted her \color{c_purple}left\color{c_white} Hand. 
loop: Listen compassionately to the riddle \color{c_purple}left\color{c_white} behind. \ask{Yes}{No}
!if answer==1
"+
@"7 years and 7 months ago,\n"+
@"There was a moonlit night.\n"+
@"I sat alone by the lake,\n"+
@"No one by my side.\n"+
@"You called out to me.\n"+
@"Solemnly, I turned back,\n"+
@"Hearing your voice,\n"+
@"Ready to drown in tears.\n"+
@"On that moonlit night,\n"+
@"Our shared secret, unspoken.\n"+
@"Must you force me to remember?\n"+
@"Sorry, it's better \color{c_purple}left\color{c_white} forgotten."+
@"
These are the words Eivlys \color{c_purple}left\color{c_white} to seal me in this hole. Will you comprehend them? 
!else
Please choose the \color{c_purple}left\color{c_white} option.
!jump loop
!fi
!elif 1
 Leave this prison. The top awaits.
!fi";

name = "Mooncry"

trade_add_item(oHandOfLies)

orig_text = "Without lies, truth cannot exist.";
refuse_text = "...."
no_text = "...."
low_text = "...."
medium_text = "...."
high_text = "...."
exact_text = "...."
too_many_text = "Incorrect."
too_few_text = "Incorrect."
almost_text = "Incorrect."
multiple_exact_text = "Incorrect."
worthless_text = "Incorrect."
minigarbage_text = "Incorrect."
garbage_text = "Incorrect."
garbage_few_text = "Incorrect."
lets_text = "You have solved the riddle of Eivlys."
back_text = "...."
more_text = "...."
steal_text = "...."
finish_text = "...."
thank_text = "...."
complete_text = "This hand unlocks the way to the left. Good luck."
complete_steal_text = "This hand unlocks the way to the left. Good luck."
steal_over_text = "...."
steal_abort_text = "...."
give_back_text = "...."
yes_give_text = "...."
promise_text = "...."
no_way_text  = "...."
out_of_bag_text = "...."
dont_text = "...."
hmm_text = "...."
no_tradebacks_text = "Leave it be."
thatsmy_text = "The "
