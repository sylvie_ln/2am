script=@"!if global.flag_finale==1
 The inscription is unreadable!
!else
 The stone grave reads:
 Here lies Eivlys.\nThe Grand Witch, the Designer.\nCall forth her soul.
 The word "+"\"Eivlys\""+@" is formed from metal and juts out of the rock in a strange way, as if it is some kind of button or switch.\nWill you place your hand on it? \ask{Yes}{No}
 !if answer==1
  It gives slightly under the weight of your hand. The metal is oddly warm. For some reason, your head begins to hurt a little.\nWill you push inwards?\ask{Yes}{No}
  !if answer==1
   Ask yourself if you're certain.\nThere is no going back.\nSteel your resolve.\nYou've come a long way,\nand reached the last choice.\ask{Push}{Don't Push}
   !if answer==1
	!set global.flag_finale,1
    !finale
   !fi
  !fi
 !fi
!fi"