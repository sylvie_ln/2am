script=@"Welcome to UnCoMus, your one stop shop for uncommon mushrooms during this era of stopped time. 
Only 5 SM each, it's a bargain in every sale!!"
name="Uncomus"

trade_recet_fluffy(name);

trade_restock_me(trade_add_item(oWideShroom,"Wide Shroom","So wide, great for a couch or other uses! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oTallShroom,"Tall Shroom","It's tall, it's great, I love it! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oTinyShroom,"Tiny Shroom","Very little and cute, great in a stew! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oWorthlessShroom,"Worthless Shroom","It's worthless, but you gotta have it! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oWideShroom,"Wide Shroom","So wide, great for a couch or other uses! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oTallShroom,"Tall Shroom","It's tall, it's great, I love it! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oTinyShroom,"Tiny Shroom","Very little and cute, great in a stew! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oWorthlessShroom,"Worthless Shroom","It's worthless, but you gotta have it! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oTinyShroom,"Tiny Shroom","Very little and cute, great in a stew! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oTinyShroom,"Tiny Shroom","Very little and cute, great in a stew! Costs 5 SM.",trade_standard_costs(),5));
trade_restock_me(trade_add_item(oWorthlessShroom,"Worthless Shroom","It's worthless, but you gotta have it! Costs 5 SM.",trade_standard_costs(),5));

// PRE TRADE
// When you first open the trade screen.
orig_text = "Woohoo! UnCoMus is the place to get uncommon things!";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different one?!"
// When you deselect an item.
dont_text = "Oh, you don't want this great item??!"
// When the NPC evaluates your offer
hmm_text = "Hmm....?!"
// When you select something you traded to an NPC
no_tradebacks_text = "No refunds!"
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "We only accept standard SM currency at today's rates!"
// When you offer a low-rank item.
low_text = "Wow, a sweet basic shroom!"
// When you offer a mid-rank item.
medium_text = "Yes, it's so tall or wide!"
// When you offer a high-rank item.
high_text = "YEAH!! Tiny little shroom!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "That's too much!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "That's not enough!"
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Ooh, a little more!"
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "We only accept standard SM currency at today's rates! Take out the extra stuff!"
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "We only accept standard SM currency at today's rates! Take out the extra stuff!"

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No deal!!"
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "No deal!!"
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Amazing! Let's tradey trade!"
// When you take your item back after the NPC accepts your offer.
back_text = "Waah!! Are you backing out??"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Whoa! Expanding your offer!"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Nooo! You can't back out of the trade!!"
// When the NPC begins taking you items.
thank_text = "I'll take your stuff!!"
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks so much!! Havea good day!! Just leave the area and I'll restock!!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Goodbye."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Only a cruel person would steal."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "You've proven yourself to be lower than mud."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thanks."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Good, put it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep that, but give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "Not interested. Give me the items you promised."