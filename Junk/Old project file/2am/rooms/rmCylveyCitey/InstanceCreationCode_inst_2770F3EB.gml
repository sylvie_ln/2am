script = "Canyon Cliffs is to the left. It's an area that's quite wonderful."
name = "Wirte"

trade_add_item(oMinecartLicense,"It proves the holder can drive a minecart. Can you offer me something wonderful for it?");
trade_add_item(oWorthlessShroom,"Wonderful Shroom","Though it may seem worthless to others, it's a precious treasure to me.",
trade_cost(noone,0),99);
trade_add_item(oWorthlessShroom,"Wonderful Shroom","Though it may seem worthless to others, it's a precious treasure to me.",
trade_cost(noone,0),99);
trade_add_item(oWorthlessShroom,"Wonderful Shroom","Though it may seem worthless to others, it's a precious treasure to me.",
trade_cost(noone,0),99);
trade_add_item(oWorthlessShroom,"Wonderful Shroom","Though it may seem worthless to others, it's a precious treasure to me.",
trade_cost(noone,0),99);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Hello to you.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade it back."
// Prefix before item name
thatsmy_text = "That's my "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer a high-rank item.
high_text = "Wow, now that's so wonderful!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "You offer me too much greatness...."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I only want items that are wonderful...."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I only want items that are wonderful...."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I will never trade this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "I decline."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "It's time for a wonderful trade."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh no, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
// When the NPC begins taking you items.
thank_text = "I'll treasure this."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thank you for this kindness."
// When a trade ends and you attempted to steal.
complete_steal_text = "Bye...."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "You'd steal from someone like me?"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "I guess I deserve this."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you...."
//// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised...."
// When you pick up an item that you stole.
yes_give_text = "Yes, please give back that item...."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep it, but please give back the items you promised...."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "Please, I don't want that, just give me the items you promised...."