script=@"!if global.flag_cheesey==1
 I've had my\color{c_orange} cheesey\color{c_white}, time to take it easy. Thanks for the taco.
!elif 1
 I am too hungry....
 I'm so craving something\color{c_orange} cheesey\color{c_white}.... But with time unflowing, I may never get this wish.
!fi"
name = "Flaire"

trade_add_item(oHandOfFlames);
trade_add_lootbox(oPoisonShroom,2,4); // 2:15
trade_add_lootbox(oPoisonShroom,2,4); // 2:15

// PRE TRADE
// When you first open the trade screen.

// Alt text version!
orig_alt_text = "I have in hand a cheesey treat, my tedious life is now complete.";
if global.flag_cheesey == 0 {
	orig_text = "I'd love to have a cheesey treat, mushroomey and good to eat.";
} else {
	orig_text = orig_alt_text;
}
// For alt text, add a hook in oInTheBag as well.

// When you select an item other than the selected one.
finish_text = "Oh, you want a different item?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back."
// Prefix before item name
thatsmy_text = "It's the "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "That's not poisonous."
// When you offer a mid-rank item.
medium_text = "Looking poisonous...."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't need so much poison!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Needs a bit more poison..."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "A little more poison..."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Take out everything that is not poisonous."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Take out everything that is not poisonous."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Oh yes, put that cheesey taco in my hands!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "That's not cheesey."
// When you offer more than one of the exact item.
multiple_exact_text = "I won't be able to eat two of them!"
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "I don't need the other stuff except the taco."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I will never trade this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "I disagree with your offer."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "This offer simpley isn't cheesey enough."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Hooray, let's trade."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You can't back out of the trade now."
// When the NPC begins taking you items.
thank_text = "Thanks, I'll take this items."
// When a trade ends and you didn't attempt to steal.
complete_text = "Yes! Thanks for the trade!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks for the trade, stealing fool."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Don't you steal."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "A fool is one who steals."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Yes, good."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yeah, give it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can have it, but give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that. Give me the items you promised."