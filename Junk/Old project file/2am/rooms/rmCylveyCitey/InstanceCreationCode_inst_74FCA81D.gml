script=@"Ever since the clock tower stopped its ticking, the sun has hung still in the sky, and time itself has ceased to flow....
I believe that the key to this oddity is the legendary 12 Hands of the Clock. You must collect them.
Would you like some hints about your quest? \ask{Yes}{No}
!if answer==1
 Explore the world, talk to people, and offer to trade with them. You'll find out about their problems. 
 And as you explore enough, maybe you'll get an idea of how to solve some of these problems.
 Some people might even have the amazing Hands of the Clock you're looking for. Solve their problems and make them happy.
 When you've helped many people and built up a good collection of Hands, you might want to explore the Clock Tower here. What will you find at the tower's peak?
 I have some more specific hints you can learn, but you'll need SM (Shroom Mony) to trade for them. Now how do you get this SM?
 Explore the world and look for mushrooms in Mushroom Zones. These are vast, dangerous areas where many kinds of mushrooms grow.
 Basic and uncommon mushrooms can be used as SM. You can find out the exchange rates at the Bank in the Citey Center.
 Rare mushrooms can sometimes be found, which can't be used as SM, but might have other interesting uses throughout the world.
 That's the end of the tutorial.\nGo into the world.
!fi"
name="Byclock"

trade_add_item(oHint,"Enlargening my Bag?","This hint costs 1 SM.",
trade_standard_costs(),1);
trade_add_item(oHint,"A Cheesey Treat?","This hint costs 3 SM.",
trade_standard_costs(),3);
trade_add_item(oHint,"Where's the RainbowShroom?","This hint costs 5 SM.",
trade_standard_costs(),5);
trade_add_item(oHint,"Climbing the Tower?","This hint costs 7 SM.",
trade_standard_costs(),7);
trade_add_item(oHint,"The Tower's Prisoner?","This hint costs 9 SM.",
trade_standard_costs(),9);

// PRE TRADE
// When you first open the trade screen.
orig_text = "These hints might be a little helpful.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different hint?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade it back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item and the item is tradeable.
no_text = "Please use standard SM currency to purchase hints."
// When you offer a low-rank item.
low_text = "Thanks, a basic item."
// When you offer a mid-rank item.
medium_text = "Thanks, a slightly large item."
// When you offer a high-rank item.
high_text = "Thanks, a tiny little item."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "You're overpaying a little, take some of the stuff out."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "That's not enough mony."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "You need a little more mony."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Please use standard SM currency to purchase hints."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Please only use standard SM currency to purchase hints."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I am not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Offer rejected."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "Please use standard SM currency to purchase hints."
// Special worthless text used if you offer an item without selecting anything"
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Cool! I hope you find the hint helpful on your quest."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "The trade's finalized, sorry!"
// When the NPC begins taking you items.
thank_text = "I'll take your items now."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks, see you later!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks, see you later...."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, put that back."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Please don't steal."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Great, thanks."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the mony you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, put it back...."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep that, but give back the mony you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that! Just give back the mony you promised."