visible = false;
script = "You feel a cool and mysterious breeze blowing through the gap. Beyond it lies something unimaginable."
rm = rmMushZone;
tag = "CiteyMushZone3";
door_script = mushzone_array("Cataclysmic Fountain",4444,oCiteyDark,make_color_hsl_240(80,120,30),sCataclysmicFountain,noone,3)