script=@"!if global.flag_kittey==0
A kittey, a kittey,\nI want to pet a kittey...\n"
+"So round and squishey,\nso soft and fluffey,\n"+
@"A kittey's what I need in the world!!
!elif 1
 You gave me a sweet kittey, you are now my official best friend in the world!
!fi"
name="Creamy"

trade_add_item(oHandOfTears)
trade_add_lootbox(oKittyShroom,2,6); // 3:20

// PRE TRADE
// When you first open the trade screen.
orig_text = "I simply love kitteys, they are the cutest.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different?"
// When you deselect an item.
dont_text = "Oh, you don't want?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "This is my kittey forever."
// Prefix before item name
thatsmy_text = "Meow, a "

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Don't want."
// When you offer a mid-rank item.
medium_text = "Yes!!! How kitteyish!!!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Whoa, even for me, that is too many."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need more kitteys!!"
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I need more kitteys!!"
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I only need kitteys!!"
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I only need kitteys!! And more of them!!!"

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "A kittey!! A kittey!! A sweet kittey in the world!!!!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "That is simpley not the kittey I seek"
// When you offer more than one of the exact item.
multiple_exact_text = "I just want one kittey!"
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "I don't need anything else, just a kittey!!"

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Won't trade this!"
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "That is worthless to me"
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Kittey kittey!!! Let's trade!!!"
// When you take your item back after the NPC accepts your offer.
back_text = "Noo, are you backing out??"
// When you offer more stuff after the NPC accepts your offer.
more_text = "MMeow?! More?!"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Nooooooo, you can't go back!"
// When the NPC begins taking you items.
thank_text = "Kittey in the world!!"
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks!!!!!!!!!!!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks, but you are a theaf"

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Nooooooooooooo!!! Theif!!!"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Your a thief."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thanks for returning it."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some promised items.."
// When you pick up an item that you stole.
yes_give_text = "Yes, give back it."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You must keep that item, and give me the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that, give me back the promised items."