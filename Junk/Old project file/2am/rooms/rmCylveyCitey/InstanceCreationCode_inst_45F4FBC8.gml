script=@"
!if global.flag_gap==1
 I have no more need for you. Or anything in this pathetic world. 
!elif 1
 I am searching for the Shards of Eivlys.
 If you don't have what I need, I suggest you do not try speaking to me.
!fi"
name="Galfio"

trade_add_item(oGapKey);

// PRE TRADE
// When you first open the trade screen.

// Alt text version!
orig_alt_text = "Leave me alone. I need to prepare for the ritual.";
if global.flag_gap == 0 {
	orig_text = "I need three pieces of Eivlys's corpse. You do not need to know the reason.";
} else {
	orig_text = orig_alt_text;
}
// For alt text, add a hook in oInTheBag as well.

// When you select an item other than the selected one.
finish_text = "..."
// When you deselect an item.
dont_text = "..."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "These are mine now."
// Prefix before item name
thatsmy_text = ""

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Pathetic item..."
// When you offer a low-rank item.
low_text = "Yes, that shard looks splendid."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Somehow, you have offered too many shards."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I require three shards for my plan."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I require three shards for my plan."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I only want body parts of Eivlys."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I only want body parts of Eivlys."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Don't waste my time."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "If you don't have what I need, go away."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Very well. Take this wretched key."
// When you take your item back after the NPC accepts your offer.
back_text = "You're unsure. That's understandable."
// When you offer more stuff after the NPC accepts your offer.
more_text = "I have no need for additional items."
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It is too late."
// When the NPC begins taking you items.
thank_text = "They're finally mine...."
// When a trade ends and you didn't attempt to steal.
complete_text = "I doubt you understand the consequences of this trade, but I thank you."
// When a trade ends and you attempted to steal.
complete_steal_text = "You will be cursed eternally."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Do Not Steal From Me."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Return That Immediately."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Pathetic."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "Return All Of Them."
// When you pick up an item that you stole.
yes_give_text = "Put It Back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You Cannot Back Out Now."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "That Is Worthless."