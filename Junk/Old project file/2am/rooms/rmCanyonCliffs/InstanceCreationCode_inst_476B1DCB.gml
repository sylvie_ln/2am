script=@"The Terror Graveyard of Eivlys is outside. An area where the dead lie who slipped and fell from these treacherous cliffs, or were they cursed?
Who's Eivlys you ask, heh, you don't wanna know. All I can say is that I am not one of her Followers...."
name="Graal"

trade_add_item(oRedShard);
trade_add_lootbox(oPoisonShroom,1,1); // 1:5
trade_add_lootbox(oPoisonShroom,1,1); // 1:5
trade_add_lootbox(oPoisonShroom,1,1); // 1:5
// PRE TRADE
// When you first open the trade screen.
orig_text = "I have nothing of value....\nhe he he.";
// When you select an item other than the selected one.
finish_text = "Agh, you want a different item??"
// When you deselect an item.
dont_text = "Heh, a wise choice."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "Heyahaha, I won't trade this back."
// Prefix before item name
thatsmy_text = "This is a "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Not what I seek for this particular item..."
// When you offer a low-rank item.
low_text = "Gwa ha ha! Yes, how perfect.... eyaahaahh!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Hee hee... I don't need that much!!!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need moooore....."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I need moooore....!"
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "I only need your legs.... wa ha ha."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I only need your legs.... wa ha ha."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No matter what you offer, I'm not trading this!!"
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Your offer is worthless to me.... Cackle cackle."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Heyah ha hah!! I hope you don't regret it!"
// When you take your item back after the NPC accepts your offer.
back_text = "Having second thoughts??!"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Moooore??!"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You can't back out of the trade now!!!"
// When the NPC begins taking you items.
thank_text = "Your legs are mine.... eeyaaH!!"
// When a trade ends and you didn't attempt to steal.
complete_text = "Gyaah hah haaah!!!!!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Gyaah hah haaah!!!!!"

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Screech!! Don't steal!!"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "NO!!! I need your legs!!!!!"
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "You are a true villain.... perhaps even as villainous as me!"
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the legs you promised me!!!"
// When you pick up an item that you stole.
yes_give_text = "Yes, give me the legs you use to walk with!!!!"
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can have it, just give me your legs!!!"
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want it!! Give me your legs...."