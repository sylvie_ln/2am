script=@"Known everywhere for my rocks.
Even though time has stopped, still I'm known for my rocks everywhere. Won't you look at them?"
name="Rockout"

trade_add_item(oBoringRock,"Textured Rock","This Rock has a lovely texture. Costs 2 SM.",trade_standard_costs(),2);
trade_add_item(oBoringRock,"Mischief Rock","I tripped over this Rock yesterday. Costs 2 SM.",trade_standard_costs(),2);
trade_add_item(oBoringRock,"Weapon Rock","This Rock was once thrown at someone. Costs 2 SM.",trade_standard_costs(),2);
trade_add_item(oUniqueRock);
trade_add_item(oBoringRock,"Basement Rock","I found this Rock downstairs. Costs 2 SM.",trade_standard_costs(),2);
trade_add_item(oBoringRock,"Unknown Rock","Sorry, I don't know anything about this Rock. Costs 3 SM.",trade_standard_costs(),3);
trade_add_item(oBoringRock,"Rockin Rock","This Rock really 'rocks', hehe. Costs 2 SM.",trade_standard_costs(),2);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Love the Rocks. Love the World.";
// When you select an item other than the selected one.
finish_text = "A different item rocks your world?"
// When you deselect an item.
dont_text = "This doesn't rock to you?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "Not trading it back.."
// Prefix before item name
thatsmy_text = "My "
// NPC bag is empty after evaluating offer

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Trade me something else...."
// When you offer a low-rank item.
low_text = "Delicious, just the basics."
// When you offer a mid-rank item.
medium_text = "Well, look what we have here."
// When you offer a high-rank item.
high_text = "Oh, someone's a big spender."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "Stop. That is too much."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "More please..."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "A little more please..."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Don't need some of that stuff...."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Not enough.... and I don't need some of that stuff...."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Yes, that shroom of sparkles rocks my world!!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "Now that's not a sparkly shroom, no thanks."
// When you offer more than one of the exact item.
multiple_exact_text = "I only need one of this sparkling wonder."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "I just need something which contains the sparkle power."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I will not trade this ever."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Sorry, I will not trade for such."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Rock on! Let's trade!"
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, can't rock with it?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Hey, this trade is rock-solid. You can't back out."
// When the NPC begins taking you items.
thank_text = "Great, I'll take it all now."
// When a trade ends and you didn't attempt to steal.
complete_text = "Rock on!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Bye."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "What are you doing...."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Stealing does NOT rock."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "You don't rock."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, it would rock if you give that back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep that rock."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that. Give back what you promised to trade me."
