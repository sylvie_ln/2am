script=@"
!if global.flag_bridge_canyon==0
 This is the Canyon Cliffs, up ahead is the famous cliff, and I am none other than the famous Treasure Hunter of the cliff.
 I am looking to have a bridge across the cliff so I can find the treasure.
 Why don't you exchange for one of my balls and make a wonderful bridge?
!elif global.flag_ladder_canyon==0
 I made it across the cliff of this Canyons.
 But it seems the treasure will be up in the sky.
 Why don't you exchange for one of my wonderful balls and make a ladder?
!elif global.flag_treasure_hunter==0
 The treasure of the Canyon Cliff.
 What is this? It radiates mysterious powers.
 I feel like I should not have such an object...
 I will give it to the righteous one who helped me.
 Here is the treasure I have found in this Area.
 !throw oHandOfLight,30,8
 !set global.flag_treasure_hunter,1
!else
 A Treasure Hunter like myself is satsified, by the journey of finding the Treasure.
 Even if we give up the Treasure in the end, we still have a Treasure of a Journey.
!fi"
name="Tressa"

trade_add_item(oBridgeOrb);
trade_add_item(oBridgeOrb);
trade_add_item(oLadderOrb);
trade_add_item(oLadderOrb);
trade_add_item(oBasicShroom,"Basic Shroom","That's my dinner, traveller.",trade_cost(noone,0),99);

// PRE TRADE
// When you first open the trade screen.
orig_text = "I specialize in treasure Balls. Browse my balls, traveller.";
// When you select an item other than the selected one.
finish_text = "Oh, you want another?"
// When you deselect an item.
dont_text = "Oh, you want nothing?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back."
// Prefix before item name
thatsmy_text = "It's "
// NPC bag is empty after evaluating offer

// NORMAL TRADES
// When you offer a mid-rank item.
medium_text = "Yes, grant me this essential item."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't need so many, traveller."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "A little more of this Items and I'll make the Ball of your Dreams."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "A little more of this Items and I'll make the Ball of your Dreams."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Take out the stuff I don't need, please."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "I need more of the desired shrooms, traveller."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I refuse to trade this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "That offer is no good, traveller."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Traveller, you've made me a splendid offer."
// When you take your item back after the NPC accepts your offer.
back_text = "Backing out? Hm."
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late to back out, traveller."
// When the NPC begins taking you items.
thank_text = "I'll take these amazing items."
// When a trade ends and you didn't attempt to steal.
complete_text = "See you again, traveller."
// When a trade ends and you attempted to steal.
complete_steal_text = "See you again, criminal."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Don't steal."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Criminal."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Horrible criminal."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "There, put that one back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep it, and give back the items you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No, give back the items you promised, not that thing."
