script=@"Welcome to this canyon of cliffs, I have some hints, do you want to know them? \ask{Yes}{No}
!if answer==1
  There's quite a large amount of this area that can't be easily reached. But lots of cool stuff is accessible from the start.
  That door up there is a Mushroom Zone where TinyShrooms can be found. TinyShrooms are great because they're worth more SM than other uncommon shrooms, and take up less space in your bag.
  There's a shoe shop up there, and a junk shop in the lower left. They mostly don't take SM though, so you'll need specific items to trade with them.
  Did you know it's possible to make your Bag larger? I don't really know the details, but there's someone in the Citey who will do it. You should probably do that as soon as possible, so you can hold more stuff.
  Near the junk shop, there's someone who runs a Minecart Ride. That person has one of the Hands of the Clock, and might give it to you if you display some skillful riding!
  If you want to know more, I've got some more specific hints, but they're locked behind a paywall. I gotta make mony somehow.
!fi"
name="Canyetta"
	
trade_add_item(oHint,"Crossing the Canyon?","This hint costs 1 SM.",
trade_standard_costs(),1);
trade_add_item(oHint,"A Minecart License?","This hint costs 3 SM.",
trade_standard_costs(),3);
trade_add_item(oHint,"Where's the SpringShroom?","This hint costs 5 SM.",
trade_standard_costs(),5);
trade_add_item(oHint,"Something that Sparkles?","This hint costs 7 SM.",
trade_standard_costs(),7);
trade_add_item(oHint,"The Canyon Sky?","This hint costs 9 SM.",
trade_standard_costs(),9);

// PRE TRADE
// When you first open the trade screen.
orig_text = "These hints might be a little helpful.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different hint?"
// When you deselect an item.
dont_text = "Oh, you don't want it?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade it back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item and the item is tradeable.
no_text = "Please use standard SM currency to purchase hints."
// When you offer a low-rank item.
low_text = "Thanks, a basic item."
// When you offer a mid-rank item.
medium_text = "Thanks, a slightly large item."
// When you offer a high-rank item.
high_text = "Thanks, a tiny little item."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "You're overpaying a little, take some of the stuff out."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "That's not enough mony."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "You need a little more mony."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Please use standard SM currency to purchase hints."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Please only use standard SM currency to purchase hints."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Nah, not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "No, that offer's no good."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "Please use standard SM currency to purchase hints."
// Special worthless text used if you offer an item without selecting anything"
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Cool! I hope you find the hint helpful on your quest."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "The trade's finalized, baby."
// When the NPC begins taking you items.
thank_text = "I'll take your items now."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks, see you later!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Goodbye, vile criminal stealer."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, please put that back."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Please don't steal."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Great, thank you."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the mony you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, put it back...."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep that, but give back the mony you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that! Just give back the mony you promised."