script =
@"
!if global.flag_minecart!=1
 Welcome to the famous Minecart Riding. Rides are free until time resumes its flow.
 Would you like to ride? \ask{Yes}{No}
 !if answer==1
  !bag oMinecartLicense
  !if flag_bag==1
   Okay, let's go Riding.
   !user 2, oIntangibleDoor
  !else
   Too bad, you need a license to drive a minecart. Come back with a license.
  !fi
 !else
  Wow, goodbye!
 !fi
!else
 Amazing, your skills of the minecart are unparalleled. Take the treasure now.
 !throw oHandOfEarth,90,4
 !set global.flag_minecart,2
!fi
"
name="Mineria"

if global.flag_minecart == 0 {
	trade_add_item(oHandOfEarth,"One of the legendary 12 Hands of the Clock. A prize for great minecart skills.");
}

trade_add_lootbox(oRainbowShroom,3,3); // 1:9
trade_add_lootbox(oRainbowShroom,3,3); // 1:9
trade_add_lootbox(oRainbowShroom,3,3); // 1:9

if global.flag_minecart == 0 {
	orig_text = "I have some boxes to trade. And there's a prize for minecart-experts.";
} else {
	orig_text = "I have some boxes to trade. Nothing else";
}

// PRE TRADE
// When you select an item other than the selectd one.
finish_text = "Hm, you want something else?"
// When you deselect an item.
dont_text = "Hm."
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade this back.."
// Prefix before item name
thatsmy_text = "That's my "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "You need to give me something rainbowish."
// When you offer a low-rank item.
low_text = "I guess that item is okay...."
// When you offer a mid-rank item.
medium_text = "That item seems good."
// When you offer a high-rank item.
high_text = "Now that's quite rainbow."
// When you offer exactly what they want.
too_many_text = "I don't want that much rainbow power."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "I need more than that." 
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "I need more than that." 
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "That's quite rainbow, but you have some unnecessary items."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Some of those items aren't so interesting to me."


// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I will not trade this."
// All items are garbage 
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "That's quite worthless of an offer."
// Special worthless_text for the first item added to NPC's bag.
if global.flag_minecart == 0 {
	first_worthless_text = "This can only be gained through showing your minecart expertise."
} else {
	first_worthless_text = worthless_text
}
// Special worthless text used if you offer an item without selecting anything"
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Good, shall we trade?"
// When you take your item back after the NPC accepts your offer.
back_text = "Backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "It's too late to back out.."
// When the NPC begins taking you items.
thank_text = "I'll take this now."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks, have a good day."
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks, have a rotten day."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Don't you steal now.."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "You are horrible. You are horrible."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Hmph."
//// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yeah, put it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Keep that. Give back the items you promised me."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "No, not that, give back the stuff you originally offered."
