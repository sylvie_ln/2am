script=@"!if global.flag_decap!=1
 What do you think it feels like to be junk? A spare part once used to hold something together, now discarded.
 Welcome to my melancholic Junk Shop, the place where all souls must return.
 With time being stopped and all, I won't be able to go get more parts for a while, but you can browse what I have.
!else
 Okay, I'll begin the process.
 Haaaaaaaaaaaaauuu!!
 !throw_ns oJunkParts,160,6,2
 !move 0,0,3
 There's your spring, please treasure it. Thank you.
 !set must_restock,1
 !set global.flag_decap,0
!fi"
name="Junky"

trade_recet_fluffy(name);

trade_add_item(oJunkParts,"Junk Part: Screw","Kitteys like to dig these up. I'll trade for a KitteyShroom.",
trade_cost(oKittyShroom,0),99);
trade_add_item(oJunkParts,"Junk Part: Gear","Made from poison-infused metal. I'll trade for a PoisonShroom.",
trade_cost(oPoisonShroom,0),99);
trade_add_item(oJunkParts,"Junk Part: Gear","Made from poison-infused metal. I'll trade for a PoisonShroom.",
trade_cost(oPoisonShroom,0),99);
trade_add_item(oJunkParts,"Junk Part: Spring", "These are easy for me to source. Costs 4 SM.",
trade_standard_costs(),4);
trade_add_item(oJunkParts,"Junk Part: Spring", "These are easy for me to source. Costs 4 SM.",
trade_standard_costs(),4);
trade_add_item(oJunkParts,"Junk Part: Spring", "These are easy for me to source. Costs 4 SM.",
trade_standard_costs(),4);
trade_add_item(oJunkParts,"Junk Part: Nail","Requires a golden alloy. I'll trade for a shining SparkleShroom.",
trade_cost(oSparkleShroom,0),99);
trade_add_item(oJunkParts,"Junk Part: Nut","I'll trade for a FloatShroom, they have a certain chemical needed to make it.",
trade_cost(oFloatShroom,0),99);
trade_add_item(oJunkParts,"Junk Part: Nut","I'll trade for a FloatShroom, they have a certain chemical needed to make it.",
trade_cost(oFloatShroom,0),99);
trade_add_item(oJunkParts,"Junk Part: Gemstone","I'll trade for a SpeedShroom, they're super at sniffing these out.",
trade_cost(oSpeedShroom,0),99);
trade_restock_me(trade_add_item(oDecapService,"Decap Service", "Give me a SpringShroom, and I'll take the top off and give you a Spring.",
trade_cost(oSpringShroom,0),99));
trade_add_item(oMemoryDisk,"Used Disk", "An old floppy! Been ages since I've seen one. I'll trade it for any rare shroom.",
trade_cost(oKittyShroom,3,oFloatShroom,3,oSpeedShroom,3,oSparkleShroom,3,oSpringShroom,3,oPoisonShroom,3,oRainbowShroom,3),3);
trade_add_item(oBoomBox,"Boombox","A lovely antique, but the CD drive has a broken laser. Need two RainbowShrooms to repair.",
trade_cost(oRainbowShroom,2),4);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Welcome to my world of junk.";
// When you select an item other than the selected one.
finish_text = "Oh, you want another item?"
// When you deselect an item.
dont_text = "Not interested?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "This is something I'd like to keep."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Not the item I'm looking for here...."
// When you offer a low-rank item.
low_text = "A simple item."
// When you offer a mid-rank item.
medium_text = "Yes, that is good."
// When you offer a high-rank item.
high_text = "Nice, that's a wonderful item."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "That's too much items!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "That's not enough items!"
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Need a bit more items..."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Great, but there are some unneeded items in your offer."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "There's some unneeded items in your offer...."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "Great, that's just what I need."
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = no_text
// When you offer more than one of the exact item.
multiple_exact_text = "I only need one of those for this trade."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "I don't need any extra stuff for this trade, but thanks."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "I'd like to hang on to this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "Not really what I'm looking for here...."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "That's so wonderful. Let's begin the promised exchange."
// When you take your item back after the NPC accepts your offer.
back_text = "Are you backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Hey, you can't back out of the trade now...."
// When the NPC begins taking you items.
thank_text = "I'll take it now."
// When a trade ends and you didn't attempt to steal.
complete_text = "A peaceful little trade in the world."
// When a trade ends and you attempted to steal.
complete_steal_text = "A trade in the world.."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Are you stealing...."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "A stolen memory...."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you...."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised me on that day."
// When you pick up an item that you stole.
yes_give_text = "Yes, please give this back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep it.... as long as you give back the promised items."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want it, I just want the items you promised me in a heartfelt way."