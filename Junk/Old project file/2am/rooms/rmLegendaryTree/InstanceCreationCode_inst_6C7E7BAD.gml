name="Rose"
script=@"
!set cutscene,1
!speaker Sunny
Karie? Have you finally come?
!set oChar.image_xscale,-1
!move 0,0,1,oChar
!set oChar.sprite_index,"+string(sCharR)+@"
!move -16,0,1,oChar
!set oChar.sprite_index,"+string(sCharS)+@"
!move 0,0,1,oChar
No... it's someone else.
The one I love will never appear. She doesn't share my feelings.
I'll never experience a beautiful moment with her.
She knew my intentions from the start. She ignored the letter, so my heart wouldn't be shattered...
Karie... how can you be so gentle?\nHow can you be so cruel?
To be trapped without you in a world of frozen time...\nI can't accept that I'll live forever without your warmth.
It was foolish of me to ever have hope. It was stupid of me to think my dream could come true...
What will happen... \nShould I even think of something like that?
If I jump... what will...
!speaker Rose
Jumping won't do anything in a place like this.
Karie is waiting for you.
She wants to know where you are... She's waiting by the Legendary Tree.
!speaker Sunny
You're lying. She abandoned me. She's not here.
The person I need abandoned me. What's the point...?
!speaker Rose
This is another world, isn't it? This is the darkness inside of your heart.
This is a world where twisted feelings rise from the abyss and swallow you up.
Let the purity and light of your love break the vines that bind you to this false world.
!speaker Sunny
There's nothing left of me. I flew too close to the sun and melted.
I'm burning, and choking on the smoke. If this is love, I regret loving.
!speaker Rose
Being in this place has made you forget the calmness of love. The sweet and fluffy days you spent with her.
Remember how her smile filled you with warmth.\nRemember how she painted over your life with new colours.
!speaker Sunny
I only remember tears.
I only remember false happiness brought upon by my own worthlessness.
!speaker Rose
Love is a conflict between dysphoria and euphoria. 
Until euphoria wins, don't give up.
Take my hand.
!speaker Sunny
No.
!speaker Rose
Then I'll take yours.
!set oChar.sprite_index,"+string(sCharR)+@"
!move -32,0,2,oChar
!set oChar.sprite_index,"+string(sCharS)+@"
I'll drag you out of this world.
!set oChar.sprite_index,"+string(sCharR)+@"
!move -32,0,2,oChar
!set oChar.sprite_index,"+string(sCharS)+@"
I'll show you what's waiting for you.
!set oChar.sprite_index,"+string(sCharR)+@"
!move -16,0,1,oChar
!move_ns -16,0,1,oChar
!speaker Sunny
!user 13
No!!!
Stop!!!
Let me die!!!
"