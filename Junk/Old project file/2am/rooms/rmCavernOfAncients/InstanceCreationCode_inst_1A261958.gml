script=@"Howdy. I just know that the statue here is strangely upside down. 
Could it be a clue to the Secret Tomb?
!if global.flag_pinkpaperclip==1
 Thank you for the Horse by the way, I'll take care of it so gently.
!elif 1
 Someday I'll find the tomb's treasure, and ride off into the sunset on a beautiful Horse.
!fi"
name="Country"

trade_add_item(oPinkPaperclip);
trade_add_lootbox(oSparkleShroom,3,6); // 2:20
trade_add_lootbox(oSparkleShroom,3,6); // 2:20

// PRE TRADE

// Alt text version!
orig_alt_text = "Howdy. I now have a Horse and I'm wonderful.";
if global.flag_pinkpaperclip == 0 {
	orig_text = "Howdy. I'm on the lookout for a Horse to ride.";
} else {
	orig_text = orig_alt_text;
}
// For alt text, add a hook in oInTheBag as well.

// When you select an item other than the selected one.
finish_text = "Oh, you will look at a different item?"
// When you deselect an item.
dont_text = "Oh, don't want this?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade back."
// Prefix before item name
thatsmy_text = "That's my "
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "No thanks, not sparkly enough."
// When you offer a low-rank item.
low_text = "I guess that item is okay...."
// When you offer a mid-rank item.
medium_text = "That item seems good."
// When you offer a high-rank item.
high_text = "Oh, I love that sparkling shine."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't need so many of this!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Hmm, need more than that..."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Hmm, need more than that..."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "The sparkle I want, but a country girl doesn't need these other items."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "A country girl doesn't need some of these items."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "The horse of my dreams?"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = "I'll only trade it for the incredible horse."
// When you offer more than one of the exact item.
multiple_exact_text = "I can't take care of two horses, please just give me one."
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Please, just give me the horse, I can't handle these other items."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "Sorry, I will never part with this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "In the country, we don't accept this kind of offer."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "Since it's not a horse, I don't care."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "This will be a trade for the ages."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh, backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Sorry, can't back out this late."
// When the NPC begins taking you items.
thank_text = "I'll take y'all items."
// When a trade ends and you didn't attempt to steal.
complete_text = "Thanks for the lovely trade."
// When a trade ends and you attempted to steal.
complete_steal_text = "Thanks for the trade."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Your stealing?!"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "This is why we don't trust citey folk."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you for returning it honestly."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the items you promised."
// When you pick up an item that you stole.
yes_give_text = "Yes, give it back."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can have it, just give back what you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that, just give back what you promised."