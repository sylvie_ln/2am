script=@"!if global.flag_dice==1
 Oh, did you roll the three sixes? Sorry, I wasn't paying attention.
!else 
 One in two hundred and sixteen.
 That's the chance that all three of the dice land on a six.
 I bet everything on that chance, and I lost it all.
 But isn't that the essence of gambling? Throwing yourself in a pit of wolves for a chance at glory.
 I may no longer have any possessions, but my gambler's heart will never die.
 If you want to test your luck, pick up those dice and give 'em a throw.
!fi"
name="Lady"

// NPC bag is empty
orig_text = "I no longer possess any goods, since I lost them all to Gambling."
no_text="I have nothing to offer you for this...."