visible = false;
script = @"An ancient inscription is on the statue.
Ensure the kitten does not run astray from the early morning to the end of the day.\nThe kitten will play in the oceans lagoon, and unlock the way with a glance at the moon.
There is a button shaped like a crescent moon below the inscription. Will you press it? \ask{Yes}{No}
!if answer==1
 !if global.flag_switch_1==1 and global.flag_switch_2==1 and global.flag_switch_3==1 and global.flag_switch_4==1
  Enter the Secret Tomb!
  !user 2, oIntangibleDoor
 !else
  Nothing happens, but you feel a sense of unease, as if a puzzle has not been solved correctly.
 !fi
!fi"
