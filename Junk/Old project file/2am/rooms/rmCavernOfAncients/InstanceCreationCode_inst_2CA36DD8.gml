if global.heist_stage == 3 {
	if date_compare_date(date_current_datetime(),global.heist_date) == -1 {
		instance_destroy();	
	} else {
		global.heist_stage = 0;	
	}
}
if global.heist_stage == 1 {
	global.heist_stage = 0;	
}
script=@"!if global.heist_stage==2
 Cool. It was close, but we pulled it off.
 Meet me tomorrow, "+global.heist_date_string+@", same place. We'll clean these suckers out.
 In total we have completed "+string(global.heist_count+1)+@" heists
 !throw "+global.heist_item+",315,6,"+string(global.heist_ii)+@"
 !set global.heist_count,global.heist_count+1
 !set global.heist_stage,3
 !disappear
!elif global.heist_count==0
 !if global.heist_stage==0
  So here's the plan.
  You head in the museum, keep 'em distracted, while I pick the lock on the back entrance.
  We meet up inside, you pass me the goods when the guards aren't looking. Waltz out the front like nothing happened, and I'll sneak out the juice.
  We'll keep it on the down low and just lift one item every day. By the time they notice, we'll have half their stock.
  Either you're in, or you're out. \ask{I'm in}{I'm out}
  !if answer==1
   !set global.heist_stage,1
   Great. Give me the signal when you enter and I'll get crackin'.
  !elif 1
   Shame. We could've hit it big with this heist. Come back if you change your mind.
  !fi
 !elif global.heist_stage==1
  I'm all set for our big heist. Give me the signal when you enter.
 !fi
!elif global.heist_count>=1
 !if global.heist_stage==0
  Heh, welcome back. Ready for another death-defying museum heist? \ask{I'm in}{I'm out}
  !if answer==1
   !set global.heist_stage,1
   Great. Give me the signal when you enter and I'll get crackin'.
  !elif 1
   Shame. Come back when you're ready to rejoin the dark criminal underworld.
  !fi
 !elif global.heist_stage==1
  I'm all set for our big heist. Give me the signal when you enter.
 !fi
!fi"
name="Alicia"


if global.heist_stage == 2 {
	orig_text = "Heh, check it out. A clean swipe.";
	no_text = "No need to trade me, I'll throw it to you.";
	hmm_text = "No need to trade me, I'll throw it to you.";
} else {
	// When you first open the trade screen
	orig_text = "I'm a criminal and thief, need to save my bag space for heists.";
	no_text = "I'm a criminal and thief, need to save my bag space for heists.";
	nothing_text = "I'm a criminal and thief, need to save my bag space for heists.";
	hmm_text = "I'm a criminal and thief, need to save my bag space for heists.";
}
// When you attempt to trade for an untradeable item.
refuse_text = "No need to trade me, I'll throw it to you.";

dont_text = "No need to trade me, I'll throw it to you.";
worthless_text = "No need to trade me, I'll throw it to you."
// When you select something you traded to an NPC
no_tradebacks_text = "No need to trade me, I'll throw it to you."
// Prefix before item name
thatsmy_text = "That's our "