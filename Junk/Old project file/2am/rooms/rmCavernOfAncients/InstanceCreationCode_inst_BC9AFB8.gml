script=@"Nobody knows this secret, but I am a reincarnation of one of the Ancients.
I'm using my ancient powers to run a museum. Would you like to hear how it works? \ask{Yes}{No}
!if answer==1
 My museum contains all the items you have collected on your adventure. You can browse the items you have seen and have fun with them.
 Every time you find a new item, a copy of it will suddenly appear in this museum, isn't that cool?
 Note that for security reasons, I will temporarily take your bag when you enter the museum, and you'll be given one of our trendy tote bags.
 You cannot take items out of the museum, it's just a place to have fun and relax. Heists are strictly forbidden.
!fi"
name="Queen"

trade_add_item(oTrendyTote);
trade_add_item(oTrendyTote);
trade_add_item(oTrendyTote);
trade_add_item(oTrendyTote);
trade_add_item(oTrendyTote);
// PRE TRADE
// When you first open the trade screen.
orig_text = "These are my amazing tote bags, you can't purchase them but you can hold one if you go in the museum.";
// When you select an item other than the selected one.
finish_text = "Oh, you'd like to look at this bag?"
// When you deselect an item.
dont_text = "Hee hee, isn't it a great bag?"
// When the NPC evaluates your offer
hmm_text = "Uh..."
// When you select something you traded to an NPC
no_tradebacks_text = "I don't want to trade it back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text =  "No, I won't trade it."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No, I won't trade it."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "No, it's not for sale."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text
