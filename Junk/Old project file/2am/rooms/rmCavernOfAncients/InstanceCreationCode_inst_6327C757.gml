script = @"An ancient inscription is on the statue.
Afternoon kitten that wants to play. Float down the river and into the bay.
!if global.flag_switch_1==0
 A switch is below the inscription, pointing to the left. Will you flip the switch? \ask{Yes}{No}
 !if answer==1
  !set global.flag_switch_1,1
  Now the switch is pointing to the right.
 !fi
!else
 A switch is below the inscription, pointing to the right. Will you flip the switch? \ask{Yes}{No}
 !if answer==1
  !set global.flag_switch_1,0
  Now the switch is pointing to the left.
 !fi
!fi"