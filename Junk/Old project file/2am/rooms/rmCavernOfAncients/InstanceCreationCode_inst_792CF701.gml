script=@"Below is the puzzle that noone has ever solved. Below is the riddle of the Ancients.
I'm such a puzzle fan, I hope to solve it one day. 
!if global.flag_puzzler==0
I actually am even known as the Puzzler, and created a puzzle of my own. Would you like to hear the rules? \ask{Yes}{No}
 !if answer==1
   I hid four Puzzle Pieces in the world. They are completely invisible and won't even show any arrows, but you can interact with them like normal items.
   If you find all four, I'll reward you with a power item. The hints to the Puzzle Pieces are in my bag.
 !else
   Go away go away go away.
 !fi
!elif 1
 You solved the puzzle that I created. Maybe you can take on the puzzle of this ruins as well.
!fi"
name="Zupzel"


trade_add_item(oLensOfLies);
trade_add_item(oPuzzleScroll,"Puzzle #001","Seek a beautiful queen, entombed in a pyramid in the metropolis.",
trade_cost(noone,0),99);
trade_add_item(oPuzzleScroll,"Puzzle #002","Seek a lonely princess, rooted to a floating island in the hills.",
trade_cost(noone,0),99);
trade_add_item(oPuzzleScroll,"Puzzle #003","Seek a skeletal crone, cruelly displayed by a cliff's edge.",
trade_cost(noone,0),99);
trade_add_item(oPuzzleScroll,"Puzzle #004","Seek a heartbroken girl, who rests below a fool in the ruins.",
trade_cost(noone,0),99);

// PRE TRADE
// When you first open the trade screen.
orig_text = "You can look over the hints for my great puzzle.";
// When you select an item other than the selected one.
finish_text = "Oh, you want something different?"
// When you deselect an item.
dont_text = "Done looking?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "I won't trade it back."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "I'm not interested, I only want to know if you have my puzzle pieces."
// When you offer a low-rank item.
low_text = "Oh, have you found all the kitten kittens? Sylvie was here"
// When you offer a mid-rank item.
medium_text = "Oh, have you found a pieces?"
// When you offer a high-rank item.
high_text = "Yes! I love that item!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "That's too much stuff."
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "You need to show me all four puzzle pieces."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Hmm, you just need one more piece..."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Great, please only show me the puzzle pieces."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Please only show me the puzzle pieces."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "This isn't for trade."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "No, this is not what I crave."
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = "You must show me four puzzle pieces for this."
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Wow, you amazingly solved my puzzle. Take this item I found."
// When you take your item back after the NPC accepts your offer.
back_text = "Hmm, why are you backing out now?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Huh?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "You can't cancel the trade now."
// When the NPC begins taking you items.
thank_text = "I'll take this puzzle."
// When a trade ends and you didn't attempt to steal.
complete_text = "I hope you had fun."
// When a trade ends and you attempted to steal.
complete_steal_text = "I hope you had fun, but also you're a criminal."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Wait, what are you doing?"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Don't waste my time, give it back."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Thank you."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "Give them all back...."
// When you pick up an item that you stole.
yes_give_text = "Yes, give it back to me."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "No, keep it. Just give back the items you promised to trade."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that."