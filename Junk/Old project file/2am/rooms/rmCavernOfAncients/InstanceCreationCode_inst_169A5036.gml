script=@"Welcome to Taco Sack, where Tacos are in the bag! Home of the Mushroom Queso Grande.
Unfortunately we are out of ingredience, can you get me some PoisonShrooms and I will make you a fulfilling meal."
name="Bella"

trade_add_item(oMushroomQuesoGrande);
trade_add_item(oMushroomQuesoGrande);
trade_add_item(oMushroomQuesoGrande);
trade_add_item(oMushroomQuesoGrande);
trade_add_item(oMushroomQuesoGrande);
trade_add_item(oMushroomQuesoGrande);

// PRE TRADE
// When you first open the trade screen.
orig_text = "Welcome to Taco Sack, may I take your order.";
// When you select an item other than the selected one.
finish_text = "Oh, you would like another version of the dish. Fine."
// When you deselect an item.
dont_text = "Sorry if you would like something else, we only have one food item on our menu."
// When the NPC evaluates your offer
hmm_text = "Hmm, are these the ingredients I seek?"
// When you select something you traded to an NPC
no_tradebacks_text = "I cannot trade back this ingredients."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "I need 2 PoisonShrooms to make a dish, nothing else will work!"
// When you offer a low-rank item.
low_text = "I need 2 PoisonShrooms to make a dish, nothing else will work!"
// When you offer a mid-rank item.
medium_text = "Yes, 2 of those deadly poisonous, shrooms then I can make this delicious dish."
// When you offer a high-rank item.
high_text = "Yes, 2 of those deadly poisonous, shrooms then I can make this delicious dish."
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I don't need that much ingredients!"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "We are out of ingredients for this dish, I will need 2 PoisonShrooms"
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Just a little more PoisonShrooms and I can make this food!"
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Just give me PoisonShrooms, I do not need other ingredients."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Just give me PoisonShrooms, I do not need other ingredients."

// EXACT TRADES
// When you offer exactly what they want.
exact_text = "That's exactly what I've been looking for!"
// When you offer an unwanted item, the item is tradeable, and it's an exact trade.
no_exact_text = no_text
// When you offer more than one of the exact item.
multiple_exact_text = "I just want one of those!"
// When you offer the exact item, but offer a different item alongside it.
minigarbage_text = "Don't need anything extra, just give me that one item!"

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "This item is not for order."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "We are out of ingredients for this dish, I will need 2 PoisonShrooms"
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Perfect, here is your food!"
// When you take your item back after the NPC accepts your offer.
back_text = "What? Why don't you want this??"
// When you offer more stuff after the NPC accepts your offer.
more_text = "More ingredients?!"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "Hey, you have ordered this item, you cannot return it."
// When the NPC begins taking you items.
thank_text = "Thanks for your order!"
// When a trade ends and you didn't attempt to steal.
complete_text = "Thank you, come back to Taco Sack tomorrow!"
// When a trade ends and you attempted to steal.
complete_steal_text = "If you keep trying to steal, I may have to ban you from this establishment."

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "No, you cannot steal my precious ingredients!!"
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "No. You must return the ingredients."
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Good. Do not mess around with me..."
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of the ingredients you promised."
// When you pick up an item that you stole.
yes_give_text = "Yeah, give back this ingredients."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "You can keep the snack, as long as you give back the ingredients you promised."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want that. Give me the original ingredients you promised."