script=@"Thank you for visiting this unknown ruins, known only as the Cavern of Ancients.
Why don't you buy from the Gift Shop? Great memorys, at low prices. We never ever close, now that time is stopped."
name="Gooney"

trade_add_item(oGiftShopCavernRock);
trade_add_item(oGiftShopCoffeeMug);
trade_add_item(oGiftShopKitteyPlush);
trade_add_item(oGiftShopPostcard);
trade_add_item(oGiftShopShirt);
trade_add_item(oGiftShopSnowGlobe);

// PRE TRADE
// When you first open the trade screen.
orig_text = "You have found the classic Gift Shop of the Cavern of Ancients, each item costs 3 SM.";
// When you select an item other than the selected one.
finish_text = "Oh, you want a different Gift item?"
// When you deselect an item.
dont_text = "Oh, you don't wish for this great Gift?"
// When the NPC evaluates your offer
hmm_text = "Hmm...."
// When you select something you traded to an NPC
no_tradebacks_text = "No refunds at this shop."
// Prefix before item name
thatsmy_text = ""
// NPC bag is empty after evaluating offer
nothing_text = "I have nothing to trade...."

// NORMAL TRADES
// When you offer an unwanted item, the item is tradeable, and it's not an exact trade.
no_text = "Each item costs 3 SM, please use standard currency."
// When you offer a low-rank item.
low_text = "Thank you, this is a great simple shroom."
// When you offer a mid-rank item.
medium_text = "Oh, now that is a nice shroom."
// When you offer a high-rank item.
high_text = "Wow, what a shroom!"
// When you offer too many items. More precisely:
// You have three extra items beyond what is needed to reach the threshold,
// and you haven't offered the exact item.
too_many_text = "I just don't need that many, why not save it for another Gift?"
// When you offer wanted items, but way too few of them. More precisely:
// When you didn't offer the exact item, and your item score is more than
// two points below the threshold, and your items are not garbage.
too_few_text = "Each item costs 3 SM."
// When you offer wanted items, but need just a little more. More precisely:
// When you didn't offer the exact item, and your item score is one or two
// points below the threshold, and your items are not garbage.
almost_text = "Just a little more, each item costs 3 SM."
// When you offer meets the threshold, but contains unwanted items.
garbage_text = "Each item costs 3 SM, don't include non standard currencys."
// When your offer does not meet the threshold, and contains a mix of wanted and unwanted items.
garbage_few_text = "Each item costs 3 SM, don't include non standard currencys."

// DENIAL
// When you offer something, but the item is untradeable.
refuse_text = "No way, I'm not trading this."
// All items are garbage
// (including when you trade for an untradeable)
// (including not matching the exact item)
worthless_text = "You have made an offer I can't accept!"
// Special worthless text used for the first item in NPC's inventory
first_worthless_text = worthless_text
// Special worthless text used if you offer an item without selecting anything
worthless_nothing_text = worthless_text

// TRADING NORMAL
// When the NPC accepts your offer.
lets_text = "Thank you, enjoy your Gift of a purchase."
// When you take your item back after the NPC accepts your offer.
back_text = "Oh no, backing out?"
// When you offer more stuff after the NPC accepts your offer.
more_text = "Expanding your offer?"
// When you take the NPC's item out of your bag after the NPC already started taking your stuff.
out_of_bag_text = "No refunds on Gift shop items."
// When the NPC begins taking you items.
thank_text = "Thanks you for your transcation."
// When a trade ends and you didn't attempt to steal.
complete_text = "Come again to the Giftshop!"
// When a trade ends and you attempted to steal.
complete_steal_text = "Get outta here!"

// STEALING
// When you take back your items after the NPC has started trying to take them.
steal_text = "Hey, don't do that..."
// When you complete a steal attempt (put the stolen item in your bag).
steal_over_text = "Waah, you're mean!"
// When you abort a steal attempt (put the stolen item back in the field)
// or when you return all the items you stole to the field.
steal_abort_text = "Meanie!"
// When you return a stolen item, but you still have stolen items in your bag.
give_back_text = "You still have some of my stuff...."
// When you pick up an item that you stole.
yes_give_text = "Yes, please give this back...."
// When you pick up an item the NPC gave you, but you have stolen items in your bag.
promise_text = "Hey, I just want the items you promised to trade."
// When you have stolen items in your bag, and pick up an item that was not part of the offer.
no_way_text  = "I don't want such. Just give the items you promised to trade."