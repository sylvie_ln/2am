{
    "id": "6bd094fe-a84a-4b5e-a20c-3f9f3e0785b2",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pathRaceRunner",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "6be8d9a1-b1fa-463c-aca6-69a7c0a3b2fe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 248,
            "y": 280,
            "speed": 100
        },
        {
            "id": "f629d588-af49-4a01-832c-ec125ffee55d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 432,
            "y": 216,
            "speed": 100
        },
        {
            "id": "27b767ce-7a3f-47fc-8512-06313a6805ba",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 624,
            "y": 280,
            "speed": 100
        },
        {
            "id": "12f71e06-aa3f-4543-aa97-8024ead740dc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 688,
            "y": 248,
            "speed": 100
        },
        {
            "id": "e246af4d-9010-4fa7-9db6-9605b8337ef0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 920,
            "y": 240,
            "speed": 100
        },
        {
            "id": "c5bd5061-8147-488c-9302-5fadfaab0300",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 984,
            "y": 264,
            "speed": 100
        },
        {
            "id": "f658d68e-09d3-4a82-905c-9464d0402860",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1072,
            "y": 232,
            "speed": 100
        },
        {
            "id": "9460bc5b-29f0-4831-a2c8-c27e6aa52416",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1280,
            "y": 240,
            "speed": 100
        },
        {
            "id": "17e09168-d3a0-428c-8e5c-54575673fd16",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1440,
            "y": 216,
            "speed": 100
        },
        {
            "id": "672dde07-dd82-418b-9a7d-713cc15fdec6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1480,
            "y": 248,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}