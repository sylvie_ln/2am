#!/bin/bash
pushd /mnt/c/Users/Sylvie
rm CCinMW_Win_Itch.zip
zip -r CCinMW_Win_Itch.zip "Clockwork Calamity in Mushroom World"
pushd "Clockwork Calamity in Mushroom World"
rm ../CCinMW_Win_Steam.zip
zip -r ../CCinMW_Win_Steam.zip *
popd
rm CCinMW_Lnx_Itch.zip
zip -r CCinMW_Lnx_Itch.zip "CCinMW_Linux"
pushd "CCinMW_Linux"
rm ../CCinMW_Lnx_Steam.zip
zip -r ../CCinMW_Lnx_Steam.zip *
popd
popd
