#include <iostream>
#include <fstream>
#include <vector>

int main() {
	std::string line;
	std::ifstream f_reborn("worlds.gml");
	std::ifstream f_original("worlds_v1.0.1.gml");
	std::vector<std::string> reborn, original;
	while(getline(f_original,line)) {
		original.push_back(line);
	}
	while(getline(f_reborn,line)) {
		reborn.push_back(line);
	}
	int L = 0;
	std::cout << original[L] << "\n";
	for(int i=0; i<100; ++i) {
		for(int d=0; d<3; ++d) {
			for(auto z : {"Creative Laughter", "Cackling Bust", "Cornered Wilderness", "Corpse Facade", "Cordless Funeral", "Crumbled Hearth", "Cuter Willows", "Captive Automaton", "Careless Desire", "Caustic Puddle", "Cataclysmic Fountain", "Cloudy Mixture"}) {
				for(auto t : {"mapp","door","item","luck","lore","cute"}) {
					++L;
					std::cerr << i << " " << d << " " << z << " " << t;
					if(i % 2 == 1 && z == "Corpse Facade") {
						std::cerr << "reborn" << "\n";
						std::cout << reborn[L] << "\n";
					} else {
						std::cerr << "original" << "\n";
						std::cout << original[L] << "\n";
					}
				}
			}
		}
	}
	++L;
	std::cout << original[L] << "\n";
}
