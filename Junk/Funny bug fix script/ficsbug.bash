#!/bin/bash
line=1

log() {
	>&2 echo "$@"
}

original() {
	tail -n +"$line" "worlds_v1.0.1.gml" | head -n 1
	line=$(( line + 1 ))
}

reborn() {
	tail -n +"$line" "worlds.gml" | head -n 1
	line=$(( line + 1 ))
}

for i in {0..99}; do
	for d in {0..2}; do
		for z in "Creative Laughter" "Cackling Bust" "Cornered Wilderness" "Corpse Facade" "Cordless Funeral" "Crumbled Hearth" "Cuter Willows" "Captive Automaton" "Careless Desire" "Caustic Puddle" "Cataclysmic Fountain" "Cloudy Mixture"; do
			for t in mapp door item luck lore cute; do
				if [ $(( i % 2 )) -eq 1 -a "$z" == "Corpse Facade" ]; then
					log "World $i Difficulty $d Zone $z Mapp $t Reborn"
					reborn
				else
					log "World $i Difficulty $d Zone $z Mapp $t Original"
					original
				fi
			done
		done
	done
done
